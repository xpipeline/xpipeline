% ---- Add location of struct comparison function to path.
addpath trunk/matlab/utilities/

format compact

% ---- Load matlab files.
oldFile  = getenv('oldFile');
newFile  = getenv('newFile');
old = load(oldFile);
new = load(newFile);

% ---- Recursively compare the contents.
tolerance = 1e-12;
disp(['Comparing the contents of files ' oldFile ' and ' newFile ...
      ' with tolerance of ' num2str(tolerance) ' for numeric data.']);
[zc,z1,z2] = comp_struct(old,new,0,0,tolerance);

% ---- Remove expected differences. These are:
%        analysis.svnversion_grbwebpage
%        analysis.svnversion_xdetection
%        triggersForHist 
%        user_tag
%      The numeric data in triggersForHist can't be compared because it is
%      in a cell array, but these are copies of triggers stored elsewhere
%      in the struct and which are compared, so we can ignore this field.
z1.analysis = rmfield(z1.analysis,'svnversion_grbwebpage');
z1.analysis = rmfield(z1.analysis,'svnversion_xdetection');
if isempty(fieldnames(z1.analysis))
    z1 = rmfield(z1,'analysis');
end
z1 = rmfield(z1,'triggersForHist');
z1 = rmfield(z1,'user_tag');
%
z2.analysis = rmfield(z2.analysis,'svnversion_grbwebpage');
z2.analysis = rmfield(z2.analysis,'svnversion_xdetection');
if isempty(fieldnames(z2.analysis))
    z2 = rmfield(z2,'analysis');
end
z2 = rmfield(z2,'triggersForHist');
z2 = rmfield(z2,'user_tag');
%
[status,result] = system('rm -f results_IDENTICAL.log results_DIFFER.log')
if isempty(fieldnames(z1)) & isempty(fieldnames(z2))
    disp('Results identical.');
    [status,result] = system('touch results_IDENTICAL.log')
else
    warning('File contents differ:');
    [status,result] = system('touch results_DIFFER.log')
    disp(' ');
    disp('Common content:');
    disp(zc);
    disp(' ');
    disp(['Differing content of ' oldFile ':']);
    disp(z1);
    disp(' ');
    disp(['Differing content of ' newFile ':']);
    disp(z2);
    disp(' ');
end


