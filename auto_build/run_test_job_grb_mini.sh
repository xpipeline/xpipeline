#!/bin/bash

# This script runs the "grb mini" example analysis. It only works for user patrick.sutton ...
echo "# ----------------------------------------------------------- "
echo "     Running the grb mini example analysis ... "
echo "# ----------------------------------------------------------- "
echo 

# Source our new installation of X-Pipeline.
echo "Executing command:    source install/bin/envsetup.sh"
source install/bin/envsetup.sh
# Kludge: source a stable installation of X-Pipeline for testing this script.
#source /home/patrick.sutton/opt/xpipeline/dev/bin/envsetup.sh

# Record our starting directory and the version of X-Pipeline, and report the python version.
basedir=$(pwd)
svnversion_full=$(report-svnversion.sh)
svnversion_number=$(report-svnversion.sh | awk '{print $NF}')
echo "Running with X-Pipeline version $svnversion_full"
#echo "                version number: $svnversion_number"
echo
echo "Using the following version of python:"
python --version
echo

# Enter the grb analysis directory. 
cd trunk/matlab/examples/grb/
grbdir=$(pwd)
echo "Moved into grb analysis directory: $grbdir"

# Set up the first-stage analysis.
echo "Running grb.py ..." 
grb.py -p grb_mini.ini -n GRB160830 -g 1156610000 -r 307.65 -d 45.72 -i H1 -i L1 --user patrick.sutton 1>grb.log 2>grb.err
echo "   ... finished. See grb.log and grb.err for messages."
# Run the first-stage analysis.
condor_submit_dag grb_alljobs.dag > condor_submit_dag.log
number=$(grep "submitted to cluster" condor_submit_dag.log | sed -s 's/\.//g' |  awk '{print $NF}') 
echo " "
echo "First-stage jobs submitted under cluster number $number"
# Wait one minute for jobs to start then watch for lock file. When that 
# disappears wait another minute or two to be sure the jobs have completed, 
# then proceed to post-processing.
echo "Waiting for condor jobs to complete. This may take up to several hours."
sleep 60
lockfile=grb_alljobs.dag.lock
while [ -f $lockfile ]
do
  sleep 60
done
echo "Condor lock file gone! Sleep for 60 seconds then proceed with post-processing." 
echo " "
sleep 60

echo "Running xgrbwebpage.py ..."
echo "Running command:   xgrbwebpage.py -g GRB160830 -d $grbdir -a $grbdir -w /home/patrick.sutton/public_html/GRB/test/ -l /local/patrick.sutton/ -u r$svnversion_number -c linearCutCirc -p 50-50 --big-mem 8000 --user patrick.sutton 1>xgrbwebpage.log 2>xgrbwebpage.err"
xgrbwebpage.py -g GRB160830 -d $grbdir -a $grbdir -w /home/patrick.sutton/public_html/GRB/test/ -l /local/patrick.sutton/ -u r$svnversion_number -c linearCutCirc -p 50-50 --big-mem 8000 --user patrick.sutton 1>xgrbwebpage.log 2>xgrbwebpage.err
echo "   ... finished. See xgrbwebpage.log and xgrbwebpage.err for messages."
echo " "

# Run the second-stage analysis.
cd auto_web/ 
condor_submit_dag grb_web.dag > condor_submit_webdag.log
webnumber=$(grep "submitted to cluster" condor_submit_webdag.log | sed -s 's/\.//g' |  awk '{print $NF}') 
echo "Submitted post-processing jobs to cluster number: $webnumber"
# Wait one minute for jobs to start then watch for lock file. When that 
# disappears wait another minute or two to be sure the jobs have completed.
echo "Waiting for condor jobs to complete. This may take up to several hours."
sleep 60
lockfile=grb_web.dag.lock
while [ -f $lockfile ]
do
  sleep 60
done
echo "Condor lock file gone! Sleep for 60 seconds then proceed." 
echo " "
sleep 60

# Copy xgrbwebpage.param file to basedir. The contents will tell us where to 
# find the web page output files for comparison to the next run of the script. 
cp xgrbwebpage.param $basedir

# Done. Return to starting directory.
echo "Finished the GRB analysis."
echo 
echo
cd $basedir

