#!/bin/bash

# This script downloads and builds the HEAD version of X-Pipeline and runs an example 
# analysis. All of the files are kept under the current directory. 
# Currently it only works for user patrick.sutton.

# INSTRUCTIONS:
#
# This script takes no input arguments, simply run it. You may wish to divert 
# screeen output to a log file, e.g.:
#
#   ./auto_build.sh 2>&1 | tee auto_build.log   
#
# The script will create a complete copy of the HEAD version of X-Pipeline in 
# a subdirectory named trunk/, and wil install the compiled pipeline in a 
# subdirectory named install/. For this reason you may wish to copy the 
# the auto_build directory to somewhere outside your working copy of X-Pipeline
# to avoid cluttering your working copy.


# cron jobs have a limited set of environment variables defined by default.
# This variable is required by grb.py. Set it here for the moment.
export LIGO_DATAFIND_SERVER=ldr.ldas.cit:80


# Record our process ID number.
pid=$(pgrep -u patrick.sutton auto_build)
# Record where we are and the locations of the source and install directories.
basedir=$(pwd)
sourcedir=$(pwd)/trunk
instdir=$(pwd)/install
# Create an empty ".lock" file to indicate that the script is running.
touch ${basedir}/auto_build.lock
# Delete files from any previous run that could cause confusion.
rm -f results_IDENTICAL.log results_DIFFER.log


echo "# ----------------------------------------------------------- "
echo "   Auto-build and test of HEAD version of X-Pipeline "
echo "# ----------------------------------------------------------- "
echo 
echo "Running as process: $pid"
echo
echo "Base directory: $basedir"
echo "Putting source code in: $sourcedir"
echo "Installing executables in: $instdir"
echo


## Check to see if we have an auto_build.log file from a previous run of the
## script. If so rename it for safe-keeping.
#if [ -f "auto_build.log" ]
#then
#    echo "Log file auto_build.log from previous run exists, Renaming to auto_build.log.old"
#    mv auto_build.log auto_build.log.old
#fi

# Check to see if we have an xgrbwebpage.param file from a previous run of the
# script. If so rename it for safe-keeping.
if [ -f "xgrbwebpage.param" ]
then
    echo "File xgrbwebpage.param from previous run exists, Renaming to xgrbwebpage.param.old"
    mv xgrbwebpage.param xgrbwebpage.param.old
fi
# Check to see if we have an xgrbwebpage.param.old file which we can use for 
# comparison to the current run.
if [ -f "xgrbwebpage.param.old" ]
then
    echo "File xgrbwebpage.param.old from previous run exists, We will run the GRB test and compare to this previous version."
    do_comparison=true
else
    echo "No file xgrbwebpage.param.old from previous run exists, We will run the GRB test but make no comparison to previous versions."
    do_comparison=false
fi


# Download X-Pipeline if required; otherwise clean up the previous installation and update to HEAD.
# Check for X-Pipeline by seeing if the trunk/ directory exists.
# WARNING: My kludged {} || {} attempt at a try-catch block does not seem to work.
{ 
    if [ ! -d "trunk/" ] 
    then
        echo "Directory trunk/ does not exist." 
        echo "Creating one and checking out latest version of X-Pipeline." 
        echo "Executing command:    svn checkout https://svn.ligo.caltech.edu/svn/xpipeline/trunk"
        svn checkout https://svn.ligo.caltech.edu/svn/xpipeline/trunk
        echo 
        cd trunk
        svnversion=$(svnversion .)
    else
        # Directory exists. Enter it and get a clean copy of the latest version of X-Pipeline.
        echo "Directory trunk/ exists. Updating to latest version of code." 
        echo "Executing command:    cd trunk"
        cd trunk
        echo "Executing command:    source clean_svnversion.sh"
        source clean_svnversion.sh
        svnversion_old=$(svnversion .)
        echo "Executing command:    svn update"
        svn update
        svnversion=$(svnversion .)
        echo 
        if [ $svnversion = $svnversion_old ] ; 
        then
            echo "svn version unchanged. No need to proceed further."
            exit 0
        fi
    fi
    echo "Current version of X-Pipeline: $svnversion"
} || { 
    echo "Error: unable to download / update X-Pipeline source. Exiting."
    exit 1
}
echo 
echo


# We are now in the trunk/ directory ($instdir). Build X-Pipeline.
# To do: Automate so that previous build is moved to "old", previous "old" is deleted?
echo "# ----------------------------------------------------------- "
echo "   Commencing build of HEAD version of X-Pipeline "
echo "# ----------------------------------------------------------- "
echo 
echo "Commencing build procedure ... creating installation directory $instdir"
rm -rf $instdir
mkdir -p $instdir
echo "Executing command:    ./00boot"
./00boot
echo "Executing command:    ./configure --prefix=$instdir  --with-frgetvectpath=/home/edaw/libframe/matlab --with-xtarget=everything --with-matlab=/ldcg/matlab_r2015a"
./configure --prefix=$instdir  --with-frgetvectpath=/home/edaw/libframe/matlab --with-xtarget=everything --with-matlab=/ldcg/matlab_r2015a
echo "Executing command:    make"
make
echo "Executing command:    make install"
make install
echo "Executing command:    source $instdir/bin/envsetup.sh"
source $instdir/bin/envsetup.sh
echo "Executing command:    cp $sourcedir/local_mcr_script.sh $instdir/bin/"
cp $sourcedir/local_mcr_script.sh $instdir/bin/
echo "Executing command:    cd $instdir/bin/"
cd $instdir/bin/
echo "Executing command:    source local_mcr_script.sh"
source local_mcr_script.sh
echo "   ... finished build procedure."
echo 
echo 
 

# Now run a test job.
cd $basedir
./run_test_job_grb_mini.sh


# Now check that everything completed and run a comparsion of the output to the expected output.
if [ "$do_comparison" == true ]
then
    ./compare_results.sh
else
    echo "# ----------------------------------------------------------- "
    echo "    Since no previous GRB test run exists, there are no " 
    echo "    comparisons to be done. Script finished."
    echo "# ----------------------------------------------------------- "
    echo 
fi

# Remove lock file.
rm ${basedir}/auto_build.lock


