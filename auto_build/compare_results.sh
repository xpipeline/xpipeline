#!/bin/bash

# This script compares the output of the "grb mini" example analysis to that of the previous successful auto_build run.
echo "# ----------------------------------------------------------- "
echo "     Running the results comparison test ... "
echo "# ----------------------------------------------------------- "
echo 
echo "Comparing the output of the current grb mini example analysis to that of the previous version ... "
echo 

# Locate matlab summary files for current (new) and previous (old) runs:
old_name=$(awk '{{for(i=1;i<=NF;i++)if($i == "-g") printf $(i+1)} print ""; }' xgrbwebpage.param.old)
old_webdir=$(awk '{{for(i=1;i<=NF;i++)if($i == "-w") printf $(i+1)} print ""; }' xgrbwebpage.param.old)
old_usertag=$(awk '{{for(i=1;i<=NF;i++)if($i == "-u") printf $(i+1)} print ""; }' xgrbwebpage.param.old)
old_file="${old_name}_${old_usertag}_closedbox.mat"
old_path="${old_webdir}/${old_name}_${old_usertag}/"
#
new_name=$(awk '{{for(i=1;i<=NF;i++)if($i == "-g") printf $(i+1)} print ""; }' xgrbwebpage.param)
new_webdir=$(awk '{{for(i=1;i<=NF;i++)if($i == "-w") printf $(i+1)} print ""; }' xgrbwebpage.param)
new_usertag=$(awk '{{for(i=1;i<=NF;i++)if($i == "-u") printf $(i+1)} print ""; }' xgrbwebpage.param)
new_file="${new_name}_${new_usertag}_closedbox.mat"
new_path="${new_webdir}/${new_name}_${new_usertag}/"

echo "Comparing current X-Pipeline results with those of previous run."
echo " Current results file: ${new_path}${new_file}"
echo "Previous results file: ${old_path}${old_file}"

# Export these file names (with paths) as environment variables. These can be 
# imported by the matlab script - an easy way to pass in arguments wen running 
# matlab in batch mode. 
export oldFile=${old_path}${old_file}
export newFile=${new_path}${new_file}

# Call the matlab script that compares the results files.
/ldcg/matlab_r2015a/bin/matlab -nosplash -nodesktop < compare_results.m
#/Applications/MATLAB_R2017a.app/bin/matlab -nosplash -nodesktop < /Users/psutton/Documents/Projects/xpipeline/compare_results.m 2>&1

# The matlab script creates an empty file with a name that indicates whether
# the two runs produced the same results. Check to see which exists.
if [ -f "results_IDENTICAL.log" ]
then
    message="HEAD version of X-Pipeline (${new_usertag}) produces results identical to previous tested version (${old_usertag}). See log file for details." 
elif [ -f "results_DIFFER.log" ]
then
    message="HEAD version of X-Pipeline (${new_usertag}) produces results different to previous tested version (${old_usertag}). See log file for details." 
else
    message="ERROR: Unable to compare HEAD version of X-Pipeline (${new_usertag}) to previous tested version (${old_usertag}). See log file for details." 
fi
# Ecgho message to the screen and send to the user via email.
echo $message 
echo $message | 
    /usr/bin/mail \
    -s "x-pipeline auto-build results for ${new_usertag}" \
    -a auto_build.log \
    "<${USER}@ligo.org>" |
    >/dev/null 2>&1
echo "$message | /usr/bin/mail -s x-pipeline auto-build results for ${new_usertag} -a auto_build.log <${USER}@ligo.org> >/dev/null 2>&1"

# Done. Return to starting directory.
echo
echo "Finished!"
echo
echo "# ----------------------------------------------------------- "
echo "   COMPLETED: build and test of HEAD version of X-Pipeline "
echo "# ----------------------------------------------------------- "
echo


