#!/usr/bin/env python
"""
XFIXZOMBIEJOBS - kill condor jobs that are running longer than expected 

$Id$

"""
__author__  = 'Patrick Sutton <patrick.sutton@ligo.org>'
__date__    = '$Date$'
__version__ = '$Revision$'

import sys, os, getopt
from time import sleep


# ---- Function usage.
def usage():
    msg = """\
XFIXZOMBIEJOBS - kill condor jobs that are running longer than expected 

Usage: 
    xfixzombiejobs.py [options]
    -u, --user <name>    Optional user name of jobs to be checked. If not 
                         specified, defaults to the script's user.
    -t, --time <hours>   Optional maximum run time of jobs, in hours. If not
                         specified, defaults to 4 hours.
    --dry-run            Optional flag. By default, a script is generated that 
                         the user can run to kill the zombie jobs. If this flag 
                         is invoked then no script is generated.
    -h, --help           Display this message and exit.

  e.g.,
    xfixzombiejobs.py -u patrick.sutton -t 2.5

The output of condor_q for the user is parsed to find jobs that have been 
running longer than a predetermined time. Such jobs are killed.

Note that typically the script can only fix the user's own jobs to due to condor
and file-system permissions. For other users information on zombie jobs is 
reported but the kill script (if generated) won't work for them.
"""
    print(msg, file=sys.stderr)


def xfixzombiejobs(user=None,max_time=4.0,dry_run=False):

    # --------------------------------------------------------------------------
    #    Find zombie jobs
    # --------------------------------------------------------------------------

    if not user:
        # ---- Guess the condor username.
        user = os.popen('whoami').read().rstrip()

    # ---- Convert max_time to seconds, for convenience.
    max_time = max_time * 3600

    # ---- We will be using temporary files to capture and process output from 
    #      condor_q. We will put these in the user's home directory. 
    user_home = os.path.expanduser('~')
    zombie_file = user_home + "/.zombie_jobs.txt"
    kill_file = user_home + "/kill_zombie_jobs.sh"

    # ---- Call condor_q, then extract only lines containing the user name and 
    #      which are not for the dag. This command returns the user name 
    #      truncated to 14 characters, so truncate the name in the call to grep.
    os.system("condor_q -nobatch " + user + " | grep " + user[0:14] + " | grep -v dagman > " + zombie_file)

    # ---- Parse job list.
    print("The following jobs have been running longer than", max_time, "seconds:")
    number_of_zombies = 0 
    kill_file_open = False
    f = open(zombie_file,'r')
    lines = f.read().splitlines()     
    for line in lines:
        # ---- Format of file: ID OWNER SUBMITTEDDAY SUBMITTEDTIME RUN_TIME ST PRI SIZE CMD
        job_ID = line.split()[0]
        job_time = line.split()[4]
        job_type = line.split()[8].split()[0]
        # ---- Convert job run time to seconds.
        #      This script converts a time in the format reported by condor_q to seconds.
        #      Understood formats are of the form 21+18:26:30, 15:28:37, or 47:12.
        tmp = job_time.split(':')
        if len(tmp)==2 :
            job_time = int(tmp[0])*60+int(tmp[1])
        elif len(tmp)==3 :
            if len(tmp[0].split('+'))==1 :
                job_time = int(tmp[0])*3600+int(tmp[1])*60+int(tmp[2])
            else :
                job_time = int(tmp[0].split('+')[0])*86400+int(tmp[0].split('+')[1])*3600+int(tmp[1])*60+int(tmp[2])
        if job_time > max_time :
            print("Job: " + job_ID + "  Type: " + job_type + "  Run time:",  job_time,  "seconds")
            number_of_zombies = number_of_zombies + 1 
            if not dry_run :
                if not kill_file_open :
                    kill = open(kill_file,'w')
                    kill_file_open = True
                # ---- Append kill command to condor_rm.txt file.
                kill.write("condor_rm " + job_ID + "\n")
    f.close()
    if kill_file_open :
        kill.close()


    # -----------------------------------------------------------------------------
    #    Report to the user.
    # -----------------------------------------------------------------------------

    if number_of_zombies == 0 :
        print("No zombie jobs found.")
    else : 
        print("Found", number_of_zombies, "jobs running longer than", max_time, "seconds.")
        print("To kill these jobs run the script " + kill_file)


    # -----------------------------------------------------------------------------
    #    Clean up.
    # -----------------------------------------------------------------------------

    os.system("rm -rf " + zombie_file)


if __name__ == '__main__':
    # ---- This function is being executed directly as a script (as opposed to 
    #      being imported by another code). Parse the command line arguments 
    #      then run the xfixzombiejobs() function.

    # -------------------------------------------------------------------------
    #      Parse the command line options.
    # -------------------------------------------------------------------------

    # ---- Assign defaults to command-line arguments.
    user = None
    max_time = 4.0  # hours
    dry_run = False

    # ---- Syntax of options, as required by the getopt command.
    # ---- Short form.
    shortop = "hu:t:"
    # ---- Long form.
    longop = [
        "help",
        "user=",
        "time=",
        "dry-run"
        ]

    # ---- Get command-line arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    # ---- Parse command-line arguments.  Arguments are returned as strings, so 
    #      convert type as necessary.
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-u", "--user"):
            user = a      
        elif o in ("-t", "--time"):
            max_time = float(a)
        elif o in ("--dry-run"):
            dry_run = True 
        else:
            print("Unknown option:", o, file=sys.stderr)
            usage()
            sys.exit(1)

    # ---- Call the xfixzombiejobs() function.
    xfixzombiejobs(user,max_time,dry_run)


