function xjitterinclinationgrbinjectionfile(inFile,outFile,sigmaOmega)
% XJITTERINCLINATIONGRBINJECTIONFILE - Simulate calibration and position errors.
%
% XJITTERINCLINATIONGRBINJECTIONFILE modifies an X-Pipeline formatted injection
% log file to simulate the effect of gamma ray collimation cone
%
% usage:
%
%  xmiscalibrategrbinjectionfile(inFile,outFile,sigmaOmega)
%
%  inFile           String.  Name of input injection file to be modified.
%  outFile          String.  Name of output file with modified injections.
%  sigmaOmega       String.  Standard deviation in inclination
%                   uncertainty [rad]. Optional; default 0 (no error).
%
% $Id$

% ---- Check input.
error(nargchk(2, 3, nargin, 'struct'))
if nargin < 3
    sigmaOmega = 0;
else
    if ~ischar(sigmaOmega)
        error('Input sigmaOmega must a string.');
    else
        sigmaOmega = str2num(sigmaOmega);
    end
end
if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end

if isempty(strfind(inFile,'incljitter'))
  disp(['Skipping inclination jittering, no "incljitter" string in injection ' ...
        'file name: ' inFile]);
  return
end

% ---- Read and parse input injection file.
injection = readinjectionfile(0,1e10,inFile);
[gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);
% ---- Parser code foolishly outputs "name" and "parameters" as cell arrays
%      of 1x1 cell arrays of strings; reformat to remove extra layer of
%      cell arrays.
name = cell(nInjection,1);
parameters = cell(nInjection,1);
for ii = 1:nInjection
    name{ii} = temp_name{ii}{1};
    parameters{ii} = temp_parameters{ii}{1};
end

% ---- Location of amplitude and central frequency parameters depends on
%      the waveform type.
paramArray = cell(nInjection,1);
amplIndex = zeros(nInjection,1);
for ii = 1:nInjection
    switch lower(name{ii})
        case {'chirplet'}  
            % ---- Chirping sine/cosine-Gaussian.  Parameters are
            %      [hrss,tau,f0,alpha,delta,iota].
            inclIndex(ii) = 6;
            paramArray{ii} = tildedelimstr2numorcell(parameters{ii},1);
            incl(ii) = paramArray{ii}{inclIndex(ii)};
        case {'inspiral','inspiralsmooth','lalinspiral'}  
            % ---- Post-Newtonian inspiral.  Parameters are
            %      [mass1,mass2,iota,dist].  Hardwire central frequency to
            %      155 Hz -- good approximation for SRD noise for 1.4-1.4
            %      and 1.4-5.0 binaries.
            inclIndex(ii) = 3;
            paramArray{ii} = tildedelimstr2numorcell(parameters{ii},1);
            incl(ii) = paramArray{ii}{inclIndex(ii)};
        otherwise
            error(['Current version of the function can only handle ' ... 
                'chirplet, inspiral waveform types.  See xmakewaveform.']); 
    end
    if incl(ii) ~= 0
      error(['Jittering inclination works only for jittering around ' ...
             '0, you asked for: ' num2str(incl(ii))]);
    end
end

% ---- Set the random number generator seed based on the name of the
% input file
randn('state',sum(inFile))
rand('twister',sum(inFile))

% ---- Jitter injection inclination
inclination = acos((1-cos(sigmaOmega))*rand(nInjection,1)+cos(sigmaOmega));
directionFlip=random('norm',0,1,nInjection,1)>0;
% ---- Set inclination of first injection to 0 so that web page
% captions look sensible
inclination(1) = 0;
directionFlip(1) = 0;

if any(inclination>=pi)
    error(['Inclination angle above pi -- exponential distribution needs ' ...
           'to be wrapped']);
end

% ---- Write down the jittered injection parameters
numData = cell(1,1);
nameData = cell(1,1);
paramData = cell(1,1);
numData{1} = [gps_s(:,1), gps_ns(:,1), phi, theta, psi];
nameData{1} = name;
paramData{1} = cell(nInjection,1);
for ii = 1:nInjection
  jitteredParam = paramArray{ii};
  jitteredParam{inclIndex(ii)} = inclination(ii,1) + ...
      directionFlip(ii,1)*(3.14159 - 2*inclination(ii,1)); %-- reset inclination
  paramData{1}{ii} = numorcell2tildedelimstr(jitteredParam);
end

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
  %----- Write signal type and parameters to output file.
  fprintf(fid,'%d %d %e %e %e ',numData{1}(ii,:));
  fprintf(fid,'%s ',nameData{1}{ii});
  fprintf(fid,'%s ',paramData{1}{ii});
  fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return
