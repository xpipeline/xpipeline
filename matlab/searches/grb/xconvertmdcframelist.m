function [] = xconvertmdcframelist(gravenFrameDir,xpipelineFrameList)
% XCONVERTMDCFRAMELIST - convert BurstMDC frame list file to X-Pipeline format.
%
% usage:
%
%   xconvertmdcframelist(gravenFrameDir,xpipelineFrameList)
%
% gravenFrameDir      String. Path of dir containing GravEn/BurstMDC frame list.
% xpipelineFrameList  String. Name of output X-Pipeline formatted frame list.
%
% Note: By not specifying the specific name of the GravEn framelist (which 
% contains a unique numeric string) we simplify the batch production of 
% X-Pipeline formatted MDC frame list files.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- GravEn frame list format:
%        frames/H-H1-SGC1000Q8d9-842946645-256.gwf 16172604 842946645
% ---- X-Pipeline frame list format:
% H H1-SGC1000Q8d9 842946645 842946901 256 /data/node1/eharstad/ExtTrigMDC/MOCK_842946773/SGC1000Q8d9/frames

dirContents = dir([gravenFrameDir '/ExtTrig*.txt']);

gravenFrameList = [gravenFrameDir '/' dirContents.name]

textformat = ['%s %f %f'];

fid = fopen(gravenFrameList,'r');
thisCell = textscan(fid, textformat,'CommentStyle','#');
fclose(fid); 

framelistsIdx = strfind(gravenFrameList,'framelists');
framePath = ([gravenFrameList(1:framelistsIdx - 1)  'frames' ])

gravenPaths = thisCell{1};
dummy       = thisCell{2};
gpsStart    = thisCell{3};

duration = [];
gpsEnd   = [];
site     = {};
name     = {};

for idx = 1:length(gravenPaths)
   dashIdx       = strfind(gravenPaths{idx},'-');
   duration(idx) = str2num(gravenPaths{idx}(dashIdx(4)+1:length(gravenPaths{idx})-4));
   gpsEnd(idx)   = gpsStart(idx) + duration(idx);
   site{idx}     = gravenPaths{idx}(dashIdx(1)-1:dashIdx(1)-1);
   name{idx}     = gravenPaths{idx}(dashIdx(1)+1:dashIdx(3)-1);
end

% ---- Write X-Pipeline formatted list file.
fout = fopen(xpipelineFrameList,'w');
for idx=1:length(gravenPaths)
    fprintf(fout,'%s %s %d %d %d %s \n', ...
       site{idx}, name{idx}, gpsStart(idx), gpsEnd(idx), duration(idx), framePath);
end
fclose(fout);

