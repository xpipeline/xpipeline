function [UL90p] = xbatchwebsimplesummary(openFileList,ulFileList,user_tag,popDetStr,zFile,inputData)
% XBATCHWEBSIMPLESUMMARY - create a web report for a batch FRB analysis
%
% xbatchwebsummary(openFileList,ulFileList,userTag,popDetStr,zFile,inputData)
%
% openFileList   String. Name of file containing the list of open-box
%                result files to process (best if in a web-accessible
%                location). Should be produced by xbatchwebsummary.py.
% ulFileList     String. Name of file containing the list of ul-box
%                result files to process (best if in a web-accessible
%                location). Should be produced by xbatchwebsummary.py.
% userTag        String. Arbitrary tag that will be included in the name
%                of all produced files.
% popDetStr      Optional string. Tilde-delimited string containing the
%                names of population detection statistics to use; see
%                POPDETSTAT for allowed values. Set to an empty string ('')
%                if not needed.
% zFile          Optional string. Name of file containing observed external
%                trigger redshifts. The text file format must a single
%                column containing the different redshifts; it does not
%                need to be sorted. If unavailable use an empty string ('').
% inputData      Optional string. Name of file containing data necessary to
%                construct results, for instance the output .mat file of
%                a previous run of this function. Data from this file
%                will be used, instead of the data scattered in the files
%                listed in openFileList and ulFileList. This option is
%                useful for processing results from CBC searches, or
%                rerunning after minor code changes.
%
% Example usage (O3A FRB search) in interactive matlab:
%
%  >> openFileList = 'tmpGRBopen2.txt'
%  >> ulFileList = 'tmpGRBul2.txt'
%  >> user_tag = 'paper'
%  >> xbatchwebsummary(openFileList,ulFileList,userTag)
%
% $Id$

% ---- Check number of input arguments.
narginchk(3, 6);

% ---- Assign default values to optional arguments.
if nargin < 5
    zFile = '';
end
if nargin < 4
    popDetStr = '';
end

% ---- Format stdout.
format compact


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Load data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin <= 5
    
    % ---------------------------------------------------------------------
    %    Get list of .mat files
    % ---------------------------------------------------------------------
    
    % ---- No inputData argument supplied; read analysis results from the
    %      matlab files listed in openFileList, ulFileList.
    
    % ---- Read files listing result .mat files.
    fid = fopen(openFileList);
    openFileCell = textscan(fid,'%s');
    openFileCell = openFileCell{1};% convert to single vector cell
    fclose(fid);
    fid = fopen(ulFileList);
    ulFileCell = textscan(fid,'%s');
    ulFileCell = ulFileCell{1};% convert to single vector cell
    fclose(fid);
    
    % ---- Check for consistency between the two file lists.
    if length(ulFileCell) ~= length(openFileCell)
        error(['The list of files in: ' openFileList ' and in ' ulFileList ...
            ' do not have the same length.'])
    else
        nGRBs = length(openFileCell);
    end
    
    % ---------------------------------------------------------------------
    %    Read .mat and .ini files for each GRB
    % ---------------------------------------------------------------------
    
    % ---- Loop over GRBs.
    disp(['Will process ' num2str(nGRBs) ' external triggers']);
    for iGRB = 1:nGRBs
        
        % ---- Load results for this GRB: on-source events from open-box file, 
        %      injection information from ul-box file, and 50% efficiencies from
        %      closed-box file.  
        openRes = load(openFileCell{iGRB},'analysis','onSource');
        ulRes   = load(ulFileCell{iGRB},'analysis','UL90p','efficiencySorted','injectionScaleSorted','cFreq');
        closedFileName = regexprep(openFileCell{iGRB},'openbox','closedbox');
        closedRes = load(closedFileName,'UL50p');        

        % ---- Verify that the names are consistent for both results files
        %      before proceeding. 
        if not(strcmp(openRes.analysis.grb_name,ulRes.analysis.grb_name))
            error(['The names of the trigger in the open-box and ul-box files do not ' ...
                'match:'  openRes.analysis.grb_name ' ' ulRes.analysis.grb_name])
        else
            grbName{iGRB} = openRes.analysis.grb_name;
            disp(['**** Processing trigger ' num2str(iGRB)  ' of ' num2str(nGRBs) ': ' grbName{iGRB} ' **** ']);
        end
        
        % ---- Save the analysis struct for all GRBs, for checks.
        analysis{iGRB} = ulRes.analysis;

        % ---- Record on-source events and the per-GRB probability of the
        %      on-source loudest event.
        onSource{iGRB} = openRes.onSource;
        if isempty(min(openRes.onSource.probability(logical(openRes.onSource.pass))))
            % ---- KLUDGE: set background probability to 1 if no surviving
            %      events found.
            pOnSource(iGRB) = 1;
        else
            pOnSource(iGRB) = min(openRes.onSource.probability(logical(openRes.onSource.pass)));
        end
        
        % ---- Before extracting efficiencies, verify that all GRBs are
        %      performing the same injections. Check the ini files for this.
        
        % ---- Find .ini file by parsing the grb.param file in the auto_dir directory.
        idx = strfind(openFileCell{iGRB},'/');
        auto_dir = openFileCell{iGRB}(1:idx(end));
        [grb_cmd, grb_args] = parseparamfile([auto_dir 'grb.param']);
        ini_file = [];
        for ii=1:size(grb_args,1)
            if strcmp(grb_args{ii,1},'-p') || strcmp(grb_args{ii,1},'--params-file')
                % ---- If the ini file string starts with "/" then it is specified with full 
                %      path, otherwise its location is relative to auto_dir.
                if grb_args{ii,2}(1)=='/'
                    ini_file = [grb_args{ii,2}];
                else
                    ini_file = [auto_dir grb_args{ii,2}];
                end
            end
        end
        % ---- Parse .ini file.
        ini_data = parseinifile(ini_file);
        
        % ---- Extract list of injection sets and associated parameters for this
        %      GRB from the ini file. Parameter strings are of the form, eg, 
        %      'chirplet!1.0e-21~0.003448~290~0~0~-1;1;linear' so should be
        %      split on the '!'.
        wf_sets = fieldnames(ini_data.waveforms);
        for ii=1:length(wf_sets)
            tmp = getfield(ini_data.waveforms,wf_sets{ii});
            idx = strfind(tmp,'!');
            wf_types{ii,1}   = tmp(1:idx-1);
            wf_params{ii,1} = tmp(idx+1:end);
        end
        
        % ---- Verify that injection sets are the same as for the first GRB.
        waveformList = [];
        if iGRB == 1
            % ---- Use the injections for the first GRB as the "master" list.
            master_sets   = wf_sets;
            master_types  = wf_types;
            master_params = wf_params;
            master_scales = ulRes.injectionScaleSorted;
            rescale       = ones(1,length(master_params));
            %
            nWave = length(master_sets);
            waveformList = 1:nWave;
        else
            % ---- Verify that GRBs 2,3,... used the same injection sets, and
            %      correct the order if it is different for some reason. Any
            %      additional sets will be ignored; any missing sets will cause
            %      an error. Compare by set name.
            for iWave = 1:nWave
                waveformList = [waveformList find(strcmp(master_sets{iWave},wf_sets))];
            end
            if length(waveformList) < nWave
                error(['Trigger ' num2str(iGRB) ' (' openFileCell{iGRB} ') contains only ' ...
                    num2str(length(waveformList)) ' injection sets instead of the ' ...
                    num2str(nWave) ' expected.'])
            end
            % ---- Also verify that the injection type and parameters are the
            %      same, and check for any differences in amplitude parameters.
            [valid,rescale] = compareinjparams(wf_types(waveformList), ...
                wf_params(waveformList),master_types,master_params);
            rescale = rescale(:).'; %-- ensure row vector
            if ~valid
                error(['Trigger ' num2str(iGRB) ' (' openFileCell{iGRB} ') uses different ' ...
                    'injection parameters than the first trigger.']);                
            end
            % ---- When applying the rescale factor to correct for differing injection
            %      amplitudes between runs we need to rescale differently when
            %      parameter=distance=1/amplitude (divide hrss ULs by rescale factor; 
            %      multiply distance LLs by rescale factor). To keep things simple 
            %      invert rescale factor for'distance' types so all waveforms can be 
            %      handled by the same code.          
            idx = find( strcmp(master_types,'inspiral') | ...
                        strcmp(master_types,'adi-a') | ...
                        strcmp(master_types,'adi-b') | ... 
                        strcmp(master_types,'adi-c') | ... 
                        strcmp(master_types,'adi-d') | ... 
                        strcmp(master_types,'adi-e') );
            if ~isempty(idx)
                rescale(idx) = 1./rescale(idx);
            end
            % ---- Finally verify that the same injection scales are used.
            if ulRes.injectionScaleSorted(waveformList,:) ~= master_scales
                error(['Trigger ' num2str(iGRB) ' (' openFileCell{iGRB} ') uses different ' ...
                    'injection scales than the first trigger.']);                
            end
        end

        % ---- We are now ready to extract the efficiency and upper limit results.

        % ---- 50% amplitude sensitivity estimates from the closed-box run (uses
        %      the same injections in the same order as the open-box run). 
        detSensCL99(iGRB,:) = closedRes.UL50p(waveformList) ./ rescale;
        % ---- 90% amplitude limits from the ul-box run. 
        UL90p(iGRB,:) = ulRes.UL90p(waveformList) ./ rescale;
        
        % ---- Record the efficiency curves. We correct these by rescaling the
        %      injection scales.
        effCurve(iGRB,:,:) = ulRes.efficiencySorted(waveformList,:);        
        injScale(iGRB,:,:) = ulRes.injectionScaleSorted(waveformList,:) .* ...
            repmat(rescale.',[1,size(ulRes.injectionScaleSorted,2)]);

    end  %-- loop over GRBs
    
else
    
    % ---------------------------------------------------------------------
    %    Read inputData file
    % ---------------------------------------------------------------------
    
    % ---- Read results from the provided inputData file.
    load(inputData,'UL90p','onSource','master_params','injScale','effCurve',...
        'master_types','pOnSource','nGRBs','grbName','detSensCL99','user_tag',...
        'nominalDist','detSensCL99orig','nWave');
    disp(['Loaded results for ' num2str(nGRBs) ' external triggers']);
    
end

% ---- Convert nGRBs to an integer data type.
nGRBs = int32(nGRBs);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Write web page header
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Write web page header. Start by setting fake names to reuse the
%      header function from xmakegrbwebpage.
dummyAnalysis.grb_name = 'multipleTriggers';
dummyAnalysis.type     = 'simple';
dummyAnalysis.svnversion_xdetection = 'NOT RELEVANT';
% ---- Try to get svnversion from report-svnversion.sh.
[status,result] = ...
    system('sh $XPIPE_INSTALL_BIN/report-svnversion.sh');
if status==0
    dummyAnalysis.svnversion_grbwebpage = result;
else
    warning('report-svnversion.sh has failed');
    disp(status);
    disp(result);
    warning('setting dummyAnalysis.svnversion_grbwebpage to Unknown');
    dummyAnalysis.svnversion_grbwebpage = 'Unknown';
end
[fout,figures_dirName,figfiles_dirName] = xwritewebpageheader(user_tag,dummyAnalysis);

% ---- Name of output web page.
webPageFileName = [dummyAnalysis.grb_name '_' user_tag '_' dummyAnalysis.type '.shtml'];

% ---- Clear dummy dummyAnalysis structure used to create the web page header
clear dummyAnalysis


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Perform binomial null test
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',['<h2>  Binomial null test (unweighted, 5% tail) </h2>']);
% ---- MAGIC NUMBERS:
Nmc = 1e5;
Ptail = 0.05; %-- test top 5%.

% ---- Sort the p values and take the top Ptail.
[p,I] = sort(pOnSource);
Ndraws = length(p);
Ntail = ceil(Ptail*Ndraws);

% ---- Perform binomial test on all GRBs.
disp('Performing binomial test on all GRBs ...');
[Pbinom, Pmin, index] = grbbinomialtest(p(1:Ntail),Ndraws,Nmc);
fprintf(fout,'%s<br>\n',['Binomial test results for all GRBs:']);
fprintf(fout,'%s<br>\n',['  Background probability = ' num2str(Pmin) ]);
fprintf(fout,'%s<br>\n',['  The largest deviation occurs when looking at ' ...
    'the lowest ' num2str(index) ' p-values' ]);
disp(['  Consistency with the null hypothesis is ' num2str(Pmin) '.']);

% ---- Compute distribution of minimum binomial probability under the null
%      hypothesis.
threshold = 5;
disp(['Find ' num2str(threshold) ' percentile cumulative binomial probability']);
P_1pc = grbbinomialtest_threshold(Ndraws,Ntail,threshold,Nmc);

% ---- Compute 2-sigma point-by-point contours, lower and upper contour.
localProbMC = sort(rand(Nmc,Ndraws),2);
Plow  = prctile(localProbMC, 100*erfc(2/sqrt(2))/2);
Phigh = prctile(localProbMC, 100*(1-erfc(2/sqrt(2))/2));

% ---- Compute binomial probabilities for 1% consistency with null
%      hypothesis (consistency reported in paper).
localProbMC = logspace(-4,-1,100);
p0d948 = zeros(Ntail,1);
for itail = 1:Ntail
    PMC = log(1 - binocdf(itail-1,Ndraws,localProbMC));
    % ---- Approximation to low value by first binomial term in cdf.
    PMC(PMC<log(1e-15)) = log(nchoosek(Ndraws,itail)) + itail* ...
        log(localProbMC(PMC<log(1e-15)));
    PMC = PMC + 0.0001*random('unif',0,1,size(PMC));
    p0d948(itail) = exp(interp1(PMC,log(localProbMC),log(P_1pc)));
end

% ---- Plot binomial test
figure; set(gca,'fontsize',16);
loglog([1e-9;1],[1e-9;1],'k--','linewidth',1)
grid on; hold on
loglog(p,[1/Ndraws:1/Ndraws:1]','-o','linewidth',2,'markerfacecolor','b')
% ---- Add first to obtain a starting vertical line, and extend last bar up
%      to infinity.
prcEdge = ([[0:Ntail]']+0.5)/Ndraws;
prcEdge(Ntail+1) = 1;
stairs([p0d948(1)*0.99; p0d948],prcEdge,'k-','linewidth',2)
plot(Plow,1/Ndraws:1/Ndraws:1,Phigh,1/Ndraws:1/Ndraws:1)
axis([min(min(p),1e-4) 1 5e-1/Ndraws 1])
axis square
xlabel('local probablity p');
ylabel('fraction of GRBs')
legend('expected','data',['needed for ~' num2str(threshold) '% CL'],'Location','SouthEast')
loglog(p(index),index/Ndraws,'rs','markerfacecolor','r')
% ---- Save plot and put into table
plotName = '/binomialTest';
xsavefigure(plotName,figures_dirName,figfiles_dirName);
xaddfiguretable(fout,plotName,figures_dirName);
clear plotName


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Perform population detection LR test
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ----- Generate a single set of MC realizations and use the same one for
%       all tests (should reduce statistical error of comparision).
pMC = rand([size(detSensCL99) Nmc]);

% ---- Rescale detector sensitivity to the best one.
detSensCL99orig = detSensCL99;
detSensCL99 = detSensCL99./repmat(min(detSensCL99),[nGRBs 1]);

% ---- Loop over methods.
methodCell = tildedelimstr2numorcell(popDetStr);
for iMethod = 1:length(methodCell)
    
    popMethod = methodCell{iMethod};
    disp(['Performing population detection test with: ' popMethod])
    fprintf(fout,'%s\n',['<h2>  Population detection, method: ' popMethod ' </h2>']);
    
    % ---- Compute population statistic for the on-source p-values.
    popDetStat = popdetstat(repmat(pOnSource',[1 size(detSensCL99,2)]),popMethod,detSensCL99);
    % ---- Compute population statistic for Nmc realizations of the null hypothesis.
    popDetStatMC = popdetstat(pMC, popMethod, detSensCL99);
    
    % ---- Plot background distribution of population detection statistics
    %      and obtained results. Cover cumulative probabilities from 1e-3 to 1.
    plotThr = logspace(-3,0,100);
    popDetStatBkgPlot = prctile(popDetStatMC,(1-plotThr)*100,2);
    figure; hold on
    set(gca,'FontSize',20)
    plot(popDetStatBkgPlot(1,:),plotThr,'k-','linewidth',2);
    % ---- Interpolate background distribution to find background prob of
    %      on-source p-values. Use NaN in case of errors (common in closed-box
    %      analysis case).
    [tmp uniqueXvalues] = unique(popDetStatBkgPlot(1,:));
    try
        popBkgProb = interp1(popDetStatBkgPlot(1,uniqueXvalues), ...
            plotThr(uniqueXvalues)',popDetStat(1));
    catch
        warning('Failed to compute on-source bkg prob')
        popBkgProb = NaN;
    end
    plot(popDetStat(1),popBkgProb,'r+','linewidth',2,'MarkerFaceColor','r','MarkerSize',20)
    set(gca,'yscale','log');grid;
    ylim([1e-3 1])
    xlabel('population detection statistic')
    ylabel('cumulative probability')
    % ---- Save plot.
    plotName = ['/popsig_' popMethod];
    xsavefigure(plotName,figures_dirName,figfiles_dirName);
    xaddfiguretable(fout,plotName,figures_dirName);
    clear plotName
    
    fprintf(fout,'<br>Population detection statistic values: %f',popDetStat(1));
    fprintf(fout,'<br>Background probability of this value: %f',popBkgProb);
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Plot histograms of exclusion distances
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Creating exclusion distance histograms')
fprintf(fout,'%s\n',['<h2>  Exclusion distance </h2>']);

% ---- Record the default hrss values for the ad hoc waveforms (chirplet, wnb, etc.).
hrss = zeros(1,nWave);

% ---- Loop over waveforms
for iWave = 1:nWave

    % ---- Parse the injection parameters and find the ones that control the
    %      amplitude and central frequency. 
    parameters = tildedelimstr2numorcell(master_params{iWave});
    [ampl_idx,freq_idx] = xmakewaveform(master_types{iWave});
    % ---- KLUDGE: for inspirals reset ampl_idx to last parameter to handle the
    %      case where the mass parameters have been randomised. 
    if strcmp(master_types{iWave},'inspiral')
        ampl_idx = length(parameters);
    end
    if iscell(parameters(ampl_idx))
        ampl_param = cell2mat(parameters(ampl_idx));
        freq_param = cell2mat(parameters(freq_idx));
    else
        ampl_param = parameters(ampl_idx);
        freq_param = parameters(freq_idx);
    end

    % ---- Determine nominal injection distance in Mpc. The interpretation of
    %      the amplitude parameter depends on the waveform type, so we have to
    %      do an exhaustive listing ... 
    % ---- Assumed energy emission for ad hoc waveforms.
    refEnergy = 1e-2; %-- solar masses
    % ---- Parse waveform type.
    switch master_types{iWave}
        case {'inspiral','adi-a','adi-b','adi-c','adi-d','adi-e'}
            nominalDist(iWave)  = ampl_param;
        case {'chirplet'}
            hrss(iWave) = ampl_param; 
            % ---- Compute Egw at 1 Mpc.
            Egw_Msun = gwbenergy(1e6,ampl_param,freq_param,'elliptical');
            nominalDist(iWave) = sqrt(refEnergy/Egw_Msun);
        case {'wnb'}
            hrss(iWave) = ampl_param; 
            % ---- Compute Egw at 1 Mpc.
            Egw_Msun = gwbenergy(1e6,ampl_param,freq_param,'isotropic');
            nominalDist(iWave) = sqrt(refEnergy/Egw_Msun);
        case {'ds2p'}
            % ---- This is a special case because the ds2p amplitude parameter
            %      is the peak amplitude not the hrss (should update for O4!).
            tau = parameters(2);
            if iscell(tau)
                tau = cell2mat(tau);
            end
            hrss(iWave) = (0.55*tau)^0.5 * ampl_param;
            % ---- Compute Egw at 1 Mpc.
            Egw_Msun = gwbenergy(1e6,hrss(iWave),freq_param,'elliptical');
            nominalDist(iWave) = sqrt(refEnergy/Egw_Msun);
        otherwise
            error(['Unrecognized waveform type: ' master_types{iWave} ...
                '. Only inspiral, adi, chirplet, ds2p, and wnb are currently supported'])
    end
 
    % ---- Plot the figure.
    figure; hold on
    set(gca,'FontSize',20)
    % ---- Use log-spaced bins for the histogram.
    distBinEdges=[-inf logspace(log10(0.02),log10(400),1000) inf];
    n(iWave,:)=histc(nominalDist(iWave)./UL90p(:,iWave), distBinEdges);
    medianExclDist(iWave) = nanmedian(nominalDist(iWave)./UL90p(:,iWave));
    maxExclDist(iWave) = max(nominalDist(iWave)./UL90p(:,iWave));
    % ---- Start plotting.
    stairs(distBinEdges,cumsum(n(iWave,:)))
    X=xlim;
    xlim([0.01 X(2)*1.1]);
    set(gca,'xscale','log');grid;
    title(master_sets{iWave});
    xlabel('exclusion distance (Mpc)')
    ylabel('number of GRBs')
    % ---- Save plot.
    plotName{iWave} = ['/dist_' master_sets{iWave}];
    xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);

end % -- loop over waveforms

% ---- Put all plots into a table.
xaddfiguretable(fout,plotName,figures_dirName);

% ---- Combine all distance exclusions into one plot.
figure; hold on
set(gca,'FontSize',20)
colorSet = {'b','r--','g','k--','m','c','b--','r','g--','k','m--','c--','b:', ...
    'r:','g:','k:','m:','c:','b-.','-.','g-.','k-.','m-.','c-.'};
for iWave=1:nWave
    stairs(distBinEdges,cumsum(n(iWave,:)),colorSet{iWave},'linewidth',2*(nWave+1-iWave))
end
legend(master_sets,'location','northwest');
X=xlim;
xlim([0.01 X(2)*1.1]);
set(gca,'xscale','log');grid;
xlabel('exclusion distance (Mpc)')
ylabel('number of GRBs')
plotName = '/dist_combined';
xsavefigure(plotName,figures_dirName,figfiles_dirName);
xaddfiguretable(fout,plotName,figures_dirName);

% ---- Also record hrss90% limits - again, only meaningful for the ad hoc waveforms (chirplets, etc).
hrss90 = UL90p .* repmat(hrss,nGRBs,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Write table of median exclusion distances.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Write table header.
fprintf(fout,'<table border=1><tr><th>');
for iWave = 1:nWave
    fprintf(fout,'<th>%s',master_sets{iWave});
end
fprintf(fout,'<tr><th> Median Exclusion (Mpc)');
fprintf(fout,'<td>%.1f',medianExclDist);
fprintf(fout,'<tr><th> Max Exclusion (Mpc)');
fprintf(fout,'<td>%.1f',maxExclDist);
fprintf(fout,'</table>\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Write latex result table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Can make the GRB table only for burst results. Therefore skip if
%      inputData is supplied (main use for this input is for CBC results).
if nargin <= 5
    
    disp('Creating latex result table')
    
    % ---- Open result table.
    tableName = ['batchSummary_' user_tag '.tex'];
    ftable = fopen(tableName,'w');
    
    % ---- Open grb info.
    tableName = ['batchSummary_grbinfo_' user_tag '.tex'];
    fgrb = fopen(tableName,'w');
    
    % ---- Open reduced for publication table.
    tableName = ['batchSummary_paper_' user_tag '.tex'];
    fpaper = fopen(tableName,'w');
    
    % ---- Write out GRBs sorted by time.
    GRBgpsTime = [];
    for iGRB = 1:length(analysis)
        GRBgpsTime = [GRBgpsTime analysis{iGRB}.gpsCenterTime];
    end
    [tmp IsortedGRB]=sort(GRBgpsTime);
    for iGRB = IsortedGRB
        % ---- Convert GPS time to UTC time.
        [status clockTime] = system(['tconvert -f "%T" ' ...
            num2str(analysis{iGRB}.gpsCenterTime)]);
        % ---- Compute RA, Dec from Earth-fixed coordinates and time.
        [ra dec] = earthtoradec(analysis{iGRB}.skyPositions(1,2), ...
            analysis{iGRB}.skyPositions(1,1), ...
            analysis{iGRB}.skyPositionsTimes(1,1));
        [raStr, decStr] = radec2string(ra, dec);
        % ---- Print line of info on given GRB.
        fprintf(ftable,'%s & %s & %s & %s & %s & [%d,%d] & %.3g', ...
            grbName{iGRB}, ...
            clockTime(1:end-2), ...
            raStr, decStr, ...
            [analysis{iGRB}.detectorList{:}], ...
            analysis{iGRB}.onSourceBeginOffset, ...
            analysis{iGRB}.onSourceEndOffset, ...
            pOnSource(iGRB));
        fprintf(ftable,' & %3.1f', ...
            nominalDist./UL90p(iGRB,:));
        fprintf(ftable,' \\\\\n');
        
        % ---- KLUDGE: hard-code the list of possible detectors.
        if iGRB==1
            warning(['Using hard-coded list of detectors:']);
        end
        detList = {'H1','L1','V1'};
        for iDet = 1:length(detList)
            % ---- Compute antenna response of each detector only if used
            %      in the analysis.
            iDetInList = find(strcmp(detList{iDet},analysis{iGRB}.detectorList));
            if not(isempty(iDetInList))
                antennaFactor{iDet} = sprintf('%.2f',sqrt( ...
                    analysis{iGRB}.FpList(iDetInList).^2 + ...
                    analysis{iGRB}.FcList(iDetInList).^2));
            else
                % ---- Otherwise replace antenna response with a dash.
                antennaFactor{iDet} = '--';
            end
        end
        % ---- Write TeX-formatted table.
        fprintf(fgrb,'%s & %s & %s & %s & %s & %s & %s',...
            grbName{iGRB}(4:end), ...
            clockTime(1:end-2), ...
            raStr, decStr, ...
            antennaFactor{1}, antennaFactor{2}, antennaFactor{3});
        % ---- Don't add trailing \\ so that the table can be merged with
        %      additional information not present here (T90, redshift,
        %      satellite providing trigger, ...).
        fprintf(fgrb,' \n');
        % ---- Paper version of the table.
        %      Anotations to the GRB name:
        %        * for any GRB which has a -120 second on-source window
        %        + for any GRB which uses the T90 for the positive value of
        %          the onsource window
        if analysis{iGRB}.onSourceBeginOffset == -120
            annoBegin = '$^*$';
        else
            annoBegin = '';
        end
        if analysis{iGRB}.onSourceEndOffset > 60
            annoEnd = '$^\dagger$';
        else
            annoEnd = '';
        end
        
        % ---- Print out all waveform limits into .tex table. 
        waveformList = [];
        for iWave = 1:nWave
            waveformList = [waveformList iWave];
        end
        % ---- Print out only CSG150, CSG300, ADI-C, NSNS and NSBH - this is the
        %      usual choice for GRB searches.
        % waveformList = [];
        % for iWave = 1:nWave
        %     switch master_types{iWave}
        %         case {'inspiral','adi-c','adi-a'}
        %             waveformList = [waveformList iWave];
        %         case {'chirplet'}
        %             parameters = tildedelimstr2numorcell(master_params{iWave});
        %             if iscell(parameters)
        %                 f0 = cell2mat(parameters(3));
        %             else
        %                 f0 = parameters(3);
        %             end
        %             if f0 == 150 || f0 == 300
        %                 waveformList = [waveformList iWave];
        %             end
        %     end
        % end
        fprintf(fpaper,'%s & %s & %s & %s & %s ',...
            [grbName{iGRB}], ...
            clockTime(1:end-2), ...
            raStr, decStr, ...
            [analysis{iGRB}.detectorList{:} annoBegin annoEnd]);
        fprintf(fpaper,' & %3.1f', ...
            nominalDist(waveformList)./UL90p(iGRB,waveformList));
        fprintf(fpaper,' \\\\\n');
        
    end
    
    % ---- Close the latex table files.
    fclose(ftable);
    fclose(fgrb);
    fclose(fpaper);
    
else
    
    warning('Skipping creation of latex result table')
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Perform population exclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- MAGIC numbers:
sensGain = 3;       %-- Expected gain in sensitivity
nGRBgain = 6;       %-- Expected gain in number of GRBs, must be integer
cosmoMode = 'flat'; %-- Cosmology used

disp('Creating population exclusion plot')
% ---- Compute volume efficiency for a bunch of radii.
for iWave = 1:nWave
    % ---- Choose the volume radii on which to perform the exclusion.
    disp(['Processing injection set ' num2str(iWave) ' of ' num2str(nWave) ...
        ', with nominal distance ' num2str(nominalDist(iWave)) ' Mpc.'])
    volumeRadius = nominalDist(iWave)*10.^[-1:0.033:3.6];
    % ---- Preallocate output variables.
    popEff(iWave,:,:) = zeros(length(volumeRadius)+1,nGRBs+2);
    %popEffStatGain(iWave,:,:) = zeros(length(volumeRadius)+1,nGRBs*nGRBgain+2);
    popEffFullGain(iWave,:,:) = zeros(length(volumeRadius)+1,nGRBs*nGRBgain+2);
    for iRadius = 1:length(volumeRadius)
        for iGRB = 1:nGRBs
            try
                % ---- Compute volume-marginalized efficiency curve.
                volEff(iGRB,iWave,iRadius) = volumeefficiency( ...
                    squeeze(injScale(iGRB,iWave,:)),...
                    squeeze(effCurve(iGRB,iWave,:)),...
                    nominalDist(iWave),volumeRadius(iRadius),cosmoMode);
                volEffSensGain(iGRB,iWave,iRadius) = volumeefficiency( ...
                    squeeze(injScale(iGRB,iWave,:)),...
                    squeeze(effCurve(iGRB,iWave,:)),...
                    sensGain*nominalDist(iWave),volumeRadius(iRadius),cosmoMode);
            catch
                errorStruct = lasterror;
                errorStruct.message
                disp(['iGRB: ' num2str(iGRB) ', iWave: ' num2str(iWave) ...
                    ', iRadius: ' num2str(iRadius) ', nominalDist: ' ...
                    num2str(nominalDist(iWave)) ', volumeRadius: ' ...
                    num2str(volumeRadius(iRadius))])
                % ---- In this case set efficiencies to zero (conservative).
                volEff(iGRB,iWave,iRadius) = 0;
                volEffSensGain(iGRB,iWave,iRadius) = 0;
            end
        end % -- loop over GRBs
        % ---- Compute population exclusion at given radii and fraction of
        %      analyzed GRBs with significant GW emission.
        popEff(iWave,iRadius,1:nGRBs+1) = 1 - ...
            poly(diag(-(1-volEff(:,iWave,iRadius)))) ./ poly(diag(-ones(nGRBs,1)));
        % popEffStatGain(iWave,iRadius,1:nGRBgain*nGRBs+1) = 1 - ...
        %     poly(diag(repmat(-(1-volEff(:,iWave,iRadius)),[nGRBgain 1 1]))) ...
        %     ./ poly(diag(-ones(nGRBgain*nGRBs,1)));
        popEffFullGain(iWave,iRadius,1:nGRBgain*nGRBs+1) = 1 - ...
            poly(diag(repmat(-(1-volEffSensGain(:,iWave,iRadius)),[nGRBgain 1 1]))) ...
            ./ poly(diag(-ones(nGRBgain*nGRBs,1)));
        
        % ---- Marginalize over binomial distribution of nearby GRBs in the
        %      analyzed sample.
        popFraction = [0 logspace(-3,0,300)];
        for iFrac = 1:length(popFraction)
            popEffBino(iWave,iRadius,iFrac) = sum( ...
                binopdf(0:nGRBs,nGRBs,popFraction(iFrac))' .* ...
                squeeze(popEff(iWave,iRadius,1:nGRBs+1)));
            % popEffBinoStatGain(iWave,iRadius,iFrac) = sum( ...
            %     binopdf(0:nGRBgain*nGRBs,nGRBgain*nGRBs,popFraction(iFrac))' .* ...
            %     squeeze(popEffStatGain(iWave,iRadius,1:nGRBs*nGRBgain+1)));
            popEffBinoFullGain(iWave,iRadius,iFrac) = sum( ...
                binopdf(0:nGRBgain*nGRBs,nGRBgain*nGRBs,popFraction(iFrac))' .* ...
                squeeze(popEffFullGain(iWave,iRadius,1:nGRBs*nGRBgain+1)));
        end
        
        % ---- Find 90% exclusion line in the radii vs fraction plane.
        fractionBino = popFraction(squeeze((min(find(popEffBino(iWave,iRadius,:)>=0.9))-1)));
        % fractionBinoStatGain = popFraction(squeeze((min( ...
        %     find(popEffBinoStatGain(iWave,iRadius,:)>=0.9))-1)));
        fractionBinoFullGain = popFraction(squeeze((min( ...
            find(popEffBinoFullGain(iWave,iRadius,:)>=0.9))-1)));
        if isempty(fractionBino)
            fractionBino = NaN;
        end
        % if isempty(fractionBinoStatGain)
        %     fractionBinoStatGain = nan;
        % end
        if isempty(fractionBinoFullGain)
            fractionBinoFullGain = NaN;
        end
        distribExclBino(iWave,iRadius) = fractionBino;
        % distribExclBinoStatGain(iWave,iRadius) = fractionBinoStatGain;
        distribExclBinoFullGain(iWave,iRadius) = fractionBinoFullGain;
    end
    % ---- Plot the uniform-in-volume exclusion (both with and without
    %      binomial marginalization) in terms of redshift along with the
    %      observed GRB redshift distribution.
    if not(isempty(zFile))
        grbZ = load(zFile);
        grbZ = sort(grbZ);
    else
        grbZ = [];
    end
    figure; hold on
    set(gca,'FontSize',20)
    plot(dist2z(volumeRadius,cosmoMode),distribExclBino(iWave,:),'k-','linewidth',2);
    % plot(dist2z(volumeRadius,cosmoMode),distribExclBinoStatGain(iWave,:),'b--','linewidth',2);
    plot(dist2z(volumeRadius,cosmoMode),distribExclBinoFullGain(iWave,:),'b:','linewidth',2);
    stairs([1e-10 grbZ'],0:1/length(grbZ):1,'r','linewidth',2)
    % set axis
    set(gca,'xscale','log');  set(gca,'yscale','log');
    xlim([1e-3 10])
    grid;
    title([master_sets{iWave}]);
    legend('Exclusion',...
        ['Adv det, sens x ' num2str(sensGain) ', nGRBs x ' num2str(nGRBgain)],...
        'measured','location','southeast')
    % ['Adv det, nGRBs x ' num2str(nGRBgain)],...
    ylabel('Cumulative distribution')
    xlabel('redshift')
    % ---- Save plot.
    plotNameZ{iWave} = ['/zExcl_' master_types{iWave} '_' master_params{iWave}];
    xsavefigure(plotNameZ{iWave},figures_dirName,figfiles_dirName);
    
end

% ---- Add figure to web page.
fprintf(fout,'%s\n',['<h2>  Z distribution exclusion </h2>']);
xaddfiguretable(fout,plotNameZ,figures_dirName);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Write table with xpipeline links & results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',['<h2>  Table of links </h2>']);

% ---- Write table header.
fprintf(fout,['<table border=1><tr><th><th><th colspan="%d">exclusion ' ...
    'in terms of injection scale\n<th colspan="%d">detection sensitivity ' ...
    'in terms of injection scale\n'],nWave,nWave);
fprintf(fout,'<tr><th>GRB<th>p-value');
for iWave = 1:nWave
    fprintf(fout,'<th>%s',master_sets{iWave});
end
for iWave = 1:nWave
    fprintf(fout,'<th>%s',master_sets{iWave});
end
for iGRB = 1:nGRBs
    % ---- Fill table of links to per-GRB pages, p-values and ULs.
    %      Find root of the output file name; will be used to put links to
    %      the result web pages.
    %      Only fill in links if openFileList is specified.
    if isempty(openFileList)
        fprintf(fout,['<tr><td> %s <td>%.3g' ],grbName{iGRB},pOnSource(iGRB));
        fprintf(fout,'<td> %.3g',UL90p(iGRB,:));
        fprintf(fout,'<td> %.3g',detSensCL99orig(iGRB,:));
        fprintf(fout,'\n');
    else
        rootOpenResName = openFileCell{iGRB}(1:end-4);
        rootULResName = ulFileCell{iGRB}(1:end-4);
        fprintf(fout,['<tr><td> %s <A HREF="%s.shtml">OPEN-BOX</A> <A HREF="%s'  ...
            '.shtml">UL-BOX</A><td>%.3g' ],grbName{iGRB},rootOpenResName, ...
            rootULResName,pOnSource(iGRB));
        fprintf(fout,'<td> %.3g',UL90p(iGRB,:));
        fprintf(fout,'<td> %.3g',detSensCL99orig(iGRB,:));
        fprintf(fout,'\n');
    end
end
fprintf(fout,'</table>\n');

% ---- Close web page.
fclose(fout);

% ---- Save output to files. 
% ---- Copy some data into names used in older versions of this code so that
%      follow-on codes will work. 
parametersCell = master_params;
nameCell       = master_types;
cFreq          = ulRes.cFreq;
outputFileName = ['simpleSummary_' user_tag '.mat'];
save(outputFileName,'UL90p','onSource','parametersCell','nameCell',...
    'master_params','master_types',...
    'pOnSource','nGRBs','grbName','user_tag','nominalDist',...
    'detSensCL99','detSensCL99orig','injScale','effCurve');
% ---- Save output for upper limit energy calculations to file.
EnergyOutputFileName = ['energy_ULs_' user_tag '.mat'];
save(EnergyOutputFileName,'UL90p','hrss','parametersCell','nameCell',...
    'master_params','master_types',...
    'pOnSource','nGRBs','grbName','user_tag','nominalDist',...
    'hrss90','analysis','cFreq');


% ---- Done.
return

