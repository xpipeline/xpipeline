function [] = xwriteulplots(fout,analysis,cFreq,UL90p,hrss,...
    figures_dirName,figfiles_dirName)
% XWRITEULPLOTS - Plot hrss upper limits with detector noise spectra.
% 
% xwriteulplots overlays hrss upper limits on plots of the IFO amplitude
% spectra.  It is a helper function for xmakegrbwebpage.
%
% Usage:
%
%    xwriteulplots(fout,analysis,cFreq,UL90p,hrss,...
%        figures_dirName,figfiles_dirName)
%
%   fout             Pointer to open and writable webpage file  
%   analysis         Structure created by xmakegrbwebpage
%   cFreq            Vector of central frequencies of our injections
%   UL90p            Vector of injection scales at which 90% detection 
%                    efficiency is achieved for our injections
%   hrss             Vector of intrinsic hrss amplitude of injected waveforms.
%   figures_dirName  String, path of output dir for figures where figures will
%                    be saved
%   figfiles_dirName String, path of output dir for figures where figfiles will
%                    be saved
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(7, 7, nargin));

% ---- Plot detector spectra with hrss ULs superimposed.
open([figfiles_dirName 'detectors_spectra.fig']); hold on;

% ---- retrieve legend from figure
%      note that the location needs to be set again 
legendStr = get(legend,'String');

plot(cFreq(:),UL90p(:).*hrss(:),'ko','markerfacecolor','k');
title('Upper Limits vs. Frequency')

legendStr{length(legendStr)+1} = 'upper limit';

legend(legendStr ,'Location','SouthEast')
hold off
% ---- Save figure.
xsavefigure('FspectraWithInj',figures_dirName,figfiles_dirName);

% ---- Only make this plot if we have analysis.eNullIndex
if (analysis.eNullIndex)
     % ---- Plot detector "null stream spectra" with hrss ULs superimposed.
     open([figfiles_dirName 'Incspectra.fig']); hold on;
     plot(cFreq(:),UL90p(:).*hrss(:),'ko','markerfacecolor','k');
     title('Upper Limits and Null Stream Sensitivity')
     hold off
    % ---- Save figure.
    xsavefigure('IncspectraWithInj',figures_dirName,figfiles_dirName);
end

% ---- Add these plots to the web page.
if (analysis.eNullIndex)
    xaddfiguretable(fout,{'FspectraWithInj','IncspectraWithInj'},...
        figures_dirName,{analysis.captions.FspectraWithInj,...
        analysis.captions.FspectraWithInj});
else
    xaddfiguretable(fout,{'FspectraWithInj'},figures_dirName,...
        analysis.captions.FspectraWithInj);
end

