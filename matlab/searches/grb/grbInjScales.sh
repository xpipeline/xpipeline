#!/bin/bash

onsourceparam=`cat grb_on_source.dag | grep VARS | gawk -F '"' '{ print $2}'`
xdetection $onsourceparam

grbname=`cat grbname.txt`
grbparam=`cat grb.param`
cd output
rm -f on*merged.mat
xmergegrbresults on_source
${XPIPELINE_ROOT}/grb/makeAutoFiles2.py -p ../grb.ini
${XPIPELINE_ROOT}/post/estimateInjScale.sh $grbname fastauto.txt
cat ${grbname}InjScales.txt >> ../grb.ini
cd ..
${XPIPELINE_ROOT}/grb/grb.py $grbparam



