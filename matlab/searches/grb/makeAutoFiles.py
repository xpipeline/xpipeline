#!/usr/bin/env python


"""
Script that creates text files needed by makeWebPage.m, it should be
executed in the output subdirectory of the directory where grb.py and
makeExtract.py where run.
$Id$
"""
# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt
import configparser

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  grb.sh [options]
  -p, --params-file <file>    parameters (.ini) file [REQUIRED]
  -g, --grb-time <gps>        GRB trigger time (GPS seconds) [REQUIRED]
  -r, --right-ascension <ra>  right ascension of GRB (degrees, from 0 to 360) [REQUIRED]
  -d, --declination <decl>    declination of GRB (degrees, from -90 to 90) [REQUIRED]
  -i, --detector <ifo>        add detector to the network [REQUIRED]
  -h, --help                  display this message and exit
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
trigger_time = None
ra = None
decl = None
detector = []

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:g:r:d:i:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "grb-time=",
   "right-ascension=",
   "declination=",
   "detector="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a      
    elif o in ("-g", "--grb-time"):
        trigger_time = float(a)       
    elif o in ("-r", "--right-ascension"):
        ra = float(a)
#    ra = ra * (math.pi) / 180.0  # conversion to earth-fixed, radians now done in xdetection
    elif o in ("-d", "--declination"):
        decl = float(a)
#    decl = (90.0 - decl) * (math.pi) / 180.0  # conversion to earth-fixed, radians now done in xdetection
    elif o in ("-i", "--detector"):
        detector.append(a) 
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not trigger_time:
    print("No GRB trigger time specified.", file=sys.stderr)
    print("Use --grb-time to specify it.", file=sys.stderr)
    sys.exit(1)
if not ra:
    print("No right ascension specified.", file=sys.stderr)
    print("Use --right-ascension to specify it.", file=sys.stderr)
    sys.exit(1)
if not decl:
    print("No declination specified.", file=sys.stderr)
    print("Use --declination to specify it.", file=sys.stderr)
    sys.exit(1)
if not detector:
    print("No detectors specified.", file=sys.stderr)
    print("Use --detector to specify each detector in the network.", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("WARNING: The on- and off-source DAGs write to the same log file.  This should be fixed.", file=sys.stdout)
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#              X-GRB Search Pipeline               #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print("    GRB trigger time:", trigger_time, file=sys.stdout)   
print(" GRB right ascension:", ra, file=sys.stdout)
print("     GRB declination:", decl, file=sys.stdout)
print("    detector network:", detector, file=sys.stdout) 
print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Preparatory.
# -------------------------------------------------------------------------

# ---- Record the current working directory in a string.
cwdstr = os.getcwd( )
# print >> sys.stdout, "   current directory:", cwdstr   
# print >> sys.stdout

# -------------------------------------------------------------------------
#      Read configuration file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)

# ---- Create configuration-file-parser object
cp = configparser.ConfigParser()
cp.read(params_file)

# ---- Read [injection] parameters. 
waveform_set = cp.options('waveforms')
injectionScalesList = cp.get('injection','injectionScales')
injectionScales = injectionScalesList.split(',')

mdcList = cp.get('mdc','mdc_sets')
mdc_set = mdcList.split(',')
waveform_set.extend(mdc_set)
# ---- Read how distribute results on node and write to file
jobNodeFileOffSource = cp.get('output','jobNodeFileOffSource')
jobNodeFileOnSource = cp.get('output','jobNodeFileOnSource')
jobNodeFileSimulationPrefix = cp.get('output','jobNodeFileSimulationPrefix')

jobNodeFileOnSource =subprocess.getoutput('echo ' + jobNodeFileOnSource + '| cut -d . -f 1')
jobNodeFileOffSource =subprocess.getoutput('echo ' + jobNodeFileOffSource + '| cut -d .  -f 1') 

(status,offsource) = subprocess.getstatusoutput('ls | grep .mat | grep ' + jobNodeFileOffSource)    
(status,onsource) = subprocess.getstatusoutput('ls | grep .mat | grep ' + jobNodeFileOnSource)

# ---- Loop over waveforms
for set in waveform_set :
    autoFile = open('auto_' + set + '.txt','w')
    fastFile = open('fastauto_' + set + '.txt','w')
    autoFile.write(offsource + "\n")
    fastFile.write(onsource + "\n")
    (status,out) = subprocess.getstatusoutput('ls | grep .mat | grep ' + jobNodeFileSimulationPrefix  +  '|grep ' + set+ '_ ')
    autoFile.write(subprocess.getoutput('ls | grep .mat | grep ' + jobNodeFileSimulationPrefix + '|grep ' + set+ '_ '  +'|wc -l') + "\n")
    fastFile.write(subprocess.getoutput('ls | grep .mat | grep ' + jobNodeFileSimulationPrefix  + '|grep ' + set+ '_ ' +'|wc -l') + "\n")
    autoFile.write(out + "\n")
    fastFile.write(out + "\n")
    autoFile.write(onsource + "\n")
    fastFile.write(onsource + "\n")
    autoFile.close()
    fastFile.close()

# ----
autoFile = open('auto.txt','w')
fastFile = open('fastauto.txt','w')
autoFile.write(offsource + "\n")
autoFile.write(onsource + "\n")
autoFile.write("%d"%(len(waveform_set)) + "\n")
fastFile.write(onsource + "\n")
fastFile.write(onsource + "\n")
fastFile.write("%d"%(len(waveform_set)) + "\n")
for set in waveform_set :
    autoFile.write('auto_' + set + '.txt\n')
    fastFile.write('fastauto_' + set + '.txt\n')
for set in waveform_set :
    autoFile.write('../input/injection_' + set +'.txt\n')
    fastFile.write('../input/injection_' + set +'.txt\n')
autoFile.close()
fastFile.close()
