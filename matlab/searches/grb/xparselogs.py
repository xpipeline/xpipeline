#!/usr/bin/env python
"""
XPARSELOGS - parse X-Pipeline log files and produce a summary report

$Id$

The X-Pipeline log/ directory is scanned for .out and .err files for 
jobs that are no longer running. If the .err file is empty then some 
summary information is retrieved from the corresponding .out file and 
both files are then deleted. The sumary info is written to 
logs/xparselogs.summary.

This script can be run on both first-stage (trigger generation) and 
second-stage (post-processing) directories.

"""
__author__  = 'Patrick Sutton <patrick.sutton@ligo.org>'
__date__    = '$Date$'
__version__ = '$Revision$'

import os, sys, getopt
import configparser


# ---- Function usage.
def usage():
    msg = """\
Usage: 
    xparselogs.py [options]
    -d, --dir <path>        [REQUIRED] Path to directory to be checked (absolute
                            or relative).
    -h, --help              Display this message and exit.

  e.g.,
    xparselogs.py -d /home/gareth.jones/S5/GRB070508 
"""
    print(msg, file=sys.stderr)


def xparselogs(grb_dir):

    # -----------------------------------------------------------------------------
    #    Move to target directory.
    # -----------------------------------------------------------------------------

    # ---- Record starting directory.
    start_dir = os.getcwd()

    # ---- Move to target logs directory.
    os.chdir(grb_dir + 'logs/')

    # -----------------------------------------------------------------------------
    #    Check to see if user is also owner of the target files. 
    # -----------------------------------------------------------------------------

    # ---- Determine user running this script.
    user_name = os.popen('whoami').read().rstrip() 
    # ---- Determine owner of target files, from the home directory name.
    target_user = None
    grb_dir_full = os.getcwd() #-- use in case grb_dir is a relative path
    if grb_dir_full.startswith('/home'):
        target_user = grb_dir_full.split('/')[2]
    if user_name == target_user:
        fix_files = True
    else:
        fix_files = False
        print() 
        print("Script user (" + user_name + ") differs from owner of target files (" + target_user + ").")
        print("We will scan the log files but cannot remove them.")

    # -----------------------------------------------------------------------------
    #      Get list of currently running jobs.
    # -----------------------------------------------------------------------------

    # ---- Get list of currently running jobs. We won't remove logs
    #      if the job that created it is still running.
    print("Getting list of running condor jobs.")
    os.system("condor_q -format \"%d.\" ClusterId -format \"%d\n\" ProcId > .job_list.txt")
    j = open(".job_list.txt","r")
    running_jobs = j.read().splitlines()
    j.close()
    print("Found", len(running_jobs), "running condor jobs.")

    # -----------------------------------------------------------------------------
    #    Get list condor IDs for finished jobs.
    # -----------------------------------------------------------------------------

    # ---- Get lists of xsearch-*.out and xmerge-*.out files. Use these to get list of condor job numbers.
    print() 
    print("Scanning logs/ directory. This may take a minute ...")
    xsearch_IDs = os.popen('ls xsearch*.out | sed -e \'s/xsearch-//g\' -e \'s/-0.out//g\' ').read().splitlines()
    print("Found", len(xsearch_IDs), " xsearch logs.")
    xmerge_IDs = os.popen('ls xmerge*.out | sed -e \'s/xmerge-//g\' -e \'s/-0.out//g\' ').read().splitlines()
    print("Found", len(xmerge_IDs), " xmerge logs.")
    xmakegrbwebpage_IDs = os.popen('ls xmakegrbwebpage*.out | sed -e \'s/xmakegrbwebpage-//g\' -e \'s/-0.out//g\' ').read().splitlines()
    print("Found", len(xmakegrbwebpage_IDs), " xmakegrbwebpage logs.")
    xtunegrbwebpage_IDs = os.popen('ls xtunegrbwebpage*.out | sed -e \'s/xtunegrbwebpage-//g\' -e \'s/-0.out//g\' ').read().splitlines()
    print("Found", len(xtunegrbwebpage_IDs), " xtunegrbwebpage logs.")

    # ---- Drop and files for which the corresponding job is still running.
    print() 
    print("Checking log file list against currently running jobs ...")
    for job in running_jobs:
        if job in xsearch_IDs :
            xsearch_IDs.remove(job)
        elif job in xmerge_IDs :
            xmerge_IDs.remove(job)
        elif job in xmakegrbwebpage_IDs :
            xmakegrbwebpage_IDs.remove(job)
        elif job in xtunegrbwebpage_IDs :
            xtunegrbwebpage_IDs.remove(job)
    print("Found", len(xsearch_IDs), " logs for completed xsearch jobs.")
    print("Found", len(xmerge_IDs), " logs for completed xmerge jobs.")
    print("Found", len(xmakegrbwebpage_IDs), " logs for completed xmakegrbwebpage jobs.")
    print("Found", len(xtunegrbwebpage_IDs), " logs for completed xtunegrbwebpage jobs.")

    # -----------------------------------------------------------------------------
    #    Record and remove log files with no errors.
    # -----------------------------------------------------------------------------

    # ---- Find empty .err files.
    summary_file = open('xparselogs.summary','a')
    for condor_ID in xsearch_IDs :
        if os.path.getsize('xsearch-' + condor_ID + '-0.err') == 0 :
            # ---- Job completed with no reported errors. Extract data and delete log files.
            log = open('xsearch-' + condor_ID + '-0.out','r')
            data = log.readlines()
            log.close()
            msg = data[1].rstrip().split()[3:] 
            if len(data) > 3 :
                msg.append(data[-4].rstrip().split()[0])
            else :
                msg.append('-1')
            msg.insert(0,condor_ID)
            msg = ' '.join(msg)
            summary_file.write(msg + '\n')
            os.system('rm xsearch-' + condor_ID + '-0.err xsearch-' + condor_ID + '-0.out')
    for condor_ID in xmerge_IDs :
        if os.path.getsize('xmerge-' + condor_ID + '-0.err') == 0 :
            # ---- Job completed with no reported errors. Extract data and delete log files.
            log = open('xmerge-' + condor_ID + '-0.out','r')
            data = log.readlines()
            log.close()
            msg = data[1].rstrip().split()[3:] 
            msg.insert(0,condor_ID)
            msg = ' '.join(msg)
            summary_file.write(msg + '\n')
            os.system('rm xmerge-' + condor_ID + '-0.err xmerge-' + condor_ID + '-0.out')
    for condor_ID in xmakegrbwebpage_IDs :
        if os.path.getsize('xmakegrbwebpage-' + condor_ID + '-0.err') == 0 :
            # ---- Job completed with no reported errors. Extract data and delete log files.
            log = open('xmakegrbwebpage-' + condor_ID + '-0.out','r')
            data = log.readlines()
            log.close()
            msg = data[1].rstrip().split()[3:] 
            msg.insert(0,condor_ID)
            msg = ' '.join(msg)
            summary_file.write(msg + '\n')
            os.system('rm xmakegrbwebpage-' + condor_ID + '-0.err xmakegrbwebpage-' + condor_ID + '-0.out')
    for condor_ID in xtunegrbwebpage_IDs :
        if os.path.getsize('xtunegrbwebpage-' + condor_ID + '-0.err') == 0 :
            # ---- Job completed with no reported errors. Extract data and delete log files.
            log = open('xtunegrbwebpage-' + condor_ID + '-0.out','r')
            data = log.readlines()
            log.close()
            msg = data[1].rstrip().split()[3:] 
            msg.insert(0,condor_ID)
            msg = ' '.join(msg)
            summary_file.write(msg + '\n')
            os.system('rm xtunegrbwebpage-' + condor_ID + '-0.err xtunegrbwebpage-' + condor_ID + '-0.out')
    summary_file.close()

    # -----------------------------------------------------------------------------
    #    Return to starting directory.
    # -----------------------------------------------------------------------------

    os.chdir(start_dir)


if __name__ == '__main__':
    # ---- This function is being executed directly as a script (as opposed to 
    #      being imported by another code). Parse the command line arguments 
    #      then run the xfixemptymatfiles() function.

    # -------------------------------------------------------------------------
    #      Parse the command line options.
    # -------------------------------------------------------------------------

    # ---- Initialize variables.
    grb_dir = None

    # ---- Syntax of options, as required by the getopt command.
    # ---- Short form.
    shortop = "hd:"
    # ---- Long form.
    longop = [
        "help",
        "dir="
        ]

    # ---- Get command-line arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    # ---- Parse command-line arguments.  Arguments are returned as strings, so 
    #      convert type as necessary.
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-d", "--dir"):
            grb_dir = a      
        else:
            print("Unknown option:", o, file=sys.stderr)
            usage()
            sys.exit(1)

    # ---- Check that all required arguments are specified, else exit.
    if not grb_dir:
        print("No directory specified.", file=sys.stderr)
        print("Use the --dir option to specify one:", file=sys.stderr)
        print() 
        usage()
        sys.exit(1)

    # ---- Force grb_dir to end with '/'.
    if not grb_dir.endswith('/'):
        grb_dir = grb_dir + '/'

    # ---- Call the xfixemptymatfiles() function.
    xparselogs(grb_dir)


