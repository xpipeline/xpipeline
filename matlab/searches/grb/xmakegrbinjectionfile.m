function xmakegrbinjectionfile(fileName,waveform,timeSegmentsString, ...
    timeOffsetString,injectionIntervalString,rightAscensionString, ...
    declinationString,seed,gridType,gridSimFile,polarisation)
% XMAKEGRBINJECTIONFILE - Make an X-Pipeline file for GRB injections.
%
%     xmakegrbinjectionfile(fileName,waveform,timeSegments,timeOffset,...
%         interval,RA,Dec,seed,gridType,simFile,psi)
%
% fileName  String.  Name of output injection file.
% waveform  String.  Comma-delimited list of waveforms to inject. An
%           exclamation '!' separates the type of waveform from its 
%           parameters.  Parameters are a tilde-delimited string. Type and
%           parameters are in xmakewaveform format (see below for
%           examples). 
% timeSegments String.  Tilde delimited list of GPS start-stop times of
%           injection periods, consecutive pairs of times are the start-stop
%           times of each segment.
% timeOffset String. Tilde delimited list of time offsets (seconds) that are
%           added to the injection time before conversion from RA,Dec to
%           earth fixed coordinates, set to non zero to obtain injections at
%           off-source times that have same earth fixed position as injection
%           at on-source time. One offset per time segment or a single offset.
% interval  String.  If positive: time between injections (seconds). 
%           If negative and integer: abs(interval) is the number
%           of injections.
% RA        String.  Right ascension of GWB source (degrees). 
% Dec       String.  Declination of GWB source (degrees).
% seed      String.  Seed value for random number generator.
% gridType  Optional string. If 'file' then [RA,Dec] values for the injections 
%           will be read from simFile, over-riding the RA,Dec arguments.
% simFile   Optional string. Name of file listing RA,Dec pairs for injections.
%           Columns: RA (deg), Dec (deg). The sky position for each injection 
%           will be drawn by selecting randomly one of these pairs.
% psi       Optional string. Polarisation of source: angle of spin axis anti-
%           clockwise from due north (degrees, from 0 to 360). or 'uniform'.  
%           Wirth this definition, a source polarisation of 0 produces a GW with 
%           polarisation angle of 0 as defined in ComputeAntennaResponse.m. If 
%           not specified then a uniform distribution is used for injections.
% 
% When interval>0 injections are made 'interval' seconds apart starting at 
% 'startTime'+'interval'/2.  When interval<0 the injections are distributed 
% randomly and uniformly over [startTime,endTime].
%
% The file is made by calling xmakegwbinjectionfile using 'uniform'
% distribution of polarization angles and random draws of the waveform type
% for each injection.
%
% For the special case where either RA or Dec cannot be converted to a number by
% str2num(), for example for input values of '' or 'NaN', the injections will be
% distributed isotropically over the sky.
%
% All of the input arguments are strings because this function needs to be
% compiled for use with the automated data analysis pipeline.
%
% % ---- Sample input arguments to the function
% fileName = 'injection_SG.txt'
% waveform = 'DFM!10~A3B5G4,DFM!10~A4B5G4'
% timeRangeString = '700000000~700000100'
% injectionIntervalString = '3.1415926'
% rightAscensionString = '4.1'
% declinationString = '-0.32'
%
% original write: Patrick J. Sutton 2007.03.09
%
% $Id: xmakegrbinjectionfile.m 5731 2020-04-21 13:10:15Z patrick.sutton@LIGO.ORG $

% ---- Check input parameters and assign defaults.
error(nargchk(8, 11, nargin));
% ---- Fill gridType with empty string if not provided as input.
if nargin < 11
    polarisation = 'uniform';
end
if nargin < 9
    gridType = '';
end

% ---- Set matlab's random seed to seed.  We must do this for every random 
%      number generator used.
seed = str2num(seed) + sum(waveform);
rand('seed',seed);

% ---- Convert string of waveform names into cell array.
waveform_cell = strread(waveform,'%s','delimiter',',');

% ---- Split waveform_cell entries in waveform type and waveform parameters.
for jType = 1:length(waveform_cell)
    [a b] = strread(waveform_cell{jType},'%s %s','delimiter','!');
    signal{jType,1}= a{1};
    signal{jType,2}= b{1};
end

% ---- Cook up injection times.
% ---- Read and shape list of segments.
timeSegments = tildedelimstr2numorcell(timeSegmentsString);
timeSegments = reshape(timeSegments,[2 length(timeSegments)/2])';
startTime = timeSegments(:,1);
endTime = timeSegments(:,2);
injectionInterval = str2num(injectionIntervalString);
% ---- Parse time offsets string.
timeOffset = tildedelimstr2numorcell(timeOffsetString);
% ---- If scalar repeat value (only the 0 value make sense I guess) for each time segment.
if length(timeOffset) == 1
    timeOffset = repmat(timeOffset,[length(startTime) 1]);
end

% ---- When the injection interval is a negative integer interpret its
%      absolute value as the number of injections to make.  Injections 
%      are distributed randomly and uniformly.
if injectionInterval < 0
    if round(injectionInterval) == injectionInterval
        % keep generating injection times over the whole span of time and rejects
        % those falling into gaps until requested number of injection is obtained
        % pad injection time by 10ms, if padded injection time overlaps one of
        % the injection segment it is considered good.
        nGoodInjs = 0;
        peakTime = nan(-injectionInterval,1);
        peakTimeOffset = nan(-injectionInterval,1);
        while nGoodInjs < -injectionInterval
            tmpPeakTime = min(startTime) + (max(endTime)-min(startTime))*rand(-injectionInterval,1);
            indices = fastcoincidence2([startTime endTime-startTime-1e-6],...
                                       [tmpPeakTime zeros(size(tmpPeakTime))]);
            nNewInjs = min(-injectionInterval-nGoodInjs,size(indices,1));
            newInjSubset = randperm(nNewInjs);
            newInjSubset = newInjSubset(1:nNewInjs);
            if nNewInjs > 0
                peakTime((nGoodInjs+1):(nGoodInjs+nNewInjs)) = tmpPeakTime(indices(newInjSubset,2));
                peakTimeOffset((nGoodInjs+1):(nGoodInjs+nNewInjs)) = timeOffset(indices(newInjSubset,1));
                nGoodInjs = nGoodInjs + nNewInjs;
            end
        end
        [peakTime order] = sort(peakTime,'ascend');
        peakTimeOffset = peakTimeOffset(order);
    else
        error(['Non-integer negative interval value provided: ' injectionIntervalString])
    end
elseif injectionInterval > 0
    % ---- Fill each segment with injection spaced by injectionInterval.
    startLag = injectionInterval*rand(length(startTime),1);
    peakTime = [];
    peakTimeOffset = [];
    for iSeg = 1:length(startTime)
        newPeakTime = (startTime(iSeg)+startLag):injectionInterval: ...
                      (endTime(iSeg)-injectionInterval+startLag);
        peakTime = [peakTime newPeakTime];
        peakTimeOffset = [peakTimeOffset timeOffset(iSeg)*ones(size(newPeakTime))];
    end
    peakTime = peakTime';
    peakTimeOffset = peakTimeOffset';
else
    error('Asked to make zero injections. This should not happen.')
end

% ---- Make the GWB injection file.
for jType = 1:length(waveform_cell)
    if strcmp(lower(signal{jType,1}),'ninja');
        GWBflag = 1;
    else
        GWBflag = 0;
    end
end

if GWBflag == 1

    % ---- We're injecting a NINJA waveform. 
    if size(signal,1) ~= 1
        error('NINJA waveform type can only be injected by itself.')
    else
        [status,result] =...
            system(['ligolw_print -d " " -t "sim_inspiralgroup:sim_inspiral:table" '...
                    '-c geocent_end_time -c geocent_end_time_ns -c longitude ' ...
                    '-c latitude -c polarization ' signal{1,2}]);
        params = str2num(result);
        % ---- Convert sky position to earthfixed coordinates.
        [phi, theta] = radectoearth(params(:,3)*180/pi,params(:,4)*180/pi, ...
            params(:,1)+params(:,2)*1e-9);
        params(:,[3 4]) = [phi, theta];
        % ---- Time-sort parameters.
        tParams = [params(:,1)+params(:,2)*1e-9 params];
        tParams = sortrows(tParams,1);
        params = tParams(:,2:end);
        % ----- Write injection log file.
        fid = fopen(fileName,'w');
        for iInjection = 1:size(params,1)
            fprintf(fid,'%d %d %e %e %e ', params(iInjection,:));
            fprintf(fid,'ninja %s\n',signal{1,2});
        end
        fclose(fid)
    end

else

    % ---- Check if this is an IPN error box ---- %
    if strcmp(gridType,'file') == 1

        % ---- Read file with simulated positions ---- %
        data = load(gridSimFile);

        if size(data,2) ~= 2
            error(['sky positions file: ' gridSimFile ' should contain 2 columns. ']);
        end

        % ---- Read contents of gridSimFile ---- %
        %      These should be (R.A.,Dec) values ---- %
        ra_trigger  = data(:,1);
        dec_trigger = data(:,2);

        simPositions = [ra_trigger,dec_trigger];

        % ---- Use X-Pipeline's built-in simulation engine.
        xmakegwbinjectionfile(fileName,peakTime,peakTimeOffset,simPositions, ...
            polarisation,signal,'skyCoordinateSystem','radec');

    else

        % ---- Xsphrad: if there is no set RA & Dec, distribute injections isotropically
        %      over the sky.
        if (isnan(str2num(rightAscensionString))) && (isnan(str2num(declinationString)))
            position = 'isotropic';
        else
            position = [str2num(rightAscensionString), str2num(declinationString)];
        end
        xmakegwbinjectionfile(fileName,peakTime,peakTimeOffset,position,polarisation,signal,'skyCoordinateSystem','radec');
    end

end

% ---- Done.
return

