#! /usr/bin/env python
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This script will plot a histogram of the time taken for the xsearch jobs for a GRB analysis
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from numpy import *
import subprocess, os
import matplotlib
from optparse import OptionParser
matplotlib.use("Agg")
import pylab

usage = """usage: %prog [options]

This script will collect the elapsed times of the trigger-generation jobs for an X-pipeline GRB search.

Required options:

--analysis-directory
--figures_dirName
--grb_name

Example command line: 

plotTriggerJobDuration.py -a /home/dhoak/grbs/S6/test/May15/GW100916_1/ -f GW100916_test_closedbox_figures -n GW100916

"""

parser = OptionParser(usage=usage)

parser.add_option("-a", "--analysis_directory", action="store", type="str",\
                      help="absolute path to directory containing trigger generation, specifically logs directory with condor job output")

parser.add_option("-f", "--figures_dirName", action="store", type="str",\
                      help="path to directory to save plots (relative to current directory)")

parser.add_option("-n", "--grb_name", action="store", type="str",\
                      help="GRB name")

(options, args) = parser.parse_args()

figures_dirName = options.figures_dirName
grb_dir = options.analysis_directory
grbName = options.grb_name

jobfile = 'job_labels.txt'

cmd = "grep -E 'seconds elapsed|Reading parameters from' " + grb_dir + "/logs/xsearch*.out > " + jobfile
try:
    os.system(cmd)
except OSError:
    print('Could not get job names!')
    print(cmd)
    sys.exit()


jf = open(jobfile)
xsearch_lines = jf.readlines()
jf.close()

os.system('rm ' + jobfile)

# Check that jobfile has an event number of lines
if mod(len(xsearch_lines),2):
    print('The grep command returned an off number of lines!')
    print(len(xsearch_lines))
    sys.exit()

job_times = zeros(len(xsearch_lines)/2)
job_labels = []

j = 0
total_time = 0.0
for line in xsearch_lines:

    # all the lines will have a "filename:" prefix from grep

    if 'Reading parameters from' in line:

        # We expect these lines to be of the form (two examples):
        # 'Reading parameters from ./input/parameters_simulation_csg150q9_21_1.txt'
        # 'Reading parameters from ./input/parameters_off_source_1.txt'

        path, txt1, txt2, name = line.split(' ',4)
        y = name.split('_')
        if len(y)==5:
            path, sim, job_type, injscale, seg = name.split('_',5)
            job_labels.append(job_type)
        else:
            path, job_type, source, seg = name.split('_',4)
            job_labels.append(job_type)

    elif 'seconds elapsed' in line:

        # We expect these lines to be of the form:
        # '1234 seconds elapsed'

        path_and_time, txt, txt = line.split(' ',3)
        string, seconds_str = path_and_time.split(':',2)
        seconds = float(seconds_str)

        total_time += seconds
        job_times[j] = seconds

        j += 1

    else:
        print('Line was not what we were expecting!')
        print(line)
        sys.exit()


jobtypes = unique(array(job_labels))
job_hist = [None]*len(jobtypes)

for j in range(len(jobtypes)):
    job_idx = [i for i, x in enumerate(job_labels) if x == jobtypes[j]]
    job_hist[j] = job_times[job_idx]

node_hours = total_time / 3600.0


matplotlib.rcParams.update({'savefig.dpi':150,})

fignum=0
fignum=fignum+1
pylab.figure(fignum)

n, bins, patches = pylab.hist(job_hist, 30, normed=0, histtype='barstacked', alpha=0.75,label=jobtypes)

n1 = sum(n,0)

pylab.legend(loc=1,fancybox=True,prop={'size':12})

#pylab.xscale('linear')
#pylab.yscale('log')
pylab.xlabel('xdetection job duration [sec]',fontsize=12)
pylab.ylabel('number of jobs',fontsize=12)
pylab.grid(True, which='both', linestyle=':')
pylab.ylim(0.7,1.1*max(n1))
pylab.title(grbName + ': ' + str(len(xsearch_lines)/2) + ' xdetection jobs, ' + '%.1f' % node_hours + ' total CPU hours',fontsize=12)
pylab.savefig(figures_dirName + '/xsearch_times.png')
pylab.close()

