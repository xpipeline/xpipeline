echo "This script takes the clustered triggers from the output_clustered/off_source_X directories and moves them to output_clustered/output so you can run a final merging of them all"
echo "Run this only after all clustering jobs have finished succesfully!"
mkdir -p output_clustered/off_source
echo "Copying offsource triggers from off_source_1"
cp output_clustered/off_source_1/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_2"
cp output_clustered/off_source_2/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_3"
cp output_clustered/off_source_3/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_4"
cp output_clustered/off_source_4/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_5"
cp output_clustered/off_source_5/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_6"
cp output_clustered/off_source_6/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_7"
cp output_clustered/off_source_7/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_8"
cp output_clustered/off_source_8/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_9"
cp output_clustered/off_source_9/*off_source*mat* output_clustered/off_source
echo "Copying offsource triggers from off_source_10"
cp output_clustered/off_source_10/*off_source*mat* output_clustered/off_source
echo "Removing merged file"
rm output_clustered/*off_source*merged*
echo "Done copying everything, you can run the final merging of all clustered offsource files"
