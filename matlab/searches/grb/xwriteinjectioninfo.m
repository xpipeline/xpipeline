function [] = xwriteinjectioninfo(fout,injSeed,iWave,nInjections, ...
    nInjectionsBy2,injIdx_tuning,injIdx_ulCalc)
% XWRITEINJECTIONINFO - write list of injections for tuning and UL to webpage.
%
% xwriteonoffsourceinfo writes lists of injections used for veto tuning and
% upper limit calculations to a webpage.  It is a helper function for 
% xmakegrbwebpage.m 
%
% Usage:
%  xwriteinjectioninfo(fout,injSeed,iWave,nInjections,nInjectionsBy2,...
%   injIdx_tuning,injIdx_ulCalc)
%
%  fout           Pointer to open and writable webpage file
%  injSeed        Integer. Random seem used to select which injections to
%                 use for veto tuning and upper limit calc
%  iWave          Integer. Number indicating which injection is being 
%                 analysed
%  nInjections    Integer. Number of injections made
%  nInjectionsBy2 Integer. Half number of injections made
%  injIdx_tuning  Vector. List of injections used to veto tuning
%  injIdx_ulCalc  Vector. List of injections used to calc upper limit
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(7, 7, nargin));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Write list of jobNumber used for tuning and ulCalc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout, '%s\n', ['We use random number generator rand with seed = '...
    num2str(injSeed) ' to choose injections ' ...
    'for veto tuning and upper limit calculation']);
fprintf(fout, '<br> \n');

% ---- injections for tuning...
fprintf(fout,'%s\n',[' <input id="injForTuning_' iWave '" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''injForTuning_' iWave ''');" />']);
fprintf(fout, ['For tuning we use ' num2str(length(injIdx_tuning)) ' injections: \n']);
fprintf(fout,'%s\n',[' <div id="div_injForTuning_' iWave '" style="display: none;"> ']);
for injIdx = 1:nInjectionsBy2
    fprintf(fout,'%s\n, ',num2str(injIdx_tuning(injIdx)));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_injForTuning
fprintf(fout, '<br> \n');

% ---- injections for ul calc...
fprintf(fout,'%s\n',[' <input id="injForULCalc_' iWave '" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''injForULCalc_ ' iWave ''');" />']);
fprintf(fout, ['For UL calculation we use ' num2str(length(injIdx_ulCalc)) ' injections: \n']);
fprintf(fout,'%s\n',[' <div id="div_injForULCalc_ ' iWave '" style="display: none;">   ']);
for injIdx = 1:(nInjections - nInjectionsBy2)
    fprintf(fout,'%s\n, ',num2str(injIdx_ulCalc(injIdx)));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_injForULCalc
fprintf(fout, '\n');

