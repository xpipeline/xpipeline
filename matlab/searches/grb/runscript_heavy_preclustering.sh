echo "This script takes the unclustered triggers from the output directory and moves them to output_clustered so you can submit the clustering DAGs"
echo "Run this only after all xdetection jobs have finished succesfully!"
echo "This script should be used for heavy duty cp operations e.g. when copying tens of thousands of files"
echo "Copying offsource triggers  with pattern off_source_0_1* to off_source_1"
find output/off_source/ -name "*off_source_0_1*" | xargs -i cp {} output_clustered/off_source_1
echo "Copying offsource triggers  with pattern off_source_0_2* to off_source_2"
find output/off_source/ -name "*off_source_0_2*" | xargs -i cp {} output_clustered/off_source_2
echo "Copying offsource triggers  with pattern off_source_0_3* to off_source_3"
find output/off_source/ -name "*off_source_0_3*" | xargs -i cp {} output_clustered/off_source_3
echo "Copying offsource triggers  with pattern off_source_0_4* to off_source_4"
find output/off_source/ -name "*off_source_0_4*" | xargs -i cp {} output_clustered/off_source_4
echo "Copying offsource triggers  with pattern off_source_0_5* to off_source_5"
find output/off_source/ -name "*off_source_0_5*" | xargs -i cp {} output_clustered/off_source_5
echo "Copying offsource triggers  with pattern off_source_0_6* to off_source_6"
find output/off_source/ -name "*off_source_0_6*" | xargs -i cp {} output_clustered/off_source_6
echo "Copying offsource triggers  with pattern off_source_0_7* to off_source_7"
find output/off_source/ -name "*off_source_0_7*" | xargs -i cp {} output_clustered/off_source_7
echo "Copying offsource triggers  with pattern off_source_0_8* to off_source_8"
find output/off_source/ -name "*off_source_0_8*" | xargs -i cp {} output_clustered/off_source_8
echo "Copying offsource triggers  with pattern off_source_0_9* to off_source_9"
find output/off_source/ -name "*off_source_0_9*" | xargs -i cp {} output_clustered/off_source_9
echo "Copying offsource triggers  with pattern off_source_0_0* to off_source_10"
find output/off_source/ -name "*off_source_0_0*" | xargs -i cp {} output_clustered/off_source_10
echo "Done copying offsource triggers"
echo "Copying onsource triggers"
find output/on_source/ -name "*mat*" | xargs -i cp {} output_clustered/on_source
echo "Done copying onsource triggers"
echo "Copying injection *simulations_* directories"
rm -r output_clustered/*simulations_*
cp -r output/*simulations_* output_clustered
echo "Done copying everything, you can run the clustering DAGs now"
