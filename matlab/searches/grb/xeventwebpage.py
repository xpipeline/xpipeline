#! /usr/bin/env python
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This script generates a event display web pages for an X-pipeline GRB analysis
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from numpy import *
import subprocess, sys, time, os
from optparse import OptionParser

usage = """usage: %prog [options]

This script will generate an event display webpage for an X-pipeline GRB search

xeventdisplay.py [options]

  -d, --event_dir <path>      Path to event directory [REQUIRED]

  -t, --results_type <string> Type of analysis (closedbox, openbox, ulbox) [REQUIRED]

  -p, --event_pass <bool>     Whether event passes or fails cuts

  -n, --event_number <int>    Event number in event list file.  Starts with 1, not zero (i.e. matlab counting).

  -h, --help                  Display this message and exit.


The script calls xplotgrbevent and plotEventLocation as subroutines.


"""

def GPStoGMST(gps):

    # Function to convert a time in GPS seconds to Greenwich Mean Sidereal Time
    # Necessary for converting RA to longitude

    gps0 = 630763213   # Start of J2000 epoch, Jan 1 2000 at 12:00 UTC

    D = (gps-gps0)/86400.0   # Days since J2000

    d_u = floor(D)+0.5  # Days between J2000 and last 0h at Greenwich (always integer + 1/2)

    df_u = D-d_u

    T_u = d_u/36525.0
    gmst0h = 24110.54841 + 8640184.812866*T_u + 0.093104*T_u**2 - 6.2e-6*T_u**3

    gmst = gmst0h + 1.00273790935*86400*df_u

    if (gmst>=86400):
        gmst = gmst - floor(gmst/86400)*86400;

    return gmst


def earthtoradec(theta,phi,gps):

    # Script to convert Earth-centered coordinates to Right Ascension, declination
    # 
    # phi is azimuthal angle (longitude), -pi <= phi < pi
    # theta is polar angle (latitude),   0 <= theta <= pi  zero at north pole, pi at south pole

    gmst = GPStoGMST(gps)  # get time in sidereal day

    gmst_deg = gmst / 86400.0 * 360.0   # convert time in sidereal day to 

    phi = mod(phi,2*pi)
    phi_deg = phi * 180 / pi
    theta_deg = theta * 180 / pi

    ra = phi_deg + gmst_deg
    ra = mod(ra,360)   # wrap angles to 360
    dec = 90 - theta_deg

    return ra, dec




parser = OptionParser(usage=usage)

parser.add_option("-d", "--event_dir", action="store", type="str",\
                      help="absolute path to post-processing directory")

parser.add_option("-t", "--results_type", action="store", type="str", \
                      help="type of reults, closedbox or openbox")

parser.add_option("-p", "--event_pass", action="store", type="int", \
                      help="0 or 1, does event pass cuts?")

parser.add_option("-n", "--event_number", action="store", type="int", \
                      help="number of event in events list file")

(options, args) = parser.parse_args()


event_dir = options.event_dir
results_type = options.results_type
event_pass = options.event_pass
event_number = options.event_number

# ---- Check dir names end in '/'
if not(event_dir.endswith('/')):
    event_dir = event_dir + '/'


cmd = 'lal_tconvert -d'
msg = subprocess.getoutput(cmd)
print()
print('Event display web page generation starts:')
print(msg)
print()

#------------------------------------------------------------------------------
#                             read xgrbwebpage.param
#------------------------------------------------------------------------------

# open the post-processing parameters file to get the analysis directory, web directory, grb name, and user tag
webparams_handle = open(event_dir + 'xgrbwebpage.param')
webparams = webparams_handle.readlines()
webparams_handle.close()
webparams_list = webparams[0].split(' ')

grb_name_idx = webparams_list.index('-g')
grb_name = webparams_list[grb_name_idx + 1]

grb_dir_idx = webparams_list.index('-d')
grb_dir = webparams_list[grb_dir_idx + 1]

web_dir_idx = webparams_list.index('-w')
web_dir = webparams_list[web_dir_idx + 1]

user_tag_idx = webparams_list.index('-u')
user_tag = webparams_list[user_tag_idx + 1]



#------------------------------------------------------------------------------
#                             collect GRB event parameters
#------------------------------------------------------------------------------

params_file = grb_dir + '/grb.param'
summary_file = grb_dir + '/grb_summary.txt'

summary_handle = open(summary_file)
summary = summary_handle.readlines()
summary_handle.close()

name, gps, ra, dec, network, numSky, numBG, analyse = summary[-1].split('\t',8)

params_handle = open(params_file)
params = params_handle.readlines()
params_handle.close()
params_list = params[0].split(' ')

error_idx = params_list.index('-e')
error = params_list[error_idx + 1]




#------------------------------------------------------------------------------
#                             read results parameters file
#------------------------------------------------------------------------------

# open the post-processing parameters file to get the analysis directory, web directory, grb name, and user tag
eventparams_handle = open(event_dir + grb_name + '_' + results_type + '_' + user_tag + '_eventparams.txt')
eventparams = eventparams_handle.readlines()
eventparams_handle.close()

job_string = eventparams[0].rsplit('\n')[0]
numEvents = int(eventparams[1].rsplit('\n')[0])
numPass = int(eventparams[2].rsplit('\n')[0])
numTimes = int(eventparams[3].rsplit('\n')[0])
detectorList = eventparams[4].rsplit('\n')[0]
numLikelihoods = int(eventparams[5].rsplit('\n')[0])
likelihoods = eventparams[6].rsplit('\n')[0]

# Parse the string of job numbers, the last element is always a space. Convert to integers
jobNumbers = job_string.split(' ')[0:-1]
jobNumbers = [ int(x) for x in jobNumbers ]

"""
### The xdetection jobs should have left files of the form 'results_{jobNumber}.mat' in the event directory.
### Rename these files into something more unique.
for jobNumber in jobNumbers:
    old_results_name = event_dir + 'results_' + str(jobNumber) + '.mat'
    new_results_name = event_dir + grb_name + '_TFmap_' + results_type + '_' + str(jobNumber) + '.mat'

    print 'Copying ' + old_results_name + ' to ' + new_results_name

    cmd = 'cp ' + old_results_name + ' ' + new_results_name
    try:
        #msg = commands.getoutput(cmd)
        os.system(cmd)
    except OSError:
        print 'Could not rename xdetection results file!'
        print cmd
        sys.exit()
"""

detectors = detectorList.split(' ')[0:-1]
numDetectors = len(detectors)

# parse the string of likelihoods, the last element is always a space
likelihoodTypes = likelihoods.split(' ')[0:-1]

detectionStatistic = likelihoodTypes[0]

# ------------------------------------------------------------------------------
#                 read event parameters file, generate plots
# ------------------------------------------------------------------------------

# open the post-processing parameters file to get the analysis directory, web directory, grb name, and user tag
if event_pass:
    eventlist_handle = open(event_dir + grb_name + '_' + results_type + '_' + user_tag + '_events_pass.txt')
    print('Starting analysis of loudest events that passed cuts.')
else:
    eventlist_handle = open(event_dir + grb_name + '_' + results_type + '_' + user_tag + '_events.txt')
    print('Starting analysis of loudest events.')

list_of_events = eventlist_handle.readlines()
eventlist_handle.close()

# check that the number of lines in the events file matches the number of expected events
if event_pass:
    if len(list_of_events) == numPass:
        print('There are ' + str(numPass) + ' loudest events which passed cuts.')
    else:
        print('The number of passing events does not match the number of lines!')
        sys.exit()

else:
    if len(list_of_events) == numEvents:
        print('There are ' + str(numPass) + ' loudest events.')
    else:
        print('The number of events does not match the number of lines!')
        sys.exit()


# events are numbered in the matlab way, but be sure to refer to the lines in the pythonic way

event_string = list_of_events[event_number-1].rsplit('\n')[0]
event_params = event_string.split(' ')[0:-1]
event_params = [ float(x) for x in event_params ]

print('Event ' + str(event_number) + ' ' + str(event_params[0]))

# Plot the TF maps
print('Plotting TF maps.')
cmd = 'xplotgrbevent ' + event_dir + ' ' + grb_name + ' ' + results_type + ' ' + user_tag + ' ' + str(event_pass) + ' ' + str(event_number)
try:
    os.system(cmd)
except OSError:
    print('xplotgrbevent failed!')
    print(cmd)
    sys.exit()


# work out the (RA, Dec) coordinates of the event from the best (theta,phi) that were found by xdetection
event_gps = event_params[0]
for i in range(numLikelihoods):
    if likelihoodTypes[i] == 'skypositiontheta':
        event_theta = event_params[6+i]
    elif likelihoodTypes[i] == 'skypositionphi':
        event_phi = event_params[6+i]

event_ra, event_dec = earthtoradec(event_theta,event_phi,event_gps)

# Plot the event over the antenna patterns and search grid
print('Plotting sky location.')
cmd = 'plotEventLocation.py -d ' + event_dir + ' -t ' + results_type + ' -p ' + str(event_pass) + ' -n ' + str(event_number) + \
    ' -o ' + str(event_theta) + ' -f ' + str(event_phi)
try:
    os.system(cmd)
except OSError:
    print('plotEventLocation failed!')
    print(cmd)
    sys.exit()

# ------------------------------------------------------------------------------
#                             build the html page
# ------------------------------------------------------------------------------

if event_pass:
    plot_dir = event_dir + grb_name + '_' + results_type + '_' + user_tag + '_event_pass' + str(event_number) + '/'    
    plot_prefix = grb_name + '_' + results_type + '_' + user_tag + '_pass' + str(event_number)
    webfilename = plot_dir + plot_prefix + '.html'
    print('Building web page: ' + webfilename)
else:
    plot_dir = event_dir + grb_name + '_' + results_type + '_' + user_tag + '_event_' + str(event_number) + '/'
    plot_prefix = grb_name + '_' + results_type + '_' + user_tag + '_' + str(event_number)
    webfilename = plot_dir + plot_prefix + '.html'
    print('Building web page: ' + webfilename)


webfile = open(webfilename, 'w')

header_text = 'Event Display for ' + str(event_params[0]) + ' from ' + results_type.upper() + ' Analysis of ' + grb_name

webfile.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\n')
webfile.write('<html>\n')
webfile.write('<head>\n')
webfile.write('  <title> ' + header_text + ' </title>\n')
webfile.write('  <style type="text/css">\n')
webfile.write('body {\n')
# Georgia is a nice font, too.
webfile.write('font-family: Garamond,Times,serif;\n')
webfile.write('color: black;\n')
webfile.write('background-color: white;\n')
webfile.write('}\n')
webfile.write('h1 {\n')
webfile.write('color: #ffffff;\n')
webfile.write('background-color: #881c1c;\n')
webfile.write('padding: 0.35em;\n')
webfile.write('border: 1px solid black;\n')
webfile.write('}\n')
webfile.write('h2 {\n')
webfile.write('color: #000000;\n')
webfile.write('background-color: #b7b6b6;\n')
webfile.write('padding: 0.35em;\n')
webfile.write('border: 1px solid black;\n')
webfile.write('}\n')
webfile.write('  </style>\n')

webfile.write(' <script type="text/javascript">\n')
webfile.write(' function toggleVisible(division) {\n')
webfile.write(' if (document.getElementById("div_" + division).style.display == "none") {\n')
webfile.write(' document.getElementById("div_" + division).style.display = "block";\n')
webfile.write(' document.getElementById("input_" + division).checked = true;\n')
webfile.write(' } else {\n')
webfile.write(' document.getElementById("div_" + division).style.display = "none";\n')
webfile.write(' document.getElementById("input_" + division).checked = false;\n')
webfile.write(' }\n')
webfile.write(' }\n')
webfile.write(' </script>\n')

webfile.write('</head>\n')
webfile.write('<body>\n')

webfile.write('<h1>' + header_text + '</h1>\n')

webfile.write('<h2><font face="Times" size="5">Analysis Parameters</font></h2>\n')

# GRB info

webfile.write('<br><table border=1 cellspacing="1" cellpadding="5">\n')
webfile.write('<tr>\n')
webfile.write(' <td align="center"> GRB GPSTime </td>\n')
webfile.write(' <td align="center"> RA [deg] </td>\n')
webfile.write(' <td align="center"> Dec [deg] </td>\n')
webfile.write(' <td align="center"> Localization Error [deg] </td>\n')
webfile.write(' <td align="center"> Search Grid Positions </td>\n')
for i in range(numDetectors):
    webfile.write(' <td align="center"> IFO ' + str(i+1) + '</td>\n')
webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('<tr>\n')
webfile.write(' <td align="center"> ' + gps + '</td>\n')
webfile.write(' <td align="center"> ' + ra + ' </td>\n')
webfile.write(' <td align="center"> ' + dec + ' </td>\n')
webfile.write(' <td align="center"> ' + error + ' </td>\n')
webfile.write(' <td align="center"> ' + numSky + ' </td>\n')
for i in range(numDetectors):
    webfile.write(' <td align="center"> ' + detectors[i] + '\n')
webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('</table><br>\n')


# Event Parameters

webfile.write('<h2><font face="Times" size="5">Event Parameters</font></h2>\n')

webfile.write('<br><table border=1 cellspacing="1" cellpadding="5">\n')
webfile.write('<tr>\n')
webfile.write(' <td align="center"> Peak Time </td>\n')
webfile.write(' <td align="center"> Significance </td>\n')
webfile.write(' <td align="center"> Probability </td>\n')
webfile.write(' <td align="center"> Freq. Resolution at Max Likelihood [Hz] </td>\n')
webfile.write(' <td align="center"> Peak Frequency [Hz] </td>\n')
webfile.write(' <td align="center"> Duration [msec] </td>\n')
webfile.write(' <td align="center"> Lower Frequency [Hz] </td>\n')
webfile.write(' <td align="center"> Upper Frequency [Hz] </td>\n')
webfile.write(' <td align="center"> # of TF pixels </td>\n')
webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('<tr>\n')
webfile.write(' <td align="center"> ' + str(event_params[0]) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[3]) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[4]) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[2]) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[numLikelihoods+6]) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[numLikelihoods+6+1]*1000) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[numLikelihoods+6+2]) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[numLikelihoods+6+3]) + '</td>\n')
webfile.write(' <td align="center"> ' + str(event_params[numLikelihoods+6+4]) + '</td>\n')
webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('</table><br>\n')



# Likelihoods

webfile.write('<br><table border=1 cellspacing="1" cellpadding="5">\n')
webfile.write('<tr>\n')

for i in range(numLikelihoods):
    if likelihoodTypes[i]=='skypositiontheta':
        webfile.write(' <td align="center"> RA [deg] </td>\n')
    elif likelihoodTypes[i]=='skypositionphi':
        webfile.write(' <td align="center"> Dec [deg] </td>\n')
    else:
        webfile.write(' <td align="center"> ' + likelihoodTypes[i] + ' </td>\n')

webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('<tr>\n')

for i in range(numLikelihoods):
    if likelihoodTypes[i]=='skypositiontheta':
        webfile.write(' <td align="center"> ' + '%.2f' % event_ra + ' </td>\n')
    elif likelihoodTypes[i]=='skypositionphi':
        webfile.write(' <td align="center"> ' + '%.2f' % event_dec + ' </td>\n')
    else:
        webfile.write(' <td align="center"> ' + str(event_params[6+i]) + ' </td>\n')

webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('</table><br>\n')



# Pass Cuts

webfile.write('<br><table border=1 cellspacing="1" cellpadding="5">\n')
webfile.write('<tr>\n')
webfile.write(' <td align="center"> Pass Window? </td>\n')
webfile.write(' <td align="center"> Pass Veto Segs? </td>\n')
webfile.write(' <td align="center"> Pass Fixed Cut? </td>\n')
webfile.write(' <td align="center"> Pass Ratio Cut? </td>\n')
webfile.write(' <td align="center"> <b>Pass?</b> </td>\n')
webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('<tr>\n')
for i in range(4):
    if event_params[numLikelihoods+6+6+i]:
        webfile.write(' <td align="center"> Yes </td>\n')
    else:
        webfile.write(' <td align="center"> No </td>\n')

if event_params[numLikelihoods+6+5]:
    webfile.write(' <td align="center"> <b>Yes</b> </td>\n')
else:
    webfile.write(' <td align="center"> <b>No</b> </td>\n')
webfile.write(' </td>\n')
webfile.write('</tr>\n')

webfile.write('</table><br>\n')



# If we're analyzing time slides, print the unslid time in each detector

if results_type == 'closedbox':
    webfile.write('<br><table border=1 cellspacing="1" cellpadding="5">\n')
    webfile.write('<tr>\n')
    for i in range(numDetectors):
        webfile.write(' <td align="center"> Unslid time in ' + detectors[i] + ' </td>\n')
    webfile.write(' </td>\n')
    webfile.write('</tr>\n')

    webfile.write('<tr>\n')
    for i in range(numDetectors):
        webfile.write(' <td align="center"> ' + str(event_params[numLikelihoods+6+10+i]) + ' </td>\n')
    webfile.write(' </td>\n')
    webfile.write('</tr>\n')

    webfile.write('</table><br>\n')




# Plot TFMaps for the detection statistic for all analysisTimes

webfile.write('<h2><font face="Times" size="5">Detection Statistic TFMaps Across Time Resolutions</font></h2>\n')

webfile.write('<p>\n')
webfile.write(' <input id="TFMaps_analysisTime_Unmasked" type="checkbox" checked \n')
webfile.write(' onclick="toggleVisible(' + "'TFMaps_analysisTime_Unmasked '" + ');" /> \n')
webfile.write(' Unmasked TF Maps \n')
webfile.write(' <div id="div_TFMaps_analysisTime_Unmasked " style="display: block;"> \n')
webfile.write('</p>\n')

numColumns = 3
numRows = numTimes/numColumns
for i in range(numRows):

    webfile.write('<br><TABLE BORDER=0><TR>\n')
    for j in range(numColumns):

        timeIDX = i*3 + j + 1
        plot_file = plot_prefix + '_' + detectionStatistic + '_' + str(timeIDX) + '.png'
        webfile.write('<TD ALIGN="left"><A HREF="' + plot_file + '"><IMG SRC="' +  plot_file + '" WIDTH=300 ALT="' + detectionStatistic + ' ' + str(timeIDX) + \
                          ' TFMap ALT" TITLE="' + detectionStatistic + ' ' + str(timeIDX) + ' TFMap"></A></TD>\n')

    webfile.write('</TR></TABLE>\n')

webfile.write('</div>\n')




# Plot TFMaps for the detection statistic for all analysisTimes - below-threshold pixels masked

webfile.write('<p>\n')
webfile.write(' <input id="TFMaps_analysisTime_Masked" type="checkbox" unchecked \n')
webfile.write(' onclick="toggleVisible(' + "'TFMaps_analysisTime_Masked '" + ');" /> \n')
webfile.write(' Masked TF Maps \n')
webfile.write(' <div id="div_TFMaps_analysisTime_Masked " style="display: none;"> \n')
webfile.write('</p>\n')

numColumns = 3
numRows = numTimes/numColumns
for i in range(numRows):

    webfile.write('<br><TABLE BORDER=0><TR>\n')
    for j in range(numColumns):

        timeIDX = i*3 + j + 1
        plot_file = plot_prefix + '_' + detectionStatistic + '_' + str(timeIDX) + '_masked.png'
        webfile.write('<TD ALIGN="left"><A HREF="' + plot_file + '"><IMG SRC="' +  plot_file + '" WIDTH=300 ALT="' + detectionStatistic + ' ' + str(timeIDX) + \
                          ' TFMap ALT" TITLE="' + detectionStatistic + ' ' + str(timeIDX) + ' TFMap"></A></TD>\n')

    webfile.write('</TR></TABLE>\n')

webfile.write('</div>\n')


# Plot TFMaps for all likelihoods

webfile.write('<h2><font face="Times" size="5">Likelihood Time-Frequency Maps</font></h2>\n')

webfile.write('<p>\n')
webfile.write(' <input id="TFMaps_Likelihoods_Unmasked" type="checkbox" checked \n')
webfile.write(' onclick="toggleVisible(' + "'TFMaps_Likelihoods_Unmasked '" + ');" /> \n')
webfile.write(' Unmasked TF Maps \n')
webfile.write(' <div id="div_TFMaps_Likelihoods_Unmasked " style="display: block;"> \n')
webfile.write('</p>\n')

numColumns = 3
numRows = numLikelihoods/numColumns
for i in range(numRows):

    webfile.write('<br><TABLE BORDER=0><TR>\n')
    for j in range(numColumns):

        likeIDX = i*3 + j
        if likelihoodTypes[likeIDX]=='skypositiontheta':
            continue
        elif likelihoodTypes[likeIDX]=='skypositionphi':
            continue
        else:
            plot_file = plot_prefix + '_' + likelihoodTypes[likeIDX] + '_peakdT.png'
            webfile.write('<TD ALIGN="left"><A HREF="' + plot_file + '"><IMG SRC="' +  plot_file + '" WIDTH=300 ALT="' + likelihoodTypes[likeIDX] + \
                                      ' TFMap ALT" TITLE="' + likelihoodTypes[likeIDX] + ' TFMap"></A></TD>\n')

    webfile.write('</TR></TABLE>\n')

webfile.write('</div>\n')


# Plot TFMaps for all likelihoods - below-threshold pixels masked

webfile.write('<p>\n')
webfile.write(' <input id="TFMaps_Likelihoods_Masked" type="checkbox" unchecked \n')
webfile.write(' onclick="toggleVisible(' + "'TFMaps_Likelihoods_Masked '" + ');" /> \n')
webfile.write(' Masked TF Maps \n')
webfile.write(' <div id="div_TFMaps_Likelihoods_Masked " style="display: none;"> \n')
webfile.write('</p>\n')

numColumns = 3
numRows = numLikelihoods/numColumns
for i in range(numRows):

    webfile.write('<br><TABLE BORDER=0><TR>\n')
    for j in range(numColumns):

        likeIDX = i*3 + j
        if likelihoodTypes[likeIDX]=='skypositiontheta':
            continue
        elif likelihoodTypes[likeIDX]=='skypositionphi':
            continue
        else:
            plot_file = plot_prefix + '_' + likelihoodTypes[likeIDX] + '_peakdT_masked.png'
            webfile.write('<TD ALIGN="left"><A HREF="' + plot_file + '"><IMG SRC="' +  plot_file + '" WIDTH=300 ALT="' + likelihoodTypes[likeIDX] + \
                                      ' TFMap ALT" TITLE="' + likelihoodTypes[likeIDX] + ' TFMap"></A></TD>\n')

    webfile.write('</TR></TABLE>\n')

webfile.write('</div>\n')

# Plot the reconstructed event sky location compared to the search grid and antenna patterns

webfile.write('<h2><font face="Times" size="5">Recovered Sky Location</font></h2>\n')

webfile.write('<br><TABLE BORDER=0><TR>')
plot_file = plot_prefix + '_FullSkyAntennaSensitivityDP.png'
webfile.write('<TD ALIGN="left"><A HREF="' + plot_file + '"><IMG SRC="' +  plot_file + '" WIDTH=300 ALT="Event Location ALT" TITLE="Event Location"></A></TD>\n')
for i in range(numDetectors):
    plot_file = plot_prefix + '_FullSkyAntennaSensitivity' + detectors[i] + '.png'
    webfile.write('<TD ALIGN="left"><A HREF="' + plot_file + '"><IMG SRC="' +  plot_file + \
                      '" WIDTH=300 ALT="Event Location ALT" TITLE="Event Location"></A></TD>\n')

webfile.write('</TR></TABLE>\n')

webfile.write('\n')
webfile.close()

print('Done.')
print()


cmd = 'lal_tconvert -d'
msg = subprocess.getoutput(cmd)
print()
print('Event display web page generation finished.')
print(msg)
