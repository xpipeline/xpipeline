# -*- coding: utf-8 -*-
# Copyright (C) Scott Coughlin (2017)
# Updated by Patrick Sutton (2024)
#
# This file is part of XPypeline / x-pipeline.
#
# GWpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GWpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GWpy.  If not, see <http://www.gnu.org/licenses/>.

"""This module contains utility functions for setUpJobs 
"""
from gwpy.segments import Segment, DataQualityDict, SegmentList, DataQualityFlag
import operator
import os
import functools 


def query_veto_definer_file(ifo, start, end, cp):
    # ---- Obtain all segments that are analysis-ready in the interval [start,end).
    analysis_ready = DataQualityFlag.query(cp.get(ifo,"include-segments"), start, end)
    # ---- Query for vetos found in the interval [start,end).
    vdf = DataQualityDict.from_veto_definer_file(cp.get('segfind', 'veto-file'), start, end)
    # ---- Populate the veto dictionary object with vetoes during analysis-ready segments,
    #      ignoring vetoes during non-analysis-ready times.
    vdf.populate(segments=analysis_ready.active)

    return vdf, analysis_ready


def filter_for_cat_type(vdf, ifo, cat):
    return functools.reduce(operator.or_, [f.active for f in vdf.values() if f.ifo == ifo and f.category in cat], SegmentList())
   

def generate_segments_and_vetos(ifos, start, end, cp):
    """determine vetos during requested analysis time
    Inputs
    ------
    ifos  : `str`, list of ifos used in X-Pipeline analysis
    start : `float`, :class:`~gwpy.time.LIGOTimeGPS`
    end   : `float`, `~gwpy.time.LIGOTimeGPS`
    cp    : `object` ConfigParser object

    Returns
    -------
    DataQualityDict : ~gwpy.segments.DataQualityDict`
    """
    analysis_seg_files = []
    veto_seg_files = []

    # ---- If simulating noise skip all segment, veto and network validation checks. 
    #      The on-source and off-source are simulated and therefore have no DQ issues.
    if cp.has_option('parameters','makeSimulatedNoise'):
        for ifo in ifos:
            print('Making simulated noise, creating temp segment file')
            f = open('segments_{0}.txt'.format(ifo),'w')
            f.write('0 {0} {1} {2}'.format(start, end ,end - start))
            f.close()
            analysis_seg_files.append('segments_{0}.txt'.format(ifo))
            veto_seg_files.append("None")
    else:
        for ifo in ifos:
            if cp.has_option(ifo,'segment-list'):
                if not os.path.isfile(cp.get(ifo,'segment-list')):
                    raise ValueError('Please uncomment the '
                                     'the segment file in ini file '
                                     'as it does not exist. '
                                     'If you want to use a '
                                     'supplied file please provide one.')
                else:
                    analysis_seg_files.append(cp.get(ifo,'segment-list'))
            if cp.has_option(ifo,'veto-list'):
                if not os.path.isfile(cp.get(ifo,'veto-list')):
                    raise ValueError('Please uncomment the '
                                     'the veto file in ini file '
                                     'as it does not exist. '
                                     'If you want to use a '
                                     'supplied file please provide one.')
                else:
                    veto_seg_files.append(cp.get(ifo,'veto-list'))
            else:
                # ---- Query veto definer file. This function also returns the analysis ready segments.
                vdf, analysis_ready = query_veto_definer_file(ifo, start, end, cp)

                # ---- Construct (analysis_ready - cat1) analysis segments.

                # ---- Filter for cat1 vetos. These will be subtracted from the 
                #      observing-mode segments to determine the periods x-pipeline
                #      analyses.
                segs = filter_for_cat_type(vdf, ifo, [1])

                # ---- Write out cat1 vetoes to text files. These files are not used 
                #      by x-pipeline, but are useful for debugging.
                filename_cat1 = "input/" + ifo +  "-veto-cat1.txt"
                segs.write(filename_cat1)

                # ---- Subtract cat 1 vetoes from analysis_ready.
                analysis_ready_minus_cat1 = analysis_ready.active - segs

                # ---- Save new segment list to file.
                filename_analysis_ready_minus_cat1 = "input/" + ifo + "_science_cat1.txt"
                analysis_ready_minus_cat1.write(filename_analysis_ready_minus_cat1)
                analysis_seg_files.append(filename_analysis_ready_minus_cat1)

                # ---- Construct (cat2 + cat4) veto segments.

                # ---- Filter for category 2 and 4 vetos and write these to files.
                #      These files are not used by x-pipeline, but are useful for
                #      understanding the origin of vetoes applied the GW events.
                cat = [2, 4]
                for iCat in cat:
                    segs = filter_for_cat_type(vdf, ifo, [iCat])
                    filename = "input/" + ifo +  "-veto-cat{0}.txt".format(iCat)
                    segs.write(filename)

                # ---- Combine the category 2 and 4 vetos in a single file.
                segs = filter_for_cat_type(vdf, ifo, cat)
                filename = "input/" + ifo + "_cat24veto.txt"
                segs.write(filename)
                veto_seg_files.append(filename)

    return analysis_seg_files, veto_seg_files


#    Divides the science segment into chunks of length seconds overlapped by 
#    overlap seconds. 
#    when generating chunks 
#    """
#    chunks = []
#    for iseg in segment:
#        segtemp = SegmentList([iseg])
#        time_left = abs(segtemp)
#        start = segtemp.extent()[0]
#        increment = length - overlap
#        while time_left >= length:
#            end = start + length
#            chunks.append(Segment(start, end))
#            start += increment
#            time_left -= increment
#
#    return SegmentList(chunks)


