#!/usr/bin/env python
"""
XFIXHELDJOBS - find held condor jobs and attempt to fix known common problems

$Id$

"""
__author__  = 'Patrick Sutton <patrick.sutton@ligo.org>'
__date__    = '$Date$'
__version__ = '$Revision$'

import sys, os, getopt
from time import sleep


# ---- Function usage.
def usage():
    msg = """\
XFIXHELDJOBS - find held condor jobs and attempt to fix known common problems

Usage: 
    xfixheldjobs.py [options]
    -u, --user <name>    Optional user name of jobs to be checked. If not 
                         specified, defaults to the script's user.
    -h, --help           Display this message and exit.

  e.g.,
    xfixheldjobs.py -u patrick.sutton

The output of condor_q for the user is parsed to find any held jobs. The error
message for each held job is compared to a list of known common issues and the
script attempts to fix the issue and release the job.

Note that typically the script can only fix the user's own jobs to due to condor
and file-system permissions. For other users information on held jobs is 
reported.
"""
    print(msg, file=sys.stderr)


def xfixheldjobs(user=None):

    # --------------------------------------------------------------------------
    #    Find held jobs
    # --------------------------------------------------------------------------

    # ---- We will be using temporary files to capture and process output from 
    #      condor_q. We will put these in the user's home directory. 
    user_home = os.path.expanduser('~')
    held_file = user_home + "/.held_jobs.txt"

    # ---- Get dump of held jobs for the specified --user (if any) or for the 
    #      user running this script. Write this information to a hidden file.
    if user:
        os.system("condor_q " + user + " -hold > " + held_file)
    else: 
        os.system("condor_q -hold > " + held_file)

    # ---- Report total number of held jobs.
    number_held = 0
    f = open(held_file,'r')
    lines = f.read().splitlines()     
    for line in lines:
        if line.startswith("Total for query:"):
            number_held = line.split()[3]
    f.close()
    print("Found " + number_held + " held jobs.")


    # --------------------------------------------------------------------------
    #    Find jobs with log permission errors.
    # --------------------------------------------------------------------------

    # ---- Use grep to find all entries with error number 13, which is invalid 
    #      file permissions. Then use awk to extract the condor job number and 
    #      the log file name. Write this information to a hidden file.
    num_file = user_home + "/.num_file.txt"
    grep_command =   "grep \"(errno 13)\" " + held_file \
                   + " | awk -F\\' '{print $2, $1}' | awk '{print $2, $1}' " \
                   + ">  " + num_file
    os.system(grep_command)
    f = open(num_file,"r")
    lines = f.readlines()
    if len(lines):
        print("Attempting to fix log file permissions for", len(lines), "jobs.") 
        print("(This will fail if the script user is not the owner of the files.)")
        for x in lines:
            jobnum  = x.split(' ')[0]
            logfile = x.split(' ')[1]
            logfile = logfile.rstrip()
            if logfile.endswith('out'):
                os.system("chmod 666 " + logfile)
                logfile = logfile[:-3]
                logfile = logfile + "err"
                os.system("chmod 666 " + logfile)
            elif logfile.endswith('err'):
                os.system("chmod 666 " + logfile)
                logfile = logfile[:-3]
                logfile = logfile + "out"
                os.system("chmod 666 " + logfile)
            else:
                print("No method to handle this file:", logfile)
            os.system("condor_release " + jobnum)
    else:
        print("No jobs held due to known fixable issues.")
    f.close()


    # -----------------------------------------------------------------------------
    #    Clean up.
    # -----------------------------------------------------------------------------

    os.system("rm -rf " + held_file + " " + num_file )


if __name__ == '__main__':
    # ---- This function is being executed directly as a script (as opposed to 
    #      being imported by another code). Parse the command line arguments 
    #      then run the xfixheldjobs() function.

    # -------------------------------------------------------------------------
    #      Parse the command line options.
    # -------------------------------------------------------------------------

    # ---- Assign defaults to command-line arguments.
    user = None

    # ---- Syntax of options, as required by the getopt command.
    # ---- Short form.
    shortop = "hu:"
    # ---- Long form.
    longop = [
        "help",
        "user=",
        ]

    # ---- Get command-line arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    # ---- Parse command-line arguments.  Arguments are returned as strings, so 
    #      convert type as necessary.
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-u", "--user"):
            user = a      
        else:
            print("Unknown option:", o, file=sys.stderr)
            usage()
            sys.exit(1)

    # ---- Call the xfixheldjobs() function.
    xfixheldjobs(user)


