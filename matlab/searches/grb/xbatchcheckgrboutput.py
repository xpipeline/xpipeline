#!/usr/bin/env python

"""
Checks existence of output files of many xdetection as run using grb.py
using xcheckgrboutput.py
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt
import configparser

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  xbatchcheckgrboutput.py [options]
  -p, --parent-dir <path>     path of parent dir [REQUIRED] 
  -h, --help                  display this message and exit

e.g.,
  xbatchcheckgrboutput.py -p /archive/home/gjones/S5/v3/H1H2L1/
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
grb_list = None
detector = []

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:"
# ---- Long form.
longop = [
   "help",
   "parent-dir=",
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--parent-dir"):
        parent_dir = a      
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not parent_dir:
    print("No parent dir specified.", file=sys.stderr)
    print("Use --parent-dir to specify it.", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#    Checking output of X-Pipeline GRB search      #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("             parent dir:", parent_dir, file=sys.stdout)
print(file=sys.stdout)

dirContents = os.listdir(parent_dir)

grbDirs = []

# ---- check dir names end in '/'
if not(parent_dir.endswith('/')):
    parent_dir = parent_dir + '/'

# ---- loop through contents of parent_dir
#      if contents name begins with GRB and is a dir add to our list
#      of grbDirs
print('We will process the following dirs:')
for idx in range(len(dirContents)):
    # look for GRB* dirs 
    if dirContents[idx].startswith('GRB') and os.path.isdir(parent_dir + dirContents[idx]):
        grbDirs.append(dirContents[idx])
        print(parent_dir + dirContents[idx])

    # look for MOCK* dirs 
    if dirContents[idx].startswith('MOCK') and os.path.isdir(parent_dir + dirContents[idx]):
        grbDirs.append(dirContents[idx])
        print(parent_dir + dirContents[idx])

print(file=sys.stdout)

for idx in range(len(grbDirs)):

    command = "xcheckgrboutput.py -d " + parent_dir + grbDirs[idx] 
    print(command)
    os.system(command) 

print(file=sys.stdout)
print(" ... finished.", file=sys.stdout)


