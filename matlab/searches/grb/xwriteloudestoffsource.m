function [] = xwriteloudestoffsource(fout,analysis,offLoudestEventCell,...
    offLoudestEventName,binArray,figures_dirName,figfiles_dirName)
% XWRITELOUDESTOFFSOURCE - Plot rate vs. significance of ludest events
% 
% XWRITELOUDESTOFFSOURCE makes a .fig file showing the cumulative distribution
% of the significance of the loudest event in each off-source trial. It is a
% helper function for XMAKEHTMLREPORT. 
%
% Usage:
%
%  xwriteloudestoffsource(fout,analysis,offLoudestEventCell, ...
%      offLoudestEventName,binArray,figures_dirName,figfiles_dirName)
%
%   fout                  Pointer to open and writable webpage file.  
%   analysis              Structure created by XMAKEGRBWEBPAGE.
%   offLoudestEventCell   Cell array. Each element is a struct containing the 
%                         significance value for the loudest event in each of 
%                         the off-source trials before or after various cuts are
%                         applied The significance field will contain a number 
%                         of elements equal to the number of off-source trials.
%   offLoudestEventName   Cell array. Each element is a string describing what 
%                         cuts have been applied for the corresponding element 
%                         of offLoudestEventCell.
%   binArray              Vector of bins used when making significance
%                         histograms.
%   figures_dirName       String, Path of output directory where .png figures
%                         will be saved.
%   figfiles_dirName      String, Path of output directory where .fig figures
%                         will be saved.
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(7, 7, nargin));

fprintf(fout,'%s\n','<h2>Loudest Off-source Events</h2>');

% ---- Get number of background trials.
Ntrials = length(offLoudestEventCell{1}.significance);

% ---- Make differential and cumulative histograms.
for ii = 1:length(offLoudestEventCell)
    histOffLoudest(:,ii) = histc(offLoudestEventCell{ii}.significance,binArray);
end
cumHistOffLoudest = flipud(cumsum(flipud(histOffLoudest)));
% ---- Truncate cumulative histograms to start at lowest-significance loudest event.
for ii=1:length(offLoudestEventCell)
    idx = find(cumHistOffLoudest(:,ii)==Ntrials,1,'last')
    if ~isempty(idx) && idx>1
        cumHistOffLoudest(1:idx-1,ii) = 0;  %-- 0 will not appear on a loglog plot
    end
end

% ---- Plot histograms.
lineStyles = {'b','g','r','k','c','m','y'};
lineWidths = [ 8,  6,  4,  2,  2,  2,  2];
figure; set(gca,'FontSize',20);
for ii = 1:length(offLoudestEventCell)
    % ---- FAP vs significance.
    loglog(binArray,cumHistOffLoudest(:,ii)/Ntrials,lineStyles{ii},'LineWidth',lineWidths(ii));
    hold on;
end
grid;
xlabel('significance');
ylabel('false alarm probability');
legend(offLoudestEventName)
% % ---- Add extra y-axis to right showing number of background trials.
% %      The yyaxis command requires matlab 2016a or later.
% ylim = get(gca,'ylim');
% yyaxis right
% set(gca,'ylim',ylim*Ntrials)
% ylabel('number of background trials');

% ---- Save plot
plotNameHistOffLoudest = ['/hist_offLoudest_' analysis.clusterType];
xsavefigure(plotNameHistOffLoudest,figures_dirName,figfiles_dirName);

% ---- Put all plots into a table.
xaddfiguretable(fout,plotNameHistOffLoudest,figures_dirName,...
    analysis.captions.hist_offLoudest);

% ---- Done.
return

