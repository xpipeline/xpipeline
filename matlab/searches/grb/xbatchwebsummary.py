#!/usr/bin/env python

"""
Collect summary information from closed-/open-/ul-box webpages for web presentation
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt, glob
import configparser

# ---- Workaround to get relpath in python 2.5 (it is standard for >=2.6).
import posixpath
from posixpath import curdir, sep, pardir, join
def relpath(path, start=curdir):
    """Return a relative version of a path"""
    if not path:
        raise ValueError("no path specified")
    start_list = posixpath.abspath(start).split(sep)
    path_list = posixpath.abspath(path).split(sep)
    # ---- Work out how much of the filepath is shared by start and path.
    i = len(posixpath.commonprefix([start_list, path_list]))
    rel_list = [pardir] * (len(start_list)-i) + path_list[i:]
    if not rel_list:
        return curdir
    return join(*rel_list)


# ---- Function usage.
def usage():
    msg = """\

xbatchwebsummary.py - collect summary information from closed-box or open-box 
and ul-box webpages for web presentation

Usage: 

  xbatchwebsummary.py [options]
  ***Only one of the following two arguments should be provided.***
    -p, --parent-dir <path>     [REQUIRED] Absolute path of parent directory 
    -g, --dir-list <path>       [REQUIRED] Absolute path to file containing list 
                                of result directories 
    --pop-det-stat <string>     Tilde-delimited string of population detection 
                                statistics to use. See searches/grb/popdetstat.m
                                for allowed statistics.
    --redshift-file <path>      Absolute path to file containing list of 
                                observed redshifts (single column, unsorted).
    --closed-box                Process closed-box page only; if not supplied 
                                open-box and ul-box pages will be processed.
    --simplified                Produce only basic results using 
                                xbatchwebsimplesummary.m. STRONGLY RECOMMENDED. 
                                Otherwise xbatchwebsummary.m is used and the 
                                results are not known to be reliable.
    -h, --help                  Display this message and exit.

example:

  xbatchwebsummary.py -p /home/patrick.sutton/Runs/O3/ --closed-box --simplified

Use of the --simplified flag is STRONGLY RECOMMENDED.

"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command-line options.
# -------------------------------------------------------------------------

# ---- Initialise command-line argument variables.
params_file   = None
grb_list      = None
detector      = []
parent_dir    = None
dir_list_file = None
isClosedBox   = False
isSimplified  = False
redshift_file = ""
popDetString  = ""

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:g:"
# ---- Long form.
longop = [
    "help",
    "parent-dir=",
    "dir-list=",
    "redshift-file=",
    "closed-box",
    "simplified",
    "pop-det-stat="
    ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--parent-dir"):
        parent_dir = a      
    elif o in ("-g", "--dir-list"):
        dir_list_file = a      
    elif o in ("--redshift-file"):
        redshift_file = a      
    elif o in ("--closed-box"):
        isClosedBox = True  
    elif o in ("--simplified"):
        isSimplified = True  
    elif o in ("--pop-det-stat"):
        popDetString = a
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if parent_dir and dir_list_file:
    print("Both --parent-dir and --dir-list options used.", file=sys.stderr)
    print("Please use exactly one of these two options.", file=sys.stderr)
    sys.exit(1)
if not parent_dir and not dir_list_file:
    print("Neither --parent-dir nor --dir-list specified.", file=sys.stderr)
    print("Please use exactly one of these two options.", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#  Summarizing results of X-Pipeline GRB search    #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
if parent_dir:
    print("             parent dir:", parent_dir, file=sys.stdout)
if dir_list_file:
    print("    list of directories:", dir_list_file, file=sys.stdout)
if isClosedBox:
    print("               box type: closed", file=sys.stdout)
else:
    print("               box type: open/ul", file=sys.stdout)
if isSimplified:
    print("    simplified analysis: true", file=sys.stdout)
else:
    print("    simplified analysis: false", file=sys.stdout)
if popDetString:
    print("  popln detection stats:", popDetString, file=sys.stdout)
else:
    print("  popln detection stats: none", file=sys.stdout)
if redshift_file:
    print("          redshift_file:", redshift_file, file=sys.stdout)
else:
    print("          redshift_file: none", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Construct list of GRB directories to process.
# -------------------------------------------------------------------------

grbDirs = []
if parent_dir:

    # ---- Get contents of parent directory.
    dirContents = os.listdir(parent_dir)
    # ---- Ensure directory names end in '/'
    if not(parent_dir.endswith('/')):
        parent_dir = parent_dir + '/'

    # ---- Loop through contents of parent_dir. If contents name begins with GRB*, MOCK*, or E* 
    #      and is a directory then add it to our list of grbDirs to be processed.
    print('We will process the following directories:')
    for idx in range(len(dirContents)):
        # ---- Look for GRB* directories. 
        if dirContents[idx].startswith('GRB') and os.path.isdir(parent_dir + dirContents[idx]):
            grbDirs.append(dirContents[idx])
            print(parent_dir + dirContents[idx])

        # ---- Look for MOCK* directories. 
        if dirContents[idx].startswith('MOCK') and os.path.isdir(parent_dir + dirContents[idx]):
            grbDirs.append(dirContents[idx])
            print(parent_dir + dirContents[idx])

        # ---- Look for E* directories. 
        if dirContents[idx].startswith('E') and os.path.isdir(parent_dir + dirContents[idx]):
            grbDirs.append(dirContents[idx])
            print(parent_dir + dirContents[idx])

else:

    # ---- Record GRB directories from supplied file. 
    f = open(dir_list_file, 'r')
    for line in f:
        grbDirs.append(line[:-1])

print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Scan GRB directories for matlab files.
# -------------------------------------------------------------------------

# ---- Initialise storage.
closedboxResults   = []
noClosedboxResults = []
openboxResults     = []
noOpenboxResults   = []
ulboxResults       = []
noULboxResults     = []

# ---- Loop over GRB directories.
for grbIdx in range(len(grbDirs)):

    if parent_dir:
        auto_dir = parent_dir + grbDirs[grbIdx]
    else:
        auto_dir = grbDirs[grbIdx] 

    closedPage = glob.glob(auto_dir + '/*closedbox.mat')
    openPage   = glob.glob(auto_dir + '/*openbox.mat')
    ulPage     = glob.glob(auto_dir + '/*ulbox.mat')
    if isClosedBox :
        if closedPage :
            closedboxResults.append(closedPage)
        else :
            noClosedboxResults.append(auto_dir)
    else :
        if openPage and ulPage :
            openboxResults.append(openPage)
            ulboxResults.append(ulPage)
        else: 
            if not openPage :
                noOpenboxResults.append(auto_dir)
            if not ulPage :
                noULboxResults.append(auto_dir)

# ---- Write available result pages to a file that will be read by the matlab function.
if isClosedBox :
    fclosed = open("tmpGRBclosed.txt","w")
    for idx in range(len(closedboxResults)):
        fclosed.write(relpath(closedboxResults[idx].pop()) + '\n')
    fclosed.close()
else :
    fopenbox = open("tmpGRBopen.txt","w")
    for idx in range(len(openboxResults)):
        fopenbox.write(relpath(openboxResults[idx].pop()) + '\n')
    fopenbox.close()
    fulbox = open("tmpGRBul.txt","w")
    for idx in range(len(ulboxResults)):
        fulbox.write(relpath(ulboxResults[idx].pop()) + '\n')
    fulbox.close()

# ---- Report on directories that are missing results.
if isClosedBox :
    print("Missing closed box results (" + str(len(noClosedboxResults)) + "): ", file=sys.stdout) 
    for idx in range(len(noClosedboxResults)):
        print(noClosedboxResults[idx], file=sys.stdout)
    print(file=sys.stdout)
else :
    print("Missing open box results (" + str(len(noOpenboxResults)) + "): ", file=sys.stdout) 
    for idx in range(len(noOpenboxResults)):
        print(noOpenboxResults[idx], file=sys.stdout)
    print(file=sys.stdout)

    print("Missing UL box results (" + str(len(noULboxResults)) + "): ", file=sys.stdout) 
    for idx in range(len(noULboxResults)):
        print(noULboxResults[idx], file=sys.stdout)
    print(file=sys.stdout)

# ---- Construct system call to compiled matlab function.
if isSimplified :
    if isClosedBox :
        summary_command = ' '.join(["xbatchwebsimplesummary",
                                    "tmpGRBclosed.txt",
                                    "tmpGRBclosed.txt",
                                    "closed",popDetString,redshift_file])                   
    else :    
        summary_command = ' '.join(["xbatchwebsimplesummary",
                                    "tmpGRBopen.txt",
                                    "tmpGRBul.txt",
                                    "open",popDetString,redshift_file])                   
else :
    if isClosedBox :
        summary_command = ' '.join(["xbatchwebsummary",
                                    "tmpGRBclosed.txt",
                                    "tmpGRBclosed.txt",
                                    "closed",popDetString,
                                    redshift_file])                   
    else :    
        summary_command = ' '.join(["xbatchwebsummary",
                                    "tmpGRBopen.txt",
                                    "tmpGRBul.txt",
                                    "open",popDetString,
                                    redshift_file])                   
print(summary_command, file=sys.stdout)

# ---- Execute system call to compiled matlab function.
os.system(summary_command)


print(" ... finished.", file=sys.stdout)

