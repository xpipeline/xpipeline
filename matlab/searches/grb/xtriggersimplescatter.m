function [plotName] = xtriggersimplescatter(fout,trigProperty1,...
    trigPropertyName1,trigProperty2,trigPropertyName2,legendCell,...
    figures_dirName,figfiles_dirName,plotTag,...
    loudest1,loudest2,loudestLegend,plotType)
% XTRIGGERSIMPLESCATTER - produce scatter plots of pairs of trigger properties.
% 
% xtriggersimplescatter produces scatter plot of two trigger properties
% (e.g., significance and central frequency) for a number of different 
% data types (e.g., on source, off source, injections). 
% It is a helper function for xwritetriggerhists.
%
% Usage [plotName] = xtriggersimplescatter(fout,trigProperty1,...
%    trigPropertyName1,trigProperty2,trigPropertyName2,legendCell,...
%    figures_dirName,figfiles_dirName,plotTag,...
%    loudest1,loudest2,loudestLegend,plotType)
%
%   fout              Pointer to open and writable webpage file  
%   trigProperty1     Cell array containing values of a given trigger property
%                     (e.g., significance) for each of the data sets we are
%                     looking at, e.g., onSource, offSource, injections.
%   trigPropertyName1 String describing trigProperty, e.g., 'significance'
%   trigProperty2     Cell array containing values of a given trigger property
%                     (e.g., significance) for each of the data sets we are
%                     looking at, e.g., onSource, offSource, injections.
%   trigPropertyName2 String describing trigProperty, e.g., 'significance'
%   legendCell        Cell array of strings listing names of the different data
%                     sets we are considering, e.g., 
%                     {'on source','off source','injections'}
%   figures_dirName   String, path of output dir for figures where figures will
%                     be saved
%   figfiles_dirName  String, path of output dir for figures where figfiles will
%                     be saved
%   plotTag           String used to name output plots
%   loudest1          Double, property1 of loudest trigger. This will be added to
%                     the scatter plots.
%   loudest2          Double, property2 of loudest trigger. This will be added to
%                     the scatter plots.
%   loudestLegend     String describing loudest trigger.
%   plotType          Optional string determining type of axes used, can be:
%                     'loglog','semilogx','semilogy','linear'. Defaults to
%                     'linear';
%   
%   $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(12, 13, nargin));

% ---- If plotType is not specified, set it to 'linear'
if nargin == 12
    plotType = 'linear';
end

% ---- Choose which plotting routine we will use.
switch plotType
    case 'loglog',
        plotter = @loglog;
    case 'semilogx',
        plotter = @semilogx;
    case 'semilogy',
        plotter = @semilogy;
    case 'linear',
        plotter = @plot;
    otherwise
        error('plotType must be one of loglog,semilogx,semilogy,linear')
end % -- switch on plotType 

% ---- Check trigProperty1 and trigProperty2 have the same number of cells
if length(trigProperty1) ~= length(trigProperty2)
    error('trigProperty1 and trigProperty2 must have the same number of cells');
end

% ---- Create figure.
figure; set(gca,'FontSize',20);
color = {'k','r','b','g','c'};

% ---- Make scatter plots
plotter(trigProperty1{1},trigProperty2{1},[color{1} '+'],'MarkerSize',15);
hold on;
for idx = 1:length(trigProperty1)-1
    plotter(trigProperty1{idx+1},trigProperty2{idx+1},...
        [color{idx+1} '+'],'MarkerSize',15);
end
plotter(loudest1,loudest2,'p','MarkerEdgeColor','m','LineWidth',2,'MarkerSize',15)
grid on;

% ---- Add total number of triggers plotted to legend
for idx = 1:length(legendCell)
    legendCell{idx} = [legendCell{idx} ' ' ...
        num2str(length(trigProperty1{idx}))];
end

legendCell{length(legendCell)+1} = loudestLegend;

legend(legendCell);
title([trigPropertyName2 ' vs ' trigPropertyName1]);
xlabel(trigPropertyName1);
ylabel(trigPropertyName2);

% ---- Construct name of plot from input args
plotName = 'scatter_';
%for idx = length(legendCell)
%    plotName = [plotName legendCell{idx} '_'];
%end
plotName = [plotName trigPropertyName1 '_' trigPropertyName2 '_' plotTag];
% ---- Remove any whitespace
spaceIdx=isspace(plotName);
plotName(spaceIdx)='';

% ---- Save figure.
xsavefigure(plotName,figures_dirName,figfiles_dirName);
