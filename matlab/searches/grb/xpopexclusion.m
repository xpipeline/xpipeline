function [popEff popEffFF popEffDist popEffBino distribExcl distribExclBino]=...
    xpopexclusion(fout,figures_dirName,figfiles_dirName,...
                  nominalDist,nameCell,parametersCell,...
                  injScale,effCurve,injScaleFF,effCurveFF,grbZdistribFile)

nWave = size(injScale,2);
nGRBs = size(injScale,1);
% compute volume efficiency for a bunch of radii
for iWave = 1:nWave
  % choose the volume radii on which to perfomr the exclusion
  volumeRadius = nominalDist(iWave)*10.^[-1:0.1:2.5];
  % preallocate output variables
  popEff(iWave,:,:) = zeros(length(volumeRadius)+1,nGRBs+2);
  popEffFF(iWave,:,:) = zeros(length(volumeRadius)+1,nGRBs+2);
  popEffDist(iWave,:,:) = zeros(length(volumeRadius)+1,nGRBs+2);
  for iRadius = 1:length(volumeRadius)
    for iGRB = 1:nGRBs
      try
      % compute volume marginalized efficiency curve
      volEff(iGRB,iWave,iRadius) = ...
          volumeefficiency(squeeze(injScale(iGRB,iWave,:)),...
                           squeeze(effCurve(iGRB,iWave,:)),...
                           nominalDist(iWave),volumeRadius(iRadius));
      % compute volume marginalized efficiency curve for flare fit curve
      volEffFF(iGRB,iWave,iRadius) = ...
          volumeefficiency(squeeze(injScaleFF(iGRB,iWave,:)),...
                           squeeze(effCurveFF(iGRB,iWave,:)),...
                           nominalDist(iWave),volumeRadius(iRadius));
      catch
        disp(['iGRB: ' num2str(iGRB) ', iWave: ' num2str(iWave) ...
              ', iRadius: ' num2str(iRadius) ', nominalDist: ' ...
             num2str(nominalDist(iWave)) ', volumeRadius: ' num2str(volumeRadius(iRadius))])
      end
      % compute efficiency curve at fixed distance
      if nominalDist(iWave)/volumeRadius(iRadius) < ...
            min(squeeze(injScale(iGRB,iWave,:))')
        % if fixed distance larger than all injection distances set
        % efficiency to zero
        distEff(iGRB,iWave,iRadius) = 0;
      elseif nominalDist(iWave)/volumeRadius(iRadius) > ...
            max(squeeze(injScale(iGRB,iWave,:))')
        % if fixed distance smaller than all injection distances set
        % efficiency to the maximal one
        distEff(iGRB,iWave,iRadius) = max(squeeze(effCurve(iGRB,iWave,:))');
      else
        % interpolate efficiency curve to obtain efficency at requested distance
        addGrid = 2e-2:1e-2:0.9;
        tmpInjScale = squeeze(injScale(iGRB,iWave,:));
        tmpEffCurve = squeeze(effCurve(iGRB,iWave,:));
        for iPoint = 1:length(addGrid)
          addInjScale(iPoint) = upperbound(tmpInjScale,tmpEffCurve, ...
                                                addGrid(iPoint));
        end
        tmpInjScale = [tmpInjScale; addInjScale'];
        tmpEffCurve = [tmpEffCurve; addGrid'];
        [tmpInjScale iSort] = sort(tmpInjScale);
        tmpEffCurve = tmpEffCurve(iSort);
        [tmpInjScale iU] = unique(tmpInjScale,'first');
        tmpEffCurve = tmpEffCurve(iU);

        distEff(iGRB,iWave,iRadius) = ...
            interp1(tmpInjScale,...
                    tmpEffCurve,...
                    nominalDist(iWave)/volumeRadius(iRadius));
      end
    end
    % compute population exclusion at given radii and fraction of
    % analyzed GRBs with significant GW emission
    popEff(iWave,iRadius,1:nGRBs+1) = 1 - ...
        poly(diag(-(1-volEff(:,iWave,iRadius)))) ./ poly(diag(-ones(nGRBs,1)));
    popEffFF(iWave,iRadius,1:nGRBs+1) = 1 - ...
        poly(diag(-(1-volEffFF(:,iWave,iRadius)))) ./ poly(diag(-ones(nGRBs,1)));
    popEffDist(iWave,iRadius,1:nGRBs+1) = 1 - ...
        poly(diag(-(1-distEff(:,iWave,iRadius)))) ./ poly(diag(-ones(nGRBs,1)));
    % marginalize over binomial distribution of nearby GRBs in the
    % analyzed sample
    popFraction = [0 logspace(-3,0,100)];
    for iFrac = 1:length(popFraction)
      popEffBino(iWave,iRadius,iFrac) = sum(binopdf(0:nGRBs,nGRBs,popFraction(iFrac))'.*...
                                            squeeze(popEff(iWave,iRadius,1:nGRBs+1)));
    end

    % find 90% exclusion line in the radii vs fraction plane, both before
    % and after binomial distribution of GW emitting GRBs marginalization
    fraction = squeeze((min(find(popEff(iWave,iRadius,:)>=0.9))-1)/nGRBs);
    fractionBino =  popFraction(squeeze((min(find(popEffBino(iWave,iRadius,:)>=0.9))-1)));
    if isempty(fraction)
      fraction = nan;
    end
    if isempty(fractionBino)
      fractionBino = nan;
    end
    distribExcl(iWave,iRadius) = fraction;
    distribExclBino(iWave,iRadius) = fractionBino;
  end

  % plot pop detection CL exclusion map for uniform in given volume exclusion
  figure; hold on
  set(gca,'FontSize',20)
  pcolor(((-1:nGRBs)+0.5)/nGRBs,[0 volumeRadius]*1.0593,squeeze(popEff(iWave,:,:)))
  shading flat
  set(gca,'yscale','log');ylim([0.1 1e3])
  colorbar;grid;
  title([nameCell{iWave} parametersCell{iWave}]);
  xlabel('fraction GRB closeby')
  ylabel('distance (Mpc)')
  % ---- Save plot.
  plotName{iWave} = ['/popexcl_' nameCell{iWave} parametersCell{iWave}];
  xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);

  % plot pop detection CL exclusion map for uniform in given volume and
  % flare fitting exclusion
  figure; hold on
  set(gca,'FontSize',20)
  pcolor(((-1:nGRBs)+0.5)/nGRBs,[0 volumeRadius]*1.0593,squeeze(popEffFF(iWave,:,:)))
  shading flat
  set(gca,'yscale','log');ylim([0.1 1e3])
  colorbar;grid;
  title([nameCell{iWave} parametersCell{iWave}]);
  xlabel('fraction GRB closeby')
  ylabel('distance (Mpc)')
  % ---- Save plot.
  plotNameFF{iWave} = ['/popexclFF_' nameCell{iWave} parametersCell{iWave}];
  xsavefigure(plotNameFF{iWave},figures_dirName,figfiles_dirName);

  % plot pop detection CL exclusion map for uniform in given volume and
  % binomial distribution marginalized exclusion
  figure; hold on
  set(gca,'FontSize',20)
  pcolor(popFraction,[volumeRadius],squeeze(popEffBino(iWave,:,:)))
  shading flat
  set(gca,'yscale','log');ylim([0.1 1e3]);xlim([1e-2 1])
  colorbar;grid;
  title([nameCell{iWave} parametersCell{iWave}]);
  xlabel('fraction GRB closeby')
  ylabel('distance (Mpc)')
  % ---- Save plot.
  plotNameBino{iWave} = ['/popexclBino_' nameCell{iWave} parametersCell{iWave}];
  xsavefigure(plotNameBino{iWave},figures_dirName,figfiles_dirName);

  % plot pop detection CL exclusion map for fixed distance exclusion
  % (this the true unconstrained distribution exclusion)
  figure; hold on
  set(gca,'FontSize',20)
  pcolor(((-1:nGRBs)+0.5)/nGRBs,[0 volumeRadius]*1.0593,squeeze(popEffDist(iWave,:,:)))
  shading flat
  set(gca,'yscale','log');ylim([0.1 1e3])
  colorbar;grid;
  title([nameCell{iWave} parametersCell{iWave}]);
  xlabel('fraction GRB closeby')
  ylabel('fixed distance (Mpc)')
  % ---- Save plot.
  plotNameDist{iWave} = ['/popexclDist_' nameCell{iWave} parametersCell{iWave}];
  xsavefigure(plotNameDist{iWave},figures_dirName,figfiles_dirName);

  % plot the 90% exclusion assuming uniform in volume
  % distribution up to given fraction vs radius point
  figure; hold on
  set(gca,'FontSize',20)
  plot(volumeRadius,distribExcl(iWave,:),'k-o','linewidth',2,'MarkerFaceColor','k');
  set(gca,'xscale','log');  set(gca,'yscale','log');
  xlim([0.1 1e5])
  grid;
  title([nameCell{iWave} parametersCell{iWave}]);
  ylabel('Cumulative distribution exclusion')
  xlabel('distance (Mpc)')
  % ---- Save plot.
  plotNameDexcl{iWave} = ['/distribExcl_' nameCell{iWave} parametersCell{iWave}];
  xsavefigure(plotNameDexcl{iWave},figures_dirName,figfiles_dirName);

  % plot the 90% exclusion assuming uniform in volume distribution up to
  % given fraction vs radius point. This exclusion is marginalized over
  % binomial distribution of GW emitting GRBs
  figure; hold on
  set(gca,'FontSize',20)
  plot(volumeRadius,distribExclBino(iWave,:),'k-o','linewidth',2,'MarkerFaceColor','k');
  set(gca,'xscale','log');  set(gca,'yscale','log');
  xlim([0.1 1e5])
  grid;
  title([nameCell{iWave} parametersCell{iWave}]);
  ylabel('Cumulative distribution exclusion')
  xlabel('distance (Mpc)')
  % ---- Save plot.
  plotNameDexclBino{iWave} = ['/distribExclBino_' nameCell{iWave} parametersCell{iWave}];
  xsavefigure(plotNameDexclBino{iWave},figures_dirName,figfiles_dirName);

  % if redshift distributino provided plot the uniform in volume
  % exclusion (both with and without binomial marginalization) in terms
  % of redshift along with the observed GRB redshift distribution
  if not(isempty(grbZdistribFile))
    grbZ = load(grbZdistribFile);
    grbZ = sort(grbZ);
    figure; hold on
    set(gca,'FontSize',20)
    plot(dist2z(volumeRadius,'linear'),distribExcl(iWave,:),'k-','linewidth',2);
    plot(dist2z(volumeRadius,'linear'),distribExclBino(iWave,:),'m-','linewidth',2);
    plot(dist2z(10*volumeRadius,'linear'),distribExclBino(iWave,:),'b-','linewidth',2);
    plot(dist2z(100*volumeRadius,'linear'),distribExclBino(iWave,:),'g-','linewidth',2);
    stairs([1e-10 grbZ'],0:1/length(grbZ):1,'r','linewidth',2)
    set(gca,'xscale','log');  set(gca,'yscale','log');
    xlim([1e-5 10])
    grid;
    title([nameCell{iWave} parametersCell{iWave}]);
    legend('Unmarginalized','Exclusion','Adv det','ET','measured')
    ylabel('Cumulative distribution exclusion')
    xlabel('redshift')
    % ---- Save plot.
    plotNameZ{iWave} = ['/zExcl_' nameCell{iWave} parametersCell{iWave}];
    xsavefigure(plotNameZ{iWave},figures_dirName,figfiles_dirName);

  end
  

end
% ---- Put all plots into a table.
fprintf(fout,'%s\n',['<h2>  Population exclusion using UL-box efficiency ' ...
                    'curves </h2>']);
xaddfiguretable(fout,plotName,figures_dirName);
fprintf(fout,'%s\n',['<h2>  Population exclusion using UL-box efficiency, ' ...
                    'marginalized over binomial nearby GRB distribution</h2>']);
xaddfiguretable(fout,plotNameBino,figures_dirName);
fprintf(fout,'%s\n',['<h2>  Population exclusion using FlareFit curves</h2>']);
xaddfiguretable(fout,plotNameFF,figures_dirName);
fprintf(fout,'%s\n',['<h2>  Fixed distance population exclusion </h2>']);
xaddfiguretable(fout,plotNameDist,figures_dirName);
fprintf(fout,'%s\n',['<h2>  Distance distribution exclusion </h2>']);
xaddfiguretable(fout,plotNameDexcl,figures_dirName);
fprintf(fout,'%s\n',['<h2>  Distance distribution exclusion, binomial ' ...
                    'distribution with fraction close-by</h2>']);
xaddfiguretable(fout,plotNameDexclBino,figures_dirName);
if not(isempty(grbZdistribFile))
  fprintf(fout,'%s\n',['<h2>  Z distribution exclusion </h2>']);
  xaddfiguretable(fout,plotNameZ,figures_dirName);
end

