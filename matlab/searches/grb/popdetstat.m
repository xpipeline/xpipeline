function detStat = popdetstat(p,method,detSens)
% POPDETSTAT - compute population detection statistics
%
% detStat = popdetstat(p,method,detSens)
%
% p         Array. P-values to test. Assumed dimension order
%           is GRB number, waveform number, other stuff. That is the test
%           is performed for each available column.
% method    String. Name of the population detection statistic to
%           use. See code for details.
% detSens   Array. Contains the typical sensitivity for each analysis in terms
%           of relative hrss (or injection scale). Assumed dimension order
%           is GRB number, waveform number, other
%           stuff. Necessary only for somu population detection statistics.
%
% detStat   Array. Value of the population detection statistic for each
%           column in p.
%

nGRBs = size(p,1);
nWave = size(p,2);

% ---- duplicate the sensitivities volumes for furthe computation if needed
if strcmp(method,'weightbinomial') || ...
      not(isempty(regexp(method,'prodbinomial'))) || ...
      not(isempty(regexp(method,'pseudobinomial'))) || ...
      not(isempty(regexp(method,'method'))) 
  if nargin < 3
    error(['detSens array is required to compute statistic: ' method]);
  end
  sizeP = size(p);
  sizeP(1:2) = 1;
  detSens = repmat(detSens,sizeP);
  % average the relative sensitive volumes to best case for all waveforms
  if strcmp(method,'weightbinomial') || not(isempty(regexp(method,'prodbinomial')))
    detSens = repmat((sum((detSens).^(-3),2)/nWave).^(-1/3),[1 nWave]);
  end
end

% ---- Compute the population detection statistic
switch method
 case 'method1' 
  % Method initially proposed by Nick Fotopoulos that uses volume/FAP
   detStat = squeeze(sum(detSens.^(-3)./p) ./ sum(detSens.^(-3))/nGRBs);
%detStat = squeeze(sum(1./p) /nGRBs);
 case 'method2' 
  % method obtained from a likelihood ratio derviation assuming a
  % quadratically growing detection statistic for signals and powerlaw
  % background distribution with some reasonable powerlaw slope that I don't remember
  detStat = squeeze(prod(1+1e-3*(2/11*p.^(-8/11)-14/11*p.^(3/11))) );
% detStat = squeeze(sum((2/11*p.^(-8/11)-14/11*p.^(3/11))) );
 case 'method3'
  % same as method2 but including the sensitive volumes
  detStat = squeeze(prod(1+1e-3*detSens.^(-3).*(2/11*p.^(-8/11)-14/11*p.^(3/11))) );
 case 'pseudobinomial'
  %   don't remember
  detStat = squeeze(sum((p<1e-2)./detSens.^3).*sum(detSens.^3)/nGRBs);
%detStat = squeeze(sum((p<1e-2)));
 case 'pseudobinomial25'
  %   don't remember
  detStat = squeeze(sum((p<9e-2)./detSens.^3).*sum(detSens.^3)/nGRBs);
%detStat = squeeze(sum((p<9e-2)));
 case {'binomial','binomial25','binomial100'}
  % binomial test with different top fractions
  switch method
   case 'binomial'
    Ptail = 0.05; %-- test top 5%.
   case 'binomial25'
    Ptail = 0.25; %-- test top 25%.
   case 'binomial100'
    Ptail = 1; %-- test top 100%.
  end
  Ntail = ceil(Ptail*nGRBs);
  localProb = sort(p,1);
  sizeP = size(p);
  sizeP(1) = 1;
  detStat = inf*ones(sizeP);
  for iTail = 1:Ntail
    detStat = min(1-binocdf((iTail-1)*ones(sizeP),nGRBs,reshape(localProb(iTail,:),sizeP)),...
                  detStat);
  end
  detStat = -squeeze(log(detStat));

 case {'prodbinomial','prodbinomial02','prodbinomial25'}
  % sensitive volume weighted binomial test with different top fractions
  switch method
   case 'prodbinomial'
    Ptail = 0.05; %-- test top 5%.
   case 'prodbinomial02'
    Ptail = 0.02; %-- test top 2%.
   case 'prodbinomial25'
    Ptail = 0.25; %-- test top 25%.
  end
  Ntail = ceil(Ptail*nGRBs);
  p = p.*detSens.^3;
  [localProb localI]= sort(p,1);
  detSens = detSens(localI);
  sizeP = size(p);
  sizeP(1) = 1;
  sizeEnd = size(p);
  sizeEnd(1) = Ntail;
  for itail = 1:Ntail
    binom(itail,1) = nchoosek(nGRBs,itail);
  end
  binom = repmat(binom,sizeP);
  detStat = -squeeze(log(min(binom.*...
                             cumprod(reshape(localProb(1:Ntail,:),sizeEnd)),[],1)));


 case 'weightbinomial'
  % an older version of the sensitive volume weithed binomial test
  Ptail = 0.05; %-- test top 5%.
  Ntail = ceil(Ptail*nGRBs);
  [localProb localI]= sort(p,1);
  detSens = detSens(localI);
  sizeP = size(p);
  sizeP(1) = 1;
  sizeEnd = size(p);
  sizeEnd(1) = Ntail;
  detStat = -squeeze(log(min(...
       cumprod(reshape(detSens(1:Ntail,:).^(3),sizeEnd)).* ...
      (1 - binocdf(repmat([0:Ntail-1]',sizeP), ...
                  nGRBs,reshape(localProb(1:Ntail,:),sizeEnd)))...
      )));

 case 'binomialSingle'
  % look only at the single smallest p-value. The general implementation
  % of the binomial test doesn't work for Ntail = 1 because of multi-D
  % matrix squeezing
  Ntail = 1;
  localProb = sort(p,1);
  sizeP = size(p);
  sizeP(1) = 1;
  sizeEnd = size(p);
  sizeEnd(1) = Ntail;
  detStat = -squeeze(log((1 - binocdf(repmat(0,sizeP), ...
                                      nGRBs,reshape(localProb(1,:),sizeEnd)))));

  case 'utest'
   % Mann-Whitney U test, p-value distribution compared to uniform
   % p-value distribution with a 10 times larger sample
   sizeP = size(p);
   sizeP(1) = sizeP(1)*10;
   addp = rand(sizeP);
   p = [p;addp];
   [tmp order]=sort(p);
   clear tmp
   [tmp rank]=sort(order);
   clear tmp order
   R1=sum(rank(1:nGRBs,:));
   R2=sum(rank(nGRBs+1:end,:));
   N1 = nGRBs; N2 = nGRBs*10;
   N = N1 + N2;
   check = R1 + R2 - N*(N+1)/2;
   detStat=N*(N+1)/2-min(R1 - N1*(N1+1)/2,R2-N2*(N2+1)/2);
   sizeP(1) = 1;
   detStat = squeeze(reshape(detStat,sizeP));


 otherwise
  error(['Population detection method: ' method ...
         ' is not recognized'])
end



