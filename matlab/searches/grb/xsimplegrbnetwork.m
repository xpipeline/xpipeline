function [] = xsimplegrbnetwork(grbFile,ifos,segFiles)
% XSIMPLEGRBNETWORK - Determine which detectors are on for list of GRBs.
%
% XSIMPLEGRBNETWORK is a simplified version of xgrbnetwork.  This function
% reads a list of grbs and segment lists from files, and determines which
% ifos were on for a full 256 sec interval centered on each GRB time. The
% function then outputs one file for each network coincidence combination,
% each file listing the GRBs for that coincidence combination.
%
% usage:
% 
%   xsimplegrbnetwork(grbFile,ifos,segFiles)
%
%   grbFile   Name of ASCII file containing GRB info; see below.
%   ifos      Cell array containing list of IFOs to be considered; e.g.,
%             {'H2','G1','V1'}.
%   segFiles  Cell array naming the segment files for each of the detectors
%             listed in ifos.  Order must match that in ifos.  Files should
%             be in segwizard format.  
%    
% The GRB input file should be in the format used by the "validated GRB
% list" files available from 
% http://www.uoregon.edu/~ileonor/ligo/s5/grb/online/S5grbs_list_valid.html
%
% This function also handles the case of a single IFO.
%
% example:
%
%   xgrbnetwork('currentgrbs.txt', {'H2','G1','V1'}, ... 
%      {'H2_cat1.txt','G1_cat1.txt','V1_cat1.txt'});


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   MAGIC NUMBERS.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Range of time of interest.  For the moment, do no down-selection.
t0 = 0;    
t1 = Inf;

% ---- Minimum required segment duration.
min_dur = 256;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check lists of IFOs and segments.
numberIfos = length(ifos);
disp(['number of ifos = ' num2str(numberIfos)]);
numSegs    = length(segFiles);
if not(numberIfos==numSegs)
    error('Error: number of segFiles must equal number of ifos specified')
end

% ---- Read GRB list.
%      Assume format of Isabel's ASCII files; see e.g. 
%        http://www.uoregon.edu/~ileonor/ligo/s5/grb/online/S5grbs_list_valid.html    
%      Note that we read in the RA and dec as strings in case they contain
%      'n/a'; these GRBs will be discarded.
fid = fopen(grbFile,'r');
GRB = textscan(fid,'%d%s%s%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s');
fclose(fid);
numberGRBs = length(GRB{1});
disp(['Read info on ' num2str(numberGRBs) ' GRBs.']);
% ---- GPS times of GRBs.
gps = GRB{4};

% ---- Load segment lists, compute complements.
for jj = 1:numberIfos 
    [segno start stop dur] = textread(segFiles{jj},'%d %d %d %d','commentstyle','shell');
	% ---- Drop segments less than min_dur long.
	k = find(dur < min_dur);
	start(k) = []; segno(k) = []; stop(k) = []; dur(k) = [];
	% ---- Drop segments outside interval of interest.
	k = find(start+dur < t0);
	start(k) = []; segno(k) = []; stop(k) = []; dur(k) = [];
	k = find(start < t0);
	start(k) = t0;
	k = find(start > t1);
	start(k) = []; segno(k) = []; stop(k) = []; dur(k) = [];
	k = find(start + dur > t1);
	dur(k) = t1 - start(k);
    % ---- Record segment list and compute complement of segment list.
    seg{1,jj} = [start dur];
    seg{2,jj} = ComplementSegmentList(start,dur,t0,t1);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Compare GRB times to segment lists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Compare GRB times to segment lists.
grb_mask = zeros(numberGRBs,numberIfos);
for iIfo = 1:numberIfos 
    C = Coincidence2(seg{1,iIfo}(:,1),seg{1,iIfo}(:,2),gps-min_dur/2, ...
        min_dur*ones(size(gps)));
    % ---- Drop coincidences that do not cover full interval 
    %      [gps-min_dur/2,gps+min_dur/2].
    k = find(C(:,2)<min_dur);
    C(k,:) = [];
    % ---- Mark GRBs that occur while this Ifo is on.
    grb_mask(C(:,end),iIfo) = 1;
end

% ---- Append IFO name to coincidenceString for each GRB.
coincidenceString = cell(numberGRBs,1);
for iGRB = 1:numberGRBs 
    coincidenceString{iGRB} = '';
    for iIfo = 1:numberIfos 
        if (grb_mask(iGRB,iIfo)==1)
            coincidenceString{iGRB} = [coincidenceString{iGRB}, ifos{iIfo}];
        end
    end
end

% ---- Array used to record number of GRBs in single, double, triple, etc.
%      times.  Can handle any number of detectors.
numGRB = zeros(numberIfos,1);
% maxNumGRBCoincComb = 0;
% for ii = 2:numberIfos
%     maxNumGRBCoincComb = maxNumGRBCoincComb + nchoosek(numberIfos,ii);
% end
% if (maxNumGRBCoincComb > 0)
%     numGRB = zeros(maxNumGRBCoincComb,1);
% end

% ---- Write GRB lists to files by coincidence combination.
coincidenceComb = unique(coincidenceString);
numGRBCoincComb = zeros(length(coincidenceComb));
for ii = 1:length(coincidenceComb)

    if (~isempty(coincidenceComb{ii}))

        % ---- Find all GRBs in this coincidence combination.
        numGRBCoincComb(ii) = sum(strcmp(coincidenceString,coincidenceComb{ii}));
        disp(['Network ' coincidenceComb{ii} ': Contains ' ...
            num2str(numGRBCoincComb(ii)) ' GRBs']);

        % if (maxNumGRBCoincComb > 0)
        %     % ---- Number of IFOs in this coincidence type.
        %     numIfos = length(coincidenceComb{ii})/2;
        %     % ---- Populate GRB statistics array.
        %     numGRB(numIfos) = numGRB(numIfos) + numGRBCoincComb(ii);
        % end
        % ---- Number of IFOs in this coincidence type.
        numIfos = length(coincidenceComb{ii})/2;
        % ---- Populate GRB statistics array.
        numGRB(numIfos) = numGRB(numIfos) + numGRBCoincComb(ii);

        % ---- Write file listing these GRBs.
        filename = ['grbs_' coincidenceComb{ii} '.txt'];
        fid = fopen(filename,'w');
        for iGRB = find(strcmp(coincidenceString,coincidenceComb{ii})).';
            if or((~strcmp(GRB{5}{iGRB},'n/a')),(~strcmp(GRB{6}{iGRB},'n/a')))
                fprintf(fid,'%s ',GRB{2}{iGRB});
                fprintf(fid,'%13.3f %g %g\n',[GRB{4}(iGRB,:), ...
                    str2double(GRB{5}(iGRB)), str2double(GRB{6}(iGRB))]);
            else
                disp(['GRB ' GRB{2}{iGRB} ' had n/a for RA or Dec, ' ...
                    'removing it from list']) 
                numGRBCoincComb(ii) = numGRBCoincComb(ii) - 1;
            end 
        end
        disp(['Network ' coincidenceComb{ii} ': Writing ' ...
            num2str(numGRBCoincComb(ii)) ' GRBs to ' filename]);
        disp(' ');

    end
end

% ---- Display coincidence combination statistics.
disp('GRB network statistics:');
disp(['Percentages are fractions of: ' num2str(numberGRBs) ]);
for idx = 1:length(numGRB)
    disp(['Number of GRBs in ' num2str(idx) ' ifo times: ' ...
        num2str(numGRB(idx)) ' (' num2str(100 * numGRB(idx)/numberGRBs) ...
        '%)'  ]);
end
disp(['Total number of GRBs in at least one ifo: ' num2str(sum(numGRB)) ...
    ' (' num2str(100 * sum(numGRB)/numberGRBs) '%)' ]);

% ---- Done.
return;
