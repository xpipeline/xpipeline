function [UL90p, ULdiff, grbName] = xbatchwebsummary(openFileList, ...
                                                  ulFileList,user_tag,popDetStr,grbZdistribFile)
% XBATCHWEBSUMMARY - create a report for a batch of GRB analysis
%
% xbatchwebsummary(openFileList,ulFileList,user_tag,grbZdistribFile)
%
% openFileList   String. Name of file containting the list of open-box
%                result files to process. Better if those located in a
%                web accessible path. Should be produced by xbatchwebsummary.py
% ulFileList     String. Name of file containting the list of ul-box
%                result files to process. Better if those located in a
%                web accessible path. Should be produced by xbatchwebsummary.py
% user_tag       String. Arbitrary tag that will be included in the name
%                of all produced files.
% popDetStr      String. Tilde delimited string containing the names of 
%                population detection statistic to use. [OPTIONAL]
% grbZdistribFile  String. Name of file containing a distribution of
%                  observed external trigger redshifts. The text file format
%                  must a single column containing the different
%                  redshifts. [OPTIONAL]

% MAGIC numbers
nInj = 600; % number of injections done per wf/scale (this should be read
            % from file)
format compact

% If no population detection statistic provided set to empty
if nargin < 4
  popDetStr = '';
end

% read files listing result mat files
fid = fopen(openFileList);
openFileCell = textscan(fid,'%s');
openFileCell = openFileCell{1};
fclose(fid);
fid = fopen(ulFileList);
ulFileCell = textscan(fid,'%s');
ulFileCell = ulFileCell{1};
fclose(fid);

% check for consistency between the two file lists
if length(ulFileCell) ~= length(openFileCell)
  error(['The list of files in: ' openFileList ' and in ' ulFileList ...
         ' do not have the same length'])
else
  nGRBs = length(openFileCell);
end

% ---- write web page header, start by setting fake names to reuse header from xmakegrbwebpage
analysis.grb_name = 'multipleGRBs';
analysis.type = 'batch';
analysis.svnversion_xdetection = 'NOT RELEVANT';
% ---- Try and get svnversion of postprocessing using 
%      report-svnversion-grbwebpage.sh, this bash script is created
%      when running make install-grbwebpage and is copied to the INSTDIR
%      which should be in the users path
[status,result] = ...
    system('sh $XPIPE_INSTALL_BIN/report-svnversion-grbwebpage.sh');
if status==0
    analysis.svnversion_grbwebpage = result;
else
    warning('report-svnversion-grbwebpage.sh has failed');
    disp(status);
    disp(result);
    warning('setting analysis.svnversion_grbwebpage to Unknown');
    analysis.svnversion_grbwebpage = 'Unknown';
end
[fout,figures_dirName,figfiles_dirName] = xwritewebpageheader(user_tag,analysis);
[fbkg,figures_dirName,figfiles_dirName] = ...
    xwritewebpageheader([user_tag '_bkg'],analysis);

% ---- Name of output web page.
webPageFileName = [analysis.grb_name '_' user_tag '_' ...
    analysis.type '.shtml'];

clear analysis

% ---- collect/plot efficiency curve and bkg information from each GRBs
disp(['Will process ' num2str(nGRBs) ' external trigger']);
for iGRB = 1:nGRBs
  % load results for this GRB
  openRes=load(openFileCell{iGRB},'analysis','UL90p','UL50p','nameCell', ...
               'parametersCell','onSource','offSourceSelectedBeforeDQ', ...
               'efficiencySorted','totSNR');
  ulRes=load(ulFileCell{iGRB},'analysis','UL90p','UL50p','efficiencySorted', ...
             'injectionScaleSorted','nameCell','parametersCell','totSNR','SNRlist');
  % find root of the output file name, will be used to put links to the
  % result web page
  rootOpenResName = openFileCell{iGRB}(1:end-4);
  rootULResName = ulFileCell{iGRB}(1:end-4);
  % construct closed-box results file name and extract the detection sensitivity
  closedFileName = regexprep(ulFileCell{iGRB},'ulbox','closedbox');
  closedRes=load(closedFileName,'UL50p');
  detSensCL99(iGRB,:) = closedRes.UL50p;
  % check that the names are consistent for both results files
  if not(strcmp(openRes.analysis.grb_name,ulRes.analysis.grb_name))
    error(['The names of the grb between open and ul results do not ' ...
           'correspond:'  openRes.analysis.grb_name ' ' ...
           ulRes.analysis.grb_name])
  else
    grbName{iGRB} = openRes.analysis.grb_name;
    disp(['Processing ********* ' grbName{iGRB} ' ********** ']);
  end
  % write web section header with links to result web pages
  fprintf(fout,'%s\n',['<h2> ' grbName{iGRB} ' <A HREF="' rootOpenResName ...
                      '.shtml">OPEN-BOX</A> <A HREF="' rootULResName ...
                      '.shtml">UL-BOX</A></h2>']);
  fprintf(fbkg,'%s\n',['<h2> ' grbName{iGRB} ' <A HREF="' rootULResName ...
                      '.shtml">UL-BOX</A></h2>']);
  % record relative difference between openbox and ulbox
  ULdiff(iGRB,:) = 200*(ulRes.UL90p-openRes.UL90p)./(ulRes.UL90p+openRes.UL90p);
  UL50diff(iGRB,:) = 200*(ulRes.UL50p-openRes.UL50p)./(ulRes.UL50p+openRes.UL50p);
  % record 50% efficiency SNRs
  ulSNR(iGRB,:) = ulRes.totSNR(:,end);
  openSNR(iGRB,:) = openRes.totSNR(:,end);
  ulSNRlist{iGRB} = ulRes.SNRlist;
  % keep the ulbox hrss, this will be the final figure used
  UL90p(iGRB,:) = ulRes.UL90p;
  % record efficiency curves
  effCurve(iGRB,:,:) = ulRes.efficiencySorted;
  injScale(iGRB,:,:) = ulRes.injectionScaleSorted;
  % record analysis parameters
  analysis{iGRB} = openRes.analysis;
  nameCell{iGRB} = openRes.nameCell;
  parametersCell{iGRB} = openRes.parametersCell;
  % check for consistency of injection parameters between different GRBs
  if iGRB > 1 &&  ( any(not(strcmp(nameCell{iGRB-1},nameCell{iGRB}))) ...
                    || any(not(strcmp(parametersCell{iGRB-1},parametersCell{iGRB}))) )
    error(['Injections are not the same between GRB: ' num2str(iGRB) ...
           ' and the previous one. Problem with ordering according to frequency?']);
  end
  % record on source events and the per GRB probability of the on source window
  onSource{iGRB} = openRes.onSource;
  if isempty(min(openRes.onSource.probability(logical(openRes.onSource.pass))))
    % KLUDGE set bkg probability to 1 if no survivig events found
    pOnSource(iGRB) = 1;
  else
    pOnSource(iGRB) = min(openRes.onSource.probability(logical(openRes.onSource.pass)));
  end
  % record number of off source trials
  numULjobs(iGRB) = length(unique(openRes.offSourceSelectedBeforeDQ.jobNumber));
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % --- read background distribution curve and add fit
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % read background distribution from figure
  open([rootULResName '_figfiles/hist_offLoudest_connected.fig']);
  [X Y]=datafromplot(gca);
  sigBin{iGRB}=X{1};
  bkgHist{iGRB}=Y{1};
  % use only the highest 50% of curve for the fit
  maskProbBelow50p = bkgHist{iGRB}/numULjobs(iGRB) <= 0.5;
  goodBins{iGRB} = maskProbBelow50p & bkgHist{iGRB} > 0;
  bkgFit(iGRB,:) = polyfit(log(sigBin{iGRB}(goodBins{iGRB})),...
                           log(bkgHist{iGRB}(goodBins{iGRB})),1);
  % plot background distribution and fit to it
  figure; hold on;
  set(gca,'FontSize',20);
  plot(sigBin{iGRB}, bkgHist{iGRB},'k','linewidth',2);
  plot(sigBin{iGRB}(goodBins{iGRB}),exp(log(sigBin{iGRB}(goodBins{iGRB}))*bkgFit(iGRB,1)+...
                                        bkgFit(iGRB,2)),'m');
  set(gca,'xscale','log');set(gca,'yscale','log');
  xlim([1 1e3]);ylim([1 1e4]);
  xlabel('significance');grid; 
  hold off
  % ---- Save plot.
  bkgPlotName = ['/bkgHist_' grbName{iGRB} ];
  xsavefigure(bkgPlotName,figures_dirName,figfiles_dirName);
  % ---- Put all plots into a table.
  xaddfiguretable(fbkg,bkgPlotName,figures_dirName);  
  fprintf(fbkg,'alpha=%f, level=%f <br>\n', bkgFit(iGRB,:));

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % --- plot both efficiency curves for all waveforms
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for iWave = 1:length(ulRes.nameCell)
    % perform flare fitting to the efficiency curve
    fitoptions.forceIsDiscrete = 1;
    fitoptions.useHighModel = true;
    flareHrss = 1e-21*repmat(squeeze(ulRes.injectionScaleSorted(iWave,:)),...
                        [1 nInj]);
    refEff = repmat(1/nInj:1/nInj:1,[size(effCurve,3) 1]);
    flareDetection = repmat(squeeze(ulRes.efficiencySorted(iWave,:)),...
                        [1 nInj]) > refEff(:)';

    [fit90,fit50,ci,minGF,xFF,yFF]= FlareFit('SGL',...
                                             flareHrss,flareDetection,fitoptions,'DummyName');
    % record efficiency curves
    injScaleFF(iGRB,iWave,:) = 1e21*xFF;
    effCurveFF(iGRB,iWave,:) = max(yFF,0);
    UL90pFF(iGRB,iWave) = upperbound(1e21*xFF,yFF,0.9);
    if(yFF(1)>1e-3)
      warning(['FlareFit efficiency curve do not start at zero but at ' ...
               num2str(yFF(1)')])
    end
    % plot the efficiency curve and the fit to it
    figure; hold on;
    set(gca,'FontSize',20)
    X = ulRes.injectionScaleSorted(iWave,:);
    Yopen = openRes.efficiencySorted(iWave,:) - 1e-5;
    Yul = ulRes.efficiencySorted(iWave,:) - 1e-5;
    semilogx(X,Yopen,'k-o','linewidth',2,'MarkerFaceColor','k')
    semilogx(X,Yul,'r-o','linewidth',1,'MarkerFaceColor','r','MarkerSize',4)
    semilogx(1e21*xFF,yFF,'c','linewidth',0.5);
    set(gca,'xscale','log')
    legend('Open-box curve','UL-box curve','FlareFit to UL-box','Location','SouthEast')
    title('detection efficiency');
    xlabel('injection scale')
    ylabel('fraction above loudest event')
    axis([min(X) max(X) 0 1.0]);
    grid;
    hold off;

    % record significance of each injection for sensitivity of pop detection
    aTrig=load([rootULResName 'associatedTriggers_'  ulRes.nameCell{iWave} ...
          '_' ulRes.parametersCell{iWave} '.mat']);
    [tmp injScaleI] = sort(aTrig.injectionScale(iWave,:));
    % KLUDGE -- work around when non constant number of injections is used
    sizeSigInj = inf(1,4);
    if exist('sigInj')
      sizeSigInj = size(sigInj);
      sigInj(iGRB,iWave,:,:) = zeros(sizeSigInj(3:4));
    else
      sigInj(iGRB,iWave,:,:) = zeros(size(aTrig.injAssociatedTrigger));
    end
    % END KLUDGE -- but see sizeSigInj a few lines below!
    for iScale = 1:length(injScaleI)
      % the associated trigger list is either empty or contain one cluster
      tmpSig = vertcat(aTrig.injAssociatedTrigger(:,injScaleI(iScale)).significance);
      % apply passing of various consistency tests
      tmpPass = vertcat(aTrig.injAssociatedTrigger(:,injScaleI(iScale)).pass);
      tmpSig = tmpSig(logical(tmpPass));
      if not(isempty(tmpSig))
        sigInj(iGRB,iWave,1:min(length(tmpSig),sizeSigInj(3)),iScale) = tmpSig(1:min(length(tmpSig),sizeSigInj(3)));
      end
    end

    % ---- Save efficiency curve plot plot.
    plotName{iWave} = ['/eff_' grbName{iGRB} ulRes.nameCell{iWave} ulRes.parametersCell{iWave}];
    xsavefigure(plotName{iWave},figures_dirName,figfiles_dirName);
  end
  % ---- Put all efficiency curve plots into a table.
  xaddfiguretable(fout,plotName,figures_dirName);
  clear plotName
  
end

% save output to file
outputFileName = ['batchSummary_' user_tag '.mat'];
save(outputFileName)

fclose(fout);
fclose(fbkg);

% create astrophysical results (e.g. for paper)
if exist('grbZdistribFile')
  xastroresults(outputFileName, webPageFileName, figures_dirName, ...
                figfiles_dirName, popDetStr, grbZdistribFile);
else
  xastroresults(outputFileName, webPageFileName, figures_dirName, ...
                figfiles_dirName, popDetStr);
end
