#!/usr/bin/env python 
"""
XCHECKDAGS - check the status of a first-stage x-pipeline analysis

$Id$

This script checks the status of a first-stage X-Pipeline analysis and attempts
to fix any problems encountered. 

"""
__author__  = 'Valeriu Predoi, Patrick Sutton <patrick.sutton@ligo.org>'
__date__    = '$Date$'
__version__ = '$Revision$'
__name__    = 'xcheckdags'


# -------------------------------------------------------------------------
#    Imports and function definitions.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt, re
from time import sleep
import configparser


# ---- Function usage.
def usage():
    msg = """\

XCHECKDAGS - check the status of a first-stage x-pipeline analysis

Usage: 

  xcheckdags.py [options]
  -d, --dir <path>        [REQUIRED] Absolute or relative path to directory 
                          holding the dags to be checked.
  -u, --user <name>       Albert.Einstein username under which the dags to be
                          checked are being run. Defaults to the script user's 
                          name.
  -t, --time <hours>      Optional maximum run time of jobs [hours] for "zombie" test.
                          If not specified, defaults to 4 hours.
  --ignore-held           Do not check for held jobs. If not supplied the script
                          will attempt to fix and release any held jobs. 
  --ignore-missing        Do not check for missing output files. If not supplied
                          the script will check for missing matlab files and 
                          issue warnings.
  --ignore-empty          Do not check for empty .mat output files. If not 
                          supplied the script will check for such files and try
                          to rerun the jobs that created them.
  --ignore-lost-inj       Do not check for missing triggers in injection output files.
                          If not supplied, the script will check for missing
                          triggers and try to rerun the jobs that created them.
  --ignore-zombie         Do not check for jobs that are taking an inordinately long 
                          time to run ("zombie jobs"); such jobs usually never finish. 
                          If not supplied, the script will check for such jobs and 
                          kill them to allow the dag to retry them.
  -h, --help              Display this message and exit.

e.g.,
xcheckdags.py -d /home/patrick.sutton/test/

This script checks the status of a first-stage X-Pipeline analysis and attempts
to fix any problems encountered. Specifically, it reports summary statistics on
number of jobs submitted and completed. It also optionally checks for and 
attempts to fix problems with missing or empty output files and held jobs.

"""
    print(msg, file=sys.stderr)


# ---- Function that parses dagman.out files.
def get_dag_status(filename, totals, debug=False):

    # ---- Initialise variables.
    tab        = "    "
    dag_status = 0
    num        = 0
    output     = 0

    thisdag = open(filename + '.dagman.out', 'r')

    # ---- dagman.out files periodically report the status of jobs, with three 
    #      lines of the following format:
    #        07/21/20 07:06:52  Done     Pre   Queued    Post   Ready   Un-Ready   Failed
    #        07/21/20 07:06:52   ===     ===      ===     ===     ===        ===      ===
    #        07/21/20 07:06:52  5175       0     1688       0    9945       2256        0
    #      Find this pattern by searching for the word 'Done' on each line, then 
    #      recording that line and the two following. Later instances over-write
    #      earlier ones, so if there are multiple instances in the file only the
    #      final one is output.
    # ---- If the dag has completed successfully the final line will contain the 
    #      string 'EXITING WITH STATUS 0'.
    if dag_status == 0:
        line = "dummy"
        while line:
            line = thisdag.readline()
            if line.find('Done')>=0:
                output = tab + line      
                line = thisdag.readline()  # line of "==="s
                output = output + tab + line
                line = thisdag.readline()  # line of numbers
                output = output + tab + line
                num = line  # record copy of numbers line for use below
                #numbers = line.split(" ")
                #number_of_undone_jobs = int(numbers[len(numbers)-1])
            if line.find('EXITING WITH STATUS 0')>=0:
                dag_status = 1

        # ---- Count the number of jobs, if we have found any 'Done' reports.
        if num != 0:  
            # ---- Split the line by whitespaces.
            num = num.split()
            # ---- Drop the date and time entries.
            num = num[2:]
            # ---- Record the numbers as a vector of integers.
            for i in range( len( num ) ):
                totals[i] += int(num[i])

    # ---- Dump the job numbers to screen, if we have found any 'Done' reports.
    #      The last element of output is a carriage return, so omit it.
    if output != 0:
        print(output[:-1])

    return dag_status


# -------------------------------------------------------------------------
#    Executable code.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
parent_dir    = None
check_missing = True
fix_empty     = True
fix_held      = True
fix_lost_inj  = True
fix_zombie    = True
user_name     = None
max_time      = 4.0
# ---- Guess the condor username. We initialise with "None" above in case 
#      something goes wrong here.
user_name     = os.popen('whoami').read().rstrip() 

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hd:u:t:"
# ---- Long form.
longop = [
    "help",
    "dir=",
    "ignore-empty",
    "ignore-held",
    "ignore-missing",
    "ignore-lost-inj",
    "ignore-zombie",
    "user=",
    "time="
    ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-d", "--dir"):
        parent_dir = str(a)      
    elif o in ("-u","--user"):
        user_name = str(a)      
    elif o in ("-t","--time"):
        max_time = float(a)      
    elif o in ("--ignore-empty"):
        fix_empty = False
    elif o in ("--ignore-held"):
        fix_held = False
    elif o in ("--ignore-missing"):
        check_missing = False
    elif o in ("--ignore-lost-inj"):
        fix_lost_inj = False
    elif o in ("--ignore-zombie"):
        fix_zombie = False
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not parent_dir:
    print("No directory specified. Use the --dir option to specify one.", file=sys.stderr)
    usage()
    sys.exit(1)

# ---- Ensure directory names end in '/'.
if not(parent_dir.endswith('/')):
    parent_dir = parent_dir + '/'

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("################################################################################", file=sys.stdout)
print("#                                  xcheckdags                                  #", file=sys.stdout)
print("################################################################################", file=sys.stdout)
print(file=sys.stdout)
print("Checking the status of all dags in the directory " + parent_dir, file=sys.stdout)
if user_name:
    print("  for the user", user_name, file=sys.stdout)
if fix_held:
    print("Will attempt to fix and release any held jobs.", file=sys.stdout)
else:
    print("Will not attempt to fix any held jobs.", file=sys.stdout)
if fix_empty:
    print("Will attempt to fix any empty output .mat files.", file=sys.stdout)
else:
    print("Will not check for empty output .mat files.", file=sys.stdout)
if check_missing:
    print("Will check for missing output .mat files.", file=sys.stdout) 
else:
    print("Will not check for missing output .mat files.", file=sys.stdout)
if fix_lost_inj: 
    print("Will check for and attempt to fix any injection files missing triggers.", file=sys.stdout)
else:
    print("Will not check for injection files missing triggers.", file=sys.stdout)
if fix_zombie: 
    print("Will check for and attempt to restart any jobs that are running longer than", max_time, "hours.", file=sys.stdout)
else:
    print("Will not check for jobs that are taking too long to run.", file=sys.stdout)
print(file=sys.stdout)


# -----------------------------------------------------------------------------
#    Find all dags in the directory with .dagman.out files.
# -----------------------------------------------------------------------------

print("################################################################################")
print() 
print("Checking status of dags...") 

# ---- Loop through contents of parent_dir. If contents name ends with 
#      'dagman.out' then add the corresponding dag to our list of dags to 
#      process.
print("Looking for dags that have been submitted to condor:", file=sys.stdout)
dir_contents = os.listdir(parent_dir)
filenames    = []
for file in dir_contents:
    if file.endswith('dagman.out'):
        dag_file = file[:-11]
        filenames.append(dag_file)
        print("  Found dag:", dag_file)
sub_dags = len( filenames )
print('Found', str( sub_dags ), ' submitted dags in total.')   
print(file=sys.stdout)


# -----------------------------------------------------------------------------
#    Parse the dags one by one.
# -----------------------------------------------------------------------------

print('Parsing the dag files for status...')
sleep(1.5)
done_dags = 0  
totals = [0,0,0,0,0,0,0,0]
for i in range(0,len(filenames), 1):

    filename = parent_dir + filenames[i]
    print('-------------------------------------------------------------------------')
    print("Parsing " + filename)
    # ---- How many jobs are completed/failed ?
    status = get_dag_status(filename, totals)

    if status==1: 
        done_dags += 1 
        print("COMPLETE")
    elif status==0:
        print("INCOMPLETE")
    elif status==-1:
        # ---- I don't understand how we ever get here!
        print("dag not yet started!")
    elif status==-2:
        # ---- I don't understand how we ever get here!
        print("dag pending and not yet planned!")


# -----------------------------------------------------------------------------
#    Print totals over all dags.
# -----------------------------------------------------------------------------

print('-------------------------------------------------------------------------')
print("Totals over all dags:")
print("      Done    =", totals[0])
#print " Pre     =", totals[1]
print("      Queued  =", totals[2])
#print " Post    =", totals[3]
print("      Ready   =", totals[4])
print("      Unready =", totals[5])
print("      Failed  =", totals[6])
print("      Submitted Jobs = " + str( sum( totals[:7] ) ))
print("      Completed Jobs = " + str( totals[0] ))
print("      Total Jobs     = " + str( sum(totals[:]) ))
if sub_dags != 0:
    print("      Completed dags =", str( done_dags ) + "/" + str( sub_dags ))
print()


# -----------------------------------------------------------------------------
#    Check for missing output files, if requested.
# -----------------------------------------------------------------------------

if check_missing:
    print("################################################################################")
    print() 
    print("Checking for missing output files...")
    import xcheckgrboutput
    verbose     = False
    check_merge = False
    try:
        xcheckgrboutput.xcheckoutputfiles(parent_dir,verbose,check_merge)
    except:
        print("Warning: xcheckgrboutput check failed.") 
    print()


# -----------------------------------------------------------------------------
#    Attempt to fix any held jobs, if requested.
# -----------------------------------------------------------------------------

if fix_held:
    print("################################################################################")
    print()
    if user_name:
        print("Checking for held jobs for user " + user_name + " ...")
    else:
        print("Checking for held jobs...")
    import xfixheldjobs
    try:
        xfixheldjobs.xfixheldjobs(user_name)
    except:
        print("Warning: xfixheldjobs check failed.") 
    print()


# -----------------------------------------------------------------------------
#    Attempt to fix any empty .mat output files, if requested.
# -----------------------------------------------------------------------------

if fix_empty:
    print("################################################################################")
    print()
    print("Checking for empty .mat output files...")
    import xfixemptymatfiles
    try:
        xfixemptymatfiles.xfixemptymatfiles(parent_dir)
    except:
        print("Warning: xfixemptymatfiles check failed.") 
    print()


# ----------------------------------------------------------------------------
#    Check for injection files missing triggers and attempt to fix, if requested.
# -----------------------------------------------------------------------------

if fix_lost_inj:
    print("################################################################################")
    print()
    print("Checking for injection .mat output files that are missing triggers...")
    import xfixmissinginjtrigs
    verbose          = False
    dry_run          = False 
    exhaustive_check = False
    try:
        xfixmissinginjtrigs.xfixmissinginjtrigs(parent_dir,verbose,dry_run,exhaustive_check)
    except:
        print("Warning: xfixmissinginjtrigs check failed.") 
    print()


# ----------------------------------------------------------------------------
#    Check for injection files missing triggers and attempt to fix, if requested.
# -----------------------------------------------------------------------------

if fix_zombie:  
    print("################################################################################")
    print()
    print("Checking for zombie jobs ... ")
    import xfixzombiejobs     
    dry_run          = False 
    try:
        xfixzombiejobs.xfixzombiejobs(user_name,max_time,dry_run)
    except:
        print("Warning: xfixzombiejobs check failed.") 
    print()


