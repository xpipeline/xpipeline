function xmiscalibrategrbinjectionfile(inFile,outFile,detectorListStr,frameTypeListStr)
% XMISCALIBRATEGRBINJECTIONFILE - Simulate calibration errors in injection file.
%
% XMISCALIBRATEGRBINJECTIONFILE modifies an X-Pipeline formatted injection
% log file to simulate the effect of calibration uncertainties.
%
% usage:
%
%  xmiscalibrategrbinjectionfile(inFile,outFile,detectorListStr,frameTypeListStr)
%
%  inFile           String.  Name of input injection file to be modified.
%  outFile          String.  Name of output file with modified injections.
%  detectorListStr  String.  Tilde separated list of detectors in the 
%                   network, e.g., 'H1~L1~V1'  
%  frameTypeListStr String.  Tilde separated list of frame types for the 
%                   detectors in detectorListStr.
%
% Calibration uncertainties are loaded from GETCALIBRATIONUNCERTAINTIES. The
% amplitude uncertainty is applied by rescaling the amplitude independently for
% each injection and detector by (1+N(-amplitude.mu,amplitude.sigma)). For
% injections characterised by distance, the factor is
% (1+N(-amplitude.mu,amplitude.sigma)). The timing error is applied by shfting
% the injection time by N(-time.mu,time.sigma). The phase error is applied as an
% additional time shift by treating the injection as narrowband and time
% shifting it by N(-phase.mu,phase.sigma) / (2*pi*f0) where f0 is the injection
% central frequency.
%
% $Id$

% Note for developers: Here are the requirements for a waveform to be
% miscalibrated:
%  1. You need to know which parameter controls the amplitude or distance.
%  2. You need to be able to assign a central frequency to the waveform. 
% For an injection of type <type> you can get these parameters from
%     [amplDistIndex,freqIndex,distIndex] = xmakewaveform(<type>) 
% e.g.,
%   [amplDistIndex,freqIndex,distIndex] = xmakewaveform('wnb') .
% An empty output ([]) means that parameter is not defined for that waveform type.

% ---- Check input.
error(nargchk(4, 4, nargin, 'struct'))
if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end
if ~ischar(detectorListStr)
    error('Input detectorList must be a string.');
end
if ~ischar(frameTypeListStr)
    error('Input frameTypeList must be a string.');
end

% ---- Convert detectorListStr into a cell array.
detectorList = tildedelimstr2numorcell(detectorListStr);

% ---- Convert frameTypeListStr into a cell array.
frameTypeList = tildedelimstr2numorcell(frameTypeListStr);

% ---- Dimensions.
nDetector = length(detectorList);

% ---- Read and parse input injection file.
injection = readinjectionfile(inFile);
[~, gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);
% ---- Parser code foolishly outputs "name" and "parameters" as cell arrays
%      of 1x1 cell arrays of strings; reformat to remove extra layer of
%      cell arrays.
name = cell(nInjection,1);
parameters = cell(nInjection,1);
for ii = 1:nInjection
    name{ii} = temp_name{ii}{1};
    parameters{ii} = temp_parameters{ii}{1};
end

% ---- Get calibration uncertainties using the time of the first injection.
[amplitude, phase, time] = getcalibrationuncertainties(gps_s(1),detectorListStr,frameTypeListStr);

% ---- Extract un-jittered arrival time, amplitude, central frequency.
centerTime = gps_s + 1e-9*gps_ns;
% ---- Location of amplitude and central frequency parameters depends on
%      the waveform type. Retrieve this information from xmakewaveform.
centerFreq    = zeros(nInjection,1);
injAmplitude  = zeros(nInjection,1);
paramArray    = cell(nInjection,1);
amplDistIndex = zeros(nInjection,1);
amplDistSign  = zeros(nInjection,1);
for ii = 1:nInjection
    % ---- Determine which injection parameters control amplitude/distance and
    %      central frequency.
    [amplIndex,freqIndex,distIndex] = xmakewaveform(lower(name{ii}));
    if ~isempty(amplIndex)
        amplDistIndex(ii) = amplIndex;
        amplDistSign(ii) = -1; %-- correction for hrss has opposite sign to amplitude.mu; see below
    elseif ~isempty(distIndex)
        amplDistIndex(ii) = distIndex;
        amplDistSign(ii) = 1; %-- correction for distance has same sign as amplitude.mu; see below
    else
        error(['Neither amplIndex nor distIndex defined for waveform type ' lower(name{ii}) '.']);
    end
    % ---- Split injection parameter string into array and extract amplitude or
    %      distance parameter.
    paramArray{ii} = tildedelimstr2numorcell(parameters{ii},1);
    injAmplitude(ii) = paramArray{ii}{amplDistIndex(ii)};
    % ---- Extract central frequency, or assign one for waveforms that do not
    %      have a central frequency parameter.  
    if ~isempty(freqIndex)
        centerFreq(ii) = paramArray{ii}{freqIndex};
    else
        % ---- Compute centerFreq for special cases as necessary.
        switch lower(name{ii})
            case {'cusp'}
                % ---- string cusp waveforms. Parameters are
                %      [hrss,f0] with f0 the cut-off frequency
                %      Use ad-hoc central frequency which roughly correspond
                %      to where most of the SNR is accumulated 
                %      f_c = 75 Hz for f0<=150 Hz
                %      f_c = f0/2 for 150<f0<300 Hz
                %      f_c = 150 Hz for f0>=300 Hz
                freqIndex = 2;
                cutoffFreq = paramArray{ii}{freqIndex};
                centerFreq(ii) = min(max(cutoffFreq/2,75),150);
            case {'g'}
                % ---- Double array [hrss,tau], where 'hrss' is the RSS amplitude 
                %      and 'tau' is the duration. For Gaussians 
                %      centerFreq ~ 1/(2*pi*tau)
                freqIndex = 2;
                tau = paramArray{ii}{freqIndex};
                centerFreq(ii) = 1/(2*pi*tau);
            otherwise 
                centerFreq(ii) = 155;
                if ii == 1
                    warning(['Central frequency has been set at 155 Hz for calculating time shift ' ...
                    'for calibration uncertainty']);
                end
        end
    end
end

% ---- Set the random number generator seed based on the name of the
%      input file.
randn('state',sum(inFile))

% ---- Jitter injection amplitude. Note that amplitude.mu>0 means noise is
%      higher than nominal value, so simulate by lowering injection hrss
%      amplitude (amplDistSign=-1) or increasing distance (amplDistSign=+1). 
amplitudeScale = 1 + amplDistSign * amplitude.mu ...
    + randn(nInjection,nDetector) * diag(amplitude.sigma);
% ---- Reset amplitude scale of first injection to 1 so that web page
%      captions look sensible.
amplitudeScale(1,:) = 1;
% ---- We should not get any negative amplitudes with uncertainties <=10%, but
%      check anyway.
if any(amplitudeScale<0)
    error('Negative amplitude generated -- Gaussian distribution not good.');
end
% ---- Apply scale factors.
injAmplitude = repmat(injAmplitude,[1,nDetector]) .* amplitudeScale;

% ---- Account for phase error by jittering injection time.  This is
%      equivalent to assuming the waveform is narrowband and applying a
%      time shift defined by 
%        time_shift = phase_error / (2*pi*centerFreq)
phaseError = repmat(-phase.mu,nInjection,1) ...
    + randn(nInjection,nDetector) * diag(phase.sigma);
timeShift = 1/(2*pi) * phaseError  ./ repmat(centerFreq,[1,nDetector]);
% ---- Add in jitter due to timing uncertainty.
timeShift = timeShift + repmat(-time.mu,nInjection,1) ...
    + randn(nInjection,nDetector) * diag(time.sigma);
% ---- Apply time shift. 
centerTime = repmat(centerTime,[1,nDetector]) + timeShift;
% ---- Convert jittered centerTime back to gps_s and gps_ns parts. 
gps_s = floor(centerTime);
gps_ns = round(1e9*(centerTime - gps_s));

% ---- Make separate copies of the jittered injection data for each
%      detector, in a format ready to be written to a log file.
numData = cell(1,nDetector);
nameData = cell(1,nDetector);
paramData = cell(1,nDetector);
for jj = 1:nDetector
    numData{jj} = [gps_s(:,jj), gps_ns(:,jj), phi, theta, psi];
    nameData{jj} = name;
    paramData{jj} = cell(nInjection,1);
    for ii = 1:nInjection
        jitteredParam = paramArray{ii};
        jitteredParam{amplDistIndex(ii)} = injAmplitude(ii,jj); %-- reset amplitude
        paramData{jj}{ii} = numorcell2tildedelimstr(jitteredParam);
    end
end

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
    for jj = 1:nDetector
        %----- Write signal type and parameters to output file.
        fprintf(fid,'%d %d %e %e %e ',numData{jj}(ii,:));
        fprintf(fid,'%s ',nameData{jj}{ii});
        fprintf(fid,'%s ',paramData{jj}{ii});
    end
    fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return

