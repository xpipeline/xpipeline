function []=xwriteeventtable(fout,analysis,onSource,skipFirstTable)
% XWRITEONSOURCEEVENTTABLE - write table of loudest on-source events.
% 
% xwriteonsourceeventtable writes a table of the loudest events in the 
% on-source data before and after vetoes are applied.  It is a helper 
% function for xmakegrbwebpage.m 
%
% Usage: 
%
%    xwriteonsourceeventtable(fout,analysis,onSource)
%
%  fout            Pointer to open and writable webpage file
%  analysis        Structure created by xmakegrbwebpage
%  onSource        Structure containing onSource events
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(3, 3, nargin));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Make two tables, one of loudest events and one of loudest events 
%                           passing vetoes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for thisTable = 2:2

    % ---- Sort events using significance
    if (thisTable == 1)
        [s I] = sort(onSource.significance,'descend');
    elseif (thisTable == 2)
        [s I] = sort(onSource.significance.*onSource.pass,'descend');
    end

    % ---- write lists of quantities sorted by significance
    sLL  = onSource.likelihood(I,:);
    sBB  = onSource.boundingBox(I,:);
    sAT  = onSource.analysisTime(I);
    sNB  = onSource.nPixels(I);
    sPT  = onSource.peakTime(I);

    if (thisTable == 1)
        % ---- top ten of all onSource events
        fprintf(fout,'%s\n',[' Rows in <font color="red">red</font> indicate ' ...
            'events that failed one or more of the consistency or DQ vetoes.']);
        nLoudestClusters = min(10,length(onSource.analysisTime));
     elseif (thisTable == 2)
         % ---- top ten of all onSource events surviving cuts
         fprintf(fout,'%s\n',['All events in this table have ' ...
              'passed all consistency and DQ vetoes'])
         nLoudestClusters = min(10,sum(onSource.pass));
    end

    fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
    fprintf(fout,'%s\n','<br>');

    % ---- Write html table headers.
    fprintf(fout,'%s\n','<tr>');
    fprintf(fout,'%s','<td><b>significance</b></td>');
    for likelihoodIdx=1:length(analysis.likelihoodType)
        fprintf(fout,'%s', [ '<td><b>' ...
            analysis.likelihoodType{likelihoodIdx} '</b></td>']);
    end
    fprintf(fout,'%s', [...
        '<td><b> peak time <br> GPS </b></td>' ...
        '<td><b>duration<br>(ms)</b></td>' ...
        '</td><td><b>lowest frequency<br>(Hz)</b></td>' ...
        '<td><b> highest frequency <br>(Hz)</b></td>' ...
        '<td><b>1/analysis time</b></td>' ...
        '<td><b>number of <br>TF pixels</b></td>']);
    fprintf(fout,'%s\n','</tr>');

    % ---- Write event data.
    for iC=1:nLoudestClusters
        % ---- Color an event red if it fails a veto test.
        if onSource.pass(I(iC))
            rowcolor = 'white';
        else
            rowcolor = 'red';
        end
        fprintf(fout,['<tr bgcolor="' rowcolor '">']);
        % ---- Significance.
        fprintf(fout,'<td>%g</td>',onSource.significance(I(iC)));
        % ---- All likelihoods.
        for likelihoodIdx=1:length(analysis.likelihoodType)
            fprintf(fout,'<td>%g</td>',sLL(iC,likelihoodIdx));
        end
        % ---- Time-frequency info.
        fprintf(fout,[...
            '<td>%9.5f</td>'...
            '<td>%3.1f</td>'...
            '<td>%g</td>'...
            '<td>%g</td>'...
            '<td>%g</td>' ...
            '<td>%g</td></tr>\n'], ...
            sPT(iC),...
            sBB(iC,3)*1000, ...
            sBB(iC,2),...
            sBB(iC,2)+sBB(iC,4),...
            1/sAT(iC),...
            sNB(iC));
    end % ---- end loop over loudesnt events

    % ---- Close table and clean up.
    fprintf(fout,'%s\n','</table><br>');
    clear sLL sBB sAT sNB sPT

end % ---- end of loop to make two plots


