function [val,unit] = getinjamplordist(nameStr,parametersStr)
% GETINJAMPLORDIST - determine amplitude or distance of injection
%
% Usage: 
%
%  [val,unit] = getinjamplordist(nameStr,parametersStr)
%
%  nameStr          String. Waveform type as recognised by XMAKEWAVEFORM, e.g. 
%                   'chirplet'.
%  parametersStr    String. Waveform parameters as recognised by XMAKEWAVEFORM
%                   or as specified in an ini file., e.g.
%                   '5.0e-22~6.25e-4~1600~0~0~-1;1;linear'.
%
%  val              Scalar. Value (amplitude or distance) of the injection 
%                   parameter.
%  unit             String. Units of the injection parameter, e.g. 'Hz^(-0.5)' 
%                   or 'Mpc',
%
% $Id$


% ---- Check for valid number and type of inputs. 
narginchk(2,2)

% ---- Determine whether injection is parametrised by hrss or distance. 
% ---- Extract parameters.
params = tildedelimstr2numorcell(parametersStr);
% ---- A complication: a parameter string read from an .ini file may represent a code for
%      generating 1 or more than 1 random parameters, such as generating 2 mass or 6 spins
%      for inspiral injections - see the ASSIGNPARAMETER helper function in
%      XMAKEGWBINJECTIONFILE. Here we will check for the two cases where the string
%      represents more than one parameter: the mass and spin cases. We will make a
%      new version of params with the correct number of elements.
if iscell(params)
    tmp_params = [];
    for iParam = 1:length(params)
        if ~isempty(strfind(params{iParam},';')) && ~isempty(strfind(params{iParam},'mass'))
            tmp_params{end+2} = params{iParam};
        elseif ~isempty(strfind(params{iParam},';')) && ~isempty(strfind(params{iParam},'spin'))
            tmp_params{end+6} = params{iParam};
        else
            tmp_params{end+1} = params{iParam};
        end
    end
    params = tmp_params;
end
% ---- Determine nature of waveform.
[amplIdx,freqIdx,distIdx] = xmakewaveform(nameStr);
if ~isempty(amplIdx)
    % ---- Waveform is characterised by hrss amplitude [Hz^-0.5]. Determine 
    %      nominal amplitude.
    if iscell(params)
        val = params{amplIdx};
    else
        val = params(amplIdx);
    end
    unit = 'Hz^(-0.5)';
elseif ~isempty(distIdx)
    % ---- Waveform is characterised by distance [Mpc]. Determine nominal
    %      distance.
    if iscell(params)
        val = params{distIdx};
    else
        val = params(distIdx);
    end
    unit = 'Mpc';
else
    error(['Waveform type ' nameStr ' has neither amplitude nor distance parameter.']) 
end

% ---- Done.
return

