function [totSNR,incnullSNR]= xwritesummarytable(fout,analysis,...
    cFreq,UL50p,UL90p,hrss,percentNans,nameCell,...
    hrssUnits,SNRlist,nullSNRlist)
% XWRITESUMMARYTABLE - write table summarising loudest event upper limits. 
% 
% xwritesummarytable writes a table summarising the upper limits 
% calculated for each injected waveform.  It is a helper function for 
% xmakegrbwebpage.
%
% Usage:
%
%   [totSNR,incnullSNR]= xwritesummarytable(fout,analysis,...
%       cFreq,UL50p,UL90p,hrss,percentNans,nameCell,...
%       hrssUnits,SNRlist,nullSNRlist)
%
%   fout             Pointer to open and writable webpage file  
%   analysis         Structure created by xmakegrbwebpage
%   cFreq            Vector of central frequencies of our injections
%   UL50p            Vector of injection scales at which 50% detection 
%                    efficiency is achieved for our injections
%   UL90p            Vector of injection scales at which 90% detection 
%                    efficiency is achieved for our injections
%   hrss             Vector of intrinsic hrss amplitude of injected waveforms.
%   percentNans      Diagnostic info: percent of tuning tests that give UL=NaN,
%                    Reported by XMAKEGRBWEBPAGE.
%   nameCell         Cell array of strings. Name of each of the injections.  
%   hrssUnits        Cell array of strings. Parameters for each of the 
%                    injection sets. Each string must be one of 'Hz^(-0.5)'
%                    or 'Mpc'.
%   SNRlist          Vector of SNRs of each of the injections 
%   nullSNRlist      Vector of null SNRs of each of the injections
%   figures_dirName  String, path of output dir for figures where figures will
%                    be saved
%   figfiles_dirName String, path of output dir for figures where figfiles will
%                    be saved
%
% $Id$
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(11, 11, nargin));

% ---- Now add table with ULs to the web page.
% ---- Add column headers to table.
fprintf(fout,'%s\n','<br>');
fprintf(fout,'%s\n','<br>');
fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr><td><b>Injection set</b></td>' ...
                     '<td><b>central<br>frequency (Hz)</b></td>' ...
                     '<td><b>hrss_50% (Hz^{-1/2}) or<br>distance_50% (Mpc)</b></td>' ...
                     '<td><b>hrss_90% (Hz^{-1/2}) or<br>distance_90% (Mpc)</b></td>' ...
                     '<td><b>injection scale<br>50% UL</b></td>' ...
                     '<td><b>injection scale<br>90% UL</b></td>' ...
                     '<td><b>% of NaNs<br>during tuning</b></td>']);

for iDetector = 1:length(analysis.detectorList)
    fprintf(fout,'%s\n',['<td><b>amplitude<br>SNR in ' ...
        analysis.detectorList{iDetector} '</b></td>']);
end

% ---- Only make this column if we have analysis.eNullIndex.
if (analysis.eNullIndex)
     fprintf(fout,'%s\n',['<td><b>incoherent null stream<br>amplitude SNR</b></td>']);
end

fprintf(fout,'%s\n',['<td><b>total (root sum square)<br>amplitude SNR</b></td>']);
fprintf(fout,'%s\n','</tr>');

% ---- Now add results for each waveform.
for iWave = 1:length(nameCell)
    if strcmpi(hrssUnits{iWave},'Hz^(-0.5)')
        % ---- Waveform is characterised by hrss amplitude [Hz^-0.5]. 
        limit50 = hrss(iWave) * UL50p(iWave);
        limit90 = hrss(iWave) * UL90p(iWave);
    elseif strcmpi(hrssUnits{iWave},'Mpc')
        % ---- Waveform is characterised by distance [Mpc]; i.e., 
	%      'hrss(iWave)' value is actually a distance.
        limit50 = hrss(iWave) / UL50p(iWave);
        limit90 = hrss(iWave) / UL90p(iWave);
    else
        error(['Units ' hrssUnits{iWave} ' for injection set ' nameCell{iWave} ' not recognised.'])
    end
    fprintf(fout,['<tr><td> <A href="#' nameCell{iWave} ...
        '">%s</A></td><td>%g Hz</td><td>%g </td><td>%g</td><td>%g</td>' ...
        '<td>%g</td><td>%g</td>'], ...
        nameCell{iWave},cFreq(iWave),limit50,limit90, ...
        UL50p(iWave),UL90p(iWave),percentNans(iWave));
    for iDetector =1:length(analysis.detectorList)
        fprintf(fout,'<td>%g</td>',UL50p(iWave)*SNRlist(iWave,iDetector).*( ...
            (analysis.FpList(iDetector)^2+analysis.FcList(iDetector)^2)/2)^0.5);
    end
    % ---- Only make this column if we have analysis.eNullIndex.
    if (analysis.eNullIndex)
        tmpSNR = sqrt(squeeze(sum((UL50p(iWave)*nullSNRlist(iWave,:).*( ...
            (analysis.FpList(:)'.^2+analysis.FcList(:)'.^2)/2).^0.5).^2)));
        fprintf(fout,'<td>%g</td>',tmpSNR);
        incnullSNR(iWave,1) = tmpSNR;
    else
        incnullSNR(iWave,1) = 0;
    end
    tmpSNR = sqrt(squeeze(sum((UL50p(iWave)*SNRlist(iWave,:).* ( ...
        (analysis.FpList(:)'.^2+analysis.FcList(:)'.^2)/2).^0.5).^2)));
    fprintf(fout,'<td>%g</td>',tmpSNR);
    totSNR(iWave,1)=tmpSNR;
    fprintf(fout,'%s\n','</tr>');
end
fprintf(fout,'%s\n','</table><br>');

% ---- Done.
return

