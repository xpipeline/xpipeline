function info = xreadsummaryfile(file)
% XREADSUMMARYFILE - read the summary text file produced by xmakegrbwebpage
%
% XREADSUMMARYFILE reads and parses the summary text file produced by
% xmakegrbwebpage, which contains the upper limits for each waveform type
% and the tuned consistency test thresholds.
%
% usage:
%
%  info = xreadsummaryfile(file)
%
%  file    String.  Name of summary file to be read.
%
%  info    Struct with the following fields:
%            gps - Integer. GPS time of the GRB trigger.
%            ifos - String.  Detector network used.
%            wfs - Cell array of strings.  Each element is the waveform
%              type (as recognized by XMAKEWAVEFORM) for one set of 
%              injections.  
%            hrss50 - Vector.  50% confidence limit for corresponding wvfs.
%            hrss90 - Vector.  90% confidence limit for corresponding wvfs.
%            tuningNameStr, nullVetoType, plusVetoType, crossVetoType - 
%              Strings. Value of input argument of the same name used in
%              XMAKEGRBWEBPAGE. 
%            tunedNullVeto, tunedPlusVeto, tunedCrossVeto - Scalars. Test
%            thresholds used in XMAKEGRBWEBPAGE.
%
% $Id$

% ---- Check for valid number and type of inputs.
error(nargchk(1,1,nargin));

% ---- Open the file.
fid = fopen(file);

% ---- Read column headers (which we ignore).  These are the first 12
%      entries of the file (all on the first line).
C_text = textscan(fid, '%s',12);

% ---- Read the remaining lines.
C_data0 = textscan(fid, '%d %s %s %f %f %s %s %f %s %f %s %f');

% ---- Parse.
info.gps = C_data0{1}(1);
info.ifos = C_data0{2}{1};
info.wfs = C_data0{3};
info.hrss50 = C_data0{4};
info.hrss90 = C_data0{5};
info.tuningNameStr = C_data0{6}{1};
info.nullVetoType = C_data0{7}{1};
info.tunedNullVeto = C_data0{8}(1);
info.plusVetoType = C_data0{9}{1};
info.tunedPlusVeto = C_data0{10}(1);
info.crossVetoType = C_data0{11}{1};
info.tunedCrossVeto = C_data0{12}(1);

% ---- Close the file.
fclose(fid);

% ---- Done.
return
