#!/bin/sh

# Script to re-run the xmerge portion of X-Pipeline trigger generation, 
# allowing triggers from a previous run to be re-merged using different 
# coherent test thresholds.  Should work for both GRB and SN analyses.
# 
# usage:
#   ./remerge.sh grb_dir tuned_file pre_tuned_file
#
# grb_dir        Directory of original trigger production run.
# tuned_file     File containing coherent consistency cuts to be applied in 
#                the re-merging. This must be of the format of an  
#                auto_web/xmakegrbwebpage_*_xmake_args_tuned.txt file.
#                The corresponding .digest file must also exist.
# pretuned_file  File containing coherent consistency "pre" cuts to be applied 
#                in the re-merging. This must be of the format of an  
#                auto_web/xmakegrbwebpage_*_xmake_args_tuned_pre.txt file.
#                The corresponding .digest file must also exist.
# 
# The script will create symlinks to the needed files in the grb_dir and 
# create a new dag called remerge.dag that will perform the merge jobs 
# using the specified threshold files.
#
# REQUIREMENT: The remerge can only performed if the analysis in grb_dir was set
# up using the --use-merging-cuts option (i.e., it is a "second pass" analysis).
# This is because the application of cuts in merging requires that the 
# "*closedbox.mat" file exists from a previous web-page run (see 
# xmergegrbfiles.m).  This is specified in the merge jobs in the grb_dir. (The
# only information taken from this file are the variables that define the types
# of cuts to apply, currently 'analysis', 'plusVetoType', 'crossVetoType', 
# 'nullVetoType', 'medianA', 'medianC', 'maxE'). 
#
# Note on (pre)tuned_file: The only information taken from these files is the 
# fourth entry (vetoMethod). The actual cut thresholds are read from the 
# corresponding .digest files. So these can be prepared by simple modifications 
# of the files form a previous run.
 
# ---- Check input arguments.
num_args=$#
if [ \( ${num_args} -eq 2 \) -o \( ${num_args} -eq 3 \) ]
then
  grb_dir=${1}
  tuned_file=${2}
  if [ ${num_args} -eq 3 ]
  then
    pre_tuned_file=${3}
  fi
  # ---- Dump command to record file.
  echo $0 $@ > remerge.param
else 
  echo "Error! Must have 2 or 3 input arguments."
  exit 1
fi 


# ---- Prepare output directory.
mkdir output
cd output
ln -s ${grb_dir}/output/on_source .
ln -s ${grb_dir}/output/off_source .
ln -s ${grb_dir}/output/simulations_* .
if [ -d "${grb_dir}/output/ul_source" ]; then
  ln -s ${grb_dir}/output/ul_source .
fi
cd ..

# ---- Prepare logs directory.
mkdir logs

# ---- Prepare input directory (symlink to original).
ln -s ${grb_dir}/input .

# ---- Need to link to merge.sub file.
ln -s ${grb_dir}/xmerge.sub .

# ---- Don't know if we need these other links ...
ln -s ${grb_dir}/*.ini .
ln -s ${grb_dir}/grb.param .

# ---- Extract all merge jobs from the original dag:
grep xmerge ${grb_dir}/grb_alljobs.dag | grep -v PARENT > .tmp.dag 

# ---- Change pointer to manual pretuned cuts file:
#sed 's/xmakegrbwebpage_SN2007gr-H1H2L1V1_run8_xmake_args_tuned.txt/MANUAL-xmakegrbwebpage_SN2007gr-H1H2L1V1_run8_xmake_args_tuned.txt/g' foo > remerge.dag

# ---- Determine original cuts files, replace them in dag with user-specified 
#      files.
grep macroarguments .tmp.dag | awk '{print $5}' | sed 's/"//g' | head -n 1 > .tuned_file.txt
while read filename
do
  echo "s:${filename}:${tuned_file}:g" > .sed1.txt
  sed -f .sed1.txt .tmp.dag > .tmp2.dag
done < .tuned_file.txt
cp .tmp2.dag remerge.dag
#
# ---- Check if user specified "pre" cuts file.
if [ ${num_args} -eq 3 ]
then
  grep macroarguments .tmp.dag | awk '{print $7}' | sed 's/"//g' | head -n 1 > .pre_tuned_file.txt
  while read filename
  do
    echo "s:${filename}:${pre_tuned_file}:g" > .sed2.txt
    sed -f .sed2.txt .tmp2.dag > remerge.dag
  done < .pre_tuned_file.txt
fi

# ---- Completed successfully. Clean up and exit.
rm -f .pre_tuned_file.txt .tuned_file.txt .sed1.txt .sed2.txt .tmp.dag .tmp2.dag
exit 0

