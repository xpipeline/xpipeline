echo "This script takes the unclustered triggers from the output directory and moves them to output_clustered so you can submit the clustering DAGs"
echo "Run this only after all xdetection jobs have finished succesfully!"
echo "Copying offsource triggers  with pattern off_source_0_1* to off_source_1"
cp output/off_source/*off_source_0_1*mat* output_clustered/off_source_1
echo "Copying offsource triggers  with pattern off_source_0_2* to off_source_2"
cp output/off_source/*off_source_0_2*mat* output_clustered/off_source_2
echo "Copying offsource triggers  with pattern off_source_0_3* to off_source_3"
cp output/off_source/*off_source_0_3*mat* output_clustered/off_source_3
echo "Copying offsource triggers  with pattern off_source_0_4* to off_source_4"
cp output/off_source/*off_source_0_4*mat* output_clustered/off_source_4
echo "Copying offsource triggers  with pattern off_source_0_5* to off_source_5"
cp output/off_source/*off_source_0_5*mat* output_clustered/off_source_5
echo "Copying offsource triggers  with pattern off_source_0_6* to off_source_6"
cp output/off_source/*off_source_0_6*mat* output_clustered/off_source_6
echo "Copying offsource triggers  with pattern off_source_0_7* to off_source_7"
cp output/off_source/*off_source_0_7*mat* output_clustered/off_source_7
echo "Copying offsource triggers  with pattern off_source_0_8* to off_source_8"
cp output/off_source/*off_source_0_8*mat* output_clustered/off_source_8
echo "Copying offsource triggers  with pattern off_source_0_9* to off_source_9"
cp output/off_source/*off_source_0_9*mat* output_clustered/off_source_9
echo "Copying offsource triggers  with pattern off_source_0_0* to off_source_10"
cp output/off_source/*off_source_0_0*mat* output_clustered/off_source_10
echo "Done copying offsource triggers"
echo "Copying onsource triggers"
cp output/on_source/*mat* output_clustered/on_source
echo "Done copying onsource triggers"
echo "Copying injection *simulations_* directories"
rm -r output_clustered/*simulations_*
cp -r output/*simulations_* output_clustered
echo "Done copying everything, you can run the clustering DAGs now"
