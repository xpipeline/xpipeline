function [percentLevel UL] = xoverrideullevel(vetoMethod,UL50p,UL90p,UL95p)
% XOVERRIDEULLEVEL - override default upper limit percentage for cut tuning
%
% XOVERRIDEULLEVEL over-rides the default efficiency value (90%) supplied
% to tune coherent consistency cuts.  
%
%  [percentLevel UL] = xoverrideullevel(vetoMethod,UL50p,UL90p,UL95p)
% 
%  vetoMethod    String.  One of the veto methods recognized by
%                xgrbwebpage.py.
%  UL50p, etc.   Scalar.  Amplitude limits at efficiency = 50% (or 90% or
%                95%) for one waveform.
%
%  percentLevel  Scalar.  Efficiency value at output upper limit.
%  UL            Scalar.  Amplitude limit at specified efficiency value.
%
% This is a pretty trivial function.  By default, veto cuts are tuned based
% on the hrss at 90% efficiency.  However, for certain cuts we reset to
% using the hrss at 50% or 95% efficiency.  This function simply keeps
% track of which cut type uses which percentage: 
%
%   95% 'linearcut', 'linearcutcirc'
%   50% 'medianalphacut', 'medianalphacutcirc', 'alphacut','alphacutcirc', 
%       'medianlincut', 'medianlincutcirc', 'alphalincut', 'alphalincutcirc'
%   90%  anything else ...
%
% $Id$

% ---- Check number of input arguments.
if nargout > 1 && nargin < 4
  error(['Asked to output UL at appropriate percentage level but was not ' ...
         'provided with input ULs']);
end

if regexp(vetoMethod, 'linearCut')
        percentLevel = '95';
        if nargout > 1
            UL = UL95p;  
            warning('Overriding the 90% UL with 95% UL')
        end
elseif regexp(vetoMethod, '(medianAlphaCut|alphaCut|medianLin|alphaLin)')
        percentLevel = '50';
        if nargout > 1
            UL = UL50p;
            warning('Overriding the 90% UL with 50% UL')
        end
else
        percentLevel = '90';
        if nargout > 1
            UL = UL90p;
        end
end
