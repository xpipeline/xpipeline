function [passCut passFixedCut] = ...
    xapplycoherentcuts(cluster,analysis,vetoMethod,ratioArray,deltaArray,...
                       medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray)
% XAPPLYCOHERENTCUTS - apply coherent consistency test to candidate events
%
% usage:
%
%  [passCut passFixedCut] = xapplycoherentcuts(cluster,analysis,vetoMethod, ...
%      ratioArray,deltaArray,medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray)
%
%  cluster          Struct of X-Pipeline triggers. Only the likelihood
%                   field (an array wit one row per trigger) is used.
%  analysis         Required for 'median' tests. 
%  vetoMethod       String. Recognised values are 'alphaCut', 'linearCut',
%                   'medianCut', 'alphaLinCut', 'medianAlphaCut',
%                   'medianLinCut'. If not recognised then no cuts are
%                   applied and all pass outputs = 1. 
%  ratioArray       Required for all vetoMethod values. 
%  deltaArray       Optional for 'linearCut'; ignored otherwise.
%  medianA          Required for 'medianCut', 'medianAlphaCut',
%                   'medianLinCut'; ignored otherwise. 
%  medianC          Required for 'medianCut', 'medianAlphaCut',
%                   'medianLinCut'; ignored otherwise. 
%  maxE             Required for 'medianCut', 'medianAlphaCut',
%                   'medianLinCut'; ignored otherwise. 
%  fixedRatioArray  Required for 'alphaLinCut', 'medianAlphaCut',
%                   'medianLinCut'; ignored otherwise.  
%  fixedDeltaArray  Optional for 'alphaLinCut', 'medianLinCut'; ignored
%                   otherwise.
%
%  passCut          Vector. 1 (0) if corresponding row of cluster passes
%                   (fails) first test.
%  passFixedCut     Vector. 1 (0) if corresponding row of cluster passes
%                   (fails) second test. If no second test then = 1 for all
%                   rows.
%
% XAPPLYCOHERENTCUTS applies coherent consistency tests to the events in
% cluster by calling the functions XAPPLYALPHARATIOTEST,
% XAPPLYMEDIANRATIOTEST, and XAPPLYRATIOTEST. Which tests are applied is
% determined by 'vetoMethod'; e.g., 'alphaLinCut' causes
% XAPPLYALPHARATIOTEST to be applied first, then XAPPLYRATIOTEST. See those
% functions for definitions of the input aguments analysis, ratioArray,
% deltaArray, medianA, medianC, maxE, fixedRatioArray, and fixedDeltaArray.
% 
% $Id$ 

if regexp(vetoMethod,'alphaCut') 
    % ---- Apply a single "alpha" test.
    passCut      = xapplyalpharatiotest(cluster.likelihood,ratioArray);
    passFixedCut = ones(size(passCut)); 
elseif regexp(vetoMethod,'linearCut')
    % ---- Apply a single "ratio" (linear) test.
    passCut      = xapplyratiotest(cluster.likelihood,ratioArray,deltaArray);
    passFixedCut = ones(size(passCut)); 
elseif regexp(vetoMethod,'medianCut') 
    % ---- Apply a single "median" test.
    passCut      = xapplymedianratiotest(cluster.likelihood,ratioArray,analysis,medianA,medianC,2*maxE);
    passFixedCut = ones(size(passCut)); 
elseif regexp(vetoMethod,'alphaLinCut') 
    % ---- Apply both the "alpha" and "ratio" (linear) tests.
    passCut      = xapplyalpharatiotest(cluster.likelihood,ratioArray);
    passFixedCut = xapplyratiotest(cluster.likelihood,fixedRatioArray,fixedDeltaArray);
elseif regexp(vetoMethod,'medianAlphaCut') 
    % ---- Apply both the "median" and "alpha" tests.
    passCut      = xapplymedianratiotest(cluster.likelihood,ratioArray,analysis,medianA,medianC,2*maxE);
    passFixedCut = xapplyalpharatiotest(cluster.likelihood,fixedRatioArray);
elseif regexp(vetoMethod,'medianLinCut') 
    % ---- Apply both the "median" and "ratio" (linear) tests.
    passCut      = xapplymedianratiotest(cluster.likelihood,ratioArray,analysis,medianA,medianC,2*maxE);
    passFixedCut = xapplyratiotest(cluster.likelihood,fixedRatioArray,fixedDeltaArray);
else
    % ---- We do not perform any consistency tests, so set all pass values to 1;
    warning(['Unrecognized veto method: ' vetoMethod '. Coherent consistency cuts are not used'])
    passCut      = ones(size(cluster.likelihood,1),1); 
    passFixedCut = passCut;
end

% ---- Done.
return

