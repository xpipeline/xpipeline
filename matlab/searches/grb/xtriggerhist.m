function [histName] = xtriggerhist(fout,trigProperty,trigPropertyName,...
    legendCell,figures_dirName,figfiles_dirName,plotTag,...
    loudestTrig,loudestLegend,bins)
% XTRIGGERHIST - Produce histograms of trigger properties.
%
% XTRIGGERHIST produces histograms of trigger properties (such as 
% significance and time-frequency properties) for clusters.  It handles 
% on-source, off-source, and injection clusters. It is a helper function for 
% xwritetriggerhists.m 
%
% Usage: xtriggerhist(fout,trigProperty,trigPropertyName,legendCell,...
%    figures_dirName,figfiles_dirName,plotTag,loudestTrig,loudestLegend,bins)
%
%   fout             Pointer to open and writable webpage file  
%   trigProperty     Cell array containing values of a given trigger property
%                    (e.g., significance) for each of the data sets we are
%                    looking at, e.g., onSource, offSource, injections.
%   trigPropertyName String describing trigProperty, e.g., 'significance'
%   legendCell       Cell array of strings listing names of the different data
%                    sets we are considering, e.g., 
%                    {'on source','off source','injections'}
%   figures_dirName  String, path of output dir for figures where figures will
%                    be saved
%   figfiles_dirName String, path of output dir for figures where figfiles will
%                    be saved
%   plotTag          String used to name output plots
%   loudestTrig      Double, trig property of a single event that will be plotted
%                    on the histogram e.g., loudest on-source survivor. [OPTIONAL]
%   loudestLegend    String, description of loudestTrig. [OPTIONAL]
%   bins             Vector, used to bin histograms. [OPTIONAL]
 
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(7, 10, nargin));

% ---- Create figure.
figure; set(gca,'FontSize',20);

if nargin < 8
   loudestTrig = 0;
   loudestLegend = '';
end

if nargin < 9
   loudestLegend = '';
end

if nargin < 10
    % ---- Set histogram bins.
    maxVal = -Inf;
    minVal = Inf;
    for idx = 1:size(trigProperty,2)
        trigProperty{idx} = trigProperty{idx}(not(isinf(trigProperty{idx})));
        maxVal = max([maxVal max(trigProperty{idx})]);
        minVal = min([minVal min(trigProperty{idx})]);
    end

    % ---- Check to see if all trigProperty values are zero.
    if (maxVal == minVal) 
      if (maxVal == 0)
        warning(['All ' trigPropertyName ' are zero']);
        % ---- Set maxVal to 10 so that code below doesn't fail.
        %      We still produce histogram so that user is made aware
        %      of all trigProperties ==0.     
        maxVal = 10;
      else
        maxVal = maxVal + 0.1*abs(maxVal);
        minVal = minVal - 0.1*abs(minVal);
      end
    end

    binwidth = (maxVal - minVal) / 25;
    bins = minVal:binwidth:maxVal;
end

% ---- Make histograms
for idx = 1:size(trigProperty,2)
    %[NN{idx},XX{idx}] = hist(trigProperty{idx},bins);
    [NN{idx}] = hist(trigProperty{idx},bins);
end

warning('OFF','MATLAB:log:logOfZero');
% ---- Make plots
color = {'k','r','b','g','c'};
%bar(XX{1},log10(NN{1}),color{1});
%stairs(XX{1},log10(NN{1}),color{1},'linewidth',2);
plot(bins,log10(NN{1}),[color{1} 'o'],'linewidth',2);
hold on;
for idx = 1:size(trigProperty,2)-1
    %bar(XX{idx+1},log10(NN{idx+1}),color{idx+1});
    %stairs(XX{idx+1},log10(NN{idx+1}),color{idx+1},'linewidth',2);
    plot(bins,log10(NN{idx+1}),[color{idx+1} 'o'],'linewidth',2);
end
% ---- Plot loudest trigger if it has been provided.
if loudestTrig
    ylim = get(gca,'YLim');
    plot([loudestTrig,loudestTrig],ylim,'m','LineWidth',2);
end
grid on;
hold off;
warning('ON','MATLAB:log:logOfZero');

% ---- Add total number of triggers plotted to legend.
for idx = 1:length(legendCell)
    legendCell{idx} = [legendCell{idx} ' ' ...
        num2str(length(trigProperty{idx}))];
end

legendCell{length(legendCell)+1} = loudestLegend;

legend(legendCell);
title(trigPropertyName);
xlabel(trigPropertyName);
ylabel('log10(#)');

% ---- Construct name of plot from input args.
histName = 'hist_';
%for idx = 1:length(legendCell)
%    histName = [histName legendCell{idx} '_'];
%end
histName = [histName trigPropertyName '_' plotTag];
% ---- Remove any whitespace.
spaceIdx=isspace(histName);
histName(spaceIdx)='';

% ---- Save figure.
xsavefigure(histName,figures_dirName,figfiles_dirName);

