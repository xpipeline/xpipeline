function [fout,figures_dirName,figfiles_dirName] = ...
    xwritewebpageheader(user_tag,analysis)
% XWRITEWEBPAGEHEADER - open webpage and write header information. 
% This is a helper function for xmakegrbwebpage.
%
% Usage:
%
%  [fout,figures_dirName,figfiles_dirName]=...
%         xwritewebpageheader(user_tag,analysis)
%
%  user_tag            String.
%  analysis            Structure created by xmakegrbwebpage
%
%  fout                Pointer to webpage file, opened in this function
%  figures_dirName     Name of output dir for figures to be created
%  figfiles_dirName    Name of output dir for figfiles to be created
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(2, 2, nargin));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Create comments file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Record input args in comments file
commentsFileName = [analysis.grb_name '_' user_tag '_comments.html'];
if(~exist(['./' commentsFileName ],'file'))
    comments=fopen(['./' commentsFileName],'w');
    fprintf(comments,'%s\n',['Any text/html added to ' commentsFileName  ...
        ' will appear in the "Human added comments" section of the '...
        'results webpage']);
    fclose(comments);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Create fig directories
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Create 'figures' and 'figfiles' directories, if they don't already
%      exist. These will hold the web page .png image files and the matlab 
%      .fig files used to generate them. 
figures_dirName = [analysis.grb_name '_' user_tag '_' ...
    analysis.type '_figures/'];
if(size(dir(figures_dirName), 1) == 0)
    mkdir(figures_dirName)
end

figfiles_dirName = [analysis.grb_name '_' user_tag '_' ...
    analysis.type '_figfiles/'];
if(size(dir(figfiles_dirName), 1) == 0)
    mkdir(figfiles_dirName)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Open web page and write header info.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Name of output web page.
webPageFileName = [analysis.grb_name '_' user_tag '_' ...
    analysis.type '.shtml'];

% ---- Open web page file.
fout = fopen(webPageFileName,'w');

% ---- Write web page headers and style info.
fprintf(fout,'%s\n',['<!DOCTYPE HTML PUBLIC "-//W3C//DTD '...
                     'HTML 4.01 Transitional//EN">']);
fprintf(fout,'%s\n','<html>');
fprintf(fout,'%s\n','<head>');
s = pwd;
fprintf(fout,'%s\n',['  <title> ' s ' </title>']);
fprintf(fout,'%s\n','  <style type="text/css">');
fprintf(fout,'%s\n','body {');
fprintf(fout,'%s\n','font-family: Tahoma,Geneva,sans-serif;');
fprintf(fout,'%s\n','color: black;');
fprintf(fout,'%s\n','background-color: white;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','h1 {');
fprintf(fout,'%s\n','color: #ffffff;');
fprintf(fout,'%s\n','background-color: #000088;');
fprintf(fout,'%s\n','padding: 0.25em;');
fprintf(fout,'%s\n','border: 1px solid black;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','h2 {');
fprintf(fout,'%s\n','color: #000000;');
fprintf(fout,'%s\n','background-color: #cccccc;');
fprintf(fout,'%s\n','padding: 0.25em;');
fprintf(fout,'%s\n','border: 1px solid black;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','  </style>');

fprintf(fout,'%s\n',' <script type="text/javascript">');
fprintf(fout,'%s\n',' function toggleVisible(division) {');
fprintf(fout,'%s\n',[' if (document.getElementById("div_" + '...
                    'division).style.display == "none") {']);
fprintf(fout,'%s\n',[' document.getElementById("div_" + '...
                     'division).style.display = "block";']);
fprintf(fout,'%s\n',[' document.getElementById("input_" + '...
                     'division).checked = true;']);
fprintf(fout,'%s\n',' } else {');
fprintf(fout,'%s\n',[' document.getElementById("div_" + '...
                     'division).style.display = "none";']);
fprintf(fout,'%s\n',[' document.getElementById("input_" + '...
                     'division).checked = false;']);
fprintf(fout,'%s\n',' }');
fprintf(fout,'%s\n',' }');
fprintf(fout,'%s\n',' </script>');

fprintf(fout,'%s\n','</head>');
fprintf(fout,'%s\n','<body>');

if (strcmp(analysis.type,'closedbox') || ...
    strcmp(analysis.type,'snrulclosedbox'))
    fprintf(fout,'%s\n',['<h1>CLOSED-BOX Analysis of ' analysis.grb_name ...
                         ' with X-Pipeline</h1>']);
    fprintf(fout,'%s\n',['This page provides preliminary results '...
                         'from the analysis of ' analysis.grb_name]);
    fprintf(fout,'%s\n','using X-Pipeline.');
elseif strcmp(analysis.type,'openbox')
    fprintf(fout,'%s\n',['<h1>OPEN-BOX Analysis of ' analysis.grb_name ...
                         ' with X-Pipeline</h1>']);
    fprintf(fout,'%s\n',['This page provides results from the '...
                         'analysis of ' analysis.grb_name]);
    fprintf(fout,'%s\n','using X-Pipeline.');
else
  fprintf(fout,'%s\n',['<h1>UL-BOX Analysis of ' analysis.grb_name ...
                         ' with X-Pipeline</h1>']);
    fprintf(fout,'%s\n',['This page provides sanity check ULs from the '...
                         'analysis of ' analysis.grb_name]);
    fprintf(fout,'%s\n','using X-Pipeline.');  
end

fprintf(fout,'%s\n','<h2>Human added comments</h2>');
fprintf(fout,'%s\n',['<!--#include file="' commentsFileName '" -->']);

fprintf(fout,'%s\n','<h2>Code versions</h2>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Codes </b></td>']);
fprintf(fout,'%s\n',['<td><b> svnversion </b></td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Processing (i.e., xdetection) </td>']);
fprintf(fout,'%s\n',['<td> ' analysis.svnversion_xdetection ' </td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> Post-processing (i.e., xmakegrbwebpage) </td>']);
fprintf(fout,'%s\n',['<td> ' analysis.svnversion_grbwebpage ' </td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');


