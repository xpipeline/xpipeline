function [] = xwritelikelihoodscatterplots(fout, figures_dirName, ...
    figfiles_dirName, analysis, onSource, offSource, vetoMethod, ratioArray, ...
    medianE, medianI, sourceType, fixedRatioArray, fixedDeltaArray)
% XWRITELIKELIHOODSCATTERPLOTS - Write likelihood scatter plots to a webpage.
%
% Usage: 
%
% [] = xwritelikelihoodscatterplots(fout, figures_dirName, ...
%     figfiles_dirName, analysis, onSource, offSource, vetoMethod, ratioArray, ...
%     medianE, medianI, sourceType);
%
%  fout                Pointer to open and writable file.
%  figures_dirName     Name of output directory for figures.
%  figfiles_dirName    Name of output directory for matlab fig files.
%  analysis            Structure created by xmakegrbwebpage.
%  onSource            Structure containing onSource events.
%  offSource           Structure containing offSource events.
%  vetoMethod          String.  Determines what type of veto ratio test to 
%                      use.  Either 'linearCut' or 'medianCut' or 'medianCutCirc'.
%  ratioArray          Array.  Contains parameters of veto cut to be
%                      performed.   
%  medianE             Array.  Median distribution of either onSource or
%                      offsource likelihood pairs.  For vetoMethod =
%                      'linearCut', use medianE = [].
%  medianI             Array.  Median distribution of either offSource or
%                      offsource likelihood pairs.  For vetoMethod =
%                      'linearCut', use medianI = [].
%  sourceType          String.  Indicates contents of events.  Either
%                      'onsource' or 'offsource'.
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(11, 13, nargin));

if regexp(vetoMethod, '(medianLinCut|medianAlphaCut|alphaLinCut)')
  error(nargchk(13, 13, nargin));
end

allowedSourceTypes = {'onsource','offsource'};

if not(max(strcmp(sourceType,allowedSourceTypes)))
    error(['sourceType must be one of onsource,offsource'])
end

if strcmp(sourceType,'onsource')
    events = onSource;
else
    events = offSource;
end

% ---- Hardwire "deltaArray" to be zero.  This is only used by the 
%      linearCut, and it is hardwired to zero in xmakegrbwebpage also.
deltaArray = zeros(size(ratioArray));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Make scatter plot of on-source events.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Likelihoods measured in pairs, (ePlus,iPlus), (eCross,iCross),
%      (eNull,iNull).  Loop over these pairs making plots.
% ---- When analysing only a single ifo, analysis.likelihoodPairs will be 
%      empty and we skip this loop.
for pairIdx=1:length(analysis.likelihoodPairs)

   % ---- Plot only 10000 points with the highest significance - greatly speeds up plotting.
   maxPoints = 10000;
   [sigSorted, sortIdx] = sort(events.significance,'descend');
   if length(sigSorted) > maxPoints
        warning(['We are using only the ' num2str(maxPoints) ...
            ' most significant triggers when making scatter plots.']);
   end
   numPoints = min([maxPoints length(events.significance)]);
   plotIdx = sortIdx(1:numPoints);   

   % ---- Make scatter plot of all events.
   xscatter(events.likelihood(plotIdx,analysis.likelihoodPairs(pairIdx).energy), ...
            events.likelihood(plotIdx,analysis.likelihoodPairs(pairIdx).inc), ...
            events.significance(plotIdx), ...
            analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).energy}, ...
            analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).inc});

   % ---- Initialise legend string for events scatter plot.
   legendStr = {};
   legendStr{length(legendStr)+1} = sourceType;

   % ---- Format plot.
   X=xlim;
   Y=ylim;
   axis([max(X(1),0.1) max(X(2),10) max(Y(1),0.1) max(Y(2),10)]);
   title(sourceType);
   hold on;

   % ---- Plot a diagonal line over a large range.  The axes limits might
   %      increase when we add in injections.
   Xvec = 1:10^5;
   plot(Xvec,Xvec,':k','LineWidth',0.5);  %-- diagonal line
   legendStr{length(legendStr)+1} = 'diagonal';


   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %         Plot ratio cut lines and write info to web page.            
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   numCuts = 0;

   if regexp(vetoMethod, 'linearCut')
      numCuts = xplotratiocuts(ratioArray,deltaArray,analysis,pairIdx,fout,1);
   elseif regexp(vetoMethod, 'alphaLinCut')  
      numCuts = xplotalpharatiocuts(ratioArray,fixedRatioArray,fixedDeltaArray,...
                                    analysis,pairIdx);
   elseif regexp(vetoMethod, 'medianCut')
      numCuts = xplotmedianratiocuts(ratioArray,medianE,medianI,analysis, ...
          events.likelihood,pairIdx,fout,1);
   elseif regexp(vetoMethod, 'medianLinCut')  
     numCuts = xplotmedianlinratiocuts(ratioArray,medianE,medianI,...
                                       fixedRatioArray,fixedDeltaArray,analysis, ...
                                       events.likelihood,pairIdx,fout,1);
   elseif regexp(vetoMethod, 'medianAlphaCut')
     numCuts = xplotmedianalpharatiocuts(ratioArray,medianE,medianI,...
                                       fixedRatioArray,fixedDeltaArray,analysis, ...
                                       events.likelihood,pairIdx,fout,1);
   elseif regexp(vetoMethod, '(N|n)one')
     % do nothing
   else
     warning(['Unrecognized veto method: ' vetoMethod ...
            '. Do not know how to plot cut lines']);
   end

   if numCuts
      if regexp(vetoMethod,'median.*')
        legendStr{length(legendStr)+1} = 'median';
      end
      for i = 1:numCuts
         legendStr{length(legendStr)+1} = 'ratio cut';
      end
   end


   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %             Add loudest onSource event to plots.
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   % ---- Add marker for loudest (most significant) onSource event that passes
   %      vetoes.
   if strcmp(sourceType,'onsource') 
       thisCaption{pairIdx} = analysis.captions.onsource_scatter; 
   elseif strcmp(sourceType,'offsource')  
       thisCaption{pairIdx} = analysis.captions.offsource_scatter; 
   end

   % ---- Find loudest (most significant) events event that
   %                            passes vetoing.
   [maxOnSource maxIdx] = max((onSource.significance).*(onSource.pass));

   % ---- If all events were vetoed then we have set maxSigLikelihoods
   %      to -1.
   if maxOnSource == 0
       onSource.maxSignificance  = 0;
       onSource.maxSigLikelihood = -1 * ones(1,length(analysis.likelihoodType));
   else
       onSource.maxSignificance  = onSource.significance(maxIdx);
       onSource.maxSigLikelihood = onSource.likelihood(maxIdx,:);
   end

   scatter(onSource.maxSigLikelihood(:,analysis.likelihoodPairs(pairIdx).energy), ...
           onSource.maxSigLikelihood(:,analysis.likelihoodPairs(pairIdx).inc), ...
           400, log10(onSource.maxSignificance), ...
           'p','filled','MarkerEdgeColor','r','LineWidth',1)

   legendStr{length(legendStr)+1} = 'loudest on source event surviving vetoes';

   %else
   %    thisCaption{pairIdx} = analysis.captions.offsource_scatter;
   %end % -- end if onSource statement

   % ---- Reset axes.
   axis([max(X(1),0.1) max(X(2),10) max(Y(1),0.1) max(Y(2),10)]);

   hold off;
   legend(legendStr,'Location','SouthEast');

   % ---- Save plot.
   plotName{pairIdx} = [sourceType '_' ...
      analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).energy} '_' ...
      analysis.likelihoodType{analysis.likelihoodPairs(pairIdx).inc} '_' ...
      analysis.clusterType ];
   xsavefigure(plotName{pairIdx},figures_dirName,figfiles_dirName);

end % ---- end loop over likelihood pairs

% ---- If we made plots, add them to table.
%      When analysing only a single ifo, analysis.likelihoodPairs will be 
%      empty and we skip this if statement.
if length(analysis.likelihoodPairs)
    xaddfiguretable(fout,plotName,figures_dirName,thisCaption)
    clear plotName
    fprintf(fout,'%s\n','<br>');
    fprintf(fout,'%s\n','<br>');
end
