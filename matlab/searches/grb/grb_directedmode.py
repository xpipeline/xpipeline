#!/usr/bin/env python

"""
Script for setting up X-Pipeline triggered search jobs.
Adapted from time_slides.py by X. Siemens.
$Id: grb.py 6477 2024-04-04 10:58:46Z patrick.sutton@LIGO.ORG $
"""

# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, shutil, math, random, copy, getopt, re, string, time
import configparser, glob, operator
#from glue import segments
#from glue import segmentsUtils
# ---- replacement for previous two lines, suggested by Ryan Fisher.
from ligo.segments import utils as segmentsUtils
import ligo.segments as segments
from glue import pipeline
from glue.lal import CacheEntry
from numpy import loadtxt
#from pylal.date import LIGOTimeGPS
import utils as segmentutils 
from gwpy.segments import SegmentList, Segment

__author__ = "Patrick Sutton <psutton@ligo.caltech.edu>, Michal Was <michal.was@ens.fr>"
__date__ = "$Date: 2014-06-04 09:43:32 -0700 (Wed, 04 Jun 2014) $"
__version__ = "$Revision: 4282 $"

# ---- Function usage.
def usage():
    msg = """\
Usage:
  grb.py [options]
  -p, --params-file <file>    [REQUIRED] Parameters (.ini) file 
  -n, --grb-name <name>       [REQUIRED] Name of GRB, e.g., GRB070201 
  -g, --grb-time <gps>        [REQUIRED] GRB trigger time (GPS seconds)
  -r, --right-ascension <ra>  [REQUIRED] Right ascension of GRB (degrees, from 0 to 360)
  -d, --declination <decl>    [REQUIRED] Declination of GRB (degrees, from -90 to 90)
  -i, --detector <ifo>        [REQUIRED] Add detector <ifo> to the network. At least one detector
                              must be specified.
  -e, --sky-pos-err <skyerr>  1-sigma uncertainty in sky position of GRB (degrees) or file name of
                              sky position grid to be used with "-t file" option.
                              If a file name, the file must have 4 columns: right ascension,
                              declination, probability, area of each grid tile.
                              Default '0' (exact sky position).
  -t, --grid-type <grid>      String. Determines what shape of sky position grid will be generated.
                              Recognized values are:
                                'circular' - 2-d grids constructed from concentric circles
                                'healpix' - 2-d grids constructed using the healpix algorithm
                                'line' - 1-d arc grid
                                'file' - user generated grid of points, given with the -e option
                                'timedelay' - 1-D arc that creates fixed time delay steps along 
                                  the two-detector baseline
			      Default 'circular'. 
  -f, --grid-sim-file <grsim> String. File which contains (RA,Dec) coordinates [deg] of simulated
                              source positions.  To be used with "--grid-type file" option.
  -s, --network-selection     If this flag is set we select the appropriate network of IFOs from
                              the set specified using the --detector option using certain 
                              data-quality selection criteria.
  --injdistrib <params>       Tilde delimited string of parameters describing the injection 
                              distribution in the error circle. Formats are:
                                1 parameter: 1-sigma containment of a fisher distribution
                                3 parameters: lognormal distribution in degrees
                                4 parameters: fisher distribution of statistical error and
                                    core + tail fisher distribution of systematic error.
                                    [stat_sigma sys_core_sigma fraction_core sys_tail_sigma]
                                    all sigma are in degrees
  --priority <prio>           Integer specifying priority of condor jobs. Higher priority jobs are
                              submitted to the cluster before lower priorty jobs by the same user.
                              Default 0. 
  --end-offset <offset>       Specify that the end offset of the onsource window should be greater
                              than or equal to <offset>. The maximum of the end offset in the .ini
                              file and <offset> is used. 
  --disable-fast-injections   If set disables the fast processing of injections, where the time 
                              frequency map is produced only for a small window around the injection
                              time.
  -c, --catalogdir            Specify location of waveform catalog files for astrophysical
                              injections such as supernovae and adi waveforms.
  --big-mem <memory>          Integer specifying the minimal memory requirement [MB] for condor jobs
  --use-merging-cuts <path>   Specify the location of a post-processing directory from which 
                              coherent cuts should be read and applied to triggers in the xmerge
                              jobs.
  --reuse-inj                 If --use-merging-cuts option is also set, will reuse the injections 
                              from the previous cut-tuning run. Use with caution no check is 
                              performed on consistency between provided and needed injections.
  --xtmva                     Perform XTMVA analysis. 
  --off-source-inj            If set perform injections into the off-source instead of the on-source
                              region.
  --long-inj                  If specified, then when making injection dags a buffer will be used in
                              comparing injection peak time to block interval to determine if an 
                              injection should be processed by multiple blocks. Works only for
                              waveform sets with names beginning with adi-a, adi-b, adi-c, adi-d, 
                              adi-e, ebbh-a, ebbh-d, ebbh-e, mva. See code for details.  
                              USE WITH CAUTION.
  --smart-cluster             This will produce an extra set of DAGs that will run the smart 
                              clustering for large production analyses.
                              USE WITH CAUTION.
  --user                      User's Albert.Einstein username. If not specified then the whoami
                              system comand will be used to guess it.
  --directed-mode             Set up a directed search, designed to search a single sky position
                              over an extended period of time. The analysis differs from the standard
                              GRB-style search as follows:
                                1. a single sky position is searched, fixed in (RA,DEC)
                                2. the same time interval is used for both on-source (zero lag) and
                                   off-source (non-zero lag only) analyses.
                              With the --directed-mode option the --sky-pos-err is treated as 0, the 
                              --nework-selection option is ignored, and the backgroundPeriod and 
                              asymmetric background options in the ini file are ignored.
  -h, --help                  Display this message and exit

"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file       = None
trigger_time      = None
ra                = None
decl              = None
detector          = []
grb_name          = None
grid_type         = 'circular'
grid_sim_file     = ''
mdc_path          = None
network_selection = False
sky_pos_err       = None
injdistrib        = None
manualEndOffset   = 0
condorPriority    = "0"
disableFastInjections = False
catalog_dir       = None
minimalMem        = False
mergingCutsPath   = False
reUseInj          = False
xtmvaFlag         = False
offSourceInj      = False
doAsymmetricBackground    = 0
backgroundAsymmetryFactor = 0.5
longInjections    = False
smartCluster      = False
directedMode      = False
# ---- Guess the condor username.
UserName          = os.popen('whoami').read().rstrip()

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:g:r:d:i:n:t:f:m:sc:e:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "grb-time=",
   "right-ascension=",
   "declination=",
   "detector=",
   "grb-name=",
   "grid-type=",
   "priority=",
   "network-selection",
   "disable-fast-injections",
   "sky-pos-err=",
   "grid-sim-file=",
   "injdistrib=",
   "catalogdir=",
   "big-mem=",
   "end-offset=",
   "use-merging-cuts=",
   "reuse-inj",
   "xtmva",
   "off-source-inj",
   "long-inj",
   "smart-cluster",
   "directed-mode",
   "user="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- We will record the command line arguments to grb.py in a file called
#      grb.param.
#      This file is used by xgrbwebpage.py which expects the short form of
#      the options to have been used
command_string = 'grb.py '

# ---- Parse command-line arguments.  Arguments are returned as strings, so
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a
        command_string = command_string + ' -p ' + a
    elif o in ("-g", "--grb-time"):
        trigger_time = round(float(a))
        command_string = command_string + ' -g ' + a
    elif o in ("-r", "--right-ascension"):
        ra = float(a)
        command_string = command_string + ' -r ' + a
    elif o in ("-d", "--declination"):
        decl = float(a)
        command_string = command_string + ' -d ' + a
    elif o in ("-i", "--detector"):
        detector.append(a)
        command_string = command_string + ' -i ' + a
    elif o in ("-n", "--grb-name"):
        grb_name = a
        command_string = command_string + ' -n ' + a
    elif o in ("-t", "--grid-type"):
        grid_type = a
        command_string = command_string + ' -t ' + a
    elif o in ("-f", "--grid-sim-file"):
        grid_sim_file = a
        command_string = command_string + ' -f ' + a
    elif o in ("-s", "--network-selection"):
        network_selection = True
        command_string = command_string + ' -s '
    elif o in ("-c", "--catalogdir"):
        catalog_dir = a
        command_string = command_string + ' -c ' + a
    elif o in ("--use-merging-cuts"):
        mergingCutsPath = a
        command_string = command_string + ' --use-merging-cuts ' + a
    elif o in ("--reuse-inj"):
        reUseInj = True
        command_string = command_string + ' --reuse-inj '
    elif o in ("--xtmva"):
        xtmvaFlag = True
        command_string = command_string + ' --xtmva '
    elif o in ("--off-source-inj"):
        offSourceInj = True
        command_string = command_string + ' --off-source-inj '
    elif o in ("--priority"):
        condorPriority = a
        command_string = command_string + ' --priority ' + a
    elif o in ("--big-mem"):
        minimalMem = a
        command_string = command_string + ' --big-mem ' + a
    elif o in ("-e", "--sky-pos-err"):
        if grid_type == 'file':
            sky_pos_err = a
        else:
            sky_pos_err = float(a)
        command_string = command_string + ' -e ' + a
    elif o in ("--injdistrib"):
        injdistrib = a
        command_string = command_string + ' --injdistrib ' + a
    elif o in ("--end-offset"):
        manualEndOffset = int(math.ceil(float(a)))
        command_string = command_string + ' --end-offset ' + a
    elif o in ("--disable-fast-injections"):
        disableFastInjections = True
        command_string = command_string + ' --disable-fast-injections '
    elif o in ("--long-inj"):
        longInjections = True
        command_string = command_string + ' --long-inj '
    elif o in ("--smart-cluster"):
        smartCluster = True
        command_string = command_string + ' --smart-cluster '
    elif o in ("--directed-mode"):
        directedMode = True
        command_string = command_string + ' --directed-mode '
    elif o in ("--user"):
        UserName = a
        command_string = command_string + ' --user ' + a
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not trigger_time:
    print("No GRB trigger time specified.", file=sys.stderr)
    print("Use --grb-time to specify it.", file=sys.stderr)
    sys.exit(1)
if not ra:
    print("No right ascension specified.", file=sys.stderr)
    print("Use --right-ascension to specify it.", file=sys.stderr)
    sys.exit(1)
if not decl:
    print("No declination specified.", file=sys.stderr)
    print("Use --declination to specify it.", file=sys.stderr)
    sys.exit(1)
if not detector:
    print("No detectors specified.", file=sys.stderr)
    print("Use --detector to specify each detector in the network.", file=sys.stderr)
    sys.exit(1)
if not grb_name:
    print("No GRB name specified.", file=sys.stderr)
    print("Use --grb-name to specify name of GRB.", file=sys.stderr)
    sys.exit(1)
if reUseInj and not mergingCutsPath:
    print("Want to reuse injection path to previous tuning not provided", file=sys.stderr)
    print("Use --use-merging-cuts to specify the path to previous tuning", file=sys.stderr)
    sys.exit(1)

# ---- If were are running in directed mode then make sure some other options are disabled
#      and set the sky coordinated system to (ra,dec).
if directedMode:
    print("Directed mode requested. Turning off network_selection and sky_pos_err options.", file=sys.stdout)
    network_selection   = False
    sky_pos_err         = None
    skyCoordinateSystem = 'radec'
else:
    skyCoordinateSystem = 'earthfixed'
if directedMode and offSourceInj:
    print("Error: the --off-source-inj flag is inccompatible with the --directed-mode flag.", file=sys.stdout)
    sys.exit(1)

# ---- grb_name should have format e.g., "GRB070201"
#      we append GRB prefix if missing unless we are analysing a
#      MOCK GRB.
if not(grb_name.startswith('MOCK')) and not(grb_name.startswith('GRB')):
    grb_name = 'GRB' + grb_name

# ---- if grb_name begins with MOCK then we are doing a MOCK analysis.
if grb_name.startswith('MOCK'):
    mockAnalysis = 1
else:
    mockAnalysis = 0


# -------------------------------------------------------------------------
#      Get host name to determine where we are running.
# -------------------------------------------------------------------------

# ---- Get partial hostname.
os.system('hostname > host.txt')
f = open('host.txt','r')
hostname=f.read()
f.close()

# ---- Get full hostame.
os.system('hostname -f > host.txt')
f = open('host.txt','r')
fullhostname=f.read()
f.close()

# ---- Clean up.
os.system('rm host.txt')

# ---- Check hostname and set flag if necessary.
if 'atlas' in fullhostname:
    atlasFlag = 1
elif 'h2' in hostname:
    atlasFlag = 1
elif 'coma' in fullhostname:
    atlasFlag = 1
else:
    atlasFlag = 0


# -------------------------------------------------------------------------
#      Status message.  Report all supplied arguments.
# -------------------------------------------------------------------------

print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#              X-GRB Search Pipeline               #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print("        trigger time:", trigger_time, file=sys.stdout)
print("        trigger name:", grb_name, file=sys.stdout)
print("     right ascension:", ra, file=sys.stdout)
print("         declination:", decl, file=sys.stdout)
print("         sky pos err:", sky_pos_err, file=sys.stdout)
if injdistrib:
    print("          injdistrib:", injdistrib, file=sys.stdout)
print("    detector network:", detector, file=sys.stdout)
if network_selection:
    print("   network selection: automatic", file=sys.stdout)
print("           grid type:", grid_type, file=sys.stdout)
if grid_type == 'file':
    print("       grid sim file:", grid_sim_file, file=sys.stdout)
print("     condor priority:", condorPriority, file=sys.stdout)
if mergingCutsPath:
    print("   merging cuts from:", mergingCutsPath, file=sys.stdout)
if atlasFlag:
    print("    running on atlas: yes", file=sys.stdout)
if xtmvaFlag:
    print("       running XTMVA: yes", file=sys.stdout)
print("             user ID:", UserName, file=sys.stdout)
print(file=sys.stdout)

# ---- Write ASCII file holding grb.py command.
pfile = open('grb.param','w')
pfile.write(command_string + "\n")
pfile.close()

summary_file = 'grb_summary.txt'
# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t'.join(['\nname','\tgps','\tra','dec','network','numSky','numBG','analyse','\n']))
sfile.write('\t'.join([grb_name,str(trigger_time),str(ra),str(decl)]))
sfile.close()

# -------------------------------------------------------------------------
#      Preparatory.
# -------------------------------------------------------------------------

# ---- Generate unique id tag.
os.system('uuidgen > uuidtag.txt')
f = open('uuidtag.txt','r')
uuidtag=f.read()
f.close()
os.system('rm uuidtag.txt')

# ---- Record the current working directory in a string.
cwdstr = "."

# ---- Make directory to store text files (segment lists, parameter
#      files, etc.) that will be input to X-Pipeline.  This is done
#      lsto minimize clutter in the working directory.
try: os.mkdir( 'input' )
except: pass  # -- Kludge: should probably fail with error message.

# ---- Find files which define merging cuts
mergingCutsString = ""
print(mergingCutsPath)
if mergingCutsPath:
    preCutFile = glob.glob(mergingCutsPath + '/*xmake_args_tuned_pre.txt')
    cutFile = glob.glob(mergingCutsPath + '/*xmake_args_tuned.txt')
    origResults = glob.glob(mergingCutsPath + '/*closedbox.mat')
    mergingCutsString = cutFile[0] + " " + origResults[0]
    if preCutFile:
        mergingCutsString = mergingCutsString + " " + preCutFile[0]


# -------------------------------------------------------------------------
#      Read configuration file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)

# ---- Check the params_file exists
if not os.path.isfile(params_file):
    print("Error: non existent parameter file: ", \
        params_file, file=sys.stderr)
    sys.exit(1)

# ---- Create configuration-file-parser object and read parameters file.
cp = configparser.ConfigParser()
cp.read(params_file)

if cp.has_option('tags','version') :
    ini_version = cp.get('tags','version')
    print("Parameter file CVS tag:", ini_version, file=sys.stdout)

# ---- NOTE: The following reading of variables can be split up and
#      moved into the relevant sections of the script where the
#      variables are actually used.

# ---- Read needed variables from [parameters] and [background] sections.
if cp.has_option('background','backgroundPeriod') :
    background_period = int(cp.get('background','backgroundPeriod'))
if cp.has_option('background','doAsymmetricBackground') :
    doAsymmetricBackground   = int(cp.get('background','doAsymmetricBackground'))
if cp.has_option('background','backgroundAsymmetryFactor') :
    backgroundAsymmetryFactor = float(cp.get('background', 'backgroundAsymmetryFactor'))
blockTime         =  int(cp.get('parameters','blockTime'))
whiteningTime     =  float(cp.get('parameters','whiteningTime'))
transientTime     =  4 * whiteningTime
onSourceBeginOffset = int(cp.get('parameters','onSourceBeginOffset'))
onSourceEndOffset   = int(cp.get('parameters','onSourceEndOffset'))
onSourceEndOffset   = max(onSourceEndOffset,manualEndOffset)
onSourceTimeLength = onSourceEndOffset - onSourceBeginOffset
jobsPerWindow=int(math.ceil(float(onSourceTimeLength)/float(blockTime-2*transientTime)))
onSourceWindowLength=2*transientTime+jobsPerWindow*(blockTime-2*transientTime)
minimumFrequency = int(cp.get('parameters','minimumFrequency'));
maximumFrequency = int(cp.get('parameters','maximumFrequency'));
if cp.has_option('segfind','applyGates'):
    applyGates = int(cp.get('segfind','applyGates'))
    if applyGates:
        print("Will attempt to apply gating to this analysis.", file=sys.stdout)
else:
    applyGates = 0
    print("Running analysis without applying gates.", file=sys.stdout)

# ---- Interval of data to be analysed.
if directedMode:
    # ---- Interval of data to be analysed for directedMode includes transientTime buffers.
    #      Note that typically onSourceBeginOffset < 0 and onSourceEndOffset > 0.
    start_time = int(trigger_time + onSourceBeginOffset - transientTime)
    end_time = int(trigger_time + onSourceEndOffset + transientTime)
else:
    # ---- The standard GRB-style search allows for asymmetric background analyses, 
    #      typically used by the online analysis.
    if doAsymmetricBackground == 0:
        print("Running analysis with symmetric background", file=sys.stdout)
        start_time = int(trigger_time - background_period / 2)
        end_time = int(trigger_time + background_period / 2)
    else:
        # ---- Use asymmetric background.
        if backgroundAsymmetryFactor < 1.0:
            print("Running analysis with asymmetric background", file=sys.stdout)
            start_time = int(trigger_time - background_period*backgroundAsymmetryFactor)
            end_time = int(trigger_time + background_period*(1.0 - backgroundAsymmetryFactor))
        else:
            print("ERROR: Running analysis with asymmetric background but with wrong fractional asymmetry. backgroundAsymmetryFactor should be less than 1.0", file=sys.stderr)
            sys.exit(1)
duration = int(end_time - start_time)

# ---- We will copy all ifo data and mdc frame caches to the location
#      stored in frameCacheAll.
frameCacheAll = 'input/framecache.txt'

# ---- Read [input] channel parameters.
detectorListLine  = cp.get('input','detectorList')
detectorList      = detectorListLine.split(',')
virtualDetectorList = detectorList
if cp.has_option('input','virtualDetectorList') :
    virtualDetectorListLine   = cp.get('input','virtualDetectorList')
    virtualDetectorList       = virtualDetectorListLine.split(',')
    print(" ", file=sys.stdout)
    print("********************************************************************************", file=sys.stdout)
    print("  WARNING: using virtual detectors. ", file=sys.stdout)
    print("       real detector network:", detectorList, file=sys.stdout)
    print("    virtual detector network:", virtualDetectorList, file=sys.stdout)
    print("  The real detector list will be used for segments, frame finding, data quality", file=sys.stdout)
    print("  criteria, and calibration uncertainties. The virtual detector list will be", file=sys.stdout) 
    print("  used for the search sky grid and determining likelihoods, lag files, and MDC", file=sys.stdout) 
    print("  frames. The channels.txt file will contain both lists.", file=sys.stdout)
    print("********************************************************************************", file=sys.stdout)
    print(" ", file=sys.stdout)
channelListLine   = cp.get('input','channelList')
channelList       = channelListLine.split(',')
frameTypeListLine = cp.get('input','frameTypeList')
frameTypeList     = frameTypeListLine.split(',')

# ---- Variables that may or may not be defined in .ini file:

# ---- Check for frame cache for real ifo data
try:
    dataFrameCache = cp.get('input','frameCacheFile')
except:
    print("Warning: No frameCacheFile file specified in " \
        "[input] section of configuration file.", file=sys.stdout)
    print("        A frameCache for the ifo data file will be " \
        "generated automatically.", file=sys.stdout)
    dataFrameCache = None

# ---- Check for a seed value for matlab's random number generator.
try:
    seed = int(cp.get('parameters','seed'))
except:
    seed = 931316785
    print("Warning: No seed specified in configuration file.", file=sys.stdout)
    print("         seed will be set to: ", seed, file=sys.stdout)

# ---- Matlab seed can take values between 0 and 2^31-2.
if (seed > pow(2,31)-2) or (seed < 0):
    print("Error: seed must have value between 0 and 2^31-2", file=sys.stderr)
    sys.exit(1)

# ---- Get datafind server.
try:
    datafind_server = cp.get('datafind','datafind_server')
except:
    datafind_server = os.environ['LIGO_DATAFIND_SERVER']
# ---- Get datafind executable e.g., ligo_data_find.
datafind_exec = cp.get("datafind", 'datafind_exec')
# ---- Get segfind executable e.g., ligolw_segment_query.
segfind_exec = cp.get("datafind", "segfind_exec")
# ---- Get segs_from_cats executable e.g., ligolw_ligolw_segments_from_cats.
segs_from_cats_exec = cp.get("datafind", "segs_from_cats_exec")
# ---- Get ligolw_print executable e.g., ligolw_print.
ligolw_print_exec = cp.get("datafind", "ligolw_print_exec")

# ---- Status message.
print("... finished parsing parameters (ini) file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Validate list of detectors given.
# -------------------------------------------------------------------------

# ---- KLUDGE:TODO: Move this below automatic network selection so that we
#      only perform this once???

# ---- Status message.
print("Comparing requested network to list of known detectors ...", file=sys.stdout)

# ---- For each detector specified with the --detector option, compare to
#      the list of known detectors from the .ini file.  Keep only the requested
#      detectors and corresponding channel name and frame type.
#      KLUDGE:TODO: Should exit with error message if any of the detectors
#      is not recognized.
# ---- Indices of the detectors requested for this analysis.
keepIndex = []
for i in range(0,len(detector)) :
    for ii in range(0,len(detectorList)) :
        if detector[i] == detectorList[ii] :
            keepIndex.append(ii)
# ---- Sort indices so that order matches that used in ini file.
keepIndex.sort()
# ---- We now have a list of the indices of the detectors requested for the
#      analysis.  Keep only these.  Note that we over-write the 'detector'
#      list because we want to make sure the order matches the channel and
#      frameType lists.
detector  = []
virtualDetector  = []
channel   = []
frameType = []
for jj in range(0,len(keepIndex)):
    detector.append(detectorList[keepIndex[jj]])
    virtualDetector.append(virtualDetectorList[keepIndex[jj]])
    channel.append(channelList[keepIndex[jj]])
    frameType.append(frameTypeList[keepIndex[jj]])

# ---- Status message.
print("... finished validating network.          ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Write Matlab-formatted files required by the seedless analysis.
# -------------------------------------------------------------------------

outputType=cp.get('parameters','outputType')
if outputType == 'seedless':
    # ---- Status message.
    print("Writing Matlab-formatted clustering file ...", file=sys.stdout)
    doGPUOnSource = cp.get('seedless','doGPUOnSource')
    doGPUOffSource = cp.get('seedless','doGPUOffSource')
    doGPUSimulation = cp.get('seedless','doGPUSimulation')
    doGPUUL = cp.get('seedless','doGPUUL')
    doParallelOnSource = cp.get('seedless','doParallelOnSource')
    doParallelOffSource = cp.get('seedless','doParallelOffSource')
    doParallelSimulation = cp.get('seedless','doParallelSimulation')
    doParallelUL = cp.get('seedless','doParallelUL')
    T = cp.get('seedless','T')
    F = cp.get('seedless','F')
    mindur = cp.get('seedless','mindur')
    doPCA = cp.get('seedless','doPCA')
    pca_catalogfile = cp.get('seedless','pca_catalogfile')
    pca_type = cp.get('seedless','pca_type')
    doCBC = cp.get('seedless','doCBC')
    doECBC = cp.get('seedless','doECBC')
    doBezier = cp.get('seedless','doBezier')
    doExponential = cp.get('seedless','doExponential')
    doRModes = cp.get('seedless','doRModes')
    norm = cp.get('seedless','norm')
    savePlots = cp.get('seedless','savePlots')

    f = open('input/seedless_onsource.txt','w')
    f.write('doGPU:%s\n'%(doGPUOnSource))
    f.write('doParallel:%s\n'%(doParallelOnSource))
    f.write('T:%s\n'%(T))
    f.write('F:%s\n'%(F))
    f.write('mindur:%s\n'%(mindur))
    f.write('doCBC:%s\n'%(doCBC))
    f.write('doECBC:%s\n'%(doECBC))
    f.write('doBezier:%s\n'%(doBezier))
    f.write('doExponential:%s\n'%(doExponential))
    f.write('doRModes:%s\n'%(doRModes))
    f.write('doPCA:%s\n'%(doPCA))
    f.write('pca_catalogfile:%s\n'%(pca_catalogfile))
    f.write('pca_type:%s\n'%(pca_type))
    f.write('norm:%s\n'%(norm))
    f.write('savePlots:%s\n'%(savePlots))     
    f.close()

    f = open('input/seedless_offsource.txt','w')
    f.write('doGPU:%s\n'%(doGPUOffSource))
    f.write('doParallel:%s\n'%(doParallelOffSource))
    f.write('T:%s\n'%(T))
    f.write('F:%s\n'%(F))
    f.write('mindur:%s\n'%(mindur))
    f.write('doCBC:%s\n'%(doCBC))
    f.write('doECBC:%s\n'%(doECBC))
    f.write('doBezier:%s\n'%(doBezier))
    f.write('doExponential:%s\n'%(doExponential))
    f.write('doRModes:%s\n'%(doRModes))
    f.write('doPCA:%s\n'%(doPCA))
    f.write('pca_catalogfile:%s\n'%(pca_catalogfile))
    f.write('pca_type:%s\n'%(pca_type))
    f.write('norm:%s\n'%(norm))
    f.write('savePlots:%s\n'%(savePlots))
    f.close()

    f = open('input/seedless_simulation.txt','w')
    f.write('doGPU:%s\n'%(doGPUSimulation))
    f.write('doParallel:%s\n'%(doParallelSimulation))
    f.write('T:%s\n'%(T))
    f.write('F:%s\n'%(F))
    f.write('mindur:%s\n'%(mindur))
    f.write('doCBC:%s\n'%(doCBC))
    f.write('doECBC:%s\n'%(doECBC))
    f.write('doBezier:%s\n'%(doBezier))
    f.write('doExponential:%s\n'%(doExponential))
    f.write('doRModes:%s\n'%(doRModes))
    f.write('doPCA:%s\n'%(doPCA))
    f.write('pca_catalogfile:%s\n'%(pca_catalogfile))
    f.write('pca_type:%s\n'%(pca_type))
    f.write('norm:%s\n'%(norm))
    f.write('savePlots:%s\n'%(savePlots))
    f.close()

    f = open('input/seedless_ul.txt','w')
    f.write('doGPU:%s\n'%(doGPUUL))
    f.write('doParallel:%s\n'%(doParallelUL))
    f.write('T:%s\n'%(T))
    f.write('F:%s\n'%(F))
    f.write('mindur:%s\n'%(mindur))
    f.write('doCBC:%s\n'%(doCBC))
    f.write('doECBC:%s\n'%(doECBC))
    f.write('doBezier:%s\n'%(doBezier))
    f.write('doExponential:%s\n'%(doExponential))
    f.write('doRModes:%s\n'%(doRModes))
    f.write('doPCA:%s\n'%(doPCA))
    f.write('pca_catalogfile:%s\n'%(pca_catalogfile))
    f.write('pca_type:%s\n'%(pca_type))
    f.write('norm:%s\n'%(norm))
    f.write('savePlots:%s\n'%(savePlots))
    f.close()


# -------------------------------------------------------------------------
#      Retrieve single-IFO segment lists for analysis period.
# -------------------------------------------------------------------------

# ---- Write time range to a temporary segment file named gps_range.txt.
#      We'll then read this file into a ScienceData object.  It's a hack, but
#      it seems that the only way to populate ScienceData objects is to read
#      segments from a file.
f=open('input/gps_range.txt', 'w')
time_range_string = '1 ' + str(start_time) + ' ' + str(end_time) + ' ' + str(duration) + '\n'
f.write(time_range_string)
f.close()
# ---- Read full analysis time range back in to a ScienceData object for easy manipulation.
analysis_segment = pipeline.ScienceData()
analysis_segment.read( 'input/gps_range.txt', blockTime )

# ---- Prepare storage for full segment lists and gating filenames.
full_segment_list = []
gating_files      = []

# ---- If simulating noise skip all segment and veto related stuff. Make fake 
#      segment list covering full analysis period, plus 1000 sec padding on 
#      either side, and use no DQ vetoes.
if cp.has_option('parameters','makeSimulatedNoise'):
    print("WARNING: Simulated detector noise option may not work with virtual detector option.", file=sys.stdout)
    # ---- Shut off gating, with a warning.
    if applyGates:
        applyGates = 0
        print("WARNING: Turning off gating with simulated data.", file=sys.stdout)

    # ---- Generate segment files. These consist of analysis_seg_files (listing
    #      science-cat1 times) and veto_seg_files (listing cat2+cat4 times).
    #      The following calls generate these files by reading from the .ini 
    #      file or by querying the segment database. The returned arguments are
    #      lists of filenames stored in the input/ directory.
    print('Generating science-mode and data-quality segments. This may take several minutes ...')
    analysis_seg_files = segmentutils.validate_segments(detector, start_time, end_time, cp, trigger_time)
    veto_seg_files = segmentutils.validate_vetos(detector, start_time, end_time, cp)
    print('... finished.')

else:
    # ---- See if user has supplied gating files.
    if applyGates:
        for ifoIdx in range(0,len(detector)):
            ifo = detector[ifoIdx]
            if cp.has_option(ifo, "gate-list"):
                gating_files.append(cp.get(ifo,"gate-list"))
                if not os.path.isfile(gating_files[ifoIdx]):
                    print("Non existent gating file for ifo ", ifoIdx, file=sys.stderr)
                    sys.exit(1)
            else:
                # ---- Create and point to an empty file (will be treated as no gates 
                #      for that IFO).
                os.system("touch input/empty.txt")
                gating_files.append("input/empty.txt")
        f=open('input/gates.txt', 'w')
        f.write("\n".join(gating_files))
        f.close()

    # ---- Generate segment files. These consist of analysis_seg_files (listing
    #      science-cat1 times) and veto_seg_files (listing cat2+cat4 times).
    #      The following calls generate these files by reading from the .ini 
    #      file or by querying the segment database. The returned arguments are
    #      lists of filenames stored in the input/ directory.
    print('Generating science-mode and data-quality segments. This may take several minutes ...')
    analysis_seg_files = segmentutils.validate_segments(detector, start_time, end_time, cp)
    veto_seg_files = segmentutils.validate_vetos(detector, start_time, end_time, cp)
    print('... finished.')


# ---- Read the science-cat1 lists into the full_segment_list list.
for ifoIdx in range(0,len(detector)):
    ifo = detector[ifoIdx]

    # ---- The gwpy-based segment generation codes print the segment duration
    #      as a float rather rather than an integer (e.g. 5.0 rather than 5).
    #      This causes the full_segment.read() command below to fail. We 
    #      therefore pass the file through awk to recompute the duration as an
    #      integer, skipping over the header line. We write the modified file
    #      to input/ because if we are reading someone else's files we won't
    #      have permission to over-write them.
    print('Casting segment data from', analysis_seg_files[ifoIdx], 'to int.')
    new_file_name = 'input/' + ifo + '_science_cat1.txt'
    command = ' '.join(["awk '{if (NR==1) print $0; else print $1, $2, $3, $3-$2}' ", analysis_seg_files[ifoIdx], "  > tmp_file ; mv tmp_file ", new_file_name])
    os.system(command)
    analysis_seg_files[ifoIdx] = new_file_name 
    print('Casting veto data from', veto_seg_files[ifoIdx], 'to int.')
    new_file_name = 'input/' + ifo + '_cat24veto.txt'
    command = ' '.join(["awk '{if (NR==1) print $0; else print $1, $2, $3, $3-$2}' ", veto_seg_files[ifoIdx], "  > tmp_file ; mv tmp_file ", new_file_name])
    os.system(command)
    veto_seg_files[ifoIdx] = new_file_name 
    print('... finished reformatting segment and veto lists for', ifo, '.')

    # ---- Read full segment list into a ScienceData object for easy
    #      manipulation.  Throw away science segments shorter than the
    #      blockTime value read from the configuration file.
    print('Reading segment list from file ', analysis_seg_files[ifoIdx])
    full_segment = pipeline.ScienceData()
    full_segment.read( analysis_seg_files[ifoIdx], blockTime )
    print('... finished reading segment list.')

    # ---- Now restrict to desired analysis time range around trigger_time.
    full_segment.intersection(analysis_segment)
    # ---- Finally, append segment for this detector to the full list.
    full_segment_list.append(full_segment)


# -------------------------------------------------------------------------
#      Choose detector network based on on-source data quality.
# -------------------------------------------------------------------------

# ---- Pulled the segment lists stuff outside of the "if network_selection"
#      statement since we need these to test the off-source segments.
# ---- Get cat1 and cat2 segment files for all 5 ifos.
#      Use 'None' when no file exists.
all_detectors = ['H1', 'K1', 'L1', 'G1', 'V1']
cat1_segment_file_list = []
cat2_segment_file_list = []
for ifo in all_detectors:
    try:
        # ----- Did we consider current ifo.
        idx = detector.index(ifo)
        cat1_segment_file_list.append(analysis_seg_files[idx])
        cat2_segment_file_list.append(veto_seg_files[idx])
    except (ValueError):
        cat1_segment_file_list.append("None")
        cat2_segment_file_list.append("None")

# ---- Write string listing detectors
detectorStr = ''
detectorStrTilde = ''
for ifo in detector:
    detectorStr = detectorStr + ifo
    detectorStrTilde = detectorStrTilde + ifo + '~'
detectorStrTilde = detectorStrTilde[0:len(detectorStrTilde)-1]

if network_selection and blockTime < 256:
    print("Warning: network selection does not currently ", \
        "work for blockTimes < 256s \n", file=sys.stderr)

if network_selection :
    print("\nSelecting ifo network from ", detector, file=sys.stdout)

    # ---- Write file containing GRB trigger time.
    ftriggertime=open('input/trigger_time.txt', 'w')
    ftriggertime.write("%f\n"%trigger_time)
    ftriggertime.close()

    # ---- Write file containing time offsets for on-source.
    ftimeoffsets=open('input/time_offsets.txt', 'w')
    for ifo in detector:
        ftimeoffsets.write("0 ")
    ftimeoffsets.close()

    network_outputFile = 'input/xnetworkselection_onsource.dat'

    xnetworkselectionCall = ' '.join([ "xnetworkselection",
        detectorStr,
        "input/trigger_time.txt",
        "input/time_offsets.txt",
        cat1_segment_file_list[0],
        cat1_segment_file_list[1],
        cat1_segment_file_list[2],
        cat1_segment_file_list[3],
        cat1_segment_file_list[4],
        cat2_segment_file_list[0],
        cat2_segment_file_list[1],
        cat2_segment_file_list[2],
        cat2_segment_file_list[3],
        cat2_segment_file_list[4],
        network_outputFile,
        str(onSourceBeginOffset),
        str(onSourceEndOffset),
        str(transientTime),
        str(onSourceWindowLength)
        ])

    print(xnetworkselectionCall, file=sys.stdout)
    os.system(xnetworkselectionCall)

    fnet = open(network_outputFile)
    network_selection_on = fnet.read()
    fnet.close()

    if network_selection_on.endswith("\n"):
        network_selection_on = network_selection_on[0:len(network_selection_on)-1]
    detectorStr = network_selection_on

    # ---- Figure out which if our ifos made it into the network.
    network_selection_on_list = []
    network_virtual_on_list   = []
    retained_ifo_indices      = []
    full_segment_list_updated = []
    channel_updated           = []
    frameType_updated         = []
    # ---- skip GRB if the network is not well determined
    if network_selection_on[0] != 'X':
        # ---- Loop over the ifos we originally considered.
        for ifoIdx in range(0,len(detector)):
            ifo = detector[ifoIdx]
            # ---- If this ifo is in our new network.
            if network_selection_on.count(ifo):
                network_selection_on_list.append(ifo)
                network_virtual_on_list.append(virtualDetector[ifoIdx])
                full_segment_list_updated.append(full_segment_list[ifoIdx])
                retained_ifo_indices.append(ifoIdx)
                channel_updated.append(channel[ifoIdx])
                frameType_updated.append(frameType[ifoIdx])

    # ---- Update channel list.
    channel = channel_updated;

    # ---- Update frameType list.
    frameType = frameType_updated;

    # ---- Update list of segments.
    full_segment_list = full_segment_list_updated

    # ---- Update list of ifos we are using.
    detector = network_selection_on_list

    # ---- Update list of virtual ifos we are using.
    virtualDetector = network_virtual_on_list

# -------------------------------------------------------------------------
#      Choose appropriate likelihoods and lags for this network.
# -------------------------------------------------------------------------

# ---- Append to summary file.
#      KLUDGE / WARNING: Note that real detectors are listed, rather than virtual.
sfile = open(summary_file,'a')
sfile.write('\t' + detectorStr)
sfile.close()

print(" ", file=sys.stdout)

if len(detector) ==0:
    print("Error: No ifos in network!!!", file=sys.stderr)
    sys.exit(1)

elif len(detector) ==1:
    print("One ifo in network: ", detector, file=sys.stdout)
    lagType = []
    likelihoodType = "likelihoodType_1det1site"

elif len(detector) ==2:
    print("Two ifos at two sites: ", virtualDetector, file=sys.stdout)
    lagType = "lags_2det2site"
    likelihoodType = "likelihoodType_2det2site"

elif len(detector) ==3:
    print("Three ifos at three sites: ", virtualDetector, file=sys.stdout)
    lagType = "lags_3det3site"
    likelihoodType = "likelihoodType_3det3site"

elif len(detector) ==4:
    print("Four ifos at four sites: ", virtualDetector, file=sys.stdout)
    lagType = "lags_4det4site"
    likelihoodType = "likelihoodType_4det4site"

elif len(detector) ==5:
    print("Five ifos at five sites: ", virtualDetector, file=sys.stdout)
    lagType = "lags_5det5site"
    likelihoodType = "likelihoodType_5det5site"

# -----------------------------------------------------------------------------
#               Construct tilde-separated list of sites
# -----------------------------------------------------------------------------

# ---- Initialise tilde-separated list of sites.
siteStrTilde = ''
for iDet in range(0,len(virtualDetector)):
    # ---- Get name of current site from current (virtual) detector.
    siteTemp = virtualDetector[iDet][0]
    # ---- Add current site to list only if does not already appear in it.
    if siteStrTilde.count(siteTemp)==0:
        siteStrTilde = '~'.join([siteStrTilde,siteTemp])

# ---- Remove preceding tilde.
siteStrTilde = siteStrTilde[1:len(siteStrTilde)]

# -----------------------------------------------------------------------------
#               Construct tilde-separated list of detectors
# -----------------------------------------------------------------------------

# ---- Real detector list: for reading frames, segments, etc.
detectorStr = ''
detectorStrTilde = ''
for ifo in detector:
    detectorStr = detectorStr + ifo
    detectorStrTilde = detectorStrTilde + ifo + '~'
detectorStrTilde = detectorStrTilde[0:len(detectorStrTilde)-1]

# ---- Real frame type, for reading frames.
frameTypeStrTilde = ''
for frameTypeName in frameType:
    frameTypeStrTilde = frameTypeStrTilde + frameTypeName + '~'
frameTypeStrTilde = frameTypeStrTilde[0:len(frameTypeStrTilde)-1]

# -----------------------------------------------------------------------------
#         Having figured out network read in appropriate lag file.
# -----------------------------------------------------------------------------

# ---- We only need to read in a lag file if our network has 2 or more ifos.
if lagType:
    try:
        lagFile =  cp.get('background',lagType)
    except:
        print("Warning: No lagFile specified in configuration file.", file=sys.stdout)
        print("         No time lag jobs will be made.", file=sys.stdout)
        lagFile =  None

    if lagFile:
        if not os.path.isfile(lagFile):
            print("Error: non existant lag file: ",lagFile, file=sys.stderr)
            sys.exit(1)
else:
    lagFile = None

print("Using lag file: ", lagFile, file=sys.stdout)


# -----------------------------------------------------------------------------
#         Having figured out network read in appropriate likelihood types.
# -----------------------------------------------------------------------------

try:
    likelihoodTypeStr =  cp.get('parameters',likelihoodType)
except:
    print("Error: required likelihoodType not specified in " \
       "configuration file.", file=sys.stderr)
    sys.exit(1)

print("Using likelihoods: ", likelihoodTypeStr, file=sys.stdout)

# -----------------------------------------------------------------------------
#      Write Matlab-formatted parameters files.
# -----------------------------------------------------------------------------

# ---- We will write three sets of parameters files:  one on-source file,
#      one off-source file, and (for each waveform set and injection scale)
#      one injections file.

# ---- Status message.
print("Writing Matlab-formatted parameter files ...", file=sys.stdout)

# ---- Write all options available in the parameters section to a file.
parameters = cp.options('parameters')


# ---- Parameter files for MDC waveform analyses, if requested.
if cp.has_option('mdc','mdc_sets') :
    # ---- Read [mdc] sets and injection scales.  If mdc_sets is empty or specifies
    #      unknown MDC sets then the script will have already exited when trying to
    #      write the mdcchannel file above.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    # ---- Write one parameters file for each (mdc set, injection scale) pair.
    for set in mdc_sets :
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            # ---- This check lets you specify different injection scales for
            #      each MDC set.
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else:
            # ---- Otherwise, use the injection scales specified in
            #      the [injection] section.
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')

        # ---- Write a separate parameter file for each injection scale.
        scale_counter = 0
        for injectionScale in injectionScales :
            f=open("input/parameters_" + set + "_" + str(scale_counter) + ".txt", 'w')
            # ---- First write framecache file, channel file, event file, and sky position.
            f.write('channelFileName:input/channels.txt' + '\n')
            f.write('frameCacheFile:' + frameCacheAll + '\n')
            f.write('eventFileName:input/event_'+ set + '.txt' + '\n')
            f.write('skyPositionList:' + skyPositionList + '\n')
            f.write('skyCoordinateSystem:' + skyCoordinateSystem + '\n')
            f.write('likelihoodtype:' + likelihoodTypeStr + '\n')
            # ---- Now write all of the other parameters from the parameters section.
            #      We ignore the likelihoodType_* lines since this is handled above.
            for i in range(0,len(parameters)) :
                if not(parameters[i].startswith("likelihoodtype")):
                    value = cp.get('parameters',parameters[i])
                    if parameters[i] == "outputtype"  and value == "clusters" and not(disableFastInjections):
                        f.write('outputtype:injectionclusters\n')
                    elif parameters[i] == "onsourceendoffset":
                        f.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
                    elif parameters[i] == "circtimeslidestep":
                        continue
                    else:
                        f.write(parameters[i] + ':' + value + '\n')
            # ---- Write mdc info.
            f.write('mdcChannelFileName:input/channels_' + set + '.txt' + '\n')
            f.write('injectionFileName:input/injection_' + set + '.txt' + '\n')
            f.write('injectionScale:' + str(injectionScale) + '\n')
            if outputType == 'seedless' :
                f.write('seedlessparams:input/seedless_simulation.txt\n')
            f.close()
            scale_counter = scale_counter + 1

# ---- Status message.
print("... finished writing parameter files.     ", file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Write channel file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing Matlab-formatted channel file ...      ", file=sys.stdout)

# ---- For each detector, write the corresponding channel name, frame type,
#      and virtual channel name to the channels file to be read by xdetection.
f=open('input/channels.txt', 'w')
for i in range(0,len(detector)) :
    f.write(detector[i] + ':' + channel[i] + ' ' + frameType[i] + ' ' + virtualDetector[i] + ':' + channel[i] + '\n')
f.close()

# ---- Status message.
print("... finished writing channel file.        ", file=sys.stdout)
print(file=sys.stdout)

# -------------------------------------------------------------------------
#      Determine MDCs to process, write mdcChannelFiles.
# -------------------------------------------------------------------------

# ---- Check for MDC sets.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing Matlab-formatted MDC channel files "\
        "and framecache files... ", file=sys.stdout)

    # ---- Get list of MDC sets to process.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    # print >> sys.stdout, "mdc_sets:", mdc_sets

    # ---- We will create a list of all the log files for each mdc set
    mdc_log_files = []

    # ---- Make MDC channels file for each set.
    for setIdx in range(len(mdc_sets)) :
        set = mdc_sets[setIdx]
        # print >> sys.stdout, "set:", set
        if cp.has_section(set) :
            # ---- Read channel parameters for this mdc set.
            mdcChannelListLine = cp.get(set,'channelList')
            mdcChannelList = mdcChannelListLine.split(',')
            mdcFrameTypeListLine = cp.get(set,'frameTypeList')
            mdcFrameTypeList = mdcFrameTypeListLine.split(',')
            numberOfChannels = cp.get(set,'numberOfChannels')
            # ---- Keep only info for detectors requested for the analysis.
            #      WARNING / KLUDGE / ERROR - Wrong channels will be assigned 
            #      if any detectors have been dropped due to DQ problems (using
            #      the -s flag). This is because keepIndex was assigned around
            #      lines 625-9 based on requested real detectors before applying 
            #      any DQ or segment requirements, and has not been updated 
            #      when detectors are dropped due to DQ issues. Also, the 
            #      mdcChannelList must be updated to drop "bad" detectors.
            mdcChannel = []
            mdcFrameType = []
            for jj in range(0,len(keepIndex)):
                mdcChannel.append(mdcChannelList[keepIndex[jj]])
                mdcFrameType.append(mdcFrameTypeList[keepIndex[jj]])

            # ---- For each detector, write the
            #      corresponding channel name and frame type to a file.
            f=open('input/channels_' + set + '.txt', 'w')
            for i in range(0,len(detector)) :
                # ---- if we are doing a MOCK analysis we must not add on
                #      grb_name to mdcFrameType.
                if not(mockAnalysis):
                    # ---- mdcFrameTypes have format: H1_SGC554Q8d9_ON_GRB060211B
                    # ---- from params file mdcFrameTypes of format: H1_SGC554Q8d9_ON_
                    #      we append last underscore if missing and append grb_name.
                    if not(mdcFrameType[i].endswith('_')):
                        mdcFrameType[i] = mdcFrameType[i] + '_'
                    mdcFrameType[i] = mdcFrameType[i] + grb_name

                f.write(detector[i] + ':' + mdcChannel[i] + ' ' + mdcFrameType[i] + '\n')

            f.close()
            # ---- end loop over ifo

            # -------------------------------------------------------------------------
            #                         Find MDC frames.
            # -------------------------------------------------------------------------

            # ---- Check to see if mdc frame cache has been specified in config file.
            try:
                mdcFrameCache = cp.get(set,'frameCacheFile')
            except:
                print("Warning: No frameCacheFile file specified " \
                    "in [" + set + "] section of configuration file.", file=sys.stdout)
                print("         A frameCache for the ifo data file " \
                    "will be generated automatically.", file=sys.stdout)
                mdcFrameCache = None

            if mdcFrameCache:
                # ---- Check that the frame cache file specified actually exists.
                if not os.path.isfile(mdcFrameCache):
                    print("Error: non existant framecache: ", mdcFrameCache, file=sys.stderr)
                    sys.exit(1)

                # ---- If the specified mdc frame cache exists then concat it
                #      other frame caches.
                command = 'cat ' + mdcFrameCache + '  >> ' + frameCacheAll
                os.system(command)

            # ---- If the mdcFrameCache was not specified we generate it here.
            #      Note that we look for frames using the virtual detector name
            #      to ensure the MDC injection data is consistent with the 
            #      detector being simulated.
            else:
                for i in range(0,len(detector)) :
                    # ---- generate frame cache file for MDCs
                    if not mdcFrameCache:
                        # ---- Status message.
                        print("Writing MDC framecache file for " \
                            "mdcset: " + set + ", ifo: " + virtualDetector[i] + " ...", file=sys.stdout)
                        # ---- Clear away any pre-existing mdcframecache files.
                        os.system('rm -f mdcframecache_temp.txt')

                        # ---- Construct dataFind command.
                        dataFindCommand = ' '.join([datafind_exec,
                        "--observatory",virtualDetector[i][0],
                        "--type",mdcFrameType[i],
                        "--gps-start-time", str(start_time),
                        "--gps-end-time",str(end_time),
                        "--url-type file",
                        "--lal-cache",
                        " > mdclalcache.txt"])
                        # ---- Issue dataFind command.
                        print("calling dataFind:", dataFindCommand)
                        os.system(dataFindCommand)
                        print("... finished call to dataFind.")

                        # ---- Convert lalframecache file to readframedata format.
                        print("calling convertlalcache:")
                        os.system('convertlalcache.pl mdclalcache.txt mdcframecache_temp.txt')
                        os.system('cat mdcframecache_temp.txt >> ' + frameCacheAll)
                        print("... finished call to convertlalcache.")
                        # ---- Clean up.
                        os.system('rm -f mdcframecache_temp.txt mdclalcache.txt')

                        # ---- Status message.
                        print("... finished writing MDC framecache file.", file=sys.stdout)
                        print(file=sys.stdout)

            #-------------------------------------------------------------------------
            #   Create a list of the mdc log files and create an mdc segment list.
            #-------------------------------------------------------------------------

            if not(mdc_path):
                print("Error: mdc_path must be specified if we are using mdcs.", file=sys.stderr)
                print("Use --mdc-path to specify it.", file=sys.stderr)
                sys.exit(1)

            # ---- check dir names end in '/'
            if not(mdc_path.endswith('/')):
                mdc_path = mdc_path + '/'
            mdc_grbdir = grb_name + '/'

            # ---- mdcFrameType[i] will have name in format H1_SGC100Q8d9_ON_GRB051105
            #      logs are in dir structure:
            #      /data/node5/eharstad/ExtTrigMDC/GRB051105/SGC1000Q8d9_ON/logs/
            #      ExtTrigMDC-SGC1000Q8d9_ON_GRB051105-815207086-256-Log.txt

            # ---- Remove <ifo>_ and _GRB* part from mdcFrameType.
            if mockAnalysis:
                strList0    = mdcFrameType[i].rsplit('-')
                mdcType     = strList0[1]
            else:
                strList0    = mdcFrameType[i].rsplit('_')
                mdcType     = strList0[1] + '_' + strList0[2]

            mdc_log_glob = mdc_path + grb_name + '/'  + mdcType +  '/logs/ExtTrigMDC-' + mdcType + '*.txt'
            print('', file=sys.stdout)
            print('Getting mdc log files from ' + mdc_log_glob, file=sys.stdout)
            mdc_log_files.append(glob.glob(mdc_log_glob))

            # ---- Report error if no log files found.
            if not(len(mdc_log_files[setIdx])):
                print('Error: No ExtTrigMDC*.txt log files in ' + mdc_log_glob, file=sys.stderr)
                sys.exit(1)

            # ---- If these are on source mdcs check we only have one log file.
            if mdcType.count('ON'):
                if len(mdc_log_files[setIdx]) > 1:
                    print('Error: We only expect one ON source ExtTrigMDC*.txt log file in ' + mdc_log_glob, file=sys.stderr)
                    sys.exit(1)

            # ---- Extract start time of each mdc block from its log file name
            #      use this to create a segment list which we later use to
            #      figure out which mdc blocks have good data quality flags.
            mdc_segs = []
            for fileIdx in range(0,len(mdc_log_files[setIdx])):
                # ---- We need to extract start times for each log file.
                # ---- We are in a loop over mdc sets.
                strList = mdc_log_files[setIdx][fileIdx].rsplit('-')

                # ---- Choose element of strList counting back from end of strList in case
                #      dir names contain a hyphen which would throw off our counting.
                mdc_start_time = int(strList[len(strList)-3])
                mdc_block_time = int(strList[len(strList)-2])
                mdc_segs.append([mdc_start_time,mdc_block_time])

            # ---- Sort our segments on mdc_start_time.
            mdc_segs_sorted=sorted(mdc_segs, key=operator.itemgetter(0))

            # ---- Write mdc segtment list.
            fmdcseg=open('input/segment_' + set + '.txt', 'w')
            for segIdx in range(0,len(mdc_segs_sorted)):
                mdc_start_time = mdc_segs_sorted[segIdx][0]
                mdc_block_time = mdc_segs_sorted[segIdx][1]
                mdc_end_time   = mdc_start_time + mdc_block_time
                time_range_string = str(segIdx) + ' ' + \
                    str(mdc_start_time) + ' ' + str(mdc_end_time)  + \
                    ' ' + str(mdc_block_time) + '\n'
                fmdcseg.write(time_range_string)
            fmdcseg.close()

        else:
            print("Error: MDC set ", set, \
                " is not defined in the parameters file.  Exiting.", file=sys.stdout)
            print(file=sys.stdout)
            sys.exit(1)

    # ---- Status message.
    print("... finished writing MDC channel and frame files.   ", file=sys.stdout)
    print(file=sys.stdout)

# -------------------------------------------------------------------------
#    Make coincidence segment list for MDCs.
# -------------------------------------------------------------------------

# ---- Check for MDC sets.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing MDC event files ...          ", file=sys.stdout)

    # ---- Get list of MDC sets to process.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')

    for set in mdc_sets:
        # ---- Read mdc segments into a "ScienceData" object.
        mdc_segment = pipeline.ScienceData()
        mdc_segment.read('input/segment_' + set + '.txt', blockTime )

        # ---- Now get the intersection of all of the detector segment lists with this
        #      on-source list. Note that the segment lists mst be sorted by start time
        #      for intersection to work properly
        mdc_coincidence_segment = copy.deepcopy(mdc_segment)
        for det in full_segment_list:
            mdc_coincidence_segment.intersection(det)

        # ---- Do some checking... check we have some MDCs in coinc...

        # ---- At this point, the ScienceData object mdc_segment contains the
        #      mdc segments.  Write this to the mdc event file.
        f=open('input/event_' + set + '.txt', 'w')
        # ---- We also rewrite our segment files so they only contain the segs
        #      we are going to analyse
        fmdcseg=open('input/segment_' + set + '.txt','w')
        # ---- loop over coinc segs
        for i in range(mdc_coincidence_segment.__len__()):
            duration = mdc_coincidence_segment.__getitem__(i).end() - \
                mdc_coincidence_segment.__getitem__(i).start()
            if (duration == blockTime):
                time_range_string = str(mdc_coincidence_segment.__getitem__(i).start() \
                    + blockTime / 2) + '\n'
                f.write(time_range_string)
                time_range_string = str(i) + ' ' + \
                    str(mdc_coincidence_segment.__getitem__(i).start()) \
                    + ' ' + str(mdc_coincidence_segment.__getitem__(i).end())  \
                    + ' ' + str(mdc_coincidence_segment.__getitem__(i).end() \
                    -mdc_coincidence_segment.__getitem__(i).start()) + '\n'
                fmdcseg.write(time_range_string)
            elif (duration > blockTime):
                # ---- this should not be possible
                print("Error: something has gone wrong in creating MDC segment list ", file=sys.stderr)
                sys.exit(1)

        f.close()
        fmdcseg.close()

    # ---- Status message.
    print("... finished writing MDC event files.", file=sys.stdout)
    print(file=sys.stdout)

    # -------------------------------------------------------------------------
    #                Write log files for  off-source MDCs.
    # -------------------------------------------------------------------------

    # ---- Now we know which mdc log files we will need lets cat them to create
    #      a single mdc log file containing all the injections we will analyse
    # ---- Status message.
    print("Writing off-source MDC log files ...          ", file=sys.stdout)

    mdc_log_file_concat = []
    for setIdx in range(len(mdc_sets)):
        set = mdc_sets[setIdx]
        mdc_log_file_concat.append('input/injection_'+set+'.txt')

        # ---- loop over the mdc blocks we will analyse
        for i in range(mdc_coincidence_segment.__len__()):
            mdc_start_time = mdc_coincidence_segment.__getitem__(i).start()

            for mdc_log_file in mdc_log_files[setIdx]:
                if mdc_log_file.count(str(mdc_start_time)):
                    command = 'cat ' + mdc_log_file + ' >> ' + mdc_log_file_concat[setIdx]
                    os.system(command)


    # ---- Status message.
    print("... finished writing off-source MDC event file.", file=sys.stdout)
    print(file=sys.stdout)


# ------------------------------------------------------------------------------
#    Generate sky positions for patch about ra, dec at trigger_time.
# ------------------------------------------------------------------------------

if sky_pos_err:
    skyPosFilename_trigger = 'input/sky_positions_trigger_time.txt'

    if grid_type == 'timedelay':
        print("Calling xchoosetimedelays to generate ", skyPosFilename_trigger, file=sys.stdout)
        skygridcommand = ' '.join([ "xchoosetimedelays",
                                     str(ra),
                                     str(decl),
                                     str(trigger_time),
                                     siteStrTilde,
                                     cp.get('skygrid','delayTol'),
                                     skyPosFilename_trigger,
                                     '0'])

    elif grid_type == 'healpix' or grid_type == 'circular' or grid_type == 'line':
        print("Calling xmakeskygrid to generate ", skyPosFilename_trigger, file=sys.stdout)
        skygridcommand = ' '.join([ "xmakeskygrid", 
                                     str(ra), 
                                     str(decl),
                                     str(trigger_time), 
                                     str(sky_pos_err),
                                     cp.get('skygrid','numSigmaSkyPos'),
                                     siteStrTilde, 
                                     cp.get('skygrid','delayTol'), 
                                     skyPosFilename_trigger,
                                     grid_type, 
                                     '0'])

    elif grid_type == 'file':
        print("Calling xconvertfilegrid to generate ", skyPosFilename_trigger, file=sys.stdout)
        skygridcommand = ' '.join([ "xconvertfilegrid", 
                                     str(sky_pos_err),
                                     str(trigger_time),
                                     skyPosFilename_trigger])

    elif grid_type == 'ipn':
        print("Error: IPN grids are currently unsupported. Please use circular or line instead.", file=sys.stdout)
        sys.exit(1)

    elif grid_type == 'opt':
        print("Error: Optimized position grids are currently unsupported. Please use circular or line instead.", file=sys.stdout)
        sys.exit(1)

    else:
        print("Error: Choice of sky position grid is unrecognized. Please use circular or line.", file=sys.stdout)
        sys.exit(1)

    # ---- Execute sky grid generation command.
    print(skygridcommand, file=sys.stdout)
    os.system(skygridcommand)


# -------------------------------------------------------------------------
#    Make coincidence segment list for on-source, zero-lag segment and check for gaps.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing on-source event file ...          ", file=sys.stdout)

# ---- Now make segment lists for coincidence operation.  First determine
#      segment list for zero lag, and verify that the on-source time is
#      contained by a segment, else quit with error.
# ---- On source period: +/- minimumSegmentLength / 2 around trigger time.
on_source_start_time = int(trigger_time + onSourceBeginOffset - transientTime)
on_source_end_time = int(trigger_time + onSourceEndOffset + transientTime)

# ---- Write time range to a temporary segment file named on_source_interval.txt.
#      We'll then read this file into a ScienceData object.  It's a hack, but
#      it seems that the only way to populate ScienceData objects is to read
#      segments from a file.
f=open('input/on_source_interval.txt', 'w')
if directedMode:
    time_range_string = '1 ' + str(on_source_start_time) + ' ' \
        + str(on_source_end_time) + ' ' + str(on_source_end_time - on_source_start_time) + '\n'
else:
    time_range_string = '1 ' + str(on_source_start_time) + ' ' \
        + str(int(on_source_start_time + onSourceWindowLength)) + ' ' + str(blockTime) + '\n'
f.write(time_range_string)
f.close()

# ---- Read on-source time range into a "ScienceData" object.
on_source_segment = pipeline.ScienceData()
on_source_segment.read( 'input/on_source_interval.txt', blockTime )

# ---- Now get the intersection of all of the detector segment lists with
#      this on-source list.
coincidence_segment = copy.deepcopy(on_source_segment)
for det in full_segment_list:
    coincidence_segment.intersection(det)

if not directedMode:
    # ---- Check for gaps in the on-source interval for GRB-style searches and exit if we 
    #      find any gaps. If the on-source interval is a coincidence segment, then the 
    #      intersection of this interval with the "not" coincidence should be empty.
    not_coincidence_segment = copy.deepcopy(coincidence_segment)
    not_coincidence_segment.invert()
    overlap = not_coincidence_segment.intersection(on_source_segment)
    if overlap != 0:
        print("Error: on-source period is not a coincidence segment of the specified network.")
        for seg in on_source_segment:
            print("on source period:", seg.start(), " ", seg.end())
        for seg in coincidence_segment:
            print("coincidence period:", seg.start(), " ", seg.end())
        sys.exit(1)


# -------------------------------------------------------------------------
#    Make on-source window, event, segment files; also sky grid and parameter files.
# -------------------------------------------------------------------------

# ---- Write the 'window', 'event', and 'segment' files for the on-source analysis. 
#      The 'window' file lists the trigger time for the external event. 
#      The 'event' file lists the center time of each block required to cover 
#      the on-source window.  In general there is no obvious relationship 
#      between the window time and event times, since you may need
#      multiple events to cover the on-source window, and the events are 
#      placed in time starting from the beginning of the window, not relative
#      to the trigger time.

# ---- Write the window_on_source.txt file. This consists of a single time - the trigger time.
fwin = open('input/window_on_source.txt', 'w')
fwin.write(str(int(trigger_time)) + ' 0' * len(detector) +'\n')
#if directedMode:
#    fwin.write(str(int(trigger_time)))
#else:
#    fwin.write(str(int(trigger_time)) + ' 0' * len(detector) +'\n')
fwin.close()

# ---- Split the on-source coincidence segment(s) into "chunks" and write each chunk to the
#      event_on_source.txt and segment_on_source.txt files.
fevent = open('input/event_on_source.txt', 'w')
fseg = open('input/segment_on_source.txt', 'w')
coincidence_segment.make_chunks(blockTime,2*transientTime)
if not directedMode:
    if len(coincidence_segment) > 1:
        print("Error: The on-source segment should not have holes? This one seem to have holes inside.")
        sys.exit(1)
for iCoincSeg in range(len(coincidence_segment)):
  seg = coincidence_segment[iCoincSeg]
  for iSeg in range(len(seg)):

    # ---- Write event to event file.
    time_range_string = str((seg.__getitem__(iSeg).start() + \
                            seg.__getitem__(iSeg).end()) / 2) + \
                            ' 0' * len(detector)  + '\n'
    fevent.write(time_range_string)
    # ---- Write segment to segment file.
    time_range_string = '0 ' + str(int(seg.__getitem__(iSeg).start())) \
                        + ' ' + str(int(seg.__getitem__(iSeg).end())) \
                        + ' ' + str(int(seg.__getitem__(iSeg).end() \
                        - seg.__getitem__(iSeg).start())) + '\n'
    fseg.write(time_range_string)
    gpsBlockCenterTime = (seg.__getitem__(iSeg).start()+seg.__getitem__(iSeg).end())/2

    # ---- If the sky position uncertainty is nonzero then we compute a list of
    #      sky positions we need to search and write these to a file.
    if sky_pos_err:
        skyPosFilename = 'input/sky_positions_' + str(iSeg) + '.txt'
        print("Calling xconvertskylocations to generate ", skyPosFilename, file=sys.stdout)
        xconvertskylocationsCall = ' '.join([ "xconvertskylocations",
            str(skyPosFilename_trigger),
            str(trigger_time),
            str(gpsBlockCenterTime),
            skyPosFilename])
        print(xconvertskylocationsCall, file=sys.stdout)
        os.system(xconvertskylocationsCall)

        # ---- Check that the output file has been created.
        if not os.path.isfile(skyPosFilename):
            print("Error creating ", skyPosFilename, file=sys.stderr)
            sys.exit(1)

        # ---- Count number of sky positions.
        numSky = len(open(skyPosFilename).readlines())

        # ---- Set skyPositionList arg that will be written to matlab format
        #      parameters files.
        skyPositionList = skyPosFilename

    # ---- If user has not specified a sky position error we will search only a
    #      single sky position. For directed-mode searches this will be a fixed
    #      (ra,dec). For GRB-style searches this will be fixed in Earth-based 
    #      coordinates to ensure the background jobs use the same antenna response
    #      values as the on-source jobs.
    else:
        if directedMode: 
            # ---- Only need to do this once.
            if iCoincSeg==0 and iSeg==0:
                skyPositionList = '[' + str(decl) + ',' + str(ra) + ']'
                print("Trigger position (deg):", file=sys.stdout)
                print("    RA  = ", ra,   file=sys.stdout)
                print("    Dec = ", decl, file=sys.stdout)
        else: 
            # ---- Convert right ascension, declination to Earth-fixed coordinates.
            print("Calling xconvertgrbtoearth ...", file=sys.stdout)
            xconvert_command = ' '.join([
                "xconvertgrbtoearth",
                "input/earthfixedcoordinates" + str(iSeg) +".txt",
                str(ra),str(decl),str(gpsBlockCenterTime)])
            os.system(xconvert_command)
            # ---- Load earth-fixed coordinates back in.
            fEarthFixed = open('input/earthfixedcoordinates' + str(iSeg) + '.txt','r')
            phitheta = fEarthFixed.read()
            fEarthFixed.close()
            phithetaList = phitheta.split(' ')
            phi = float(phithetaList[0])
            theta = float(phithetaList[1])
            # ---- Set skyPositionList arg that will be written to matlab format
            #      parameters files.
            skyPositionList = '[' + str(theta) + ',' + str(phi) + ']'
            print("GRB position in Earth-fixed coordinates (rad):", file=sys.stdout)
            print("    phi = ", phi, file=sys.stdout)
            print("    theta = ", theta, file=sys.stdout)

        # ---- Set number of sky positions.
        numSky = 1


    # ---- Parameters file for on-source analysis. Only need to do once for directedMode.
    if not directedMode or (directedMode and iCoincSeg==0 and iSeg==0):
        fParam=open('input/parameters_on_source_' + str(iSeg) + '.txt', 'w')
        # ---- First write framecache file, channel file, event file, and sky position.
        fParam.write('channelFileName:input/channels.txt' + '\n')
        fParam.write('frameCacheFile:' + frameCacheAll + '\n')
        fParam.write('eventFileName:input/event_on_source.txt' + '\n')
        fParam.write('skyPositionList:' + skyPositionList + '\n')
        fParam.write('skyCoordinateSystem:' + skyCoordinateSystem + '\n')
        fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
        # ---- Now write all of the other parameters from the parameters section.
        #      We ignore the likelihoodType_* lines since this is handled above.
        for i in range(0,len(parameters)) :
            if not(parameters[i].startswith("likelihoodtype")):
                value = cp.get('parameters',parameters[i])
                if parameters[i] == "onsourceendoffset":
                    fParam.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
                elif parameters[i] == "circtimeslidestep":
                    continue
                else:
                    fParam.write(parameters[i] + ':' + value + '\n')
        if outputType == 'seedless' :
            fParam.write('seedlessparams:input/seedless_onsource.txt\n')
        if applyGates:
            fParam.write('gatingfile:input/gates.txt\n')
        fParam.close()

        # ---- Parameters file for ul-source analysis.
        fParam=open('input/parameters_ul_source_' + str(iSeg) + '.txt', 'w')
        # ---- First write framecache file, channel file, event file, and sky position.
        fParam.write('channelFileName:input/channels.txt' + '\n')
        fParam.write('frameCacheFile:' + frameCacheAll + '\n')
        fParam.write('eventFileName:input/event_on_source.txt' + '\n')
        fParam.write('skyPositionList:' + skyPositionList + '\n')
        fParam.write('skyCoordinateSystem:' + skyCoordinateSystem + '\n')
        fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
        # ---- Now write all of the other parameters from the parameters section.
        #      We ignore the likelihoodType_* lines since this is handled above.
        for i in range(0,len(parameters)) :
            if not(parameters[i].startswith("likelihoodtype") or parameters[i].endswith("ecimateRate")):
                value = cp.get('parameters',parameters[i])
                if parameters[i] == "onsourceendoffset":
                    fParam.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
                elif parameters[i] == "circtimeslidestep":
                    continue
                else:
                    fParam.write(parameters[i] + ':' + value + '\n')
        fParam.write('decimateRate:100\n')
        fParam.write('superDecimateRate:100\n')
        if outputType == 'seedless' :
            fParam.write('seedlessparams:input/seedless_ul.txt\n')
        if applyGates:
            fParam.write('gatingfile:input/gates.txt\n')
        fParam.close()

        # ---- Parameters file for off-source analysis.
        fParam=open('input/parameters_off_source_' + str(iSeg) + '.txt', 'w')
        # ---- First write framecache file, channel file, event file, and sky position.
        fParam.write('channelFileName:input/channels.txt' + '\n')
        fParam.write('frameCacheFile:' + frameCacheAll + '\n')
        fParam.write('eventFileName:input/event_off_source.txt' + '\n')
        fParam.write('skyPositionList:' + skyPositionList + '\n')
        fParam.write('skyCoordinateSystem:' + skyCoordinateSystem + '\n')
        fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
        # ---- Now write all of the other parameters from the parameters section.
        #      We ignore the likelihoodType_* lines since this is handled above.
        for i in range(0,len(parameters)) :
            if not(parameters[i].startswith("likelihoodtype")):
                value = cp.get('parameters',parameters[i])
                if parameters[i] == "onsourceendoffset":
                    fParam.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
                else:
                    fParam.write(parameters[i] + ':' + value + '\n')
        if outputType == 'seedless' :
            fParam.write('seedlessparams:input/seedless_offsource.txt\n')
        if applyGates:
            fParam.write('gatingfile:input/gates.txt\n')
        fParam.close()

        # ---- Parameter files for on-the-fly simulated waveform analyses, if requested.
        if cp.has_section('waveforms') :
            # ---- Read [injection] parameters.  If waveform_set is empty then no
            #      files will be written.
            waveform_set = cp.options('waveforms')
            # ---- Write one parameters file for each (waveform set, injection scale) pair.
            for set in waveform_set :
                if cp.has_section(set) & cp.has_option(set,'injectionScales') :
                    # ---- This check lets you specify different injection scales and
                    #      spacing for each waveform set.
                    injectionScalesList = cp.get(set,'injectionScales')
                    injectionScales = injectionScalesList.split(',')
                else:
                    # ---- Otherwise, use the injection scales and spacing specified in
                    #      the [injection] section.
                    injectionScalesList = cp.get('injection','injectionScales')
                    injectionScales = injectionScalesList.split(',')

                # ---- If we do long injections we will have one single parameter file for all injection scales.
                if longInjections == True:
                    fParam=open("input/parameters_simulation_" + set + "_0_" + str(iSeg) + ".txt", 'w')
                    # ---- First write framecache file, channel file, event file, and sky position.
                    fParam.write('channelFileName:input/channels.txt' + '\n')
                    fParam.write('frameCacheFile:' + frameCacheAll + '\n')
                    fParam.write('eventFileName:input/event_inj_source.txt' + '\n')
                    fParam.write('catalogdirectory:input/' + '\n')
                    fParam.write('skyPositionList:' + skyPositionList + '\n')
                    fParam.write('skyCoordinateSystem:' + skyCoordinateSystem + '\n')
                    fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
                    # ---- Now write all of the other parameters from the parameters section.
                    #      We ignore the likelihoodType_* lines since this is handled above.
                    for i in range(0,len(parameters)) :
                        if not(parameters[i].startswith("likelihoodtype")) :
                            value = cp.get('parameters',parameters[i])
                            if parameters[i] == "outputtype"  and value == "clusters" and not(disableFastInjections):
                                fParam.write('outputtype:injectionclusters\n')
                            elif parameters[i] == "onsourceendoffset":
                                fParam.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
                            elif parameters[i] == "circtimeslidestep":
                                continue
                            else:
                                fParam.write(parameters[i] + ':' + value + '\n')

                    # ---- Write simulations info.  The specified injection file
                    #      will be written later.
                    fParam.write('injectionFileName:input/injection_' + set + '.txt' + '\n')
                    fParam.write('injectionScale:' + str(injectionScalesList[:]) + '\n')
                    # fParam.write('catalogdirectory:' + XPIPELINE_ROOT  + '/waveforms\n')
                    if outputType == 'seedless' :
                        fParam.write('seedlessparams:input/seedless_simulation.txt\n')
                    if applyGates:
                        fParam.write('gatingfile:input/gates.txt\n')
                    fParam.close()
                else:
                    # ---- If not long inj, write a separate parameter file for each injection scale.
                    scale_counter = 0
                    for injectionScale in injectionScales :
                        fParam=open("input/parameters_simulation_" + set + "_" + str(scale_counter) + "_" + str(iSeg) + ".txt", 'w')
                        # ---- First write framecache file, channel file, event file, and sky position.
                        fParam.write('channelFileName:input/channels.txt' + '\n')
                        fParam.write('frameCacheFile:' + frameCacheAll + '\n')
                        fParam.write('eventFileName:input/event_inj_source.txt' + '\n')
                        fParam.write('catalogdirectory:input/' + '\n')
                        fParam.write('skyPositionList:' + skyPositionList + '\n')
                        fParam.write('skyCoordinateSystem:' + skyCoordinateSystem + '\n')
                        fParam.write('likelihoodtype:' + likelihoodTypeStr + '\n')
                        # ---- Now write all of the other parameters from the parameters section.
                        #      We ignore the likelihoodType_* lines since this is handled above.
                        for i in range(0,len(parameters)) :
                            if not(parameters[i].startswith("likelihoodtype")) :
                                value = cp.get('parameters',parameters[i])
                                if parameters[i] == "outputtype"  and value == "clusters" and not(disableFastInjections):
                                    fParam.write('outputtype:injectionclusters\n')
                                elif parameters[i] == "onsourceendoffset":
                                    fParam.write('onsourceendoffset:' + str(onSourceEndOffset) + '\n')
                                elif parameters[i] == "circtimeslidestep":
                                    continue
                                else:
                                    fParam.write(parameters[i] + ':' + value + '\n')

                        # ---- Write simulations info.  The specified injection file
                        #      will be written later.
                        fParam.write('injectionFileName:input/injection_' + set + '.txt' + '\n')
                        fParam.write('injectionScale:' + str(injectionScale) + '\n')
                        # fParam.write('catalogdirectory:' + XPIPELINE_ROOT  + '/waveforms\n')
                        if outputType == 'seedless' :
                            fParam.write('seedlessparams:input/seedless_simulation.txt\n')
                        if applyGates:
                            fParam.write('gatingfile:input/gates.txt\n')
                        fParam.close()
                        scale_counter = scale_counter + 1

fevent.close()
fseg.close()

# ---- Status message.
print("... finished writing on/off-source and simulation parameter files.", file=sys.stdout)
print(file=sys.stdout)

# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t' + str(numSky))
sfile.close()


# -------------------------------------------------------------------------
#    Copy waveform catalogs (if specified) to input/.
# -------------------------------------------------------------------------

if catalog_dir:
    print("Copying waveform catalogs to input/ ...", file=sys.stdout)
    cpCommand = ' '.join(['cp ' + catalog_dir + '/*.mat input/'])
    os.system(cpCommand)
    print("... finished copying waveform catalogs.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#   Remove on-source times from off-source coincidence segment lists.
# -------------------------------------------------------------------------

## ---- Status message.
#print("Writing off-source event file ...          ", file=sys.stdout)

# ---- For GRB-style searches, first remove the on-source times from all detector segment lists.
if not directedMode:
    off_source_segment = copy.deepcopy(on_source_segment)
    off_source_segment.invert()
    for det in full_segment_list:
        det.intersection(off_source_segment)


# -------------------------------------------------------------------------
#    Write off-source window file.
# -------------------------------------------------------------------------

# ---- Write the 'window' file for the off-source analysis. This file lists 
#      dummy trigger times for the external event. Note:
#       - The 'event' file will be generated later from the 'window' file.
#       - We do not need a 'segment' file for the off-source analysis.

# ---- Keep track of number of off-source events.
numOff = 0

if not directedMode:
    # ---- Standard GRB-style searches include the zero-lag off-source 
    #      data in the off-source analysis. Initialise the 'window' file 
    #      with the zero-lag data.
    f = open('input/window_off_source.txt', 'w')
    # ---- Make a temporary copy of the segment lists.
    lag_full_segment_list = copy.deepcopy(full_segment_list)
    # ---- List of lags for each detector (0 in this case)
    line = ' 0' * len(detector)
    # ---- Now take coincidence of segment lists.
    lag_coincidence_list = copy.deepcopy(lag_full_segment_list[0])
    for i in range(1,len(detector)):
        lag_coincidence_list.intersection(lag_full_segment_list[i])
    # ---- Split coincidence segment list into "chunks".
    lag_coincidence_list.make_chunks(onSourceWindowLength,2*transientTime)
    for seg in lag_coincidence_list:
        for i in range(len(seg)):
            # ---- Write dummy trigger time to window file.
            time_range_string = str(int(seg.__getitem__(i).start() - \
                onSourceBeginOffset + transientTime) ) + line + '\n'
            f.write(time_range_string)
    f.close()

# ---- Now open lag file (if any) and write event file for all non-zero lags.
#      The lag file will have one lag per detector; any of them may be zero.
if lagFile:
    if directedMode:
        f = open('input/event_off_source.txt', 'w')
    else:
        f = open('input/window_off_source.txt', 'a')
    lag_list = open(lagFile,mode='r')
    for line in lag_list:
        # ---- Extract time lag for each detector.
        lags = line.split(None)
        if len(lags) != len(detector):
            print("Error: the lag file should have number of " \
            "columns equal to the the number of detectors we are analysing. " \
            " Lag file: ", lagFile, file=sys.stderr)
            sys.exit(1)
        # ---- Make a time-lagged copy of the segment lists.
        lag_full_segment_list = copy.deepcopy(full_segment_list)
        # ---- Time shift segment list of each detector.
        for i in range(len(detector)):
            for seg in lag_full_segment_list[i]:
                seg.set_start(seg.start()-int(lags[i]))  # -- SUBTRACT lag
                seg.set_end(seg.end()-int(lags[i]))  # -- SUBTRACT lag
        # # ---- Sanity check: dump segments for each ifo to screen.
        # print
        # print "List of analysis segments for each detector", detector, ":"
        # for det_list in lag_full_segment_list:
        #     for seg in det_list:
        #         print seg.start(), seg.end()
        # ---- Now take coincidence of time-lagged segment lists.
        lag_coincidence_list = copy.deepcopy(lag_full_segment_list[0])
        for i in range(1,len(detector)):
            lag_coincidence_list.intersection(lag_full_segment_list[i])
        # ---- Split coincidence segment list into "chunks".
        if directedMode:
            lag_coincidence_list.make_chunks(blockTime,2*transientTime)
            for seg in lag_coincidence_list:
                for i in range(len(seg)):
                    # ---- Write event to event file.
                    time_range_string = str((seg.__getitem__(i).start() \
                        + seg.__getitem__(i).end()) / 2)
                    time_range_string = time_range_string + ' ' + line
                    f.write(time_range_string)
                    numOff = numOff + 1
        else:
            lag_coincidence_list.make_chunks(onSourceWindowLength,2*transientTime)
            for seg in lag_coincidence_list:
                for i in range(len(seg)):
                    # ---- Write dummy trigger time to window file.
                    time_range_string = str(int(seg.__getitem__(i).start() - \
                        onSourceBeginOffset + transientTime) ) + ' ' + line
                    f.write(time_range_string)
    f.close()

# ---- Status message.
print("... finished writing off-source event file.", file=sys.stdout)
print(file=sys.stdout)


# -----------------------------------------------------------------------------
#      Read in off-source window file and check each dummy window passes meets 
#                           our network criteria.
# -----------------------------------------------------------------------------

if not directedMode:

    print("Applying network criteria to our off-source events.", file=sys.stdout)

    # ---- Get triggerTimes and timeOffsets for each off-source event.

    # ---- Write file containing off-source event centre times,
    #      trigger_time_off.txt using input/event_off_source.txt.
    awkCommand = ' '.join(["awk",
            "'{print $1}'",
            "input/window_off_source.txt",
            ">", "input/trigger_time_off.txt"])
    os.system(awkCommand)

    # ---- Write file containing off-source event lag times,
    #      time_offsets_off.txt using input/event_off_source.txt.

    awkStr = ''
    for idx in range(0,len(detector)):
        awkStr = awkStr +  " \" \" " + " $" + str(idx+2)

    awkCommand = ' '.join(["awk",
        "'{print ", awkStr, "}'",
        "input/window_off_source.txt",
        ">", "input/time_offsets_off.txt"])
    os.system(awkCommand)

    # ---- Find network for each off-source event using xnetworkselection.m.
    network_outputFile = 'input/xnetworkselection_offsource.dat'

    xnetworkselectionCall = ' '.join([ "xnetworkselection",
        detectorStr,
        "input/trigger_time_off.txt",
        "input/time_offsets_off.txt",
        cat1_segment_file_list[0],
        cat1_segment_file_list[1],
        cat1_segment_file_list[2],
        cat1_segment_file_list[3],
        cat1_segment_file_list[4],
        cat2_segment_file_list[0],
        cat2_segment_file_list[1],
        cat2_segment_file_list[2],
        cat2_segment_file_list[3],
        cat2_segment_file_list[4],
        network_outputFile,
        str(onSourceBeginOffset),
        str(onSourceEndOffset),
        str(transientTime),
        str(onSourceWindowLength)
        ])

    print(xnetworkselectionCall, file=sys.stdout)
    os.system(xnetworkselectionCall)

    # ---- Discard off-source events which do not contain the ifos in
    #      our network.

    # ---- Read in event_off_source.txt
    fevents = open('input/window_off_source.txt','r')
    events = fevents.readlines()
    fevents.close()

    # ---- Read in network strings from network_outputFile.
    fnetworks = open(network_outputFile,'r')
    networks = fnetworks.readlines()
    fnetworks.close()

    eventListTempFilename = "input/window_off_source_temp.txt"
    eventListTemp = open(eventListTempFilename,'w')

    # ---- Check events and networks list have the same length.
    if not(len(events) == len(networks)):
        print("Error: off-source networks and events lists "\
            "should have the same length", file=sys.stderr)
        sys.exit(1)


    # ---- Number of off-source events before we discard any.
    origNumOff = len(events)

    # ---- Keep track of how many off-source jobs we have after
    #     discarding those with bad networks.
    numOff = 0

    # ---- If the user has specified the number of background jobs
    #      get this variable.
    if cp.has_option('background','numJobs'):
        background_numJobs = int(cp.get('background','numJobs'))
        if background_numJobs > origNumOff:
            print("We have only ", origNumOff, "background jobs", file=sys.stderr)
            print("Error: Specify more lags to get desired "\
                "number of background jobs: numJobs = ", background_numJobs, file=sys.stderr)
            sys.exit(1)

    # ---- Do these strings contain the ifos in the detector list.
    for idx in range(0,len(networks)):
        # ---- For each off-source network we will count how many
        #      ifos from our selected network it contains.
        # ---- We will discard any off-source events whose corresponding
        #      network does not contain all of the ifos in our
        #      selected network.
        numIfo = 0

        # ---- skip network if it is not well determined
        if networks[idx][0] != 'X':
            # ---- Loop over ifos in our selected network.
            for ifo in detector:
                numIfo = numIfo + networks[idx].count(ifo)

        # ---- If the off-source network contains all of the ifos
        #      in our selected network we will write out the details
        #      of the corresponding off-source event to a temporary file.
        if numIfo == len(detector):
            if not(cp.has_option('background','numJobs')) or \
                numOff < background_numJobs:
                eventListTemp.write(events[idx])
                numOff += 1

    eventListTemp.close()

    # ---- Replace events_off_source.txt with new updated file containing
    #      only those events whose networks contain the ifos in our
    #      selected network.
    mvCommand = ' '.join(['mv',
        eventListTempFilename,
        'input/window_off_source.txt'])
    os.system(mvCommand)

    print("We retain the " + str(numOff) + " jobs from a total of " + \
        str(origNumOff) + " off-source jobs that satisfy " \
        "our network criteria.\n", file=sys.stdout)

# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t' + str(numOff))
sfile.close()

# ---- If we do not have the required number of off-source jobs tell
#      the user and exit.
if cp.has_option('background','numJobs'):
    background_numJobs = int(cp.get('background','numJobs'))
    if numOff < background_numJobs:
        print("Error: Specify more lags to get desired "\
            "number of background jobs: numJobs = ", background_numJobs, file=sys.stderr)
        sys.exit(1)










# -----------------------------------------------------------------------------
#    Generate off-source 'event' file from validated off-source 'window' file.
# -----------------------------------------------------------------------------

# ---- KLUDGE: In directed mode the time-lag code above writes the off-source event file 
#      but not the window file, and vica-versa for GRB-style mode. In either case we need
#      the other file (events_off_source.txt or window_off_source.txt).
if directedMode:
    ## ---- For the directed search each data segment analysed is treated as its own window.
    #mvCommand = ' '.join(['cp input/event_off_source.txt input/window_off_source.txt'])
    #os.system(mvCommand)
    # ---- For directed-mode searches we collect time-slide jobs into groups that are 
    #      equal to the number of on-source jobs. Compute the number of effective 
    #      background trials we can make with the off-source blocks. Then fill 
    #      window_off_source.txt with that number of copies of the trigger time.
    # ---- Read the number of chunks in on-source from the event list.
    event_on_file = open('input/event_on_source.txt')
    event_on_list = event_on_file.readlines()
    nOnEvents = len(event_on_list)
    event_on_file.close()
    # ---- Compute the number of effective background trials.
    nEffBkgTrials = int(math.floor(float(numOff)/float(nOnEvents)))
    fwin = open('input/window_off_source.txt','w');
    for iTrial in range(0,nEffBkgTrials):
        fwin.write(str(int(trigger_time)) + "\n")
    fwin.close()
    # ---- Throw away off-source events that would form a last incomplete effective 
    #      background trial by over-writing event_off_source.txt.
    event_off_file = open('input/event_off_source.txt')
    event_off_list = event_off_file.readlines()
    event_off_file.close()
    event_off_file = open('input/event_off_source.txt','w')
    for iEvent in range(0,nEffBkgTrials*nOnEvents):
        event_off_file.write(event_off_list[iEvent])
    event_off_file.close()
else:
    # ---- Read the window file.
    fwin = open('input/window_off_source.txt','r')
    windowList = fwin.readlines()
    fwin.close()
    # ---- Split window list into an event list.
    fev = open('input/event_off_source.txt','w')
    for window in windowList:
        windowWord=window.split()
        for iJob in range(jobsPerWindow):
            time_range_string = str(float(windowWord[0])+onSourceBeginOffset+(blockTime-2*transientTime)*(0.5+iJob))
            for iWord in range(1,len(windowWord)):
                time_range_string = time_range_string + ' ' + windowWord[iWord]
            time_range_string = time_range_string + '\n'
            fev.write(time_range_string)
    fev.close()


# -----------------------------------------------------------------------------
#                   Find data frames, if necessary.
# -----------------------------------------------------------------------------

# ---- If dataFrameCache is specified and exists we will add it to frameCacheAll
if dataFrameCache:
    # ---- check the frameCache specified actually exists
    if not os.path.isfile(dataFrameCache):
        print("Error: non existant framecache: ",dataFrameCache, file=sys.stderr)
        sys.exit(1)

    # ---- if the specified mdc frame cache exists then concat it other frame caches
    command = 'cat ' + dataFrameCache + '  >> ' + frameCacheAll
    os.system(command)

# ---- If dataFrameCache is not specified in the config file, then call dataFind
#      for each detector, and convert to readframedata-formatted framecache file.
#      This script will exit with an error message if the datafind command reports
#      any gaps that overlap with analysis segments.
else:
    # ---- Status message.
    print("Writing framecache file for ifo data...", file=sys.stdout)
    os.system('rm -f framecache_temp.txt')
    # ---- Loop over detectors.
    for i in range(0,len(detector)):
        # ---- Construct dataFind command.
        dataFindCommand = ' '.join([datafind_exec, \
        "--server", datafind_server, \
        "--observatory", detector[i][0], \
        "--type", frameType[i], \
        "--gps-start-time", str(start_time),  \
        "--gps-end-time", str(end_time),    \
        "--url-type file", "--gaps", "--lal-cache > lalcache.txt 2> ligo_data_find.err"])
        # ---- Issue dataFind command.
        print("calling dataFind:", dataFindCommand)
        os.system(dataFindCommand)
        os.system('cat ligo_data_find.err')
        print("... finished call to dataFind.")
        # ---- Convert lalframecache file to readframedata format.
        print("calling convertlalcache:")
        os.system('convertlalcache.pl lalcache.txt framecache_temp.txt')
        os.system('cat framecache_temp.txt >> ' + frameCacheAll)
        print("... finished call to convertlalcache.")

        # ---- Read stderr log of ligo_data_find to see if there are any gaps in the frames.
        f = open('ligo_data_find.err','r')
        gaps_str=f.read()
        f.close()
        # ---- Note: the format of the ligo_data_find.err file depends on which data find executable 
        #      we are using. If we're using ligo_data_find then the error file is always non-empty, 
        #      due to a deprecation warning message. Strip this out before checking for gaps.
        if datafind_exec=='ligo_data_find':
            # ---- File contains two lines of the form 
            #        Warning: ligo_data_find has been deprecated in favour of gw_data_find, please consider using that executable, which takes the same arguments, and returnes the same results
            #        missing segments: [segment(866386702, 866393054), ..., segment(866477785, 866488107)]
            #      The code below is not expecting the warning message. Strip it out before 
            #      continuing.
            print("Attempting to parse gaps file for ligo_data_find", file=sys.stdout) 
            os.system('awk \'{ if (NR>1) print $0 }\' ligo_data_find.err > ligo_data_find_trimmed.err')
            os.system('mv ligo_data_find_trimmed.err ligo_data_find.err')
            # ---- Re-read error file -- it may be empty (the warning may have been the only line).
            f = open('ligo_data_find.err','r')
            gaps_str=f.read()
            f.close()

        # ---- Check to see if any of the missing frames overlap with our analysis segments. 
        #      If they do, remove the times with missing frames from the segment lists.
        if gaps_str:
            # ---- The format of the ligo_data_find.err file depends on which data find executable 
            #      we are using. Check which it is and act appropriately.
            if datafind_exec=='ligo_data_find':
                # ---- (trimmed) File contains one line of the form 
                #          missing segments: [segment(866386702, 866393054), ..., segment(866477785, 866488107)]
                # ---- Now proceed in confidence.
                gaps_str = gaps_str.replace('segment','segments.segment')
                # ---- Strip out segment list from the strerr log of ligo_data_find
                gaps_segments_str = 'segments.segmentlist(' + gaps_str[ gaps_str.index('[') : gaps_str.index(']')+1 ] + ')'
                # ---- Read gap segments into segment list
                gaps_segments = eval(gaps_segments_str)
                gaps_segments.coalesce()
                # ---- Need to write out these gap segments so we can read them
                #      in again as ScienceData object, we really should choose
                #      one type of segment object and use it everywhere.
                f = open("gaps.txt","w"); segmentsUtils.tosegwizard(f,gaps_segments); f.flush(); f.close()
            elif datafind_exec=='gw_data_find':
                # ---- File is of the form 
                #        Missing segments:
                #        866386702.000000 866393054.000000
                #        866396828.000000 866404754.000000
                #        866410903.000000 866412664.000000
                #        866413722.000000 866415440.000000
                #        866477785.000000 866488107.000000
                #      Use awk to turn this into a file with the segwizard format of gaps.txt generated above.
                awkcmdfile_str = '{ if (NR==1) print \"# seg start stop duration\"; else if ($2==int($2)) print NR-2, int($1), int($2), int($2)-int($1); else print NR-2, int($1), int($2)+1, int($2)-int($1)+1}'
                awkfile = open('./awkfile','w')
                awkfile.write(awkcmdfile_str)
                awkfile.close()
                os.system('awk -f awkfile ligo_data_find.err > gaps.txt')
                os.system('rm awkfile')
            else :
                print("Error: Do not know how to process gaps file produced by " + datafind_exec, file=sys.stderr)
                sys.exit(17)
            # ---- Need to write out these gap segments so we can read them
            #      in again as ScienceData object, we really should choose
            #      one type of segment object and use it everywhere.
            gaps_segments = pipeline.ScienceData()
            gaps_segments.read( 'gaps.txt', 0 )
            os.system('rm gaps.txt')
            # ---- Identify any overlap between missing frames and full_segment_list
            gaps_segments.intersection(full_segment_list[i])
            if gaps_segments:
                print("Error: missing frames in ", detector[i], file=sys.stderr)
                for jj in range(gaps_segments.__len__()):
                    time_range_string = str(jj) + ' ' + \
                        str(gaps_segments.__getitem__(jj).start()) \
                        + ' ' + str(gaps_segments.__getitem__(jj).end())  \
                        + ' ' + str(gaps_segments.__getitem__(jj).end() \
                        -gaps_segments.__getitem__(jj).start())
                    print(time_range_string)
                sys.exit(1)
        os.system('rm ligo_data_find.err')

    # ---- Clean up.
    os.system('rm -f framecache_temp.txt lalcache.txt')
    # ---- Status message.
    print("... finished writing framecache file for ifo data", file=sys.stdout)
    print(file=sys.stdout)

# ---- A common x-pipeline failure mode is that the datafind command fails, giving
#      an empty framecache. Verify that that the framecache file is not empty.
try:
    file_size = os.path.getsize(frameCacheAll)
    if (file_size == 0):
        print('Framecache file ' + frameCacheAll + ' is empty')
        sys.exit(2)
except FileNotFoundError as e:
    print('Framecache file ' + frameCacheAll + ' not found.')
    sys.exit(3)


# -------------------------------------------------------------------------
#      Write injection files for on-the-fly simulations, if needed.
# -------------------------------------------------------------------------

# ---- Construct a tilde-delimited string of time intervals in which the injections are to
#      be performed. This string will be passed to xmakegrbinjectionfilea to make the 
#      injection log files.
# ---- First select whether to inject into on-source or off-source data,
if offSourceInj:
    # ---- Off-source injections
    os.system('cp input/window_off_source.txt input/window_inj_source.txt')
    os.system('cp input/event_off_source.txt input/event_inj_source.txt')
else:
    # ---- On-source injections
    os.system('cp input/window_on_source.txt input/window_inj_source.txt')
    os.system('cp input/event_on_source.txt input/event_inj_source.txt')
# ---- Load the window and event files for the injections, as lists..
injwindows = loadtxt('input/window_inj_source.txt')
event_inj_list = loadtxt('input/event_inj_source.txt')
# ---- If we are doing on-source injections for a GRB-style search then injwindows will
#      gave a single element and be read in as a scalar instead of a list. This may be 
#      to case for event_inj_list too. In these cases forcibly reset to a list.
if injwindows.ndim == 1:
    injwindows = [injwindows]
if event_inj_list.ndim == 1:
    event_inj_list = [event_inj_list]
# ---- Now loop over the injection windows, constructing one time interval string for 
#      each window.
injTimeRangeStr = ""
injTimeOffsetStr = ""
injCenterTime = []
for iEv in range(0,len(injwindows)):
    # ---- The injection code currently handles only zero-lag injections. So, skip any 
    #      injection windows with non-zero lags (i.e., time-slid off-source windows).
    #      This means the --off-source-inj flag is inccompatible with the --directed-mode flag.
    if sum(abs(injwindows[iEv][1:])) > 0:
        break
    injTimeRangeStr = injTimeRangeStr + str(injwindows[iEv][0] + onSourceBeginOffset) + \
        '~' + str(injwindows[iEv][0] + onSourceEndOffset) + '~'
    injTimeOffsetStr = injTimeOffsetStr + str(int(trigger_time)-injwindows[iEv][0]) + '~'
    injCenterTime.append(injwindows[iEv][0])
# ---- Remove trailing tilde.
injTimeRangeStr = injTimeRangeStr[:-1]
injTimeOffsetStr = injTimeOffsetStr[:-1]

# ---- Generate an injection log file for each requested waveform set.
if cp.has_section('waveforms') :
    # ---- Read [injection] parameters.
    waveform_set = cp.options('waveforms')
    injectionInterval = cp.get('injection','injectionInterval')
    print("Making injection files ...", file=sys.stdout)
    for set in waveform_set :
        waveforms = cp.get('waveforms',set)
        baseinjfilename = "injection_" + set + ".txt"
        injfilename = "input/injection_" + set + ".txt"

        # ---- Check to see if the user has pre-made this injection file. If so, use it.
        if waveforms == baseinjfilename :
            command = ' '.join(["cp ", baseinjfilename, " input/"])
            os.system(command)
            print("    Using-premade injection file:", injfilename, file=sys.stdout)

        else:
            # ---- Construct command to make injection file for this waveform set.
            # ---- Wrap waveforms string in single quotes to escape any ; characters, 
            #      which are used for generating random injection parameters.
            tmpwaveforms = ''.join(["'", waveforms, "'"]) 
            make_injection_file_command = ' '.join(["xmakegrbinjectionfile",
            injfilename, tmpwaveforms,
            injTimeRangeStr,
            injTimeOffsetStr,
            injectionInterval, str(ra), str(decl), str(seed), grid_type, grid_sim_file ])
            # ---- Issue command to make injection file for this waveform set.
            # ---- We'll overwrite this file later with miscalibrated and/or
            #      jittered versions, if desired.
            print("    Writing :", injfilename, file=sys.stdout)
            print(make_injection_file_command, file=sys.stdout)
            os.system(make_injection_file_command)
            if injdistrib:
                jitter_injection_command = ' '.join(["xjitterinjectionskypositions",
                injfilename,
                injfilename,
                injdistrib ])
                print("    Jittering: ", injfilename, file=sys.stdout)
                print(jitter_injection_command, file=sys.stdout)
                os.system(jitter_injection_command)

        # ---- Apply calibration uncertainties to injections if required. We use
        #      the real detector (rather than the virtual detector) for determining
        #      calibration uncertainties, as the virtual detector may not have 
        #      been operational at the time of the trigger.
        if int(cp.get('injection','miscalibrateInjections'))==1:
            miscalib_injection_command = ' '.join(["xmiscalibrategrbinjectionfile",
            injfilename,
            injfilename,
            detectorStrTilde,
            frameTypeStrTilde ])
            print("    Miscalibrating :", injfilename, file=sys.stdout)
            print(miscalib_injection_command, file=sys.stdout)
            os.system(miscalib_injection_command)

    print("... finished making injection files.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Create ini file for xtmva.py, if required.
# -------------------------------------------------------------------------

# ---- Procedure: 
#      -> Copy the xtmva codes from the tmvadir location specified in the grb 
#         ini file to a new local directory called xtmvapy. This local copy 
#         will be used for the analysis to avoid conflicts if other xtmva 
#         analyses are running simultaneously. 
#      -> Write a bespoke ini file. Place it in the local run directory since 
#         xtmva.py will look for it there.
#      -> To do: add a "post" script to the all_jobs dag that runs xtmva.py. 
#         In fact this is probably easier to do in xgrbwebpage.py.
if xtmvaFlag:
    # ---- Extract required data from [xtmva] section.
    if cp.has_section('xtmva'):
        #WFtrain    = cp.get('xtmva','WFtrain')
        classifier = cp.get('xtmva','classifier')
        tmvadir    = cp.get('xtmva','tmvadir')
        # ---- Copy xtmva scripts to the local directory.
        print("Creating ./xtmvapy directory for xtmva analysis.", file=sys.stdout)
        os.system('rm -rf xtmvapy')
        os.system('mkdir xtmvapy')
        os.system('cp ' + tmvadir + '/*.* xtmvapy')
        # ---- Write ini file.
        print("Generating ini file for xtmva analysis.", file=sys.stdout)
        xtmvainifile = open('./xtmva.ini','w')
        inistring = '[Settings]' + '\n'
        xtmvainifile.write(inistring)
        #inistring = 'WFtrain=' + WFtrain + '\n'
        #xtmvainifile.write(inistring)
        inistring = 'classifier=' + classifier + '\n'
        xtmvainifile.write(inistring)
        inistring = '[Folders]' + '\n'
        xtmvainifile.write(inistring)
        inistring = 'tmvadir=' + os.getcwd() + '/xtmvapy' + '\n'
        xtmvainifile.write(inistring)
        inistring = 'xout=xtmva_' + classifier + '\n'
        xtmvainifile.write(inistring)
        inistring = '[WFtest]' + '\n'
        xtmvainifile.write(inistring)
        if cp.has_section('waveforms') :
            waveform_set = cp.options('waveforms')
            for set in waveform_set :
                # ---- Add all sets to the testing list EXCEPT for the traininiget.
                #if not (set==WFtrain):
                if 1==1:
                    # ---- Read the number of injections from the log file.
                    injfilename = "input/injection_" + set + ".txt"
                    injfile = open(injfilename)
                    injlist = injfile.readlines()
                    ninj = len(injlist)
                    injfile.close()
                    inistring = set + '=' + str(ninj) + '\n'
                    xtmvainifile.write(inistring)
        inistring = '[parameters]' + '\n'
        xtmvainifile.write(inistring)
        inistring = 'likelihoods='+likelihoodTypeStr + '\n'
        xtmvainifile.write(inistring)
        xtmvainifile.close()
        print(" ", file=sys.stdout)
    else:
        print("Error: xtmva analysis requested by ini file does not have an [xtmva] section. Exiting.", file=sys.stdout)
        sys.exit(4)


# -------------------------------------------------------------------------
#      Define special job classes.
# -------------------------------------------------------------------------

class XsearchJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x search job
    """
    def __init__(self,cp,UserName):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        os.system('which xdetection > path_file.txt')
        f = open('path_file.txt','r')
        xdetectionstr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xdetectionstr

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"\"USER=$ENV(USER) HOME=$ENV(HOME) " \
            "PATH=/usr/bin:/bin MCR_CACHE_ROOT=$$(CondorScratchDir)\"")

        # ---- Add accounting group flag.
        grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add username flag.
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory needed.
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else:
            minimalMemSearch = "1400"
            self.add_condor_cmd('request_memory',minimalMemSearch)

        # ---- Add mandatory request_disk request.
        minimalDiskSearch = "200M"
        self.add_condor_cmd('request_disk',minimalDiskSearch)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xsearch-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xsearch-$(cluster)-$(process).err')

        # If on Atlas, use getenv=true to pass variables
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xsearch.sub')

class XsearchGPUJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x search job to be run on a GPU
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """

        # ---- Get path to executable.
        os.system('which xdetection > path_file.txt')
        f = open('path_file.txt','r')
        xdetectionstr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xdetectionstr

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"\"USER=$ENV(USER) HOME=$ENV(HOME) " \
            "PATH=/usr/bin:/bin MCR_CACHE_ROOT=$$(CondorScratchDir)\"")

        # ---- Add accounting group flag.
        grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add username flag.
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory needed.
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else:
            minimalMemSearch = "4000"
            self.add_condor_cmd('request_memory',minimalMemSearch)

        # ---- Add mandatory request_disk request.
        minimalDiskSearch = "200M"
        self.add_condor_cmd('request_disk',minimalDiskSearch)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xsearch-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xsearch-$(cluster)-$(process).err')

        # If on Atlas, use getenv=true to pass variables
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Request GPU.
        self.add_condor_cmd('Requirements','TARGET.WantGPU =?= True')
        self.add_condor_cmd('+WantGPU','True')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xsearch_gpu.sub')

class XsearchNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    xsearch node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__x_jobnum = None
        self.__x_injnum = None

    # ---- Set parameters file.
    def set_param_file(self,path):
        self.add_var_arg(path)
        self.__param_file = path

    def get_param_file(self):
        return self.__param_file

    def set_x_jobnum(self,n):
        self.add_var_arg(str(n))
        self.__x_jobnum = n

    def get_x_jobnum(self):
        return self.__x_jobnum

    def set_output_dir(self,path):
        self.add_var_arg(path)
        self.__output_dir = path

    def get_output_dir(self,path):
        return self.__output_dir

    def set_x_injnum(self,n):
        self.add_var_arg(n)
        self.__x_injnum = n

    def get_x_injnum(self):
        return self.__x_injnum

class XmergeJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x merge job
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        os.system('which xmergegrbresults > path_file.txt')
        f = open('path_file.txt','r')
        xmergestr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xmergestr

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)

        # ---- Add name of 'output' directory as first argument.
        self.add_arg('output')

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"\"USER=$ENV(USER) HOME=$ENV(HOME) " \
            "MCR_CACHE_ROOT=$$(CondorScratchDir)\"")

        # ---- Add accounting group flag.
        grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add username flag.
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory needed.
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else:
            minimalMemMerge = "2000"
            self.add_condor_cmd('request_memory',minimalMemMerge)

        # ---- Add mandatory request_disk request.
        minimalDiskMerge = "10G"
        self.add_condor_cmd('request_disk',minimalDiskMerge)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xmerge-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xmerge-$(cluster)-$(process).err')

        # If on Atlas
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xmerge.sub')

class XmergeNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    merge results that were cut into blocks of less than maxInjNum jobs
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_sn_flag(self,path):
        self.add_var_arg(path)
        self.__sn_flag = path

    def get_sn_flag(self):
        return self.__sn_flag

class XmergeClusteredJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x merge clustered job
    """
    def __init__(self,cp,nDet):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        if nDet==3:
            os.system('which xmergegrbclusteredresults > path_file.txt')
        else:
            os.system('which xmergegrbclusteredresultsTwoDets > path_file.txt')
        f = open('path_file.txt','r')
        xmergestr = f.read()
        f.close()
        os.system('rm path_file.txt')
        self.__executable = xmergestr

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)

        # ---- Add name of 'output' directory as first argument.
        self.add_arg('output_clustered')

        # ---- Add required environment variables.
        self.add_condor_cmd('environment',"\"USER=$ENV(USER) HOME=$ENV(HOME) " \
            "MCR_CACHE_ROOT=$$(CondorScratchDir)\"")

        # ---- Add accounting group flag.
        grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add username flag.
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory request. Note that with current default minimalMem
        #      we never access the "else" statement below.
        if minimalMem :
            self.add_condor_cmd('request_memory',minimalMem)
        else :
            minimalMemMerge = "1500"
            self.add_condor_cmd('request_memory',minimalMemMerge)

        # ---- Add mandatory request_disk request.
        minimalDiskMerge = "10G"
        self.add_condor_cmd('request_disk',minimalDiskMerge)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xmergeclustered-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xmergeclustered-$(cluster)-$(process).err')

        # If on Atlas
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xmergeclustered.sub')

class XmergeClusteredNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    cluster and merge results that were cut into blocks of less than maxInjNum jobs
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)

    def set_dir_prefix(self,path):
        self.add_var_arg(path)
        self.__dir_prefix = path

    def get_dir_prefix(self):
        return self.__dir_prefix

    def set_sn_flag(self,path):
        self.add_var_arg(path)
        self.__sn_flag = path

    def get_sn_flag(self):
        return self.__sn_flag

    def set_sc_flag(self,path):
        self.add_var_arg(path)
        self.__sc_flag = path

    def get_sc_flag(self):
        return self.__sc_flag


# -------------------------------------------------------------------------
#    Preparations for writing dags.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing job submission files ... ", file=sys.stdout)

# ---- Retrieve job retry number from parameters file.
if cp.has_option('condor','retryNumber'):
    retryNumber = int(cp.get('condor','retryNumber'))
else:
    retryNumber = 0
if smartCluster == True:
    retryNumberClustered = 0

# ---- DAGman log file.
#      The path to the log file for condor log messages. DAGman reads this
#      file to find the state of the condor jobs that it is watching. It
#      must be on a local file system (not in your home directory) as file
#      locking does not work on a network file system.
log_file_on_source = cp.get('condor','dagman_log_on_source')
log_file_off_source = cp.get('condor','dagman_log_off_source')
if smartCluster == True:
    log_file_on_source_clustered = cp.get('condor','dagman_log_on_source')+'_clustered'
    log_file_off_source_clustered = cp.get('condor','dagman_log_off_source')+'_clustered'
if cp.has_section('waveforms') :
    log_file_simulations = cp.get('condor','dagman_log_simulations')
    if smartCluster == True:
        log_file_simulations_clustered = cp.get('condor','dagman_log_simulations')+'_clustered'
if cp.has_option('mdc','mdc_sets') :
    log_file_mdcs = cp.get('condor','dagman_log_mdcs')

# ---- Make directories to store the log files and error messages
#      from the nodes in the DAG
try: os.mkdir( 'logs' )
except: pass
# ---- Make directory to store the output of our test job.
#      NOT TO BE DONE FOR PRODUCTION RUNS!
try: os.mkdir( 'output' )
except: pass
try: os.mkdir( 'output/on_source' )
except: pass
try: os.mkdir( 'output/ul_source' )
except: pass
try: os.mkdir( 'output/off_source' )
except: pass
# ---- now make the same directories only for clustered results.
if smartCluster == True:
    try: os.mkdir( 'output_clustered' )
    except: pass
    try: os.mkdir( 'output_clustered/on_source' )
    except: pass
    try: os.mkdir( 'output_clustered/off_source' )
    except: pass
# ---- Make directories to hold results of injection runs - one for each
#      waveform and injection scale.
if cp.has_section('waveforms') :
    waveform_set = cp.options('waveforms')
    for set in waveform_set :
        # ---- The user can specify different injection scales for each
        #      waveform set by including a section with that name in the ini
        #      file.  Look for one.  If no special section for this waveform,
        #      then use the default injection scales from [injection].
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else:
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')
        scale_counter = 0
        for injectionScale in injectionScales :
            try: os.mkdir( 'output/simulations_' + set + '_' + str(scale_counter) )
            except: pass
            scale_counter = scale_counter + 1
            if smartCluster == True:
                try: os.mkdir( 'output_clustered/simulations_' + set + '_' + str(scale_counter) )
                except: pass
if cp.has_option('mdc','mdc_sets') :
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')
    for set in mdc_sets :
        # ---- The user can specify different injection scales for each
        #      waveform set by including a section with that name in the ini
        #      file.  Look for one.  If no special section for this waveform,
        #      then use the default injection scales from [injection].
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else:
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')
        scale_counter = 0
        for injectionScale in injectionScales :
            try: os.mkdir( 'output/simulations_' + set + '_' + str(scale_counter) )
            except: pass
            scale_counter = scale_counter + 1


# -------------------------------------------------------------------------
#    Define needed job classes.
# -------------------------------------------------------------------------

class XtmvaJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An xtmva classification job.
    """
    def __init__(self,cp,nDet):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable.
        self.__executable = os.getcwd() + "/xtmvapy/xtmva.py \n"

        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)

        # ---- Add name of 'output' directory as first argument.
        cwdstr_split = os.path.split(os.getcwd())
        fdir = cwdstr_split[0]
        grbdir = cwdstr_split[1]
        argumentstr = " --fdir=" + fdir + " --grbname=" + grbdir + " --nifo=" + str(nDet) + " --cname=xtmva.ini "
        print(argumentstr, file=sys.stdout)
        self.add_arg(argumentstr)

        # ---- Define environment variables that are needed by xtmva.py. We hard-code 
        #      most of them to avoid need to create a special gen-wrapper-script*.sh 
        #      script for this one function.
        envarstr = "\"USER=" + os.getenv("USER") + " " \
        + "HOME=" + os.getenv("HOME") + " " \
        + "LD_LIBRARY_PATH=" + os.getenv("LD_LIBRARY_PATH") + " " \
        + "XPIPE_INSTALL_BIN=$ENV(XPIPE_INSTALL_BIN) " \
        + "PATH=" + os.getenv("ROOTSYS") + "/bin:" + os.getenv("XPIPE_INSTALL_BIN") + ":/usr/bin:/bin " \
        + "LIBPATH=" + os.getenv("LIBPATH") + " " \
        + "DYLD_LIBRARY_PATH=" + os.getenv("DYLD_LIBRARY_PATH") + " " \
        + "PYTHONPATH=" + os.getenv("PYTHONPATH") + " " \
        + "SHLIB_PATH=" +  os.getenv("SHLIB_PATH") + " " \
        + "ROOTSYS=" + os.getenv("ROOTSYS") + "\""
        # ---- Add required environment variables.
        self.add_condor_cmd('environment',envarstr)

        # ---- Add accounting group flag.
        grouptag = 'ligo.' + \
                cp.get('condor','ProdDevSim') + '.' +\
                cp.get('condor','Era') + '.' +\
                cp.get('condor','Group') + '.' +\
                cp.get('condor','SearchType')
        self.add_condor_cmd('accounting_group',grouptag)

        # ---- Add username flag.
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory request. 
        minimalMem = 4000
        self.add_condor_cmd('request_memory',minimalMem)

        # ---- Add mandatory request_disk request. I'm not certain of the 
        #      requirements for this job but assume it can be as large as
        #      the merge jobs.
        minimalDiskMerge = "10G"
        self.add_condor_cmd('request_disk',minimalDiskMerge)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xtmva-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xtmva-$(cluster)-$(process).err')

        # If on Atlas
        if atlasFlag:
            self.add_condor_cmd('getenv',"true")

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xtmva.sub')

class XtmvaNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    cluster and merge results that were cut into blocks of less than maxInjNum jobs
    """
    def __init__(self,job):
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)


# -------------------------------------------------------------------------
#      Prepare xtmva dag.
# -------------------------------------------------------------------------

# ---- We initalise the dag here but do not write it until after all other 
#      dags have been written (on-source, off-source, simulations). This is
#      because the xtmva dag is unique in that all other dags must complete 
#      before it can run. We fix this dependency by adding all the xmerge 
#      jobs as parents to the xtmva job.
if xtmvaFlag:
    print("Writing xtmva dag ... ", file=sys.stdout)
    log_file_xtmva = cp.get('condor','dagman_log_on_source')+'_xtmva'
    print(log_file_xtmva)
    dag_xtmva = pipeline.CondorDAG(log_file_xtmva + uuidtag)
    dag_xtmva.set_dag_file( 'grb_xtmva' )
    xtmvajob  = XtmvaJob(cp,len(detector))
    xtmvanode = XtmvaNode(xtmvajob)
    xtmvanode.set_retry("1")
    xtmvanode.set_name("xtmva_on_source_" + xtmvanode.get_name())


# -------------------------------------------------------------------------
#      Write on-source dag.
# -------------------------------------------------------------------------

# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_on_source + uuidtag)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_on_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp,UserName)

if outputType == 'seedless':
    if doGPUOnSource == "true":
        job = XsearchGPUJob(cp)

# ---- Make instance of XmergeJob.
mergejob = XmergeJob(cp)
# ---- Put a single merge job node in mergejob.  This job will combine
#      into a single file the output of all of the off-source analysis jobs.
mergenode = XmergeNode(mergejob)
if xtmvaFlag:
    xtmvanode.add_parent(mergenode)

# ---- if clustering.
if smartCluster == True:
    print("Writing on-source clustering dag ... ", file=sys.stdout)
    print(log_file_on_source_clustered)
    dag_clustered = pipeline.CondorDAG(log_file_on_source_clustered + uuidtag)
    dag_clustered.set_dag_file( 'grb_on_source_clustered' )
    mergeclusteredjob = XmergeClusteredJob(cp,len(detector))
    mergeclusterednode = XmergeClusteredNode(mergeclusteredjob)

# ---- Make analysis jobs for all segments in the on-source segment list.
segmentList = pipeline.ScienceData()
segmentList.read( 'input/segment_on_source.txt' , blockTime )

# ---- Read the number of chunks from the event list
event_on_file = open('input/event_on_source.txt')
event_on_list = event_on_file.readlines()
nOnEvents = len(event_on_list)
event_on_file.close()

# ---- Add one node to the job for each segment to be analysed.
for i in range(len(segmentList)):
    node = XsearchNode(job)
    # ---- Parameters file:
    if not directedMode or (directedMode and i==0):
        matlab_param_file = cwdstr + "/input/parameters_on_source_" + str(i) + ".txt"
    else:
        matlab_param_file = cwdstr + "/input/parameters_on_source_" + str(0) + ".txt"
    node.set_param_file(matlab_param_file)
    node.set_x_jobnum(i)
    # ---- On-source results files are always written to the local
    #      output/on_source directory - there are very few such jobs.
    node.set_output_dir( os.path.join( cwdstr + "/output/on_source" ) )
    node.set_x_injnum('0')
    mergenode.add_parent(node)
    node.set_retry(retryNumber)
    # ---- Prepend human readable description to node name.
    node.set_name("xdetection_on_source_seg" + str(i) + "_" + node.get_name())
    dag.add_node(node)

# ---- Supply remaining XmergeJob node parameters, add job to the dag.
mergenode.set_dir_prefix("on_source/")
if directedMode:
    mergenode.set_sn_flag("1 " + mergingCutsString)
else:
    mergenode.set_sn_flag("0 " + mergingCutsString)
mergenode.set_retry(retryNumber)
# ---- Prepend human readable description to node name.
mergenode.set_name("xmerge_on_source_" + mergenode.get_name())
dag.add_node(mergenode)

# ---- do the same if clustering.
if smartCluster == True:
    mergeclusterednode.set_dir_prefix("on_source/")
    if directedMode:
        mergeclusterednode.set_sn_flag("1 " + mergingCutsString)
    else:
        mergeclusterednode.set_sn_flag("0 " + mergingCutsString)
    mergeclusterednode.set_sc_flag("1")
    mergeclusterednode.set_retry(retryNumberClustered)
    mergeclusterednode.set_name("xmergeclustered_on_source_" + mergeclusterednode.get_name())
    dag_clustered.add_node(mergeclusterednode)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag job
del dag
del job
del mergejob
# --- if we cluster
if smartCluster == True:
    dag_clustered.write_sub_files()
    dag_clustered.write_dag()
    del dag_clustered
    del mergeclusteredjob


# -------------------------------------------------------------------------
#      Write ul-source dag.
# -------------------------------------------------------------------------

# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_on_source + uuidtag)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_ul_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp,UserName)

if outputType == 'seedless':
    if doGPUUL == "true":
        job = XsearchGPUJob(cp)

# ---- Make instance of XmergeJob.
mergejob = XmergeJob(cp)
# ---- Put a single merge job node in mergejob.  This job will combine
#      into a single file the output of all of the off-source analysis jobs.
mergenode = XmergeNode(mergejob)
if xtmvaFlag:
    xtmvanode.add_parent(mergenode)

# ---- Make analysis jobs for all segments in the on-source segment list.
segmentList = pipeline.ScienceData()
segmentList.read( 'input/segment_on_source.txt' , blockTime )

# ---- Read the number of chunks from the event list
event_on_file = open('input/event_on_source.txt')
event_on_list = event_on_file.readlines()
nOnEvents = len(event_on_list)
event_on_file.close()

# ---- Add one node to the job for each segment to be analysed.
for i in range(len(segmentList)):
    node = XsearchNode(job)
    # ---- Parameters file:
    if not directedMode or (directedMode and i==0):
        matlab_param_file = cwdstr + "/input/parameters_ul_source_" + str(i) + ".txt"
    else:
        matlab_param_file = cwdstr + "/input/parameters_ul_source_" + str(0) + ".txt"
    node.set_param_file(matlab_param_file)
    node.set_x_jobnum(i)
    # ---- Ul-source results files are always written to the local
    #      output/ul_source directory - there are very few such jobs.
    node.set_output_dir( os.path.join( cwdstr + "/output/ul_source" ) )
    node.set_x_injnum('0')
    mergenode.add_parent(node)
    node.set_retry(retryNumber)
    # ---- Prepend human readable description to node name.
    node.set_name("xdetection_ul_source_seg" + str(i) + "_" + node.get_name())
    dag.add_node(node)

# ---- Supply remaining XmergeJob node parameters, add job to the dag.
mergenode.set_dir_prefix("ul_source/")
if directedMode:
    mergenode.set_sn_flag("1 " + mergingCutsString)
else:
    mergenode.set_sn_flag("0 " + mergingCutsString)
mergenode.set_retry(retryNumber)
# ---- Prepend human readable description to node name.
mergenode.set_name("xmerge_ul_source_" + mergenode.get_name())
dag.add_node(mergenode)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag job
del dag
del job
del mergejob


# -------------------------------------------------------------------------
#      Write off-source dag.
# -------------------------------------------------------------------------

# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file_off_source + uuidtag)

# ---- Set the name of the file that will contain the DAG.
dag.set_dag_file( 'grb_off_source' )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp,UserName)

if outputType == 'seedless':
    if doGPUOffSource == "true":
        job = XsearchGPUJob(cp)

# ---- Make instance of XmergeJob.
mergejob = XmergeJob(cp)
# ---- Put a single merge job node in mergejob.  This job will combine
#      into a single file the output of all of the off-source analysis jobs.
mergenode = XmergeNode(mergejob)
if xtmvaFlag:
    xtmvanode.add_parent(mergenode)

# ---- Load off-source segments.
#segmentList = pipeline.ScienceData()
#segmentList.read( 'input/segment_off_source.txt', blockTime )

# ---- KLUDGE FIX: now we have option of limiting number of
#      background jobs using ini file we should ensure that
#      we only run the correct number of jobs.
event_off_file = open('input/event_off_source.txt')
event_off_list = event_off_file.readlines()
nOffEvents = len(event_off_list)
event_off_file.close()
# ---- END OF KLUDGE FIX

# ---- Check how many segments are to be bundled into each job, and create
#      job nodes accordingly.
maxInjNum = int(cp.get('output','maxInjNum'))
if cp.has_option('output','maxOffNum'):
    maxOffNum = int(cp.get('output','maxOffNum'))
else:
    maxOffNum = maxInjNum

# ---- Rescale by the number of sky position so that all jobs have the
#      same length regardless of the number of sky position
maxOffNum = math.ceil(float(maxOffNum)/math.sqrt(float(numSky)))
maxInjNum = math.ceil(float(maxInjNum)/math.sqrt(float(numSky)))

if maxOffNum == 0 :
    # ---- Each segment is to be analysed as a separate job.  There may be
    #      a lot of them, so we will check below if the output files are to
    #      be distributed over the cluster nodes.
    for i in range(nOffEvents):
        node = XsearchNode(job)
        matlab_param_file = cwdstr + "/input/parameters_off_source.txt"
        node.set_param_file(matlab_param_file)
        node.set_x_jobnum(i)
        # ---- Output file to local output/off_source directory.
        node.set_output_dir( os.path.join( cwdstr + "/output/off_source" ) )
        node.set_x_injnum('0')
        mergenode.add_parent(node)
        node.set_retry(retryNumber)
        # ---- Prepend human readable description to node name.
        node.set_name("xdetection_off_source_seg" + str(i) + "_" + node.get_name())
        dag.add_node(node)
else:
    # ---- This option bundles together segments so that maxOffNum segments
    #      are analysed by each condor job node.
    #      In this case all output files come back to the local output/off_source
    #      directory.
    nSets = int(math.ceil(float(nOffEvents)/float(maxOffNum)))
    for i in range(nSets) :
        node = XsearchNode(job)
        if not directedMode or (directedMode and i==0):
            matlab_param_file = cwdstr + "/input/parameters_off_source_" + str(int(math.floor((jobsPerWindow*i)/nSets))) + ".txt"
        else:
            matlab_param_file = cwdstr + "/input/parameters_off_source_" + str(0) + ".txt"
        node.set_param_file(matlab_param_file)
        node.set_x_jobnum("%d-"%(i*maxOffNum) + "%d"%(min((i+1)*maxOffNum-1,nOffEvents-1)))
        node.set_output_dir( os.path.join( cwdstr + "/output/off_source" ) )
        node.set_x_injnum('0')
        mergenode.add_parent(node)
        node.set_retry(retryNumber)
        # ---- Prepend human readable description to node name.
        node.set_name("xdetection_off_source_seg" + str(i) + "_" + node.get_name())
        dag.add_node(node)

# ---- Supply remaining XmergeJob node parameters, add job to the dag.
mergenode.set_dir_prefix("off_source/")
if directedMode:
    mergenode.set_sn_flag("1 " + mergingCutsString)
else:
    mergenode.set_sn_flag("0 " + mergingCutsString)
mergenode.set_retry(retryNumber)
# ---- Prepend human readable description to node name.
mergenode.set_name("xmerge_off_source_" + mergenode.get_name())
dag.add_node(mergenode)
# ---- do the same if clustering.
if smartCluster == True:
    print("Writing off-source clustered dag ... ", file=sys.stdout)
    dag_clustered = pipeline.CondorDAG(log_file_off_source_clustered + uuidtag)
    dag_clustered.set_dag_file( 'grb_off_source_clustered' )
    mergeclusteredjob = XmergeClusteredJob(cp,len(detector))
    mergeclusterednode = XmergeClusteredNode(mergeclusteredjob)
    mergeclusterednode.set_dir_prefix("off_source/")
    if directedMode:
        mergeclusterednode.set_sn_flag("1 " + mergingCutsString)
    else:
        mergeclusterednode.set_sn_flag("0 " + mergingCutsString)
    mergeclusterednode.set_sc_flag("1")
    mergeclusterednode.set_retry(retryNumberClustered)
    mergeclusterednode.set_name("xmergeclustered_off_source_" + mergeclusterednode.get_name())
    dag_clustered.add_node(mergeclusterednode)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag and jobs
del dag
del job
del mergejob
if smartCluster == True:
    dag_clustered.write_sub_files()
    dag_clustered.write_dag()
    del dag_clustered
    del mergeclusteredjob


# -------------------------------------------------------------------------
#      Write on-the-fly simulations dags - one for each waveform set.
# -------------------------------------------------------------------------

# ---- All injection scales for a given waveform set will be handled by a
#      single dag.
if cp.has_section('waveforms') :

    # ---- Read [injection] parameters.
    waveform_set = cp.options('waveforms')

    # ---- Write one dag for each waveform set.
    for set in waveform_set :
        # ---- The user can specify different injection scales for each
        #      waveform set by including a section with that name in the ini
        #      file.  Look for one.  If no special section for this waveform,
        #      then use the default injection scales from [injection].
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else:
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')

        # ---- Create a dag to which we can add jobs.
        dag = pipeline.CondorDAG( log_file_simulations + "_" + set + uuidtag )

        # ---- Set the name of the file that will contain the DAG.
        dag.set_dag_file( "grb_simulations_" + set )

        # ---- Make instance of XsearchJob.
        job = XsearchJob(cp,UserName)

        if outputType == 'seedless':
            if doGPUSimulation == "true":
                job = XsearchGPUJob(cp)

        mergejob = XmergeJob(cp)

        if smartCluster == True:
            print('Writing dag for on-the-fly clustered injection set: ' + set, file=sys.stdout)
            dag_clustered = pipeline.CondorDAG( log_file_simulations_clustered + "_" + set + uuidtag )
            dag_clustered.set_dag_file( "grb_simulations_clustered_" + set )
            mergeclusteredjob = XmergeClusteredJob(cp,len(detector))

        # ---- Make analysis jobs for all segments in the on-source segment list.
        #      Read segment list from file.
        segmentList = pipeline.ScienceData()
        segmentList.read( 'input/segment_on_source.txt' , blockTime )

        # ---- Read injection file to determine number of injections
        #      for this set.
        #      Use system call to wc to figure out how many lines are
        #      in the injection file.
        os.system('wc input/injection_' + set + \
            '.txt | awk \'{print $1}\' > input/' + set + '.txt' )
        f = open('input/' + set + '.txt')
        numberOfInjections = int(f.readline())
        f.close()
        os.system('cat input/injection_' + set + \
                  '.txt | awk \'{printf \"%.9f\\n\",$1+$2*1e-9}\'' + \
                  ' > input/gps' + set + '.txt' )
        f = open('input/gps' + set + '.txt')
        injection_list_time = f.readlines()
        f.close()

        if longInjections == True:
            # ---- this method appends to an existing trigger file whatever bits of a long
            # waveform are in adjacent segments
            # The functionality is as follows: injection X is done first in segment 0 with parameters_0
            # then if straddling next segment is done in segment 1 with parameters_1 but the results are
            # appended to the existing trigger file 
            mergenode = []
            if smartCluster == True:
                mergeclusterednode = []
            for injectionScale in injectionScales :
                mergenode.append(XmergeNode(mergejob))
                if smartCluster == True:
                    mergeclusterednode.append(XmergeClusteredNode(mergeclusteredjob))
            if not(reUseInj):
                # ---- Set buffer for handling long injections.
                longInjSegBuffer = 0
                if set.startswith('adi-a') :
                    longInjSegBuffer = 20
                if set.startswith('adi-b') :
                    longInjSegBuffer = 6
                if set.startswith('adi-c') :
                    longInjSegBuffer = 132
                if set.startswith('adi-d') :
                    longInjSegBuffer = 74
                if set.startswith('adi-e') :
                    longInjSegBuffer = 43
                if set.startswith('mva') :
                    longInjSegBuffer = 132
                if set.startswith('mediummixed') :
                    longInjSegBuffer = 6
                if set.startswith('longmixed') :
                    longInjSegBuffer = 132
                if set.startswith('ebbh-a') :
                    longInjSegBuffer = 7
                if set.startswith('ebbh-d') :
                    longInjSegBuffer = 4
                if set.startswith('ebbh-e') :
                    longInjSegBuffer = 4

                # ---- simplified method to inject, a la allsky.py
                for iInj in range(numberOfInjections) :
                    thisInjSeg = []
                    for i in range(len(segmentList)):
                        if abs(float(event_on_list[i].split(' ')[0]) - float(injection_list_time[iInj]))<=blockTime/2-transientTime+longInjSegBuffer :
                            thisInjSeg.append(i)

                    nodeList = []
                    for i in range(len(thisInjSeg)):
                        if thisInjSeg :
                            thisInjSeg.sort()
                            node = XsearchNode(job)
                            if not directedMode or (directedMode and i==0):
                                matlab_param_file = cwdstr + "/input/parameters_simulation_" + set + "_0_" + str(thisInjSeg[i]) + ".txt"
                            else:
                                matlab_param_file = cwdstr + "/input/parameters_simulation_" + set + "_0_" + str(0) + ".txt"
                            node.set_param_file(matlab_param_file)
                            # --- method of grouping together segments for one condor job
                            #node.set_x_jobnum("%d-"%(thisInjSeg[0]) + "%d"%(thisInjSeg[-1]))
                            # --- we use multiple jobs but output written to a common trigger mat file
                            node.set_x_jobnum(thisInjSeg[i])
                            node.set_output_dir( os.path.join( cwdstr + '/output/simulations_' + set + '_' ) )
                            node.set_x_injnum(str(iInj+1))
                            for scale_counter in range(len(injectionScales)) :
                                mergenode[scale_counter].add_parent(node)
                            node.set_retry(retryNumber)
                            # ---- Prepend human readable description to node name.
                            node.set_name("xdetection_simulation_" + set + "_inj" + str(iInj+1) + "_" + node.get_name())
                            if len(thisInjSeg) == 1:
                                # ---- Add job node to dag
                                dag.add_node(node)
                            else:
                                # ---- Add job node to a list for parent-child relation
                                nodeList.append(node)

                    # --- if we have long injections make sure chunks are always added after the previous chunk
                    # --- in order of their segments, so xappendinjection can be applied correctly
                    if len(thisInjSeg) > 1:
                        for j in range(len(thisInjSeg) - 1):
                            parentNode = nodeList[j]
                            childNode = nodeList[j+1]
                            childNode.add_parent(parentNode)
                            dag.add_node(parentNode)
                        dag.add_node(nodeList[-1])


            # ---- Loop over injection scales.
            for scale_counter in range(len(injectionScales)) :
                # ---- Point to already produced injection results if provided.
                if reUseInj:
                    mergenode[scale_counter].set_dir_prefix(mergingCutsPath + '/../output/simulations_' \
                        + set + '_' + str(scale_counter) + '/')
                else :
                    mergenode[scale_counter].set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
                mergenode[scale_counter].set_sn_flag("1 " + mergingCutsString)
                mergenode[scale_counter].set_retry(retryNumber)
                # ---- Prepend human readable description to node name.
                mergenode[scale_counter].set_name("xmerge_simulation_" + set + "_injScale" \
                    + str(scale_counter) + "_" + mergenode[scale_counter].get_name())
                dag.add_node(mergenode[scale_counter])
                if smartCluster == True:
                    # ---- Point to already produced injection results if provided.
                    if reUseInj:
                        mergeclusterednode[scale_counter].set_dir_prefix(mergingCutsPath + '/../output/simulations_' \
                            + set + '_' + str(scale_counter) + '/')
                    else :
                        mergeclusterednode[scale_counter].set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
                    mergeclusterednode[scale_counter].set_sn_flag("1 " + mergingCutsString)
                    mergeclusterednode[scale_counter].set_sc_flag("1")
                    mergeclusterednode[scale_counter].set_retry(retryNumberClustered)
                    # ---- Prepend human readable description to node name.
                    mergeclusterednode[scale_counter].set_name("xmergeclustered_simulation_" + set + "_injScale" \
                        + str(scale_counter) + "_" + mergeclusterednode[scale_counter].get_name())
                    dag_clustered.add_node(mergeclusterednode[scale_counter])
        else:
            # ---- Loop over injection scales.
            scale_counter = 0
            for injectionScale in injectionScales :
                mergenode = XmergeNode(mergejob)
                if xtmvaFlag:
                    xtmvanode.add_parent(mergenode)
                if smartCluster == True:
                    mergeclusterednode = XmergeClusteredNode(mergeclusteredjob)
                # Skip injection trigger production if path to already available triggers provided
                if not(reUseInj):
                    # ---- Loop over segments
                    for i in range(len(segmentList)):
                        # ---- For debug only
                        #print('We do not have injections that are done at the block border, we use standard GRB injection placing')
                        if  0 == maxInjNum:
                            # ---- Create job node.
                            node = XsearchNode(job)
                            # ---- Assign first argument: parameters file
                            if not directedMode or (directedMode and i==0):
                                matlab_param_file = cwdstr + "/input/parameters_simulation_" \
                                    + set + "_" + str(scale_counter) + '_' + str(i) + ".txt"
                            else:
                                matlab_param_file = cwdstr + "/input/parameters_simulation_" \
                                    + set + "_" + str(scale_counter) + '_' + str(0) + ".txt"
                            node.set_param_file(matlab_param_file)
                            # ---- Assign second argument: job (segment) number
                            #node.set_x_jobnum("%d-"%(thisInjSeg[0]) + "%d"%(thisInjSeg[-1]))
                            node.set_x_jobnum(i)
                            # ---- Assign third argument: output directory
                            # ---- No: Set output directory to local.
                            node.set_output_dir( os.path.join( cwdstr + \
                                '/output/simulations_' + set + '_' + str(scale_counter) ) )
                            # ---- Assign fourth argument: injection number
                            #      KLUDGE: This will screw up injections if more than
                            #      one segment; injection number iterates only over
                            #      injections that fall within analysis interval.
                            node.set_x_injnum("1-%d"%(numberOfInjections))
                            mergenode.add_parent(node)
                            node.set_retry(retryNumber)
                            # ---- Prepend human readable description to node name.
                            node.set_name("xdetection_simulation_" + set + "_seg" \
                                + str(i) + "_injScale" + str(scale_counter) + "_" + node.get_name())
                            dag.add_node(node)
                        else:
                            for iWindow in range(len(injCenterTime)):
                                thisSegmentInjStart = 1e10
                                thisSegmentInjNumber = 0
                                for iInj in range(numberOfInjections) :
                                    if abs(float(event_inj_list[i+iWindow*jobsPerWindow][0])-float(injection_list_time[iInj]))<=blockTime/2-transientTime :
                                        thisSegmentInjNumber += 1
                                        thisSegmentInjStart = min(thisSegmentInjStart,iInj)
                                for iInjRange in range(int(math.ceil(float(thisSegmentInjNumber)/float(maxInjNum)))) :
                                    node = XsearchNode(job)
                                    if not directedMode or (directedMode and i==0):
                                        matlab_param_file = cwdstr + "/input/parameters_simulation_" \
                                            + set + "_" + str(scale_counter) + "_" + str(i) + ".txt"
                                    else:
                                        matlab_param_file = cwdstr + "/input/parameters_simulation_" \
                                            + set + "_" + str(scale_counter) + "_" + str(0) + ".txt"
                                    node.set_param_file(matlab_param_file)
                                    node.set_x_jobnum(i+iWindow*jobsPerWindow)
                                    node.set_output_dir( os.path.join( cwdstr \
                                        + '/output/simulations_' + set + '_' + str(scale_counter) ) )
                                    node.set_x_injnum("%d"%(thisSegmentInjStart+iInjRange*maxInjNum+1) + "-" \
                                        + "%d"%(thisSegmentInjStart+min((iInjRange+1)*maxInjNum,thisSegmentInjNumber)))
                                    mergenode.add_parent(node)
                                    node.set_retry(retryNumber)
                                    # ---- Prepend human readable description to node name.
                                    node.set_name("xdetection_simulation_" + set +  "_seg" + \
                                        str(i+iWindow*jobsPerWindow) + "_injScale" + str(scale_counter) \
                                        + "_injRange" + str(iInjRange) + "_" + node.get_name())
                                    dag.add_node(node)
                                    # ---- Add job node to the dag.
                # ---- Continue on to the next injection scale.
                # point to already produced injection results if provided
                if reUseInj:
                    mergenode.set_dir_prefix(mergingCutsPath + '/../output/simulations_' + set + '_' + str(scale_counter) + '/')
                else:
                    mergenode.set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
                if directedMode:
                    mergenode.set_sn_flag("1 " + mergingCutsString)
                else:
                    mergenode.set_sn_flag("0 " + mergingCutsString)
                mergenode.set_retry(retryNumber)
                # ---- Prepend human readable description to node name.
                mergenode.set_name("xmerge_simulation_" + set + "_injScale" \
                                + str(scale_counter) + "_" + mergenode.get_name())
                dag.add_node(mergenode)
                if smartCluster == True:
                    # ---- Point to already produced injection results if provided.
                    if reUseInj:
                        mergeclusterednode.set_dir_prefix(mergingCutsPath + '/../output/simulations_' \
                            + set + '_' + str(scale_counter) + '/')
                    else :
                        mergeclusterednode.set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
                    mergeclusterednode.set_sn_flag("1 " + mergingCutsString)
                    mergeclusterednode.set_sc_flag("1")
                    mergeclusterednode.set_retry(retryNumberClustered)
                    # ---- Prepend human readable description to node name.
                    mergeclusterednode.set_name("xmergeclustered_simulation_" + set + "_injScale" \
                        + str(scale_counter) + "_" + mergeclusterednode.get_name())
                    dag_clustered.add_node(mergeclusterednode)
                scale_counter = scale_counter + 1

        # ---- Write out the submit files needed by condor.
        dag.write_sub_files()
        # ---- Write out the DAG itself.
        dag.write_dag()

        # ---- Delete used dag job
        del dag
        del job
        del mergejob
        if smartCluster == True:
            dag_clustered.write_sub_files()
            dag_clustered.write_dag()
            del dag_clustered
            del mergeclusteredjob

# -------------------------------------------------------------------------
#      Write MDC simulation dags - one for each MDC set.
# -------------------------------------------------------------------------

# ---- All injection scales for a given MDC set will be handled by a
#      single dag.

# ---- Check for MDC sets.
if cp.has_option('mdc','mdc_sets') :

    # ---- Status message.
    print("Writing MDC job dag files ... ", file=sys.stdout)

    # ---- Get list of MDC sets to process.
    mdc_setsList = cp.get('mdc','mdc_sets')
    mdc_sets = mdc_setsList.split(',')

    # ---- Check how many MDC segments are to be bundled into each xdetection job.
    maxMDCSegNum = int(cp.get('output','maxMDCSegNum'))

    # ---- Write one dag for each waveform set.
    for set in mdc_sets :
        print('Writing dag for MDC set:' + set)
        if cp.has_section(set) & cp.has_option(set,'injectionScales') :
            injectionScalesList = cp.get(set,'injectionScales')
            injectionScales = injectionScalesList.split(',')
        else:
            injectionScalesList = cp.get('injection','injectionScales')
            injectionScales = injectionScalesList.split(',')

        # ---- Create a dag to which we can add jobs.
        dag = pipeline.CondorDAG( log_file_simulations + "_" + set + uuidtag)

        # ---- Set the name of the file that will contain the DAG.
        dag.set_dag_file( "grb_" + set )

        # ---- Make instance of XsearchJob.
        job = XsearchJob(cp,UserName)

        # ---- Make instance of XmergeJob. This job will merge the results files
        #      produced by the several analysis nodes.
        mergejob = XmergeJob(cp)

        #      Make analysis jobs for all segments in the MDC segment list.
        #      Read segment list from file.
        segFileName = 'input/segment_' + set +  '.txt'
        if not os.path.isfile(segFileName):
            print("Error: non existant segment file: ",segFileName, file=sys.stderr)
            sys.exit(1)

        segmentList = []
        segmentList = pipeline.ScienceData()
        segmentList.read( segFileName , blockTime )

        # ---- Read ini file to determine number of injections for this set.
        numberOfInjections = int(cp.get(set,'numberOfChannels'))

        # ---- Loop over injection scales.
        scale_counter = 0
        for injectionScale in injectionScales :
            # ---- Merge all results for a single waveform+injection scale.
            mergenode = XmergeNode(mergejob)
            if xtmvaFlag:
                xtmvanode.add_parent(mergenode)

            # ---- Set up of jobs depends on how many injections are to be done
            #      by each job: a fixed number (maxInjNum,maxMDCSegNum), or all
            #      (maxInjNum==0,maxMDCSegNum==0).
            if maxInjNum == 0 and maxMDCSegNum == 0:
                # ---- Each analysis job will analyse any and all injections in
                #      its analysis segment.
                i = 0

                # ---- Create job node.
                node = XsearchNode(job)
                # ---- Assign first argument: parameters file
                matlab_param_file = cwdstr + "/input/parameters_" + set \
                    + "_" + str(scale_counter) + ".txt"
                node.set_param_file(matlab_param_file)
                # ---- Assign second argument: job (segment) number
                node.set_x_jobnum(i)
                # ---- Assign third argument: output directory
                # ---- No: Set output directory to local.
                node.set_output_dir( os.path.join( cwdstr + \
                    '/output/simulations_' + set + '_' + str(scale_counter) ) )
                # ---- Assign fourth argument: injection number
                #      KLUDGE: This will screw up injections if more than
                #      one segment; injection number iterates only over
                #      injections that fall within analysis interval.
                node.set_x_injnum("1-%d"%(numberOfInjections))
                mergenode.add_parent(node)
                node.set_retry(retryNumber)
                # ---- Prepend human readable description to node name.
                node.set_name("xdetection_simulation_" + set + "_injScale" \
                    + str(scale_counter) + "_" + node.get_name())
                dag.add_node(node)
            # ---- if maxInjNum and maxMDCSegNum not 0
            else:
                # ---- Analyse maxInjNum injections in each job.  In this case output
                #      files will go to the local directory output/simulations_*.
                segJobs = list(range(int(math.ceil(float(len(segmentList))/float(maxMDCSegNum)))))
                injJobs = list(range(int(math.ceil(float(numberOfInjections)/float(maxInjNum)))))
                for iSegRange in segJobs :
                    for iInjRange in injJobs :
                        node = XsearchNode(job)
                        matlab_param_file = cwdstr + "/input/parameters_" + set \
                            + "_" + str(scale_counter) + ".txt"
                        node.set_param_file(matlab_param_file)

                        if len(segJobs)==1:
                            # ---- if we only have one segment
                            node.set_x_jobnum('0')
                        else:
                            node.set_x_jobnum("%d"%(iSegRange*maxMDCSegNum) + "-" + \
                                "%d"%(min((iSegRange+1)*maxMDCSegNum-1,len(segmentList)-1)))

                        node.set_output_dir( os.path.join( cwdstr + '/output/simulations_' \
                            + set + '_' + str(scale_counter) ) )

                        # ---- WARNING - do not set injNum = 0 when running MDCs or
                        #      we will not recreate the channel name properly in xdetection
                        node.set_x_injnum("%d"%(iInjRange*maxInjNum+1) + "-" \
                            + "%d"%(min((iInjRange+1)*maxInjNum,numberOfInjections)))

                        mergenode.add_parent(node)
                        node.set_retry(retryNumber)
                        # ---- Prepend human readable description to node name.
                        node.set_name("xdetection_simulation_" + set + "_seg" \
                            + str(iSegRange) + "_injScale" + str(scale_counter) \
                            + "_injRange" + str(iInjRange) + "_" + node.get_name())
                        dag.add_node(node)

            # ---- Add job node to the dag.
            # ---- Continue on to the next injection scale.
            mergenode.set_dir_prefix('simulations_' + set + '_' + str(scale_counter) + '/')
            if directedMode:
                mergenode.set_sn_flag("1 " + mergingCutsString)
            else:
                mergenode.set_sn_flag("0 " + mergingCutsString)
            mergenode.set_retry(retryNumber)
            # ---- Prepend human readable description to node name.
            mergenode.set_name("xmerge_simulation_" + set + "_seg" + \
                "_injScale" + str(scale_counter) + "_" + mergenode.get_name())
            dag.add_node(mergenode)
            scale_counter = scale_counter + 1

        # ---- end loop over injections scales

        # ---- Write out the submit files needed by condor.
        dag.write_sub_files()
        # ---- Write out the DAG itself.
        dag.write_dag()

        # ---- Delete used dag job
        del dag
        del job
        del mergejob
    # ---- end loop overmdc sets

# -------------------------------------------------------------------------
#      Write xtmva dag.
# -------------------------------------------------------------------------

if xtmvaFlag:
    dag_xtmva.add_node(xtmvanode)
    dag_xtmva.write_sub_files()
    dag_xtmva.write_dag()
    del dag_xtmva
    del xtmvajob

# -------------------------------------------------------------------------
#      Write single dag containing all jobs.
# -------------------------------------------------------------------------

# ---- Use grep to combine all dag files into a single dag, with the
#      PARENT-CHILD relationships listed at the end.
print("Combining all jobs into a single dag ...")
os.system('rm -f grb_alljobs.dag')
os.system('echo "DOT xpipeline_triggerGen.dot" > .dag_temp')
os.system('grep -h -v PARENT *.dag >> .dag_temp')
os.system('grep -h PARENT *.dag >> .dag_temp')
os.system('mv .dag_temp grb_alljobs.dag')


# -------------------------------------------------------------------------
#      Finished.
# -------------------------------------------------------------------------

# ---- Status message.
print("... finished writing job submission files. ", file=sys.stdout)
print(file=sys.stdout)

print("############################################", file=sys.stdout)
print("#           Completed.                     #", file=sys.stdout)
print("############################################", file=sys.stdout)

# ---- Append to summary file.
sfile = open(summary_file,'a')
sfile.write('\t 1 \n')
sfile.close()

# ---- Exit cleanly
sys.exit( 0 )


