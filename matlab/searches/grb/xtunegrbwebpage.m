function [] = xtunegrbwebpage(filepath,grb_name,...
    user_tag,xmake_args_in,xmake_args_out,vetoMethod)
% XTUNEGRBWEBPAGE - identify and apply optimal consistency test thresholds.
%
% xtunegrbwebpage reads mat files from vetoTestOnly runs of xmakegrbwebpage,
% identifies the veto cuts that give the lowest expected UL, and then
% prepares the input-argument and sub files for xmakegrbwebpage to produce
% a webpage with the optimal veto cuts. 
%
% Usage:
%   xtunegrbwebpage(filepath,grb_name,user_tag,xmake_args_in,...
%                   xmake_args_out,vetoMethod) 
%
%   filepath            String. Path to auto_web dir.
%   grb_name            String. Name of GRB, e.g. GRB070729.
%   user_tag            String. Description of current analysis, e.g., TEST
%   xmake_args_in       String. Filename of file containing list of arguments
%                       which we will run xmakegrbwebpage with. This should
%                       contain the following strings in place of the 
%                       appropriate veto cut values:
%                       __NULLVETO__, PLUSVETO__ and __CROSSVETO__.
%                       This script will will write out the same list of
%                       arguments with the __NULLVETO__ etc. strings replaced 
%                       by the optimal veto cuts.
%   xmake_args_out      String. Name of file to be written which will contain
%                       the arguments for the final xmakegrbwebpage job.  
%   vetoMethod          String. Veto method as recognised by
%                       xmakegrbwebpage.
%
% This function requires the files xmakegrbwebpage_open.sub and
% xmakegrbwebpage_ul.sub to exist in the current directory.
%
% $Id$

format compact


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(6, 6, nargin));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Find veto cut combination that minimizes the upper limit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Identify matlab files.
globStr  = [filepath '/' grb_name '_' user_tag '*vetoTest' vetoMethod '.mat'];
matFiles = dir(globStr);
numFiles = size(matFiles,1);

% ---- Check that we found some mat files.
if numFiles == 0
    error(['No files were found in ' globStr])
end

% ---- Check that we found the expected number of mat files.
[o result]=system(['cat ' filepath ...
                   '/grb_web.dag | gawk "/JOB xmakegrbwebpage_vetoTest_' ...
                   vetoMethod '_/{print}" | wc -l']);
if numFiles ~= str2num(result)
  error(['Error, expected to find ' result ' veto tuning mat files. ' ...
         'Found ' num2str(numFiles) ' files instead.']);
end

% ---- Initialise variables used to store data we will read from the mat
%      files.
ulVector90_temp     = [];
ulVector50_temp     = [];
vetoNullRangePlots_temp  = [];
vetoPlusRangePlots_temp  = [];
vetoCrossRangePlots_temp = [];
detectionStat_temp       = {};

% ---- Loop over matFiles.
for fileIdx = 1:numFiles

    % ---- Load contents of current matlab file.
    thisMatFile = [filepath '/' matFiles(fileIdx).name];
    disp(['Loading ' thisMatFile])
    contents = load(thisMatFile, ...
      'ulVector90', 'ulVector50', 'nameCell', 'parametersCell', ...
      'parametersCellClean', ...
      'setNameCell', 'vetoNullRangePlots', 'vetoPlusRangePlots', ...
      'vetoCrossRangePlots', 'tuningNameStr', 'vetoMethod', 'nullVetoType', ...
      'plusVetoType', 'crossVetoType', 'cFreq', 'detectionStat');

    % ---- Concatenate data that changes from file to file.
    ulVector90_temp     = [ulVector90_temp;     contents.ulVector90];
    ulVector50_temp     = [ulVector50_temp;     contents.ulVector50];
    vetoNullRangePlots_temp  = [vetoNullRangePlots_temp;  contents.vetoNullRangePlots];
    vetoPlusRangePlots_temp  = [vetoPlusRangePlots_temp;  contents.vetoPlusRangePlots];
    vetoCrossRangePlots_temp = [vetoCrossRangePlots_temp; contents.vetoCrossRangePlots];
    for iCut = 1:length(contents.vetoPlusRangePlots)
        detectionStat_temp = {detectionStat_temp{:}, contents.detectionStat};
    end

    % ---- If on the last file, store variables which are the same across all
    %      files. We use the last (rather than first) file for finding these
    %      variables because sometimes the first file contains no veto cuts, 
    %      so vetoMethod is set to 'None' which can cause problems later
    %      when calling xoptimalvetotuning.
    if fileIdx == numFiles
        nameCell            = contents.nameCell;
        parametersCell      = contents.parametersCell;
        parametersCellClean = contents.parametersCellClean;
        setNameCell         = contents.setNameCell;
        tuningNameStr       = contents.tuningNameStr;
        vetoMethod          = contents.vetoMethod;  
        nullVetoType        = contents.nullVetoType; 
        plusVetoType        = contents.plusVetoType; 
        crossVetoType       = contents.crossVetoType;
        cFreq               = contents.cFreq; 
    end

    % ---- Clear the data for the current mat file before loading the next
    %      one.
    clear contents

    % ---- Move tuning file to storage.
    if 1==fileIdx
        [status,result] = system('mkdir -p tuning_mat_files');
        if status~=0 
            error('Unable to make directory to store tuning mat files.');
        end
    end
    [status,result] = system(['mv ' thisMatFile ' tuning_mat_files/']);

end

% ---- Rename concatenated variables.
ulVector90     = ulVector90_temp;
ulVector50     = ulVector50_temp;
vetoNullRangePlots  = vetoNullRangePlots_temp;
vetoPlusRangePlots  = vetoPlusRangePlots_temp;
vetoCrossRangePlots = vetoCrossRangePlots_temp;
detectionStat       = detectionStat_temp;

% ---- Find the index of the veto combination that gives the best expected
%      upper limit.
[bestULIdx] = xoptimalvetotuning(ulVector90,setNameCell,...
                  vetoNullRangePlots,vetoPlusRangePlots,vetoCrossRangePlots,...
                  tuningNameStr,vetoMethod,0);

% ---- Identify optimal vetos.
optimalNullCut  = vetoNullRangePlots(bestULIdx);
optimalPlusCut  = vetoPlusRangePlots(bestULIdx);
optimalCrossCut = vetoCrossRangePlots(bestULIdx);
optimalDetectionStat = detectionStat{bestULIdx};

% ---- Save combined tuning data into a single file for easy use (eg for
%      user exploration tuning options by hand offline). 
combinedMatFileName  = [filepath '/' grb_name '_' user_tag '_vetoTest_' vetoMethod '.mat'];
save(combinedMatFileName, ...
      'ulVector90', 'ulVector50', 'nameCell', 'parametersCell', ...
      'parametersCellClean', ...
      'setNameCell', 'vetoNullRangePlots', 'vetoPlusRangePlots', ...
      'vetoCrossRangePlots', 'tuningNameStr', 'vetoMethod', 'nullVetoType', ...
      'plusVetoType', 'crossVetoType', 'cFreq', 'detectionStat', ...
      'bestULIdx', 'optimalNullCut', 'optimalPlusCut', 'optimalCrossCut', 'optimalDetectionStat');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Create table showing upper limits measured for each veto cut combination
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vetoTableFileName = [grb_name '_' user_tag '_vetoTable' vetoMethod '.html'];

xwritevetotable(vetoTableFileName,vetoMethod,tuningNameStr,...
    nullVetoType,vetoNullRangePlots,...
    plusVetoType,vetoPlusRangePlots,...
    crossVetoType,vetoCrossRangePlots,...
    setNameCell,parametersCell,bestULIdx,ulVector90,cFreq,detectionStat);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Edit xmakegrbwebpage input and sub files with optimal veto cut combination
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Read xmakegrbwebpage args from file
argfilepath = [filepath '/' xmake_args_in]
disp(['Reading xmakegrbwebpage arguments from: ' argfilepath ]);
fargs = fopen(argfilepath,'r');
xmakeargsCell = textscan(fargs,'%s');
fclose(fargs);

xmakeargs = '';
for idx = 1:length(xmakeargsCell{1})
    xmakeargs = [xmakeargs  char(xmakeargsCell{1}(idx)) ' '];
end

% ---- Replace __XXXXVETO__ strings with the appropriate optimal veto cut
xmakeargs = regexprep(xmakeargs, '__NULLVETO__', num2str(optimalNullCut));
xmakeargs = regexprep(xmakeargs, '__PLUSVETO__', num2str(optimalPlusCut));
xmakeargs = regexprep(xmakeargs, '__CROSSVETO__', num2str(optimalCrossCut));
% ---- Put the name of the selected detection statistic
xmakeargs = regexprep(xmakeargs, '__DETECTIONSTAT__', optimalDetectionStat);

% ---- Write file containing arguments for final closed-box xmakegrbwebpage job
argfilepath = [filepath '/' xmake_args_out]
disp(['Writing xmakegrbwebpage arguments to: ' argfilepath ]);
fargs = fopen(argfilepath,'w');
fprintf(fargs,'%s\n',xmakeargs);
fclose(fargs);

% ---- Write file containing digest of the 3 tuning cuts
argfilepath = [filepath '/' xmake_args_out '.digest']
disp(['Writing xmakegrbwebpage digest arguments to: ' argfilepath ]);
fargs = fopen(argfilepath,'w');
fprintf(fargs,'%f %f %f\n',optimalNullCut, optimalPlusCut, optimalCrossCut);
fclose(fargs);

% ---- The tuned veto thresholds are written directly into the .sub files
%      for the open box and ul box analyses, rather than to a .txt file.

% ---- Edit submit file to run open box analysis
xmakeargs_open = regexprep(xmakeargs, 'closedbox', 'openbox');
command = ['sed -e ''s|arguments = \" .* \"|arguments = \" ' xmakeargs_open ...
           ' \"|g'' xmakegrbwebpage_open.sub > dump'];
system(command);
command = 'mv dump xmakegrbwebpage_open.sub';
system(command);

% ---- Edit submit file to run ul box analysis
xmakeargs_ul = regexprep(xmakeargs, 'closedbox', 'ulbox');
command = ['sed -e ''s|arguments = \" .* \"|arguments = \" ' xmakeargs_ul ...
           ' \"|g'' xmakegrbwebpage_ul.sub > dump'];
system(command);
command = 'mv dump xmakegrbwebpage_ul.sub';
system(command);

% ---- Done
