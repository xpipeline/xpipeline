function xconvertgrbtoearth(fileName,raString,decString,gpsString)
% XCONVERTGRBTOEARTH - Convert (ra,dec) to Earth-fixed coordinates, write file.
%
% xconvertgrbtoearth converts a single right ascension, declination, and 
% GPS time to corresponding Earth-fixed coordinates, and writes the result
% to a file.
%
%    xconvertgrbtoearth(fileName,raString,decString,gpsString)
%
%   fileName	String.  Name of output text file.
%   raString    String.  Right ascension of GRB (degrees).
%   decString   String.  Declination of GRB (degrees).
%   gpsString   String.  GPS time of GRB.
% 
% All of the input arguments are strings because this function needs to be
% compiled for use with the automated data analysis pipeline.
%
% original write: Patrick J. Sutton 2007.08.20
%
% $Id$

% ---- Convert string inputs to numbers.
ra = str2num(raString);
dec = str2num(decString);
gps = str2num(gpsString);

% ---- Convert to Earth-based coordinates.
[phi, theta] = radectoearth(ra,dec,gps);

% ---- Write the results to a file.
fid = fopen(fileName,'w');
fprintf(fid,'%f %f',[phi, theta]);
fclose(fid);

% ---- Done.
return

