function [] = xwriteonoffsourceinfo(fout,analysis,onSource,offJobSeed,...
    dummyOnSourceJobNumber,offJob_tuning,offJob_ulCalc,uniqueOffJobNumberFail)
% XWRITEONOFFSOURCEINFO - write lists of on- and off-source jobs to webpage.
% 
% xwriteonoffsourceinfo writes lists of on- and off-source jobs used for
% veto tuning and upper limit calculations to a webpage. It is a helper 
% function for xmakegrbwebpage.
%
% Usage:
%  xwriteonoffsourceinfo(fout,analysis,onSource,offJobSeed,...
%    dummyOnSourceJobNumber,offJob_tuning,offJob_ulCalc,uniqueOffJobNumberFail)
%
%    fout           Pointer to open and writable webpage file  
%    analysis       Structure created by xmakegrbwebpage
%    onSource       Structure containing onSource events
%    offJobSeed     Random seed that was used to select offSource jobs
%                   for veto tuning or upper limit calc
%    dummyOnSourceJobNumber
%                   jobNumber of the offSource job we will use as
%                   our dummy offSource job
%    offJob_tuning  jobNumbers of offSource jobs we will use for veto tuning
%    offJob_ulCalc  jobNumbers of offSource jobs we will use for UL calc
%    uniqueOffJobNumberFail 
%                   jobNumbers of offSource jobs which fail DQ criteria
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(8, 8, nargin));

% ---- Print section header.
fprintf(fout,'%s\n','<h2>On-Source and Off-Source Events (no injections)</h2>');

fprintf(fout, '%s\n', ['We use random number generator rand with seed = '...
                       num2str(offJobSeed) ' to choose off Source jobs ' ...
                       'for veto tuning and upper limit calculation']);
fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Write list of jobNumbers discarded due to bad DQ
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="offJobsDiscarded" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''offJobsDiscarded'');" />      ']);
fprintf(fout, ['The ' num2str(length(uniqueOffJobNumberFail)) ... 
               ' off source segments with the following job numbers '...
               'were discarded since they failed our DQ criteria \n']);
fprintf(fout,'%s\n',[' <div id="div_offJobsDiscarded" style="display: none;"> ']);

for iJob = 1:length(uniqueOffJobNumberFail)
   fprintf(fout,'%s,\n',num2str( uniqueOffJobNumberFail(iJob) ));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_offJobsDiscarded
fprintf(fout, '<br> \n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write list of jobNumbers used for tuning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="offJobsForTuning" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''offJobsForTuning'');" />      ']);
fprintf(fout, ['For tuning we use the ' num2str(length(offJob_tuning)) ...
               ' off source segments with the following job numbers: \n']);
fprintf(fout,'%s\n',[' <div id="div_offJobsForTuning" style="display: none;"> ']);
for iJob = 1:length(offJob_tuning)
   fprintf(fout,'%s,\n',num2str(offJob_tuning(iJob)));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_offJobsForTuning
fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write list of jobNumbers used for ul calc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n',[' <input id="offJobsForULCalc" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''offJobsForULCalc'');" />      ']);
fprintf(fout, ['For UL calc we use the ' num2str(length(offJob_ulCalc)) ... 
               ' off source segments with the following job numbers: \n']);
fprintf(fout,'%s\n',[' <div id="div_offJobsForULCalc" style="display: none;"> ']);
for iJob = 1:length(offJob_ulCalc)
   fprintf(fout,'%s,\n ',num2str(offJob_ulCalc(iJob)));
end
fprintf(fout,'%s\n','</div>'); %-- end of div_offJobsForULCalc
fprintf(fout, '<br> \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Write event properties to a table.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<br>');
fprintf(fout,'%s\n','AnalysisTimes used are :');
for iAT = 1:length(analysis.analysisTimes)
    if analysis.analysisTimes(iAT) >= 1
        fprintf(fout,'%g s, ',analysis.analysisTimes(iAT));
    else
        fprintf(fout,'1/%g s, ',1/analysis.analysisTimes(iAT));
    end
end
fprintf(fout,'%s\n','<br>');

