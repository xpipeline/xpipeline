function [] = xwritevetotable(vetoTableFileName,vetoMethod,tuningNameStr, ...
    nullVetoType,vetoNullRange,plusVetoType,vetoPlusRange, ...
    crossVetoType,vetoCrossRange,nameCell,parametersCell,bestULIdx, ...
    ulVector,cFreq,detectionStat)
% XWRITEVETOTABLE - Write html file with table of upper limits vs. threshold.
%
% usage:
% 
% [] = xwritevetotable(vetoTableFileName,tuningNameStr, ...
%     nullVetoType,vetoNullRange,plusVetoType,vetoPlusRange, ...
%     crossVetoType,vetoCrossRange,nameCell,parametersCell,bestULIdx, ...
%     ulVector,cFreq)
%
% vetoTableFileName   String.  Name of output html file.
% vetoMethod          String. Determines what type of veto ratio
%                     test we will perform.
%                     Either 'linearCut' or 'linearCutCirc' or 'medianCut' or
%                     'medianCutCirc' or  'medianLinCut' or
%                     'medianLinCutCirc'or 'None' or 'linearCutScalar'.
% tuningNameStr       String.  Name of waveform to be used for tuning, or
%                     'all'.
% nullVetoType        String. Either 'nullIoverE' or 'nullEoverI'.
%                     Determines which ratio test was performed, used
%                     to label table columns.
% vetoNullRange       Vector of all null cuts investigated.  
% plusVetoType        String. Either 'plusIoverE', 'plusEoverI', 'circEoverI'
%                     or 'scalarEoverI'
%                     Determines which ratio test was performed, used
%                     to label table columns.
% vetoPlusRange       Vector of all null cuts investigated.
% crossVetoType       String. Either 'crossIoverE' or 'crossEoverI'
%                     Determines which ratio test was performed, used
%                     to label table columns.
% vetoCrossRange      Vector of all null cuts investigated.
% nameCell            Cell array. Names of waveforms investigated, used
%                     to label table columns.   
% parametersCell      Cell array. Params of waveforms investigated, used
%                     to label table columns. 
% bestULIdx           Integer. Index of optimal veto choice, used to 
%                     highlight optimal veto combination and UL in table.  
% ulVector            Array. ULs for each waveform for each combination
%                     of veto cuts.
% cFreq               Vector. Central frequency of each waveform investigated.
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(15, 15, nargin));

% ---- Open veto table file.
fvtbl = fopen(vetoTableFileName,'w');

% ---- tuningNameStr is either a tilde-delimited string or 'all';
%      unpackage & assign to cell array
tuningNameCell = {};
tuningNameCell = lower(tildedelimstr2numorcell(tuningNameStr));

% ---- create id tag based on file name
ulTuningTag = vetoTableFileName(1:end-5);

% ---- retrieve by what percentage level ULs, the 90% UL coulumn has been
%      overwritten
percentageLvl = xoverrideullevel(vetoMethod);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     html table showing uls obtained for different veto cuts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
fprintf(fvtbl,'%s\n',[' <input id="' ulTuningTag  '" type="checkbox" unchecked ']);
fprintf(fvtbl,'%s\n',[' onclick="toggleVisible(''' ulTuningTag ''');" /> ']);
fprintf(fvtbl,'%s\n',[' Table of upper limits obtained '...
                     'for different veto thresholds.']);
fprintf(fvtbl,'%s',[' The optimal veto thresholds are '...
                    'those which minimize the ' percentageLvl '% hrss UL for ']);

fprintf(fvtbl,'%s', tuningNameStr);

fprintf(fvtbl,'%s\n',' waveforms <br>');
fprintf(fvtbl,'%s\n',[' <div id="div_' ulTuningTag '" style="display: none;"> ']);

% ---- Write table headers.
fprintf(fvtbl,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fvtbl,'%s\n',['<tr>']);
fprintf(fvtbl,'%s\n',['<td><b>' nullVetoType   '</b></td>']);
fprintf(fvtbl,'%s\n',['<td><b>' plusVetoType   '</b></td>']);
fprintf(fvtbl,'%s\n',['<td><b>' crossVetoType  '</b></td>']);
fprintf(fvtbl,'%s\n',['<td><b> detection statistic </b></td>']);


% ---- Write names of waveforms tested. Colour this header LightSkyBlue if
%      we have tuned our veto cuts using this waveform.
for iWave = 1:length(nameCell)
  if max(strcmp(nameCell{iWave},tuningNameCell)) | ...
          (strcmp('all',tuningNameCell))
        cellcolor = 'LightSkyBlue';
    else
        cellcolor = 'white';
    end
    fprintf(fvtbl,'%s\n',['<td bgcolor="' cellcolor '"> <b> ' percentageLvl  '% hrss UL for ' ...
        nameCell{iWave} '~' parametersCell{iWave} '</b></td>']);
end
fprintf(fvtbl,'%s\n',['</tr>']);

% ---- Write rows of table, each row corresponds to a different
%      combination of veto cuts.
for thisVeto=1:length(vetoNullRange)
    % ---- If the current row corresponds to the optimal combination of
    %      vetoes then colour this row in red.
    if (thisVeto ==bestULIdx)
        rowcolor = 'red';
    else
        rowcolor = 'white';
    end
    fprintf(fvtbl,['<tr bgcolor="' rowcolor '">']);

    % ---- Write out the numerical values of the current veto cut
    %      combination.
    fprintf(fvtbl,'%s\n',['<td>' ...
        num2str(vetoNullRange(thisVeto))  '</td>']);
    fprintf(fvtbl,'%s\n',['<td>' ...
        num2str(vetoPlusRange(thisVeto))  '</td>']);
    fprintf(fvtbl,'%s\n',['<td>' ... 
        num2str(vetoCrossRange(thisVeto)) '</td>']);
    fprintf(fvtbl,'%s\n',['<td>' ... 
        detectionStat{thisVeto} '</td>']);

    % ---- Loop over the waveforms writing the upper limits
    %      obtained.
    for iWave = 1:length(nameCell)
        if max(strcmp(nameCell{iWave},tuningNameCell)) | ...
              (strcmp('all',tuningNameCell))
            if (thisVeto ==bestULIdx)
                cellcolor = 'red';
            else
                cellcolor = 'LightSkyBlue';
            end
        else
            cellcolor = rowcolor;
        end
        fprintf(fvtbl,'%s\n',['<td bgcolor="' cellcolor '"> ' ...
            num2str(ulVector(thisVeto,iWave)) '</td>']);
    end
    fprintf(fvtbl,'%s\n',['</tr>']);
end
fprintf(fvtbl,'%s\n','</table><br>');
fprintf(fvtbl,'%s\n','</div>'); %-- end of div_ulTuning.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Color map showing ULs expected for different veto thresholds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % ---- This section crashes because we have not input the variables
% %      figures_dirName,figfiles_dirName, which determine where the plots
% %      are saved.  Since the plots don't use the correct statistic anyway,
% %      we comment out this block of code.
% 
% % ---- Make plot of ULs for various vetos if we have done tuning.
% 
% % ---- Find out how many veto ratios we varied.
% dim = 0;
% array = [];
% axisnames = {};
% 
% if (length(unique(vetoNullRange))>1)
%     dim = dim + 1;
%     array(:,dim)   = vetoNullRange;
%     axisnames{dim} = nullVetoType;
% end
% 
% if (length(unique(vetoPlusRange))>1)
%     dim = dim + 1;
%     array(:,dim)   = vetoPlusRange;
%     axisnames{dim} = plusVetoType;
% end
% 
% if (length(unique(vetoCrossRange))>1)
%     dim = dim + 1;
%     array(:,dim)   = vetoCrossRange;
%     axisnames{dim} = crossVetoType;
% end
% 
% % ---- KLUDGE:  Use the ULs for the first waveform only.  Should really use
% % the same statistic used for making the optimal veto selection.
% idxULTune = 1;
% 
% if (dim == 1)
%     
%     fprintf(fvtbl,'%s\n',['Color map showing how 90% upper limit at ' ...
%         num2str(cFreq(idxULTune)) 'Hz varies with veto ratio choices']);
% 
%     figure;set(gca,'FontSize',20);
%     scatter(ones(length(array(:,1)),1),array(:,1),50,...
%         ulVector(:,idxULTune),'filled')
%     hold on
%     scatter(1,array(bestULIdx,1),200,...
%          ulVector(bestULIdx,idxULTune),'om','LineWidth',3)
%     hold off
%     axis([0.5 1.5 min(array(:,1))-0.5 max(array(:,1))+0.5 ]);
%     xlabel(' ');
%     ylabel(axisnames{1});
%     colorbar;
%     % ---- Save plot.
%     plotNameMap = 'ul_tuning';
%     xsavefigure(plotNameMap,figures_dirName,figfiles_dirName);
%     % ---- Add figure table to web page.
%     xaddfiguretable(fvtbl,plotNameMap,figures_dirName);
%     clear plotNameMap
% 
% elseif (dim == 2)
%     
%     fprintf(fvtbl,'%s\n',['Color map showing how 90% upper limit at ' ...
%                          num2str(cFreq(idxULTune)) ...
%                          'Hz varies with veto ratio choices']);
% 
%     figure;set(gca,'FontSize',20);
%     scatter(array(:,1),array(:,2),50,ulVector(:,idxULTune),'filled');
%     hold on;
%     scatter(array(bestULIdx,1),array(bestULIdx,2),200,...
%         ulVector(bestULIdx,idxULTune),'om','LineWidth',3)
%     hold off;
%     axis([min(array(:,1))-0.5 max(array(:,1))+0.5  ...
%           min(array(:,2))-0.5 max(array(:,2))+0.5 ]);
%     xlabel(axisnames{1});
%     ylabel(axisnames{2});
%     colorbar;
%     % ---- Save plot.
%     plotNameMap = 'ul_tuning';
%     xsavefigure(plotNameMap,figures_dirName,figfiles_dirName);
%     % ---- Add figure table to web page.
%     xaddfiguretable(fvtbl,plotNameMap,figures_dirName);
%     clear plotNameMap
% 
% elseif (dim == 3)
%     
%     fprintf(1,'%s\n',['Color map showing how ULs vary with veto ratio '...
%         'not coded for 3 dimensions yet']);
% 
% else
%     
%     disp('something has gone wrong with the color map plot')
% 
% end
% 
% fclose(fvtbl);

% ---- Done.
return

