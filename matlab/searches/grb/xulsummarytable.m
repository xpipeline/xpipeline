function [] = xulsummarytable(parentDir,userTag)
% XULSUMMARYTABLE - Produce upper limit summary table.
% 
% usage: 
% 
%   xulsummarytable(parentDir,userTag)
% 
% parentDir   String. Path of dir containing all the GRBXXXXXX directories.
% userTag     String. The output table file is named 
%             ['ulSummaryTable_' userTag '.html'].
%
% $Id$

% ---- return user to initialDir
initialDir = pwd;

% read contents of parentDir
dirContents = dir([parentDir '/GRB*']);

grbDirs = {dirContents(logical([dirContents.isdir])).name};

disp('Preparing to process: ')
for thisDir = 1:length(grbDirs)
    disp([grbDirs{thisDir}])
end
disp(' ')

ulAmp90Array = []
vetoNullTypeCell   = {}
vetoPlusTypeCell   = {}
vetoCrossTypeCell  = {}
vetoNullArray  = []
vetoPlusArray  = []
vetoCrossArray = []

% ---- start looping through GRBdirs
for thisGrb = 1:length(grbDirs)
    disp(['Processing ' grbDirs{thisGrb}])

    % ---- construct and mv to grbpath
    grbpath = [parentDir  char(grbDirs{thisGrb}) '/auto_web/' ]
    cd(grbpath);

    load('table.mat','UL90p','hrss','nullVetoType','plusVetoType','crossVetoType', ...
      'vetoNullRange','vetoPlusRange','vetoCrossRange','ulVector');

    % ---- currently hardcoded to look at 150Hz
    ulAmp90Array(thisGrb) = UL90p(2) * hrss(2);    
    vetoNullTypeCell{thisGrb}  = nullVetoType;
    vetoPlusTypeCell{thisGrb}  = plusVetoType;
    vetoCrossTypeCell{thisGrb} = crossVetoType;
    vetoNullArray(thisGrb)  = vetoNullRange;   
    vetoPlusArray(thisGrb)  = vetoPlusRange;   
    vetoCrossArray(thisGrb) = vetoCrossRange;   

    clear UL90p hrss nullVetoType plusVetoType crossVetoType
    clear vetoNullRange vetoPlusRange vetoCrossRange ulVector
end

cd(parentDir);

% ---- produce html table
fout = fopen(['ulSummaryTable_' userTag '.html'],'w');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');

fprintf(fout,[...
             '<td><b>GRB</b></td>'...
             '<td><b>90%% conf UL at 150Hz</b></td>'...
             '<td><b>%s</b></td>'...
             '<td><b>%s</b></td>'...
             '<td><b>%s</b></td>'...
             '</tr>\n'],...
              vetoNullTypeCell{thisGrb},... 
              vetoPlusTypeCell{thisGrb},... 
              vetoCrossTypeCell{thisGrb}... 
               )

for thisGrb = 1:length(grbDirs)

    fprintf(fout,[...
             '<td>%s</td>'...
             '<td>%g</td>'...
             '<td>%g</td>'...
             '<td>%g</td>'...
             '<td>%g</td>'...
             '</tr>\n'], ...
              char(grbDirs{thisGrb}), ...
              ulAmp90Array(thisGrb),...
              vetoNullArray(thisGrb),...
              vetoPlusArray(thisGrb),...
              vetoCrossArray(thisGrb)...
                 );
end

fprintf(fout,'%s\n','</table><br>');
fclose(fout);

cd(initialDir);
% ---- Done
return

