function xjitterinjectionskypositions(inFile,outFile,sigmaOmega)
% XJITTERINJECTIONSKYPOSITIONS - Jitter injection sky positions.
%
% XJITTERINJECTIONSKYPOSITIONS modifies an X-Pipeline formatted injection
% log file to simulate the effect of sky position uncertainty.
%
% usage:
%
%  xjitterinjectionskypositions(inFile,outFile,sigmaOmega)
%
%  inFile           String.  Name of input injection file to be modified.
%  outFile          String.  Name of output file with modified injections.
%  sigmaOmega       Optional string.  Sky position uncertainty.  Can be of
%                   any format recognized by XJITTERSKYPOSITION.  Default 
%                   '0' (no error). 
%
% The sky position jittering is done by XJITTERSKYPOSITION.
%
% WARNING: This function should be run *before*
% XMISCALIBRATEGRBINJECTIONFILE since we assume only one set of gps_s,
% gps_ns, phi, theta, etc. per injection, and after miscalibration there  
% will be multiple sets of these parameters, each corresponding to a
% different detector. 
%
% $Id$

% ---- Check input.
error(nargchk(2, 3, nargin))
if nargin < 3
    sigmaOmega = '0';
else
    if ~ischar(sigmaOmega)
        error('Input sigmaOmega must a string.');
    end
end
if ~ischar(inFile) | ~ischar(outFile)
    error('Inputs inFile, outFile must strings.');
end

% ---- Read and parse input injection file.
injection = readinjectionfile(inFile);
[~, gps_s, gps_ns, phi, theta, psi, temp_name, temp_parameters] = ...
    parseinjectionparameters(injection);
nInjection = length(gps_s);

% ---- Jitter sky positions.
[phi, theta] = xjitterskyposition(phi,theta,sigmaOmega,sum(inFile));

%----- Write jittered injection log file.
fid = fopen(outFile,'w');
for ii = 1:nInjection
    %----- Write signal type and parameters to output file.
    fprintf(fid,'%d %d %e %e %e ', gps_s(ii), gps_ns(ii), phi(ii), theta(ii), psi(ii) );
    fprintf(fid,'%s ',temp_name{ii}{1});
    fprintf(fid,'%s ',temp_parameters{ii}{1});
    fprintf(fid,'\n');
end
fclose(fid);

% ---- Done.
return
