#!/usr/bin/env python 

# selects one row from xml injectin table, or split the table into one xml file per injection

from glue.ligolw import ligolw
from glue.ligolw import utils
from glue.ligolw import table
from glue.ligolw import lsctables
import getopt, sys

def write_rows(rows, table_type, filename): 
    """ 
    Create an empty LIGO_LW XML document, add a table of table_type, 
    insert the given rows, then write the document to a file. 
    (stolen from pylal.grbsummary)
    """ 
    # prepare a new XML document 
    xmldoc = ligolw.Document() 
    xmldoc.appendChild(ligolw.LIGO_LW()) 
    tbl = lsctables.New(table_type) 
    xmldoc.childNodes[-1].appendChild(tbl) 

    # insert our rows 
    tbl.append(rows) 

    # write out the document 
    utils.write_filename(xmldoc, filename) 


def fix_numrel_columns(sims):
    """                                                                                                                                                       
    Due to a mismatch between our LAL tag and the LAL HEAD, against which                                                                                     
    we compile pylal, there are missing columns that cannot be missing.                                                                                       
    Just put in nonsense values so that glue.ligolw code won't barf.                                                                                          
    """
    for sim in sims:
        sim.numrel_data="nan"
        sim.numrel_mode_max = 0
        sim.numrel_mode_min = 0

# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
injFile=[]
outputFile=[]
iRow=-1


# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hf:r:o:"
# ---- Long form.
longop = [ 
    "help",
    "file=",
    "row=",
    "output="
    ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-f", "--file"):
        injFile = a
    elif o in ("-r", "--row"):
        iRow = int(a)-1
    elif o in ("-o", "--output"):
        outputFile = a
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not injFile:
    print("No xml file specified.", file=sys.stderr)
    print("Use --file to specify it.", file=sys.stderr)
    sys.exit(1)
if not outputFile:
    print("No output file specified.", file=sys.stderr)
    print("Use --output to specify it.", file=sys.stderr)
    sys.exit(1)


# read in the file and the tables
xmldoc = utils.load_filename(injFile)
sims = table.get_table(xmldoc, lsctables.SimInspiralTable.tableName)
nInjs = len(sims)

# XXX: hack since glue is having trouble with empty columns                                                                                            
#fix_numrel_columns(sims)

if iRow >= 0 & iRow < nInjs:
    write_rows(sims[iRow], lsctables.SimInspiralTable, outputFile)
else :
    # Split injection file, into one injection file per injection
    for iInj in range(nInjs):
        newInjFileName = outputFile + str(iInj+1) 
        write_rows(sims[iInj], lsctables.SimInspiralTable, newInjFileName)

