function xmergeextbkg(outputDir,targetDir,preTunedParamsFile,tunedParamsFile,origResults)
% XMERGEGRBRESULTS - Wrapper to xmergegrbfiles.
%
% xmergegrbresults is a wrapper to xmergegrbfiles to combine the output files
% of multiple X-Pipeline GRB jobs into a single file. This wrapper keeps
% only triggers passing coherent cuts as specified by the files produced
% in a previous automatic pipeline tuning.
%
% usage:
%
% xmergegrbresults(outputDir,targetDir,preTunedParamsFile,tunedParamsFile,origResults)
%
%   outputDir  String specifying path to "output" directory in which  
%              "target" directory lives.
%   targetDir  String specifying name of "target" directory that contains
%              files to be merged.
%   preTunedParamsFile 
%              String specifying the path of the file containing the first
%              stage ("pre") tuning command line parameters for
%              xmakegrbwebpage. Actually an '.digest' is appended to the
%              provided file name and that file is read. The digest
%              should be produced alongside the full command line when
%              the post-processing dag is run.
%   tunedParamsFile
%              String. Same as above but for the second stage of tuning.
%   origResults
%              String. Path to the results of the post-pocessing
%
% $Id$

% ---- Ensure that targetDir has trailing "/" (not needed for outputDir).
if targetDir(end) ~= '/'
    targetDir = [targetDir '/'];
end

% ---- cd to output directory.
cd(outputDir);

% ---- Remove any pre-existing merged file.
[status,result] = system(['rm -f ' targetDir '*merged.mat']);

% ---- Make cell array list of directories matching targetDir string.
[status,result] = system(['ls -d --color=none ' targetDir]);
DIR_LIST = strread(result,'%s');

if length(DIR_LIST) ~= 1
  error(['Expect to merge only one directory (off-source).' ...
         ' Trying to merge ' num2str(length(DIR_LIST)) ' directories']);
end

% ---- For each target directory get list of output files and run merge
%      function on them. 
dirName = DIR_LIST{1};
[status,fileListString] = system(['ls --color=none ' dirName '| grep mat']);

% ---- Maximum number of loudest off-source events to record.
maxOffEvent = 1e4;
maxOffEventTotal = 1e7;

% ---- Parse file list string into cell array of separate file names.
fileList = strread(fileListString,'%s');

% ---- Name of first file.
fileName = fileList{1}

% ---- Create file name for merged results, based on the input file names.
mergedFileName = '';
tmp = strread(fileName,'%s','delimiter','/');  %-- split off path
tmp1 = strread(tmp{end},'%s','delimiter','_');
for i = 1:length(tmp1)-1
    mergedFileName = [ mergedFileName tmp1{i} '_' ];  %-- root name
end
mergedFileName = [ mergedFileName 'extmerged.mat' ]

% ---- Check if these results files are from off-source (background) data.
%      Done by seeing if string 'off' is part of file name.
offSourceMat = 0;
if (~isempty(strfind(mergedFileName,'off')))
    offSourceMat = 1;
else
  error(['Expected to merge an off-source directory. '...
         'Received ' mergedFileName])
end

% ---- Read tuning parameter files
fid = fopen(tunedParamsFile);
tmp = textscan(fid,'%s');
vetoMethod = tmp{1}{4};
tunedPrePassCuts=load([preTunedParamsFile '.digest']);
tunedPassCuts=load([tunedParamsFile '.digest']);
load(origResults,'analysis','plusVetoType','crossVetoType','nullVetoType',...
     'medianA','medianC','maxE')
[fixedRatioArray fixedDeltaArray]=...
    xsetratioarray(analysis,...
                   plusVetoType,crossVetoType,nullVetoType,...
                   tunedPrePassCuts(2),tunedPrePassCuts(3),tunedPrePassCuts(1));
[ratioArray deltaArray]=...
    xsetratioarray(analysis,...
                   plusVetoType,crossVetoType,nullVetoType,...
                   tunedPassCuts(2),tunedPassCuts(3),tunedPassCuts(1));


if offSourceMat 
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %      Processing for off-source results.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if offSourceMat
      event = load('../input/event_off_source.txt');
      window = load('../input/window_off_source.txt');
      jobsPerWindow = size(event,1)/size(window,1);
    end
    if floor(jobsPerWindow) ~= jobsPerWindow
      error(['Something is wrong jobsPerWindow is not an integer: ' num2str(jobsPerWindow)])
    end
    % ---- check if all expected .mat files have been produced
    if size(event,1) ~= length(fileList)
      disp('Missing job numbers are:')
      presentJobNumbers = [];
      for iFile = 1:length(fileList)
        tmp=textscan(fileList{iFile},'%s','delimiter','_');
        presentJobNumbers = [presentJobNumbers ...
                            str2num(tmp{1}{end-1})];

      end
      setdiff(0:(size(event,1)-1),presentJobNumbers)
      error(['Some on/off-source jobs failed silently. The number of files ' ...
             'is ' num2str(length(fileList)) ' it should be ' num2str(size(event,1))])
    end


    % ---- Target variable is 'cluster', a struct that contains an a priori
    %      unknown number of fields. In a given struct, each field contains
    %      a numeric array with the same number of rows.  For each file, we
    %      read this variable, then loop over the fields and append the
    %      contents to a running 'mergedCluster' struct.


    % ---- Loop over all files to be merged.
    for iFile = 1:length(fileList)

      if mod(iFile,1000) == 0
        disp(['Processed ' num2str(iFile) '/' num2str(length(fileList))]);
      end

        % ---- Initialize 'cluster' variable to an empty array.  This
        %      avoids a compiler error that confuses later use of cluster
        %      with a function of the same name.  Declaring cluster
        %      explicitly as a variable before first use tells the compiler
        %      it is a variable.  
        cluster = [];

        % ---- Load next results file.  Note that this over-writes the
        %      cluster variable just declared.
        fileName = [dirName fileList{iFile}];
        load(fileName)

        % ---- First file processed.  Initialize mergedCluster struct to 
        %      cluster from first file, then null out the data.  Then 
        %      mergedCluster will be an empty struct with the desired set
        %      of fields.
        if (1 == iFile)
          % ---- allocate temporary oversized matrix of merged
          % clusters fields
          clusterFields = fieldnames(cluster);
          for ii = 1:length(clusterFields)
            fieldValue = getfield(cluster,clusterFields{ii});
            mergedClusterField{ii} = zeros(maxOffEventTotal,size(fieldValue,2));
          end
          firstEmptyCellIndex=1;
          mergedCluster = cluster;
        end

        % ---- Remove clusters that do not pass fixed coherent cuts but
        % keep at least one cluster per job for bookkeeping purposes
        [passRatio passFixedRatio] = ...
            xapplycoherentcuts(cluster,analysis,vetoMethod,ratioArray,deltaArray,...
                               medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
        [junk uniqueJobIndex] = unique(cluster.jobNumber);
        cluster = xclustersubset(cluster,union(find(passFixedRatio & passRatio),uniqueJobIndex));

        % ---- Sort current batch of clusters.  First, find ordering
        %      of clusters by significance in decending order (i.e.,
        %      loudest first).
        [junk Ic] = sort(cluster.significance(:,1),'descend');
        % ---- Keep at most the maxOffEvent loudest clusters.
        if (length(Ic)>maxOffEvent)
            Ic = Ic(1:maxOffEvent);
        end
        
          
        % ---- Sort data arrays and keep only loudest events.
        clusterFields = fieldnames(cluster);
        for ii = 1:length(clusterFields)
          % ---- Get the data for this field.
          fieldValue = getfield(cluster,clusterFields{ii});
          mergedClusterField{ii}(firstEmptyCellIndex: ...
                                 firstEmptyCellIndex+length(Ic)-1,:) = fieldValue(Ic,:);
        end        
        % ---- update index of first empty cell
        firstEmptyCellIndex = firstEmptyCellIndex+length(Ic);
        if(firstEmptyCellIndex+maxOffEvent>maxOffEventTotal)
          error(['Run out of space to add new off/on source events, changed' ...
                 'change total limit: ' num2str(maxOffEventTotal) ...
                 ' or throttle the output trigger rate.'])
        end
        

    end  %-- loop over files

    % ---- flush out empty cells and add to merged cluster
    for ii = 1:length(clusterFields)
      mergedClusterField{ii} = ...
          mergedClusterField{ii}(1:firstEmptyCellIndex-1,:);
      mergedCluster = setfield(mergedCluster,clusterFields{ii}, ...
                                             mergedClusterField{ii});
    end        

    mergedCluster.jobNumber = floor(mergedCluster.jobNumber/ ...
                                    jobsPerWindow);
    mergedCluster.centerTime = window(1+mergedCluster.jobNumber,1);

    % ---- Copy final lists from temporary storage into desired variable names.
    cluster = mergedCluster;

    if(offSourceMat)
      % ---- Save merged file for background results.
      save(mergedFileName,'analysisTimesCell','blockTime','cluster', ...
           'detectorList','injectionScale','likelihoodType', ...
           'maximumFrequency','minimumFrequency','sampleFrequency', ...
           'skyPositions','transientTime');
    end

end

% ---- It's a rude function that doesn't return to the starting directory.
cd('..');

% ---- Done.
return

