function []= xwritetriggerhists(fout,analysis,triggers,trigNames,...
   figures_dirName,figfiles_dirName,plotTag,plotLoudest)
% XWRITETRIGGERHISTS - write histograms of trigger properties to a webpage.
% 
% xwritetriggerhists writes histograms of various trigger properties
% to a webpage.  It is a helper function for xmakegrbwebpage.
%
% Usage: 
%
%   xwritetriggerhists(fout,analysis,triggers,trigNames,...
%       figures_dirName,figfiles_dirName,plotTag,plotLoudest)
%
%   fout             Pointer to open and writable webpage file  
%   analysis         Structure created by xmakegrbwebpage
%   triggers         Cell of structures containing triggers, e.g.,
%                    {offSourceSelected,onSource,loudestInjEvents};
%   trigNames        Cell array of strings containing names of structures
%                    contained in triggers variable. e.g., 
%                      {'off source','on source','loudest inj'}
%   figures_dirName  String, path of output dir for figures where figures will
%                    be saved
%   figfiles_dirName String, path of output dir for figures where figfiles will
%                    be saved
%   plotTag          String used to name output plots
%   plotLoudest      Integer specifying one of the structures in triggers whose
%                    loudest trigger will be added to the plots.
%
  
%   $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(8, 8, nargin));

fprintf(fout,'%s\n',['<p>']);
fprintf(fout,'%s\n',[' <input id="trigHists_' plotTag  '" type="checkbox" unchecked ']);
fprintf(fout,'%s\n',[' onclick="toggleVisible(''trigHists_ ' plotTag ' '');" /> ']);
fprintf(fout,'%s\n',[' Histograms of trigger properties']);
fprintf(fout,'%s\n',[' <div id="div_trigHists_ ' plotTag ' " style="display: none;"> ']);
fprintf(fout,'%s\n',['</p>']);

% ---- Find properties of the loudest trigger we will plot.
[maxSig maxIdx] = max((triggers{plotLoudest}.significance)...
                    .*(triggers{plotLoudest}.pass));

loudestLegend = ['loudest trigger in ' trigNames{plotLoudest}];
sigLoudest       = maxSig; 
timeLoudest      = triggers{plotLoudest}.boundingBox(maxIdx,1) ...
                   - triggers{plotLoudest}.centerTime(maxIdx,1); 
durationLoudest  = triggers{plotLoudest}.boundingBox(maxIdx,3);
freqLoudest      = triggers{plotLoudest}.peakFrequency(maxIdx);
bandwidthLoudest = triggers{plotLoudest}.boundingBox(maxIdx,4);
npixelLoudest    = triggers{plotLoudest}.nPixels(maxIdx);
atLoudest        = triggers{plotLoudest}.analysisTime(maxIdx);
likelihoodLoudest= triggers{plotLoudest}.likelihood(maxIdx,:);

% ---- Make hist of trigger time.
disp('Making hist of trigger time');
trigProperty = {};
for idx = 1:length(triggers)
    trigProperty{idx} = triggers{idx}.boundingBox(:,1)-triggers{idx}.centerTime; 
end
trigPropertyName = 'trigger time - external triggers time';
[timehist]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,timeLoudest,loudestLegend);

% ---- Make hist of trigger duration.
disp('Making hist of duration');
trigProperty = {};
minDur = Inf;
maxDur = -Inf;
for idx = 1:length(triggers)
    trigProperty{idx} = log2(triggers{idx}.boundingBox(:,3));
    minDur = min([minDur min(trigProperty{idx})]);
    maxDur = max([maxDur max(trigProperty{idx})]);
end
trigPropertyName = 'log2(trigger duration)';
[durationhist]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,log2(durationLoudest),loudestLegend,...
    linspace(floor(minDur),ceil(maxDur),1+2*(ceil(maxDur)-floor(minDur))));

% ---- Make hist of trigger central freq.
disp('Making hist of central freq');
trigProperty = {};
for idx = 1:length(triggers)
    trigProperty{idx} = log10(triggers{idx}.peakFrequency(:)); 
end
trigPropertyName = 'log10(central freq)';
[freqhist]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,log10(freqLoudest),loudestLegend);

% ---- Make hist of trigger bandwidth.
disp('Making hist of central bandwidth');
trigProperty = {};
minBand = Inf;
maxBand = -Inf;
for idx = 1:length(triggers)
    trigProperty{idx} = log2(triggers{idx}.boundingBox(:,4));
    minBand = min([minBand min(trigProperty{idx})]);
    maxBand = max([maxBand max(trigProperty{idx})]);
end
trigPropertyName = 'log2(trigger bandwidth)';
[bandwidthhist]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,log2(bandwidthLoudest),loudestLegend,...
    linspace(floor(minBand),ceil(maxBand),1+2*(ceil(maxBand)-floor(minBand))));

% ---- Make hist of num pixels.
disp('Making hist of number of pixels');
trigProperty = {};
maxPix = -Inf;
for idx = 1:length(triggers)
    trigProperty{idx} = triggers{idx}.nPixels(:);
    maxPix = max([maxPix max(trigProperty{idx})]);
end
trigPropertyName = 'number of pixels';
[npixelhist]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,npixelLoudest,loudestLegend,...
    linspace(0,10*ceil(maxPix/10),1+(10*ceil(maxPix/10))/5));

% ---- Make hist of analysisTime.
disp('Making hist of analysis time');
minAT = Inf;
maxAT = -Inf;
trigProperty = {};
for idx = 1:length(triggers)
    trigProperty{idx} = log2(triggers{idx}.analysisTime(:));
    minAT = min([minAT min(trigProperty{idx})]);
    maxAT = max([maxAT max(trigProperty{idx})]);
end
trigPropertyName = 'log2(analysis Time)';
[athist]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,log2(atLoudest),loudestLegend,...
    linspace(floor(minAT),ceil(maxAT)+1,1+2*(1+ceil(maxAT)-floor(minAT))));

% ---- Make hist of trigger significance.
disp('Making hist of significance');
trigProperty = {};
for idx = 1:length(triggers)
    trigProperty{idx} = log10(triggers{idx}.significance);
end
trigPropertyName = 'log10(significance)';
[significancehist]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,log10(sigLoudest),loudestLegend);

% ---- Make hist of each likelihood.
for iLike = 1:length(analysis.likelihoodType)
  takeLogFlag = 1;
  if strcmp(analysis.likelihoodType{iLike},'skypositiontheta') || strcmp(analysis.likelihoodType{iLike},'skypositionphi') 
    takeLogFlag = 0;
  end
    disp(['Making hist of ' analysis.likelihoodType{iLike} ]);
    trigProperty = {};
    for idx = 1:length(triggers)
      if takeLogFlag
        trigProperty{idx} = log10(triggers{idx}.likelihood(:,iLike));
      else
        trigProperty{idx} = triggers{idx}.likelihood(:,iLike);
      end
    end
    if takeLogFlag
      trigPropertyName = ['log10(' analysis.likelihoodType{iLike}  ')'];
      loudest = log10(triggers{plotLoudest}.likelihood(maxIdx,iLike));
    else
      trigPropertyName = analysis.likelihoodType{iLike} ;
      loudest = triggers{plotLoudest}.likelihood(maxIdx,iLike);
    end
    [likelihoodHist{iLike}]= xtriggerhist(fout,trigProperty,trigPropertyName,trigNames,...
        figures_dirName,figfiles_dirName,plotTag,loudest,loudestLegend);
end

% ---- Prep for scatter plots.
% ---- We will limit ourselves to plotting no more than 100000 points,
%      if we plot too many matlab crashes. We choose the 100000 points
%      with the largest significance.
maxPoints = 100000;
for idx = 1:length(triggers)
    [sigSortedTemp, sortIdx] = sort(triggers{idx}.significance,'descend');
    if length(sigSortedTemp) > maxPoints
        warning(['We are using only the ' num2str(maxPoints) ...
            ' most significant triggers when making scatter plots.']);
    end
    numPoints = min([length(sigSortedTemp) maxPoints]);   
    sigSorted{idx}        = sigSortedTemp(1:numPoints); 
    centerTimeSorted{idx} = triggers{idx}.centerTime(sortIdx(1:numPoints),1);
    timeSorted{idx}       = triggers{idx}.boundingBox(sortIdx(1:numPoints),1);
    freqSorted{idx}       = triggers{idx}.peakFrequency(sortIdx(1:numPoints));
end

% ---- Make scatter plot of significance vs time
disp(['Making scatter plot of significance vs time ']);
trigProperty1 = {};
trigProperty2 = {};
trigPropertyName1 = 'trigger time';
trigPropertyName2 = 'significance';
loudest1 = timeLoudest; 
loudest2 = sigLoudest; 
for idx = 1:length(triggers)
    trigProperty1{idx} = timeSorted{idx} - centerTimeSorted{idx};
    trigProperty2{idx} = sigSorted{idx};
end
[sigTimeScatter]= xtriggersimplescatter(fout,trigProperty1,...
    trigPropertyName1,trigProperty2,trigPropertyName2,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,...
    loudest1,loudest2,loudestLegend,'semilogy');

% ---- Make scatter plot of significance vs frequency
disp(['Making scatter plot of significance vs frequency ']);
trigProperty1 = {};
trigProperty2 = {};
trigPropertyName1 = 'central frequency';
trigPropertyName2 = 'significance';
loudest1 = freqLoudest; 
loudest2 = sigLoudest; 
for idx = 1:length(triggers)
    trigProperty1{idx} = freqSorted{idx};
    trigProperty2{idx} = sigSorted{idx};
end
[sigFreqScatter]= xtriggersimplescatter(fout,trigProperty1,...
    trigPropertyName1,trigProperty2,trigPropertyName2,trigNames,...
    figures_dirName,figfiles_dirName,plotTag,...
    loudest1,loudest2,loudestLegend,'loglog');

figureList = [{timehist, durationhist, freqhist, bandwidthhist, ...
    npixelhist, athist, sigTimeScatter, sigFreqScatter} ...
    significancehist likelihoodHist];

% ---- Make scatter plot of skypositiontheta vs skypositionphi if
%      available
iSkyPosTheta = find(strcmp(lower(analysis.likelihoodType), ...
                           'skypositiontheta'));
iSkyPosPhi = find(strcmp(lower(analysis.likelihoodType), ...
                           'skypositionphi'));
if not(isempty(iSkyPosPhi)) && not(isempty(iSkyPosTheta))
  disp(['Making scatter plot of skypositiontheta vs skypositionphi'])
  % ---- We will limit ourselves to plotting no more than 100000 points,
  %      if we plot too many matlab crashes. We choose the 100000 points
  %      with the largest significance.
  maxPoints = 100000;
  for idx = 1:length(triggers)
      [sigSortedTemp, sortIdx] = sort(triggers{idx}.significance,'descend');
      if length(sigSortedTemp) > maxPoints
          warning(['We are using only the ' num2str(maxPoints) ...
              ' most significant triggers when making scatter plots.']);
      end
      numPoints = min([length(sigSortedTemp) maxPoints]);   
      likelihoodSorted{idx} = triggers{idx}.likelihood(sortIdx(1:numPoints),:);
  end  

  trigProperty1 = {};
  trigProperty2 = {};
  trigPropertyName1 = 'relative phi';
  trigPropertyName2 = 'relative theta';
  loudest1 = mod(likelihoodLoudest(:,iSkyPosPhi)...
                 -analysis.skyPositions(1,2)+pi,2*pi)-pi; 
  loudest2 = mod(likelihoodLoudest(:,iSkyPosTheta)...
                 -analysis.skyPositions(1,1)+pi/2,pi)-pi/2;
  % -- first plot searched sky positions
  trigNames= {'sky grid',trigNames{:}};
  trigProperty1{1} = mod(...
      analysis.skyPositions(:,2)...
      -analysis.skyPositions(1,2)+pi,2*pi)-pi;
  trigProperty2{1} = mod(...
      analysis.skyPositions(:,1)...
      -analysis.skyPositions(1,1)+pi/2,pi)-pi/2;
  % -- copy sky positions from triggers and add a small random jitter to
  %    see the density of triggers at given sky position
  for idx = 1:length(likelihoodSorted)
    trigProperty1{idx+1} = mod(...
        likelihoodSorted{idx}(:,iSkyPosPhi)...
        -analysis.skyPositions(1,2)+pi,2*pi)-pi ...
        +5e-3*randn(size(likelihoodSorted{idx},1),1);
    trigProperty2{idx+1} = mod(...
        likelihoodSorted{idx}(:,iSkyPosTheta)...
        -analysis.skyPositions(1,1)+pi/2,pi)-pi/2 ...
        +5e-3*randn(size(likelihoodSorted{idx},1),1);
  end
  [phiThetaScatter]= xtriggersimplescatter(fout,trigProperty1,...
                                           trigPropertyName1,trigProperty2,trigPropertyName2,trigNames,...
                                           figures_dirName,figfiles_dirName,plotTag,...
                                           loudest1,loudest2, ...
                                           loudestLegend,'linear');  
  figureList = [figureList phiThetaScatter];
end

% ---- Add figures to table
xaddfiguretable(fout,figureList,figures_dirName);


fprintf(fout,'%s\n','</div>'); %-- end of div_trigHists
% -- Done
