#!/usr/bin/env python

"""
XCHECKGRBOUTPUT - check existence of output files of xdetection as run using grb.py.
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt
import configparser


# ---- Function usage.
def usage():
    msg = """\
Usage: 
    xcheckgrboutput.py [options]
    -d, --grb-dir <path>    [REQUIRED] Path to directory to be checked (absolute
                            or relative).
    -m, --merge             Check merge job log files for errors.
    -v, --verbose           Extra verbosity.
    -h, --help              Display this message and exit.

  e.g.,
    xcheckgrboutput.py -d /home/gareth.jones/S5/GRB070508 -v
"""
    print(msg, file=sys.stderr)


# ---- Output file checking function. 
def xcheckoutputfiles(grb_dir,verbose=True,check_merge=False):

    if verbose:
        # ---- Status message.  Report all supplied arguments.
        print(file=sys.stdout)
        print("####################################################", file=sys.stdout)
        print("#    Checking output of X-Pipeline GRB search      #", file=sys.stdout)
        print("####################################################", file=sys.stdout)
        print(file=sys.stdout)
        print("Parsed input arguments:", file=sys.stdout)
        print(file=sys.stdout)
        print("             GRB dir:", grb_dir, file=sys.stdout)
        print("        verbose flag:", verbose, file=sys.stdout)
        print("         check merge:", check_merge, file=sys.stdout)
        print(file=sys.stdout)

    # ---- Assume all files are present.
    filesMissing  = 0

    # ---- Ensure directory names end in '/'.
    if not(grb_dir.endswith('/')):
        grb_dir = grb_dir + '/'

    # ---- Verify <grb_dir>/input/ directory exists.
    if not os.path.isdir(grb_dir + 'input/'): 
        print("Not a valid directory: ", grb_dir + 'input/', file=sys.stdout)
        sys.exit()

    # ---- Check that on-source output files are present.
    if not os.path.isfile(grb_dir + 'output/on_source_0_0_merged.mat'): 
        filesMissing = filesMissing + 1
        print("Missing file: ", grb_dir + 'output/on_source_0_0_merged.mat', file=sys.stdout)

    # ---- Check that ul-source output files are present.
    if not os.path.isfile(grb_dir + 'output/ul_source_0_0_merged.mat'): 
        filesMissing = filesMissing + 1
        print("Missing file: ", grb_dir + 'output/ul_source_0_0_merged.mat', file=sys.stdout)

    # ---- Check that off-source output files are present.
    if not os.path.isfile(grb_dir + 'output/off_source_0_0_merged.mat'):
        filesMissing = filesMissing + 1
        print("Missing file: ", grb_dir + 'output/off_source_0_0_merged.mat', file=sys.stdout)

    # ---- Check that all simulation output files are present. 
    #      Determine the expected simulation_<set>_<scale>_0_0_merged.mat files 
    #      by using the fact that there should be one such file for each 
    #      directory simulations_<set>_<scale>, e.g. simulations_sgc150q9_11/ 
    #      and simulation_sgc150q9_11_0_0_merged.mat.
    dir_contents = os.listdir(grb_dir + 'output')
    for sim_dir in dir_contents:
        if sim_dir.startswith('simulations_') and not sim_dir.endswith('_'):
            inj_set   = sim_dir.split('_')[1]
            inj_scale = sim_dir.split('_')[2]
            sim_file  = grb_dir + 'output/' + 'simulation_' + inj_set + '_' + inj_scale + '_0_0_merged.mat'
            if not os.path.isfile(sim_file):
                filesMissing = filesMissing + 1
                print("Missing file: ", sim_file, file=sys.stdout)

    if filesMissing:
        print(grb_dir, " :Number of output files missing: ", filesMissing, file=sys.stdout)
    else:
        print(grb_dir, " :No output files missing", file=sys.stdout)

    if check_merge: 
        print("Checking log dir for errors with xmerge jobs ...", file=sys.stdout)
        errCommand = 'ls --time-style=long-iso -al ' + grb_dir +  '/logs/xmerge*.err | awk \'{if ($5 > 0) print $5 " " $8}\' '
        os.system(errCommand)

    if verbose:
        print(file=sys.stdout)
        print(" ... finished.", file=sys.stdout)

    print(" ", file=sys.stdout) 


if __name__ == '__main__':
    # ---- This function is being executed directly as a script (as opposed to 
    #      being imported by another code). Parse the command line arguments 
    #      then run the xcheckoutputfiles() function.

    # -------------------------------------------------------------------------
    #      Parse the command line options.
    # -------------------------------------------------------------------------

    # ---- Assign defaults to command-line arguments.
    check_merge = False
    verbose     = False

    # ---- Syntax of options, as required by the getopt command.
    # ---- Short form.
    shortop = "hvmd:"
    # ---- Long form.
    longop = [
        "help",
        "merge",
        "verbose",
        "grb-dir=",
        ]

    # ---- Get command-line arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    # ---- Parse command-line arguments.  Arguments are returned as strings, so 
    #      convert type as necessary.
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-d", "--grb-dir"):
            grb_dir = a      
        elif o in ("-m", "--merge"):
            check_merge = True   
        elif o in ("-v", "--verbose"):
            verbose = True   
        else:
            print("Unknown option:", o, file=sys.stderr)
            usage()
            sys.exit(1)

    # ---- Check that all required arguments are specified, else exit.
    if not grb_dir:
        print("No grb dir specified.", file=sys.stderr)
        print("Use --grb-dir to specify it.", file=sys.stderr)
        sys.exit(1)

    # ---- Call the xcheckoutputfiles() function.
    xcheckoutputfiles(grb_dir,verbose,check_merge)


