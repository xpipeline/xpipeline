#!/usr/bin/env python

"""
Script that generates sets up event display generation for a single GRB.

This program will generate a directory called event/ in the auto_web directory passed in the options.  A dag file will be generated
in this directory which will:
 - collect event information from the post-processing results
 - rerun xdetection for the on-source jobs to save the TF maps
 - generate plots of the TF maps for various time resolutions
 - plot the recovered sky location of the events
 - build a webpage with the results

The event display generation will use the following scripts:

Scripts called from the command line:

xgrbeventdisplay.py   - (this script) Collects parameters, generates directory structure, writes condor DAG
xprepeventdisplay.m   - Subroutine of xgrbeventdisplay.py. Reads post-processing results, gathers event times and parameters


Scripts in the condor DAG:

xdetection.m           - Rerun GRB search algorithm to generate time-frequency maps for onsource / dummy-onsource jobs
xplotgrbevent.m        - Collects TF maps, makes plots of likelihoods around event times
xeventwebpage.py       - Builds event display web page
plotEventLocation.py   - plots search grid  with marker for reconstructed sky location. Subroutine of xeventwebpage.py. Boring for Swift-type analyses.

"""

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt, string, re
from numpy import *
import random
from optparse import OptionParser
import configparser
from glue import pipeline


# ---- Function usage.
usage = """usage: %prog [options]
Usage: 
  xeventdisplay.py [options]

  -a, --auto_dir <path>       Path to auto_web directory [REQUIRED]

  -t, --results_type <string> Type of analysis (closedbox, openbox, ulbox) [REQUIRED]

  -h, --help                  Display this message and exit.

Example: xgrbeventdisplay.py -a /home/dhoak/grbs/S6/test/Apr15/GW100916_22/auto_web/ -t openbox

"""

parser = OptionParser(usage=usage)

parser.add_option("-a", "--auto_dir", action="store", type="str",\
                      help="absolute path to post-processing directory")

parser.add_option("-t", "--results_type", action="store", type="str", \
                      help="type of reults, closedbox or openbox")

(options, args) = parser.parse_args()


auto_dir = options.auto_dir
results_type = options.results_type


# ---- Check dir names end in '/'
if not(auto_dir.endswith('/')):
    auto_dir = auto_dir + '/'



#------------------------------------------------------------------------------
#                             create dir structure 
#------------------------------------------------------------------------------

# results files will be placed in the auto_web directory, and then copied to the web directory
# note that we assume write permission to the auto_web directory

# ---- Make event directory for this GRB, this is where we will put the
#      results webpage etc 
event_dir_full =  auto_dir + 'event/'
try:
    os.makedirs(event_dir_full)
except OSError:
    print('WARNING: event dir already exists, continuing...') 



#------------------------------------------------------------------------------
#                             read xgrbwebpage.param
#------------------------------------------------------------------------------

# open the post-processing parameters file to get the analysis directory, web directory, grb name, and user tag
webparams_handle = open(auto_dir + 'xgrbwebpage.param')
webparams = webparams_handle.readlines()
webparams_handle.close()
webparams_list = webparams[0].split(' ')

grb_name_idx = webparams_list.index('-g')
grb_name = webparams_list[grb_name_idx + 1]

grb_dir_idx = webparams_list.index('-d')
grb_dir = webparams_list[grb_dir_idx + 1]

log_dir_idx = webparams_list.index('-l')
log_dir = webparams_list[log_dir_idx + 1]

user_tag_idx = webparams_list.index('-u')
user_tag = webparams_list[user_tag_idx + 1]

web_dir_idx = webparams_list.index('-w')
web_dir = webparams_list[web_dir_idx + 1] + '/' + grb_name + '_' + user_tag + '/'

if '--priority' in webparams_list:
    condor_priority_idx = webparams_list.index('--priority')
    condorPriority = webparams_list[condor_priority_idx + 1]
else:
    condorPriority = '0'

if '--user' in webparams_list:
    user_idx = webparams_list.index('--user')
    UserName = webparams_list[user_idx + 1]
else:
    # ---- Guess the condor username.
    UserName = os.popen('whoami').read().rstrip()

# ---- Check dir names end in '/'
if not(grb_dir.endswith('/')):
    grb_dir = grb_dir + '/'

# ---- Check dir names end in '/'
if not(web_dir.endswith('/')):
    web_dir = web_dir + '/'


# copy xgrbwebpage.param to the event directory
cmd = 'cp ' + auto_dir + 'xgrbwebpage.param ' + event_dir_full
subprocess.getoutput(cmd)


# make a web directory to store the events results
events_web_dir = web_dir + 'events/'
try:
    os.makedirs(events_web_dir)
except OSError:
    print('WARNING: events web dir already exists, continuing...') 


# make a log directory to store the condor logs
event_log_dir = event_dir_full + 'logs'
try:
    os.makedirs(event_log_dir)
except OSError:
    print('WARNING: log dir already exists, continuing...') 



#------------------------------------------------------------------------------
#                  read the ini file
#------------------------------------------------------------------------------

# we need to get the condor accounting tags from the ini file

# read the ini file that is dynamically linked from the auto_web directory
# there should be only one...

ini_file = []
for filename in os.listdir(auto_dir):
    if filename.endswith(".ini"):
        ini_file = auto_dir + filename

if not(ini_file):
    print('No ini file found!')
    sys.exit()

cp = configparser.ConfigParser()
cp.read(ini_file)

blockTime = int(cp.get('parameters','blockTime'))
whiteningTime = int(cp.get('parameters','whiteningTime'))
onSourceBeginOffset = int(cp.get('parameters','onSourceBeginOffset'))
onSourceEndOffset = int(cp.get('parameters','onSourceEndOffset'))
maxOffNum = int(cp.get('output','maxOffNum'))

transientTime     =  4 * whiteningTime
onSourceTimeLength = onSourceEndOffset - onSourceBeginOffset


# ---- Get the condor accounting tags
grouptag = 'ligo.' + \
    cp.get('condor','ProdDevSim') + '.' +\
    cp.get('condor','Era') + '.' +\
    cp.get('condor','Group') + '.' +\
    cp.get('condor','SearchType')

# get the number of sky positions
cmd = 'wc -l ' + grb_dir + '/input/sky_positions_trigger_time.txt'
line = subprocess.getoutput(cmd).rsplit('\n')[0]
numSky, skyfile = line.split(' ')


# -------------------------------------------------------------------------
#      Run xprepeventdisplay.m to gather job numbers, event times from 
#      post-processing files
# -------------------------------------------------------------------------

# Looks like this needs to write to a txt file, not sure how to collect output from compiled matlab code
# hard-code it to write the txt file to event_dir_full
cmd = 'xprepeventdisplay ' + auto_dir + ' ' + grb_name + ' ' + results_type + ' ' + user_tag
try:
    #msg = commands.getoutput(cmd)
    os.system(cmd)
except OSError:
    print('Error with xprepeventdisplay!')
    print(cmd)
    sys.exit()

# -------------------------------------------------------------------------
#      Read results file from xprepeventdisplay.m
# -------------------------------------------------------------------------

eventparams_handle = open(event_dir_full + grb_name + '_' + results_type + '_' + user_tag + '_eventparams.txt')
eventparams = eventparams_handle.readlines()
eventparams_handle.close()

job_string = eventparams[0].rsplit('\n')[0]
numEvents = eventparams[1].rsplit('\n')[0]
numPass = eventparams[2].rsplit('\n')[0]

# parse the string of job numbers, the last element is always a space, convert to integers
jobNumbers = job_string.split(' ')[0:-1]
jobNumbers = [ int(x) for x in jobNumbers ]


# make the directories that will hold the html results

for i in range(int(numEvents)):
    event_dir = event_dir_full + grb_name + '_' + results_type + '_' + user_tag + '_event_' + str(i+1)
    try:
        os.makedirs(event_dir)
    except OSError:
        pass

    event_web_dir = events_web_dir + grb_name + '_' + results_type + '_' + user_tag + '_event_' + str(i+1)
    try:
        os.makedirs(event_web_dir)
    except OSError:
        pass


for i in range(int(numPass)):
    event_dir = event_dir_full + grb_name + '_' + results_type + '_' + user_tag + '_event_pass' + str(i+1)
    try:
        os.makedirs(event_dir)
    except OSError:
        pass

    event_web_dir = events_web_dir + grb_name + '_' + results_type + '_' + user_tag + '_event_pass' + str(i+1)
    try:
        os.makedirs(event_web_dir)
    except OSError:
        pass




# -------------------------------------------------------------------------
#      Parse the dag file from the analysis directory and get the parameters
#      files to change.  Also copy xsearch.sub to the new directory.
# -------------------------------------------------------------------------

if results_type=='closedbox':
    xsearch_dag_file = 'grb_off_source.dag'
elif results_type=='openbox':
    xsearch_dag_file = 'grb_on_source.dag'
else:
    print('Results type not recognized!')
    sys.exit()

# Open the dag file and load the jobs
f = open(grb_dir + '/' + xsearch_dag_file)
jobs = f.readlines()
f.close()

parameters_files = []
xdetection_jobs = []

for i in range(len(jobs)):
    condorline = jobs[i].rsplit('\n')[0]
    condor_params = condorline.split(' ')
    if condor_params[0]=='JOB' and condor_params[-1]=='xsearch.sub':
        arguments = jobs[i+2].rsplit('\n')[0]
        arg_list = arguments.split(' ')

        if arg_list[3][0:3] == 'mac':
            # we're reading a DAG file that was generated by a recent version of glue
            # macroarguments are labeled differently

            macro_start = arg_list[2]
            parameters_file = macro_start.split('/')[-1][:-1]

            job_list = arg_list[3]
            [mactxt, job_list, tail] = job_list.split('"')
            if '-' in job_list:
                job_start, job_stop = job_list.split('-')
                job_range = list(range(int(job_start),int(job_stop)+1))
            else:
                job_range = [int(job_list)]

            matches = set(job_range) & set(jobNumbers)
            if matches:
                # we have some jobs that use this parameters file
                for match in matches:
                    xdetection_jobs.append(match)
                    parameters_files.append(parameters_file)

        else:
            # the macroarguments are all glommed together
            # need to parse the line and get the parameters file name

            macro_start = arg_list[2]
            parameters_file = macro_start.split('/')[-1]

            job_list = arg_list[3]
            if '-' in job_list:
                job_start, job_stop = job_list.split('-')
                job_range = list(range(int(job_start),int(job_stop)+1))
            else:
                job_range = [int(job_list)]

            matches = set(job_range) & set(jobNumbers)
            if matches:
                # we have some jobs that use this parameters file
                for match in matches:
                    xdetection_jobs.append(match)
                    parameters_files.append(parameters_file)


# -------------------------------------------------------------------------
#      Modify the parameters file to be read by xdetection
# -------------------------------------------------------------------------

event_pfilenames = []
for i in range(len(parameters_files)):

    event_pfilenames.append(parameters_files[i].replace('parameters','event'))

    # Open the parameters file and read the lines.
    # This is a little inefficient - we might wind up writing the same file multiple times.
    # But it's just string operations so it doesn't need to be efficient
    f = open(grb_dir + '/input/' + parameters_files[i])
    lines = f.readlines()
    f.close()

    eventfile = open(event_dir_full + event_pfilenames[i], 'w')

    for line in lines:
        param = line.rsplit('\n')[0]
        first, last = param.split(':')

        # some of the parameters are filenames
        # for these parameters we need to provide the absolute path to the grb analysis directory
        if first=='channelFileName':
            last = grb_dir + last

        if first=='frameCacheFile':
            last = grb_dir + last

        if first=='eventFileName':
            last = grb_dir + last

        if first=='skyPositionList':
            last = grb_dir + last

        # change the output to TF maps
        if first=='outputtype':
            last = 'timefrequencymap'

        eventfile.write(first + ':' + last + '\n')

    eventfile.close()


# -------------------------------------------------------------------------
#      Write the mid script and end script
# -------------------------------------------------------------------------

midscript_name = grb_name + '_' + results_type + '_' + user_tag + '_mid.sh'
mid_script = open(event_dir_full + midscript_name, 'w')
mid_script.write('#!/bin/bash\n\n')

### The xdetection jobs should have left files of the form 'results_{jobNumber}.mat' in the event directory.
### Rename these files into something unique to this analysis.
for jobNumber in jobNumbers:
    old_results_name = event_dir_full + 'results_' + str(jobNumber) + '.mat'
    new_results_name = event_dir_full + grb_name + '_TFmap_' + results_type + '_' + str(jobNumber) + '.mat'
    cmd = 'cp ' + old_results_name + ' ' + new_results_name
    mid_script.write(cmd + '\n')
mid_script.close()

cmd = 'chmod +x '+ event_dir_full + midscript_name
subprocess.getoutput(cmd)



# write the post script that copies results to the web directory
post_script_name = grb_name + '_' + results_type + '_' + user_tag + '_post.sh'
post_script = open(event_dir_full + post_script_name, 'w')
post_script.write('#!/bin/bash\n\n')

for i in range(int(numEvents)):
    post_script.write('cp -r ' + grb_name + '_' + results_type + '_' + user_tag + '_event_' + str(i+1) + '/* ' + \
                          events_web_dir + grb_name + '_' + results_type + '_' + user_tag + '_event_' + str(i+1) + '\n')
for i in range(int(numEvents)):
    post_script.write('cp -r ' + grb_name + '_' + results_type + '_' + user_tag + '_event_pass' + str(i+1) + '/* ' + \
                          events_web_dir + grb_name + '_' + results_type + '_' + user_tag + '_event_pass' + str(i+1) + '\n')
post_script.close()

cmd = 'chmod +x '+ event_dir_full + post_script_name
subprocess.getoutput(cmd)





# -------------------------------------------------------------------------
#      Define condor job classes
# -------------------------------------------------------------------------

# ---- Fix the minimum memory required for condor jobs. We should allow this
#      to be specified like in the --big-mem option to grb.py.
minimalMem = '4G'

class XsearchJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x search job

    We create these xdetection jobs as placeholders. These jobs are written to the DAG file, but we copy
    xsearch.sub from the analysis directory.
    This ensures that the event display plots are generated using the same code version that was used in the 
    trigger generation.

    """
    def __init__(self,cp):

        # ---- Name of condor job submission file
        self.__universe = 'vanilla'
        self.__executable = 'xdetection'

        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)

        self.__param_file = None

        self.add_condor_cmd('getenv',"true")

        # ---- Add condor accounting group & username.
        self.add_condor_cmd('accounting_group',grouptag)
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory needed.
        self.add_condor_cmd('request_memory',minimalMem)

        # ---- Add mandatory request_disk request.
        minimalDiskSearch = "200M"
        self.add_condor_cmd('request_disk',minimalDiskSearch)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xsearch-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xsearch-$(cluster)-$(process).err')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xsearch.sub')


class XsearchNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    xsearch node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__x_jobnum = None
        self.__x_injnum = None

    # ---- Set parameters file.
    def set_param_file(self,path):
        self.add_var_arg(path)
        self.__param_file = path

    def get_param_file(self):
        return self.__param_file

    def set_x_jobnum(self,n):
        self.add_var_arg(str(n))
        self.__x_jobnum = n

    def get_x_jobnum(self):
        return self.__x_jobnum

    def set_output_dir(self,path):
        self.add_var_arg(path)
        self.__output_dir = path

    def get_output_dir(self,path):
        return self.__output_dir

    def set_x_injnum(self,n):
        self.add_var_arg(str(n))
        self.__x_injnum = n

    def get_x_injnum(self):
        return self.__x_injnum


class XmidscriptJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    Job to run script that copies results files
    """
    def __init__(self,cp):

        # ---- Specify executable.
        self.__executable = midscript_name

        # ---- Get condor universe from parameters file.
        self.__universe = 'vanilla'
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        self.add_condor_cmd('getenv',"true")

        # ---- Add condor accounting group & username.
        self.add_condor_cmd('accounting_group',grouptag)
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory needed.
        self.add_condor_cmd('request_memory',minimalMem)

        # ---- Add mandatory request_disk request.
        minimalDiskMid = "200M"
        self.add_condor_cmd('request_disk',minimalDiskMid)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/midscript-$(cluster)-$(process).out')
        self.set_stderr_file('logs/midscript-$(cluster)-$(process).err')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file(event_dir_full + 'midscript_' + results_type + '.sub')


class XmidscriptNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    midscript node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)


class XpostscriptJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    Job to run script that copies results files
    """
    def __init__(self,cp):

        # ---- Specify executable.
        self.__executable = post_script_name

        # ---- Get condor universe from parameters file.
        self.__universe = 'vanilla'
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        self.add_condor_cmd('getenv',"true")

        # ---- Add condor accounting group & username.
        self.add_condor_cmd('accounting_group',grouptag)
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add minimal memory needed.
        self.add_condor_cmd('request_memory',minimalMem)

        # ---- Add mandatory request_disk request.
        minimalDiskPost = "30G"
        self.add_condor_cmd('request_disk',minimalDiskPost)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/postscript-$(cluster)-$(process).out')
        self.set_stderr_file('logs/postscript-$(cluster)-$(process).err')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file(event_dir_full + 'postscript_' + results_type + '.sub')


class XpostscriptNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    postscript node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)


class XeventJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An xeventwebpage job
    """
    def __init__(self,cp):

        # ---- Get path to executable.
        self.__executable = subprocess.getoutput('which xeventwebpage.py')

        # ---- Get condor universe from parameters file.
        self.__universe = 'vanilla'
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        self.add_condor_cmd('getenv',"true")

        # ---- Add condor accounting group & username.
        self.add_condor_cmd('accounting_group',grouptag)
        self.add_condor_cmd('accounting_group_user',UserName)

        # ---- Add priority specification.
        self.add_condor_cmd('priority',condorPriority)

        # ---- Add memory request.
        self.add_condor_cmd('request_memory',minimalMem)

        # ---- Add mandatory request_disk request.
        minimalDiskPost = "30G"
        self.add_condor_cmd('request_disk',minimalDiskPost)

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xeventweb-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xeventweb-$(cluster)-$(process).err')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file(event_dir_full + 'xeventweb.sub')


class XeventNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    xeventwebpage node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__x_jobnum = None
        self.__x_injnum = None


    # event_dir, results_type, event_pass, event_num

    # ---- Set xeventwebpage inputs.  Need to use python parser flags.
    def set_event_dir(self,path):
        self.add_var_arg('-d ' + path)
        self.__event_dir = path

    def get_event_dir(self):
        return self.__event_dir

    def set_results_type(self,result):
        self.add_var_arg('-t ' + result)
        self.__results_type = result

    def get_results_type(self):
        return self.__results_type

    def set_event_pass(self,n):
        self.add_var_arg('-p ' + n)
        self.__event_pass = n

    def get_event_pass(self,n):
        return self.__event_pass

    def set_event_num(self,n):
        self.add_var_arg('-n ' + n)
        self.__event_num = n

    def get_event_num(self):
        return self.__event_num




# -------------------------------------------------------------------------
#      Write the dag file
# -------------------------------------------------------------------------

dagstring = ''.join(random.choice('0123456789ABCDEF') for i in range(16))

# ---- Create a dag to which we can add jobs
dag = pipeline.CondorDAG(log_dir + dagstring)

dag.set_dag_file(event_dir_full + 'grb_event_' + results_type)
#dag.set_dag_file('grb_event_' + results_type)

# Make an xsearch job
searchjob = XsearchJob(cp)

# Make an midscript job
midscriptjob = XmidscriptJob(cp)
midscriptnode = XmidscriptNode(midscriptjob)

for i in range(len(parameters_files)):
    searchnode = XsearchNode(searchjob)

    searchnode.set_param_file(event_pfilenames[i])
    searchnode.set_x_jobnum(xdetection_jobs[i])
    searchnode.set_output_dir(event_dir_full)    
    searchnode.set_x_injnum('0')

    midscriptnode.add_parent(searchnode)
    searchnode.set_retry(1)

    searchnode.set_name('xdetection_' + results_type + '_seg' + str(xdetection_jobs[i]) + '_' + searchnode.get_name())

    dag.add_node(searchnode)

    #dag_file.write('JOB ' + job_name + ' xsearch.sub\n')
    #dag_file.write('RETRY ' + job_name + ' 3\n')
    #dag_file.write('VARS ' + job_name + ' macroarguments="' + event_dir_full + event_pfilenames[i] + ' ' + str(xdetection_jobs[i]) + ' ' + event_dir_full + ' 0"\n')



midscriptnode.set_name('midscript_' + midscriptnode.get_name())
dag.add_node(midscriptnode)


# Make an xeventwebpage job
eventjob = XeventJob(cp)

# Make an postscript job
postscriptjob = XpostscriptJob(cp)
postscriptnode = XpostscriptNode(postscriptjob)

# add the webpage-generation jobs to the dag
for i in range(int(numEvents)):
    eventnode = XeventNode(eventjob)

    # event_dir, results_type, event_pass, event_num

    eventnode.set_event_dir(event_dir_full)
    eventnode.set_results_type(results_type)
    eventnode.set_event_pass(str(0))
    eventnode.set_event_num(str(i+1))

    eventnode.add_parent(midscriptnode)
    eventnode.set_retry(1)

    postscriptnode.add_parent(eventnode)

    eventnode.set_name('xeventwebpage_' + results_type + '_event_' + str(i+1) + '_' + eventnode.get_name())

    dag.add_node(eventnode)


for i in range(int(numPass)):
    eventnode = XeventNode(eventjob)

    # event_dir, results_type, event_pass, event_num

    eventnode.set_event_dir(event_dir_full)
    eventnode.set_results_type(results_type)
    eventnode.set_event_pass(str(1))
    eventnode.set_event_num(str(i+1))

    eventnode.add_parent(midscriptnode)
    eventnode.set_retry(1)

    postscriptnode.add_parent(eventnode)

    eventnode.set_name('xeventwebpage_' + results_type + '_pass_' + str(i+1) + '_' + eventnode.get_name())

    dag.add_node(eventnode)


postscriptnode.set_name('postscript_' + postscriptnode.get_name())
dag.add_node(postscriptnode)

dag.write_sub_files()
dag.write_dag()



# copy the xsearch.sub file from the grb analysis directory, so we're using the 
# same version of xdetection

# We can get into trouble here if the DAG file for the trigger generation was generated with 
# a different version of glue.pipeline than is being used by this script to generate the event display
# DAG.  In this case, the syntax for macroarguments will be different, and the event display DAG
# will fail.
cmd = 'cp ' + grb_dir + 'xsearch.sub ' + event_dir_full
try:
    os.system(cmd)
except OSError:
    print('Could not find xsearch.sub!')
    print(cmd)
    sys.exit()
