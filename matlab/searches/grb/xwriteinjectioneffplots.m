function [] = xwriteinjectioneffplots(fout,injectionScale,efficiency,analysisEff,...
    nameStr,parametersStr,injUL90p,injUL50p,plotName,figures_dirName,figfiles_dirName)

% XWRITEINJECTIONEFFPLOTS - write injection related plots to webpage.
% Helper function for xmakegrbwebpage.m 
%
% Usage: xwriteinjectioneffplots(fout,injectionScale,efficiency,analysisEff,...
%  nameStr,parametersStr,injUL90p,injUL50p,plotName,figures_dirName,figfiles_dirName)
%
%  fout             Pointer to open and writable webpage file.
%  injectionScale   Vector containing scales at which injections were made.
%  efficiency       Vector containing detection efficiency values 
%                   corresponding to the injectionScales.
%  analysisEff      Same as efficiency but DQ not applied to injections.
%  nameStr          String. Waveform type as recognised by XMAKEWAVEFORM, e.g. 
%                   'chirplet'.
%  parametersStr    String. Waveform parameters as recognised by XMAKEWAVEFORM
%                   or as specified in an ini file., e.g.
%                   '5.0e-22~6.25e-4~1600~0~0~-1;1;linear'.
%  injUL90p         Double. Injection scale at which 90% detection efficiency
%                   is achieved. 
%  injUL50p         Double. Injection scale at which 50% detection efficiency
%                   is achieved. 
%  plotName         String. Used to name plots.
%  figures_dirName  String. Name of output dir for figures.
%  figfiles_dirName String. Name of output dir for figfiles.  
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(11, 11, nargin));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Plot efficency versus injection scale.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Preparatory.
% ---- Shorthands.
X = injectionScale;
Y = efficiency;
Yp = analysisEff;
% ---- Ensure injection scales are sorted in ascending order.
[X, IX] = sort(X);
Y = Y(IX);
Yp = Yp(IX);
% ---- Numerical errors can cause efficiencies to be slightly greater than 1.
%      Enforce range [0,1].
Y(Y<0) = 0;
Y(Y>1) = 1;
Yp(Yp<0) = 0;
Yp(Yp>1) = 1;

% ---- Determine whether we want to plot eff vs hrss or eff vs distance, and the
%      default hrss or distance. 
[val,unit] = getinjamplordist(nameStr,parametersStr);
if strcmpi(unit,'Hz^(-0.5)')
    % ---- Waveform is characterised by hrss amplitude [Hz^-0.5]. 
    amplPlot = true;
    hrss = val;
elseif strcmpi(unit,'Mpc')
    % ---- Waveform is characterised by distance [Mpc]. 
    amplPlot = false;
    dist = val;
end

% ---- Make plot of efficiencies with and without DQ vetoes applied.
figure; hold on;
if amplPlot
    semilogx(X*hrss,Y,'k-o','linewidth',2,'MarkerFaceColor','k')
    plot(injUL90p*hrss,0.9,'rs','MarkerFaceColor','r');
    plot(injUL50p*hrss,0.5,'rs','MarkerFaceColor','y');
    semilogx(X*hrss,Yp,'b-','linewidth',1)
    % ---- Color green points that are not zero efficiency but are below 5%; this 
    %      is to draw the eye to false associations.
    mask = Y>0 & Y<=0.05;
    semilogx(X(mask)*hrss,Y(mask),'go','linewidth',2,'MarkerFaceColor','g');
    % ---- Format plot.
    xlabel('hrss for optimal orientation [Hz^{-1/2}]')
    axis([min(X)*hrss max(X)*hrss 0 1.0]);
else
    semilogx(dist./X,Y,'k-o','linewidth',2,'MarkerFaceColor','k')
    plot(dist./injUL90p,0.9,'rs','MarkerFaceColor','r');
    plot(dist./injUL50p,0.5,'rs','MarkerFaceColor','y');
    semilogx(dist./X,Yp,'b-','linewidth',1)
    % ---- Color green points that are not zero efficiency but are below 5%; this 
    %      is to draw the eye to false associations.
    mask = Y>0 & Y<=0.05;
    semilogx(dist./X(mask),Y(mask),'go','linewidth',2,'MarkerFaceColor','g');
    % ---- Format plot.
    xlabel('distance [Mpc]')
    axis([dist./max(X) dist./min(X) 0 1.0]);
end
% ---- Additional formatting.
set(gca,'FontSize',20)
set(gca,'xscale','log')
legend('on source','90% upper limit','50% upper limit','analysis eff','Location','SouthEast')
title('detection efficiency');
ylabel('efficiency')
grid
hold off

% ---- Save plot.
xsavefigure(plotName,figures_dirName,figfiles_dirName);

% ---- Done.
return

