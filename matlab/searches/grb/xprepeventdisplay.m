function [] = xprepeventdisplay(auto_dir, grb_name, results_type, user_tag)

% XPREPEVENTDISPLAY - gather event parameters from post-processing results
%
% Usage:
%
% [] = xplotasdcomparison()
%
% auto_dir		Path to post-processing results
%
% grb_name		String with GRB label
%
% results_type		'closedbox' or 'openbox'
%
% user_tag		label for post-processing
%
% Note that prefix for .mat files from xmakegrbwebpage will be {grb_name}_{user_tag}_{results_type}.
% We expect these files to be in the auto_dir.
%
% Example: xprepeventdisplay('/home/dhoak/grbs/S6/test/Apr15/GW100916_22/auto_web/', 'GW100916', 'openbox', 'r4844')
%

% Build the path to the results file from the post-processing
post_file = [auto_dir grb_name '_' user_tag '_' results_type '.mat'];

% load the on-source results from the mat file (for a closedbox analysis, this is the dummy on-source)
load(post_file,'onSource','analysis');

% how many events are we dealing with?
nEvents = length(onSource.significance);

if nEvents >= 10
   numEvents = 10;
else
   numEvents = nEvents;
end

nPass = sum(onSource.pass);
if nPass >= 10
   numPass = 10;
else
   numPass = nPass;
end

B = tabulate(onSource.realJobNumber);
B(B(:,2)==0,:) = [];
jobNumbers = B(:,1);
numJobs = length(jobNumbers);
numTimes = length(analysis.analysisTimes);

numLikelihoods = length(analysis.likelihoodType);

paramsFileName = [auto_dir 'event/' grb_name '_' results_type '_' user_tag '_eventparams.txt'];

fparams = fopen(paramsFileName,'w');

for i=1:numJobs
    fprintf(fparams,'%s ',num2str(jobNumbers(i)));
end
fprintf(fparams,'\n');

fprintf(fparams,'%s\n',num2str(numEvents));
fprintf(fparams,'%s\n',num2str(numPass));
fprintf(fparams,'%s\n',num2str(numTimes));

for i=1:length(analysis.detectorList)
    fprintf(fparams,'%s ', analysis.detectorList{i});
end
fprintf(fparams,'\n');

fprintf(fparams,'%s\n',num2str(numLikelihoods));
for i=1:numLikelihoods
    fprintf(fparams,'%s ',analysis.likelihoodType{i});
end
fprintf(fparams,'\n');

fclose(fparams);


% now print files with all the useful event parameters
for thisTable = 1:2

    % sort the events
    if (thisTable == 1)
       [s I] = sort(onSource.significance,'descend');
       fname = [auto_dir 'event/' grb_name '_' results_type '_' user_tag '_events.txt'];

       % print out the important event properties

       fevents = fopen(fname,'w');

       for i=1:numEvents
	   fprintf(fevents,'%9.5f ',onSource.peakTime(I(i)));
	   fprintf(fevents,'%g ',onSource.realJobNumber(I(i)));
	   fprintf(fevents,'%g ',1/onSource.analysisTime(I(i)));
	   fprintf(fevents,'%g ',onSource.significance(I(i)));
	   fprintf(fevents,'%g ',onSource.probability(I(i)));
	   fprintf(fevents,'%g ',onSource.probabilityZeroLag(I(i)));

	   for jj=1:length(analysis.likelihoodType)
	       fprintf(fevents,'%g ',onSource.likelihood(I(i),jj));
	   end

 	   fprintf(fevents,'%g ',onSource.peakFrequency(I(i)));
	   fprintf(fevents,'%g ',onSource.boundingBox(I(i),3));
	   fprintf(fevents,'%g ',onSource.boundingBox(I(i),2));
	   fprintf(fevents,'%g ',onSource.boundingBox(I(i),2)+onSource.boundingBox(I(i),4));
	   fprintf(fevents,'%g ',onSource.nPixels(I(i)));

	   fprintf(fevents,'%g ',onSource.pass(I(i)));
	   fprintf(fevents,'%g ',onSource.passWindow(I(i)));
	   fprintf(fevents,'%g ',onSource.passVetoSegs(I(i)));
	   fprintf(fevents,'%g ',onSource.passFixedRatio(I(i)));
	   fprintf(fevents,'%g ',onSource.passRatio(I(i)));

	   % for closedbox results we might be interested in the unslid time for each detector
	   % the peakTime is in terms of the first detector in detectorList

	   for ii=1:length(analysis.detectorList)
	       fprintf(fevents,'%9.5f ',onSource.unslidTime(I(i),ii));
	   end	   

	   fprintf(fevents,'\n');
        end

	fclose(fevents);

    elseif (thisTable == 2)
       [s I] = sort(onSource.significance.*onSource.pass,'descend');
       fname = [auto_dir 'event/' grb_name '_' results_type '_' user_tag '_events_pass.txt'];

       % print out the important event properties

       fevents = fopen(fname,'w');

       for i=1:numPass
       	   
	   fprintf(fevents,'%9.5f ',onSource.peakTime(I(i)));
	   fprintf(fevents,'%g ',onSource.realJobNumber(I(i)));
	   fprintf(fevents,'%g ',1/onSource.analysisTime(I(i)));
	   fprintf(fevents,'%g ',onSource.significance(I(i)));
	   fprintf(fevents,'%g ',onSource.probability(I(i)));
	   fprintf(fevents,'%g ',onSource.probabilityZeroLag(I(i)));

	   for ii=1:length(analysis.likelihoodType)
	       fprintf(fevents,'%g ',onSource.likelihood(I(i),ii));
	   end

	   fprintf(fevents,'%g ',onSource.peakFrequency(I(i)));
	   fprintf(fevents,'%g ',onSource.boundingBox(I(i),3));
	   fprintf(fevents,'%g ',onSource.boundingBox(I(i),2));
	   fprintf(fevents,'%g ',onSource.boundingBox(I(i),2)+onSource.boundingBox(I(i),4));
	   fprintf(fevents,'%g ',onSource.nPixels(I(i)));

	   fprintf(fevents,'%g ',onSource.pass(I(i)));
	   fprintf(fevents,'%g ',onSource.passWindow(I(i)));
	   fprintf(fevents,'%g ',onSource.passVetoSegs(I(i)));
	   fprintf(fevents,'%g ',onSource.passFixedRatio(I(i)));
	   fprintf(fevents,'%g ',onSource.passRatio(I(i)));

	   % for closedbox results we might be interested in the unslid time for each detector
	   % the peakTime is in terms of the first detector in detectorList

	   for ii=1:length(analysis.detectorList)
	       fprintf(fevents,'%9.5f ',onSource.unslidTime(I(i),ii));
	   end

	   fprintf(fevents,'\n');
	end

	fclose(fevents);

    end

end