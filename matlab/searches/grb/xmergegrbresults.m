function xmergegrbresults(outputDir,targetDir,forceFlag,cutFile,matFile,preCutFile,threshold,purgeFlag)
% XMERGEGRBRESULTS - Wrapper to xmergegrbfiles.
%
% xmergegrbresults is a wrapper to xmergegrbfiles to combine the output files
% of multiple X-Pipeline GRB jobs into a single file.
%
% usage:
%
% xmergegrbresults(outputDir,targetDir,forceFlag,cutFile,matFile,preCutFile,threshold)
%
%   outputDir  String specifying path to "output" directory in which "target"  
%              directory lives.
%   targetDir  String specifying name of "target" directory that contains files
%              to be merged.
%
% The optional inputs forceFlag, cutFile, matFile, preCutFile, threshold, and purgeFlag are all 
% strings. Please see XMERGEGRBFILES for their definitions.
%
% $Id: xmergegrbresults.m 6518 2024-04-24 10:15:38Z patrick.sutton@LIGO.ORG $

% ---- Assign required defaults. 
if nargin < 8
    purgeFlag = 1;
else
    purgeFlag = str2num(purgeFlag);
end
if purgeFlag
    disp('Purging redundant event triggers.');
end
%
if nargin < 7
    threshold = 0;
else
    threshold = str2num(threshold);
end
if threshold 
    disp(['Applying event significance threshold of ' str(threshold)]);
end
%
if nargin < 6
    preCutFile = 'None';
end
if nargin < 5
    % ---- We need both cutFile and matFile if either is supplied.
    cutFile = 'None';
    matFile = 'None';
end
if ~strcmpi(preCutFile,'None') &  ~strcmpi(cutFile,'None')
    disp(['Apply pre-set coherent two-shape cuts found in:' cutFile ', ' matFile ', ' preCutFile]);
elseif ~strcmpi(cutFile,'None')
    disp(['Apply pre-set coherent cuts found in:' cutFile ', ' matFile ]);
end
%
if nargin < 3
    forceFlag = 0;
else
    forceFlag = str2num(forceFlag);
end
if forceFlag
    disp('Ignoring missing trigger files (supernova flag).');
end

% ---- Ensure that targetDir has trailing "/" (not needed for outputDir).
if targetDir(end) ~= '/'
    targetDir = [targetDir '/'];
end

% ---- cd to output directory.
cd(outputDir);

% ---- Remove any pre-existing merged file.
[status,result] = system(['rm -f ' targetDir '*merged.mat']);

% ---- Make cell array list of directories matching targetDir string.
[status,result] = system(['ls -d --color=none ' targetDir]);
DIR_LIST = strread(result,'%s');

% ---- For each target directory get list of output files and run merge
%      function on them. 
for ii = 1:length(DIR_LIST)
    [status,FILE_LIST] = system(['ls -v --color=none ' DIR_LIST{ii} '| grep mat']);
    xmergegrbfiles(DIR_LIST{ii},FILE_LIST,forceFlag,cutFile,matFile,preCutFile,threshold,purgeFlag);
end

% ---- It's a rude function that doesn't return to the starting directory.
cd('..');

% ---- Done.
return

