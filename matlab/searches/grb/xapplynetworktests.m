function [networkCell,pass] = xapplynetworktests(detectors_considered, ...
    centerTimes, timeOffsets, live_seglist, veto_seglist, verbosity, ...
    windowBeginOffset, windowEndOffset, transientTime, windowLength)  
% XAPPLYNETWORKTESTS - tests list of detectors to see which satisfy
% our criteria for being part of our detector network. 
%
%  usage: [networkCell,pass] = xapplynetworktests(detectors_considered,...
%     centerTimes, timeOffsets, live_seglist, veto_seglist, verbosity, ...
%     windowBeginOffset, windowEndOffset, transientTime, windowLength)
%
%  detectors_considered       Cell array of ifos to be considered, e.g.,
%                             {'H1' 'H2' 'L1'}, size (1 * nIfo).
%  centerTimes                Vector of segment center times, size (nJobs * 1).
%  timeOffsets                Array of segment time offsets, size(nJobs * nIfo).
%  live_seglist               Struct containing livetime segments (typically  
%                             "science-cat1DQ" times). Fields:
%                               live_seglist(nIfo).gpsStart()
%                               live_seglist(nIfo).duration()
%  veto_seglist               Similar to live_seglist but containing deadtimes 
%                             (typically cat2 + cat4 times).
%  verbosity                  String used to control verbosity of output,
%                             either 'verbose' or 'quiet'. 
%  windowBeginOffset          Scalar. Offset [sec] between start of on-source
%                             window and trigger time, e.g., -600. 
%  windowEndOffset            Scalar. Offset [sec] between end of on-source 
%                             window and trigger time, e.g., 60.
%  transientTime              Scalar. Length of time [sec] discarded at each
%                             end of each analysis block, e.g., 16.
%  windowLength               Scalar. Total amount of data (including start
%                             and end transients) that must be read to cover
%                             the on-source window with a whole number of
%                             analysis jobs:
%                               2*transientTime+jobsPerWindow*(blockTime-2*transientTime)
%
%  networkCell                Cell array listing ifos which should be included
%                             in the network.
%  pass                       Array indicating whether ifo should be included in
%                             our network. 1 means include, 0 means don't.
%                             Size(nJobs * nIfo).
% 
%  Our criteria for including an ifo in our network is as follows:
%
%  1) We require an ifo to be in science mode for the entire duration of the
%      interval for which data has to be read. This is [gps1,gps2] where 
%        gps1 = unslidCenterTimes + windowBeginOffset - transientTime;
%        gps2 = gps1 + windowLength;
%     Livetimes are typically defined as science-cat1 times. Any cat1 flags 
%     in the interval analysed (including in transient time buffers) could lead
%     to inaccurate PSD estimation, which affects the whitening.
%
%  2) Less than 5% deadtime in the on-source window incurred by data quality
%     flags (typically category 2 flags), where the on-source window is 
%        [unslidCenterTimes + windowBeginOffset, unslidCenterTimes + windowBeginOffset]
%     Some of our glitch rejection cuts are tuned using the estimated 90% or 95%
%     UL on injection amplitude. If we lose much more than 5% deadtime in our
%     (dummy) onsource we may not be able to accurately estimate the injection
%     amplitude leading to 90% or 95% detection efficiency.
%
%  3) No deadtime incurred by category 2 flags in an interval [-5,+1]s about 
%     the GRB trigger time. Many GRB models predict similar arrival times of 
%     EM and GW emission from a GRB. We therefore require that inclusion of 
%     an ifo does not kill times close to the GRB trigger time.
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(10, 10, nargin));

% ---- Check that verbosity is correctly defined.
if ~( strcmp(verbosity,'verbose') | ...
    strcmp(verbosity,'quiet') )
    error('verbosity must be either verbose or quiet')
end
if strcmp(verbosity,'verbose')
    verboseFlag = 1;
else
    verboseFlag = 0;
end

% ---- Number of jobs equals number of centerTimes passed in.
nJobs = length(centerTimes);

% ---- Initially assume all ifos FAIL our criteria for all of the jobs.
% ---- Note that unlike other pass flags this has one element per job (rather
%      than one per trigger).
pass = zeros(nJobs,length(detectors_considered));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Apply network test.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over jobs, each job represents a choice
%      of segment centerTimes and timeOffsets.
for iJob = 1:nJobs

    % ---- Loop over ifos we are considering.
    for iIfo = 1:length(detectors_considered)

       
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                     Unslide centerTimes.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Before applying vetoSeg cuts we must unslide the triggers.
        unslidCenterTimes(iJob,iIfo) = centerTimes(iJob) + timeOffsets(iJob,iIfo);


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %  Check that we have science mode data covering the full interval about 
        %                         unslid centerTimes.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Define our interval about the (unslid) centerTimes.
        goodBefore = -windowBeginOffset+transientTime;
        duration   = windowLength;

        % ---- Find intersections between livetimes and this interval.
        coincOut = Coincidence2(unslidCenterTimes(iJob,iIfo)-goodBefore, duration,...
            live_seglist(iIfo).gpsStart, live_seglist(iIfo).duration);      
      
        if ~isempty(coincOut)
            % ---- If our interval is completely contained within a live segment
            %      we should only have one intersection and it should have the 
            %      duration of our interval.
            if (size(coincOut,1) == 1) & (coincOut(1,2) == duration)
                pass(iJob,iIfo) = 1;
                % ---- Optional verbosity.
                if (verboseFlag == 1)
                    fprintf(1,['%s has good cat 1 DQ for job %d \n'],...
                    detectors_considered{iIfo}, iJob );
                end 
            end
        end
      
      
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %    Check for veto flags raised in the [-5,+1]s interval about unslid 
        %                        centerTimes.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Define our [-5,+1]s interval about the (unslid) centerTimes.
        goodBefore = 5;
        duration   = 6;

        % ---- Find intersections between cat2 segments and the [-5,+1]s interval
        %      about unslid centerTimes.
        coincOut = Coincidence2(unslidCenterTimes(iJob,iIfo)-goodBefore,duration,...
            veto_seglist(iIfo).gpsStart, veto_seglist(iIfo).duration);

        % ---- If there were any coincidences between our interval and the
        %      vetoSegs we must discard this job. We don't count
        %      coincidences with zero duration (edge overlap only).
        if ~isempty(coincOut) && any(coincOut(:,2)>0)
            % ---- Setting pass for this job to zero.
            pass(iJob,iIfo) = 0;
            if (verboseFlag == 1)
                fprintf(1,['Time killed in [-%f,+%f]s interval about center '...
                   'time \n'],goodBefore,duration-goodBefore);
                fprintf(1,'startTime     stopTime\n');
                fprintf(1,'--------------------------\n');
                for iCoinc = 1:length(coincOut(:,1));
                    fprintf(1,'%9.2f  %9.2f \n',...
                        coincOut(iCoinc,1), ...
                        coincOut(iCoinc,1) + coincOut(iCoinc,2));
                end
                fprintf(1,['Setting pass to zero \n']);
            end
        end

      
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     Find deadtime in on-source window. 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Define on-source interval about the (unslid) centerTimes.
        goodBefore = -windowBeginOffset;
        duration   = windowEndOffset-windowBeginOffset;

        % ---- Allowed threshold on deadtime in on-source window is 5%.
        killedThresh = 0.05*duration;

        % ---- Find intersections between veto segs and our on-source region.
        coincOut = Coincidence2(unslidCenterTimes(iJob,iIfo)-goodBefore, duration, ...
            veto_seglist(iIfo).gpsStart, veto_seglist(iIfo).duration);

        % ---- Record times killed by segments. 
        if isempty(coincOut)
            % ---- No times killed by cat2 veto segments.
            liveTimes{iJob,iIfo} = [unslidCenterTimes(iJob,iIfo)-goodBefore,duration];
        else
            % ---- Some times killed by veto segments. Any ifo with deadtime >=
            %      killedThresh is discarded straight away.
            % ---- Sum up duration of intersections to find deadtime.
            totDeadTime_cat2(iJob,iIfo) = sum(coincOut(:,2)); 
            if totDeadTime_cat2(iJob,iIfo) >= killedThresh
                pass(iJob,iIfo) = 0;
            end
            liveTimes{iJob,iIfo} = ComplementSegmentList(coincOut(:,1),coincOut(:,2),...
                unslidCenterTimes(iJob,iIfo)-goodBefore,...
                unslidCenterTimes(iJob,iIfo)-goodBefore+duration);
        end

        % ---- Now reslide the liveTimes in order to measure the total deadtime in
        %      the on-source interval. To do this we must SUBTRACT timeOffsets.
        reslidLiveTimes{iJob,iIfo} = [liveTimes{iJob,iIfo}(:,1) - ...
            timeOffsets(iJob,iIfo), liveTimes{iJob,iIfo}(:,2)];

    end % -- Loop over ifos considered.

    % ---- For current job, find intersection of reslid liveTimes from all ifos
    %      that are still in our network.
    intersectReslidLiveTimes{iJob} = [0, Inf];
    for iIfo = 1:length(detectors_considered)
        % ---- We only care about the ifo's livetime if it is still in our
        %      network. 
        if pass(iJob,iIfo)
            coincOut = Coincidence2(intersectReslidLiveTimes{iJob}(:,1),...
                intersectReslidLiveTimes{iJob}(:,2),...
                reslidLiveTimes{iJob,iIfo}(:,1),...
                reslidLiveTimes{iJob,iIfo}(:,2));
            if isempty(coincOut)
                intersectReslidLiveTimes{iJob} = [0,0];
            else
                intersectReslidLiveTimes{iJob} = [coincOut(:,1), coincOut(:,2)];
            end
        end % -- if pass(iJob,iIfo)
    end % -- loop over detectors_considered. 

    % ---- For current job, find time killed by veto flags.
    finalKilledTimes{iJob} = ComplementSegmentList(...
        intersectReslidLiveTimes{iJob}(:,1),...
        intersectReslidLiveTimes{iJob}(:,2),...
        centerTimes(iJob)-goodBefore,centerTimes(iJob)-goodBefore+duration);
    deadtime(iJob) = sum(finalKilledTimes{iJob}(:,2));

   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %         Discard jobs with >=5% deadtime in the on-source window. 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if (verboseFlag == 1)
        fprintf(1,['Total time killed in [' num2str(windowBeginOffset) ',' ... 
            num2str(windowEndOffset) ']s interval about '...
            'centre time: %9.2f \n'],...
            deadtime(iJob));
        fprintf(1,'startTime     stopTime\n');
        fprintf(1,'--------------------------\n');
        for iCoinc = 1:length(finalKilledTimes{iJob}(:,1));
            fprintf(1,'%9.2f  %9.2f \n',...
                finalKilledTimes{iJob}(iCoinc,1), ...
                finalKilledTimes{iJob}(iCoinc,1) + ...
                finalKilledTimes{iJob}(iCoinc,2));
        end
    end % -- if (verboseFlag == 1)

    % ---- Check total duration of killed times
    if deadtime(iJob) >= killedThresh;

        warning(['Killing network due to veto flags in on-source. ' ...
            'We should investigate this network further']);

        networkCell{iJob} = {'X'};
      
        for iIfo = 1:length(detectors_considered) 
            if pass(iJob,iIfo)
                networkCell{iJob} = [networkCell{iJob} detectors_considered{iIfo}];
            end
            pass(iJob,iIfo) = 0;
        end 

        if (verboseFlag == 1)
            fprintf(1,'More than %ds killed, setting pass to zero \n',killedThresh);
        end
      
    else
        
        networkCell{iJob} = {};

    end

   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                     Construct network cell array.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Loop over ifos we are considering.
    for iIfo = 1:length(detectors_considered)
        if pass(iJob,iIfo) 
            networkCell{iJob} = [networkCell{iJob} detectors_considered{iIfo}];
        end     
    end

end % -- Loop over jobs.

% ---- Done.
return
