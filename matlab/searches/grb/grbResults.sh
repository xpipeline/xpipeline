#!/bin/bash

if [[ -z $1 ]];then
    echo "No analysis type supplied. Possible values are:"
    echo "openbox, closedbox, snrulclosedbox"
    exit 1
fi
atype=$1

if [[ -z $2 ]];then
    ratioarray="0"
else
    ratioarray=$2
fi

if [[ -a grbname.txt ]]; then
grbname=`cat grbname.txt`
else
echo "No grbname.txt file found, this file should contain the name of the GRB"
exit 1
fi

cd output
${XPIPELINE_ROOT}/grb/makeAutoFiles2.py -p ../grb.ini

${XPIPELINE_ROOT}/post/makeWebPage.sh $grbname auto.txt $ratioarray $atype
rm -rf ../$atype
mkdir ../$atype
mv figures ../$atype
mv figfiles ../$atype
mv $grbname.shtml ../$atype




