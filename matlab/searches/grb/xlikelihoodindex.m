function [likelihoodIndex,vetoRangeOut] = xlikelihoodindex( ...
   likelihoodName,likelihoodType,likelihoodArray,vetoRange)
% XLIKELIHOODINDEX - Find requested likelihood in likelihoodType array.
%
% XLIKELIHOODINDEX reports which element of likelihoodType corresponds to
% likelihoodName.  It also checks whether the values in the corresponding
% column of likelihoodArray are nonzero.
%
% Usage: 
%
% [likelihoodIndex,vetoRangeOut] = xlikelihoodindex(likelihoodName, ...
%     likelihoodType,likelihoodArray,vetoRange)
%
%   likelihoodName   String. Name of likelihood we want to identify in our
%                    likelihoodArray.  Must be one of 'eplus', 'iplus',
%                    'ecross', 'icross', 'enull', 'inull','enullH1H2',
%                    'inullH1H2' . This function
%                    takes into account that each likelihood has
%                    alternative names.
%   likelihoodType   Cell array of strings listing likelihoods.
%   likelihoodArray  Optional.  Array of likelihoods, with one column for 
%                    each element of likelihoodType.
%   vetoRange        Optional.  Vector of veto cuts to be performed
%                    involving likelihoodName.
%
%   likelihoodIndex  The column of likelihoodType and likelihoodArray that
%                    contains likelihoods of type likelihoodName.  Returns
%                    zero if likelihoodType does not contain any likelihood
%                    corresponding to likelihoodName, or if the matching
%                    values in likelihoodArray are all identically zero.
%   vetoRangeOut     If likelihoodIndex == 0, or if likelihoodArray and
%                    vetoRange are not supplied, then vetoRangeOut is set
%                    to zero.  Otherwise, the input value of vetoRange is
%                    returned. 
%
% $Id$ 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number of inputs: 2 or 4.
error(nargchk(2, 4, nargin));
if (nargin==3)
    error('wrong number of input arguments.');
end
if (nargin==2)
    vetoRangeOut = 0;
end

% ---- Check on input arguments.
if not(isstr(likelihoodName))
    error('likelihoodName must be a string.');
end
if not(iscell(likelihoodType))
    error('likelihoodType must be a cell array.');
end
if (nargin==4)
    if not(isnumeric(likelihoodArray))
        error('likelihoodArray must be a numeric array.');
    end
    if not(isnumeric(vetoRange))
        error('vetoRange must be a numeric array.');
    end
    if (size(likelihoodArray,2)~=length(likelihoodType))
        error(['likelihoodArray must have the same number of columns ' ...
            'as the length of likelihoodType.']);
    end
end

% ---- We expect the user to supply one of the following for
%      likelihoodName.
allowedLikelihoodNames = {'eplus','iplus','ecross','icross',...
                          'enull','inull','enullH1H2','inullH1H2',...
                          'icirc','ecirc','icircnull','ecircnull',...
                          'iamp','eamp','iampnull','eampnull',...
                          'icircnullreg','ecircnullreg',...
                          'escalar','iscalar','iscalarnull','escalarnull'};

% ---- Verify that likelihoodName is one of the allowedLikelihoodNames.
if not(max(strcmp(likelihoodName,allowedLikelihoodNames)))
   error(['likelihoodName should be one of eplus, iplus, ecross, '...
          'icross, enull, inull, enullH1H2, inullH1H2, escalar, iscalar'...
          'iscalarnull, escalarnull, icirc, ecirc, icircnull, ecircnull']);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Identify likelihoods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Likelihoods can have a variety of names.
ePlusAltNames     = {'plusenergy','eplus','hardconstraint'};
iPlusAltNames     = {'plusinc','iplus'};
eCircAltNames     = {'circenergy','ecirc'};
iCircAltNames     = {'circinc','icirc'};
eCircNullAltNames = {'circnullenergy','ecircnull'};
iCircNullAltNames = {'circnullinc','icircnull'};
eScalarAltNames   = {'scalarenergy','escalar'};
iScalarAltNames   = {'scalarinc','iscalar'};
eScalarNullAltNames = {'scalarnullenergy','escalarnull'};
iScalarNullAltNames = {'scalarnullinc','iscalarnull'};
eAmpAltNames     = {'ampenergy','eamp'};
iAmpAltNames     = {'ampinc','iamp'};
eAmpNullAltNames = {'ampnullenergy','eampnull'};
iAmpNullAltNames = {'ampnullinc','iampnull'};
eCircNullRegAltNames = {'circnullenergyreg','ecircnullreg'};
iCircNullRegAltNames = {'circnullincreg','icircnullreg'};
eCrossAltNames    = {'crossenergy','ecross'};
iCrossAltNames    = {'crossinc','icross'};
eNullAltNames     = {'nullenergy','enull'};
iNullAltNames     = {'incoherentenergy','nullinc','inull'};
eNullH1H2AltNames = {'H1H2nullenergy'};
iNullH1H2AltNames = {'H1H2nullinc'};

% ---- Find this alternative names associated with our likelihoodName.
if strcmp(likelihoodName,'eplus')
   altNames = ePlusAltNames; 
elseif strcmp(likelihoodName,'iplus')
   altNames = iPlusAltNames; 
elseif strcmp(likelihoodName,'icirc')
   altNames = iCircAltNames; 
elseif strcmp(likelihoodName,'ecirc')
   altNames = eCircAltNames; 
elseif strcmp(likelihoodName,'icircnull')
   altNames = iCircNullAltNames; 
elseif strcmp(likelihoodName,'ecircnull')
   altNames = eCircNullAltNames; 
elseif strcmp(likelihoodName,'iscalar')
   altNames = iScalarAltNames; 
elseif strcmp(likelihoodName,'escalar')
   altNames = eScalarAltNames; 
elseif strcmp(likelihoodName,'iscalarnull')
   altNames = iScalarNullAltNames; 
elseif strcmp(likelihoodName,'escalarnull')
   altNames = eScalarNullAltNames; 
elseif strcmp(likelihoodName,'iamp')
   altNames = iAmpAltNames; 
elseif strcmp(likelihoodName,'eamp')
   altNames = eAmpAltNames; 
elseif strcmp(likelihoodName,'iampnull')
   altNames = iAmpNullAltNames; 
elseif strcmp(likelihoodName,'eampnull')
   altNames = eAmpNullAltNames; 
elseif strcmp(likelihoodName,'icircnullreg')
   altNames = iCircNullRegAltNames; 
elseif strcmp(likelihoodName,'ecircnullreg')
   altNames = eCircNullRegAltNames; 
elseif strcmp(likelihoodName,'ecross')
   altNames = eCrossAltNames; 
elseif strcmp(likelihoodName,'icross')
   altNames = iCrossAltNames; 
elseif strcmp(likelihoodName,'enull')
   altNames = eNullAltNames; 
elseif strcmp(likelihoodName,'inull')
   altNames = iNullAltNames; 
elseif strcmp(likelihoodName,'enullH1H2')
   altNames = eNullH1H2AltNames;
elseif strcmp(likelihoodName,'inullH1H2')
   altNames = iNullH1H2AltNames;
else
   % ---- This should be impossible
   error('likelihoodName is not valid');
end

% ---- Check to see whether any of the altNames appear in likelihoodType.
likelihoodIndex = 0;
for idx = 1:length(altNames)
    % ---- temp is the index (or indices) of likelihoodType which match 
    %      altName{idx}.  It will be empty if altNames{idx} does not occur
    %      in likelihoodType.
    temp = find(strcmp(altNames{idx},likelihoodType));
    if not(isempty(temp))
        % ---- Extract last instance of altNames{idx} in likelihoodType.
        %      (This function is robust against the case where the same
        %      likelihood appears repeatedly.)
        likelihoodIndex = max([likelihoodIndex,temp(:).']);
    end
end

% ---- If a likelihoodArray and veto range have been supplied, then check
%      that likelihood values are nonzero for this likelihood.
if (4==nargin)
    % ---- Default behaviour is to return input vetoRange.
    vetoRangeOut = vetoRange;
    if (0==likelihoodIndex)
        % ---- If likelihoodName, or one of its altNames, does not appear
        %      in likelihoodType then we have not calculated this
        %      likelihood and we will not perform veto cuts using it.
        vetoRangeOut = 0;
        display(['WARNING: ' likelihoodName ' likelihoods have not ' ...
            'been calculated by xdetection.']);  
        display('We will not perform veto cuts using this likelihood');
    elseif (max(abs(likelihoodArray(:,likelihoodIndex)))==0)
        % ---- If the values stored in the likelihoodArray column
        %      corresponding to likeihoodName are all zeroes we will ignore
        %      them and not perform veto cuts using this likelihood.
        likelihoodIndex = 0;
        vetoRangeOut = 0;
        display(['WARNING: ' likelihoodName ' likelihoods values all ' ...
            'equal to zero.']);
        display('We will not perform veto cuts using this likelihood');
    end
end

% ---- Done.
return

