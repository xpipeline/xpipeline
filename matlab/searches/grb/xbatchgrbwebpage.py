#!/usr/bin/env python


"""
Script for batch running of xgrbwebpage.py. The script identifies sub-directories with 
names of the format GRB*, E*, or MOCK* in a user-specified parent directory. The script
then creates a new "auto-dir" directory containing one subdirectory for each GRB. In each 
of these subdirectories the script sets up post-processing jobs using xgrbwebpage.py, 
including the creation of condor DAG files.
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, subprocess, getopt, re
import configparser

# ---- Function usage.
def usage():
    msg = """\

Script for batch running of xgrbwebpage.py. The script identifies sub-directories with 
names of the format GRB*, E*, or MOCK* in a user-specified parent directory. The script
then creates a new "auto-dir" directory containing one subdirectory for each GRB. In each 
of these subdirectories the script sets up post-processing jobs using xgrbwebpage.py, 
including the creation of condor DAG files.

Usage: 

  xbatchgrbwebpage.py [options]
  -d, --parent-dir <path>     [REQUIRED] Absolute path to parent directory containing GRB 
                              analysis subdirectories
  -a, --auto-dir <path>       [REQUIRED] Absolute path to directory where we will write 
                              the auto_web directories
  -w, --web-dir <path>        [REQUIRED] Absolute path to directory where we will write 
                              the web page reports
  -l, --log-dir <path>        [REQUIRED] Absolute path to directory where we will write 
                              the condor log files. This must be on a local file system, 
                              not in your home directory); e.g., /usr1/albert.einstein/
  -u, --user-tag <string>     [REQUIRED] Descriptive name of run; used in naming of output
                              files.
  -c, --veto-method <type>    [REQUIRED] Type of veto cut to perform.  Must be one of 
                              '(linear|median|medianLin|alpha|medianAlpha|alphaLin)
                               (Cut|CutCirc|CutAmp|CutScalar)', e.g. 'medianAlphaCutCirc'.
  -p, --percentile <string>   [REQUIRED] String in XX-YY format, where XX is the 
                              percentile level of the background distribution to use for 
                              coherent veto test tuning and YY is the percentile level 
                              used for sensitivity estimation.
  --freq-veto <path>          Path to file with frequency bands to veto.
  -s, --add-stat <string>     Name of an additional detection statistic for which the 
                              tuning will be done. 
  --priority <prio>           Integer specifying the priority of the job. Higher-priority
                              jobs are submitted to the cluster before lower-priority jobs
                              by the same user. Default value is 0.
  --big-mem <memory>          Integer specifying the minimal memory requirement for condor
                              jobs in MB.
  --tuning-waveforms <string> Tilde-delimited string of waveform names to be used for
                              tuning.  Waveforms must be a subset of the sets injected.
                              Names must be in *lower case*, and exactly match those in
                              the [waveforms] section of the .ini file, e.g. 
                              "csg500Q9incljitter5~csg1000q9incljitter5".
                              Default 'all' tunes using all waveforms.
  --user                      User's Albert.Einstein username. If not specified then the 
                              whoami system comand will be used to guess it when running
                              xgrbwebpage.py.
  -e, --event-display <#>     Boolean (1 or 0) flag to enable event display generation. If
                              1, event display scripts and condor dags are automatically
                              run as part of the 'post' scripts for closedbox and openbox 
                              results. 
  -h, --help                  Display this message and exit.

e.g.,
xbatchgrbwebpage.py \\
--parent-dir /home/mwas/GRBrerun/S6A/analyzeGRBs \\
--auto-dir /home/mwas/GRBrerun/S6A/alphaLin \\
--web-dir ~/WWW/LSC/GRBrerun/S6A/alphaLin  \\
--log-dir /local/user/mwas/log/ \\
--user-tag michal-alphalin \\
--veto-method alphaLinCutCirc \\
--percentile 99-99 \\
--add-stat powerlaw \\
--big-mem 12000 \\
--event-display 1

Note: All paths need to be global; use `pwd` instead of ./ to obtain a local path that 
is expanded to a global path by your shell

This script sets up the directory structure and creates the condor submit files required
to run post-processing (xmakegrbwebpage) jobs on a cluster. Once you have run this script
you will find a new shell script has been created: 

  run_xmakegrbwebpageDAGJob_<user-tag>_ALL.sh 

Run this script to launch the post-processing jobs on condor.

"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
grb_list = None
detector = []
stat_list = []
condorPriority = 0
minimalMem = False
veto_method = []
tuning_waveforms = ''
user_name = None
freq_veto_file = None
event_display = 0

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hd:a:w:l:u:c:s:p:e:"
# ---- Long form.
longop = [
   "help",
   "parent-dir=",
   "auto-dir=",
   "web-dir=",
   "log-dir=",
   "user-tag=",
   "user=",
   "veto-method=",
   "add-stat=",
   "freq-veto=",
   "percentile=",
   "priority=",
   "big-mem=",
   "tuning-waveforms=",
   "event-display="
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-d", "--parent-dir"):
        parent_dir = str(a)      
    elif o in ("-a", "--auto-dir"):
        auto_dir = str(a)      
    elif o in ("-w", "--web-dir"):
        output_dir = str(a)      
    elif o in ("-l", "--log-dir"):
        log_dir = str(a)      
    elif o in ("--user","--user"): # kludge to avoid -u (--user-tag) matching - exact matches only when comparing to a tuple
        user_name = str(a)      
    elif o in ("-u", "--user-tag"):
        user_tag = str(a)      
    elif o in ("--add-stat", "-s"):
        stat_list.append(str(a))
    elif o in ("--freq-veto"):
        freq_veto_file = str(a)
    elif o in ("-p", "--percentile"):
        percentile_str = str(a)
    elif o in ("-c", "--veto-method"):
        veto_method = str(a)      
    elif o in ("--priority"):
        condorPriority = str(a)
    elif o in ("--big-mem"):
        minimalMem = str(a)
    elif o in ("--tuning-waveforms"):
        tuning_waveforms = str(a)
    elif o in ("-e","--event-display"):
        event_display = int(a)
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not parent_dir:
    print("No parent-dir specified.", file=sys.stderr)
    print("Use --parent-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not auto_dir:
    print("No auto-dir specified.", file=sys.stderr)
    print("Use --auto-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not output_dir:
    print("No web-dir specified.", file=sys.stderr)
    print("Use --web-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not log_dir:
    print("No log-dir specified.", file=sys.stderr)
    print("Use --log-dir to specify it.", file=sys.stderr)
    sys.exit(1)
if not user_tag:
    print("No user-tag specified.", file=sys.stderr)
    print("Use --user-tag to specify it.", file=sys.stderr)
    sys.exit(1)
if not percentile_str:
    print("No percentile levels specified.", file=sys.stderr)
    print("Use --percentile to specify it.", file=sys.stderr)
if not veto_method:
    print("No veto-method specified.", file=sys.stderr)
    print("Use --veto-method to specify it.", file=sys.stderr)
    sys.exit(1)
if not(re.match('(medianAlpha|alphaLin|medianLin|linear|median|alpha)(Cut|CutCirc|CutAmp|CutScalar)',veto_method) or veto_method == 'None'):
    print("veto-method is currently set to " + veto_method, file=sys.stderr)
    print("veto-method must be of the form '(medianAlpha|alphaLin|medianLin|linear|median|alpha)(CutCirc|CutAmp|Cut)'", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#                 xbatchgrbwebpage                 #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("          parent dir       :", parent_dir, file=sys.stdout)
print("            auto dir       :", auto_dir, file=sys.stdout)
print("             web dir       :", output_dir, file=sys.stdout)
print("             log dir       :", log_dir, file=sys.stdout)
print("            user tag       :", user_tag, file=sys.stdout)
print("         veto method       :", veto_method, file=sys.stdout)
print("           stat list       :", stat_list, file=sys.stdout)
print("       event display       :", event_display, file=sys.stdout)
print("          percentile       :", percentile_str, file=sys.stdout)
if freq_veto_file:
    print(" frequency veto file       :", freq_veto_file, file=sys.stdout)
if condorPriority:
    print("            priority       :", condorPriority, file=sys.stdout)
if tuning_waveforms:
    print("    tuning waveforms       :", tuning_waveforms, file=sys.stdout)
if user_name:
    print("           user name       :", user_name, file=sys.stdout)
print(file=sys.stdout)

# ---- Ensure directory names end in '/'.
if not(parent_dir.endswith('/')):
    parent_dir = parent_dir + '/'
if not(auto_dir.endswith('/')):
    auto_dir = auto_dir + '/'
if not(output_dir.endswith('/')):
    output_dir = output_dir + '/'
if not(log_dir.endswith('/')):
    log_dir = log_dir + '/'

# ---- Record the current working directory so we can return here as needed.
initial_dir = os.getcwd();
if not(initial_dir.endswith('/')):
    initial_dir = initial_dir + '/'


#------------------------------------------------------------------------------
#     construct list of the GRBs which have been analysed in the parent_dir
#------------------------------------------------------------------------------

dirContents = os.listdir(parent_dir)
grbDirs = []

# ---- Loop through contents of parent_dir. If contents name begins with GRB, E, or MOCK
#      and is a directory then add it to our list of grbDirs. (The E**** naming convention
#      was adopted in O1.)
print('We will process the following directories:')  

for idx in range(len(dirContents)):
    if dirContents[idx].startswith('GRB') and os.path.isdir(parent_dir + dirContents[idx]):
        grbDirs.append(dirContents[idx])
        print(parent_dir + dirContents[idx])

for idx in range(len(dirContents)):
    if dirContents[idx].startswith('MOCK') and os.path.isdir(parent_dir + dirContents[idx]):
        grbDirs.append(dirContents[idx])
        print(parent_dir + dirContents[idx])

for idx in range(len(dirContents)):
    if dirContents[idx].startswith('E') and os.path.isdir(parent_dir + dirContents[idx]):
        grbDirs.append(dirContents[idx])
        print(parent_dir + dirContents[idx])


#------------------------------------------------------------------------------
#                     create output directory structure
#------------------------------------------------------------------------------

# ---- Check if run command is already present.
frun_name = 'run_xmakegrbwebpageDAGJob_' + user_tag + '_ALL.sh'
print(frun_name)
diplayRunScript = 1
if os.path.isfile(frun_name): 
    print('WARNING, file: ' + frun_name + ' already exists. The run command will be wrong')
    diplayRunScript = 0

# ---- Make the output directory, using the makedirs command which acts recursively making
#      intermediate directories as needed.
print('making output dir:' + output_dir)
try:
    os.makedirs(output_dir)
except OSError:
    print('error making output dir: ', OSError)  

# ---- cd into the output (web) directory.
os.chdir(output_dir)
output_dir_full = []

# ---- Make a sub-directory for each GRB.
for thisGRB in range(len(grbDirs)):
    output_dir_full.append(output_dir + grbDirs[thisGRB] + '_' + user_tag)
    print('making directory:' + output_dir_full[thisGRB])
    try:
        os.mkdir(output_dir_full[thisGRB])
    except OSError:
        print('output directory already exists, continuing...') 


#------------------------------------------------------------------------------
#                      launch xgrbwebpage.py for each GRB
#------------------------------------------------------------------------------

# ---- Create a list of additional statistics to pass to xgrbwebpage.py for tuning/testing.
statListStr = ''
for iStat in range(len(stat_list)):
    statListStr = statListStr + ' --add-stat ' + stat_list[iStat]

# ---- Return to the initial directory.
os.chdir(initial_dir)

# ---- Process each GRB with xgrbwebpage.py.
for thisGRB in range(len(grbDirs)):

    command = 'xgrbwebpage.py ' + \
    ' --grb-dir ' + parent_dir + grbDirs[thisGRB] + \
    ' --grb-name ' + grbDirs[thisGRB] + \
    ' --auto-dir ' + auto_dir + grbDirs[thisGRB] + \
    ' --web-dir ' +  output_dir + \
    ' --log-dir ' + log_dir + \
    ' --user-tag ' + user_tag + \
    ' --veto-method ' + veto_method + \
    ' --percentile ' + percentile_str + \
    statListStr + \
    ' --batch-mode ' 

    if condorPriority:
        command = command + ' --priority ' + condorPriority

    if minimalMem:
        command = command + ' --big-mem ' + minimalMem

    if tuning_waveforms:
        command = command + ' --tuning-waveforms ' +  tuning_waveforms

    if event_display: 
        command = command + ' --event-display 1'

    if freq_veto_file:
        command = command + ' --freq-veto ' +  freq_veto_file

    if user_name:
        command = command + ' --user ' +  user_name 

    print(command)  
    os.system(command)


#------------------------------------------------------------------------------
#       check scripts for user exist and print instructions to screen
#------------------------------------------------------------------------------

# ---- xgrbwebpage should have created scripts that can be used to launch
#      our DAG jobs 
frunall_name = 'run_xmakegrbwebpageDAGJob_' + user_tag + '_ALL.sh'
if not os.path.isfile(frunall_name):
    print("Error: xgrbwebpage failed to create file: ",frunall_name, file=sys.stderr)
    sys.exit(1)
os.system('chmod +x ' + frunall_name)

if diplayRunScript:
    print('Now run ' + frunall_name + ' to launch the xmakegrbwebpage condor jobs', file=sys.stdout)

print(file=sys.stdout)
print(" ... finished.", file=sys.stdout)

