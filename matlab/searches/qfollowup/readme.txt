
This is the python script that is being developed as part of the Xpipeline
follow up of Qpipeline triggers. It will run if you copy and paste the 
following:

./qfollowup.py --params-file qfollowup.ini --resolution 0.035 --detector H1 --detector H2 --detector L1 --trigger-file test_trigg.txt

./qfollowup.py --params-file qfollowup_LVIIb.ini --resolution 0.035 --detector H1 --detector V1 --detector L1 --trigger-file test_lv_trigg.txt
