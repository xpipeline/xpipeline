#!/usr/bin/env python

"""
Script for launching xpipeline triggered search jobs.
Patrick Sutton, adapted from time_slides.py by X. Siemens
$Id$
"""


# -------------------------------------------------------------------------
#      Setup.
# -------------------------------------------------------------------------

# ---- Import standard modules to the python path.
import sys, os, shutil, math, random, copy, getopt, re, string, time
import configparser
from glue import segments
from glue import pipeline
from glue.lal import CacheEntry
#from pylal.date import LIGOTimeGPS

__author__ = "Patrick Sutton <psutton@ligo.caltech.edu>"
__date__ = "$Date: 2007-11-20 11:12:04 -0800 (Tue, 20 Nov 2007) $"
__version__ = "$Revision: 1947 $"

# ---- Function usage.
def usage():
    msg = """\
Usage: 
  qfollowup.py [options]
  -p, --params-file <file>    parameters (.ini) file [REQUIRED]
  -f, --trigger-file <file>   Q pipeline trigger file [REQUIRED]
  -r, --resolution <ra>       desired angular resolution (rad) [REQUIRED]
  -i, --detector <ifo>        add detector to the network [REQUIRED]
  -m, --mdc-set <mdcs>        analyse MDC set <mdcs> [OPTIONAL]
  -s, --mdc-scale <scale>     scale MDC injections by factor <scale> [OPTIONAL]
  -g, --G1lag <lag>           time lag for G1 (sec) [OPTIONAL] 
  -x, --H1lag <lag>           time lag for H1 (sec) [OPTIONAL] 
  -y, --H2lag <lag>           time lag for H2 (sec) [OPTIONAL] 
  -l, --L1lag <lag>           time lag for L1 (sec) [OPTIONAL] 
  -v, --V1lag <lag>           time lag for V1 (sec) [OPTIONAL] 
  -h, --help                  display this message and exit
 
  Both --mdc-set and --mdc-scale must be specified together or not at all.
  Lags are asumed to be zero unless specified. 
"""
    print(msg, file=sys.stderr)


# -------------------------------------------------------------------------
#      Parse the command line options.
# -------------------------------------------------------------------------

# ---- Initialise command line argument variables.
params_file = None
trigger_file = None
res = None
detector = []
use_mdcs = 0;
mdc_set = None
mdc_scale = None
use_lags = 0;
g1_lag = 0;
h1_lag = 0;
h2_lag = 0;
l1_lag = 0;
v1_lag = 0;

# ---- Syntax of options, as required by getopt command.
# ---- Short form.
shortop = "hp:f:r:i:m:s:g:x:y:l:v:"
# ---- Long form.
longop = [
   "help",
   "params-file=",
   "trigger-file=",
   "resolution=",
   "detector=",
   "mdc-set=",
   "mdc-scale=",
   "G1lag",
   "H1lag",
   "H2lag",
   "L1lag",
   "V1lag"
   ]

# ---- Get command-line arguments.
try:
    opts, args = getopt.getopt(sys.argv[1:], shortop, longop)
except getopt.GetoptError:
    usage()
    sys.exit(1)

# ---- Parse command-line arguments.  Arguments are returned as strings, so 
#      convert type as necessary.
for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif o in ("-p", "--params-file"):
        params_file = a      
    elif o in ("-f", "--trigger-file"):
        trigger_file = a
    elif o in ("-r", "--resolution"):
        res = float(a)
    elif o in ("-m", "--mdc-set"):
        mdc_set = a
        use_mdcs = 1;
    elif o in ("-s", "--mdc-scale"):
        mdc_scale = float(a)
        use_mdcs = 1;
    elif o in ("-i", "--detector"):
        detector.append(a) 
    elif o in ("-g", "--G1lag"):
        g1_lag = float(a)
        use_lags = 1;
    elif o in ("-x", "--H1lag"):
        h1_lag = float(a)
        use_lags = 1;
    elif o in ("-y", "--H2lag"):
        h2_lag = float(a)
        use_lags = 1;
    elif o in ("-l", "--L1lag"):
        l1_lag = float(a)
        use_lags = 1;
    elif o in ("-v", "--V1lag"):
        v1_lag = float(a)
        use_lags = 1;
    else:
        print("Unknown option:", o, file=sys.stderr)
        usage()
        sys.exit(1)

# ---- Check that all required arguments are specified, else exit.
if not params_file:
    print("No parameter file specified.", file=sys.stderr)
    print("Use --params-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not trigger_file:
    print("No trigger file specified.", file=sys.stderr)
    print("Use --trigger-file to specify it.", file=sys.stderr)
    sys.exit(1)
if not res:
    print("No angular resolution specified.", file=sys.stderr)
    print("Use --resolution to specify it.", file=sys.stderr)
    sys.exit(1)
if not detector:
    print("No detectors specified.", file=sys.stderr)
    print("Use --detector to specify each detector in the network.", file=sys.stderr)
    sys.exit(1)
if (not mdc_set) and mdc_scale :
    print("No MDC set specified.", file=sys.stderr)
    print("An MDC set must be specified when --mdc-scale is used.", file=sys.stderr)
    print("Use --mdc-set to specify it.", file=sys.stderr)
    sys.exit(1)
if mdc_set and (not mdc_scale) :
    print("No MDC scale specified.", file=sys.stderr)
    print("An MDC scale must be specified when --mdc-set is used.", file=sys.stderr)
    print("Use --mdc-scale to specify it.", file=sys.stderr)
    sys.exit(1)

# ---- Status message.  Report all supplied arguments.
print(file=sys.stdout)
print("WARNING: The on- and off-source DAGs write to the same log file.  This should be fixed.", file=sys.stdout)
print(file=sys.stdout)
print("####################################################", file=sys.stdout)
print("#           X-Followup Search Pipeline             #", file=sys.stdout)
print("####################################################", file=sys.stdout)
print(file=sys.stdout)
print("Parsed input arguments:", file=sys.stdout)
print(file=sys.stdout)
print("     parameters file:", params_file, file=sys.stdout)
print("        trigger file:", trigger_file, file=sys.stdout)   
print("  angular resolution:", res, file=sys.stdout)
print("    detector network:", detector, file=sys.stdout) 
print("             MDC set:", mdc_set, file=sys.stdout)
print("           MDC scale:", mdc_scale, file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Preparatory.
# -------------------------------------------------------------------------

# ---- Generate unique id tag.
os.system('uuidgen > uuidtag.txt')
f = open('uuidtag.txt','r')
uuidtag=f.read()
f.close()

# ---- Record the current working directory in a string.
cwdstr = os.getcwd( )

# ---- X-Pipeline installation directory.
XPIPELINE_ROOT = os.getenv('XPIPELINE_ROOT')

# ---- Make directory to store output text files (segment lists, parameter 
#      files, etc.) to minimize clutter in the working directory.
try: os.mkdir( 'input' )
except: pass  # -- Kludge: should probably fail with error message.


# -------------------------------------------------------------------------
#      Read configuration file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Parsing parameters (ini) file ...", file=sys.stdout)   

# ---- Create configuration-file-parser object
cp = configparser.ConfigParser()
cp.read(params_file)

# ---- Parrot info on the version of the parameters file.
if cp.has_option('tags','version') :
    ini_version = cp.get('tags','version')
    print("Parameter file CVS tag:", ini_version, file=sys.stdout)

# ---- NOTE: The following reading of variables can be split up and
#      moved into the relevant sections of the script where the
#      variables are actually used.

# ----  Read needed variables from [parameters] and [background] sections.
blockTime =  int(cp.get('parameters','blockTime'))
whiteningTime =  float(cp.get('parameters','whiteningTime'))
transientTime =  4 * whiteningTime

# ---- Variables that may or may not be defined in .ini file:
try:
    frameCache = cp.get('input','frameCacheFile')
    frameCacheFlag = 1
except:
    print("Warning: No frameCache file specified in configuration file.", file=sys.stdout)
    print("         A frameCache file will be generated automatically.", file=sys.stdout)
    frameCache = None
    frameCacheFlag = 0
#try:
#    lagFile =  cp.get('background','lagFile')
#except:
#    print >> sys.stdout, "Warning: No lagFile specified in configuration file."
#    print >> sys.stdout, "         No time lag jobs will be made."
#    lagFile =  None

# ---- Status message.
print("... finished parsing parameters (ini) file.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Determine network.
# -------------------------------------------------------------------------

# ---- Status message.
print("Comparing requested network to list of known detectors ...", file=sys.stdout)

# ---- Read [input] channel parameters. 
detectorListLine = cp.get('input','detectorList')
detectorList = detectorListLine.split(',')
channelListLine = cp.get('input','channelList')
channelList = channelListLine.split(',')
frameTypeListLine = cp.get('input','frameTypeList')
frameTypeList = frameTypeListLine.split(',')

# ---- For each detector specified with the --detector option, compare to 
#      the list of known detectors from the .ini file.  Keep only the requested
#      detectors and corresponding channel name and frame type.
#      KLUDGE:TODO: Should exit with error message if any of the detectors 
#      is not recognized.
# ---- Indices of the detectors requested for this analysis.
keepIndex = [] 
for i in range(0,len(detector)) :
    for ii in range(0,len(detectorList)) :
        if detector[i] == detectorList[ii] :
            keepIndex.append(ii)
# ---- Sort indices so that order matches that used in ini file.
keepIndex.sort()
# ---- We now have a list of the indices of the detectors requested for the 
#      analysis.  Keep only these.  Note that we over-write the 'detector'
#      list because we want to make sure the order matches the channel and 
#      frameType lists.
detector = []
channel = []
frameType = []
for jj in range(0,len(keepIndex)):
    detector.append(detectorList[keepIndex[jj]])
    channel.append(channelList[keepIndex[jj]])
    frameType.append(frameTypeList[keepIndex[jj]])

# ---- Set time lags.
timeLag = [];
for ifo in detector :
    if ifo == 'G1' :
        timeLag.append(g1_lag)
    elif ifo == 'H1' :
        timeLag.append(h1_lag)
    elif ifo == 'H2' :
        timeLag.append(h2_lag)
    elif ifo == 'L1' :
        timeLag.append(l1_lag)
    elif ifo == 'V1' :
        timeLag.append(v1_lag)
    else :
        timeLag.append(0)
timeLagString = '';
timeLagTag = '';
for ii in range(len(detector)) :
    timeLagString = timeLagString + ' ' + str(timeLag[ii]) 
    timeLagTag = timeLagTag + '_' + str(timeLag[ii]) 
print("Time lags:", timeLagString, file=sys.stdout)

# ---- Status message.
print("... finished validating network.          ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      MDC run or not?
# -------------------------------------------------------------------------

# ---- Current qfollow.py is designed to handle two possible scenarios:
#      follow-up of triggers from an MDC run at zero lag, or follow-up 
#      of triggers from a non-MDC run with time shifts (which may be zero).
#      To keep dgs, etc., generated by separate runs of qfollowup.py from
#      clashing, all of the important files produced are tagged by a string 
#      which should be unique to ech run.  For MDC runs, this tag specifies
#      the MDC set and the injection scale.  For non-MDC runs, it specifies
#      the time lags used.
if mdc_set :
    fileTag = '_' + mdc_set + '_' + str(mdc_scale)
else :
    fileTag = timeLagTag
print("Tag for output files:", fileTag, file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Determine MDCs to process, write mdcChannelFiles.
# -------------------------------------------------------------------------

# ---- Check for MDC set.
if mdc_set :

    # ---- Status message.
    print("Writing Matlab-formatted MDC channel files ... ", file=sys.stdout)

    # ---- Make MDC channels file. 
    if cp.has_section(mdc_set) :
        # ---- Read channel parameters for this mdc set.
        mdcChannelListLine = cp.get(mdc_set,'channelList')
        mdcChannelList = mdcChannelListLine.split(',')
        mdcFrameTypeListLine = cp.get(mdc_set,'frameTypeList')
        mdcFrameTypeList = mdcFrameTypeListLine.split(',')
        # ---- Keep only info for detectors requested for the analysis. 
        mdcChannel = []
        mdcFrameType = []
        for jj in range(0,len(keepIndex)):
            mdcChannel.append(mdcChannelList[keepIndex[jj]])
            mdcFrameType.append(mdcFrameTypeList[keepIndex[jj]])
        # ---- For each detector, write the 
        #      corresponding channel name and frame type to a file.
        f=open('input/channels_' + mdc_set + '.txt', 'w')
        for i in range(0,len(detector)) :
            f.write(detector[i] + ':' + mdcChannel[i] + ' ' + mdcFrameType[i] + '\n')
        f.close()
    else :
        print("ERROR: MDC set ", mdc_set, " is not defined in the parameters file.  Exiting.", file=sys.stdout)
        print(file=sys.stdout)
        sys.exit(28)

    # ---- Status message.
    print("... finished writing MDC channel files.   ", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Write channel file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing Matlab-formatted channel file ...      ", file=sys.stdout)

# ---- For each detector, write the 
#      corresponding channel name and frame type to a file.
f=open('input/channels.txt', 'w')
for i in range(0,len(detector)) :
    f.write(detector[i] + ':' + channel[i] + ' ' + frameType[i] + '\n')
f.close()

# ---- Status message.
print("... finished writing channel file.        ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Write Matlab-formatted parameters file.
# -------------------------------------------------------------------------

# ---- We can write two sets of parameters files:  one on-source file,
#      and (for each MDC waveform set and injection scale)
#      one injections file.

# ---- Status message.
print("Writing Matlab-formatted parameter file ...", file=sys.stdout)

# ---- If frameCache is not defined, then we'll make one below, so fill 
#      in appropriate name in parameters files.
if not frameCacheFlag:
    frameCache_string = 'input/framecache.txt' 
else:
    frameCache_string = frameCache

# ---- Write all options available in the parameters section to a file.
parameters = cp.options('parameters')

# ---- Write parameter file for MDC waveform analyses or non-MDC analysis, 
#      as desired.
f = open('input/parameters' + fileTag + '.txt', 'w')
f.write('eventFileName:input/event' + fileTag + '.txt' + '\n')
if mdc_set :
    #f=open('input/parameters_' + mdc_set + '_' + str(mdc_scale) + '.txt', 'w')
    #f.write('eventFileName:input/event_' + mdc_set + '_' + str(mdc_scale) +'.txt' + '\n')
    # ---- Write mdc info.
    f.write('mdcChannelFileName:input/channels_' + mdc_set + '.txt' + '\n') 
    f.write('injectionScale:' + str(mdc_scale) + '\n')
#else :
#    f=open('input/parameters_' + timeLagTag + '.txt', 'w')
#    f.write('eventFileName:input/event_' + timeLagTag + '.txt' + '\n')
# ---- First write framecache file, channel file, event file, and sky position. 
f.write('channelFileName:input/channels.txt' + '\n')
f.write('frameCacheFile:' + frameCache_string + '\n')
f.write('skyPositionList:[' + str(res) + ']' + '\n')
f.write('skyCoordinateSystem:earthfixed' + '\n')
# ---- Now write all of the other parameters from the parameters section.
for i in range(0,len(parameters)) :
    value = cp.get('parameters',parameters[i])
    f.write(parameters[i] + ':' + value + '\n')
f.close()

# ---- Status message.
print("... finished writing parameter file.     ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Parse Q pipeline trigger file to get trigger times.
# -------------------------------------------------------------------------

print("Parsing trigger file:", trigger_file, file=sys.stdout)
fq = open(trigger_file,'r')
triggers = fq.readlines()
num_triggers = len(triggers)
print("Number of triggers read:", num_triggers, file=sys.stdout)

# ---- Get times of first and last triggers; will be used for compiling 
#      segment list and finding frames.  (Add buffer of blockTime + max 
#      abs(timeLag) to edges.) 
maxTimeLag = 0
if use_lags :
    for ii in range(len(timeLag)) :
        if abs(timeLag[ii]) > maxTimeLag :
            maxTimeLag = abs(timeLag[ii])
buffer = blockTime + maxTimeLag
first_trigger = triggers[0].split(" ")
#print >> sys.stdout, "first trigger (unsplit):", triggers[0]
#print >> sys.stdout, "first trigger:", first_trigger
#first_trigger = triggers[0].split("	") #LV triggers delimited by tab
trigger_time = float(first_trigger[0])
start_time = int(trigger_time - buffer)
last_trigger = triggers[num_triggers-1].split(" ")
#print >> sys.stdout, "last trigger (unsplit):", triggers[num_triggers-1]
#print >> sys.stdout, "last trigger:", last_trigger
trigger_time = float(last_trigger[0])
end_time = int(trigger_time + buffer)
print("Time range for analysis: [", start_time, ",", end_time, "]", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Find frames, if necessary.
# -------------------------------------------------------------------------

# ---- If frameCache is not defined, then call LSCdataFind for each 
#      detector, and convert to readframedata-formatted framecache file.
if not frameCacheFlag:
    # ---- Status message.
    print("Writing framecache file ...", file=sys.stdout)
    print("WARNING: framecache generation for MDC frames not yet supported.", file=sys.stdout)
    # ---- Clear away any pre-existing framecache files.
    os.system('rm -f framecache_temp.txt')
    # ---- Get LSCdataFind server from parameters file.
    datafind_server = cp.get('datafind','server')
    # ---- Loop over detectors.
    for i in range(0,len(detector)):
        # ---- Construct LSCdataFind command.
        LSCdataFindCommand = "LSCdataFind " \
        + " --server " + datafind_server \
        + " --observatory " + detector[i][0] \
        + " --type " + frameType[i] \
        + " --gps-start-time " + str(start_time)  \
        + " --gps-end-time " + str(end_time)    \
        + " --url-type file --lal-cache > lalcache.txt"
        # ---- Issue LSCdataFind command.
        print("calling LSCdataFind:", LSCdataFindCommand)
        os.system(LSCdataFindCommand)
        print("... finished call to LSCdataFind.")
        # ---- Convert lalframecache file to readframedata format.
        print("calling convertlalcache:")
        os.system('convertlalcache.pl lalcache.txt framecache_temp.txt')
        os.system('cat framecache_temp.txt >> input/framecache.txt')
        print("... finished call to convertlalcache.")
    # ---- Clean up.
    os.system('rm -f framecache_temp.txt lalcache.txt')
    # ---- Set frameCache variable to point to our new file.
    frameCache = 'input/framecache.txt'
    # ---- Status message.
    print("... finished writing framecache file.", file=sys.stdout)
    print(file=sys.stdout)


# -------------------------------------------------------------------------
#    Generate segment lists, if necessary.
# -------------------------------------------------------------------------

# ---- Status message.
print("Retrieving segment lists ... ", file=sys.stdout)

# ---- Generate a segment list for full analysis period from first to last 
#      trigger.
full_segment_list = []
for ifo in detector:

    # ---- Check for segment list for this detector.
    segmentListFile = None
    if cp.has_option(ifo,'segmentListFile') :
        segmentListFile = cp.get(ifo,'segmentListFile')
        print("    Found pre-existing segment list for", ifo, file=sys.stdout)

    if not segmentListFile :
        # ---- Use segwizard or LSCsegFind to get segment list for this detector.
        # ---- Get data quality flags from parameter file.
        datafind_server = cp.get('datafind','server')
        if cp.has_option(ifo,'dqflags') :
            dqflags = cp.get(ifo,'dqflags')
        # ---- Use segwizard to get full segment list for this detector (entire run).
        # print 'calling: segwizard ' + run + ' ' + ifo + ' ' + dqflags + ' > .segments_all.txt'
        # os.system('segwizard ' + run + ' ' + ifo + ' ' + dqflags + ' > .segments_all.txt')
        # print '... finished segwizard call.'
        # ---- Use LSCsegFind to get segment list for this detector over analysis
        #      interval.
        print('calling: LSCsegFind ' +  ' --server=' + datafind_server + ' -i '+ ifo + ' -t ' + dqflags + ' -s %d'%(start_time) + ' -e %d'%(end_time) + ' -o segwizard > segments_all_' + ifo + '.txt', file=sys.stdout)
        os.system('LSCsegFind ' +  ' --server=' + datafind_server + ' -i '+ ifo + ' -t ' + dqflags + ' -s %d'%(start_time) + ' -e %d'%(end_time) + ' -o segwizard > segments_all' + ifo + '.txt')
        print('... finished LSCsegFind call.', file=sys.stdout)
        # ---- Read full segment list into a ScienceData object for easy manipulation.
        #      Throw away science segment shorter than the blockTime value read from
        #      the configuration file.
        full_segment = pipeline.ScienceData()
        full_segment.read( 'segments_all' + ifo + '.txt', blockTime )
        # print 'length of full_segment:', len(full_segment)
    else :
        # ---- Read full segment list into a ScienceData object for easy manipulation.
        #      Throw away science segment shorter than the blockTime value read from
        #      the configuration file.
        print('    Reading segment list from file ...', file=sys.stdout)
        full_segment = pipeline.ScienceData()
        full_segment.read( segmentListFile, blockTime )
        print('    ... finished reading segment list.', file=sys.stdout)
    # ---- Add segment list for this detector to full network list.
    full_segment_list.append(full_segment)
    # ---- Dump all segments into file to be reduced around analysis time later.
    #f=open('input/segment_full.txt', 'a')
    #for seg in full_segment:
    #    f.write('1 ' + str(seg.start()) + ' ' + str(seg.end()) + ' ' + str(seg.end()-seg.start()) + '\n')
    #f.close()

# ---- Apply time lags to the segment lists.
for i in range(len(detector)):
    for seg in full_segment_list[i]:
        seg.set_start(seg.start()-int(timeLag[i]))  # -- SUBTRACT lag
        seg.set_end(seg.end()-int(timeLag[i]))  # -- SUBTRACT lag

# ---- Status message.
print("... finished retrieving segment lists.", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Write events file.
# -------------------------------------------------------------------------

# ---- Status message.
print("Writing event file ...          ", file=sys.stdout)

# ---- Make event list and segment list files for the zero-lag times.
fevent = open('input/event' + fileTag + '.txt', 'a')
fseg = open('input/segment' + fileTag + '.txt', 'a')

# ---- Now need to cycle through triggers and parse columns.
for j in range(0, num_triggers) :

    # ---- Format for Q-Pipeline triggers.
    trigger = triggers[j].split(" ")
    #trigger_time = float(trigger[0])
    trigger_time = round(float(trigger[0]))
    #trigger_dur = float(trigger[1])
    #trigger_freq = float(trigger[2])
    #trigger_bw = float(trigger[3])
    # ---- Format for LV triggers.
    #trigger = triggers[j].split("	") # LV triggers delimited by tab
    #trigger_time = float(trigger[2]) # t_max is third column


    # -------------------------------------------------------------------------
    #      Verify that this trigger is in a coincdence segment. 
    # -------------------------------------------------------------------------

    # ---- Interval of data to be analysed for this trigger.
    on_source_start_time = int(trigger_time - blockTime / 2)
    on_source_end_time = int(trigger_time + blockTime / 2)
    duration = int(blockTime)

    # ---- Write time range to a temporary segment file named gps_range.txt.
    #      We'll then read this file into a ScienceData object.  It's a hack, but
    #      it seems that the only way to populate ScienceData objects is to read
    #      segments from a file.
    f=open('input/gps_range.txt', 'w')
    time_range_string = '1 ' + str(on_source_start_time) + ' ' + str(on_source_end_time) + ' ' + str(duration) + '\n'
    f.write(time_range_string)
    f.close()
    # ---- Read full analysis time range back in to a ScienceData object for easy manipulation.
    analysis_segment = pipeline.ScienceData()
    analysis_segment.read( 'input/gps_range.txt', blockTime )

    # ---- Verify that analysis_segment is a coincidence segment for all detectors.
    coincidence_segment = copy.deepcopy(analysis_segment)
    for det in full_segment_list:
        coincidence_segment.intersection(det)

    # ---- If the analysis interval is a coincidence segment, then its intersection
    #      with the "not" coincidence should be empty.
    not_coincidence_segment = copy.deepcopy(coincidence_segment)
    not_coincidence_segment.invert()
    overlap = not_coincidence_segment.intersection(analysis_segment)
    if overlap != 0 :
        print("Error: analysis period for trigger", j, "is not a coincidence segment of the specified network.")
    else :
        print("Adding trigger", j, "to the event list.")
        # ---- Write event to event file, including lags.
        time_range_string = str(on_source_start_time + duration/2) + timeLagString + '\n'
        fevent.write(time_range_string)
        # ---- Write segment to segment file.
        time_range_string = str(j) + ' ' + str(on_source_start_time) + ' ' + str(on_source_end_time) + ' ' + str(duration) + '\n'
        fseg.write(time_range_string)

# ----- Close events file and clean up.
f.close()
fseg.close()
os.system('rm input/gps_range.txt')
print("... finished writing event file.          ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Define special job class.
# -------------------------------------------------------------------------

class XsearchJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    An x search job
    """
    def __init__(self,cp):
        """
        cp = ConfigParser object from which options are read.
        """
        # ---- Get path to executable from parameters file.
        # self.__executable = cp.get('condor','xsearch')
        self.__executable = XPIPELINE_ROOT + "/xdetection.sh"
        # ---- Get condor universe from parameters file.
        self.__universe = cp.get('condor','universe')
        pipeline.CondorDAGJob.__init__(self,self.__universe,self.__executable)
        pipeline.AnalysisJob.__init__(self,cp)
        self.__param_file = None

        # ---- Add required environment variables.
        # self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME)")
        self.add_condor_cmd('environment',"USER=$ENV(USER);HOME=$ENV(HOME);XPIPELINE_ROOT=$ENV(XPIPELINE_ROOT);XPIPELINE_MATLAB_ROOT=$ENV(XPIPELINE_MATLAB_ROOT)")

        # ---- Path and file names for standard out, standard error for this job.
        self.set_stdout_file('logs/xsearch-$(cluster)-$(process).out')
        self.set_stderr_file('logs/xsearch-$(cluster)-$(process).err')

        # ---- Name of condor job submission file to be written.
        self.set_sub_file('xsearch.sub')

    # ---- All XsearchJob nodes use a common parameters file.  Set it here.
    # def set_param_file(self,path):
    #   self.add_arg(path)
    #   self.__param_file = path

    # def get_param_file(self):
    #   return self.__param_file

class XsearchNode(pipeline.CondorDAGNode, pipeline.AnalysisNode):
    """
    xseach node
    """
    def __init__(self,job):
        """
        job = A CondorDAGJob that can run an instance of lalapps_tmpltbank.
        """
        pipeline.CondorDAGNode.__init__(self,job)
        pipeline.AnalysisNode.__init__(self)
        self.__x_jobnum = None
        self.__x_injnum = None

    # ---- Set parameters file.
    def set_param_file(self,path):
        self.add_var_arg(path)
        self.__param_file = path

    def get_param_file(self):
        return self.__param_file

    def set_x_jobnum(self,n):
        self.add_var_arg(str(n))
        self.__x_jobnum = n

    def get_x_jobnum(self):
        return self.__x_jobnum

    def set_output_dir(self,path):
        self.add_var_arg(path)
        self.__output_dir = path

    def get_output_dir(self,path):
        return self.__output_dir

    def set_x_injnum(self,n):
        self.add_var_arg(n)
        self.__x_injnum = n

    def get_x_injnum(self):
        return self.__x_injnum


# -------------------------------------------------------------------------
#    Preparations for writing dag.
# -------------------------------------------------------------------------

# ---- Note on DAGman log file:
#      The path to the log file for condor log messages. DAGman reads this
#      file to find the state of the condor jobs that it is watching. It
#      must be on a local file system (not in your home directory) as file
#      locking does not work on a network file system.

# ---- Status message.
print("Writing job submission files ... ", file=sys.stdout)

# ---- Retrieve job retry number from parameters file.
if cp.has_option('condor','retryNumber'):
    retryNumber = int(cp.get('condor','retryNumber'))
else :
    retryNumber = 0

# ---- Set up output directories and DAG log file name.
# ---- Make parent directory to store the output of the jobs.
try: os.mkdir( 'output' )
except: pass
if mdc_set :
    log_file = cp.get('condor','dagman_log_mdcs')
    #try: os.mkdir( 'output/simulations_' + mdc_set + '_' + str(mdc_scale) )
    try: os.mkdir( 'output/simulations' + fileTag )
    except: pass
else :
    log_file = cp.get('condor','dagman_log_on_source')
    #try: os.mkdir( 'output/on_source' )
    try: os.mkdir( 'output/on_source' + fileTag )
    except: pass
# ---- Make directories to store the log files and error messages 
#      from the nodes in the DAG
try: os.mkdir( 'logs' )
except: pass


# -------------------------------------------------------------------------
#      Write dag.
# -------------------------------------------------------------------------

# ---- Create a dag to which we can add jobs.
dag = pipeline.CondorDAG(log_file + uuidtag)

# ---- Set the root name of the file that will contain the DAG.
#if mdc_set :
#    dag.set_dag_file( 'qfollowup_' + mdc_set + '_' + str(mdc_scale) )
#else : 
#    dag.set_dag_file( 'qfollowup' )
dag.set_dag_file( 'qfollowup' + fileTag )

# ---- Make instance of XsearchJob.
job = XsearchJob(cp)

# ---- Load on-source segments.
segmentList = pipeline.ScienceData()
segmentList.read( 'input/segment' + fileTag + '.txt' , blockTime )
print("Number of events to process:", len(segmentList), file=sys.stdout)

# ---- Add one node to the job for each segment to be analysed.
for i in range(len(segmentList)):
    # ---- Create job node.
    node = XsearchNode(job)
    # ---- Assign first argument: parameters file
    matlab_param_file = cwdstr + '/input/parameters' + fileTag + '.txt'
    node.set_param_file(matlab_param_file)
    # ---- Assign second argument: job (segment) number 
    node.set_x_jobnum(i)
    if mdc_set :
        # ---- Assign first argument: parameters file
        #matlab_param_file = cwdstr + '/input/parameters_' + mdc_set + '_' + str(mdc_scale) + '.txt'
        #node.set_param_file(matlab_param_file)
        # ---- Assign second argument: job (segment) number 
        #node.set_x_jobnum(i)
        # ---- Assign third argument: output directory
        node.set_output_dir( os.path.join( cwdstr + '/output/simulations' + fileTag ) )
    else :
        # ---- Assign first argument: parameters file
        #matlab_param_file = cwdstr + '/input/parameters_on_source.txt'
        #node.set_param_file(matlab_param_file)
        # ---- Assign second argument: job (segment) number 
        #node.set_x_jobnum(i)
        # ---- Assign third argument: output directory
        #node.set_output_dir( os.path.join( cwdstr + '/output/on_source' ) )
        node.set_output_dir( os.path.join( cwdstr + '/output/on_source' + fileTag ) )
    # ---- Assign fourth argument: injection number 
    node.set_x_injnum('0')
    node.set_retry(retryNumber)
    dag.add_node(node)

# ---- Write out the submit files needed by condor.
dag.write_sub_files()
# ---- Write out the DAG itself.
dag.write_dag()
# ---- Delete used dag job
del dag
del job

# ---- Status message.
print("... finished writing job submission files. ", file=sys.stdout)
print(file=sys.stdout)


# -------------------------------------------------------------------------
#      Finished.
# -------------------------------------------------------------------------

print("############################################", file=sys.stdout)
print("#           Completed.                     #", file=sys.stdout)
print("############################################", file=sys.stdout)


# ---- Exit cleanly
sys.exit( 0 )
