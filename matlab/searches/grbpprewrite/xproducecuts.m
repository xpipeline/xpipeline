function [] = xproducecuts(...
    autoFileName, vetoMethod, detectionStatTilde,...
    H1vetoSegList, L1vetoSegList, V1vetoSegList, ...
    FAR_Tuning,FAR_UL,analysisType,user_tag,grb_name,ULalpha,ULlinear)

% XPRODUCECUTS - Output UL Injection Curves and other information for
% webpage.
% See xapplyratiocuts.m xapplyratiocutsinjections.m
%     xapplyalphacuts.m xapplyalphacutsinjections.m
%     xapplyalphacutsonsource.m
%
% usage:
%
% [ULInjectionCurves] = xmakegrbwebpage(
%     autoFileName, vetoMethod, ...
%      H1vetoSegList, L1vetoSegList, V1vetoSegList, ...
%     FAR_Tuning,FAR_UL)
%
% autoFileName        String. Name of file listing results files, as
%                     produced by grb/makeAutoFiles2.py.
%
% vetoMethod          String. Determines what type of veto ratio
%                     test we will perform. Must be of the form
%                     '(alphaLin|linear|alpha)
%                       (CutScalar|CutCirc|CutAmp|Cut)'
%                     or 'None'
%
% H1vetoSegList ... V1vetoSegList
%                     Strings. Contain paths to veto segment lists, these
%                     should be ordered H1,L1,V1. We assume segment
%                     lists are in segwizard format and contain lists of
%                     times KILLED by the appropriate veto flags. Use
%                     'None' if you do not want to apply veto segs for a
%                     particular ifo. 
%
% detectionStatCell   Tilde sepearted string of detection/ranking statistics you
%		     Want to loop over.
%
%
% FAR_(UL/Tuning)    Inputs are integer or fractional values such as 1 or
%                    0.5 which would imply a False alarm probability 
%                    of 1 in 100 and 1 in 200 respectively.
%
% AnalysisType    Open or Closedbox.
%                  
%                    
%
%
% $Id: xmakegrbwebpage.m 4832 2015-04-13 13:05:05Z scott.coughlin@LIGO.ORG $

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. Assign default values 
%      where needed.
narginchk(11,13);
fprintf(1,'Checking input arguments are valid ...');

% ---- Check that vetoMethod is correctly defined.
if isempty(regexp(vetoMethod,'(alphaLin|linear|alpha)(CutScalar|CutCirc|CutAmp|Cut)', 'once')) && ...
        not(strcmp(vetoMethod,'None'))
   error('vetoMethod must be either (alphaLin|linear|alpha)(CutScalar|CutCirc|CutAmp|Cut) or None')
end

if ischar(FAR_Tuning) && ischar(FAR_UL)
    FAR_Tuning = str2num(FAR_Tuning);
    FAR_UL     = str2num(FAR_UL);
else
    error('How?')
end

if nargin<13
	ULalpha=0.5;
	ULlinear=0.95;
else
        ULalpha=str2num(ULalpha);
        ULlinear=str2num(ULlinear);
end
% ---- Pack veto files into a single convenient cell array.
vetoSegList = {H1vetoSegList L1vetoSegList V1vetoSegList};
[detectionStatCell] = tildedelimstr2numorcell(detectionStatTilde);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Detemine the correct on source window                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

analysis.onSourceBeginOffset = [];
analysis.onSourceEndOffset = [];
%----- Read each line into separate element of a cell array. 
parameterFileName='../input/parameters_off_source_0.txt';
display(['Reading parameters from ' parameterFileName])
paramLines = textread(parameterFileName,'%s','delimiter','\n','commentstyle','shell');

%----- Number of parameters read.
nParams = length(paramLines);

%----- Loop over parameters.
for iParam=1:nParams,

    %----- Split line into name: value strings
    [paramName,paramValStr] = dataread('string',paramLines{iParam}, ...
                                      '%s%s','delimiter',':');

    %----- Parse name: value strings and assign parameter values.
    switch lower(paramName{1})
        case 'onsourcebeginoffset',
         analysis.onSourceBeginOffset = str2double(paramValStr{1});
        case 'onsourceendoffset',
         analysis.onSourceEndOffset = str2double(paramValStr{1});
        otherwise,
                    
    end;

end;
% --- Override defaults on source window if parameters found in file
window.offset = -analysis.onSourceBeginOffset;
window.duration = analysis.onSourceEndOffset - analysis.onSourceBeginOffset;
clear paramName paramValStr parameterFileName nParams paramLines iParam

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Parse auto.txt file to find on-source, off-source and injection files   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Read and parse the file listing all of the results files to be 
%      processed.
fauto = fopen(autoFileName,'r');
offSourceMatFile = fscanf(fauto,'%s',1);
onSourceMatFile = fscanf(fauto,'%s',1);
nWaveforms = fscanf(fauto,'%d',1);
for i=1:nWaveforms
    autoFilesCell{i} = fscanf(fauto,'%s',1);
end
for i=1:nWaveforms
    injectionFilesCell{i} = fscanf(fauto,'%s',1);
end
% ---- Find duration of offSource region 
backgroundPeriod = fscanf(fauto,'%d',1);
catalogDir = fscanf(fauto,'%s',1);
fclose(fauto);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Load on-source                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(1,['Loading on source results files from ' onSourceMatFile  ' ...']);
% ---- Load on-source events and analysis information.
load(onSourceMatFile,'amplitudeSpectraCell','analysisTimesCell', ...
    'blockTime','cluster','detectorList', ...
    'gaussianityCell','likelihoodType', ...
    'maximumFrequency','minimumFrequency','svnversion_xdetection', ...
    'sampleFrequency','skyPositions','skyPositionsTimes','transientTime');
% ---- Pack analysis parameters and information into convenient structure.
analysis.amplitudeSpectraCell  = amplitudeSpectraCell;
analysis.analysisTimes         = cell2mat(analysisTimesCell); 
analysis.blockTime             = blockTime;
analysis.clusterType           = 'connected';
analysis.detectorList          = detectorList;
analysis.gaussianityCell       = gaussianityCell;
analysis.likelihoodType        = likelihoodType;
analysis.maximumFrequency      = maximumFrequency;
analysis.minimumFrequency      = minimumFrequency;
analysis.sampleFrequency       = sampleFrequency;
analysis.skyPositions          = skyPositions;
analysis.skyPositionsTimes     = skyPositionsTimes;
analysis.transientTime         = transientTime;
analysis.svnversion_xdetection = svnversion_xdetection;
analysis.vetoMethod            = vetoMethod;
analysis.type                  = analysisType;
analysis.grb_name              = grb_name;
analysis.offSourceMatFile      = offSourceMatFile;
analysis.onSourceMatFile       = onSourceMatFile;

% ---- Clean up.
clear amplitudeSpectraCell analysisTimesCell blockTime detectorList
clear gaussianityCell likelihoodType maximumFrequency minimumFrequency
clear sampleFrequency skyPositions 
clear transientTime svnversion_xdetection

% ---- Set on source and on source window durations
analysis.onSourceTimeLength = analysis.onSourceEndOffset - analysis.onSourceBeginOffset;
analysis.jobsPerWindow=ceil(analysis.onSourceTimeLength/(analysis.blockTime-2* ...
                                                  analysis.transientTime));
analysis.onSourceWindowLength=2*analysis.transientTime+analysis.jobsPerWindow*(analysis.blockTime-2* ...
                                                  analysis.transientTime);

% ---- Rename on-source clusters struct, add some additional information.
onSource = cluster;
% ---- Each event will have a pass flag, pass = 1 (0) indicates that the 
%      event has survived (failed) the veto cuts which have been applied. 
onSource.pass         = ones(size(cluster.significance));
% ---- Clean up.
clear cluster 
fprintf(1,'done! \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Find time GRB and do some sanity checking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- GRB on source center time is set to the trigger time by xmerge
analysis.gpsCenterTime = unique(onSource.centerTime);

% ---- Check that there is only one on source center time
if (length(analysis.gpsCenterTime) ~= 1)
  analysis.gpsCenterTime
  error('There is not only one on source center time, aborting')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Get some network info and initial random seeds             %  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[analysis.FpList,analysis.FcList,analysis.dF,~,~,~,~,~]=...
        xnetworkinfo(analysis);
% ---- set rand seed used by randperm to matlab default
%      seed will be different for each GRB but will be the same for
%      each GRB when we run it again and again
%      Information is used to pick tuning and UL injections and off source
%      jobs
rand('seed',931316785+analysis.gpsCenterTime);
initialSeed = rand('seed');
disp(' ');
%disp(['initial random seed: ' int2str(initialSeed)]);
disp(['initial random seed: '  sprintf(['%', '.0f'], initialSeed)]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Load off-source                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(1,['Loading off source results files from ' offSourceMatFile  ' ...']);
% ---- Load off-source events for background estimation and tuning.
load(offSourceMatFile,'cluster');
% ---- Pack off-source event info into convenient structure.
offSource = cluster;
offSource.pass         = ones(size(cluster.significance));
% ---- Info needed later for likelihood rate histograms.
nJobFilesProcessed     = length(unique(offSource.jobNumber));
totalTimeOn            = window.duration;
%totalTimeOff           = nJobFilesProcessed*totalTimeOn;
% % ---- Significance bins for histrograms.  Set here so that the range
% %      covers up to the loudest background event before any cuts.
% binArray = exp([ 1:0.01:log(max(offSource.significance)) inf]);
% ---- Clean up.
clear cluster 
uniqueOffJobNumber = unique(offSource.jobNumber);
fprintf(1,'done! \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     Load injection files                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic

% ---- Waveform name.
%name = cell(1,length(autoFilesCell));

% ---- Loop over waveforms tested.
for iWave = 1:length(autoFilesCell)

    % -------------------------------------------------------------
    %                     Waveform properties.
    % -------------------------------------------------------------

    % ---- Read and parse injection parameters.
    disp(['Loading file ' injectionFilesCell{iWave}]);
    
    % ---- Read "auto" file for this specific waveform type.
    input = fopen(autoFilesCell{iWave});
    offSourceFile = fscanf(input,'%s',1); 
    nInjectionScales =  fscanf(input,'%d',1);
    
    for iInjScale = 1:nInjectionScales

        % ---- Name of matlab file containing injection triggers for this
        %      scale.
        simulated{iInjScale} = fscanf(input,'%s',1);
        disp(['Loading injection file ' simulated{iInjScale}])
        % ---- Load injection triggers from this file.
        temp = load(simulated{iInjScale},'clusterInj', ...
            'injectionProcessedMask','injectionScale','peakTime','onSourceTimeOffset');
        % ---- Copy clusterInj array for this injection scale into
        %      higher-dimensional struct array: number of
        %      injections x number of scales.
        nInjections(iWave) = length(temp.clusterInj);
        for iN = 1:nInjections(iWave)
          clusterInj(iWave,iN,iInjScale) = temp.clusterInj(iN);                 
        end
        injectionProcessedMask{iWave}(:,iInjScale) = temp.injectionProcessedMask;
        injectionScale(iWave,iInjScale) = temp.injectionScale;
        onSourceTimeOffset(iWave) = temp.onSourceTimeOffset;
        injPeakTime{iWave} = temp.peakTime;

        clear temp
    end %-- end loop over injection scales.
    toc
end % End loop of number of waveforms

% -------------------------------------------------------------
%           Injection results for each injection scale.
% -------------------------------------------------------------

% Update ClusterInj Info. Must be done post loading
for iWave = 1:length(autoFilesCell)
    for iInjScale = 1:nInjectionScales
        for iN = 1:nInjections(iWave)
            nTriggers = length(clusterInj(iWave,iN,iInjScale).significance);
            if(nTriggers)
              clusterInj(iWave,iN,iInjScale).pass         = logical(ones(nTriggers,1));
              clusterInj(iWave,iN,iInjScale).passVetoSegs = logical(ones(nTriggers,1));
              clusterInj(iWave,iN,iInjScale).passInjCoinc = logical(ones(nTriggers,1));
            else
              clusterInj(iWave,iN,iInjScale).pass         = [];
              clusterInj(iWave,iN,iInjScale).passVetoSegs = [];
              clusterInj(iWave,iN,iInjScale).passInjCoinc = [];
            end
        end
    end

    clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]). ...
        likelihood = vertcat(clusterInj(iWave,:,:).likelihood);
    clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]). ...
        peakTime = vertcat(clusterInj(iWave,:,:).peakTime);
    clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]). ...
        boundingBox = vertcat(clusterInj(iWave,:,:).boundingBox);
end
           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Find time of offSource triggers after time-shifts have been undone
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(1,['Finding off-source trigger times after time-shifts '...
    'have been removed ... ']); 
for thisIfo = 1:length(analysis.detectorList) 
    % ---- The circular time slides are shifts of data so opposite to time
    %      offsets which are shifts in read data, they also need to be
    %      unwrapped with reference to the start of the time frequency map
    offSource.unslidTime(:,thisIfo) = mod(offSource.peakTime - offSource.circTimeSlides(:,thisIfo) ...
                                          - (offSource.startTime + analysis.transientTime), ...
                                          analysis.blockTime - 2*analysis.transientTime) + ...
        offSource.startTime + analysis.transientTime ...
        + offSource.timeOffsets(:,thisIfo);
    offSource.unslidCenterTime(:,thisIfo) = offSource.centerTime(:) ...
        + offSource.timeOffsets(:,thisIfo);

    % ---- The on source should not be slid but various functions
    %      require the unslidTime and unslidCenterTime fields so
    %      we set them here.
    onSource.unslidTime(:,thisIfo) = onSource.peakTime;
    onSource.unslidCenterTime(:,thisIfo) = onSource.centerTime(:);
end
fprintf(1,'done! \n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Find appropriate DQ segments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vetoSegs = [];

ifos = {'H1','L1','V1'};

% ---- We discard veto segments which lay beyond the offSource region
%      of the particular GRB we are analysing.
%      Reducing the number of veto segments we use saves us considerable 
%      time.
% ---- Number of seconds before centerTime that offSource region begins,
%      should always be +ve
offSourceRegion.offset = 0.5 * backgroundPeriod;
% ---- Duration of window in seconds
offSourceRegion.duration = backgroundPeriod;

% ---- Loop over ifos we have analysed
for thisIfo = 1:length(analysis.detectorList)

    % ---- Find vetoSegList corresponding to thisIfo
    thisVetoSegList = vetoSegList{strcmp(ifos,analysis.detectorList(thisIfo))};  

    % ---- Read in veto seg files
    if ~strcmp(thisVetoSegList,'None') 
        % ---- Read in veto seg files

        % ---- Write output to stdout
        fprintf(1,'Reading segments from %s for %s ... ', thisVetoSegList, ...
            analysis.detectorList{thisIfo});
        tempVetoSegs = [];
        fsegin = fopen(thisVetoSegList,'r');
        tempVetoSegs = textscan(fsegin, '%f %f %f %f','CommentStyle','#');
        fclose(fsegin); 
        fprintf(1,'done! \n') 
    
        vetoSegsTemp(thisIfo).gpsStart = tempVetoSegs{2};
        vetoSegsTemp(thisIfo).duration = tempVetoSegs{4};

        disp(['Keeping only veto segments that lay in window' ...
            ' beginning ' int2str(offSourceRegion.offset) ...
            's before GRB time' ...
            ' with duration ' int2str(offSourceRegion.duration) ]);
        disp(['    GRB times : ' int2str(analysis.gpsCenterTime) ]);
        disp(['    offSourceRegion spans from ' ...
             int2str(analysis.gpsCenterTime - offSourceRegion.offset) ...
             ' to ' ...
             int2str(analysis.gpsCenterTime - offSourceRegion.offset ...
             + offSourceRegion.duration)]);
        % ---- Keep only segments which overlap with our offSourceRegion
        coincOut=Coincidence2(analysis.gpsCenterTime - offSourceRegion.offset,...
                              offSourceRegion.duration,...
                              vetoSegsTemp(thisIfo).gpsStart,...
                              vetoSegsTemp(thisIfo).duration);

        if ~isempty(coincOut)
            % ---- Sixth column of coincOut contains indices of tempVetoSegs
            %      overlapping with our offSource region
            fprintf(1,'Keeping %d of %d of veto segments for %s \n', ...
                length(coincOut(:,6)),...
                length(vetoSegsTemp(thisIfo).gpsStart),...
                analysis.detectorList{thisIfo} );

            vetoSegs(thisIfo).gpsStart = ...
                vetoSegsTemp(thisIfo).gpsStart(coincOut(:,6));
            vetoSegs(thisIfo).duration = ...
                vetoSegsTemp(thisIfo).duration(coincOut(:,6));

        % ---- If none of the veto segments overlap with our offSource region
        else
            disp(['No veto segs for ' analysis.detectorList{thisIfo} ...
                 ' overlap with our off source region ']); 
            vetoSegs(thisIfo).gpsStart = [];
            vetoSegs(thisIfo).duration = [];
        end

    % ---- If we did not supply veto segments for this ifo
    else
        fprintf(1,'No veto segs for %s\n', analysis.detectorList{thisIfo});
        vetoSegs(thisIfo).gpsStart = [];
        vetoSegs(thisIfo).duration = [];
    end  

end % -- End of for loop over ifos


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Check on source segments satisfy our DQ criteria.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Note that unlike the other pass flags, onJobsPass has one element 
%      per onSource job, not for each onSource trigger.
disp('Checking data quality of on source segments');
[onJobsPass, onJobsDeadtime] = ...
    xoffsourcedataqualitycheck(analysis,onSource,vetoSegs,'quiet');
if onJobsPass
    fprintf(1,'On source segment passes data quality checks\n')
else
    error('On source segment fails data quality checks!');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Check off source segments satisfy our DQ criteria.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Note that unlike the other pass flags, offJobsPass has one element 
%      per offSource job, not for each offSource trigger.
disp('Checking data quality of off source segments');
[offJobsPass, offJobsDeadtime] = ...
    xoffsourcedataqualitycheck(analysis,offSource,vetoSegs,'quiet');
fprintf(1,'%d of the %d off source segments pass data quality checks \n',...
    sum(offJobsPass),length(offJobsPass));

% ---- Number of jobs passing our DQ criteria
nOffJobsPass = sum(offJobsPass);

% ---- Find jobNumbers of jobs that pass DQ criteria
uniqueOffJobNumberPass = uniqueOffJobNumber(logical(offJobsPass));

% ---- Find jobNumbers of jobs that fail DQ criteria
uniqueOffJobNumberFail = uniqueOffJobNumber(not(logical(offJobsPass)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Apply veto seg lists to on, off, and injection data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Applying veto segments to on source clusters']);
% ---- Note that xapplyvetosegments expects a column of trigger times for
%      each ifo analysed. Therefore we must multiply the column vector 
%      onSource.boundingBox(:,1) by a row vector of ones with length numIfos
onSource.passVetoSegs = ...
    xapplyvetosegments(onSource,onSource.unslidTime,...
    vetoSegs,analysis,'onSource','quiet');
onSource.pass = min(onSource.pass,onSource.passVetoSegs);
onSource = xclustersubset(onSource,find(onSource.passVetoSegs));

disp(['Applying veto segments to off source clusters']);
offSource.passVetoSegs = ...
    xapplyvetosegments(offSource,offSource.unslidTime,...
    vetoSegs,analysis,'offSource','quiet');
offSource.pass = min(offSource.pass,offSource.passVetoSegs);
offSource = xclustersubset(offSource,find(offSource.passVetoSegs));

for iWave = 1:length(autoFilesCell)
    %fprintf(1,'Applying veto segments to %s waveform injections \n', name{iWave});
    passVetoSegsPrecompute = ...
        xapplyvetosegments(clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]),...
                           clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]).peakTime*...
                           ones(1,length(analysis.detectorList)),...
                           vetoSegs,analysis,...
                           ['inj_' sprintf(['%', '.0f'], iWave) ],'quiet');
    iCluster = 0;                   
    for iInjScale = 1:nInjectionScales
        for iN = 1:nInjections(iWave)
            % ---- Find range of precomputed values
            precomputeRange = iCluster+1:iCluster+length(clusterInj(iWave,iN,iInjScale).pass);
            iCluster = iCluster + length(clusterInj(iWave,iN,iInjScale).pass);
            % -- work around 0x1 and 0x0 empty matrices not being equal
            if length(precomputeRange)==0
              precomputeRange = [];
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %        Apply veto segments to injection triggers.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            clusterInj(iWave,iN,iInjScale).passVetoSegs = ...
                passVetoSegsPrecompute(precomputeRange);

        end  %-- loop over injection number
    end  %-- loop over injection scales
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Apply veto window cuts to injection triggers                %      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for iWave = 1:length(autoFilesCell)   
    %Obtain name from injection file in order to detemrine appriorate coincident window
    [injectionParameters] = ...
     readinjectionfile(char(injectionFilesCell{iWave}));
    [~, ~, ~, ~, ~, ~, name, ~] = ...
                 parseinjectionparameters(injectionParameters);
    waveformName{iWave}           = cell2mat(name{1});
    for iInjScale = 1:nInjectionScales
        for iN = 1:nInjections(iWave)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %  Keep only triggers which lie within 100ms of injection.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % skip if there are no triggers
            if(not(isempty(clusterInj(iWave,iN,iInjScale).boundingBox)))
                if strcmp(lower(waveformName{iWave}),'inspiral') || ...
                   strcmp(lower(waveformName{iWave}),'inspiralsmooth') || ...
                   strcmp(lower(waveformName{iWave}),'ninja') || ...
                   strcmp(lower(waveformName{iWave}),'lalinspiral') 
                    % ---- Larger coincidence window for inspirals.
                    injCoincTimeWindow = [10.0 0.1]; 
                else
                    % ---- Default coincidence window.
                    injCoincTimeWindow = [0.1 0.1]; 
                end


                % ---- Bounding box of injection in the time-frequency plane.
                injectionBox = [(injPeakTime{iWave}(iN)-injCoincTimeWindow(1)-onSourceTimeOffset(iWave)), ...
                    analysis.minimumFrequency, sum(injCoincTimeWindow), ...
                    analysis.maximumFrequency-analysis.minimumFrequency];   

                % ---- Find all clusters whose bounding box does NOT overlap that 
                %      of the injection and set passInjCoinc to zero for them
                mask = find(not(rectintersect(injectionBox,clusterInj(iWave,iN,iInjScale).boundingBox)));
                clusterInj(iWave,iN,iInjScale).passInjCoinc(mask) = 0;

            end
        end  %-- loop over injection number
    end  %-- loop over injection scales
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Apply asymmetric window to on and off source data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp(['Keeping only clusters that lay in window' ...
      ' beginning ' int2str(window.offset) 's before center time of block' ...
      ' with duration ' int2str(window.duration) ]);

fprintf(1,'Checking on source times for vetoed clusters ...\n');
onSource.passWindow = xapplywindowcut(window,onSource,'onSource');
onSource.pass = min(onSource.pass,onSource.passWindow);

fprintf(1,'Checking off source times for vetoed clusters ... \n');
offSource.passWindow = xapplywindowcut(window,offSource,'offSource');
offSource.pass = min(offSource.pass,offSource.passWindow);

onSource = xclustersubset(onSource,find(onSource.passWindow));
offSource = xclustersubset(offSource,find(offSource.passWindow));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Select what jobs will be used for tuning and UL.
%     Afterwards, choose the ones for tuning.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Get random order of indices between 1 and 
%      nOffJobsPass.
rand('seed',initialSeed + 11);
offJobSeed = rand('seed');
randOffIdx = randperm(nOffJobsPass);
nOffJobsPassBy2 = ceil(nOffJobsPass/2);

% ---- Use the first half of randomly ordered injections for tuning.
offIdx_tuning  = randOffIdx(1:nOffJobsPassBy2);
nOffJob_tuning = length(offIdx_tuning);   
offJob_tuning  = uniqueOffJobNumberPass(offIdx_tuning);    
offJob_tuning  = sort(offJob_tuning);   

% ---- Use the second half of randomly ordered injections for ul calc.
offIdx_ulCalc  = randOffIdx(nOffJobsPassBy2+1:nOffJobsPass);
nOffJob_ulCalc = length(offIdx_ulCalc);   
offJob_ulCalc  = uniqueOffJobNumberPass(offIdx_ulCalc); 
offJob_ulCalc  = sort(offJob_ulCalc); 

% Select the off source jobs designated for tuning
jobNumbers = offJob_tuning;

% ---- Pull from the offSource structure every trigger whose
%      job number is listed in the jobNumbers vector.
% ---- First we identify which elements of offSource we want
%      in clusterIndex. 
% ---- Reuse function of coincidence between segments and events
coincIndex = fastcoincidence2([jobNumbers 0.1*ones(size(jobNumbers))],...
                              [offSource.jobNumber 0.1*ones(size(offSource.jobNumber))]);
clusterIndex = coincIndex(:,2);

% ---- Use xclustersubset to pull out the jobs whose indices are
%      listed in clusterIndex and write these to a new structure
%      offSourceSelected.
offSourceSelected = [];
offSourceSelected = xclustersubset(offSource,clusterIndex); 
save('offsourcepp.mat','offSource')
clear offSource

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Select injections for tuning and UL                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for iWave = 1:length(autoFilesCell)
    % ---- Get random order of indices between 1 and nInjections(iWave). 
    rand('seed',initialSeed + 22);
    injSeed = rand('seed');
    randInjIdx = randperm(nInjections(iWave));
    nInjectionsBy2(iWave) = ceil(nInjections(iWave)/2);

    % ---- Use the first half of randomly ordered injections for tuning.
    injIdx_tuning{iWave} = randInjIdx(1:nInjectionsBy2(iWave));
    injIdx_tuning{iWave} = sort(injIdx_tuning{iWave});
    % ---- Use the second half of randomly ordered injections for ul calc.
    injIdx_ulCalc{iWave} = randInjIdx(nInjectionsBy2(iWave)+1:nInjections(iWave));
    injIdx_ulCalc{iWave} = sort(injIdx_ulCalc{iWave});

    % ---- Create a mask to pass injections for tuning.
    injMask_tuning{iWave} = zeros(nInjections(iWave),1);
    injMask_tuning{iWave}(injIdx_tuning{iWave}) = 1;
    % ---- Create a mask to pass injections for ul calc.
    injMask_ulCalc{iWave} = zeros(nInjections(iWave),1);
    injMask_ulCalc{iWave}(injIdx_ulCalc{iWave}) = 1;
    fprintf(1,'Selecting injections to use for tuning... \n');
    % ---- Loop over all injections, injectionScales.
    for iInjScale = 1:nInjectionScales
        for iN = 1:nInjections(iWave)
            % ---- Use injMask_tuning{iWave}.
            injectionProcessedMask{iWave}(iN,iInjScale) = ...
                min(injectionProcessedMask{iWave}(iN,iInjScale),injMask_tuning{iWave}(iN));
        end
    end 
end
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Clear some field from the structure array for memory purposes        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fields = {'boundingBox','nPixels','listIndex','analysisTime','jobNumber','fakeJobNumber',...
'peakFrequency','peakTime','timeOffsets','circTimeSlides','centerTime','startTime','realJobNumber'};
clusterInj = rmfield(clusterInj,fields);

fields = {'boundingBox','nPixels','listIndex','analysisTime','fakeJobNumber',...
'peakFrequency','peakTime','timeOffsets','circTimeSlides','centerTime','startTime','realJobNumber',...
'unslidTime','unslidCenterTime'};
offSourceSelected = rmfield(offSourceSelected,fields);

fields = {'boundingBox','peakTime'};
for iWave = 1:length(autoFilesCell)
    clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]) = ...
        rmfield(clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]),fields);
end

clear vetoSegsTemp tempVetoSegs fields


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Based on vetoMethod determine appropriate tuning numbers            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% If One Detector make sure vetoMethod is none

if length(analysis.detectorList) == 1;
    vetoMethod = 'None';
end

% Initialize flag fo assume that an alphaLinCut has not been requested
prePassFlag = 0;

% ---- Identify cuts to loop over.
if ~isempty(regexp(vetoMethod,'alphaLinCut.*', 'once'))
    
     nullOneSidedRangeAlphaCut = [0,-0.5,-1,-1.5,-2,-2.5,-3,-3.5,-4,-4.5,-5,-5.5,-6,-6.5,-7]; 
     oneSidedRangeAlphaCut     = [0,-0.5,-1,-1.5,-2,-2.5,-3,-3.5,-4,-4.5,-5,-5.5,-6,-6.5,-7]; 
     twoSidedRangeAlphaCut     = [0,0.18,0.22,0.27,0.34,0.43,0.54,0.67,...
                        0.84,1.05,1.31,1.64,2.05,2.56,3.2,4.0,5.0,6.0,7.0];
     prePassFlag           = 1;
end
    
if ~isempty((regexp(vetoMethod,'(linearCut.*|alphaCut.*)'))) || (prePassFlag ==1)
    
    nullOneSidedRangeLinearCut = [0,-1,-1.01,-1.03,-1.06,-1.1,-1.15,-1.2,...
                        -1.27,-1.35,-1.45,-1.57,-1.71,-1.9,-2.1,...
                        -2.4,-2.7,-3,-3.3,-3.6,-4];
    oneSidedRangeLinearCut     = [0,-1,-1.01,-1.03,-1.06,-1.1,-1.15,-1.2,...
                        -1.27,-1.35,-1.45,-1.57,-1.71,-1.9,-2.1,...
                        -2.4,-2.7,-3,-3.3,-3.6,-4];
    twoSidedRangeLinearCut     = [0,1,1.01,1.03,1.06,1.1,1.15,1.2,1.27,1.35,...
                        1.45,1.57,1.71,1.9,2.1,2.4,2.7,3,3.3,3.6,4];
end
                

% vetoMethod determine the appropriate range (one or two sided)

if ~isempty(regexp(vetoMethod,'(alphaLinCut)($|Linear)', 'once'))
    vetoPlusRangeAlphaCut  = twoSidedRangeAlphaCut;
    vetoCrossRangeAlphaCut = twoSidedRangeAlphaCut; 
elseif regexp(vetoMethod,'(alphaLinCut)(Scalar|Circ|Amp)')
    vetoPlusRangeAlphaCut  = oneSidedRangeAlphaCut;
    vetoCrossRangeAlphaCut = oneSidedRangeAlphaCut; 
end

if ~isempty(regexp(vetoMethod,'(.*Cut$|.*Linear)', 'once'))
    vetoPlusRangeLinearCut  = twoSidedRangeLinearCut;
    vetoCrossRangeLinearCut = twoSidedRangeLinearCut; 
elseif ~isempty(regexp(vetoMethod,'(.*Scalar|.*Circ|.*Amp)', 'once'))
    vetoPlusRangeLinearCut  = oneSidedRangeLinearCut;
    vetoCrossRangeLinearCut = oneSidedRangeLinearCut;      
end

% Determine whether Null cuts are necessary (more than 2 detectors)
% Also even if nullenergy, nullinc was not calculated..calculate anyways

if length(analysis.detectorList) <= 2;
    vetoNullRangeLinearCut      = [0];
    vetoNullRangeAlphaCut       = [0];
    
else
    vetoNullRangeLinearCut      = nullOneSidedRangeLinearCut;
    vetoNullRangeAlphaCut       = nullOneSidedRangeAlphaCut;
end

if ~isempty(regexp('None',vetoMethod, 'once'))
    
    vetoNullRangeLinearCut  = [0];
    vetoPlusRangeLinearCut  = [0];
    vetoCrossRangeLinearCut = [0]; 
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Preparation for veto cuts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Get all distinct combinations of veto thresholds, pack into vectors.

if ~isempty(regexp(vetoMethod,'alphaLinCut.*', 'once'))
    
    [X,Y,Z] = meshgrid(vetoNullRangeAlphaCut,vetoPlusRangeAlphaCut,vetoCrossRangeAlphaCut);
    alphaCutNullRange  = X(:);
    alphaCutPlusRange  = Y(:);
    alphaCutCrossRange = Z(:);
    clear X Y Z
    
end

if  ~isempty((regexp(vetoMethod,'(linearCut.*|alphaCut.*)'))) || (prePassFlag ==1)
    
    [X,Y,Z] = meshgrid(vetoNullRangeLinearCut,vetoPlusRangeLinearCut,vetoCrossRangeLinearCut);
    linearCutNullRange  = X(:);
    linearCutPlusRange  = Y(:);
    linearCutCrossRange = Z(:);
    clear X Y Z    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Identify likelihoods used for coherent vetoes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Figure our which column of onSource.likelihood contains each
%      likelihoodType

if ~isempty(regexp(vetoMethod,'.*Cut$', 'once'))
    
    ePlusIndex  = find(strcmp(analysis.likelihoodType,'plusenergy'));
    eCrossIndex = find(strcmp(analysis.likelihoodType,'crossenergy'));
    iPlusIndex  = find(strcmp(analysis.likelihoodType,'plusinc'));
    iCrossIndex = find(strcmp(analysis.likelihoodType,'crossinc'));

    
elseif ~isempty(regexp(vetoMethod,'.*CutCirc', 'once'))
    
    ePlusIndex  = find(strcmp(analysis.likelihoodType,'circenergy'));
    eCrossIndex = find(strcmp(analysis.likelihoodType,'circnullenergy'));
    iPlusIndex  = find(strcmp(analysis.likelihoodType,'circinc'));
    iCrossIndex = find(strcmp(analysis.likelihoodType,'circnullinc'));
    
elseif ~isempty(regexp(vetoMethod,'.*CutScalar', 'once'))
    
    ePlusIndex  = find(strcmp(analysis.likelihoodType,'scalarenergy'));
    eCrossIndex = find(strcmp(analysis.likelihoodType,'scalarnullenergy'));
    iPlusIndex  = find(strcmp(analysis.likelihoodType,'scalarinc'));
    iCrossIndex = find(strcmp(analysis.likelihoodType,'scalarnullinc'));
    
elseif ~isempty(regexp(vetoMethod,'.*CutLinear', 'once'))
       
    ePlusIndex  = find(strcmp(analysis.likelihoodType,'elinear'));
    eCrossIndex = find(strcmp(analysis.likelihoodType,'elinearnull'));
    iPlusIndex  = find(strcmp(analysis.likelihoodType,'ilinear'));
    iCrossIndex = find(strcmp(analysis.likelihoodType,'ilinearnull'));
else
    error('Cut Type cannot assign E and I values')
end

if (~isempty(find(strcmp(analysis.likelihoodType,'nullenergy'), 1))) && ...
	(length(analysis.detectorList) > 2);
    eNullIndex  = find(strcmp(analysis.likelihoodType,'nullenergy'));
    iNullIndex  = find(strcmp(analysis.likelihoodType,'nullinc'));
    analysis.typeOfCutNull = 'IoverE';
else
    eNullIndex = 0;
    iNullIndex = 0;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            If your cut is a one-sided cut, determine 
%           determine whether I over E or E over I
%           should be used for tuning your cuts by seeing what part of the 
%           E/I plan your triggers for your first Waveform fall in.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isempty(regexp(vetoMethod,'(.*CutLinear|.*CutScalar|.*CutCirc)', 'once'))

    plusRatioEoverI = log(clusterPrecompute.Wave1.likelihood(:,ePlusIndex)./clusterPrecompute.Wave1.likelihood(:,iPlusIndex));
    crossRatioEoverI = log(clusterPrecompute.Wave1.likelihood(:,eCrossIndex)./clusterPrecompute.Wave1.likelihood(:,iCrossIndex));
    plusRatioIoverE = log(clusterPrecompute.Wave1.likelihood(:,iPlusIndex)./clusterPrecompute.Wave1.likelihood(:,ePlusIndex));
    crossRatioIoverE = log(clusterPrecompute.Wave1.likelihood(:,iCrossIndex)./clusterPrecompute.Wave1.likelihood(:,eCrossIndex));
    
    if (sum(plusRatioEoverI > 0)> sum(plusRatioIoverE > 0))
        typeOfCutPlus  = 'EoverI';
    else
        typeOfCutPlus  = 'IoverE';
    end   
    if (sum(crossRatioEoverI > 0)> sum(crossRatioIoverE > 0))
        typeOfCutCross = 'EoverI';
    else
        typeOfCutCross = 'IoverE';
    end   
    clear plusRatioEoverI crossRatioEoverI plusRatioIoverE crossRatioIoverE
    analysis.typeOfCutPlus = typeOfCutPlus;
    analysis.typeOfCutCross = typeOfCutCross;
       
else
    typeOfCutPlus = 'twosided';
    typeOfCutCross = 'twosided';
    analysis.typeOfCutPlus = typeOfCutPlus;
    analysis.typeOfCutCross = typeOfCutCross;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Determine index of the Ranking/DetStats you requested!                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

detStats = [];
for iDetStat = 1:length(detectionStatCell)
    detIndex  = find(strcmp(analysis.likelihoodType,detectionStatCell{iDetStat}));
    detStats = [detStats,detIndex];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Determine the Magic (Coherent Cut) numbers                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loop over all possible detection significance values

for iDetStat = 1:length(detStats)

    detectionStat = analysis.likelihoodType(detStats(iDetStat));
    fprintf(1,'Using Detection Stat %s\n', detectionStat{1});
    
    % Update OffSource Selected with this detection Statistic
    offSourceSelected.significance = offSourceSelected.likelihood(:,detStats(iDetStat));
    
    % Update ClusterInj Info with detection Stat
    for iWave = 1:length(autoFilesCell)
        for iInjScale = 1:nInjectionScales
            for iN = 1:nInjections(iWave)
                if isempty(clusterInj(iWave,iN,iInjScale).likelihood)
                    clusterInj(iWave,iN,iInjScale).significance = [];
                else
                    clusterInj(iWave,iN,iInjScale).significance = ...
                    clusterInj(iWave,iN,iInjScale).likelihood(:,detStats(iDetStat));
                end
            end
        end
        clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]). ...
            significance = vertcat(clusterInj(iWave,:,:).significance);
    end
    
    
    if ~(isempty(regexp(vetoMethod,'None', 'once')))
        warning('Veto method is None, apply ratio cuts of zero and only apply significance Cuts')
    else
        fprintf(1,'Applying Ratio cuts to off source\n');
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %            Apply veto cuts to off-source events.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      

    [loudestBackgroundRatio] = ...      
        xapplyratiocuts(offSourceSelected,...
                    ePlusIndex,eCrossIndex,eNullIndex,...
                    iPlusIndex,iCrossIndex,iNullIndex,...
                    linearCutPlusRange,linearCutCrossRange,linearCutNullRange,...
                      FAR_Tuning,typeOfCutPlus,typeOfCutCross); 
    toc;

    if sum(offSourceSelected.pass) == 0  
            warning('We have vetoed all of the offSourceSelected events.')
    end

    for iWave = 1:length(autoFilesCell)

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %            Apply veto tests to injection triggers.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            fprintf(1,'Applying Ratio Cuts to waveform %d \n', iWave);

            % ---- Precompute coherent consistency test results for speed (vectorization)
            %tic;

            [passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)])] = ...
                xapplyratiocutsinjections(clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]),...
                    ePlusIndex,eCrossIndex,eNullIndex,...
                    iPlusIndex,iCrossIndex,iNullIndex,...
                    linearCutPlusRange,linearCutCrossRange,linearCutNullRange,...
                      loudestBackgroundRatio,typeOfCutPlus,typeOfCutCross);
             toc;

           fprintf(1,'Assigning Values to clusters in waveform %d \n', iWave);
           % ---- iterator on precomputed outputs
            iCluster=0;

            % ---- Loop over injection scales and injection number.
            for iInjScale = 1:nInjectionScales
                for iN = 1:nInjections(iWave)
                    % ---- Find range of precomputed values
                    precomputeRange = iCluster+1:iCluster+length(clusterInj(iWave,iN,iInjScale).pass);
                    iCluster = iCluster + length(clusterInj(iWave,iN,iInjScale).pass);
                    % -- work around 0x1 and 0x0 empty matrices not being equal
                    if length(precomputeRange) == 0
                      precomputeRange = [];
                    end           


                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %   See if Cluster Passed Cut
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    clusterInj(iWave,iN,iInjScale).passRatio = ...
                            passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]).passRatioCuts(precomputeRange,:);  


                end  %-- loop over injection number
            end  %-- loop over injection scales
            passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]) = ...
                rmfield(passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]),'passRatioCuts');

    % Now we have applied window, DQ and Ratio/Significance Cuts.
    % Now, create a master pass only for those waveforms designated as
    % tuning by the injectionPrcoessedMask variable.
    % If it is in the tuning set we will tick up the processed variable and
    % calculate the effiency.
    
    processed =0;
    [~,numRatioCuts] = size(clusterInj(iWave,iN,iInjScale).passRatio);
        for iInjScale=1:nInjectionScales
            analysisRatioEff{detStats(iDetStat),iInjScale,iWave} = zeros(1,numRatioCuts);
            for iN = 1:nInjections(iWave)
                [numClusters,~] = size(clusterInj(iWave,iN,iInjScale).passRatio);
                if numClusters==0;
                else
                    clusterInj(iWave,iN,iInjScale).passMaster = ...                 
                            logical(repmat(clusterInj(iWave,iN,iInjScale).passInjCoinc,...
                            1,numRatioCuts).* ...
                            repmat(clusterInj(iWave,iN,iInjScale).passVetoSegs,...
                            1,numRatioCuts).* ...                    
                        clusterInj(iWave,iN,iInjScale).passRatio);
                                        
                end
                if (injectionProcessedMask{iWave}(iN,iInjScale) ==1) && (numClusters ==0)
                    processed = processed+1;
                elseif injectionProcessedMask{iWave}(iN,iInjScale) ==1
                    processed = processed+1;                    
                    analysisRatioEff{detStats(iDetStat),iInjScale,iWave} = ...
                            analysisRatioEff{detStats(iDetStat),iInjScale,iWave} + ...
                            (sum(clusterInj(iWave,iN,iInjScale).passMaster,1)~=0);
                else
                end
            end
            analysisRatioEff{detStats(iDetStat),iInjScale,iWave} = analysisRatioEff{detStats(iDetStat),iInjScale,iWave} ./ processed;
            processed = 0;
        end
        fields = {'passMaster','passRatio'};
        clusterInj = rmfield(clusterInj,fields);
    end % loop over waveform
end % Loop over detection stats

% Currently we have a cell array that tells us the hrss value for all the
% cut thresholdsfor a given injections cale detstat and waveform.
% Now we must take that information and create an # of cuts by # of
% waveforms array so we can calculate the SSFEUL.

for iDetStat = 1:length(detStats)
    analysisRatioEff95percent{detStats(iDetStat)} = 10*injectionScale(1,nInjectionScales)*...
        ones(length(analysisRatioEff{detStats(iDetStat),1,1}),length(autoFilesCell));
    for iWave = 1:length(autoFilesCell)
        numcuts = length(analysisRatioEff{detStats(iDetStat),1,iWave});
        goFlag = ones(1,numcuts);
        for iInjScale=(nInjectionScales):-1:2       
            % ---- Determine the Injection Scale for which
            % ---- The efficiency is above 95 percent
            % ---- Then record the cut for which this occurs
            % ---- If the cut never produces 95 percent efficiency then it
            % ---- stay at the worst injection scale. Start from the worst
            % (so the highest injection scale and go backwards

            cond = (analysisRatioEff{detStats(iDetStat),iInjScale,iWave} >= ULlinear) & ...
                (analysisRatioEff{detStats(iDetStat),iInjScale-1,iWave} < ULlinear) & (goFlag==1) ;
            %| ((analysisRatioEff{detStats(iDetStat),nInjectionScales,iWave} < ULlinear));
            if any(cond)
                [~,effCols] = find(cond);              
                %[~,effCols] = find(analysisRatioEff{detStats(iDetStat),iInjScale,iWave} <=0.95);   
                % Found crossover point but what more precise 95 percent UL
                % so we will interpolate between injectionscale-1 and
                % injection scale. Our Interpolation will simply be a line
                % connection the two points.
                yDiff = (analysisRatioEff{detStats(iDetStat),iInjScale,iWave} - analysisRatioEff{detStats(iDetStat),iInjScale-1,iWave});
                xDiff = (log10(injectionScale(iWave,iInjScale))-log10(injectionScale(iWave,iInjScale-1))); 
                
                interp95PercentSlope = yDiff(effCols)./xDiff;
                injectionScale95Percent = log10(injectionScale(iWave,iInjScale-1)) + ...
                        ((ULlinear- analysisRatioEff{detStats(iDetStat),iInjScale-1,iWave}(effCols))./ interp95PercentSlope);
                
                analysisRatioEff95percent{detStats(iDetStat)}(effCols,iWave) = (10.^injectionScale95Percent)';
                goFlag(effCols) = 0;

            end
        end
        cond = (analysisRatioEff{detStats(iDetStat),1,iWave} >= ULlinear) & (goFlag==1);
        if any(cond)
            [~,effCols] = find(cond);  
            analysisRatioEff95percent{detStats(iDetStat)}(effCols,iWave) = injectionScale(iWave,1);
        end
    end
end

% Calculate the SSFEUL per detectionStat.
% Which is The sum over waveforms of these values
% ((Hrss-hrss_min)/hrss_min)^2 for each column

SSFEULRatioPerDetStat = [];
combine90percent      = [];

for iDetStat = 1:length(detStats)
    combine90percent = vertcat(combine90percent,analysisRatioEff95percent{detStats(iDetStat)});        
end

injectionscalemin = repmat(min(combine90percent),length(combine90percent),1);
SSFEULRatioPerDetStat = sum(...
    (((combine90percent - injectionscalemin)./injectionscalemin).^2),2);

SSFEULRatioPerDetStat = reshape(SSFEULRatioPerDetStat,numcuts,length(detStats));

[mostEffRatioCuts, ~] = find(SSFEULRatioPerDetStat == min(min(SSFEULRatioPerDetStat)));
mostEffRatioCut    = mostEffRatioCuts(1);
linearCutPlusTuned   = linearCutPlusRange(mostEffRatioCut);
linearCutCrossTuned  = linearCutCrossRange(mostEffRatioCut);
linearCutNullTuned   = linearCutNullRange(mostEffRatioCut);
fprintf(1,'Successfully chose optimal ratio cut! Plus %f Cross %f Null %f\n',...
    linearCutPlusTuned, linearCutCrossTuned, linearCutNullTuned);


toc

if ~isempty(regexp(vetoMethod,'alphaLinCut.*', 'once'))
    fprintf(1,'Using optimal ratio cut decisions to help pick alpha cuts\n');  
    for iDetStat = 1:length(detStats)

        detectionStat = analysis.likelihoodType(detStats(iDetStat));
        fprintf(1,'Using Detection Stat %s\n', detectionStat{1});

        % Update OffSource Selected with this detection Statistic
        offSourceSelected.significance = offSourceSelected.likelihood(:,detStats(iDetStat));

        % Update ClusterInj Info with detection Stat
        for iWave = 1:length(autoFilesCell)
            for iInjScale = 1:nInjectionScales
                for iN = 1:nInjections(iWave)
                    if isempty(clusterInj(iWave,iN,iInjScale).likelihood)
                        clusterInj(iWave,iN,iInjScale).significance = [];
                    else
                        clusterInj(iWave,iN,iInjScale).significance = ...
                        clusterInj(iWave,iN,iInjScale).likelihood(:,detStats(iDetStat));
                    end
                end
            end
            clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]). ...
                significance = vertcat(clusterInj(iWave,:,:).significance);
        end  

        fprintf(1,'Applying Alpha cuts to off source\n');

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %            Apply veto cuts to off-source events.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      

        [loudestBackgroundAlpha] = ...      
            xapplyalphacuts(offSourceSelected,...
                        ePlusIndex,eCrossIndex,eNullIndex,...
                        iPlusIndex,iCrossIndex,iNullIndex,...
                        linearCutPlusTuned,linearCutCrossTuned,linearCutNullTuned,...
                        alphaCutPlusRange,alphaCutCrossRange,alphaCutNullRange,...
                          FAR_Tuning,typeOfCutPlus,typeOfCutCross); 
        toc;

        if sum(offSourceSelected.pass) == 0     
                warning('We have vetoed all of the offSourceSelected events.')
        end

        for iWave = 1:length(autoFilesCell)

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %            Apply veto tests to injection triggers.
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                fprintf(1,'Applying Alpha Cuts to waveform %d \n', iWave);

                % ---- Precompute coherent consistency test results for speed (vectorization)
                %tic;
                
                [passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)])] = ...
                    xapplyalphacutsinjections(clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]),...
                        ePlusIndex,eCrossIndex,eNullIndex,...
                        iPlusIndex,iCrossIndex,iNullIndex,...
                        linearCutPlusTuned,linearCutCrossTuned,linearCutNullTuned,...
                        alphaCutPlusRange,alphaCutCrossRange,alphaCutNullRange,...
                          loudestBackgroundAlpha,typeOfCutPlus,typeOfCutCross);
                 toc;

               fprintf(1,'Assigning Values to clusters in waveform %d \n', iWave);
               % ---- iterator on precomputed outputs
                iCluster=0;

                % ---- Loop over injection scales and injection number.
                for iInjScale = 1:nInjectionScales
                    for iN = 1:nInjections(iWave)
                        % ---- Find range of precomputed values
                        precomputeRange = iCluster+1:iCluster+length(clusterInj(iWave,iN,iInjScale).pass);
                        iCluster = iCluster + length(clusterInj(iWave,iN,iInjScale).pass);
                        % -- work around 0x1 and 0x0 empty matrices not being equal
                        if length(precomputeRange) == 0
                          precomputeRange = [];
                        end           


                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %   See if Cluster Passed Cut
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        clusterInj(iWave,iN,iInjScale).passAlpha = ...
                                passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]).passAlphaCuts(precomputeRange,:);  


                    end  %-- loop over injection number
                end  %-- loop over injection scales
        passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]) = ...
                rmfield(passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]),'passAlphaCuts');

        % Now we have applied window, DQ and Ratio/Significance Cuts.
        % Now, create a master pass only for those waveforms designated as
        % tuning by the injectionPrcoessedMask variable.
        % If it is in the tuning set we will tick up the processed variable and
        % calculate the effiency.

        processed =0;
        [~,numAlphaCuts] = size(clusterInj(iWave,iN,iInjScale).passAlpha);
            for iInjScale=1:nInjectionScales
                analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} = zeros(1,numAlphaCuts);
                for iN = 1:nInjections(iWave)
                    [numClusters,~] = size(clusterInj(iWave,iN,iInjScale).passAlpha);
                    if numClusters==0;
                    else
                        clusterInj(iWave,iN,iInjScale).passMaster = ...                 
                                logical(repmat(clusterInj(iWave,iN,iInjScale).passInjCoinc,...
                                1,numAlphaCuts).* ...
                                repmat(clusterInj(iWave,iN,iInjScale).passVetoSegs,...
                                1,numAlphaCuts).* ...                    
                            clusterInj(iWave,iN,iInjScale).passAlpha);

                    end
                    if (injectionProcessedMask{iWave}(iN,iInjScale) ==1) && (numClusters ==0)
                        processed = processed+1;
                    elseif injectionProcessedMask{iWave}(iN,iInjScale) ==1
                        processed = processed+1;                    
                        analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} = ...
                                analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} + ...
                                (sum(clusterInj(iWave,iN,iInjScale).passMaster,1)~=0);
                    else
                    end
                end
                analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} = ...
                    analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} ./ processed;
                processed = 0;
            end
        fields = {'passMaster','passAlpha'};
        clusterInj = rmfield(clusterInj,fields);
        end % loop over waveforms
    end % End loop over DetStats

    % Currently we have a cell array that tells us the hrss value for all the
    % cut thresholdsfor a given injections cale detstat and waveform.
    % Now we must take that information and create an # of cuts by # of
    % waveforms array so we can calculate the SSFEUL.
    for iDetStat = 1:length(detStats)
        analysisAlphaEff50percent{detStats(iDetStat)} = 10*injectionScale(1,nInjectionScales)*...
            ones(length(analysisAlphaEff{detStats(iDetStat),1,1}),length(autoFilesCell));
        for iWave = 1:length(autoFilesCell)
            numcuts = length(analysisAlphaEff{detStats(iDetStat),1,iWave});
            goFlag = ones(1,numcuts);
            for iInjScale=(nInjectionScales):-1:2       
                % ---- Determine the Injection Scale for which
                % ---- The efficiency is above 50 percent
                % ---- Then record the cut for which this occurs
                % ---- If the cut never produces 50 percent efficiency then it
                % ---- stay at the worst injection scale. Start from the worst
                % (so the highest injection scale and go backwards

                cond = ((analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} >= ULalpha) & ...
                    (analysisAlphaEff{detStats(iDetStat),iInjScale-1,iWave} < ULalpha) & (goFlag==1)); 

                if any(cond)
                    [~,effCols] = find(cond);              
                    %[~,effCols] = find(analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} <=0.95);   
                    % Found crossover point but what more precise 95 percent UL
                    % so we will interpolate between injectionscale-1 and
                    % injection scale. Our Interpolation will simply be a line
                    % connection the two points.
                    yDiff = (analysisAlphaEff{detStats(iDetStat),iInjScale,iWave} - analysisAlphaEff{detStats(iDetStat),iInjScale-1,iWave});
                    xDiff = log10(injectionScale(iWave,iInjScale))-log10(injectionScale(iWave,iInjScale-1)); 
                    
                    interp50PercentSlope = yDiff(effCols)./xDiff;
                    injectionScale50Percent = log10(injectionScale(iWave,iInjScale-1)) + ...
                            ((ULalpha- analysisAlphaEff{detStats(iDetStat),iInjScale-1,iWave}(effCols))./ interp50PercentSlope);
                    analysisAlphaEff50percent{detStats(iDetStat)}(effCols,iWave) = (10.^injectionScale50Percent)';
                    goFlag(effCols) = 0;
                end
            end
            cond = (analysisAlphaEff{detStats(iDetStat),1,iWave} >= ULalpha) & (goFlag==1);
            if any(cond)
                [~,effCols] = find(cond);  
                analysisAlphaEff50percent{detStats(iDetStat)}(effCols,iWave) = injectionScale(iWave,1);
            end
        end
    end

    % Calculate the SSFEUL per detectionStat.
    % Which is The sum over waveforms of these values
    % ((Hrss-hrss_min)/hrss_min)^2 for each column
    SSFEULAlphaPerDetStat = [];
    combine50percent      = [];

    for iDetStat = 1:length(detStats)
        combine50percent = vertcat(combine50percent,analysisAlphaEff50percent{detStats(iDetStat)});        
    end

    injectionscalemin = repmat(min(combine50percent),length(combine50percent),1);
    SSFEULAlphaPerDetStat = sum(...
        (((combine50percent - injectionscalemin)./injectionscalemin).^2),2);

    SSFEULAlphaPerDetStat = reshape(SSFEULAlphaPerDetStat,numcuts,length(detStats));

    [mostEffAlphaCuts, mostEffDetStats] = find(SSFEULAlphaPerDetStat == min(min(SSFEULAlphaPerDetStat)));
    mostEffAlphaCut    = mostEffAlphaCuts(1);
    mostEffDetStat     = mostEffDetStats(1);
    alphaCutPlusTuned  = alphaCutPlusRange(mostEffAlphaCut);
    alphaCutCrossTuned = alphaCutCrossRange(mostEffAlphaCut);
    alphaCutNullTuned  = alphaCutNullRange(mostEffAlphaCut);
    iDetStat           = detStats(mostEffDetStat);
    detectionStat      = analysis.likelihoodType(iDetStat);
    fprintf(1,'Successfully chose optimal detections stat %s and alpha cuts %f %f %f!!\n',...
        detectionStat{1},alphaCutPlusTuned,alphaCutCrossTuned,alphaCutNullTuned);
else
    alphaCutPlusTuned  = 0;
    alphaCutCrossTuned = 0;
    alphaCutCrossTuned = 0;

end % End if AlphaLinCut*

toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Calculate Upper Limits                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('offsourcepp.mat')
% Select the off source jobs designated for tuning
jobNumbers = offJob_ulCalc;

% ---- Pull from the offSource structure every trigger whose
%      job number is listed in the jobNumbers vector.
% ---- First we identify which elements of offSource we want
%      in clusterIndex. 
% ---- Reuse function of coincidence between segments and events
coincIndex = fastcoincidence2([jobNumbers 0.1*ones(size(jobNumbers))],...
                              [offSource.jobNumber 0.1*ones(size(offSource.jobNumber))]);
clusterIndex = coincIndex(:,2);

% ---- Use xclustersubset to pull out the jobs whose indices are
%      listed in clusterIndex and write these to a new structure
%      offSourceSelected.
offSourceSelected = [];
offSourceSelected = xclustersubset(offSource,clusterIndex); 
clear offSource


fprintf(1,'Selecting injections to use for UL calc... \n')
for iWave = 1:length(autoFilesCell);
    % ---- Loop over all injections, injectionScales.
    for iInjScale = 1:nInjectionScales
        for iN = 1:nInjections(iWave)
             % ---- Use injMask_ulCalc{iWave}.
             injectionProcessedMask{iWave}(iN,iInjScale) = ...
                 max(injectionProcessedMask{iWave}(iN,iInjScale),injMask_ulCalc{iWave}(iN));
             injectionProcessedMask{iWave}(iN,iInjScale) = ...
                 min(injectionProcessedMask{iWave}(iN,iInjScale),injMask_ulCalc{iWave}(iN));
        end
    end 
end

fprintf(1,'Calculating UpperLimits\n');

fprintf(1,'Using Detection Stat %s\n', detectionStat{1});

onSource.significance = onSource.likelihood(:,iDetStat);

% Update OffSource Selected and injections  with this detection Statistic
offSourceSelected.significance = offSourceSelected.likelihood(:,iDetStat);

for iWave = 1:length(autoFilesCell)
    for iInjScale = 1:nInjectionScales
        for iN = 1:nInjections(iWave)
            if isempty(clusterInj(iWave,iN,iInjScale).likelihood)
                clusterInj(iWave,iN,iInjScale).significance = [];
            else
                clusterInj(iWave,iN,iInjScale).significance = ...
                clusterInj(iWave,iN,iInjScale).likelihood(:,iDetStat);
            end
        end
    end
    clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]). ...
        significance = vertcat(clusterInj(iWave,:,:).significance);
end


fprintf(1,'Applying Alpha and Ratio cuts to UL off source\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Apply ratio and alpha cuts to off-source events.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

[loudestBackgroundAlpha,loudestBackgroundAlphaAll] = ...      
    xapplyalphacuts(offSourceSelected,...
                ePlusIndex,eCrossIndex,eNullIndex,...
                iPlusIndex,iCrossIndex,iNullIndex,...
                linearCutPlusTuned,linearCutCrossTuned,linearCutNullTuned,...
                alphaCutPlusTuned,alphaCutCrossTuned,alphaCutNullTuned,...
                  FAR_UL,typeOfCutPlus,typeOfCutCross); 
              
toc;


if sum(offSourceSelected.pass) == 0     
        warning('We have vetoed all of the offSourceSelected events.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            This is where it get a little tricky. If we are doing a
%            CLOSEDBOX analysis then we will need to overwite our onsource with
%            the events from the job number of the loudestBackgroundAlpha.
%            The injections then must be louder then the most significant
%            background (which is the value of 'loudestBackgroundAlpha').
%            If we are doing an open box then the injections must simply be
%            louder than the most significant surviving onsource event. So
%            we keep our onSource the way it is and make
%            loudestBackGroundAlpha the most significant surviving onSource
%            event.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(analysis.type,'closedbox')
    
    dummyOnSourceIndex = find(loudestBackgroundAlpha == offSourceSelected.significance);
    dummyOnSourceIndex = dummyOnSourceIndex(1);
    dummyOnSourceEvents = find(offSourceSelected.jobNumber == offSourceSelected.jobNumber(dummyOnSourceIndex));
    onSource = xclustersubset(offSourceSelected,dummyOnSourceEvents);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %            Now Apply ratio and alpha cuts to dummy on-source events
    %            if closedbox
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [onSource] = xapplyalphacutsonsource(onSource,...
            ePlusIndex,eCrossIndex,eNullIndex,...
            iPlusIndex,iCrossIndex,iNullIndex,...
            linearCutPlusTuned,linearCutCrossTuned,linearCutNullTuned,...
            alphaCutPlusTuned,alphaCutCrossTuned,alphaCutNullTuned,...
                typeOfCutPlus,typeOfCutCross);
    onSource.pass = min(onSource.pass,onSource.passAlphaCuts); 
    
else
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %            Apply ratio and alpha cuts to on-source events if open box
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [onSource] = xapplyalphacutsonsource(onSource,...
            ePlusIndex,eCrossIndex,eNullIndex,...
            iPlusIndex,iCrossIndex,iNullIndex,...
            linearCutPlusTuned,linearCutCrossTuned,linearCutNullTuned,...
            alphaCutPlusTuned,alphaCutCrossTuned,alphaCutNullTuned,...
                typeOfCutPlus,typeOfCutCross);
    onSource.pass = min(onSource.pass,onSource.passAlphaCuts); 
    loudestBackgroundAlpha = sort(onSource.significance*onSource.pass,'descend');
    
end

for iWave = 1:length(autoFilesCell)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %            Apply ratio and alpha cuts to injection triggers.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        fprintf(1,'Applying Alpha Cuts to waveform %d \n', iWave);

        % ---- Precompute coherent consistency test results for speed (vectorization)
        %tic;

        [passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)])] = ...
            xapplyalphacutsinjections(clusterPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]),...
                ePlusIndex,eCrossIndex,eNullIndex,...
                iPlusIndex,iCrossIndex,iNullIndex,...
                linearCutPlusTuned,linearCutCrossTuned,linearCutNullTuned,...
                alphaCutPlusTuned,alphaCutCrossTuned,alphaCutNullTuned,...
                  loudestBackgroundAlpha(1),typeOfCutPlus,typeOfCutCross);
         toc;
end
for iWave = 1:length(autoFilesCell)
       fprintf(1,'Assigning Values to clusters in waveform %d \n', iWave);
       % ---- iterator on precomputed outputs
        iCluster=0;

        % ---- Loop over injection scales and injection number.
        for iInjScale = 1:nInjectionScales
            for iN = 1:nInjections(iWave)
                % ---- Find range of precomputed values
                precomputeRange = iCluster+1:iCluster+length(clusterInj(iWave,iN,iInjScale).pass);
                iCluster = iCluster + length(clusterInj(iWave,iN,iInjScale).pass);
                % -- work around 0x1 and 0x0 empty matrices not being equal
                if length(precomputeRange) == 0
                  precomputeRange = [];
                end           


                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %   See if Cluster Passed Cut
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                clusterInj(iWave,iN,iInjScale).passAlphaandRatio = ...
                        passCohPrecompute.(['Wave' sprintf(['%', '.0f'], iWave)]).passAlphaCuts(precomputeRange,:);  


            end  %-- loop over injection number
        end  %-- loop over injection scales
end
    
    % Now we have applied window, DQ and Ratio/Significance Cuts.
    % Now, create a master pass only for those waveforms designated as
    % tuning by the injectionPrcoessedMask variable.
    % If it is in the UL set we will tick up the processed variable and
    % calculate the effiency.
    
processed =0;
analysisULEff =zeros(nInjectionScales,length(autoFilesCell));
for iWave = 1:length(autoFilesCell)
    for iInjScale=1:nInjectionScales
        for iN = 1:nInjections(iWave)
            [numClusters,~] = size(clusterInj(iWave,iN,iInjScale).passAlphaandRatio);
            if numClusters==0;
            else
                clusterInj(iWave,iN,iInjScale).passMaster = ...                 
                        clusterInj(iWave,iN,iInjScale).passInjCoinc.* ...
                        clusterInj(iWave,iN,iInjScale).passVetoSegs.* ...                    
                        clusterInj(iWave,iN,iInjScale).passAlphaandRatio;

            end
            if (injectionProcessedMask{iWave}(iN,iInjScale) ==1) && (numClusters ==0)
                processed = processed+1;
            elseif injectionProcessedMask{iWave}(iN,iInjScale) ==1
                processed = processed+1;                    
                analysisULEff(iInjScale,iWave) = ...
                        analysisULEff(iInjScale,iWave) + ...
                        (any(clusterInj(iWave,iN,iInjScale).passMaster~=0));
            else
            end
        end
        analysisULEff(iInjScale,iWave) = analysisULEff(iInjScale,iWave) ./ processed;
        processed = 0;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Find UL 50 and 90 percent and injection scale closest to 90 percent
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

analysisULEff50and90percent = 10*injectionScale(1,nInjectionScales)*...
    ones(2,length(autoFilesCell));
analysisULEff90percentindex = nInjectionScales * ones(1,length(autoFilesCell));
for iWave = 1:length(autoFilesCell)
    goFlag90 = 1;
    goFlag50 = 1;
    for iInjScale=(nInjectionScales):-1:2       
        % ---- Determine the Injection Scale for which
        % ---- The efficiency is above 50 percent
        % ---- Then record the cut for which this occurs
        % ---- If the cut never produces 50 percent efficiency then it
        % ---- stay at the worst injection scale. Start from the worst
        % (so the highest injection scale and go backwards

        cond = (analysisULEff(iInjScale,iWave) >= 0.5) & ...
            (analysisULEff(iInjScale-1,iWave) < 0.5) & (goFlag50==1);
        if any(cond)         
            % Found crossover point but what more precise 50 percent UL
            % so we will interpolate between injectionscale-1 and
            % injection scale. Our Interpolation will simply be a line
            % connection the two points.
            yDiff = (analysisULEff(iInjScale,iWave) - analysisULEff(iInjScale-1,iWave));
            xDiff = log10(injectionScale(iWave,iInjScale))-log10(injectionScale(iWave,iInjScale-1)); 

            interp50PercentSlope = yDiff./xDiff;
            injectionScale50Percent = log10(injectionScale(iWave,iInjScale-1)) + ...
                    ((0.5- analysisULEff(iInjScale-1,iWave))./ interp50PercentSlope);
            analysisULEff50and90percent(1,iWave) = (10.^injectionScale50Percent)';
            goFlag50 = 0;
        end
        
        cond = (analysisULEff(iInjScale,iWave) >= 0.9) & ...
            (analysisULEff(iInjScale-1,iWave) < 0.9) & (goFlag90==1);
        if any(cond) 
            % Found crossover point but what more precise 50 percent UL
            % so we will interpolate between injectionscale-1 and
            % injection scale. Our Interpolation will simply be a line
            % connection the two points.
            yDiff = (analysisULEff(iInjScale,iWave) - analysisULEff(iInjScale-1,iWave));
            xDiff = log10(injectionScale(iWave,iInjScale))-log10(injectionScale(iWave,iInjScale-1)); 

            interp90PercentSlope = yDiff./xDiff;
            injectionScale90Percent = log10(injectionScale(iWave,iInjScale-1)) + ...
                    ((0.9- analysisULEff(iInjScale-1,iWave))./ interp90PercentSlope);
            analysisULEff50and90percent(2,iWave) = (10.^injectionScale90Percent)';
            analysisULEff90percentindex(1,iWave) = iInjScale;
            goFlag90 = 0;
        end
    end
    cond = (analysisULEff(1,iWave) >= 0.5) & (goFlag50==1);
    if any(cond)
        analysisULEff50and90percent(1,iWave) = injectionScale(iWave,1);
    end
    cond = (analysisULEff(1,iWave) >= 0.9) & (goFlag90==1);
    if any(cond)
        analysisULEff50and90percent(2,iWave) = injectionScale(iWave,1);
        analysisULEff90percentindex(1,iWave) = 1;
    end
end

clear H1vetoSegList L1vetoSegList V1vetoSegList
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Save info for webpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(['EfficiencyData' vetoMethod],'analysisULEff','injectionScale',...
    'SSFEULAlphaPerDetStat','SSFEULRatioPerDetStat',...
    'linearCutPlusTuned','linearCutCrossTuned','linearCutNullTuned',...
    'alphaCutPlusTuned','alphaCutCrossTuned','alphaCutNullTuned','detectionStat',...
    'ePlusIndex','iPlusIndex','eCrossIndex','iCrossIndex','eNullIndex','iNullIndex',...
    'injIdx_tuning','nInjectionsBy2','nInjections','injIdx_ulCalc','injSeed','injectionFilesCell',...
    'offJobSeed','uniqueOffJobNumber','uniqueOffJobNumberFail','offJob_tuning','offJob_ulCalc',...
    'FAR_UL','FAR_Tuning','onSource','onJobsDeadtime','offJobsDeadtime','window',...
    'offIdx_tuning','nOffJob_ulCalc','offIdx_ulCalc','nJobFilesProcessed','nOffJob_tuning','offSourceSelected',...
    'analysisULEff50and90percent','analysisULEff90percentindex','dummyOnSourceIndex','loudestBackgroundAlphaAll',...
    'loudestBackgroundAlphaAll','analysisRatioEff95percent','analysisAlphaEff50percent',...
    'alphaCutNullRange','alphaCutPlusRange','alphaCutCrossRange',...
    'linearCutNullRange','linearCutPlusRange','linearCutCrossRange','detIndex')

disp('Post Processing Finished! The Webpage is now being made');
toc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Run Webpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xproducewebpage(user_tag,analysis,vetoSegList,clusterInj)
toc;

disp('X-webpage made successfully');
return

function mask = rectintersect(A, B)
% RECTINTERSECT Rectangle intersection
%
%   Returns bool table of intersection, for speed A is assumed to contain
%   only on rectangle
%
%  mask = rectintersect(A, B)
%
%  A      1-by-4 matrix giving rectangle in [X,Y,WIDTH,HEIGHT] format
%  B      N-by-4 matrix giving rectangles in [X,Y,WIDTH,HEIGHT] format
%
%  mask   bool vector of intersection between A and rectangles in B

if size(A,1) ~= 1
  error(['Number of rectangles in A is ' int2str(size(A,1)) ...
         ', it should be 1.']);
end

horMask = (A(1) <= B(:,1) & B(:,1) < A(1) + A(3)) | ...
          (B(:,1) <= A(1) & A(1) < B(:,1) + B(:,3));
verMask = (A(2) <= B(:,2) & B(:,2) < A(2) + A(4)) | ...
          (B(:,2) <= A(2) & A(2) < B(:,2) + B(:,4));
mask = horMask & verMask;

