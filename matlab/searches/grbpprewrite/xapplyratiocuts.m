function [loudestBackgroundRatioOut]= ...
                xapplyratiocuts(cluster,...
                    ePlusIndex,eCrossIndex,eNullIndex,...
                    iPlusIndex,iCrossIndex,iNullIndex,...
                    vetoPlusRange,vetoCrossRange,vetoNullRange,...
                       FAR_Tuning,typeOfCutPlus,typeOfCutCross)

% Calculate all Ratio Values

plusRatioEoverI  = log(cluster.likelihood(:,ePlusIndex)./cluster.likelihood(:,iPlusIndex));

crossRatioEoverI = log(cluster.likelihood(:,eCrossIndex)./cluster.likelihood(:,iCrossIndex));

plusRatioIoverE  = log(cluster.likelihood(:,iPlusIndex)./cluster.likelihood(:,ePlusIndex));

crossRatioIoverE = log(cluster.likelihood(:,iCrossIndex)./cluster.likelihood(:,eCrossIndex));


if iNullIndex == 0
    nullRatioIoverE = ones(size(plusRatioEoverI));    
else
    nullRatioIoverE = log(cluster.likelihood(:,iNullIndex)./cluster.likelihood(:,eNullIndex));
end


% Determine unique jobNumbers
uniqueJobNumber = unique(cluster.jobNumber);

% Depending on wether you are requesting a two sided or one sided cut. 
% construct specific ratio and cut arrays.

if strcmp(typeOfCutPlus,'twosided')
    
    ratioCutsPlus   = log(vetoPlusRange);
    ratioCutsCross  = log(vetoCrossRange);
    ratioCutsNull   = log(vetoNullRange);
    ratioArrayPlus  = [plusRatioEoverI,plusRatioIoverE];
    ratioArrayCross = [crossRatioEoverI,crossRatioIoverE];
    ratioArrayNull  = nullRatioIoverE;
    sidedCut        = 2;
    
else
    
    ratioCutsPlus   = log(abs(vetoPlusRange));
    ratioCutsCross  = log(abs(vetoCrossRange));
    ratioCutsNull   = log(abs(vetoNullRange));
    if strcmp(typeOfCutPlus,'EoverI')
        ratioArrayPlus  = plusRatioEoverI;
    else
        ratioArrayPlus  = plusRatioIoverE;
    end   
    if strcmp(typeOfCutCross,'EoverI')
        ratioArrayCross = crossRatioEoverI;
    else
        ratioArrayCross = crossRatioIoverE;
    end   
    ratioArrayNull  = nullRatioIoverE;
    sidedCut        = 1;
    

end
    
for iJob = 1:length(uniqueJobNumber)
    % find indicies associated with jobNumber
    ijobNumber               = uniqueJobNumber(iJob);
    iUniqueJobNumber         = find(cluster.jobNumber == ijobNumber);
    ratioArrayPlusJobNumber  = ratioArrayPlus(iUniqueJobNumber,:);
    ratioArrayCrossJobNumber = ratioArrayCross(iUniqueJobNumber,:);
    ratioArrayNullJobNumber  = ratioArrayNull(iUniqueJobNumber,:);
    
    % Reshape Calculated Ratios and Cut Values such that they are the same size
    % Do this by repating the ratio array # of Cuts times and by
    % repeating each cut combination # of ratioarray values.

    ratioArrayTempPlus   = repmat(ratioArrayPlusJobNumber,length(ratioCutsPlus),1);
    ratioArrayTempCross  = repmat(ratioArrayCrossJobNumber,length(ratioCutsCross),1);
    ratioArrayTempNull   = repmat(ratioArrayNullJobNumber,length(ratioCutsNull),1);
    
    vetoPlusRangeRep     = kron(ratioCutsPlus,ones(length(ratioArrayPlusJobNumber),sidedCut));
    vetoPlusCrossRep     = kron(ratioCutsCross,ones(length(ratioArrayCrossJobNumber),sidedCut));
    vetoPlusNullRep      = kron(ratioCutsNull,ones(length(ratioArrayNullJobNumber),1));
    
    % Determine what clusters passed all the Ratio cuts
    ratioPassCut = min(...
        min(...
        (sum(ratioArrayTempPlus  > vetoPlusRangeRep,2)>0),...
        (sum(ratioArrayTempCross > vetoPlusCrossRep,2)>0)...
        ),...
        (sum(ratioArrayTempNull  > vetoPlusNullRep ,2)>0)...
            );

    % Reshape the pass cuts into matrix of 1s and 0s with dimensions
    % # of clusters X # of cuts

    ratioPassCutReshape = reshape(ratioPassCut,...
        length(ratioArrayPlusJobNumber),length(ratioCutsPlus));

    % Find Surviving Offsource
    backGroundArray= ratioPassCutReshape.*...
        repmat(cluster.significance(iUniqueJobNumber,:),1,length(ratioCutsPlus));

    % Find loudest for each column

    loudestBackgroundRatioJob = max(backGroundArray)';

    loudestBackgroundRatio.(['job' sprintf(['%', '.0f'], ijobNumber)]) = ...
                        loudestBackgroundRatioJob;

end

% Now Find the FAR loudest background per Cut
% Essentially this means you construct get a 
% jobnumbers by cuts array for which you will take the
% FAR/100* total jobNumbers index from each column

indexFARBackground = ceil((FAR_Tuning/100)*length(uniqueJobNumber));
loudestBackgroundPerRatioCut = zeros(length(uniqueJobNumber),length(vetoPlusRange));


for iJob = 1:length(uniqueJobNumber)
    % find indicies associated with jobNumber
    ijobNumber          = uniqueJobNumber(iJob);

    % Place row vector of loudest background for given job.
    loudestBackgroundPerRatioCut(iJob,:) = ...
        loudestBackgroundRatio.(['job' sprintf(['%', '.0f'], ijobNumber)])';

end

% Order Columns from highest to lowest signifance
loudestBackgroundPerRatioCut = sort(loudestBackgroundPerRatioCut,'descend');

% Pick theindexFAR row

loudestBackgroundRatioOut= loudestBackgroundPerRatioCut(indexFARBackground,:);
