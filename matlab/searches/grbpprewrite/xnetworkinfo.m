function [FpList,FcList,dF,S,Snull,wFpDPmean,wFcDPmean,wFbDPmean] = xnetworkinfo(analysis)
% XWRITENETWORKINFO - Write information about the detector network to a webpage.
% xwritenetworkinfo is a helper function for xmakegrbwebpage.
%
% Usage:
%
%  [FpList,FcList,dF,S,Snull] = xwritenetworkinfo(analysis,varargin)
%
%    analysis           Structure created in xmakegrbwebpage.
%    varargin           Optional.  If specfied, must consist of the following 
%                       three inputs:
%                         fout: Pointer to webpage html file, must be open and 
%                           writable.
%                         figures_dirName: String. Name of directory where 
%                           image files (.png) are stored.
%                         figfiles_dirName: String. Name of directory where 
%                           matlab figure files (.fig) are stored.
%
%    FpList             Array, plus polarization antenna response value 
%                       for each detector in our network 
%    FcList             Array, cross polarization antenna response value 
%                       for each detector in our network 
%    dF                 Double, best frequency resolution 
%    S                  Array, sensitivity curve at best frequency
%                       resolution
%    Snull              Array, sensitivity in incoherent null streams at
%                       best frequency resolution
%
% $Id: xwritenetworkinfo.m 4832 2015-04-13 13:05:05Z scott.coughlin@LIGO.ORG $

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Find antenna responses for each detector in Earth-based
%      polarization frame (the default for X-Pipeline).
for iDetector = 1:length(analysis.detectorList)
    [Fp, Fc, Fb] = ComputeAntennaResponse(analysis.skyPositions(1,2), ...
        analysis.skyPositions(1,1),0,analysis.detectorList{iDetector});
    FpList(1,iDetector) = Fp;
    FcList(1,iDetector) = Fc;
    FbList(1,iDetector) = Fb;
end
clear Fp Fc Fb

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Make noise spectra & coherent projection plots for web page.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ------------------------------------------------------------------------
%    Preparatory
% ------------------------------------------------------------------------

% ---- Find the detector spectra with the best frequency resolution (for
%      plots and for estimating SNRs).
bATi = 1; maxPrec = 0;
for i = 1:length(analysis.amplitudeSpectraCell)
    if (maxPrec < length(analysis.amplitudeSpectraCell{i}))
        maxPrec = length(analysis.amplitudeSpectraCell{i});
        bATi = i;
    end
end
dF = 1/analysis.analysisTimes(bATi);

% ---- Record spectra and Gaussianity / Rayleigh "spectra" for best
%      frequency resolution.
if isfield(analysis,'gaussianityCell') && ~isempty(analysis.gaussianityCell)
    gaussianity = squeeze(analysis.gaussianityCell{bATi});
end
S = analysis.amplitudeSpectraCell{bATi}.^2;

% ---- Convert antenna responses in the dominant-polarization frame, after
%      weighting by the noise spectra.
%      Array entries are (frequency bin) x (detector).
wFp = repmat(FpList(1,:),[size(S,1) 1]) ./ sqrt(S);
wFc = repmat(FcList(1,:),[size(S,1) 1]) ./ sqrt(S);
wFb = repmat(FbList(1,:),[size(S,1) 1]) ./ sqrt(S);
[wFpDP wFcDP] = convertToDominantPolarizationFrame(wFp,wFc);
wFbDP = wFb;
clear wFp wFc wFb
% ---- Normalized antenna response vectors ('Hat' := unit vector).
wFpDPHat = wFpDP./repmat(sum(wFpDP.^2,2).^0.5,[1,length(analysis.detectorList)]);
wFcDPHat = wFcDP./repmat(sum(wFcDP.^2,2).^0.5,[1,length(analysis.detectorList)]);
wFbDPHat = wFbDP./repmat(sum(wFbDP.^2,2).^0.5,[1,length(analysis.detectorList)]);
% ---- Compute mean antenna response in DP frame
wFpDPmean = mean(wFpDP,1);
wFcDPmean = mean(wFcDP,1);
wFbDPmean = mean(wFbDP,1);

% ------------------------------------------------------------------------
%      Plots of null stream projection components vs frequency.
% ------------------------------------------------------------------------

% ---- Check for H1H2-only network.

% ---- Only make these plots if we have analysis.eNullIndex.
if length(analysis.detectorList)>3

    % ---- Null stream.  Only make this plot if we have analysis.eNullIndex

    A = abs(1 - wFpDPHat.^2 - wFcDPHat.^2);

    % ---- Sensitivity in incoherent null streams
    Snull = S./A;
% ---- Pass out empty Snull array if we cannot construct null stream
else
    Snull = [];
end

