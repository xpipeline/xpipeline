function [] = xplotonsourcevent(event_path, onSource,onSourceIndex,detectionStat,analysis,...
    figfiles_dirName,figures_dirName)

% XPLOTONSOURCEEVENT - gather event parameters from post-processing results


fout = fopen(event_path,'w');

header_text = ['Event Display for '  num2str(onSource.peakTime(onSourceIndex)) ...
    ' from ' analysis.type  ' Analysis of '  analysis.grb_name];

fprintf(fout,'%s\n',['<!DOCTYPE HTML PUBLIC "-//W3C//DTD '...
                     'HTML 4.01 Transitional//EN">']);
fprintf(fout,'%s\n','<html>');
fprintf(fout,'%s\n','<head>');
s = pwd;
fprintf(fout,'%s\n',['  <title> ' s ' </title>']);
fprintf(fout,'%s\n','  <style type="text/css">');
fprintf(fout,'%s\n','body {');
fprintf(fout,'%s\n','font-family: Tahoma,Geneva,sans-serif;');
fprintf(fout,'%s\n','color: black;');
fprintf(fout,'%s\n','background-color: white;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','h1 {');
fprintf(fout,'%s\n','color: #ffffff;');
fprintf(fout,'%s\n','background-color: #000088;');
fprintf(fout,'%s\n','padding: 0.25em;');
fprintf(fout,'%s\n','border: 1px solid black;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','h2 {');
fprintf(fout,'%s\n','color: #000000;');
fprintf(fout,'%s\n','background-color: #cccccc;');
fprintf(fout,'%s\n','padding: 0.25em;');
fprintf(fout,'%s\n','border: 1px solid black;');
fprintf(fout,'%s\n','}');
fprintf(fout,'%s\n','  </style>');

fprintf(fout,'%s\n',' <script type="text/javascript">');
fprintf(fout,'%s\n',' function toggleVisible(division) {');
fprintf(fout,'%s\n',[' if (document.getElementById("div_" + '...
                    'division).style.display == "none") {']);
fprintf(fout,'%s\n',[' document.getElementById("div_" + '...
                     'division).style.display = "block";']);
fprintf(fout,'%s\n',[' document.getElementById("input_" + '...
                     'division).checked = true;']);
fprintf(fout,'%s\n',' } else {');
fprintf(fout,'%s\n',[' document.getElementById("div_" + '...
                     'division).style.display = "none";']);
fprintf(fout,'%s\n',[' document.getElementById("input_" + '...
                     'division).checked = false;']);
fprintf(fout,'%s\n',' }');
fprintf(fout,'%s\n',' }');
fprintf(fout,'%s\n',' </script>');

fprintf(fout,'%s\n','</head>');
fprintf(fout,'%s\n','<body>');

fprintf(fout,'%s\n',['<h1>' header_text '<h1>'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Write Analysis Parameters 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<h2>Analysis Paramters</h2>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> GRB GPSTime </b></td>']);
fprintf(fout,'%s\n',['<td><b> RA [deg] </b></td>']);
fprintf(fout,'%s\n',['<td><b> Dec [deg] </b></td>']);
fprintf(fout,'%s\n',['<td><b> Localization Error [deg] </b></td>']);
fprintf(fout,'%s\n',['<td><b> Search Grid Positions </b></td>']);
for iDet = 1:length(analysis.detectorList)
    fprintf(fout,'%s\n',['<td><b> IFO' num2str(iDet) '</b></td>']);
end
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> ' num2str(analysis.gpsCenterTime) ' </td>']);
[ra,dec] = earthtoradec(analysis.skyPositions(2),analysis.skyPositions(1),analysis.gpsCenterTime);
fprintf(fout,'%s\n',['<td> ' num2str(ra) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(dec) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(analysis.minimumFrequency) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(analysis.minimumFrequency) ' </td>']);
for iDet = 1:length(analysis.detectorList)
    fprintf(fout,'%s\n',['<td> ' analysis.detectorList{iDet} ' </td>']);
end
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Write Event Parameters 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fout,'%s\n','<h2>Event Parameters</h2>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Peak Timee </b></td>']);
fprintf(fout,'%s\n',['<td><b> Signifiance </b></td>']);
fprintf(fout,'%s\n',['<td><b> Probability </b></td>']);
fprintf(fout,'%s\n',['<td><b> Freq. Resolution at Max Likelihood [Hz] </b></td>']);
fprintf(fout,'%s\n',['<td><b> Peak Frequency [Hz] </b></td>']);
fprintf(fout,'%s\n',['<td><b> Duration [ms] </b></td>']);
fprintf(fout,'%s\n',['<td><b> Low Frequency [Hz] </b></td>']);
fprintf(fout,'%s\n',['<td><b> Upper Frequency [Hz] </b></td>']);
fprintf(fout,'%s\n',['<td><b> # of TF Pixels </b></td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td> ' num2str(onSource.peakTime(onSourceIndex)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onSource.significance(onSourceIndex)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onSource.probability(onSourceIndex)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(1/onSource.analysisTime(onSourceIndex)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(1/onSource.peakFrequency(onSourceIndex)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onSource.boundingBox(onSourceIndex,3)*1000) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onSource.boundingBox(onSourceIndex,2)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onSource.boundingBox(onSourceIndex,2)+...
                                onSource.boundingBox(onSourceIndex,4)) ' </td>']);
fprintf(fout,'%s\n',['<td> ' num2str(onSource.nPixels(onSourceIndex)) ' </td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
for iLikeIndex = 1:length(analysis.likelihoodType)
    fprintf(fout,'%s\n',['<td> ' analysis.likelihoodType{iLikeIndex} ' </td>']);
end
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
for iLikeIndex = 1:length(analysis.likelihoodType)
    fprintf(fout,'%s\n',['<td> ' num2str(onSource.likelihood(onSourceIndex,iLikeIndex)) ' </td>']);
end
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');

fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
fprintf(fout,'%s\n',['<tr>']);
fprintf(fout,'%s\n',['<td><b> Pass Window? </b></td>']);
fprintf(fout,'%s\n',['<td><b> Pass Veto Segs? </b></td>']);
fprintf(fout,'%s\n',['<td><b> Pass Linear or Alpha Cut? </b></td>']);
fprintf(fout,'%s\n',['<td><b> Pass? </b></td>']);
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n',['<tr>']);
if onSource.pass(onSourceIndex)==1
    fprintf(fout,'%s\n',['<td> Yes </td>']);
    fprintf(fout,'%s\n',['<td> Yes </td>']);
    fprintf(fout,'%s\n',['<td> Yes </td>']);
    fprintf(fout,'%s\n',['<td> Yes </td>']);
else
    if onSource.passVetoSegs(onSourceIndex) ==1
        fprintf(fout,'%s\n',['<td> Yes </td>']);
    else
        fprintf(fout,'%s\n',['<td> No </td>']);
    end
    if onSource.passWindow(onSourceIndex) ==1
        fprintf(fout,'%s\n',['<td> Yes </td>']);
    else
        fprintf(fout,'%s\n',['<td> No </td>']);
    end
    if onSource.passAlphaCuts(onSourceIndex) ==1
        fprintf(fout,'%s\n',['<td> Yes </td>']);
    else
        fprintf(fout,'%s\n',['<td> No </td>']);
    end
    fprintf(fout,'%s\n',['<td> No </td>']);
end
fprintf(fout,'%s\n',['</tr>']);
fprintf(fout,'%s\n','</table><br>');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Detection Statistics TF-Map various Time Resolutions 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(analysis.type,'closedbox')
    fileID = fopen('../input/event_off_source.txt','r');
    blockTime = analysis.onSourceWindowLength/2;
    if length(analysis.detectorList) == 2
        GPSonSource = textscan(fileID,'%f %f %f');
    elseif length(analysis.detectorList) == 3
        GPSonSource = textscan(fileID,'%f %f %f %f');
    else
	error('too many detectors')
    end
    for onSourceTimes = 1:length(GPSonSource{1})
	if length(analysis.detectorList) == 2
        cond = (onSource.peakTime(onSourceIndex) <= (GPSonSource{1}(onSourceTimes)+blockTime)) & ...
            (onSource.peakTime(onSourceIndex) >= (GPSonSource{1}(onSourceTimes)-blockTime)) & ...
            (onSource.timeOffsets(onSourceIndex,1) == (GPSonSource{2}(onSourceTimes))) & ...
            (onSource.timeOffsets(onSourceIndex,2) == (GPSonSource{3}(onSourceTimes))) ;
	elseif length(analysis.detectorList) == 1
        cond = (onSource.peakTime(onSourceIndex) <= (GPSonSource{1}(onSourceTimes)+blockTime)) & ...
            (onSource.peakTime(onSourceIndex) >= (GPSonSource{1}(onSourceTimes)-blockTime))
	else
        cond = (onSource.peakTime(onSourceIndex) <= (GPSonSource{1}(onSourceTimes)+blockTime)) & ...
            (onSource.peakTime(onSourceIndex) >= (GPSonSource{1}(onSourceTimes)-blockTime)) & ...
            (onSource.timeOffsets(onSourceIndex,1) == (GPSonSource{2}(onSourceTimes))) & ...
            (onSource.timeOffsets(onSourceIndex,2) == (GPSonSource{3}(onSourceTimes))) & ...
            (onSource.timeOffsets(onSourceIndex,3) == (GPSonSource{4}(onSourceTimes)));
	end
        if cond
            break
        else
        end
    end
    clear fileID

    fileID = fopen('../input/event_follow_up_on_source.txt','w');
    fileID2 = fopen(['../input/parameters_off_source_0.txt'],'r');
    originalParams = textscan(fileID2,'%s');

    for iParams = 1:length(originalParams{1})

       if ~(isempty(regexp(originalParams{1}{iParams},'channelFileName.*', 'once')))
           fprintf(fileID,'channelFileName:../input/channels.txt\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'frameCacheFile.*', 'once')))
           fprintf(fileID,'frameCacheFile:../input/framecache.txt\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'eventFileName.*', 'once')))
           fprintf(fileID,'eventFileName:../input/event_off_source.txt\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'outputtype.*', 'once')))
           fprintf(fileID,'outputtype:timefrequencymap\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'skyPositionList:input.*', 'once')))
           fprintf(fileID,'skyPositionList:../input/sky_positions_0.txt\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'likelihoodtype:.*', 'once')))
           fprintf(fileID,['likelihoodtype:' detectionStat{1} '\n'])
       elseif ~(isempty(regexp(originalParams{1}{iParams},'circtimeslidestep:.*', 'once')))
	   fprintf(fileID,['circtimeslidestep:0\n'])
       else
           fprintf(fileID,[originalParams{1}{iParams} '\n'])
       end
    end

    % Generate time frequencymap for this onSource event.
    if (exist(['./event/results_' num2str(onSourceTimes-1) '.mat'],'file')==2)
    else
    [status] = system(['$XPIPE_INSTALL_BIN/xdetection ../input/event_follow_up_on_source.txt ' num2str(onSourceTimes-1) '-' num2str(onSourceTimes-1) ' ./event/ ' num2str(0)])
    end
else
    fileID = fopen('../input/event_on_source.txt','r');
    GPSonSource = textscan(fileID,'%f %f %f');
    blockTime = analysis.onSourceWindowLength/2;
    for onSourceTimes = 1:length(GPSonSource{1})
        cond = onSource.peakTime(onSourceIndex) <= (GPSonSource{1}(onSourceTimes)+blockTime) & ...
            onSource.peakTime(onSourceIndex) >= (GPSonSource{1}(onSourceTimes)-blockTime);
        if cond
            break
        else
        end
    end
    clear fileID

    fileID = fopen('../input/event_follow_up_on_source.txt','w');
    fileID2 = fopen(['../input/parameters_on_source_' num2str(onSourceTimes-1) '.txt'],'r');
    originalParams = textscan(fileID2,'%s');

    for iParams = 1:length(originalParams{1})

       if ~(isempty(regexp(originalParams{1}{iParams},'channelFileName.*', 'once')))
           fprintf(fileID,'channelFileName:../input/channels.txt\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'frameCacheFile.*', 'once')))
           fprintf(fileID,'frameCacheFile:../input/framecache.txt\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'eventFileName.*', 'once')))
           fprintf(fileID,'eventFileName:../input/event_on_source.txt\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'outputtype.*', 'once')))
           fprintf(fileID,'outputtype:timefrequencymap\n')
       elseif ~(isempty(regexp(originalParams{1}{iParams},'skyPositionList:input.*', 'once')))
           fprintf(fileID,'skyPositionList:../input/sky_positions_0.txt\n')
       else
           fprintf(fileID,[originalParams{1}{iParams} '\n'])
       end

    end

    % Generate time frequencymap for this onSource event.
    if (exist(['./event/results_' num2str(onSourceTimes-1) '.mat'],'file')==2)
    else
    [status] = system(['$XPIPE_INSTALL_BIN/xdetection ../input/event_follow_up_on_source.txt ' num2str(onSourceTimes-1) ' ./event/ ' num2str(0)]);
    end
end

fprintf(fout,'%s\n',['<h2>Likelihood TF-Maps For Best Time Resolution:'...
    num2str(onSource.analysisTime(onSourceIndex)) '</h2>']);

load(['./event/results_' num2str(onSourceTimes-1) '.mat']);

% first convert the useful cell arrays into normal arrays
analysisTimes = cell2mat(analysisTimesCell);
analysisIdx   = find(analysisTimes == onSource.analysisTime(onSourceIndex));

blockLength = blockTime*sampleFrequency;
nyquistFrequency = sampleFrequency / 2;
frequencyBand = [minimumFrequency, maximumFrequency];

plot_start = onSource.peakTime(onSourceIndex) - 20*analysisTimes(analysisIdx);
plot_stop  = onSource.peakTime(onSourceIndex) + 20*analysisTimes(analysisIdx);

% work out the frequency bins that are returned for this likelihood map

integrationLength = analysisTimes(analysisIdx) * sampleFrequency;
oneSidedFrequencies = nyquistFrequency * (0:integrationLength/2) / (integrationLength/2);
frequencyIndex = find( (oneSidedFrequencies>=frequencyBand(1)) & (oneSidedFrequencies<=frequencyBand(2)) );
inbandFrequencies = oneSidedFrequencies(frequencyIndex);

% work out the time bins that are returned for this TFmap

iStart = [transientLength+1:integrationLength:blockLength-transientLength-integrationLength+1];
iStop = iStart + integrationLength - 1;
segmentIndices = [ iStart' iStop' ];
numberOfSegments = size(segmentIndices, 1);
numberOfTimeBins = max(numberOfSegments-1,1)/offsetFraction;
firstTimeBinCenterIndex = segmentIndices(1, 1) + integrationLength/2;
firstTimeBinCenterTime = (firstTimeBinCenterIndex - 1)/ sampleFrequency;
timeBins = [1:1/offsetFraction:numberOfTimeBins];

% the center time of the first time-frequency bin of the map is startTime + firstTimeBinCenterTime
% the center time of the last TF bin is startTime + firstTimeBinCenterTime + (numberOfTimeBins-1)*analysisTime(analysisIdx)(timeIDX)*offsetFraction
% for some reason the last bin is dropped?

t_labels = (0:(numberOfTimeBins-1))*analysisTimes(analysisIdx)*offsetFraction+firstTimeBinCenterTime;

plot_start_idx = round((plot_start - firstTimeBinCenterTime - startTime)/(analysisTimes(analysisIdx)*offsetFraction));
plot_stop_idx = round((plot_stop - firstTimeBinCenterTime - startTime)/(analysisTimes(analysisIdx)*offsetFraction));

Z = likelihoodMapCell{analysisIdx};

% ---- Start the figure
figure; 
x = t_labels(plot_start_idx:plot_stop_idx) - (onSource.peakTime(onSourceIndex)-startTime);
y = inbandFrequencies;

for likeIDX = 1:length(likelihoodType)
    
    % keep all the frequencies and zoom in on the time axis
    TFmap = Z(:,plot_start_idx:plot_stop_idx,likeIDX);
    subplot(ceil(sqrt(length(likelihoodType))),ceil(sqrt(length(likelihoodType))),likeIDX)
    imagesc(x,y,TFmap);
    colormap jet;
    set(gca,'YDir','normal');
    h = colorbar;
    set(h,'fontsize',20);
    ylim([onSource.boundingBox(onSourceIndex,2),onSource.boundingBox(onSourceIndex,2)+onSource.boundingBox(onSourceIndex,4)])
    hold on;
    grid off;
    xlabel('Time from event peak (seconds)');
    ylabel('Frequency (Hz)');
    title([likelihoodType{likeIDX}]);
end

plotName = ['events/' num2str(onSource.peakTime(onSourceIndex)) '_'...
     num2str(onSource.analysisTime(onSourceIndex)) '_peakdT'];
% ---- Save plot.
% ---- Hardwired formatting.
plot_fullsize = [0 0 50 30];
plot_thumbsize = [0 0 50 30];
resolution_dpi = 150;  %-- matlab default is 150

% ---- Save current figure.
saveas(gcf,[figfiles_dirName plotName '.fig'],'fig')
set(gcf,'PaperUnits','inches','PaperPosition',plot_fullsize)
print('-dpng',['-r' num2str(resolution_dpi)],[figures_dirName plotName '.png']);
legend('off')
set(gcf,'PaperUnits','inches','PaperPosition',plot_thumbsize)
print('-dpng',['-r' num2str(resolution_dpi)],[figures_dirName plotName '_thumb.png']);
close

fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="../' figures_dirName ...
plotName '.png"><IMG SRC="../' figures_dirName plotName ...
'_thumb.png" WIDTH=1500 '...
'ALT="poop" TITLE="poop"></A></TD>']);


