This folder is meant to be the initial test bed for debugging and compiling the rewritten grb code. This new grb code is meant to be very intuitive and faster than the original X pp and webpage code. Instea dof multiple dags that produce various mat files and make the pp folder quite busy, this pp and webpage takes ONE job although with a very high virtual memory rewuirement. However I think the speed and amount of nodes neede dmake requesting a super high memory node not a big deal. I will hopefully have this working and compiled in this branch by the end of the weekend and than transport it to trunk.

Example

MATLAB input

% (autoFileName, vetoMethod, detectionStatCell,...
%    H1vetoSegList, L1vetoSegList, V1vetoSegList, ...
%    FAR_Tuning,FAR_UL,analysisType,user_tag,grb_name)

% Set up to run on cluster.
Two options. Build it yourself. Or source my (Scott Coughlin) build.

Option 1: Normal ./configure stuff and then 
	make install-grbpprewrite

Option 2:
	source /home/scoughlin/opt/xpipeline/webpagerewritetest/envsetup.sh

To set up the auto_web folder to run the post processing here is an example

xgrbpp.py  -g SNEWS -d /home/scoughlin/CardiffMphil/SNEWS/H1L1/XSN/RA_157_DEC-57_SN/ -a /home/scoughlin/CardiffMphil/SNEWS/H1L1/XSN/RA_157_DEC-57_SN/ -w /home/scoughlin/public_html/ -l /usr1/scoughlin/log/ -u RA_157_DEC-57_SN  -c alphaLinCut -p 1-1 -s powerlaw~loghbayesian

As you can see it is similar to the set up before but with two big
differences.

First, you MUST specify a deteciton/ranking statistic. If you want to loop
over mor than one then separte them by tilde as above. Also the -p is now FAR
instead of FAP. So if you wanted the 99th percentile off source job as you
dummy on source you would do -p 1-1 as above instead of -p 99-99 as before.

The nice thing about the way the new pp works is you can run it locally now
problem. In the case of the example above simply dot he follwoing

cd auto_web

then run

xproducecuts xproducecuts_SNEWS_RA_157_DEC-57_SN.txt

This will do the whole post processing including webpage. Of source you coulda lso
submit it via codnor but the universe for this job is already set to local
because the memory is quite large, but it is only one job so it should not be
a problem.
