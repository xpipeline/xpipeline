function [UL90p,SNRlist,percentNans] = xmakesnwebpage(grb_name, user_tag,...
    autoFileName, vetoMethod, ...
    nullVetoType, vetoNullRangeInput, ...
    plusVetoType, vetoPlusRangeInput, ...
    crossVetoType, vetoCrossRangeInput, ...
    analysisType, closedBoxMethod, injectionType, ...
    H1vetoSegList, H2vetoSegList, L1vetoSegList, ...
    G1vetoSegList, V1vetoSegList, ...
    tuningNameStr, resultType, detectionStat, ...
    percentile_tuning, percentile_ulCalc)
% XMAKESNWEBPAGE - Create a report web page for the SN analysis.
%
% usage:
%
% [UL90p,SNRlist,percentNans] = xmakesnwebpage(grb_name, user_tag, ...
%     autoFileName, vetoMethod, nullVetoType, vetoNullRangeInput, ...
%     plusVetoType, vetoPlusRangeInput, crossVetoType, vetoCrossRangeInput, ...
%     analysisType, closedBoxMethod, injectionType, H1vetoSegList, ...
%     H2vetoSegList, L1vetoSegList, G1vetoSegList, V1vetoSegList, ...
%     tuningNameStr, resultType, detectionStat, percentile_tuning, percentile_ulCalc)
%
% or 
%
% [UL90p,SNRlist,percentNans] = xmakesnwebpage(file_name)
%
% grb_name            String. Name of GRB, e.g., GRB070201
%
% user_tag            String. Description of current analysis, e.g., TEST 
%
% autoFileName        String. Name of file listing results files, as
%                     produced by grb/makeAutoFiles2.py.
%
% vetoMethod          String. Determines what type of veto ratio
%                     test we will perform. Must be of the form
%                     '(medianAlpha|alphaLin|medianLin|linear|median|alpha)(CutCirc|CutAmp|Cut)'
%                     or 'None'
%
% nullVetoType        String. Either 'nullIoverE' or 'nullEoverI'
%                     Determines which ratio test to perform
%
% vetoNullRangeInput  String. List of thresholds to be used in conjuction
%                     with ratio test
%
% plusVetoType        String. Either 'plusIoverE', 'plusEoverI', 'ampEoverI' or 'circEoverI'
%                     Determines which ratio test to perform
%
% vetoPlusRangeInput  String. List of thresholds to be used in conjuction
%                     with ratio test
%
% crossVetoType       String. Either 'crossIoverE', 'crossEoverI', 'ampnullIoverE' or 'circnullIoverE'
%                     Determines which ratio test to perform
%
% vetoCrossRangeInput String. List of thresholds to be used in conjuction
%                     with ratio test
%
% analysisType        String. Optional. Possible values are:
%        
%                     'closedbox': The on source events are not used.  The 
%                     loudest on-source event is _estimated_ as the median
%                     of the loudest events of the background / off source
%                     segments (one loudest event per segment, and then
%                     take the median).
%
%                     'snrulclosedbox': Same as closedbox, however upper
%                     limits are not done on the injection scale but on the
%                     total SNR desposited in the network by the injections
%                     (see ulSNREsitmation for more details). This analysis
%                     type is EXPERIMENTAL.  If used, please note that
%                     plots displaying scatter plots with injections are
%                     wrong. In the table at the bottom values are all
%                     wrong. The only meaningful columns are the 50%, and
%                     90% UL on the injections scale, they contain the 50%
%                     and 90% UL on the total SNR deposited into the
%                     network. 
%
%                     'openbox': A standard open box statistical analysis
%                     is performed (i.e., the UL is computed using the
%                     loudest on source event).
%
%                     'ulbox': Same as 'openbox' but uses the "ul-source"
%                     triggers which were not decimated. Should ensure
%                     that the loudest event is really the loudest.
%
% closedBoxMethod     String. Used to determined how we choose which off
%                     source job is used as simulated on source when doing
%                     closed box analysis:
%
%                     'median' to use the off source job with the median
%                     loudest loudest event
%
%                     'job_<number>' to use job number specified
%
% injectionType       String. Used to determined type of injections that are
%                     being analysed:
%                    
%                     'internalInj' Use this option when
%                     analysing injections which were generated internally
%                     within xpipeline. xmakewaveform is used to 
%                     estimate injection waveform properties,
%
%                     'mdcInj' Use this option  when 
%                     analysing MDC injections. xmakewaveform is not used to 
%                     estimate injection waveform properties, these are 
%                     instead read from the MDC log files.
%
% H1vetoSegList ... V1vetoSegList
%                     Strings. Contain paths to veto segment lists, these
%                     should be ordered H1,H2,L1,G1,V1. We assume segment
%                     lists are in segwizard format and contain lists of
%                     times KILLED by the appropriate veto flags. Use
%                     'None' if you do not want to apply veto segs for a
%                     particular ifo. 
%
% tuningNameStr       String. We will tune our veto ratio cuts in order to 
%                     minimize the expected upper limit for these waveforms.
%                     e.g., 'SGC150Q8d9' or 'all' or tilde-delimited string 
%                     of waveform names (see "--tuning-waveforms" option in 
%                     xgrbwebpage.py).
%
% resultType          String. Determines type of output will be produced:
%
%                     'vetoTestOnly' xmakegrbwebpage will estimate upper 
%                     limits for the input veto cut combinations and
%                     write the results to matFiles. 
%                     
%                     'tunedUpperLimit' xmakegrbwebpage will test the
%                     input veto cut combinations, choose the combination 
%                     which minimises the upper limit and then generates
%                     a webpage showing results using this optimal veto
%                     cut combination
%
%                     'tunedUpperLimitExtended' same as 'tunedUpperLimit'
%                     but reads off-source jobs for which the coherent
%                     cuts have already been partialy applied. Read the
%                     median fitting parameters from file.
%
% detectionStat       String. Determines which statistic is used for the 
%                     significance. Use 'default' to obtain the usual 
%                     significance based on the first listed statistic 
%                     in grb.ini
%
% percentile_tuning   String. Percentile level to use when selecting bkg
%                     job for tuning
%
% percentile_ulCalc   String. Percentile level to use when selecting bkg
%                     job for UL estimation
%
% For the alternate usage:
%
% file_name           String. Name of text file containing the above input
%                     arguments.
%
% Comments will appear when the web page file name has a shtml extension
% and viewed through a web server like Apache; local opening of this web
% page won't display comments.html
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. Assign default values 
%      where needed.
if (nargin==1)
    disp(['Attempting to read arguments from the file ' grb_name '.']);
    fileID = fopen(grb_name,'r');
    fileContents = textscan(fileID,'%s');
    % ---- The deal() command below requires fileContents to have exactly 23
    %      elements (to match the number of outputs). 
    [grb_name, user_tag, autoFileName, vetoMethod, nullVetoType, vetoNullRangeInput, ...
    plusVetoType, vetoPlusRangeInput, crossVetoType, vetoCrossRangeInput, ...
    analysisType, closedBoxMethod, injectionType, ... 
    H1vetoSegList, H2vetoSegList, L1vetoSegList, G1vetoSegList, V1vetoSegList, ...
    tuningNameStr, resultType, detectionStat, percentile_tuning, ...
    percentile_ulCalc] = deal(fileContents{1}{:});
    fclose(fileID);
elseif (nargin==23)
    disp('Reading arguments from the command line.');
else
    error('Too many or too few input arguments')
end

fprintf(1,['Checking input arguments are valid ...']);

% ---- Check that analysis type is a recognized type.
if ~( strcmp(analysisType,'closedbox') | ...
      strcmp(analysisType,'snrulclosedbox') | ...
      strcmp(analysisType,'ulbox') | ...
      strcmp(analysisType,'openbox') )
   error('analysisType must be either closedbox, snrulclosedbox, ulbox or openbox')
end

% ---- Check that injectionType is correctly defined.
if ~( strcmp(injectionType,'mdcInj') | ...
      strcmp(injectionType,'internalInj') )
   error('injectionType must be either mdcInj or internalInj')
end

% ---- Set mdcInjectionFlag.
mdcInjectionFlag = 0;
if ( strcmp(injectionType,'mdcInj'))
    mdcInjectionFlag = 1;   
end

% ---- Check that resultType is correctly defined.
if ~( strcmp(resultType,'vetoTestOnly') | ...
      strcmp(resultType,'tunedUpperLimit') | ...
      strcmp(resultType,'tunedUpperLimitExtended'))
   error('resultType must be either vetoTestOnly or tunedUpperLimit or tunedUpperLimitExtended')
end

% ---- Check that vetoMethod is correctly defined.
if isempty(regexp(vetoMethod,['(medianAlpha|alphaLin|medianLin|' ...
                      'linear|median|alpha)(CutCirc|CutAmp|Cut)'])) && ...
        not(strcmp(vetoMethod,'None'))
   error(['vetoMethod must be either (medianAlpha|alphaLin|medianLin|linear|median|alpha)(CutCirc|CutAmp|Cut) or None'])
end

if ( nargin < 21 )
  detectionStat = 'default'
end
detectionStat = lower(detectionStat);

% ---- Set vetoTestOnlyFlag and produceWebpageFlag.
produceWebpageFlag = 1; % -- default is to produce webpage
vetoTestOnlyFlag = 0;
if (strcmp(resultType,'vetoTestOnly'))
    produceWebpageFlag = 0;
    vetoTestOnlyFlag = 1;
end

% ---- Convert vetoXXXXRangeInput strings to vectors
vetoNullRangeInput  = str2num(vetoNullRangeInput);
vetoPlusRangeInput  = str2num(vetoPlusRangeInput);
vetoCrossRangeInput = str2num(vetoCrossRangeInput);

% ---- Pack veto files into a single convenient cell array.
vetoSegList = {H1vetoSegList H2vetoSegList L1vetoSegList G1vetoSegList ...
    V1vetoSegList};

% ---- Check that nullVetoType is nullEoverI or nullIoverE.
if and(~strcmp(nullVetoType,'nullEoverI'),~strcmp(nullVetoType,'nullIoverE'))
   error('Invalid string for nullVetoType, must be nullEoverI or nullIoverE');
end

% ---- Check that plusVetoType is plusEoverI or plusIoverE.
if isempty(regexp(plusVetoType,'(plus|circ|amp)EoverI')) && ~strcmp(plusVetoType,'plusIoverE')
   error('Invalid string for plusVetoType, must be plusEoverI or plusIoverE');
end

% ---- Check that crossVetoType is crossEoverI or crossIoverE.
if ~strcmp(crossVetoType,'crossEoverI') && ...
      isempty(strcmp(crossVetoType,'(cross|circnullreg|circnull|ampnull)IoverE'))
   error(['Invalid string for crossVetoType, must be crossEoverI, crossIoverE, ' ...
         'circnullIoverE or circnullregIoverE']);
end

fprintf(1,'done! \n')

% ---- Formatting of stdout messages from this function.
format short
format compact


% ---- We use an asymmetric onSource time "window" which starts 120s before 
%      the GRB trigger time and ends 60s after the GRB trigger time. 
%      We discard triggers in the onSource block which do not overlap 
%      with this window.
%      To treat offSource and injection blocks consistently we will
%      discard events which do not overlap with a window centred
%      on a given block's centerTime
% ---- Number of seconds before centerTime that window begins,
%      should always be +ve
window.offset = 120;
% ---- Duration of window in seconds
window.duration = 180;
% Override the above defaults with values in the parameter files
analysis.onSourceBeginOffset = [];
analysis.onSourceEndOffset = [];
%----- Read each line into separate element of a cell array. 
parameterFileName='../input/parameters_off_source_0.txt';
display(['Reading parameters from ' parameterFileName])
paramLines = textread(parameterFileName,'%s','delimiter','\n','commentstyle','shell');

%----- Number of parameters read.
nParams = length(paramLines);

%----- Loop over parameters.
for iParam=1:nParams,

    %----- Split line into name: value strings
    [paramName,paramValStr] = dataread('string',paramLines{iParam}, ...
                                      '%s%s','delimiter',':');

    %----- Parse name: value strings and assign parameter values.
    switch lower(paramName{1})
        case 'onsourcebeginoffset',
         analysis.onSourceBeginOffset = str2num(paramValStr{1});
        case 'onsourceendoffset',
         analysis.onSourceEndOffset = str2num(paramValStr{1});
        otherwise,
                    
    end;

end;
% --- Override defaults on source window if parameters found in file
window.offset = -analysis.onSourceBeginOffset;
window.duration = analysis.onSourceEndOffset - analysis.onSourceBeginOffset;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 Read on- and off-source results files.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Read and parse the file listing all of the results files to be 
%      processed.
fauto = fopen(autoFileName,'r');
offSourceMatFile = fscanf(fauto,'%s',1);
onSourceMatFile = fscanf(fauto,'%s',1);
% change the on source to undecimated on source if using the ulbox
% analysis type
if strcmp(analysisType,'ulbox')
  onSourceMatFile = regexprep(onSourceMatFile,'on_source','ul_source');
end
nWaveforms = fscanf(fauto,'%d',1);
for i=1:nWaveforms
    autoFilesCell{i} = fscanf(fauto,'%s',1);
end
for i=1:nWaveforms
    injectionFilesCell{i} = fscanf(fauto,'%s',1);
end
% ---- Find duration of offSource region 
backgroundPeriod = fscanf(fauto,'%d',1);
catalogDir = fscanf(fauto,'%s',1);
fclose(fauto);

% ---- Version of xgrbwebpage.py prior to r2561 do not write
%      backgroundPeriod to auto file.
%      We should check it has been set and quit otherwise.
if isempty(backgroundPeriod)
    error(['backgroundPeriod missing from auto file, '...
           'update xpipeline, compile and try running again']);
end

fprintf(1,['Loading on source results files from ' onSourceMatFile  ' ...']);
% ---- Load on-source events and analysis information.
load(onSourceMatFile,'amplitudeSpectraCell','analysisTimesCell', ...
    'blockTime','cluster','detectorList', ...
    'gaussianityCell','likelihoodType', ...
    'maximumFrequency','minimumFrequency', ...
    'sampleFrequency','skyPositions','skyPositionsTimes','transientTime');
% ---- Pack analysis parameters and information into convenient structure.
analysis.amplitudeSpectraCell = amplitudeSpectraCell;
analysis.analysisTimes        = cell2mat(analysisTimesCell); 
analysis.blockTime            = blockTime;
analysis.clusterType          = 'connected';
analysis.detectorList         = detectorList;
analysis.gaussianityCell      = gaussianityCell;
analysis.likelihoodType       = likelihoodType;
analysis.maximumFrequency     = maximumFrequency;
analysis.minimumFrequency     = minimumFrequency;
analysis.sampleFrequency      = sampleFrequency;
analysis.skyPositions         = skyPositions;
analysis.skyPositionsTimes    = skyPositionsTimes;
analysis.transientTime        = transientTime;
analysis.type                 = analysisType;
analysis.grb_name             = grb_name;
analysis.offSourceMatFile     = offSourceMatFile;
analysis.onSourceMatFile      = onSourceMatFile;

% ---- Clean up.
clear amplitudeSpectraCell analysisTimesCell blockTime detectorList
clear gaussianityCell likelihoodType maximumFrequency minimumFrequency
clear sampleFrequency skyPositions 
clear transientTime


%%%%%%%%%%%%%% ioannis test - editing on-source distribution %%%%%%%%%%%%%%%%
%% assuming one to one correspondence between sign. and frequency 
%freqarray = [415 425; 487 498; 790 805; 2900 2950];
%for jj=1:size(cluster.peakFrequency,1)
%	for kk=1:size(freqarray,1)
%		if (cluster.peakFrequency(jj) >= freqarray(kk,1) && cluster.peakFrequency(jj) <= freqarray(kk,2))
%			cluster.significance(jj) = 0;
%		end
%	end
%end
%disp(['cluster min after editing =', min(cluster.significance)])


% ---- Set on source and on source window durations
analysis.onSourceTimeLength = analysis.onSourceEndOffset - analysis.onSourceBeginOffset;
analysis.jobsPerWindow=ceil(analysis.onSourceTimeLength/(analysis.blockTime-2* ...
                                                  analysis.transientTime));
analysis.onSourceWindowLength=2*analysis.transientTime+analysis.jobsPerWindow*(analysis.blockTime-2* ...
                                                  analysis.transientTime)


% ---- update significance of events if demanded
detectionIdx=[];
nullIdx=[];
if strcmp(detectionStat,'circovernull')
  detectionIdx=find(strcmp('circenergy',lower(analysis.likelihoodType)));
  nullIdx=find(strcmp('circnullenergy',lower(analysis.likelihoodType)));
  incIdx=find(strcmp('circinc',lower(analysis.likelihoodType)));
  if length(detectionIdx) ~= 1 || length(nullIdx) ~= 1 || ...
         length(incIdx) ~= 1
    error(['Detection statistic ' detectionStat ...
           ' is not in the list of computed statistic, ' ...
           'or it is multiple time in the list']);
  else
    disp(['Changing the significance to the detection statistic: ' ...
          detectionStat])
    cluster.significance = cluster.likelihood(:,detectionIdx).*...
        abs(cluster.likelihood(:,detectionIdx)-cluster.likelihood(:,incIdx))./...
        (cluster.likelihood(:,nullIdx)+abs(cluster.likelihood(:,detectionIdx)-cluster.likelihood(:,incIdx)));
  end
elseif not(strcmp(detectionStat,'default'))
  detectionIdx=find(strcmp(detectionStat,lower(analysis.likelihoodType)));
  if isempty(detectionIdx) || length(detectionIdx) > 1
    error(['Detection statistic ' detectionStat ...
           ' is not in the list of computed statistic, ' ...
           'or it is multiple time in the list']);
  else
    disp(['Changing the significance to the detection statistic: ' detectionStat])
    cluster.significance = cluster.likelihood(:,detectionIdx);
  end
end

% ---- Rename on-source clusters struct, add some additional information.
onSource = cluster;
% ---- Each event will have a pass flag, pass = 1 (0) indicates that the 
%      event has survived (failed) the veto cuts which have been applied. 
onSource.pass         = ones(size(cluster.significance));
% ---- Clean up.
clear cluster 
fprintf(1,'done! \n')

fprintf(1,['Loading off source results files from ' offSourceMatFile  ' ...']);
% ---- Load off-source events for background estimation and tuning.
load(offSourceMatFile,'cluster');
% ---- update significance of events if demanded
if not(isempty(detectionIdx))
  disp(['Changing the significance to the detection statistic: ' detectionStat])
  if isempty(nullIdx)
    cluster.significance = cluster.likelihood(:,detectionIdx);
  else
    cluster.significance = cluster.likelihood(:,detectionIdx).*...
        abs(cluster.likelihood(:,detectionIdx)-cluster.likelihood(:,incIdx))./...
        (cluster.likelihood(:,nullIdx)+abs(cluster.likelihood(:,detectionIdx)-cluster.likelihood(:,incIdx)));
  end
end


%%%%%%%%%%%%% ioannis test - editing off-source distribution %%%%%%%%%%%%%%%%
% assuming one to one correspondence between sign. and frequency 
%freqarray = [415 425; 487 498; 790 805; 2900 2950];
%for jj=1:size(cluster.peakFrequency,1)
%	for kk=1:size(freqarray,1)
%		if (cluster.peakFrequency(jj) >= freqarray(kk,1) && cluster.peakFrequency(jj) <= freqarray(kk,2))
%			cluster.significance(jj) = 0;
%		end
%	end
%end
%disp(['cluster min after editing =', min(cluster.significance)])


% ---- Pack off-source event info into convenient structure.
offSource = cluster;
offSource.pass         = ones(size(cluster.significance));
% ---- Info needed later for likelihood rate histograms.
% ---- !!!!! totalTimeOff do not take into account our
%      asymmetric windows, maybe they should....
nJobFilesProcessed     = length(unique(offSource.jobNumber));
totalTimeOn            = window.duration;
totalTimeOff           = nJobFilesProcessed*totalTimeOn;
% ---- Significance bins for histrograms.  Set here so that the range
%      covers up to the loudest background event before any cuts.
binArray = exp([ 1:0.01:log(max(offSource.significance)) inf]);
% ---- Clean up.
clear cluster 
uniqueOffJobNumber = unique(offSource.jobNumber);
fprintf(1,'done! \n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Magic numbers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Setting magic numbers...');

% ---- Scale factor used for MDCs.
hrssFactor = 1e-21;

% ---- When choosing dummy on-source events we choose off-source job with
%      loudest event which is the Nth percentile loudest event of all
%      the off-source events, where N is defined below 
% convert provided string to doubles
percentile_tuning = str2num(percentile_tuning);
percentile_ulCalc = str2num(percentile_ulCalc);

% ---- If we are performing medianCut vetoes we find it is also useful
%      to apply some fixed linearCuts in order to throw away 
%      significant glitches lying close to the I=E line.
% ---- The value of 1.2 was found by checking the closed box results
%      pages of the H1H2 GRBs analysed during the S5 preliminary run. 
% ---- A study performed by Peter Raffia showed that a value of 0
%      works well for H1H2L1 GRBs. We need to investigate other networks.

% ---- Load fixed ratio cuts from file if in pre pass mode
if not(isempty(regexp(vetoMethod,'median.+Cut.*')) && ...
       isempty(regexp(vetoMethod,'alpha.+Cut.*')))
  if vetoTestOnlyFlag
    % strip the extra job number in the user_tag string that is added for
    % tuning jobs
    tmp = textscan(user_tag,'%s','delimiter','_');
    tmp = tmp{1};
    shortUserTag = tmp{1};
    for iCell=2:(length(tmp)-1)
      shortUserTag = [shortUserTag '_' tmp{iCell}];
    end
    clear tmp
  else
    shortUserTag = user_tag;
  end
  tunedPrePassCuts=load(['xmakegrbwebpage_' grb_name  '_' ...
                      shortUserTag '_xmake_args_tuned_pre.txt.digest'])
% ---- H1H2  
elseif max(strcmp(analysis.detectorList,'H1')) & ...
   max(strcmp(analysis.detectorList,'H2')) & ...
   length(analysis.detectorList)==2
    fixedRatioCut = 1.2;
    disp(['H1H2 network detected, setting fixedRatioCut to: '...
        num2str(fixedRatioCut)]);
% ---- All other networks
else
    fixedRatioCut = 0;
    disp(['Non-H1H2 network detected, setting fixedRatioCut to: '...
        num2str(fixedRatioCut)]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Find time GRB and do some sanity checking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- GRB on source center time is set to the trigger time by xmerge
analysis.gpsCenterTime = unique(onSource.centerTime);

% ---- Check that there is only one on source center time
if (length(analysis.gpsCenterTime) ~= 1)
  analysis.gpsCenterTime
  error('There is not only one on source center time, aborting')
end

% ---- GRB time is the center of the on-source segment.
% analysis.gpsCenterTime = (analysis.startTime+analysis.stopTime)/2;

% ---- All onSource.centerTime values should be the same
%if length(unique(onSource.centerTime)) > 1
%    error('There should only be a single unique onSource.centerTime');
%end

% ---- Check our calculated GRB time is the same as our unique 
%      onSource.centerTime
%if ~(analysis.gpsCenterTime == unique(onSource.centerTime))
%    error('analysis.gpsCenterTime should equal unique(onSource.centerTime)');
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Find svnversions of codes used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Try and get svnversion of processing from on source file
try 
    load(onSourceMatFile,'svnversion_xdetection');
    analysis.svnversion_xdetection = svnversion_xdetection;
    clear svnversion_xdetection
    
catch
    warning('svnversion of xdetection was not stored by xdetection: ');
    warning('setting analysis.svnversion_xdetection to Unknown');
    analysis.svnversion_xdetection = 'Unknown';
end

% ---- Try and get svnversion of postprocessing using 
%      report-svnversion.sh, this bash script is created
%      when running make install-grbwebpage and is copied to the INSTDIR
%      which should be in the users path
[status,result] = ...
    system('sh $XPIPE_INSTALL_BIN/report-svnversion.sh');
if status==0
    analysis.svnversion_grbwebpage = result;
else
    warning('report-svnversion.sh has failed');
    disp(status);
    disp(result);
    warning('setting analysis.svnversion_grbwebpage to Unknown');
    analysis.svnversion_grbwebpage = 'Unknown';
end

disp(['For processing we used svnversion      : ' ...
    analysis.svnversion_xdetection ]);
disp(['For post processing we used svnversion : ' ...
    analysis.svnversion_grbwebpage ]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Find time of offSource triggers after time-shifts have been undone
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(1,['Finding off-source trigger times after time-shifts '...
    'have been removed ... ']); 
for thisIfo = 1:length(analysis.detectorList) 
    % ---- The circular time slides are shifts of data so opposite to time
    %      offsets which are shifts in read data, they also need to be
    %      unwrapped with reference to the start of the time frequency map
    offSource.unslidTime(:,thisIfo) = mod(offSource.peakTime - offSource.circTimeSlides(:,thisIfo) ...
                                          - (offSource.startTime + analysis.transientTime), ...
                                          analysis.blockTime - 2*analysis.transientTime) + ...
        offSource.startTime + analysis.transientTime ...
        + offSource.timeOffsets(:,thisIfo);
    offSource.unslidCenterTime(:,thisIfo) = offSource.centerTime(:) ...
        + offSource.timeOffsets(:,thisIfo);

    % ---- The on source should not be slid but various functions
    %      require the unslidTime and unslidCenterTime fields so
    %      we set them here.
    onSource.unslidTime(:,thisIfo) = onSource.peakTime;
    onSource.unslidCenterTime(:,thisIfo) = onSource.centerTime(:);
end
fprintf(1,'done! \n') 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  Open web page and write header info.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if produceWebpageFlag
    [fout,figures_dirName,figfiles_dirName]=...
         xwritewebpageheader(user_tag,analysis);

    analysis.captions = [];
    analysis.captions = xcaptionsforplots(analysis.captions); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Apply asymmetric window to on and off source data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp(['Keeping only clusters that lay in window' ...
      ' beginning ' num2str(window.offset) 's before center time of block' ...
      ' with duration ' num2str(window.duration) ]);

fprintf(1,'Checking on source times for vetoed clusters ...\n');
onSource.passWindow = xapplywindowcut(window,onSource,true);
onSource.pass = min(onSource.pass,onSource.passWindow);

fprintf(1,'Checking off source times for vetoed clusters ... \n');
offSource.passWindow = xapplywindowcut(window,offSource,true);
offSource.pass = min(offSource.pass,offSource.passWindow);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         Read in veto segment data from files to vetoSegs struct
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Assume segment lists correspond to times KILLED by cat >=2 veto
%      cuts.  vetoSegsLists must be in order H1, H2, L1, G1, V1.  In the 
%      future these will be provided by the .ini file.
vetoSegs = [];

% ---- list of ifos in the same order as the veto seg files are
%      given on the command line, eventually this will be given
%      directly from the ini file
ifos = {'H1','H2','L1','G1','V1'};

% ---- We discard veto segments which lay outside the onSource region
%      of the supernova we are analysing. Reducing the number of veto 
%      segments we use saves us considerable time.
% ---- The following code is adapted from the GRB case, where the off-source 
%      and on-source regions are distinct. This is why the code uses a 
%      variable called "offSourceRegion". This variable is ONLY used for 
%      application of DQ vetoes, around lines  667-748 (r4288).
% ---- Number of seconds before centerTime that onSource region begins,
%      should always be positive.
offSourceRegion.offset = -analysis.onSourceBeginOffset;
% ---- Duration of window in seconds.
offSourceRegion.duration = analysis.onSourceEndOffset - analysis.onSourceBeginOffset;

disp(' ');
disp('Reading in segments from veto segment files');

% ---- Begin table listing veto segments
if produceWebpageFlag 
    fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
    fprintf(fout,'%s\n',['<tr>']);
    fprintf(fout,'%s\n',['<td><b> Detector </b></td>']);
    fprintf(fout,'%s\n',['<td><b> Veto segment list</b></td>']);
    fprintf(fout,'%s\n',['</tr>']);
end

% ---- Loop over ifos we have analysed
for thisIfo = 1:length(analysis.detectorList)

    % ---- Find vetoSegList corresponding to thisIfo
    thisVetoSegList = vetoSegList{strcmp(ifos,analysis.detectorList(thisIfo))};  

    % ---- output to webpage
    if produceWebpageFlag 
        fprintf(fout,'%s\n',['<tr>']);
        fprintf(fout,'%s\n',['<td> ' analysis.detectorList{thisIfo} ...
            ' </td>']);
        fprintf(fout,'%s\n',['<td> ' thisVetoSegList ' </td>']);
        fprintf(fout,'%s\n',['</tr>']);
    
        % ---- If this is the last ifo, end the table
        if thisIfo == length(analysis.detectorList)
            fprintf(fout,'%s\n','</table><br>');
        end
    end

    % ---- Read in veto seg files
    if ~strcmp(thisVetoSegList,'None') 
        % ---- Read in veto seg files

        % ---- Write output to stdout
        fprintf(1,'Reading segments from %s for %s ... ', thisVetoSegList, ...
            analysis.detectorList{thisIfo});
        tempVetoSegs = [];
        fsegin = fopen(thisVetoSegList,'r');
        tempVetoSegs = textscan(fsegin, '%f %f %f %f','CommentStyle','#');
        fclose(fsegin); 
        fprintf(1,'done! \n') 
    
        vetoSegsTemp(thisIfo).gpsStart = tempVetoSegs{2};
        vetoSegsTemp(thisIfo).duration = tempVetoSegs{4};

        disp(['Keeping only veto segments that lay in window' ...
            ' beginning ' num2str(offSourceRegion.offset) ...
            's before GRB time' ...
            ' with duration ' num2str(offSourceRegion.duration) ]);
        disp(['    GRB times : ' num2str(analysis.gpsCenterTime) ]);
        disp(['    offSourceRegion spans from ' ...
             num2str(analysis.gpsCenterTime - offSourceRegion.offset) ...
             ' to ' ...
             num2str(analysis.gpsCenterTime - offSourceRegion.offset ...
             + offSourceRegion.duration)]);
        % ---- Keep only segments which overlap with our offSourceRegion
        coincOut=Coincidence2(analysis.gpsCenterTime - offSourceRegion.offset,...
                              offSourceRegion.duration,...
                              vetoSegsTemp(thisIfo).gpsStart,...
                              vetoSegsTemp(thisIfo).duration);

        if ~isempty(coincOut)
            % ---- Sixth column of coincOut contains indices of tempVetoSegs
            %      overlapping with our offSource region
            fprintf(1,'Keeping %d of %d of veto segments for %s \n', ...
                length(coincOut(:,6)),...
                length(vetoSegsTemp(thisIfo).gpsStart),...
                analysis.detectorList{thisIfo} );

            vetoSegs(thisIfo).gpsStart = ...
                vetoSegsTemp(thisIfo).gpsStart(coincOut(:,6));
            vetoSegs(thisIfo).duration = ...
                vetoSegsTemp(thisIfo).duration(coincOut(:,6));

        % ---- If none of the veto segments overlap with our offSource region
        else
            disp(['No veto segs for ' analysis.detectorList{thisIfo} ...
                 ' overlap with our off source region ']); 
            vetoSegs(thisIfo).gpsStart = [];
            vetoSegs(thisIfo).duration = [];
        end

    % ---- If we did not supply veto segments for this ifo
    else
        fprintf(1,'No veto segs for %s\n', analysis.detectorList{thisIfo});
        vetoSegs(thisIfo).gpsStart = [];
        vetoSegs(thisIfo).duration = [];
    end  

end % -- End of for loop over ifos

% ---- Add link to ini file in the web page
if produceWebpageFlag 
  fprintf(fout,'%s\n','<br>Ini file used:<br>');
  fileList=dir('*.ini');
  % ---- If more than one ini file found, link all of them so that the
  % confusion over which one was used is visible
  for iFile=1:length(fileList)
    fprintf(fout,'<A HREF="%s">%s</A><br>\n',fileList(iFile).name,fileList(iFile).name);
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Check on source segments satisfy our DQ criteria.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % ---- Note that unlike the other pass flags, onJobsPass has one element 
% %      per onSource job, not for each onSource trigger.
% disp(['Checking data quality of on source segments']);
% [onJobsPass, onJobsDeadtime] = ...
%     xoffsourcedataqualitycheck(analysis,onSource,vetoSegs,'quiet');
% if onJobsPass
%     fprintf(1,'On source segment passes data quality checks\n')
% else
%     error('On source segment fails data quality checks!');
% end
warning('KLUDGE: Disabling on-source data-quality check.');
onJobsPass = 1;
% onJobsDeadtime = 0;

% ---- Compute zero-lag deadtime due to vetoes directly from segment lists.
% ---- Consruct segment list of analysed data.
event.center = load('../input/event_on_source.txt');
event.start = event.center - (analysis.blockTime/2-analysis.transientTime);
event.end   = event.center + (analysis.blockTime/2-analysis.transientTime);
event.dur   = event.end - event.start;
% ---- Combine all veto lists into one large list to get total deadtime.
allVetoes.start = vetoSegs(1).gpsStart;
allVetoes.dur = vetoSegs(1).duration;
for ii = 2:length(analysis.detectorList);
    allVetoes.start = [allVetoes.start; vetoSegs(ii).gpsStart];
    allVetoes.dur = [allVetoes.dur; vetoSegs(ii).duration];
end
% ---- Concatenate overlapping segments into unique list of veto times.
tmp = concatenatesegmentlist(allVetoes.start,allVetoes.dur);
if ~isempty(tmp)
    allVetoes.start = tmp(:,1);
    allVetoes.dur   = tmp(:,2);
end
% ---- Compute intersection of veto list with analysed segment list.
tmp = Coincidence2(allVetoes.start,allVetoes.dur,event.start,event.dur);
if ~isempty(tmp)
    onJobsDeadtime = sum(tmp(:,2));
else
    onJobsDeadtime = 0;
end
disp(['Veto deadtime in on source segments: ' num2str(onJobsDeadtime) 's.']);

% ---- Clean up. Note that variable "event" is used circa line 1653, below. 
clear tmp ii allVetoes


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Check off source segments satisfy our DQ criteria.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % ---- Note that unlike the other pass flags, offJobsPass has one element 
% %      per offSource job, not for each offSource trigger.
% disp(['Checking data quality of off source segments']);
% [offJobsPass, offJobsDeadtime] = ...
%     xoffsourcedataqualitycheck(analysis,offSource,vetoSegs,'quiet');
% fprintf(1,'%d of the %d off source segments pass data quality checks \n',...
%     sum(offJobsPass),length(offJobsPass));
warning('KLUDGE: Disabling off-source data-quality check.');
offJobsPass = ones(length(uniqueOffJobNumber),1);
offJobsDeadtime = zeros(length(uniqueOffJobNumber),1);

% ---- Number of jobs passing our DQ criteria
nOffJobsPass = sum(offJobsPass);

% ---- Find jobNumbers of jobs that pass DQ criteria
uniqueOffJobNumberPass = uniqueOffJobNumber(logical(offJobsPass));

% ---- Find jobNumbers of jobs that fail DQ criteria
uniqueOffJobNumberFail = uniqueOffJobNumber(not(logical(offJobsPass)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Apply veto seg lists to on and off source data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Applying veto segments to on source clusters']);
% ---- Note that xapplyvetosegments expects a column of trigger times for
%      each ifo analysed. Therefore we must multiply the column vector 
%      onSource.boundingBox(:,1) by a row vector of ones with length numIfos
onSource.passVetoSegs = ...
    xapplyvetosegments(onSource,onSource.unslidTime,...
    vetoSegs,analysis,'onSource','quiet');
onSource.pass = min(onSource.pass,onSource.passVetoSegs);
onSourceBeforeDQ = onSource;
onSource = xclustersubset(onSource,find(onSource.passVetoSegs));

disp(['Applying veto segments to off source clusters']);
offSource.passVetoSegs = ...
    xapplyvetosegments(offSource,offSource.unslidTime,...
    vetoSegs,analysis,'offSource','quiet');
offSource.pass = min(offSource.pass,offSource.passVetoSegs);
offSourceBeforeDQ = offSource;
offSource = xclustersubset(offSource,find(offSource.passVetoSegs));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Identify likelihoods used for coherent vetoes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Figure our which column of onSource.likelihood contains each
%      likelihoodType
if or(strcmp(plusVetoType,'plusEoverI'),strcmp(plusVetoType,'plusIoverE'))
  [analysis.ePlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('eplus',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
  [analysis.iPlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('iplus',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
elseif strcmp(plusVetoType,'circEoverI')
  [analysis.ePlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('ecirc',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
  [analysis.iPlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('icirc',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
elseif strcmp(plusVetoType,'ampEoverI')
  [analysis.ePlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('eamp',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
  [analysis.iPlusIndex,vetoPlusRangeInput] = ...
      xlikelihoodindex('iamp',analysis.likelihoodType,...
                       onSource.likelihood,vetoPlusRangeInput);
end
if or(strcmp(crossVetoType,'crossEoverI'),strcmp(crossVetoType,'crossIoverE'))
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecross',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('icross',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
elseif strcmp(crossVetoType,'circnullIoverE')
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecircnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('icircnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
elseif strcmp(crossVetoType,'ampnullIoverE')
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('eampnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('iampnull',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
elseif strcmp(crossVetoType,'circnullregIoverE')
  % saving the supplied cross cut value in case the regularized
  % circnull/inc are not found so that the escape below works
  suppliedCrossRangeInput = vetoCrossRangeInput;
  [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecircnullreg',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  [analysis.iCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('icircnullreg',analysis.likelihoodType,...
                       onSource.likelihood,vetoCrossRangeInput);
  % -- if regulated circnull/inc not found try to use normal ones
  if not(and(analysis.eCrossIndex,analysis.iCrossIndex))
    [analysis.eCrossIndex,vetoCrossRangeInput] = ...
      xlikelihoodindex('ecircnull',analysis.likelihoodType,...
                       onSource.likelihood,suppliedCrossRangeInput);
    [analysis.iCrossIndex,vetoCrossRangeInput] = ...
        xlikelihoodindex('icircnull',analysis.likelihoodType,...
                         onSource.likelihood,suppliedCrossRangeInput);
  end
end
[analysis.eNullIndex,vetoNullRangeInput] = ...
   xlikelihoodindex('enull',analysis.likelihoodType,...
   onSource.likelihood,vetoNullRangeInput);
[analysis.iNullIndex,vetoNullRangeInput] = ...
   xlikelihoodindex('inull',analysis.likelihoodType,...
   onSource.likelihood,vetoNullRangeInput);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Experimental test code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- If we set this flag to one we will try to replace the eNullIndex and 
%      iNullIndex values calculated above to point at the H1H2nullenergy and
%      H1H2nullinc (instead of the usual nullenergy and nullinc likelihoods). 
% ---- If the H1H2null likelihoods do not exist then we will simply use the
%      original null likelihoods instead.
useH1H2nullFlag = 0;

disp(['For eNull we will use column ' num2str(analysis.eNullIndex) ...
    ' of the likelihood array']);
disp(['For iNull we will use column ' num2str(analysis.iNullIndex) ...
    ' of the likelihood array']);

if useH1H2nullFlag 
    warning('Using H1H2null likelihood values')
    % ---- Find column indices of H1H2nullenergy and H1H2nullInc.

    [eNullIndexTemp,vetoNullRangeInputTemp] = ...
        xlikelihoodindex('enullH1H2',analysis.likelihoodType,...
        onSource.likelihood,vetoNullRangeInput);
    [iNullIndexTemp,vetoNullRangeInputTemp] = ...
        xlikelihoodindex('inullH1H2',analysis.likelihoodType,...
        onSource.likelihood,vetoNullRangeInputTemp);
 
    % ---- If H1H2null likelihoods were not calculate during trigger 
    %      generation we will revert to using the usual null likelihoods.
    %      This allows the useH1H2nullFlag to be set to 1 when analysing
    %      all GRB networks.
    if (eNullIndexTemp==0) | (iNullIndexTemp==0)
        warning(['H1H2null likelihoods not calculated, we will continue on '...
                 'using the standard null likelihoods instead']);
    else 
        % ---- If H1H2null likelihoods were calculated we will update the
        %      values of iNullIndex etc appropriately. 
        analysis.eNullIndex = eNullIndexTemp;
        analysis.iNullIndex = iNullIndexTemp;
        vetoNullRangeInput  = vetoNullRangeInputTemp;   
    end
end

disp(['For eNull we will use column ' num2str(analysis.eNullIndex) ...
    ' of the likelihood array']);
disp(['For iNull we will use column ' num2str(analysis.iNullIndex) ...
    ' of the likelihood array']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        End of experimental test code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ---- Populate analysis.likelihoodPairs structure with the indices of
%      the E,I pairs we wish to make plots of later
% ---- Only add likelihoods to this structure if both E and I part exist
analysis.likelihoodPairs = [];
pairIdx = 1;

% ---- plus
if and(analysis.ePlusIndex,analysis.iPlusIndex)
  if not(isempty(regexp(plusVetoType,'(circ|amp)EoverI') ))
   analysis.likelihoodPairs(pairIdx).name   = 'plus';
  else
   analysis.likelihoodPairs(pairIdx).name   = 'plus';
  end
   analysis.likelihoodPairs(pairIdx).energy = analysis.ePlusIndex;
   analysis.likelihoodPairs(pairIdx).inc    = analysis.iPlusIndex;
   pairIdx = pairIdx + 1;
end

% ---- cross
if and(analysis.eCrossIndex,analysis.iCrossIndex)
   analysis.likelihoodPairs(pairIdx).name   = 'cross';
   analysis.likelihoodPairs(pairIdx).energy = analysis.eCrossIndex;
   analysis.likelihoodPairs(pairIdx).inc    = analysis.iCrossIndex;
   pairIdx = pairIdx + 1;
end

% ---- null
if and(analysis.eNullIndex,analysis.iNullIndex)
   analysis.likelihoodPairs(pairIdx).name   = 'null';
   analysis.likelihoodPairs(pairIdx).energy = analysis.eNullIndex;
   analysis.likelihoodPairs(pairIdx).inc    = analysis.iNullIndex;
end
pairIdx = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Write time and sky position of GRB.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ---- set rand seed used by randperm to matlab default
%      seed will be different for each GRB but will be the same for
%      each GRB when we run it again and again
rand('seed',931316785+analysis.gpsCenterTime);
initialSeed = rand('seed');
disp(' ');
disp(['initial random seed: ' num2str(initialSeed)]);

% ---- Call helper function which generates plots and adds them to
%      our webpage
if produceWebpageFlag
    [analysis.FpList,analysis.FcList,analysis.dF,S,Snull,wFpDPmean,wFcDPmean]=...
        xwritenetworkinfo(analysis,fout,figures_dirName,figfiles_dirName);
else
    [analysis.FpList,analysis.FcList,analysis.dF,S,Snull,wFpDPmean,wFcDPmean]=...
        xwritenetworkinfo(analysis);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Preparation for veto cuts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Array of ratio thresholds.
numberOfLikelihoods = size(onSource.likelihood,2);
ratioArray = zeros(numberOfLikelihoods,numberOfLikelihoods);
% ---- Default should be not to use deltas.
deltaArray = zeros(numberOfLikelihoods,numberOfLikelihoods);

% ---- Get all distinct combinations of veto thresholds, pack into vectors.
[X,Y,Z] = meshgrid(vetoNullRangeInput,vetoPlusRangeInput,vetoCrossRangeInput);
vetoNullRange  = X(:);
vetoPlusRange  = Y(:);
vetoCrossRange = Z(:);

% ---- We should only ever run open box analysis with a single tuned
%      set of vetoes.
if (strcmp(analysis.type,'openbox') | strcmp(analysis.type,'ulbox'))& length(vetoNullRange)>1
    error('We should only run openbox analysis with a single tuned set of vetoes')
end

% ---- If we are only analysing a single ifo we should not have consistency cuts,
%      all veto RangeInputs should be zero
if length(analysis.detectorList) == 1
    if vetoNullRange | vetoPlusRange | vetoCrossRange
        error(['When running with a single ifo we cannot perform ' ...
               'consistency cuts. vetoNullRangeInput, vetoPlusRangeInput '...
               'and vetoCrossRangeInput should each be zero'])
    end
end

% % ---- If we running with no consistency cuts we can usefully reset 
% %      vetoMethod to None, unless compound veto method are used for which
% %      the fixed cut can be non null
% if vetoNullRange==0 & vetoPlusRange==0 & vetoCrossRange==0 & ...
%       isempty(regexp(vetoMethod,'median.+Cut.*')) & ...
%       isempty(regexp(vetoMethod,'alpha.+Cut.*'))
%     vetoMethod = 'None';
% end

% ---- Set vetosTuned and runValue flags.
if (vetoTestOnlyFlag == 1)
    % ---- If we are doing vetoTestOnly we will not have
    %      tuned vetoes and we will not make second pass of 
    %      main processing loop to make plots
    vetosTuned = 0;
    runValue = 1;
else
    if length(vetoNullRange)==1
        % ---- If we only have one veto combination, we do
        %      not need to do tuning and will pass once
        %      through the main processing loop making plots
        vetosTuned = 1;
        runValue = 1;
        % ---- If we only have one veto combination then it must
        %      be the best one
        bestULIdx = 1;
        vetoNullRangePlots = vetoNullRange;
        vetoPlusRangePlots = vetoPlusRange;
        vetoCrossRangePlots = vetoCrossRange;
    else
        % ---- If we have more than one set of vetoes we will 
        %      make two passes of the main processing loop
        %      On the first loop we will tune vetoes and on the
        %      second loop we will calculate UL and make plots
        error(['The two pass scheme has been depracated. You need to run ' ...
               'xmakegrbwebpage twice. Once for tuning and once for ' ...
                'producing results.'])
        vetosTuned = 0;
        runValue = 2;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Select off source jobs to be used for veto tuning and those
%                   to be used for upper limit calc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dummyOnSourceJobNumber = [];

% ---- Same selection should be made in cases of open or closed boxes.
if strcmp(closedBoxMethod,'median')
    % ---- If we are using the median method to choose which offSource job
    %      to use as our fake onSource we want to use:
    %      * half of the offSource jobs for veto tuning and
    %      * the other half of the offSource jobs for UL calc.  

    % ---- Get random order of indices between 1 and 
    %      nOffJobsPass.
    rand('seed',initialSeed + 11);
    offJobSeed = rand('seed');
    randOffIdx = randperm(nOffJobsPass);
    nOffJobsPassBy2 = ceil(nOffJobsPass/2);

    % ---- Use the first half of randomly ordered injections for tuning.
    offIdx_tuning  = randOffIdx(1:nOffJobsPassBy2);
    nOffJob_tuning = length(offIdx_tuning);   
    offJob_tuning  = uniqueOffJobNumberPass(offIdx_tuning);    
    offJob_tuning  = sort(offJob_tuning);   
 
    % ---- Use the second half of randomly ordered injections for ul calc.
    offIdx_ulCalc  = randOffIdx(nOffJobsPassBy2+1:nOffJobsPass);
    nOffJob_ulCalc = length(offIdx_ulCalc);   
    offJob_ulCalc  = uniqueOffJobNumberPass(offIdx_ulCalc); 
    offJob_ulCalc  = sort(offJob_ulCalc); 

elseif strcmp(closedBoxMethod(1:4),'job_')
    % ---- If we want to use a specific offSource job as our fake onSource
    %      we want to 
    %      * use the median loudest of all of the other jobs for veto tuning 
    %        and
    %      * our chosen offSource job for UL calc.

    % ---- Get chosen offSource jobNumber from closedBoxMethod.
    dummyOnSourceJobNumber = str2num(closedBoxMethod(5:length(closedBoxMethod)));
    if isempty(find(dummyOnSourceJobNumber == uniqueOffJobNumberPass))
        error(['Chosen dummy on source job (number ' ...
            num2str(dummyOnSourceJobNumber) ') fails DQ cuts' ]);
    end
    offIdx_ulCalc  = find(dummyOnSourceJobNumber == uniqueOffJobNumberPass);
    nOffJob_ulCalc = 1; 
    offJob_ulCalc  = dummyOnSourceJobNumber;

    % ---- Get random order of indices between 1 and nJobFilesProcessed-1.
    rand('seed',initialSeed + 11);
    offJobSeed = rand('seed');
    randOffIdx = randperm(nOffJobsPass-1);
    nOffJobsPassBy2 = ceil(nOffJobsPass/2);

    % ---- Remove offSource job we will use for ul calc. 
    removeIdx = find(uniqueOffJobNumberPass == offJob_ulCalc);
    uniqueOffJobNumberPass(removeIdx) = [];

    % ---- Use the first half of randomly ordered injections for tuning.
    offIdx_tuning  = randOffIdx(1:nOffJobsPassBy2);
    nOffJob_tuning = length(offIdx_tuning);   
    offJob_tuning  = uniqueOffJobNumberPass(offIdx_tuning);
    offJob_tuning  = sort(offJob_tuning);   

else
    error('Bad value for analysisType or closedBoxMethod')
end

% ---- We use this flag later to choose which half of our injections will 
%      be used for tuning and which for UL calc
%      We cannot do this until we have read in the injections because until
%      then we don't know how many injections we have.
chooseRandomInjections = 1;

% ---- Start big loop.
for thisRun = 1:runValue

    ulVector90 = [];
    ulVector50 = [];

    if (vetosTuned == 0)
       fprintf(1,'\n%s\n','Tuning our vetoes');
    else
       fprintf(1,'\n%s\n','Calculating upper limits using tuned vetoes');
    end
        
    vetoTableFileName = [analysis.grb_name '_' user_tag '_vetoTable' ...
                        vetoMethod '.html'];  

    if (vetosTuned & produceWebpageFlag)
        fprintf(fout,'%s\n','<h2>VetoTuning</h2>');

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %               summary html table showing optimal vetoes 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
        fprintf(fout,'%s\n',['<tr>']);
        fprintf(fout,'%s\n',['<td><b>'                '</b></td>']);
        fprintf(fout,'%s\n',['<td><b>' nullVetoType   '</b></td>']);
        fprintf(fout,'%s\n',['<td><b>' plusVetoType   '</b></td>']);
        fprintf(fout,'%s\n',['<td><b>' crossVetoType  '</b></td>']);
        fprintf(fout,'%s\n',['<td><b> detection statistic  </b></td>']);
        fprintf(fout,'%s\n',['</tr>']);

        fprintf(fout,'%s\n',['<tr>']);
        fprintf(fout,'%s\n',['<td>  Optimal veto threshold  (' vetoMethod ')</td>']);
        fprintf(fout,'%s\n',['<td>   ' num2str(vetoNullRangePlots(bestULIdx)) ' </td>']);
        fprintf(fout,'%s\n',['<td>   ' num2str(vetoPlusRangePlots(bestULIdx)) ' </td>']);
        fprintf(fout,'%s\n',['<td>   ' num2str(vetoCrossRangePlots(bestULIdx)) ' </td>']);
        fprintf(fout,'%s\n',['<td>   ' detectionStat ' </td>']);
        fprintf(fout,'%s\n',['</tr>']);
        fprintf(fout,'%s\n','</table><br>');
        
        fprintf(fout,'%s\n',['<!--#include file="' vetoTableFileName '" -->']);
        
        % --- print out fixed ratio cuts if they have been tuned
        if not(isempty(regexp(vetoMethod,'median.+Cut.*')) && ...
               isempty(regexp(vetoMethod,'alpha.+Cut.*')))
          
          fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
          fprintf(fout,'%s\n',['<tr>']);
          fprintf(fout,'%s\n',['<td><b>'                '</b></td>']);
          fprintf(fout,'%s\n',['<td><b>' nullVetoType   '</b></td>']);
          fprintf(fout,'%s\n',['<td><b>' plusVetoType   '</b></td>']);
          fprintf(fout,'%s\n',['<td><b>' crossVetoType  '</b></td>']);
          fprintf(fout,'%s\n',['</tr>']);
          
          fprintf(fout,'%s\n',['<tr>']);
          fprintf(fout,'%s\n',['<td>  Optimal fixed ratio veto threshold  </td>']);
          fprintf(fout,'%s\n',['<td>   ' num2str(tunedPrePassCuts(1)) ' </td>']);
          fprintf(fout,'%s\n',['<td>   ' num2str(tunedPrePassCuts(2)) ' </td>']);
          fprintf(fout,'%s\n',['<td>   ' num2str(tunedPrePassCuts(3)) ' </td>']);
          fprintf(fout,'%s\n',['</tr>']);
          fprintf(fout,'%s\n','</table><br>');

          switch vetoMethod
           case {'medianLinCut','alphaLinCut'}
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTable' ...
                                'linearCut.html'];  
           case {'medianLinCutCirc','alphaLinCutCirc'}
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTable' ...
                                'linearCutCirc.html'];  
           case {'medianLinCutAmp','alphaLinCutAmp'}
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTable' ...
                                'linearCutAmp.html'];  
           case 'medianAlphaCut'
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTable' ...
                                'alphaCut.html'];  
           case 'medianAlphaCutCirc'
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTable' ...
                                'alphaCutCirc.html'];  
           case 'medianAlphaCutAmp'
            vetoTableFileNamePre = [analysis.grb_name '_' user_tag '_vetoTable' ...
                                'alphaCutAmp.html'];  
           otherwise
            warning(['Unrecognized pre-pass mode vetoMethod: ' vetoMethod ...
                     '. Falling back to final veto table.' ])
            vetoTableFileNamePre = vetoTableFileName;
          end
          fprintf(fout,'%s\n',['<!--#include file="' vetoTableFileNamePre '" -->']);
        end

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               Select offSource for either tuning or ULCalc 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if (vetosTuned == 0)
       % ---- Find the set of job numbers analysed.
       jobNumbers = offJob_tuning;
    elseif (vetosTuned == 1)
       % ---- Find the set of job numbers analysed.
       jobNumbers = offJob_ulCalc;
    else
       error('Bad value for vetosTuned')   
    end 

    % ---- Pull from the offSource structure every trigger whose
    %      job number is listed in the jobNumbers vector.
    % ---- First we identify which elements of offSource we want
    %      in clusterIndex. 
    % ---- Reuse function of coincidence between segments and events
    coincIndex = fastcoincidence2([jobNumbers 0.1*ones(size(jobNumbers))],...
                                  [offSource.jobNumber 0.1*ones(size(offSource.jobNumber))]);
    clusterIndex = coincIndex(:,2);

    % ---- Use xclustersubset to pull out the jobs whose indices are
    %      listed in clusterIndex and write these to a new structure
    %      offSourceSelected.
    offSourceSelected = [];
    offSourceSelected = xclustersubset(offSource,clusterIndex); 


    % ---- Pull from the offSource structure every trigger whose
    %      job number is listed in the jobNumbers vector.
    % ---- First we identify which elements of offSource we want
    %      in clusterIndex. 
    % ---- Reuse function of coincidence between segments and events
    coincIndex = fastcoincidence2([jobNumbers 0.1*ones(size(jobNumbers))],...
                                  [offSourceBeforeDQ.jobNumber 0.1*ones(size(offSourceBeforeDQ.jobNumber))]);
    clusterIndex = coincIndex(:,2);

    % ---- Use xclustersubset to pull out the jobs whose indices are
    %      listed in clusterIndex and write these to a new structure
    %      offSourceSelected.
    offSourceSelectedBeforeDQ = [];
    offSourceSelectedBeforeDQ = xclustersubset(offSourceBeforeDQ,clusterIndex); 
    clear offSourceBeforeDQ

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                    Calculate median of offSource events
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Here we will measure the median of the offSource triggers in the
    %      I and E likelihood planes. We use the median when applying our 
    %      glitch rejection cuts and only calculate it when we are
    %      applying the medianCut and when we have at least a single pair
    %      of likelihoods.

    % ---- Initialise structs, these will be used for plotting the
    %      fit to the median of the offSource on the E-I plane
    medianE = [];
    medianI = [];  
    medianA = [];
    medianC = [];
    maxE = [];
    
    if strfind(vetoMethod,'median') 
        % ---- We should always calc median using offSource tuning 
        %      (rather than ulCalc) jobs.
        if (vetosTuned == 0)
            % ---- If we are doing a tuning run, we use the off source 
            %      jobs we have just selected
            offSourceForMedian = offSourceSelected;
            jobNumbersForMedian = offJob_tuning;
        elseif (vetosTuned == 1)
            % ---- If we are doing a ulCalc job, we must create the 
            %      offSourceForMedian structure
            jobNumbersForMedian = offJob_tuning;
            clusterIndex = [];
            for jobIdx = 1:length(unique(jobNumbersForMedian))
                clusterIndex = [clusterIndex; ...
                    find(offSource.jobNumber == jobNumbersForMedian(jobIdx))];
            end
            offSourceForMedian = [];
            offSourceForMedian = xclustersubset(offSource,clusterIndex); 
        else
            error('Bad value for vetosTuned')   
        end 

        % delete this eventually
        % this should ensure we do not accidentally use the whole 
        % offSource below
        offSource = [];

        % ---- Estimate median of I values vs. E for this offSource data.
        % ---- Range of E values over which to do fit to data.
        minE = 5;
        if strcmp(resultType,'tunedUpperLimitExtended')
          % -- Read the median parameters from file
          inputMatFileName = [analysis.grb_name '_' user_tag '_' ...
                          analysis.type '.mat'];
          disp(['Reading median fit parameters from ' inputMatFileName])
          load(inputMatFileName,'medianA','medianC','maxE')
        else
          % -- or compute them using offSourceData
        maxE = 100;
        [medianA,medianC] = ...
            xgetrunningmedian(analysis,offSourceForMedian,minE,maxE);
        end

        % ---- Create structures medianE and medianI which contain vectors
        %      of points which track the median distribution of the offSource
        %      for each of the likelihood pairs investigated. These vectors
        %      will be used for plotting. 
        medE = logspace(0,5);
        for pairIdx=1:length(analysis.likelihoodPairs)
            % ---- Status report, get name of likelihood (e.g., 'null') from
            %      name field of analysis.likelihoodPairs
            fprintf(1,['Calculating fit to median distribution of '... 
                analysis.likelihoodPairs(pairIdx).name ' likelihoods... ']);

            % ---- Set temporary variables, e.g., medA = medianA.null
            medA = getfield(medianA,analysis.likelihoodPairs(pairIdx).name);
            medC = getfield(medianC,analysis.likelihoodPairs(pairIdx).name);

            % ---- Calculate medI corresponding to each medE 
            [nDummy,medI] = xdeviation(medE,medE,medA,medC,2*maxE);

            % ---- Set appropriate field of medianE and medianI structures,
            %      equivalent to medianE.null = medE etc.
            medianE = setfield(medianE,analysis.likelihoodPairs(pairIdx).name,medE);
            medianI = setfield(medianI,analysis.likelihoodPairs(pairIdx).name,medI);

            % ---- Status report.
            fprintf(1,'done! \n');
        end
    end % -- if vetoMethod = medianCut

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               Begin loop over different veto cuts
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- length(vetoNullRange) is the total number of veto cuts we will
    %      perform
    for thisVeto = 1:length(vetoNullRange)

        % ---- Some output to screen 
        fprintf(1,'\nApplying veto combination %d of %d at %s\n', ...
            thisVeto,length(vetoNullRange),datestr(now));

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                      Set up ratioArray 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % ---- ratioArray is a square array with the same number of columns 
        %      as the likelihood variable. Each nonzero element is a threshold
        %      to be applied to a pair of likehoods;

        [ratioArray deltaArray]=...
            xsetratioarray(analysis,...
                           plusVetoType,crossVetoType,nullVetoType,...
                           vetoPlusRange(thisVeto),vetoCrossRange(thisVeto),vetoNullRange(thisVeto));

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                      Set up fixedRatioArray.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % ---- If we are performing medianCut vetoes we find it is also useful
        %      to apply some fixed linearCuts in order to throw away 
        %      significant glitches lying close to the I=E line.
        % ---- fixedRatioCut set in "Magic numbers" section above.
        if not(isempty(regexp(vetoMethod,'medianCut')))
            fixedRatioArray = fixedRatioCut * sign(ratioArray); 
            fixedDeltaArray = zeros(size(ratioArray));
            disp(['fixedRatioCut = ' num2str(fixedRatioCut) ]);
        elseif not(isempty(regexp(vetoMethod,'median.+Cut.*')) && ...
                   isempty(regexp(vetoMethod,'alpha.+Cut.*')))
          % Read fixed fixed ratio cut from tuned file
          disp('Using the following fixed/alpha ratio cuts:')
          [fixedRatioArray fixedDeltaArray]=...
              xsetratioarray(analysis,...
                             plusVetoType,crossVetoType,nullVetoType,...
                             tunedPrePassCuts(2),tunedPrePassCuts(3),tunedPrePassCuts(1));
        else
            fixedRatioArray = zeros(size(ratioArray));
            fixedDeltaArray = zeros(size(ratioArray));
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %            Apply veto cuts to on and off-source events.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if not(isempty(regexp(vetoMethod,'None')))
            warning('Veto method is None, skiping veto cuts')
        else
            [onSource.passRatio onSource.passFixedRatio] = ...
                xapplycoherentcuts(onSource,analysis,vetoMethod,ratioArray,deltaArray,...
                                   medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
            onSource.pass = min(onSource.pass,onSource.passRatio);
            onSource.pass = min(onSource.pass,onSource.passFixedRatio);

            [offSourceSelected.passRatio offSourceSelected.passFixedRatio] = ...
                xapplycoherentcuts(offSourceSelected,analysis,vetoMethod,ratioArray,deltaArray,...
                               medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
            offSourceSelected.pass = min(offSourceSelected.pass, ...
                offSourceSelected.passRatio);
            offSourceSelected.pass = min(offSourceSelected.pass, ...
                offSourceSelected.passFixedRatio);
        end

        if sum(offSourceSelected.pass) == 0
            warning('We have vetoed all of the offSourceSelected events.')
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %      Find loudest event for each off-source job.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- loudestBackgroundEvent will store just the significance and
        %      the jobNumber of the loudest events in each
        %      offSourceSelected job.
        loudestBackgroundEvent = [];

        offLoudestEvent.significance          = zeros(length(jobNumbers),1);
        offLoudestEventPreVeto.significance   = zeros(length(jobNumbers),1);
        offLoudestEventWindowCut.significance = zeros(length(jobNumbers),1);
        offLoudestEventVetoSegs.significance  = zeros(length(jobNumbers),1);

        % ---- Flag indicating whether events came from time-shifted or zero
        %      lag (non time-shifted) segments
        offLoudestEvent.zeroLag               = ones(length(jobNumbers),1);

        % ---- Loop over job numbers and for each job find the loudest event
        %      that survives the inc/null cut.
        for iJob = 1:length(jobNumbers)
           % ---- All clusters from this job.
           clusterIndex = find(offSourceSelected.jobNumber == jobNumbers(iJob));
           clusterIndexBeforeDQ = find(offSourceSelectedBeforeDQ.jobNumber == jobNumbers(iJob));

           % ---- If we have timeOffsets then set zeroLag flag to zero
           if sum(abs(offSourceSelected.timeOffsets(clusterIndex(1),:)) + abs(offSourceSelected.circTimeSlides(clusterIndex(1),:))) 
               offLoudestEvent.zeroLag(iJob) = 0;
           end

           % ---- Loudest event and its row number in clusterIndex.
           %      Assess loudness using significance
           % ---- Here, keep only events that pass our vetos
           [sigMax,index] = ... 
             max(offSourceSelected.significance(clusterIndex).* ...
             offSourceSelected.pass(clusterIndex));
           offLoudestEvent.significance(iJob)  = sigMax;

           % ---- Record loudest event significance and this job number.
           loudestBackgroundEvent = [loudestBackgroundEvent; ...
                sigMax, jobNumbers(iJob)];

           % ---- Record loudest event for each off source job BEFORE any 
           %      veto cuts
           [sigMaxPreVeto,indexPreVeto] = ...
              max(offSourceSelectedBeforeDQ.significance(clusterIndexBeforeDQ));
           offLoudestEventPreVeto.significance(iJob) = sigMaxPreVeto; 

           % ---- Record loudest event for each off source job AFTER window 
           %      cut has been applied but BEFORE any veto segments or cuts
           [sigMaxWindowCut,indexWindowCuts] = ...
              max(offSourceSelectedBeforeDQ.significance(clusterIndexBeforeDQ).* ... 
                  offSourceSelectedBeforeDQ.passWindow(clusterIndexBeforeDQ));
           offLoudestEventWindowCut.significance(iJob) = sigMaxWindowCut;

           % ---- Record loudest event for each off source job AFTER veto 
           %      segs have been applied but BEFORE any veto cuts
           [sigMaxVetoSegs,indexVetoSegs] = ...
              max(offSourceSelected.significance(clusterIndex).* ... 
                  offSourceSelected.passWindow(clusterIndex).* ... 
                  offSourceSelected.passVetoSegs(clusterIndex));
           offLoudestEventVetoSegs.significance(iJob) = sigMaxVetoSegs;

        end

        % ---- Write some output to webpage 
        if (vetosTuned & produceWebpageFlag)
            xwriteonoffsourceinfo(fout,analysis,onSource,offJobSeed,dummyOnSourceJobNumber,...
                offJob_tuning,offJob_ulCalc,uniqueOffJobNumberFail);    
            fprintf(fout,'%s\n','<br>On-source events<br>');
        end 

        % ---- Sort list of loudest event from each job in ascending order of significance.
        loudestBackgroundEvent = sortrows(loudestBackgroundEvent);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %      Find "loudest event" for a closed-box analysis.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- For a closed-box analysis, find the "loudest event" that will be 
        %      used to estimate the "upper limit".
        if (strcmp(analysis.type,'closedbox') || strcmp(analysis.type,'snrulclosedbox'))

            % ---- In a closed box analysis we choose one of the off source 
            %      segments to be our dummy on source segment. 
            %      We choose the off source segment based upon how loud its
            %      loudest event (rated by significance)is compared to the 
            %      loudest events in the off source jobs. 
            %      Setting percentile to 95 means we take the off source job
            %      which is louder than 95% of the other   
            if vetosTuned
                percentile = percentile_ulCalc;
            else
                percentile = percentile_tuning;
            end 

            % ---- Select a "dummy" loudest on source event.  
            index = ceil(1 + percentile/100*(size(loudestBackgroundEvent,1)-1));
            dummyOnSourceJobNumber = loudestBackgroundEvent(index,end);
            disp(['Closed-box analysis: we will use ' num2str(percentile) ...
                 'th percentile loudest off-source event as our dummy on-source.']);
            disp(['Events taken from off source job number: '...
                 num2str(dummyOnSourceJobNumber) ' as our dummy on source']);

            % ---- Replace onSource struct with offSourceSelected events with 
            %      jobNumber equal to dummyOnSourceJobNumber.
            dummyOnSourceIdx = find(offSourceSelected.jobNumber == ...
                dummyOnSourceJobNumber);
            onSource = xclustersubset(offSourceSelected,dummyOnSourceIdx);

            % ---- Write some output to webpage. 
            if (vetosTuned & produceWebpageFlag)
                fprintf(fout,'%s\n',['For veto tuning, '...
                num2str(percentile_tuning) 'th percentile loudest '...
                'off-source event taken as dummy on-source. <br>']); 
                fprintf(fout,'%s\n',['For upper limit calculation, '...
                num2str(percentile_ulCalc) 'th percentile loudest '...
                'off-source event taken as dummy on-source, '... 
                'events taken ' ...
                'from background job number ' num2str(dummyOnSourceJobNumber) ...
                ', center time = '  num2str(onSource.centerTime(1)) ...
                ', time offsets = ' num2str(onSource.timeOffsets(1,:)) ...
                ', circ time slides = ' num2str(onSource.circTimeSlides(1,:)) ...
                ' <br>']);
                % --- safety check, we want the variance on the
                % percentile level p: p(1-p)/N to be smaller than some
                % small factor squared: 0.1^2 (the allowed relative error, here
                % 0.1) times the expected value squared: (1-p)^2
                maxPercentile=max(percentile_tuning,percentile_ulCalc);
                minNjobs=min(length(offJob_tuning),length(offJob_ulCalc));
                if(1-maxPercentile/100)*0.1^2 < 1/minNjobs
                  fprintf(fout,'%s\n',...
                         ['<font color="red">***WARNING***</font> not enough background' ...
                          ' jobs, the percentile levels' ...
                          ' have an absolut error in their estimation ~'...
                           num2str(100*sqrt((1-maxPercentile/100)*maxPercentile/100/minNjobs)) '% <br>'] )
                end
            end 
        end  %-- if closedbox

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                Report deadtimes to webpage.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         
        if (vetosTuned & produceWebpageFlag)

            fprintf(fout,'%s\n','<br><table border=1 cellspacing="1" cellpadding="5">');
            fprintf(fout,'%s\n',['<tr>']);
            fprintf(fout,'%s\n',['<td><b> Data type </b></td>']);
            fprintf(fout,'%s\n',['<td><b> Deadtime (s) </b></td>']);
            fprintf(fout,'%s\n',['<td><b> Percentage of ' ...
                num2str(sum(event.dur)) 's analysed coincident data</b></td>']);
            fprintf(fout,'%s\n',['</tr>']);

            % ---- Deadtime in real on-source, and as percentage of time analysed.
            fprintf(fout,'%s\n',['<tr>']);
            fprintf(fout,'%s\n',['<td> On-source </td>']);
            fprintf(fout,'%s\n',['<td> ' num2str(onJobsDeadtime) ' </td>']);
            fprintf(fout,'%s\n',['<td> ' ...
                num2str(onJobsDeadtime*100/sum(event.dur)) ' </td>']);
            fprintf(fout,'%s\n',['</tr>']);

            % % ---- Deadtime in dummy on-source (if we have closedbox).
            % if (strcmp(analysis.type,'closedbox') || ...
            %     strcmp(analysis.type,'snrulclosedbox'))
            %     % ---- Find idx of jobNumber corresponding to dummy onSource.
            %     dummyIdx = find(uniqueOffJobNumber == ...
            %         dummyOnSourceJobNumber);
            % 
            %     fprintf(fout,'%s\n',['<tr>']);
            %     fprintf(fout,'%s\n',['<td> Dummy on-source </td>']);
            %     fprintf(fout,'%s\n',['<td> ' num2str(offJobsDeadtime(dummyIdx)) ' </td>']);
            %     fprintf(fout,'%s\n',['<td> ' ...
            %         num2str(offJobsDeadtime(dummyIdx)*100/window.duration) ' </td>']);
            %     fprintf(fout,'%s\n',['</tr>']);
            % end
            % 
            % % ---- Deadtime in all off-source.
            % fprintf(fout,'%s\n',['<tr>']);
            % fprintf(fout,'%s\n',['<td> Off-source ('...
            %     num2str(nJobFilesProcessed) ' jobs) </td>']);
            % fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime)) ' </td>']);
            % fprintf(fout,'%s\n',['<td> ' ...
            %     num2str(sum(offJobsDeadtime)*100/...
            %     (window.duration * nJobFilesProcessed)) ' </td>']);
            % fprintf(fout,'%s\n',['</tr>']);
            % 
            % % ---- Deadtime in off-source for tuning.
            % fprintf(fout,'%s\n',['<tr>']);
            % fprintf(fout,'%s\n',['<td> Off-source (' ...
            %     num2str(nOffJob_tuning) ' tuning jobs) </td>']);
            % fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_tuning))) ' </td>']);
            % fprintf(fout,'%s\n',['<td> ' ...
            %     num2str(sum(offJobsDeadtime(offIdx_tuning))*100/...
            %     (window.duration * nOffJob_tuning)) ' </td>']);
            % fprintf(fout,'%s\n',['</tr>']);
            % 
            % % ---- Deadtime in off-source for ulCalc.
            % fprintf(fout,'%s\n',['<tr>']);
            % fprintf(fout,'%s\n',['<td> Off-source ('...
            %     num2str(nOffJob_ulCalc) ' UL calc jobs) </td>']);
            % fprintf(fout,'%s\n',['<td> ' num2str(sum(offJobsDeadtime(offIdx_ulCalc))) ' </td>']);
            % fprintf(fout,'%s\n',['<td> ' ... 
            %     num2str(sum(offJobsDeadtime(offIdx_ulCalc))*100/...
            %     (window.duration * nOffJob_ulCalc)) ' </td>']);
            % fprintf(fout,'%s\n',['</tr>']);

            fprintf(fout,'%s\n','</table><br>');
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %      Compute "probability" of each surviving on-source event.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Probability used here is simply the fraction of off-source
        %      jobs producing an event as loud as or louder than the given 
        %      on-source event.  "Loudness" = significance.  Vetoed events
        %      are treated as significance = 0, so probability = 1.
        % ---- Default probability = 1.
        onSource.probability = ones(size(onSource.significance,1),1);  
        onSource.probabilityZeroLag = ones(size(onSource.significance,1),1);
        % ---- Loop over all on-source events.
        for ii = 1:size(onSource.likelihood,1)
            if onSource.pass(ii)
                % ---- Fraction of background jobs giving equal or larger event.
                onSource.probability(ii) = ...
                    sum(offLoudestEvent.significance(:) >= onSource.significance(ii)) /...
                        size(offLoudestEvent.significance,1);

                if sum(offLoudestEvent.zeroLag)
                    % ---- Fraction of zerolag background jobs giving equal or 
                    %      larger event.
                    onSource.probabilityZeroLag(ii) = ...
                        sum((offLoudestEvent.zeroLag(:).* ...
                        offLoudestEvent.significance(:)) >= ...
                        onSource.significance(ii)) /...
                        sum(offLoudestEvent.zeroLag);
                end
            end  
        end % -- Loop over all on-source events.
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     Only make plots and tables if vetos have already been tuned.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if (vetosTuned & produceWebpageFlag)
            % ---- Select triggers that pass DQ flags
            offSourceSelectedAfterDQ = ...
                xclustersubset(offSourceSelected,...
                               find(offSourceSelected.passVetoSegs));
            onSourceAfterDQ= ...
                xclustersubset(onSource,...
                               find(onSource.passVetoSegs));
            
            % ---- Make scatter plot of on-source events.
            xwritelikelihoodscatterplots(fout,figures_dirName,figfiles_dirName,...
                analysis,onSourceAfterDQ,offSourceSelectedAfterDQ,vetoMethod,...
                ratioArray,medianE,medianI,'onsource',fixedRatioArray,fixedDeltaArray);

            % ---- Add table of loudest on-source events.
            xwriteonsourceeventtable(fout,analysis,onSource,user_tag); 

            % ---- Make scatter plot of off-source events.
            xwritelikelihoodscatterplots(fout,figures_dirName,figfiles_dirName,...
                analysis,onSourceAfterDQ,offSourceSelectedAfterDQ,vetoMethod,ratioArray,...
                medianE,medianI,'offsource',fixedRatioArray,fixedDeltaArray);

            % ---- Make histograms of trigger properties
            fprintf(fout,'%s\n','All triggers');
            try
              xwritetriggerhists(fout,analysis,{offSourceSelectedBeforeDQ,onSource},...
                                 {'off source','on source'},...
                                 figures_dirName,figfiles_dirName, ...
                                 'offon',2);
            catch
              fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
            end

            % ---- Make histograms of trigger properties after DQ and
            % window
            fprintf(fout,'%s\n','Triggers after applying DQ and window cut');
            offSourceSelectedAfterDQandWindow = ...
                xclustersubset(offSourceSelected,...
                               find(offSourceSelected.passVetoSegs& ...
                               offSourceSelected.passWindow));
            onSourceAfterDQandWindow = ...
                xclustersubset(onSource,...
                               find(onSource.passVetoSegs& ...
                                    onSource.passWindow));
            try
            xwritetriggerhists(fout,analysis,...
                               {offSourceSelectedAfterDQandWindow,...
                                onSourceAfterDQandWindow},...
                {'off source after DQ/window cut','on source after DQ/window cut'},...
                figures_dirName,figfiles_dirName,'offonAfterDQandWindow',2);
            catch
              fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
            end


            % ---- Make histograms of trigger properties after all cuts
            fprintf(fout,'%s\n',['Triggers after all cuts, including ' ...
                                'coherent cuts']);
            offSourceSelectedAfterAllCuts = ...
                xclustersubset(offSourceSelected,...
                               find(offSourceSelected.pass));
            onSourceAfterAllCuts = ...
                xclustersubset(onSource,...
                               find(onSource.pass));
            try
              if(isempty(onSourceAfterAllCuts.likelihood))
                xwritetriggerhists(fout,analysis,...
                                   {offSourceSelectedAfterAllCuts},...
                                   {'off source after all cuts'},...
                                   figures_dirName,figfiles_dirName,'offonAfterAllCuts',1);
              else
                xwritetriggerhists(fout,analysis,...
                                   {offSourceSelectedAfterAllCuts,...
                                    onSourceAfterAllCuts},...
                                   {'off source after all cuts','on source after all cuts'},...
                                   figures_dirName,figfiles_dirName, ...
                                   'offonAfterAllCuts',2);
              end
            catch
              fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
            end


        end % ---- end of if (vetosTuned) statement

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                 Compute ULs for Simulated GWBs.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % -----------------------------------------------------------------
        %                     Initialize variables.
        % -----------------------------------------------------------------

        % ---- Waveform name.
        name = cell(1,length(autoFilesCell));
        % ---- Waveform parameters (as understood by xmakewaveform).
        parameters = cell(1,length(autoFilesCell));
        % ---- Default h_{rss} amplitude of waveform (i.e., at
        %      injectionScale==1).
        hrss = zeros(1,length(autoFilesCell));
        % ---- Central frequency of waveform.
        cFreq = zeros(1,length(autoFilesCell));
        % ---- Upper limit: injectionScale for 90% detection efficiency.
        UL = zeros(length(autoFilesCell),1);
        % ---- Upper limit: injectionScale for 50% detection efficiency.
        UL50 =  zeros(length(autoFilesCell),1);
        % ---- Diagnostic info: percent of tuning tests that give UL=NaN, 
        %      approximate SNRs in coherent and incoherent-null streams at
        %      UL amplitude.
        percentNans = zeros(length(autoFilesCell),1);
        SNRlist = zeros(length(autoFilesCell),length(analysis.detectorList));
        nullSNRlist = zeros(length(autoFilesCell),length(analysis.detectorList));

        % ---- Loop over waveforms tested.
        for iWave = 1:length(autoFilesCell)
            
            % -------------------------------------------------------------
            %                     Waveform properties.
            % -------------------------------------------------------------

            % ---- Read and parse injection parameters.
            disp(['Loading file ' injectionFilesCell{iWave}]);
            if (mdcInjectionFlag)
               % ---- If performing MDC injections we must read GravEn format 
               %      injection log file.
               [gps_s, gps_ns, phi, theta, psi, simName] = ...
                 getmdcinjectionparams(0, 10^12, char(injectionFilesCell{iWave}));

               % ---- We find the waveform's name from the injection 
               %      auto file which is named according to the (unique) MDC 
               %      type listed in the ini file (see [mdc] section).
               % ---- Do some string magic on the auto filename which has is
               %      named auto_mdc_<name>.txt
               idx1 = strfind(autoFilesCell{iWave},'auto_mdc_');
               idx2 = strfind(autoFilesCell{iWave},'.txt');
               nameCell{iWave} = autoFilesCell{iWave}(idx1+9:idx2-1); %-- e.g. SGC150Q8d9_ON
               % ---- Temporary copy of name for feeding into FlareFit.
               nameCellForFlare{iWave} = nameCell{iWave};

               parametersCell{iWave} = 'mdcInjection';

               % ---- Get central frequency "cFreq" of MDC from its simName. 
               fprintf(1,'Extracting central frequencies of MDC waveforms from first simName \n')
               [cFreq(iWave)]=xparsemdcsimname(simName{1});
               
               % ---- Assume hrss of MDCs is always going to be 1e-21.
               hrss(iWave) = hrssFactor;
               fprintf(1,'%s\n',['Assume hrss of MDCs is ' num2str(hrss(iWave))]);

            else
               % ---- If we have performed internally generated injections.
               [injectionParameters] = ...
                 readinjectionfile(char(injectionFilesCell{iWave}));
               [~, gps_s, gps_ns, phi, theta, psi, name, parameters] = ... 
                 parseinjectionparameters(injectionParameters);

               % ---- Record name and xmakewaveform parameters for the first 
               %      injection.
               nameCell{iWave} = cell2mat(name{1});
               parametersCell{iWave} = cell2mat(parameters{1});
             

               % ---- We find the waveform's name from the injection 
               %      auto file which is named according to the (unique) set
               %      type listed in the ini file.
               % ---- Do some string magic on the auto filename which has is
               %      named auto_<name>.txt
               idx1 = strfind(autoFilesCell{iWave},'auto_');
               idx2 = strfind(autoFilesCell{iWave},'.txt');
               setNameCell{iWave} = autoFilesCell{iWave}(idx1+5:idx2-1); %-- e.g. csg500q9incljitter5

               % ---- The osnsearch catalog waveform names contain "/"s which 
               %      cause the plot writing to fail (they are interpreted as 
               %      sub-directories).  Keep a short form that contains only 
               %      the last part of parametersCell if '/' is present.  This
               %      is also useful for NINJA waveforms.
               tmpStr = textscan(parametersCell{iWave},'%s','Delimiter','/');
               parametersCellClean{iWave} = tmpStr{1}{end};
               % ---- KLUDGE: FlareFit scans the nameCell and selects the 
               %      type of fit based on iwhat it finds.  Recognized 
               %      waveform types include 'SGC' and 'SGL'.  Here we 
               %      append the string 'SGC' to waveforms with name 
               %      'chirplet' and 'SGL' to waveforms with name 'SG'; 
               %      these waveforms are described in xmakewaveform.m.
               if strmatch('chirplet',nameCell{iWave})
                   nameCellForFlare{iWave} = [nameCell{iWave} '_SGC'];
               elseif strmatch('SG',nameCell{iWave})
                   nameCellForFlare{iWave} = [nameCell{iWave} '_SGL'];
               end
            end

            % ---- Status report.
            disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
            disp(['Working on waveform: ' setNameCell{iWave}])

            % ---- On-the-fly injection with mdc_ninja are not well
            %      supported, put dummy frequency and hrss values
            if strcmp(nameCell{iWave},'ninja')
               cFreq(iWave) = 150;
               hrss(iWave) = 1e-21;
            end

            % ---- On-the-fly injections (non-MDC injections) are made using 
            %      X-Pipeline's built-in simulation engine.  Use this engine to 
            %      compute various waveform properties (snr, hrss, etc.) and write
            %      them to the web page.
            if (~mdcInjectionFlag && ~strcmp(nameCell{iWave},'ninja'))
                % ---- If working on an internally generated waveform, 
                %      make the time-series waveform.
                snippetPad = 256;
                snippetDuration = 2*snippetPad + 1;
                [t,hp,hc,hb] = xmakewaveform(nameCell{iWave}, ...
                    parametersCell{iWave}, snippetDuration, ...
                    snippetPad, analysis.sampleFrequency, ...
                    'catalogDirectory',catalogDir);
                % ---- Compute central frequency and hrss (ignoring noise).
                [SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] =  ...
                    xoptimalsnr([hp hc hb],0,analysis.sampleFrequency,[],[],[],...
                    analysis.minimumFrequency,analysis.maximumFrequency); 
                cFreq(iWave) =  Fchar;  %-- central frequency
                hrss(iWave) = h_rss;  %-- hrss
                % ---- Loop over detectors.
                for iDetector =1:length(analysis.detectorList)
                    % ---- Compute SNR for this detector, ignoring antenna response
                    %      factors.  Use both actual noise spectrum and "effective
                    %      null spectrum".  
                    SNR =  xoptimalsnr([hp hc hb],0,analysis.sampleFrequency,S(:,iDetector),0,analysis.dF, ...
                        analysis.minimumFrequency,...
                                       min(analysis.maximumFrequency,0.48*analysis.sampleFrequency));
                    SNRlist(iWave,iDetector) = SNR ;
                    % ---- Only calculate "nullSNR" if we have analysis.eNullIndex.
                    if (analysis.eNullIndex)
                        nullSNR =  xoptimalsnr([hp hc hb],0,analysis.sampleFrequency,Snull(:,iDetector), ...
                            0,analysis.dF,analysis.minimumFrequency,...
                                               min(analysis ...
                                                   .maximumFrequency,0.48*analysis.sampleFrequency));
                        if isnan(nullSNR)
                          % KLUDGE most probably the Snull for this detector is not
                          % well defined (H1H2L1 or H1H2V1 network) and the
                          % third detector does not contribute to the
                          % null energy
                          nullSNR = 0;
                        end

                        nullSNRlist(iWave,iDetector) = nullSNR ;
                    end
                end % -- loop over ifo
            end % -- if ~mdcInjectionFlag

            % -------------------------------------------------------------
            %           Injection results for each injection scale.
            % -------------------------------------------------------------

            % ---- Read "auto" file for this specific waveform type.
            input = fopen(autoFilesCell{iWave});
            offSourceFile = fscanf(input,'%s',1); 
            nInjectionScales =  fscanf(input,'%d',1);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                Load injection triggers.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- Load injections only if this is the first tried veto
            %      threshold set, otherwise the clusters in memory can be reused
            if thisVeto == 1
            % ---- Loop over injection scales.
            for iInjScale = 1:nInjectionScales

                % ---- Name of matlab file containing injection triggers for this
                %      scale.
                simulated{iInjScale} = fscanf(input,'%s',1);
                disp(['Loading injection file ' simulated{iInjScale}])
                % ---- Load injection triggers from this file.
                temp = load(simulated{iInjScale},'clusterInj', ...
                    'injectionProcessedMask','injectionScale','peakTime','onSourceTimeOffset');
                % ---- Copy clusterInj array for this injection scale into
                %      higher-dimensional struct array: number of
                %      waveforms x number of injections x number of scales.
                nInjections(iWave) = length(temp.clusterInj);
                for iN = 1:nInjections(iWave)
                  clusterInj(iWave,iN,iInjScale) = temp.clusterInj(iN);                 
                    % ---- update significance of events if demanded
                    if not(isempty(detectionIdx)) && not(isempty(clusterInj(iWave,iN,iInjScale).likelihood))
                      if isempty(nullIdx)
                        clusterInj(iWave,iN,iInjScale).significance = ...
                            clusterInj(iWave,iN,iInjScale).likelihood(:,detectionIdx);
                      else
                        clusterInj(iWave,iN,iInjScale).significance = ...
                            clusterInj(iWave,iN,iInjScale).likelihood(:,detectionIdx).*...
                            abs(clusterInj(iWave,iN,iInjScale).likelihood(:,detectionIdx) - ...
                                clusterInj(iWave,iN,iInjScale).likelihood(:,incIdx))./...
                            (abs(clusterInj(iWave,iN,iInjScale).likelihood(:,detectionIdx) - ...
                                clusterInj(iWave,iN,iInjScale).likelihood(:,incIdx))+ ...
                             clusterInj(iWave,iN,iInjScale).likelihood(:,nullIdx));
                      end
                    end
                end
                injectionProcessedMask{iWave}(:,iInjScale) = temp.injectionProcessedMask;
                injectionScale(iWave,iInjScale) = temp.injectionScale;
                onSourceTimeOffset(iWave) = temp.onSourceTimeOffset;
                injPeakTime{iWave} = temp.peakTime;

                clear temp
            end %-- end loop over injection scales.
            end %-- end if for first set of threshold only

            % ---- Close "auto" file for this specific waveform type.
            fclose(input);

            % ---- In here we choose which injections to be used for tuning
            %      and which to be used for ul calculation. 
            % ---- In the open box case we should pick the same injections that were
            %      used in the closed box ul calc.
            
            % ---- Get random order of indices between 1 and nInjections(iWave). 
            rand('seed',initialSeed + 22);
            injSeed = rand('seed');
            randInjIdx = randperm(nInjections(iWave));
            nInjectionsBy2(iWave) = ceil(nInjections(iWave)/2);
            
            % ---- Use the first half of randomly ordered injections for tuning.
            injIdx_tuning{iWave} = randInjIdx(1:nInjectionsBy2(iWave));
            injIdx_tuning{iWave} = sort(injIdx_tuning{iWave});
            % ---- Use the second half of randomly ordered injections for ul calc.
            injIdx_ulCalc{iWave} = randInjIdx(nInjectionsBy2(iWave)+1:nInjections(iWave));
            injIdx_ulCalc{iWave} = sort(injIdx_ulCalc{iWave});

            % ---- Create a mask to pass injections for tuning.
            injMask_tuning{iWave} = zeros(nInjections(iWave),1);
            injMask_tuning{iWave}(injIdx_tuning{iWave}) = 1;
            % ---- Create a mask to pass injections for ul calc.
            injMask_ulCalc{iWave} = zeros(nInjections(iWave),1);
            injMask_ulCalc{iWave}(injIdx_ulCalc{iWave}) = 1;
            
        end % Loop over waveforms tested
        % cut needed otherwise clusters with and without pass(*) fields
        % initialized don't combine
        % ---- Loop over waveforms tested.
        for iWave = 1:length(autoFilesCell)
            

            % ---- Initilize storage for veto pass flags.
            %      Do as a loop over injection scales and injection number.
            %      Do SEPARATELY from 
            %      "load" loop because assignment of storage to first
            %      element of array will initialize these fields for all
            %      other elements.
            for iInjScale = 1:nInjectionScales
                for iN = 1:nInjections(iWave)
                    nTriggers = length(clusterInj(iWave,iN,iInjScale).significance);
                    if(nTriggers)
                      clusterInj(iWave,iN,iInjScale).pass         = ones(nTriggers,1);
                      clusterInj(iWave,iN,iInjScale).passVetoSegs = ones(nTriggers,1);
                      clusterInj(iWave,iN,iInjScale).passInjCoinc = ones(nTriggers,1);
                    else
                      clusterInj(iWave,iN,iInjScale).pass         = [];
                      clusterInj(iWave,iN,iInjScale).passVetoSegs = [];
                      clusterInj(iWave,iN,iInjScale).passInjCoinc = [];
                    end
                end
            end
                
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       Use appropriate injections for tuning, ul calc .
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- If we are currently doing the tuning.
            if (vetosTuned == 0)
                fprintf(1,'Selecting injections to use for tuning... \n')
                % ---- Loop over all injections, injectionScales.
                nInjectionScales
                nInjections
                for iInjScale = 1:nInjectionScales
                    for iN = 1:nInjections(iWave)
                        % ---- Use injMask_tuning{iWave}.
                        injectionProcessedMask{iWave}(iN,iInjScale) = ...
                            min(injectionProcessedMask{iWave}(iN,iInjScale),injMask_tuning{iWave}(iN));
                    end
                end 
            % ---- If we are currently doing the ul calc.
            elseif (vetosTuned == 1)
                fprintf(1,'Selecting injections to use for UL calc... \n')
                % ---- Loop over all injections, injectionScales.
                for iInjScale = 1:nInjectionScales
                    for iN = 1:nInjections(iWave)
                         % ---- Use injMask_ulCalc{iWave}.
                         injectionProcessedMask{iWave}(iN,iInjScale) = ...
                             min(injectionProcessedMask{iWave}(iN,iInjScale),injMask_ulCalc{iWave}(iN));
                    end
                end 
            end %-- if vetosTuned statement.

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %            Apply veto tests to injection triggers.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            fprintf(1,'Applying vetos to %s waveform injections \n', setNameCell{iWave});

            % ---- Precompute coherent consistency test results for speed (vectorization)
            clusterPrecompute.significance = vertcat(clusterInj(iWave,:,:).significance);
            clusterPrecompute.likelihood = vertcat(clusterInj(iWave,:,:).likelihood);
            clusterPrecompute.peakTime = vertcat(clusterInj(iWave,:,:).peakTime);
            clusterPrecompute.boundingBox = vertcat(clusterInj(iWave,:,:).boundingBox);
            [passRatioPrecompute passFixedRatioPrecompute ] = ...
                xapplycoherentcuts(clusterPrecompute,analysis,vetoMethod,ratioArray,deltaArray,...
                                   medianA,medianC,maxE,fixedRatioArray,fixedDeltaArray);
            % ---- Note that xapplyvetosegments expects a column of 
            %      trigger times for each ifo analysed. Therefore we 
            %      must multiply the column vector peakTime 
            %      by a row vector of ones with length numIfos   
            passVetoSegsPrecompute = ...
                xapplyvetosegments(clusterPrecompute,...
                                   clusterPrecompute.peakTime*...
                                   ones(1,length(analysis.detectorList)),...
                                   vetoSegs,analysis,...
                                   ['inj_' num2str(iWave) ],'quiet');

           % ---- iterator on precomputed outputs
            iCluster=0;

            % ---- Loop over injection scales and injection number.
            for iInjScale = 1:nInjectionScales
                for iN = 1:nInjections(iWave)
                    % ---- Find range of precomputed values
                    precomputeRange = iCluster+1:iCluster+length(clusterInj(iWave,iN,iInjScale).pass);
                    iCluster = iCluster + length(clusterInj(iWave,iN,iInjScale).pass);
                    % -- work around 0x1 and 0x0 empty matrices not being equal
                    if length(precomputeRange) == 0
                      precomputeRange = [];
                    end
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %        Apply asymmetric window to injection triggers.
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    % ---- The MDCs currently generated for the GRB search[1]
                    %      will only lie within an asymmetric window
                    %      defined in our terminology as
                    %      window.offset = 120
                    %      window.duration = 180   
                    % ---- If our window is not smaller than this interval,
                    %      then the window cut is redundant and we will save 
                    %      considerable time by not applying it
                    % [1]http://ldas-jobs.ligo.caltech.edu/~eharstad/ExtTrigMDC.html

                    if mdcInjectionFlag & ...
                        (window.offset < 120 | window.duration < 180)  
                        %disp(['Keeping only clusters that lay in window' ...
                        %    ' beginning ' num2str(window.offset) 's before GRB time' ...
                        %    ' with duration ' num2str(window.duration) ...
                        %    ' for injection with injScale number ' ...
                        %    num2str(iInjScale) ...
                        %'(' num2str(iN) '/' num2str(nInjections(iWave)) ')' ]);
                  
                        clusterInj(iWave,iN,iInjScale).passWindow = ...
                            xapplywindowcut(window,clusterInj(iWave,iN,iInjScale),true);
                        clusterInj(iWave,iN,iInjScale).pass = ...
                            min(clusterInj(iWave,iN,iInjScale).passWindow,...
                            clusterInj(iWave,iN,iInjScale).pass);
                    end                

                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %        Apply veto segments to injection triggers.
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    %disp(['Applying veto segments to ' ...
                    %    ' injections with injScale number ' ...
                    %    num2str(iInjScale) ...
                    %    '(' num2str(iN) '/' num2str(nInjections(iWave)) ')' ]);
                                      
                    clusterInj(iWave,iN,iInjScale).passVetoSegs = ...
                        passVetoSegsPrecompute(precomputeRange);
                    
                    clusterInj(iWave,iN,iInjScale).pass = ...
                        min(clusterInj(iWave,iN,iInjScale).passVetoSegs,...
                        clusterInj(iWave,iN,iInjScale).pass);

                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %   Apply coherent glitch test to injection triggers.
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    clusterInj(iWave,iN,iInjScale).passRatio = ...
                        passRatioPrecompute(precomputeRange);
                    clusterInj(iWave,iN,iInjScale).passFixedRatio = ...
                        passFixedRatioPrecompute(precomputeRange);
                    
                    clusterInj(iWave,iN,iInjScale).pass = min( ...                        
                        clusterInj(iWave,iN,iInjScale).pass, ...
                        clusterInj(iWave,iN,iInjScale).passRatio);
                    clusterInj(iWave,iN,iInjScale).pass = min( ...
                        clusterInj(iWave,iN,iInjScale).pass, ...
                        clusterInj(iWave,iN,iInjScale).passFixedRatio);

                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %  Keep only triggers which lie within 100ms of injection.
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    % skip if there are no triggers
                    if(not(isempty(clusterInj(iWave,iN,iInjScale).boundingBox)))
                    % ---- Event must peak within interval 
                    %      [-injCoincTimeWindow(1), +injCoincTimeWindow(2)] of 
                    %      the injection.
                    if strcmp(lower(nameCell{iWave}),'inspiral') || ...
                          strcmp(lower(nameCell{iWave}),'inspiralsmooth') || ...
                          strcmp(lower(nameCell{iWave}),'ninja') || ...
                          strcmp(lower(nameCell{iWave}),'lalinspiral') 
                        % ---- Larger coincidence window for inspirals.
                        injCoincTimeWindow = [5.0 0.1]; 
                    else
                        % ---- Default coincidence window.
                        %%injCoincTimeWindow = [0.1 0.1];
                        %injCoincTimeWindow = [0.9 0.9];  
                        % ---- KLUDGE: reset coincidence window to keep all triggers - need a wide window for O(100 sec) long injections!.
                        injCoincTimeWindow = [analysis.blockTime analysis.blockTime];  
                    end

                    % ---- Bounding box of injection in the time-frequency plane.
                    injectionBox = [(injPeakTime{iWave}(iN)-injCoincTimeWindow(1)-onSourceTimeOffset(iWave)), ...
                        analysis.minimumFrequency, sum(injCoincTimeWindow), ...
                        analysis.maximumFrequency-analysis.minimumFrequency];   

                    % ---- Find all clusters whose bounding box does NOT overlap that 
                    %      of the injection and set passInjCoinc to zero for them
                    mask = find(not(rectintersect(injectionBox,clusterInj(iWave,iN,iInjScale).boundingBox)));
                    clusterInj(iWave,iN,iInjScale).passInjCoinc(mask) = 0;
                    clusterInj(iWave,iN,iInjScale).pass = min( ...
                       clusterInj(iWave,iN,iInjScale).pass, ...
                       clusterInj(iWave,iN,iInjScale).passInjCoinc);
                    end %-- trigger list not empty

                end  %-- loop over injection number
            end  %-- loop over injection scales

        end % Loop over waveforms tested
        % cutting the loop into two parts is needed by the 'ulbox'
        % sanitized loudest event threshold
        % ---- Loop over waveforms tested.
        for iWave = 1:length(autoFilesCell)

            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                   Compute upper limit.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- Compute upper limit.
            if (exist('pathToInjections','var') && ~isempty(pathToInjections) && ...
                strcmp(analysis.type,'snrulclosedbox'))

                % ---- "snrulclosed" box analysis.  Calculate UL in terms of SNR.
                %      Experimental!  NOT TO BE USED FOR PUBLISHED ULs!
                warning('snrulclosed analysis is experimental!  Caveat emptor!')
                [ul90p,allFractionOnSource,effFactor,ul50p,injectionValueArray] = ...
                snrul(boxChoice.likelihood,boxChoice.jobNumber,...
                   boxChoice.jobFilesProcessed,injection.likelihoodCell,...
                   injection.maskCell,injection.scaleArray,bestRatio,...
                   analysis.iNullIndex,analysis.eNullIndex,energyIndex,pathToInjections,...
                   analysis.amplitudeSpectraCell,analysis.analysisTimes,...
                   analysis.sampleFrequency,analysis.detectorList);
                injection.scaleArray = injectionValueArray;

            else

                % ---- Compute upper limit on injection scale for 90% and
                %      50% detection efficiency. 
                threshold = max(onSource.significance .* onSource.pass);
                % ---- Up the threshold by the loudest event coming from
                %      the weakest injection to catch any spurious loud
                %      events coming from fast processing of injections
                if strcmp(analysis.type,'ulbox')
                  [m iMinInjScale] = min(injectionScale(1,:));
                  threshold = max(max(max(vertcat(clusterInj(:,:,iMinInjScale).significance).*...
                                          vertcat(clusterInj(:,:,iMinInjScale).pass))),...
                                  threshold);
                  threshold=threshold*1.01;
                  disp(['The sanity checked loudest event threshold is ' num2str(threshold)]);
                end
                [UL90p(iWave),UL50p(iWave),efficiency(iWave,:),...
                flareInjScale,flareDetection,UL95p(iWave),analysisEff(iWave,:)] = ... 
                   loudesteventul(threshold, squeeze(clusterInj(iWave,:,:)),...
                   squeeze(injectionProcessedMask{iWave}(:,:)),injectionScale(iWave,:));

                % ---- KLUDGE
                %      Replace 90% UL with 95% UL when tuning linearCut, this
                %      should limit the cases where the cut goes below
                %      90% efficiency for very loud signals
                if vetoTestOnlyFlag
                    [percentageLvl UL90p(iWave)] = xoverrideullevel( ...
                        vetoMethod,UL50p(iWave),UL90p(iWave),UL95p(iWave));
                end
                % ---- END KLUDGE

                fprintf(1,['For %s we have 50%% UL at injScale = %8.2e, '...
                           '90%% UL at injScale = %8.2e \n'],...
                   setNameCell{iWave}, UL50p(iWave), UL90p(iWave) ) 

                [injectionScaleSorted(iWave,:) sortIdx] = sort(injectionScale(iWave,:));
                efficiencySorted(iWave,:)               = efficiency(iWave,sortIdx);
                disp('injectionScale  efficiency');
                disp('--------------  ----------');
                [injectionScaleSorted(iWave,:)', efficiencySorted(iWave,:)']
           
                flareHrss = flareInjScale * hrss(iWave);
               
                doFlareFit = 0;
                %doFlareFit = 1;
                % ---- Only do FlareFit if our efficiency array contains at 
                %      least 2 different values.
                if ~(length(unique(efficiency(iWave,:))) > 1) 
                   doFlareFit = 0;
                   warning(['Not using FlareFit since all efficiency '...
                            'values are the same.'])
                end

                % ---- Initialise vectors which will contain FlareFit
                %      Later on we will check if FlareFit was performed by 
                %      seeing if these vectors have been filled 
                xFF = [];
                yFF = [];
                fit50 = [];
                fit90 = [];
               
                if doFlareFit
                   % ---- Try and run FlareFit.
                   try  
                      fitoptions.forceIsDiscrete = 1;
                      % ---- The useHighModel flag means we use the FlareFit
                      %      function sigmoidPlusHighLevelModel for SGC waveforms
                      %      which can fit to efficiency curves even when 
                      %      efficiency doesn't go to 1 at high amplitudes due to
                      %      veto deadtime. Fits to SGL waveforms use an erfc
                      %      model which handles this case naturally.
                      fitoptions.useHighModel = true;
                      % ---- KLUDGE: For the L1V1 GRBs we see that the SGC efficiency 
                      %      curves rise slowly, like the SGLs. This often causes the 
                      %      fit to go bad.  For this network force FlareFit to use 
                      %      the SGL fit function for the SGCs.  
                      if (max(strcmp(analysis.detectorList,'L1')) & ...
                          max(strcmp(analysis.detectorList,'V1')) & ...
                          length(analysis.detectorList)==2 & ...
                          ~isempty(findstr(nameCellForFlare{iWave}(1:3),'SGC'))) 
                          disp(['SGC waveform and L1V1 network detected, forcing SGL routine in FlareFit.']);
                          [fit90,fit50,ci,minGF,xFF,yFF]= FlareFit('SGL',...
                              flareHrss,flareDetection,fitoptions,'DummyName');
                      % ---- KLUDGE: For GRB070917 (H1H2V1 network) we found that
                      %      that the standard SGC fit did not work well for the 
                      %      SGC1000 waveforms. We will try using the SGL fit for 
                      %      this waveform instead.
                      elseif (max(strcmp(analysis.detectorList,'H1')) & ...
                              max(strcmp(analysis.detectorList,'H2')) & ...
                              max(strcmp(analysis.detectorList,'V1')) & ...
                              strcmp(grb_name,'GRB070917') & ...
                              strcmp(nameCell{iWave}(1:7),'SGC1000'))
                          disp(['GRB070917 SGC1000 detected, forcing SGL routine in FlareFit.']);
                          [fit90,fit50,ci,minGF,xFF,yFF]= FlareFit('SGL',...
                              flareHrss,flareDetection,fitoptions,'DummyName');
                      else
                          % ---- All other networks
                          [fit90,fit50,ci,minGF,xFF,yFF]= FlareFit(nameCellForFlare{iWave},...
                              flareHrss,flareDetection,fitoptions,'DummyName');
                      end
                      fprintf(1,['From FlareFit: '...
                          'For %s we have 50%% UL at injScale = %8.2e, '...
                          '90%% UL at injScale = %8.2e \n'],...
                          setNameCell{iWave}, fit50, fit90 )
                      % ---- If we have hrss50 and 90 values from FlareFit,
                      %      we will use them for veto tuning. 
                      % ---- We divide by hrss to find the injection scale 
                      %      which is what UL90p etc contain.
                      UL50p(iWave) = fit50 / hrss(iWave);
                      UL90p(iWave) = fit90 / hrss(iWave);
                      fprintf(1,'We will use 50%% and 90%% UL values from FlareFit \n'); 
                   % ---- if FlareFit fails:  
                   catch                   
                      disp('WARNING: FlareFit has failed, we will use linearFit efficiency curve')
                      % ---- Set doFlareFit to 0 to ensure we dont try and plot
                      %      erroneous FlareFit efficiency curve. 
                      doFlareFit = 0;
                   end
                end % -- Call to FlareFit.
            end % -- Choice of upper limit calc.

            % ---- Keep track of how many NaNs were encountered in loudesteventul
            %      call.
            nNans = sum(isnan(UL90p(iWave)),1);
            percentNans(iWave,1) = nNans / length(jobNumbers)*100;


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                 Web page plots for this waveform.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- Only make plots, tables if vetos have already been tuned.
            if (vetosTuned & produceWebpageFlag)

                % ---- Write header for this injection
                fprintf(fout,'%s<br>\n',['<h2 id="'  setNameCell{iWave} ...
                                    '" > Injected Waveform : ' setNameCell{iWave}  ...
                                    ' from injection file: ' injectionFilesCell{iWave} '</h2>']);
                  
                % ---- Write some output to webpage.
                xwriteinjectioninfo(fout,injSeed,iWave,nInjections(iWave),nInjectionsBy2(iWave),...
                    injIdx_tuning{iWave},injIdx_ulCalc{iWave}) 

                plotNameEff = ['/eff_' setNameCell{iWave}];
                xwriteinjectioneffplots(fout,injectionScale(iWave,:),efficiency(iWave,:),analysisEff(iWave,:),...
                    nameCell{iWave},parametersCell{iWave},UL90p(iWave),UL50p(iWave),plotNameEff,...
                    figures_dirName,figfiles_dirName)

                % ---- Overlay scatter plot of injection clusters at the scale 
                %      closest to the 90% UL, or loudest injection scale if UL
                %      does not exist.
                % ---- Find the desired injection scale.
                if(~isnan(UL90p(iWave)))
                    [M iS1] = nanmin(abs(UL90p(iWave)-injectionScale(iWave,:)));
                else
                    [M iS1] = max(injectionScale(iWave,:));
                end

                % ---- Report number of injections used for UL calc at this
                %      injectionScale
                nInjectionsUL(iWave) = sum(injectionProcessedMask{iWave}(:,iS1));
                fprintf(1,'%s\n',[num2str(nInjectionsUL(iWave)) ...
                    ' injections used for UL calc'])
      
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %      Find loudest event associated with each injection
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                loudestInjEvents = [];
                loudestInjEventsPassCuts = [];

                firstLoop = 1;
                firstPass = 1;
                % ---- Loop over all injections of this waveform at this 
                %      injection scale.
                for iN = 1:nInjections(iWave)
                    if (injectionProcessedMask{iWave}(iN,iS1)==1)
                        % ---- Find index of loudest trigger associated with 
                        %      this injection.
                        [injSigMax,injSigMaxIdx] = max(clusterInj(iWave,iN,iS1).significance);

                        % ---- Pull out loudest trigger.
                        thisLoudestInjEvent = xclustersubset(clusterInj(iWave,iN,iS1),injSigMaxIdx);

                        % ---- Create a list of the loudest triggers.
                        if firstLoop == 1
                            loudestInjEvents = thisLoudestInjEvent;
                            firstLoop = 0;
                        else
                            loudestInjEvents = ...
                                xclustermerge(loudestInjEvents,thisLoudestInjEvent);
                        end

                        % ---- Create list of the loudest triggers which pass
                        %      our veto cuts.
                        if thisLoudestInjEvent.pass == 1
                            if firstPass == 1
                                loudestInjEventsPassCuts = thisLoudestInjEvent;
                                firstPass = 0;
                            else
                                loudestInjEventsPassCuts = ...
                                    xclustermerge(loudestInjEventsPassCuts,thisLoudestInjEvent);
                            end
                        end
                    end % -- if injectionProcessedMask.
                end % -- Loop over all inj and given inj scale.
   
                if ~(length(loudestInjEvents.significance) == nInjectionsUL(iWave))
                  length(loudestInjEvents.significance)
                  nInjectionsUL(iWave)
                    disp(['Something wrong with loudestInjEvents, some ' ...
                          'injections do not have any loudest event'])
                end

                if ~isfield(loudestInjEventsPassCuts,'significance')
                    warning('The loudest event for each injection was vetoed');
                    numLoudestInjEventsPassCuts = 0;
                    triggersForHist = {offSourceSelectedAfterDQ,onSourceAfterDQ,loudestInjEvents};
                    triggerNamesForHist = {'off source','on source','loudest inj'};
                else 
                    numLoudestInjEventsPassCuts = ...
                        length(loudestInjEventsPassCuts.significance); 
                    triggersForHist = {offSourceSelectedAfterDQ,onSourceAfterDQ,...
                        loudestInjEvents,loudestInjEventsPassCuts};
                    triggerNamesForHist = {'off source','on source',...
                        'loudest inj','loudest inj, passed cuts'};
                end  

                disp(['For ' ...
                      num2str(numLoudestInjEventsPassCuts) ...
                      ' of our ' ...
                      num2str(size(loudestInjEvents.likelihood,1)) ...
                      ' injections, the event with largest significance'... 
                      ' survives our veto cuts' ]);
    
                % ---- Write injection scatterplots.  Need to feed "clean" 
                %      version of parametersCell to avoid mangled file names. 
                [plotNameOn,plotNameOff]=...
                    xwriteinjectionscatterplots(fout,analysis,loudestInjEvents,...
                    injectionScale(iWave,iS1).*hrss(iWave),figures_dirName,figfiles_dirName,...
                    setNameCell{iWave},parametersCellClean{iWave});

                % ---- Create cell array of captions for these plots.
                numCaptions = length(plotNameOn)+length(plotNameOff);
                injCaptions{1} = analysis.captions.efficiency; 
                for iCap = 2:numCaptions+1
                    injCaptions{iCap} = analysis.captions.injection_scatter;
                end

                % ---- Add the figure to the web page.
                xaddfiguretable(fout,[plotNameEff plotNameOn plotNameOff],figures_dirName,...
                    injCaptions);
     
                % ---- Add histograms of the trigger properties           
                try
                xwritetriggerhists(fout,analysis,...
                triggersForHist,...
                triggerNamesForHist,...
                figures_dirName,figfiles_dirName, [setNameCell{iWave}],2)
                catch
                  fprintf(fout,'%s\n</div>\n','***PLOT FAILED***');
                end

                % ---- save best matching trigger for each injection
                for iInjScale = 1:nInjectionScales
                  for iN = 1:nInjections(iWave)
                    % ---- Find index of loudest trigger associated with 
                    %      this injection that passes all cuts
                    [injSigMax,injSigMaxIdx] = max(clusterInj(iWave,iN,iInjScale).significance.*clusterInj(iWave,iN,iInjScale).pass);
                    % ---- If no passing trigger found, associate the loudest one
                    if not(clusterInj(iWave,iN,iInjScale).pass(injSigMaxIdx))
                      [injSigMax,injSigMaxIdx] = max(clusterInj(iWave,iN,iInjScale).significance);
                    end
                    % ---- pull out this trigger
                    injAssociatedTrigger(iN,iInjScale) = ...
                        xclustersubset(clusterInj(iWave,iN,iInjScale),injSigMaxIdx);
                    
                  end % -- loop over injection number
                end % -- loop over injection scale
                save([analysis.grb_name '_' user_tag '_' analysis.type ...
                      'associatedTriggers_' setNameCell{iWave}  '.mat'],'-mat',...
                     'injAssociatedTrigger','injectionScale','injectionProcessedMask','iWave');

            end % -- if (vetosTuned)
        end  %-- Loop over waveforms.

        % ---- Store ULs for all frequencies
        ulVector90(thisVeto,:) = UL90p .* hrss;
        ulVector50(thisVeto,:) = UL50p .* hrss;

    end % ---- loop over different ratios

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Store UL results for all veto combos investigated
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- If we are on our first pass of the main processing loop store
    %      these results.
    if (thisRun==1)
        vetoNullRangePlots  = vetoNullRange;
        vetoPlusRangePlots  = vetoPlusRange;
        vetoCrossRangePlots = vetoCrossRange;
        ulVector90Plots     = ulVector90;
        ulVector50Plots     = ulVector50;
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                   Find best veto combo if required
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if (vetosTuned == 0) & (vetoTestOnlyFlag == 0)
        % ---- PROBLEM: currently I do not know how to pass in cell strings to
        %      compiled matlab code? There we can only tune ULs using all option
        %      or for a single waveform type.
        %      When this is sorted we will be able to tune for arbitrary combos
        %      of waveforms.
        tuningNameCell = {};
        tuningNameCell{1} = tuningNameStr; 

        % ---- Find index of veto combo which gives the best upper limit.
        [bestULIdx]=xoptimalvetotuning(ulVector90,nameCell,...
                    vetoNullRangePlots,vetoPlusRangePlots,vetoCrossRangePlots,...
                    tuningNameCell,vetoMethod,1);

        % ---- Set veto ranges to contain only the best values of the veto.
        vetoNullRange  = vetoNullRange(bestULIdx);
        vetoPlusRange  = vetoPlusRange(bestULIdx);
        vetoCrossRange = vetoCrossRange(bestULIdx);
        ulVector90     = ulVector90(bestULIdx);
        ulVector50     = ulVector50(bestULIdx);

        % ---- Now we have tuned the vetoes set flag to 1.
        %      This means plots will get made on next run through the
        %      'big loop'.
        vetosTuned = 1;
    end

end % ---- end of big loop.

disp(['Completed loop over veto cut combinations!']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Save UL results if doing vetoTestOnly
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if vetoTestOnlyFlag
    disp(['Writing matFile summary of veto tuning investigation.'])
    vetoTestFileName = [analysis.grb_name '_' user_tag ...
        '_' analysis.type '_' detectionStat '_vetoTest' vetoMethod '.mat'];
    % ---- Save UL results to a mat file.
    save(vetoTestFileName, 'ulVector90Plots', 'ulVector50Plots', ...
      'nameCell', 'parametersCell', 'parametersCellClean', 'setNameCell', 'vetoNullRangePlots', 'vetoPlusRangePlots', ...
      'vetoCrossRangePlots', 'tuningNameStr', 'vetoMethod', ...
      'nullVetoType', 'plusVetoType', 'crossVetoType', 'cFreq', 'detectionStat');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         Histogram rate vs.significance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (vetosTuned & produceWebpageFlag)
    xwriteratevssig(fout,analysis,window,onSource,offSourceSelected,...
      binArray,figures_dirName,figfiles_dirName);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Annoying triggers properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if not(isempty(regexp(vetoMethod,'None')))
  warning('Veto method is None, skiping annoying triggers')
else
  if (vetosTuned & produceWebpageFlag)
    xwritedqtriggers(fout,user_tag,analysis,max(onSource.significance .* onSource.pass),...
                     offSourceSelectedAfterDQandWindow,...
                     offSourceSelectedAfterAllCuts,onSource,figures_dirName,figfiles_dirName);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Histogram of loudest off source events 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (vetosTuned & produceWebpageFlag)
    xwriteloudestoffsource(fout,analysis,offLoudestEventPreVeto,...
      offLoudestEventWindowCut,offLoudestEventVetoSegs,...
      offLoudestEvent,binArray,...
      figures_dirName,figfiles_dirName);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Summary Section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (vetosTuned & produceWebpageFlag)
    % ---- Write header to web page.
    fprintf(fout,'%s\n','<h2>Summary</h2>');

    xwriteulplots(fout,analysis,cFreq,UL50p,hrss,...
      figures_dirName,figfiles_dirName);

    % ---- Sort results by central frequency of corresponding waveform. 
    [cFreq,I] = sort(cFreq);
    for i = 1:length(cFreq);
        tnameCell{i} = nameCell{I(i)};
        tSetNameCell{i} = setNameCell{I(i)};
        tparametersCell{i} = parametersCell{I(i)};
        dummyCell{i} = '';
    end

    nameCell = tnameCell;
    setNameCell = tSetNameCell;
    parametersCell = tparametersCell;
    hrss = hrss(I);
    UL90p = UL90p(I); 
    percentNans = percentNans(I,:); 
    SNRlist = SNRlist(I,:); 
    nullSNRlist=nullSNRlist(I,:);
    UL50p = UL50p(I);
    efficiencySorted = efficiencySorted(I,:);
    injectionScaleSorted = injectionScaleSorted(I,:);

    % ---- some output to the screen
    for idx = 1:length(cFreq)
        fprintf(1,'cFreq = %8.2f, 50%% UL = %e, 90%% UL = %e \n ', ...
          cFreq(idx),UL50p(idx),UL90p(idx)); 
    end

    [totSNR,incnullSNR] = ...
    xwritesummarytable(fout,analysis,cFreq,UL50p,UL90p,hrss,...
    percentNans,setNameCell,dummyCell,SNRlist,nullSNRlist);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Produce summary file used by Peter Kalmus X,Flare,cWB comparisons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (vetosTuned & produceWebpageFlag)
    summaryFileName = [analysis.grb_name '_' user_tag ...
        '_' analysis.type '_summary.txt']
    fsumm = fopen(summaryFileName,'w');

    % ---- construct string listing ifos used
    detectorListStr = '';
    for thisIfo = 1:length(analysis.detectorList)
        detectorListStr = [detectorListStr analysis.detectorList{thisIfo}]; 
    end

    fprintf(fsumm,'%s\n',['GPS ifos wvfs 50%%UL 90%%UL tuningNameStr '...
                      'nullVetoType tunedNullVeto '...
                      'plusVetoType tunedPlusVeto '...
                      'crossVetoType tunedCrossVeto '...
                      ]);
    for iWave = 1:length(autoFilesCell)
        fprintf(fsumm,'%s\n',[num2str(analysis.gpsCenterTime) ' '...
                        detectorListStr ' '...
                        setNameCell{iWave} ' '...
                        num2str(UL50p(iWave)*hrss(iWave)) ' '...
                        num2str(UL90p(iWave)*hrss(iWave)) ' '...
                        tuningNameStr ' '...
                        nullVetoType ' ' num2str(vetoNullRange) ' '...
                        plusVetoType ' ' num2str(vetoPlusRange) ' '...
                        crossVetoType ' ' num2str(vetoCrossRange) ' '...
                        ]);
    end
    fclose(fsumm);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Produce text file containing tuned veto values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (vetosTuned & produceWebpageFlag)
    tunedVetoFileName = [analysis.grb_name '_' user_tag '_' ...
        analysis.type '_tunedVetos.txt']
    ftunedVeto = fopen(tunedVetoFileName,'w');
    fprintf(ftunedVeto,'%s\n',[...
        nullVetoType ' ' num2str(vetoNullRange) ' '...
        plusVetoType ' ' num2str(vetoPlusRange) ' '...
        crossVetoType ' ' num2str(vetoCrossRange) ' '...
        ]);
    fclose(ftunedVeto);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Close up and save variables.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if produceWebpageFlag
    % ---- End of web page.
    fprintf(fout,'%s\n','</body>');
    fprintf(fout,'%s\n','</html>');
    fclose(fout);

    % ---- Save UL results to a mat file.
    outputMatFileName = [analysis.grb_name '_' user_tag '_' ...
        analysis.type '.mat'];
    save(outputMatFileName, 'UL90p', 'UL50p', 'hrss', 'percentNans', ...
        'SNRlist', 'nullSNRlist', 'analysis','totSNR', 'incnullSNR', ...
        'nullVetoType', 'plusVetoType', 'crossVetoType', ...
        'vetoNullRange', 'vetoPlusRange', 'vetoCrossRange', ...
        'ulVector90', 'ulVector50', 'vetoNullRangePlots', ...
        'vetoPlusRangePlots', 'vetoCrossRangePlots', 'ulVector90Plots', ...
        'ulVector50Plots', 'cFreq', 'nameCell','parametersCell','parametersCellClean', ...
        'injectionScaleSorted','efficiencySorted','offSourceSelectedBeforeDQ',...
         'offSourceSelectedAfterDQandWindow','offSourceSelectedAfterAllCuts',...
         'onSource','offSourceSelected','medianA','medianC','maxE','clusterInj');
end

disp(['xmakegrbwebpage completed successfully!']);
% ---- Done.
return

function mask = rectintersect(A, B)
% RECTINTERSECT Rectangle intersection
%
%   Returns bool table of intersection, for speed A is assumed to contain
%   only on rectangle
%
%  mask = rectintersect(A, B)
%
%  A      1-by-4 matrix giving rectangle in [X,Y,WIDTH,HEIGHT] format
%  B      N-by-4 matrix giving rectangles in [X,Y,WIDTH,HEIGHT] format
%
%  mask   bool vector of intersection between A and rectangles in B

if size(A,1) ~= 1
  error(['Number of rectangles in A is ' num2str(size(A,1)) ...
         ', it should be 1.']);
end

horMask = (A(1) <= B(:,1) & B(:,1) < A(1) + A(3)) | ...
          (B(:,1) <= A(1) & A(1) < B(:,1) + B(:,3));
verMask = (A(2) <= B(:,2) & B(:,2) < A(2) + A(4)) | ...
          (B(:,2) <= A(2) & A(2) < B(:,2) + B(:,4));
mask = horMask & verMask;

