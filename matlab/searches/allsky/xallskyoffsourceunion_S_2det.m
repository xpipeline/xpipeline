function xoffsourceunion_S_2det(dirName,fileListString,outFile)
% XOFFSOURCEUNION_S_2DET - generalised clustering of on/off-source triggers for two-detector networks
%
% usage:
%
%   xoffsourceunion_S_2det(dirName,fileList,outFile)
%
%   dirName    String. Directory containing all files to be read.
%   fileList   String. Space-separated list of results files to be read, e.g.
%              'off_source_0_26270_0.mat off_source_0_26271_0.mat off_source_0_26272_0.mat' 
%   outFile    Optional string. File to output merged clusters to. If not
%              specified, original input files are over-written. 
%
% $Id: xoffsourceunion_S_2det.m 5010 2015-07-28 15:38:54Z patrick.sutton@LIGO.ORG $

%===========================================
% FIXME - ATTENTION
%===========================================
% This code needs updating for O1 analysis, currently tuned for S6 analyses !!!

% ---- Checks.
error(nargchk(2,3,nargin));

% ---- Parse file list string into cell array of separate file names.
fileList = strread(fileListString,'%s');

% ---- Loop over all files to be merged.
for iFile = 1:length(fileList)
    fileName = [dirName '/' fileList{iFile}];
    s = load(fileName);

    % ---- find fake job numbers for each of the slides
    fjn = unique(s.cluster.fakeJobNumber);
    for m=1:length(fjn)

        % ---- Keep all elements with flag injectionProcessedMask == 1.
        index = find(s.cluster.fakeJobNumber==fjn(m));
        % ---- find nearby triggers in time and freq
        bb = s.cluster.boundingBox(index,:);
        Ntrig = size(bb,1);
        % ---- Define coincidence window in box format.
        win = [0 0 5 8];
        % ---- Test all pairs of triggers for overlap.
        olp = rectint(bb+repmat(win,size(bb,1),1),bb+repmat(win,size(bb,1),1));
        olp = triu(olp,1);
        [indI,indJ] = find(olp);
        % ---- Combine pairs into two-column matrix.
        pairs = [indI indJ];

        % ------------------------------------------------------------------------------

        % ---- Only proceed if we have triggers that need to be merged.
        if ~isempty(pairs)
        
            % ---- Split "pairs" matrix into two columns for convenience.
            pairsI = pairs(:,1);
            pairsJ = pairs(:,2);
            % ---- Complete list of triggers to be checked for membership of a generalised cluster.
            trigToCheck = 1:Ntrig;
            % ---- Storage for list of triggers in each generalised cluster.
            genClust = cell(Ntrig,1);
            % ---- Counter over generalised clusters.
            igenClust = 0;

            % ---- Loop over all Ntrig triggers, starting from the last one.
            while ~isempty(trigToCheck)    
                % ---- Start new generalsied cluster.
                igenClust = igenClust + 1;
                % ---- Select next trigger to check, from the end of the list.
                iTrig = trigToCheck(end);
                k = find(pairsI==iTrig | pairsJ==iTrig);
                % ---- Add all pairs contaiing this trigger to our current generalsied
                %      cluster, or just this one trigger if it is not piared with anyone else.
                if ~isempty(k)
                    tmp = pairs(k,:);
                    genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                    % ---- Delete all pairs containing this trigger from our working list.
                    pairs(k,:) = [];
                    pairsI(k,:) = [];
                    pairsJ(k,:) = [];
                else
                    genClust{igenClust} = iTrig;
                end
                % ---- We're finished with this trigger. Delete it from the list of
                %      to-be-checked and add it to the list of triggers we've checked. 
                trigToCheck = setdiff(trigToCheck,iTrig); 
                checkedTrigs = iTrig;
                % ---- By adding pairs to our generalsied cluser, we may have introduced new
                %      triggers. We now have to check each of the new triggers for any
                %      additional triggers they're clustered with that also have to be added ... 
                newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                while ~isempty(newtrigs)
                    for ii = 1:length(newtrigs)
                        k = find(pairsI==newtrigs(ii) | pairsJ==newtrigs(ii));
                        if ~isempty(k)
                            tmp = pairs(k,:);
                            genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                            pairs(k,:) = [];
                            pairsI(k,:) = [];
                            pairsJ(k,:) = [];
                        end
                    end
                    checkedTrigs = union(checkedTrigs,newtrigs);
                    trigToCheck = setdiff(trigToCheck,checkedTrigs); % -- finished with this trigger
                    % --- Are there more new triggers?  If so, we go through the while loop
                    %     again.  If not, we go on to the next generalised cluster.
                    newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                end        
            end

            % ---- Delete empty genClusts.
            genClust = genClust(1:igenClust);

            % ------------------------------------------------------------------------------

            % ---- Merge triggers to be clustered together.
            for igenClust=1:length(genClust)

                % ---- Triggers to be merged for this generalised cluster.
                trigIdx = genClust{igenClust};

                % ---- chop the initial trigger list and reshape the boundingBox
                % ---- apply threshold on number of pixels <20px, second trial ---- %
                newTriggers = xclustersubset(s.cluster,index(trigIdx));
                for p=1:length(newTriggers.nPixels)
                    if newTriggers.nPixels(p)<5
                        newTriggers.nPixels(p) = 1;
                        newTriggers.likelihood(p,:) = 0.0;
                        newTriggers.significance(p) = 0.0;
                    end
                end
                % ---- apply weighted average on number of pixels ---- %
                % ---- downgrade triggers in [350,450] Hz frequency band
                if length(trigIdx)>1
                    for g=1:length(trigIdx)
                        % ---- introduce a prefactor to rid off of significance < 2.75
                        % ---- and scale the off/inj by prf1234
                        %if newTriggers.likelihood(g,2) > 1.00001 & (1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.boundingBox(g,4) + 0.0000001))) > 0.00000001 & newTriggers.boundingBox(g,4) > 4.0
                         if newTriggers.likelihood(g,2) > 5.00001 & newTriggers.boundingBox(g,4) > 4.0 & newTriggers.boundingBox(g,3) > 10.01% & newTriggers.boundingBox(g,4) < 329.0
                            % introduce basic coherence checks
                            % --------------------------------
                            % do alphaNullIoverE>1.82
                            %likelihood1 = newTriggers.likelihood(g,8:9);
                            %ratioArray1=zeros(2,2);
                            %ratioArray1(1,2) = 1.82;
                            %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                            % do circEoverI> 1.2
                            likelihood2 = newTriggers.likelihood(g,10:11);
                            ratioArray2=zeros(2,2);
                            ratioArray2(1,2) = 1.2;
                            coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                            % done coherent tests, get prefactors now
                            prf1 = (newTriggers.boundingBox(g,3)*newTriggers.boundingBox(g,4))/(newTriggers.nPixels(g)*log(newTriggers.likelihood(g,2)));
                            %prf1 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.nPixels(g)));
                            %prf2 = (newTriggers.boundingBox(g,3) + 0.0000001)/(log(newTriggers.likelihood(g,2)));
                            %prf3 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.boundingBox(g,4) + 0.0000001));
                            %prf4 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.likelihood(g,8)));
                            %prf9 = abs(newTriggers.likelihood(g,8) - newTriggers.likelihood(g,9))/newTriggers.nPixels(g);
                            %prf = prf1*prf1*prf2*prf2*prf3*prf3*coh_prf2;%*prf4*prf9*prf9);
                            prf = prf1*prf1*coh_prf2;
                        else
                            prf = 0.0;
                        end
                        % ---- threshold on prf so we can remove noise triggers
                        %if prf < 0.05
                        %    prf = 0.0;
                        %end
                        % disp(prf);
                        if newTriggers.peakFrequency(g)>340.0 & newTriggers.peakFrequency(g)<460.0 | newTriggers.peakFrequency(g)>680.0 & newTriggers.peakFrequency(g)<690.0 | newTriggers.peakFrequency(g)>865.0 & newTriggers.peakFrequency(g)<885.0
                            newTriggers.likelihood(g,:) = 0.0*newTriggers.likelihood(g,:)*newTriggers.nPixels(g)*prf;
                            newTriggers.significance(g) = 0.0*newTriggers.significance(g)*newTriggers.nPixels(g)*prf;
                            newTriggers.likelihood(g,4) = 0.0*newTriggers.nPixels(g)*prf;
                        else
                            newTriggers.likelihood(g,:) = newTriggers.likelihood(g,:)*newTriggers.nPixels(g)*prf;
                            newTriggers.significance(g) = newTriggers.significance(g)*newTriggers.nPixels(g)*prf;
                            if prf>0.0000000001
                                newTriggers.likelihood(g,8) = newTriggers.likelihood(g,2)/prf;
                                newTriggers.likelihood(g,9) = newTriggers.likelihood(g,2);
                            end
                        end
                    end
                    % ---- weigh by sum(nPixels)
                    newTriggers.likelihood(1,:) = (sum(newTriggers.likelihood))/(sum(newTriggers.nPixels));
                    newTriggers.significance(1) = (sum(newTriggers.significance))/(sum(newTriggers.nPixels));
                else
                    % ---- plop prf here again
                    if newTriggers.likelihood(1,2) > 5.00001 & newTriggers.boundingBox(1,4) > 4.0 & newTriggers.boundingBox(1,3) > 10.01% & newTriggers.boundingBox(1,4) < 329.0% & newTriggers.boundingBox(1,3) > 10.01
                        % introduce basic coherence checks
                        % --------------------------------
                        % do alphaNullIoverE>1.82
                        %likelihood1 = newTriggers.likelihood(1,8:9);
                        %ratioArray1=zeros(2,2);
                        %ratioArray1(1,2) = 1.82;
                        %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                        % do circEoverI> 1.2
                        likelihood2 = newTriggers.likelihood(1,10:11);
                        ratioArray2=zeros(2,2);
                        ratioArray2(1,2) = 1.2;
                        coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                        % done coherent tests, get prefactors now
                        prf1 = (newTriggers.boundingBox(1,3)*newTriggers.boundingBox(1,4))/(newTriggers.nPixels(1)*log(newTriggers.likelihood(1,2)));
                        %prf1 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.nPixels(1)));
                        %prf2 = (newTriggers.boundingBox(1,3) + 0.0000001)/(log(newTriggers.likelihood(1,2)));
                        %prf3 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.boundingBox(1,4) + 0.0000001));
                        %prf4 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.likelihood(1,8)));
                        %prf9 = abs(newTriggers.likelihood(1,8) - newTriggers.likelihood(1,9))/newTriggers.nPixels(1);
                        %prf = prf1*prf1*prf2*prf2*prf3*prf3*coh_prf2;%*prf4*prf9*prf9);
                        prf = prf1*prf1*coh_prf2;
                    else
                        prf = 0.0;
                    end
                    % ---- threshold on prf so we can remove noise triggers
                    %if prf < 2.5
                    %    prf = 0.0;
                    %end
                    % disp(prf);
                    if newTriggers.peakFrequency(1)>340.0 & newTriggers.peakFrequency(1)<460.0 | newTriggers.peakFrequency(1)>680.0 & newTriggers.peakFrequency(1)<690.0 | newTriggers.peakFrequency(1)>865.0 & newTriggers.peakFrequency(1)<885.0
                        newTriggers.likelihood(1,:) = 0.0*newTriggers.likelihood(1,:)*prf;
                        newTriggers.significance(1) = 0.0*newTriggers.significance(1)*prf;
                        newTriggers.likelihood(1,4) = 0.0*prf;
                    else
                        newTriggers.likelihood(1,:) = newTriggers.likelihood(1,:)*prf;
                        newTriggers.significance(1) = newTriggers.significance(1)*prf;
                        if prf>0.0000000001
                            newTriggers.likelihood(1,8) = newTriggers.likelihood(1,2)/prf;
                            newTriggers.likelihood(1,9) = newTriggers.likelihood(1,2);
                        end
                    end
                end
                newTriggers.nPixels(1) = sum(newTriggers.nPixels);
                mint = min(newTriggers.boundingBox(:,1));
                maxt = max(newTriggers.boundingBox(:,1)+newTriggers.boundingBox(:,3));
                minf = min(newTriggers.boundingBox(:,2));
                maxf = max(newTriggers.boundingBox(:,2)+newTriggers.boundingBox(:,4));
                duration = maxt - mint;
                bw = maxf - minf;
                newTriggers.boundingBox(1,:) = [mint minf duration bw];
                newTriggers.peakTime(1,:) = mint + 0.5*duration;
                newTriggers.peakFrequency(1,:) = minf + 0.5*bw;

                % ---- Keep only first (merged) trigger.
                newTriggers = xclustersubset(newTriggers,1);
    
                if igenClust==1
                    clusterInj = newTriggers;
                else
                    clusterInj = xclustermerge(clusterInj,newTriggers);
                end
    
            end

        else 

            % ---- No merging needed for this fakeJobNumber. Use unedited triggers.
            % ---- but apply threshold on pixels ---- %
            clusterInj = xclustersubset(s.cluster,index);
            for r=1:length(clusterInj.nPixels)
                if clusterInj.nPixels(r)<5
                    clusterInj.nPixels(r) = 1;
                    clusterInj.likelihood(r,:) = 0.0;
                    clusterInj.significance(r) = 0.0;
                end
            end
            % ---- extra bit of messing with likelihoods ---- %
            % ---- downgrade in frequency band
            for a=1:length(clusterInj.peakFrequency)
                % ---- plop prf here again
                if clusterInj.likelihood(a,2) > 5.00001 & clusterInj.boundingBox(a,4) > 4.0 & clusterInj.boundingBox(a,3) > 10.01% & clusterInj.boundingBox(a,4) < 329.0% & clusterInj.boundingBox(a,3) > 10.01
                    % introduce basic coherence checks
                    % --------------------------------
                    % do alphaNullIoverE>1.82
                    %likelihood1 = clusterInj.likelihood(a,8:9);
                    %ratioArray1=zeros(2,2);
                    %ratioArray1(1,2) = 1.82;
                    %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                    % do circEoverI> 1.2
                    likelihood2 = clusterInj.likelihood(a,10:11);
                    ratioArray2=zeros(2,2);
                    ratioArray2(1,2) = 1.2;
                    coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                    % done coherent tests, get prefactors now
                    prf1 = (clusterInj.boundingBox(a,3)*clusterInj.boundingBox(a,4))/(clusterInj.nPixels(a)*log(clusterInj.likelihood(a,2)));
                    %prf1 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.nPixels(a)));
                    %prf2 = (clusterInj.boundingBox(a,3) + 0.0000001)/(log(clusterInj.likelihood(a,2)));
                    %prf3 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.boundingBox(a,4) + 0.0000001));
                    %prf4 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.likelihood(a,8)));
                    %prf9 = abs(clusterInj.likelihood(a,8) - clusterInj.likelihood(a,9))/clusterInj.nPixels(a);
                    %prf = prf1*prf1*prf2*prf2*prf3*prf3*coh_prf2;%*prf4*prf9*prf9);
                    prf = prf1*prf1*coh_prf2;
                else
                    prf = 0.0;
                end
                % ---- threshold on prf so we can remove noise triggers
                %if prf < 2.5
                %    prf = 0.0;
                %end
                % disp(prf);
                if clusterInj.peakFrequency(a)>340.0 & clusterInj.peakFrequency(a)<460.0 | clusterInj.peakFrequency(a)>680.0 & clusterInj.peakFrequency(a)<690.0 | clusterInj.peakFrequency(a)>865.0 & clusterInj.peakFrequency(a)<885.0
                    clusterInj.likelihood(a,:) = 0.0*clusterInj.likelihood(a,:)*prf;
                    clusterInj.significance(a) = 0.0*clusterInj.significance(a)*prf;
                    clusterInj.likelihood(a,4) = 0.0*prf;
                else
                    clusterInj.likelihood(a,:) = clusterInj.likelihood(a,:)*prf;
                    clusterInj.significance(a) = clusterInj.significance(a)*prf;
                    if prf>0.0000000001
                        clusterInj.likelihood(a,8) = clusterInj.likelihood(a,2)/prf;
                        clusterInj.likelihood(a,9) = clusterInj.likelihood(a,2);
                    end
                end
            end
 
        end
        % ---- Save merged triggers for this fakeJobNumber into output array.
        if m==1
            outCluster = clusterInj;
        else
            outCluster = xclustermerge(outCluster,clusterInj);
        end
    end % ---- for loop on fakeJobNumber (fjn)

    % ---- Re-order field names to match original file.
    outCluster = orderfields(outCluster,s.cluster);
    % ---- Now save clusters back to original file.
    s.cluster = outCluster;

    if nargin<3
        outFile = fileName;
    end
    save(outFile, '-struct', 's');
    
end

% ---- Done.
return
