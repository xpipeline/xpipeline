#!/usr/bin/env python
"""
dagsplitter

takes a rescue dag in splits it in three dags so three people can
run those two sub-rescue -dags independently

FIXME make this option parsable for however many spewed rescue dags one needs

"""
__author__ = 'Valeriu Predoi < valeriu.predoi@astro.cf.ac.uk>'
__date__ = '$Date$'
__version__ = '$Revision$'
__name__ = 'xdags'

import numpy as np

dag = np.genfromtxt('grb_off_source.dag.rescue002',dtype="str",usecols=(0, 1))
file1 = open('file1.txt', 'w')
file2 = open('file2.txt', 'w')
file3 = open('file3.txt', 'w')

no_retry_indices=[]

# ---- get all the DONE nodes and place in ALL new rescue files
print('Writing all previously DONE jobs to all spewed rescue dags...')
for line in dag:
    if len(line)>0:
        if line[0]=='DONE':
            file1.write('DONE'+' '+line[1]+'\n')
            file1.write('RETRY'+' '+line[1]+' '+'5'+'\n')
            file2.write('DONE'+' '+line[1]+'\n')
            file2.write('RETRY'+' '+line[1]+' '+'5'+'\n')
            file3.write('DONE'+' '+line[1]+'\n')
            file3.write('RETRY'+' '+line[1]+' '+'5'+'\n')
            no_retry_indices.append(np.where(np.all(dag==line,axis=1))[0][0])
            no_retry_indices.append(np.where(np.all(dag==line,axis=1))[0][0]+1)

newdag = np.delete(dag,no_retry_indices,0)
print(('Found %i jobs previously DONE' % (len(no_retry_indices)/2)))

# ---- filter out only the RETRY that are not associated with DONE nodes
# ---- divide indices in three overlapping lists
print(('Found %i jobs that still need to be done and will be divided amongst spewed dags' % len(newdag)))
newdag1 = newdag[0:len(newdag)/3]
newdag2 = newdag[len(newdag)/3:2*len(newdag)/3]
newdag3 = newdag[2*len(newdag)/3::]

# ---- write to files file1, file2 and file3
# ---- interchange RETRY jobs for fileX to be DONE for fileY
print('Splitting main rescue dag, speweng smaller rescue dags...')
for line in newdag1:
    file1.write(line[0]+' '+line[1]+' '+'5'+'\n')
    file2.write('DONE'+' '+line[1]+' '+'5'+'\n')
    file2.write(line[0]+' '+line[1]+' '+'5'+'\n')
    file3.write('DONE'+' '+line[1]+' '+'5'+'\n')
    file3.write(line[0]+' '+line[1]+' '+'5'+'\n')
print(('...just wrote DAG 1 containing %i jobs still to be done' % len(newdag1)))
for line in newdag2:
    file2.write(line[0]+' '+line[1]+' '+'5'+'\n')
    file1.write('DONE'+' '+line[1]+' '+'5'+'\n')
    file1.write(line[0]+' '+line[1]+' '+'5'+'\n')
    file3.write('DONE'+' '+line[1]+' '+'5'+'\n')
    file3.write(line[0]+' '+line[1]+' '+'5'+'\n')
print(('...just wrote DAG 2 containing %i jobs still to be done' % len(newdag2)))
for line in newdag3:
    file3.write(line[0]+' '+line[1]+' '+'5'+'\n')
    file2.write('DONE'+' '+line[1]+' '+'5'+'\n')
    file2.write(line[0]+' '+line[1]+' '+'5'+'\n')
    file1.write('DONE'+' '+line[1]+' '+'5'+'\n')
    file1.write(line[0]+' '+line[1]+' '+'5'+'\n')
print(('...just wrote DAG 3 containing %i jobs still to be done...Done!' % len(newdag3)))










