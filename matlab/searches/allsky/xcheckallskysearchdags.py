#!/usr/bin/env python
"""
XSEARCHDAGS - give a brief dag status of xsearch jobs

$Id $

The status of each xsearch dag is
reported as well as the total number of jobs, the number of submitted jobs
and the progress.

"""
__author__ = 'Valeriu Predoi < valeriu.predoi@astro.cf.ac.uk>'
__date__ = '$Date$'
__version__ = '$Revision$'
__name__ = 'xdags'

import os
import sys
from time import sleep

##############################################################################
def get_status(filename, totals, debug=False):

    tab = "    "
    dag_status = 0
    num = 0
    output = 0

    thisdag = open(filename + '.dagman.out', 'r')

    if dag_status == 0:
        line = "dummy"
        while line:
            line = thisdag.readline()
            if line.find('Done')>=0:
                output = tab + line      
                line = thisdag.readline()
                output = output + tab + line
                #read the numbers
                line = thisdag.readline()
                #numbers = line.split(" ")
                num = line
                #number_of_undone_jobs = int(numbers[len(numbers)-1])
                output = output + tab + line
            if line.find('EXITING WITH STATUS 0')>=0:
                dag_status = 1

        # count the number of jobs
        if num != 0:  
            num=num.split()
            num = num[2:]
            for i in range( len( num ) ):
                totals[i] += int(num[i])


    if output != 0:
        print(output[:-1])

    return dag_status

# full analysis DAGs
filenames = ['grb_on_source.dag', 'grb_off_source.dag', 'grb_simulations_adi-a.dag', 'grb_simulations_adi-b.dag', 'grb_simulations_adi-c.dag', 'grb_simulations_adi-d.dag', 'grb_simulations_adi-e.dag', 'grb_simulations_ebbh-a.dag', 'grb_simulations_ebbh-d.dag', 'grb_simulations_ebbh-e.dag','grb_simulations_mva.dag','grb_simulations_brst-s6.dag','grb_simulations_elptc-s6.dag']

# stage 2 triggers only (on and off source)
#filenames = ['grb_on_source.dag', 'grb_off_source.dag']

# injection runs only
#filenames = ['grb_simulations_adi-a.dag', 'grb_simulations_adi-b.dag', 'grb_simulations_adi-c.dag', 'grb_simulations_adi-d.dag', 'grb_simulations_adi-e.dag', 'grb_simulations_ebbh-a.dag', 'grb_simulations_ebbh-d.dag', 'grb_simulations_ebbh-e.dag']

# test dag running
#filenames = ['grb_simulations_adi-a.dag']


sub_dags = len( filenames )
print('Sweet, we found', str( sub_dags ), 'xpipeline processing dags!!')
done_dags = 0  

print('Parsing the dag files for status...\n')
### we found some dag files hopefully

sleep(1.5)
totals = [0,0,0,0,0,0,0,0]

#parse all the dags one by one
for i in range(0,len(filenames), 1):
    filename = filenames[i]

    print('-------------------------------------------------------------------------')
    # How many jobs are completed/failed ?
    print("Parsing " + filenames[i])

    status = get_status(filename, totals)

    if status==1: 
        done_dags += 1 
        print("COMPLETE :)")
    elif status==0:
        print("bloody incomplete :(")
    elif status==-1:
        print("dag not yet started!")
    elif status==-2:
        print("dag pending and not yet planned!")


# Print totals
print("  --------------------------- ")
print("      Done    =", totals[0])
#print " Pre     =", totals[1]
print("      Queued  =", totals[2])
#print " Post    =", totals[3]
print("      Ready   =", totals[4])
print("      Unready =", totals[5])
print("      Failed  =", totals[6])
print("  ----------------------------------- ")
print("      Completed Jobs = " + str( totals[0] ))
print("      Submitted Jobs = " + str( sum( totals[:7] ) ))
print("      Total Jobs     = " + str( sum(totals[:]) ))
if sub_dags != 0:
    print("      Done dags      =", str( done_dags ) + "/" + str( sub_dags ))
print("  ----------------------------------- ")
