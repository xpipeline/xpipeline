function [Coinc] = Coincidence4(X,dX,Y,dY,Z,dZ,W,dW);
% COINCIDENCE4: Find overlapping segments between four segment lists.
%
%  [Coinc] = Coincidence3(X,dX,Y,dY,Z,dZ,W,dW);
%
% returns the intersection times and indices of time intervals W, X, Y, and Z   
% which overlap.
% 
%   X     Start times of first segment list
%   dX    Durations of first segment list
%   Y     Start times of second segment list
%   dY    Durations of second segment list
%   Z     Start times of third segment list
%   dZ    Durations of third segment list
%   W     Start times of fourth segment list
%   dW    Durations of fourth segment list
% 
%   Coinc Mx4 array, where M is the number of pairs of coincident segments 
%         between X, Y, Z, and W.  The columns are the start time of the 
%         intersection of the coincident segments, the duration of the 
%         intersection, the start time of the union of the coincident 
%         segments, the duration of the union, the index of the coincident   
%         X segment, the index of the coincident Y segment, the index of 
%         the coincident Z segment, and the index of 
%         the coincident W segment.
%
% An n-tuple of segments [X(i),X(i)+dX(i)], [Y(j),Y(j)+dY(j)], ..., 
% [Z(k),Z(k)+dZ(k)] are considered coincident if their intersection is 
% non-empty, ie, if 
%
%   max(X(i),Y(j),...,Z(k)) <= min(X(i)+dX(i),Y(j)+dY(j),...,Z(k)+dZ(k))
% 
% Note that the intersection may be a single point.
%
% The input lists must be time-ordered according to the start time of the 
% intervals.  Segments within a list may be overlapping.
%
% The output list is time-sorted according to the start of the intersection
% times (and not by the start of the union times).
%
% HINT: The function is typically faster if the segment lists are ordered 
% by increasing size (ie, if length(X)<=length(Y)<=length(Z)<=length(W)).
%
% Adapted from J. Sylvestre's "time_coin.m" for two-IFO coincidence.
%
% -- Patrick J. Sutton 2004.06.15
%              updated 2004.08.07
% $Id$


%----- This algorithm works by iteratively applying Coincidence2.

%----- Prepare storage.
Coinc = [];

%---- First make sure arrays non-empty.
if (max(size(X))*max(size(Y))*max(size(Z))*max(size(W))>0)

    %---- Do triple coincidence.
    tempCoinc = Coincidence3(X,dX,Y,dY,Z,dZ);
    if (max(size(tempCoinc))>0)

        %----- Quadruple coincidence
        Coinc = Coincidence2(tempCoinc(:,1),tempCoinc(:,2),W,dW);
        if (max(size(Coinc))>0)
            
            %----- Now, you've got the intersection intervals right, but the
            %      indices and union times have to be fixed.
            %----- Replace indices of "tempCoinc" by indices of X, Y, Z.
            Nrow = size(Coinc,1);
            Ncolumn = size(Coinc,2);
            Coinc(:,Ncolumn+2) = Coinc(:,Ncolumn);
            for irow=1:Nrow
                index = Coinc(irow,Ncolumn-1);
                Coinc(irow,Ncolumn-1:Ncolumn+1) = tempCoinc(index,end-2:end);
            end
            %----- Go back through triples list and get union times.
            for j=1:length(Coinc(:,1))
                start = min([X(Coinc(j,5)),Y(Coinc(j,6)),Z(Coinc(j,7)),W(Coinc(j,8))]);
                stop = max([X(Coinc(j,5))+dX(Coinc(j,5)),Y(Coinc(j,6))+dY(Coinc(j,6)),Z(Coinc(j,7))+dZ(Coinc(j,7)),W(Coinc(j,8))+dW(Coinc(j,8))]);
                dur = stop-start;
                Coinc(j,3) = start;
                Coinc(j,4) = dur;
            end

        end

    end

end

%----- Done.
return

