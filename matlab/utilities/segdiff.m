function [Z,dZ] = segdiff(X,dX,Y,dY)
% SEGDIFF - compute the difference between two segment lists
%
% usage:
%
%   [Z,dZ] = segdiff(X,dX,Y,dY)
%
% SEGDIFF returns the list of times in (X,dX) that are not in (Y,dY). 
%  
%  X     Vector of start times [sec] of first segment list.
%  dX    Vector of durations [sec] of first segment list.
%  Y     Vector of start times [sec] of second segment list.
%  dY    Vector of durations [sec] of second segment list.
%
%  Z     Vector of start times [sec] of output segment list.
%  dZ    Vector of durations [sec] of output segment list.
%
% The segments (Z,dZ) are the intersection of (X,dX) with not[(Y,dY)]. They will
% be column vectors regardless of the shape of the inputs.
%
% See also SEGUNION, Coincidence2, ComplementSegmentList.

% ---- Sanity checks.
if length(X)~=length(dX)
    error('Start time and duration lists of different length for first input segments X,dX.');
end
if length(Y)~=length(dY)
    error('Start time and duration lists of different length for second input segments Y,dY.');
end

% ---- Force column vectors.
X  = X(:);
dX = dX(:);
Y  = Y(:);
dY = dY(:);

% ---- We desire the segments in X not in Y. Compute this as the intersection of
%      X with not(Y).
tmp = ComplementSegmentList(Y,dY,0,Inf);
notY  = tmp(:,1);
notdY = tmp(:,2);

% ---- Compute intersection of X with not(Y). Run output through
%      concatenatesegmentlist to make sure segments are "clean".
tmp = Coincidence2(X,dX,notY,notdY);
if ~isempty(tmp)
    tmp = concatenatesegmentlist(tmp(:,1),tmp(:,2));
end
if ~isempty(tmp)
    Z  = tmp(:,1);
    dZ = tmp(:,2);
else
    Z  = [];
    dZ = [];
end
