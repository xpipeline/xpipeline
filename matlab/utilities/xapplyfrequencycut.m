function pass = xapplyfrequencycut(window,triggers)
% XAPPLYFREQUENCYCUT - Veto events lying outside a specified frequency band.
% 
% The XAPPLYFREQUENCYCUT function passes any cluster whose peak frequency
% lies within a specified frequency range.
% 
% usage: 
% 
%   pass = xapplyfrequencycut(window,triggers) 
%
%   window      Vector.  Elements are minimum and maximum frequency [Hz].
%   triggers    Structure containing onSource, offSource, or injection
%               triggers
%
%   pass        Logical vector.  True/1 means the peak frequency of the
%               corresponding event was in the range [window(1),window(2)].
%
% $Id$

% ---- Checks.
error(nargchk(2, 2, nargin));
if ~isvector(window) | length(window)~=2
    error('Input window must be a vector with two elements.');
end
if ~isfield(triggers,'peakFrequency')
    error('Struct field peakFrequency not found.');
end

% ---- By default we will assume that all clusters fall outside of
%      our window.
pass = (triggers.peakFrequency >= window(1)) & (triggers.peakFrequency <= window(2));

% -- Done
return
