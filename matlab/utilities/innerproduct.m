function [Y] = innerproduct(h1,h2,fs,S,F0,dF,Fmin,Fmax,doplot)
% INNERPRODUCT - Compute the noise-weighted inner product of two vectors.
%
%   Y = innerproduct(X1,X2,fs,S,F0,dF,Fmin,Fmax,doplot)
%
%   X1     Vector.  Waveform timeseries data.  
%   X2     Vector.  Waveform timeseries data.  
%   fs     Scalar.  Sampling rate [Hz] of waveform data.
%   S      Vector (optional).  Noise background one-sided POWER (not
%          amplitude) spectrum.  If supplied, it must cover at least the
%          range [Fmin, Fmax], and must be linearly sampled in ascending
%          order of frequency.  For noise-independent signal measures use
%          S=f0=df=[].  
%   F0     Scalar (optional).  Frequency at which S(1) is sampled.
%   dF     Scalar (optional).  Frequency spacing of the noise spectrum S.
%   Fmin   Scalar (optional).  Minimum frequency (Hz) to include in 
%          frequency-domain calculations.  Defaults to 0 if no noise
%          spectrum specified, or minimum frequency of noise spectrum.
%   Fmax   Scalar (optional).  Maximum frequency (Hz) to include in 
%          frequency-domain calculations. Defaults to Nyquist if no noise
%          spectrum specified, or maximum frequency of noise spectrum.
%   doplot Scalar (optional).  If set to 1, creates a new figure with a
%          plot of the modulus of the Fourier transform of h and the noise
%          spectrum S.  
%
%   Y      Inner product in the given noise background, defined as the
%          discrete approximation to
%
%            Y[t_i] = 2 \int_{-\infty}^{\infty} df exp{i2\pi*f*[(i-1)*dt]} 
%                   X1(f)*conj(X2(f)) / S(|f|)  ,  Fmin <= |f| <= Fmax
%
%          where dt = 1/fs.  The normalization is such that for X1=X2,
%          Y[1] := Y[t_1=0] is the expected SNR^2 response of an optimal
%          filter for that signal in Gaussian background noise restricted
%          to frequencies [Fmin,Fmax]:
%
%            Y[1] = Y[t_1=0] = 4 \int_Fmin^Fmax df |X1(f)|^2 / S(f)
%
%          If the noise S(f) is not specified then the constant value
%          S=2/fs, which corresponds to unit-variance white noise.  With 
%          this value Y[m] is the ordinary correlation of vectors X1 and X2
%          restricted to the desired frequency band: 
%
%            Y[m] ~ \sum_j X1[j]*X2[j-m+1] = \sum_j X1[j+m-1]*X2[j]
%            Y[1] ~ dot(X1,X2)
%
% Notes:
%
% The power spectrum S is interpolated (if needed) to the frequencies of
% fft(h).  A spectral density of Inf is used for frequencies outside the
% range specified by S or [Fmin,Fmax].
%
% Note that no windowing is used for computing FFTs; windowing and 
% averaging is not really sensible for a transient signal, since by
% definition the statistical properties of the signal are changing over
% time.  There may be problems for, e.g., band-passed noise bursts.
%
% See FFTCONVENTIONS for information on the conventions used for Fourier
% transforms. 
% 
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Checks.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Valid number of input arguments.
error(nargchk(3,9,nargin,'struct'))

% ---- Optional arguments.
if (nargin<9)
    doplot = 0;
end
% ---- Is there an input noise spectrum?
if ( (nargin<6) || isequal(S,[]) || isequal(F0,[]) || isequal(dF,[]) )
    % ---- Set flag to make dummy noise spectrum.
    noise = 0;
else
    noise = 1;
    % ---- Validate noise spectrum data.
    if (~isscalar(dF) | dF<=0)
        error('Sprectrum frequency resolution dF must be a positive scalar.');
    end
    if (~isscalar(F0) | F0<0)
        error('Sprectrum lowest frequency F0 must be a non-negative scalar.');
    end
    if ~isvector(S)
        error('Spectrum S be a vector or empty array.');
    end
    % ---- Vector of sampled noise frequencies.
    F = F0+[0:length(S)-1]'*dF;
    % ---- Force column vectors.
    S = S(:);
end
% ---- Frequency range of analysis.
if ( (nargin<8) || isequal(Fmax,[]) )
    if noise
        % ---- Default to highest frequency in supplied spectrum.
        Fmax = F(end);
    else
        % ---- Default to Nyquist.
        Fmax = floor(fs/2);
    end
end
if ( (nargin<7) || isequal(Fmin,[]) )
    if noise
        % ---- Default to lowest frequency in supplied spectrum.
        Fmin = F(1);
    else
        % ---- Default to DC.
        Fmin = 0;
    end
end

% ---- Error checks.
if (~isscalar(Fmax) | Fmax<=0)
    error('Frequency limit Fmax must be a positive scalar.');
end
if (~isscalar(Fmin) | Fmin<0)
    error('Frequency limit Fmin must be a non-negative scalar.');
end
if Fmin>=Fmax
    error('Frequency limits must satisfy Fmin<Fmax.');
end
% ---- Require positive sampling rate.
if fs<=0
    error('Sampling rate fs must be positive.');
end
if(size(h1)~=size(h2))
    error('Input vectors X1, X2 must be same size.')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Preparations.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Convert timeseries to column vectors.
h1 = h1(:);
h2 = h2(:);

% ---- Number of data points.  Force to be even.
N = length(h1);
if ~iseven(N)
    warning(['Function not guaranteed for waveforms with odd number ' ...
        'of samples.  Dropping last sample']);
    h1(end) = [];
    h2(end) = [];
    N = length(h1);
end

% ---- Duration of timeseries [sec].
T = N/fs;  

% ---- Verify that T is a power of 2.
if T ~= 2^round(log2(T))
    warning(['Function is not guaranteed for timeseries durations that ' ...
        'are not powers of 2.']);
end

%----- Standard FFT.  Returns row/column vector according to input.
h1f = 1/fs*fft(h1);
h2f = 1/fs*fft(h2);

% ---- Vector holding frequencies in usual screwy FFT order:
%        vector element:   [ 1  2  ...  N/2-1  N/2    N/2+1            N/2+2   ... N-1  N ]
%        frequency (df):   [ 0  1  ...  N/2-2  N/2-1  (N/2 or -N/2)   -N/2+1  ... -2   -1 ]
f = [ 0:N/2 , -N/2+1:-1 ]'/T;

% ---- Extract non-negative frequencies in requested analysis range.
index = find(f>=Fmin & f<=Fmax);
f_range = f(index);

% ---- Construct 2-sided noise spectrum.  Any values not specified by user
%      are set to inf.
if (~noise)
    %----- If no noise defined.  Make useful dummy noise vector of ones
    %      covering [0,Nyquist] Hz. 
    S = ones(size(f))/fs;
    % S(1) = 0.5;      %-- KLUDGE check this!
    % S(N/2+1) = 0.5;  %-- KLUDGE check this!
else
    % ---- Does vector of sampled noise frequencies cover requested range?
    if ( (F(1)>Fmin) | (F(end)<Fmax) ) 
        error('Noise spectrum does not cover desired frequency range.')
    end
    % ---- Force interpolation of S from sampled frequencies F to data
    %      frequencies f lying within [Fmin,Fmax].
    S = interp1(F,S,f_range);
    F = f_range;
    dF = 1/T;
    F0 = F(1);
    % ---- Extract zero freq, Nyquist freq if specified.
    if (F(end)==fs/2)
        SNyq = S(end);
        S(end) = [];
    else
        SNyq = inf(1);
    end
    if (F0==0)
        S0 = S(1);
        S(1) = [];
        F0 = F0 + dF;
    else
        S0 = inf(1);
    end
    % ---- Reset vector of noise frequencies to match spectrum (in case
    %      zero or Nyquist frequencies have been removed). 
    F = F0+[0:length(S)-1]'*dF;
    % ---- Pad S to fill out all positive frequency bins below Nyquist.
    %      Note translation of noise frequencies into samples:
    %        vector element:   [ 1  2  ...  N/2-1  N/2    N/2+1            N/2+2   ... N-1  N ]
    %        frequency (df):   [ 0  1  ...  N/2-2  N/2-1  (N/2 or -N/2)   -N/2+1  ... -2   -1 ]
    FfirstSample = round(F(1)*T)+1;
    FlastSample = round(F(end)*T)+1;
    N1 = FfirstSample-2;
    N2 = N/2-FlastSample;
    pad1 = [];
    pad2 = [];
    if (N1>0)
        pad1 = Inf(N1,1);
    end
    if (N2>0)
        pad2 = Inf(N2,1);
    end    
    % ---- S at all positive frequencies below Nyquist.  Use S/2 instead of
    %      S to convert from one-sided to two-sided power spectrum.
    Stemp = [ pad1; S/2; pad2 ];
    % ---- Double-sided S at all frequencies.
    S = [ S0; Stemp; SNyq; flipud(Stemp)];
end

% ---- Reset noise to Inf outside of desired band.
k = find(abs(f)<Fmin | abs(f)>Fmax);
S(k) = Inf;

% ---- Filter integrand.
p_FD = h1f.*conj(h2f)./S;

% ---- Inverse FFT to get time-domain filter output.
Y = ifft(p_FD)*fs;

% ---- Plot if desired.
if (doplot)
    k = find(f>0);
    figure; set(gca,'fontsize',16)
    loglog(f(k),2*abs(p_FD(k)),'linewidth',2)
    grid on; hold on
    xlabel('Frequency (Hz)')
    ylabel('|h_1(f) x h_2(f)^*|/S_{1 sided} (Hz^{-1})')
end

% ---- Done
return
