function [A C] = xmedianfit(E,I,minE,maxE)
% XMEDIANFIT - compute fit parameters for running median of (X,Y) data.
%
% usage:
%
%   [A C] = xmedianfit(X,Y,minX,maxX)
%
%   X       Vector.
%   Y       Vector.
%   minX    Scalar.
%   maxX    Scalar.
%
%   A       Scalar.
%   C       Scalar.
% 
% xmedianfit fits a curve of the form Y = AX^2+C to the input data.  It
% first uses the function xmedianbin to bin the data in X in log-spaced
% bins between minX,maxX and compute the median values of X,Y in each bin.
% It then uses the "\" operator to compute the values of A and C that are
% the best fit to the log-spaced median data.
%
% see xmedianbin.
%
% $Id$

% ---- Get running median.  We convert to log space for equally-spaced bins
%      in log space.
[medLogE, medLogI] = xmedianbin(log10(E),log10(I),log10(minE),log10(maxE));

% ---- Fit curve of form Y = AX^2+C to log-spaced median data.
Y = medLogI;
X(:,2) = (medLogE).^2;
X(:,1) = 1;
A = X\Y;
C = A(1);
A = A(2);

% ---- Done.
return
