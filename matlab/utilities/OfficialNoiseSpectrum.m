function [f,ASD,date] = OfficialNoiseSpectrum(run,ifo,varargin)
% OFFICIALNOISESPECTRUM - Return an official strain noise spectrum.
%
% OfficialNoiseSpectrum returns a sampled strain noise 
% spectrum for a specified science run and detector.
%
%   [f,ASD,date] = OfficialNoiseSpectrum(run,ifo,'PropertyName', ...
%       'PropertyValue',...)
%
%   run   String specifying a science run or design curve ('SRD').  See
%         below for which runs are available for a given detector.
%   ifo   String. One of 'AU' (AURIGA), 'G1', 'H1', 'H2', 'L1', 'T1', 'V1'. 
%
%   f     Frequencies at which spectrum is sampled (Hz).  
%   ASD   Strain noise amplitude spectral density (Hz^{-1/2}).
%   date  Date stamp of the spectrum (yy/mm/dd).
%
% The 'PropertyName','PropertyValue' pairs allow for optional arguments.
% Recognized pairs are:
%
%   'fileDir',string
%
%         Absolute path to the directory containing the sampled noise
%         spectra.  If not supplied, OfficialNoiseSpectrum will attempt
%         to load the appropriate spectrum from a hard-coded location.
%
% The following "official" spectra are available:
%
%     Detector     Runs
%     --------     -----
%     AU                           S3 
%     G1           SRD,  S1,       S3,  S4,  S5 
%     H1           SRD,  S1,  S2,  S3,  S4,  S5
%     H2           SRD,  S1,  S2,  S3,  S4,  S5
%     L1           SRD,  S1,  S2,  S3,  S4,  S5
%     T1                      S2,  S3 
%     V1                                     WSR10 
%
% If the requested spectrum is not available, an error occurs.
%
% Other notes:
% ------------
% o The LIGO spectra are taken from 
%     http://www.ligo.caltech.edu/~jzweizig/distribution/LSC_Data/
% o For S3 the best of the official noise curves is used for each LIGO IFO 
%   (H1: 2004.01.04, H2: 2003.11.30, L1: 2003.12.20).
% o For S4 the 'v4' calibrated psectra are used.
% o For S5 the June 2006 curves are used (the "June" H1 spectrum is 
%   actually from March 2006).  
% o For G1 the 200 Hz signal detuning design spectrum 
%   is returned for run='SRD'.
% o For G1 the S3 - part II spectrum is returned for run='S3'.
% o For G1 the 24/7 "May+ typical" spectrum is returned for run='S5'.
% o The AURIGA S3 spectrum is taken from AURIGA run R331.
% o The TAMA S2 and S3 spectra are taken from TAMA runs DT8 and DT9.
% In each case the zero frequency component is removed.  
%
% Files containing the official noise spectra are read from a hard-coded 
% location.  This mfile should be edited to point to the appropriate 
% location on your system, or the optional fileDir argument may be
% supplied. 
%
% The function SRD also returns approximate design spectra for many 
% detectors, at user-specified frequencies.
%
% -- Patrick J. Sutton 
%    initial write: 2004.02.11
%
% $Id$

% ---- Location of sampled spectra on the local system.
fileDir = '/Users/psutton/Documents/Matlab_files/SNR/OfficialNoiseSpectra/';

% ---- Check for optional arguments.
if (nargin>2 && length(varargin))
    % ---- Make sure they are in pairs.
    if (length(varargin) == 2*floor(length(varargin)/2))
        % ---- Parse them.
        index = 1;
        while index<length(varargin)
            switch lower(varargin{index})
                case 'filedir'
                    fileDir = varargin{index+1};
                otherwise
                    error(['Property value ' varargin{index} ' not recognized.']);
            end
            index = index + 2;
        end
    else
        error('Optional arguments must appear in pairs: property_name, property_value.');
    end
end

% ---- Figure out date stamp of the file we want.  Initialize to empty.
audate = '';
g1date = '';
h1date = '';
h2date = '';
l1date = '';
t1date = '';
v1date = '';
switch lower(run)
    case 's1' 
        g1date = 'S1';
        h1date = '020909';
        h2date = '020909';
        l1date = '020907';
    case 's2'
        h1date = '030408';
        h2date = '030411';
        l1date = '030301';
        t1date = '030220';
    case 's3'
        audate = 'R331';
        g1date = 'S3II';
        h1date = '040104';
        h2date = '031130';
        l1date = '031220';
        t1date = 'DT9';
    case 's4'
        g1date = 'S4';
        h1date = '050226';
        h2date = '050226';
        l1date = '050311';
    case 's5'
        g1date = 'S5';
        h1date = '060313';
        h2date = '060618';
        l1date = '060604';
    case 'wsr10'
    	v1date = 'WSR10';
    case 'srd'
        g1date = 'SRD200';
        h1date = 'SRD';
        h2date = 'SRD';
        l1date = 'SRD';
    otherwise
        error('Unknown run requested.')
end

% ---- Fix IFO.
date = '';
switch lower(ifo)
    case 'au'
        date = audate;
    case 'g1'
        date = g1date;
    case 'h1'
        date = h1date;
    case 'h2'
        date = h2date;
    case 'l1'
        date = l1date;
    case 't1'
        date = t1date;
    case 'v1'
        date = v1date;
    otherwise
        error('Unknown detector requested.')
end        

% ---- Load noise spectrum.
if isempty(date)
    error('Noise spectrum for requested run, detector not available.')
else
    [f,ASD] = textread([fileDir upper(ifo) '_' date '_strain.txt'],'%f %f');
end

%----- Done.
return
