function [lambda,beta] = equatorialtoecliptic(alpha,delta)
% EQUATORIALTOECLIPTIC: Convert from equatorial to ecliptic sky coordinates.
%  
%   [lambda,beta] = equatorialtoecliptic(alpha,delta)
%
% alpha     Vector. Right ascension in equatorial (sky-based) coordinates (rad).
% delta     Vector. Declination in equatorial coordinates (rad).
%
% lambda    Ecliptic longitude (rad), from [0,2*pi).
% beta      Ecliptic latitude (rad), from [-pi/2,pi/2].
%
% For definitions and discussion of the equatorial and ecliptic 
% cooridnate systems, see the LAL Software Document, Section 16.7, 
% "SkyCoordinates.h"
%
% -- first version: Patrick J. Sutton 2010.04.23

% ---- Constant defining ecliptic coordinate system.
%      See LAL Software Document, Section 16.7, "SkyCoordinates.h"
epsilon = 23.4392911*pi/180;

% ---- Transformation from equatorial to ecliptic coordinates.
% ---- latitude
beta = asin( sin(delta)*cos(epsilon) - cos(delta).*sin(alpha)*sin(epsilon) );
% ---- longitude, wrapped to [0,2*pi)
lambda = atan2( cos(delta).*sin(alpha)*cos(epsilon) + sin(delta)*sin(epsilon) , cos(delta).*cos(alpha) );
lambda = mod(lambda,2*pi);

% ---- Done
return
