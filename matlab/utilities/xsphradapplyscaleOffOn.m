
% ---- utility code to apply scaling factors to XSphRad triggers when switching from one FFT analysis to another
% ---- VPredoi 2015

myFolders = {'output/off_source_0_0_merged.mat','output/on_source_0_0_merged.mat'};


for i=1:length(myFolders)
                myFile = myFolders{i};
                s=load(myFile);
                if ~isempty(s.cluster.likelihood)
                  fft = s.analysisTimesCell{1};
                  s.cluster.significance = (fft^2.0)*s.cluster.significance;
                  s.cluster.likelihood(:,1) = (fft^4.0)*s.cluster.likelihood(:,1);
                  s.cluster.likelihood(:,2) = (fft^2.0)*s.cluster.likelihood(:,2);
                  s.cluster.likelihood(:,3) = (fft^2.0)*s.cluster.likelihood(:,3);
                  s.cluster.likelihood(:,4) = (fft^2.0)*s.cluster.likelihood(:,4);
                  s.cluster.likelihood(:,5) = (fft^2.0)*s.cluster.likelihood(:,5);
                  s.cluster.likelihood(:,6) = (fft^2.0)*s.cluster.likelihood(:,6);
                  s.cluster.likelihood(:,7) = (fft^2.0)*s.cluster.likelihood(:,7);
                  s.cluster.likelihood(:,8) = 0.0*s.cluster.likelihood(:,8);
                  s.cluster.likelihood(:,9) = 0.0*s.cluster.likelihood(:,9);
                  s.cluster.likelihood(:,10) = (fft^2.0)*s.cluster.likelihood(:,10);
                  s.cluster.likelihood(:,11) = (fft^2.0)*s.cluster.likelihood(:,11);
                  s.cluster.likelihood(:,12) = (fft^2.0)*s.cluster.likelihood(:,12);
                  s.cluster.likelihood(:,13) = (fft^2.0)*s.cluster.likelihood(:,13);
                end
                save(myFile,'-struct','s');
                fprintf(1, 'Done reading, scaling likelihoods and saving %s\n', myFile);
end
