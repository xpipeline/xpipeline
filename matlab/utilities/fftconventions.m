function fftconventions()
% FFTCONVENTIONS - display X-Pipeline conventions for Fourier Transforms.
%
% FFTCONVENTIONS displays the contents of a text file detailing the
% conventions used in X-Pipeline for handling Fourier transforms.  
% FFTCONVENTIONS has no input or output arguments.
type fftconventions.txt
return
