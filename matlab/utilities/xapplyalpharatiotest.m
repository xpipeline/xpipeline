function pass = xapplyalpharatiotest(likelihood,ratioArray,deltaArray)
% XAPPLYALPHARATIOTEST - apply 'alpha' coherent consistency test on pairs of likelihoods
% 
% usage: 
% 
%   pass = xapplyalpharatiotest(likelihood,ratioArray)
% 
% likelihood   Array of events, one per row.  Each column represents
%              one likelihood measure.
% ratioArray   Square array with the same number of columns as the 
%              likelihood variable. Each nonzero element is a threshold
%              to be applied to a pair of likehoods; see below.
%
% pass         Vector of 0s and 1s, with same number of rows as likelihood.
%              A 1 (0) indicates that the corresponding event passed (did
%              not pass) the likelihood consistency test.
%
% This function applies logical tests to each event (row) in the array  
% likelihood.  A nonzero value of ratioArray(i,j) is treated 
% as a test on the likelihood pair L(:,i), L(:,j) for each event.
% The form of the test depends on the sign of ratioArray(i,j), as 
% follows:
%
% ratioArray > 0:  A positive value of ratioArray(i,j) is treated 
% as a two-sided test.  The requirement to pass is
%
%   abs(2*(L(:,i)-L(:,j)) ./ (L(:,i)+L(:,j)).^alpha) >= abs(ratioArray(i,j)) - 1
%
% That is, events are passed if they lie OUTSIDE of a band centred on the
% L(:,i)=L(:,j) diagonal.   
%
% ratioArray < 0:  A negative value of ratioArray(i,j) is treated 
% as a one-sided test.  The requirement to pass is 
%
%   2*(L(:,i)-L(:,j)) ./ (L(:,i)+L(:,j)).^alpha >= abs(ratioArray(i,j)) - 1
%
% That is, events are passed if they lie sufficiently ABOVE the diagonal,  
% with L(:,i) >~ L(:,j) 
%
% Notes:
% 1) Diagonal elements and zero elements of ratioArray are ignored.
% 2) Be careful!  Setting both ratioArray(i,j) and ratioArray(j,i) 
%    to values <-1 for given i,j will result in a test that cannot 
%    be passed!  This function does not check for redundant or 
%    inconsistent tests.
%
% See also XAPPLYRATIOTEST.
%
% $Id$

% ---- Check number of input arguments.
narginchk(2, 2);

% ---- Determine number of likelihoods.
N = size(likelihood,2);

% ---- Exit gracefully if there are no events to apply the test to.
if N == 0
    pass = [];
    return
end

% ---- Check sizes of inputs.
if ((size(likelihood,2)~=size(ratioArray,2)) | ...
    (size(ratioArray,1)~=size(ratioArray,2)) )
    error(['ratioArray must be square with the same number of columns as likelihood.']);
end

% ---- Zero out diagonal elements of ratioArray.
for irow = 1:N
    ratioArray(irow,irow) = 0;
end

% ---- Note: 0/0 ratios give test (NaN>=number) which always evaluates 
%      to zero.  Turn off divideByZero warning.
warning('Off','MATLAB:divideByZero');

% ---- Apply consistency tests.
alpha = 0.8;
pass = ones(size(likelihood,1),1);
for irow = 1:N
    for icol = 1:N
        if (ratioArray(irow,icol) > 0)
            ratio     = 2*(likelihood(:,irow) - likelihood(:,icol)) ./ (likelihood(:,irow)+likelihood(:,icol)).^alpha;
            threshold = abs(ratioArray(irow,icol)) - 1;
            % ---- ratioArray > 0 -> Two-sided test: keep events that lie OUTSIDE of a band centered on the y=x line.
            pass      = pass .* ( abs(ratio) >= threshold );
        elseif (ratioArray(irow,icol) < 0)
            ratio     = 2*(likelihood(:,irow) - likelihood(:,icol)) ./ (likelihood(:,irow)+likelihood(:,icol)).^alpha;
            threshold = abs(ratioArray(irow,icol)) - 1;
            % ---- ratioArray < 0 -> One-sided test: keep events that lie ABOVE a band centered on the y=x line.
            pass      = pass .* ( ratio >= threshold );
        else 
            % ---- Do nothing for ratioArray == 0.
        end  
    end
end

% ---- Turn divideByZero warning back on.
warning('On','MATLAB:divideByZero');

% ---- Done
return
