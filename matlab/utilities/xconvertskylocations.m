function []=xconvertskylocations(skypos_infile,trigger_time,block_time,skypos_outfile)
% XCONVERTSKYLOCATIONS: Converts sky position file generated for trigger time
% for use at different times. We maintain the ra,dec values and recalculate
% earth based coords for the new time.
%
% usage: xconvertskylocations(skypos_infile,trigger_time,block_time,skypos_outfile)
%
% skypos_infile               String. Name of input sky positions file.
% trigger_time                String. GPS time to which skypos_infile 
%                             corresponds, e.g., GRB trigger time.
% block_time                  String. GPS time for which we want to generate
%                             new skypos file, i.e., skypos_outfile.
% skypos_outfile              String. Name of output sky positions file. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input args
error(nargchk(4, 4, nargin));

% ---- Convert input variables to appropriate type.
trigger_time  = str2num(trigger_time);
block_time    = str2num(block_time);

data = load(skypos_infile);

if size(data,2) ~=4
   error(['sky positions file: ' skypos_infile ' should contain 4 columns. ']);
end

% ---- Read contents of sky_pos_file.
%      These should be theta, phi corresponding to the time of the trigger.
theta_trigger  = data(:,1);
phi_trigger    = data(:,2);
pOmega_trigger = data(:,3);
dOmega_trigger = data(:,4);

% ---- Convert to ra,dec.
[ra_trigger, dec_trigger] = earthtoradec(phi_trigger,theta_trigger,trigger_time);

% ---- Convert back to earth based coords at block_time. 
[phi_block, theta_block] = radectoearth(ra_trigger,dec_trigger,block_time);

% ---- Write sky positions array at block time.
skyPositions(:,1) = theta_block;
skyPositions(:,2) = phi_block;
skyPositions(:,3) = pOmega_trigger;
skyPositions(:,4) = dOmega_trigger;

% ---- Write output file.
dlmwrite(skypos_outfile,skyPositions,'delimiter',' ','precision','%7.5f');

% ---- done.
