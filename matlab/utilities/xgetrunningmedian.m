function [medianA,medianC] = xgetrunningmedian(analysis,cluster,minE,maxE)
% XGETRUNNINGMEDIAN - compute running median of (E,I) for X-Pipe clusters. 
% 
% usage: 
%
%   [medianA,medianC] = xgetrunningmedian(analysis,cluster,minE,maxE)
%
%   analysis    Struct containing the parameters analysis.eNullIndex, ...,
%               analysis.iCrossIndex.
%   cluster     Struct containing X-Pipeline event clusters.
%   minE        Scalar.  Minimum value of coherent "E" energy to fit to.
%   maxE        Scalar.  Maximum value of coherent "E" energy to fit to.
%
%   medianA     Struct containing fit parameters; see below.
%   medianC     Struct containing fit parameters; see below.
%
% xgetrunningmedian is a wrapper to xmedianfit for computing fit
% parameters for (E,I) cluster data.
% 
% xgetrunningmedian fits a curve of the form Y = AX^2+C to cluster data,
% where X = log10(E) and Y = log10(I). That is, this function estimates the
% median value of "I" as a function of "E" firectly from the data, where E
% is a coherent energy and I is the corresponding incoherent component.
% This function will estimate the running median for each of the plus,
% cross, and null statistics if both the "E" and "I" likelihoods are
% defined.  
% In each case the fit is computed over a the range [minE,maxE] of
% E values.  
%
% The output structs medianA, medianC contain the fields "plus", "cross",
% and "null".  Each is a scalar holding the A or C fit value for that
% likelihood type, or [] if the likelihood data is not available in the
% input cluster struct. 
%
% See xmedianfit, xmedianbin.
%
% $Id$

% ---- Checks.
error(nargchk(4, 4, nargin));

% ---- Initialize output to empty arrays.
medianA = [];
medianC = [];

for pairIdx=1:length(analysis.likelihoodPairs)
    % ---- Status report, get name of likelihood (e.g., 'null') from
    %      name field of analysis.likelihoodPairs
    fprintf(1,['Calculating coeffs of quadratic fit to median distribution of '... 
              analysis.likelihoodPairs(pairIdx).name ' likelihoods... ']);

    % ---- Calculate the median fit to the events. 
    [medA medC] = xmedianfit( ...
        cluster.likelihood(:,analysis.likelihoodPairs(pairIdx).energy), ...
        cluster.likelihood(:,analysis.likelihoodPairs(pairIdx).inc), ...
        minE,maxE);

    % ---- Set appropriate field of medianA and medianC structures,
    %      equivalent to medianA.null = medA etc.
    medianA = setfield(medianA,analysis.likelihoodPairs(pairIdx).name,medA);
    medianC = setfield(medianC,analysis.likelihoodPairs(pairIdx).name,medC);

    % ---- Status report.
    fprintf(1,'done! \n')
end

% ---- Done.
return

