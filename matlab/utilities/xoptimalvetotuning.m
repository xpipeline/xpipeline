function [minIdx,totalDiff] = xoptimalvetotuning(ulArray,nameCell, ...
    nullThreshold,plusThreshold,crossThreshold,tuningNameStr, ...
    vetoMethod,requireVeto)
% XOPTIMALVETOTUNING - Choose optimal veto thresholds.
%
% This function takes as input a set of estimated upper limits for one or
% more waveforms and one or more veto threshold combinations.  It returns 
% the index of the threshold combination minimising a sum-squared measure
% of the upper limit over a selected set of waveforms. 
%
% usage: 
%
% [minIdx, SSFEUL] = xoptimalvetotuning(ulArray,nameCell, ...
%     nullThreshold,plusThreshold,crossThreshold,tuningNameStr, ...
%     vetoMethod,requireVeto)
%
% ulArray          Array of upper limits for different waveforms
%                  (columns) and veto choices (rows).
% nameCell         Cell array of strings.  Names of waveforms for which we
%                  have calculated upper limits.
% nullThreshold    Vector of null cut thresholds. 
% plusThreshold    Vector of plus cut thresholds. 
% crossThreshold   Vector of cross cut thresholds. 
% tuningNameStr   Tilde-delimited string of waveform names we wish to 
%                  tune for.  Elements of nameCell not listed here are
%                  ignored in the tuning.  Use {'all'} to include all 
%                  waveforms.
% vetoMethod       String.  one of the veto methods recognised by
%                  xgrbwebpage.py, e.g., 'medianAlphaCut'. 
% requireVeto      Integer. Set to 1 if we require that at least one veto
%                  cut be used. We do this to ensure that glitch events in
%                  the on source will be vetoes even if they are not
%                  present in our off source data. Set to 0 for no such
%                  restriction.
%
% minIdx           Scalar.  Row of ulArray with the minimum sum-squared 
%                  fractional excess upper limit (FEUL). 
% SSFEUL           Sum-squared FEUL.  The FEUL for a given waveform is
%                  defined as 
%
%                      FEUL := (UL - min(UL)) / min(UL)
%
%                  where min(UL) is the minumum over all veto thresholds 
%                  tested. 
%
% We assume that the vetos cuts are applied in the same order and that the
% same frequencies were analysed in the same order by each of the analyses
% we consider.
%
% If two different veto choices give the same SSFEUL, the first of them is
% returned.
%
% $Id$

% ---- Checks.
error(nargchk(8,8,nargin));

% ---- Status report.
fprintf(1,'xoptimalvetotuning: tuning veto cuts to minimise ULs \n');

% ---- Remove NaN ULs.  Some veto cuts give NaN ULs for one or more
%      waveforms.  This causes the FEUL to equal NaN.  If one waveform has
%      all NaN ULs (e.g., because injection weren't loud enough), the whole
%      tuning procedure will break down.  So, we reset NaN ULs to a large
%      but finite value before processing.
NaNULvalue = 1e3;  %-- larger than any injection scales used in production searches
ulArray(isnan(ulArray)) = NaNULvalue;

% ---- Extract waveform names and put in lower case
tuningNameCell = lower(tildedelimstr2numorcell(tuningNameStr));
nameCell = lower(nameCell);

% ---- Column indices (waveforms) in ulArray for which we wish to minimize
%      the upper limit.  
idxULTune = [];
if strcmp(tuningNameCell,'all')
   idxULTune = 1:size(ulArray,2);
else
   % ---- Find index of nameCell corresponding to members of
   %      tuningNameCell.
   for idx = 1:length(tuningNameCell)
      thisIdxULTune = find((strcmp(nameCell,tuningNameCell{idx}))==1);
      idxULTune = [idxULTune; thisIdxULTune];
   end
   % ---- Throw an error if none of the tuningNameCell waveforms match the
   %      requested nameCell. 
   if isempty(idxULTune)
      error(['We did not make injections with the requested name ' tuningNameCell{idx}]);
   end 
end

% ---- Prepare storage for temporary results.
minUL     = [];
ulDiff    = [];
totalDiff = 0;
  
% ---- Loop over the waveforms that are listed in tuningCellStr.
for nameIdx = 1:length(idxULTune)
   fprintf(1,['Looking at waveform:' nameCell{idxULTune(nameIdx)} ' ...']);
   % ---- Find the best UL for this waveform considering all veto cuts.
   minUL(nameIdx) = min(ulArray(:,idxULTune(nameIdx)));
   % ---- Fractional excess of the ULs from each veto cut above the best
   %      values.
   ulDiff(nameIdx).vector = (ulArray(:,idxULTune(nameIdx)) - minUL(nameIdx)) ./ minUL(nameIdx); 
   % ---- Sum the squared fractional excess UL.
   totalDiff = totalDiff + ulDiff(nameIdx).vector.^2;
   fprintf(1,'done!\n');
end

if (requireVeto == 1)
   % ---- Identify veto combinations where no veto was applied.
   if regexp(vetoMethod,'(linearCut|alphaCut)')
      % ---- Specify <=1 since a veto ratio of 1 does not actually veto any
      %      events.
      ignoreIdx = (nullThreshold > -1) & ...
                  (nullThreshold <= 1) & ...
                  (plusThreshold > -1) & ...
                  (plusThreshold <= 1) & ...
                  (crossThreshold > -1) & ...
                  (crossThreshold <= 1);
   elseif regexp(vetoMethod,'median.*Cut.*')
      ignoreIdx = (nullThreshold  == 0) & ...
                  (plusThreshold  == 0) & ...
                  (crossThreshold  == 0);
   elseif regexp(vetoMethod,'alpha.+Cut.*')
      ignoreIdx = [];
   elseif regexp(vetoMethod,'None')
      ignoreIdx = [];
   else
       error(['vetoMethod should be one of the veto methods ' ... 
           'recognised by xgrbwebpage.py.']);
   end  
   % ---- For the veto combinations for which no vetoing was done set
   %      totalDiff = Inf so that we do not choose these veto combinations.
   totalDiff(ignoreIdx) = Inf;
end

% ---- Find veto threshold combination giving lowest sum-squared
%      fractional excess UL.
[minVal,minIdx] = min(totalDiff);

% ---- Status report. 
fprintf(1,'%s \n',['optimal value of null veto is  ' num2str(nullThreshold(minIdx))]);
fprintf(1,'%s \n',['optimal value of plus veto is  ' num2str(plusThreshold(minIdx))]);
fprintf(1,'%s \n',['optimal value of cross veto is ' num2str(crossThreshold(minIdx))]);
for thisIdx = 1:(size(ulArray,2))
   fprintf(1,'%s\n',['For '  nameCell{thisIdx} ' we have UL of ' num2str(ulArray(minIdx,thisIdx)) ]);
end

% ---- Done.
return
