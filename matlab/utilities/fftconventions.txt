  ------------------------------------------------------------------------ 
  Fourier transform conventions: Continuous-time case
  ------------------------------------------------------------------------ 

  Consider a function x(t) where t is continuous on a bounded domain 
  [0,T].  One may define the Fourier transform pair x(t), X_k as
  follows:
 
    X_k := \int_0^T dt x(t) exp{-i2\pi kt/T}
 
    x(t) = 1/T \sum_{k=-\infty}^{\infty} X_k exp{i2\pi kt/T}
 
  These have the usual (desirable) properties
 
    [X_k] = [x(t)]/Hz
    [P] = [ \lim_{T\to\infty} |X_k|^2/T ] \to [x(t)]^2/Hz
 
  where [A] denotes the units of A and where P is the power spectrum.
 
  Parseval's theorem is 
 
  energy:      \int_0^T dt x(t)^2  =    1/T \sum_{k=-\infty}^{\infty} |X_k|^2 
   power:  1/T \int_0^T dt x(t)^2  =  1/T^2 \sum_{k=-\infty}^{\infty} |X_k|^2
 
  Note that 1/T = df (the frequency resolution).
 
  The decompositions of unity are:
 
    \delta(t-t') = 1/T \sum_{k=-\infty}^{\infty} exp{i2\pi k(t-t')/T}
 
    \delta_{k'k} = 1/T \int_0^T exp{i2\pi(k'-k)t/T}
 
 
  ------------------------------------------------------------------------ 
  Fourier transform conventions: Discrete-time case
  ------------------------------------------------------------------------ 
 
  Now assume t is discrete, 
 
    t \to t_j = (j-1) dt = (j-1)/fs
 
  where fs is the sample rate.  Denote by N the number of data points:
 
    T = N dt = N/fs 
 
  We can transcribe the above formulae as 
 
    X_k := 1/fs [\sum_j x_j exp{-i2\pi (k-1)(j-1)/N}]
         = 1/fs fft(x_j)
 
    x_j = 1/T \sum_{k=1}^{N} X_k exp{i2\pi (k-1)(j-1)/N}
        = 1/N \sum_{k=1}^{N} (fs*X_k) exp{i2\pi (k-1)(j-1)/N} 
        = ifft(fs*X_k)
        = 1/N \sum_{k=1}^{N} fft(x_j) exp{i2\pi (k-1)(j-1)/N} 
        = ifft(fft(x_j))
 
  Here (i)fft(x_j) is the Matlab definition of the discrete (inverse)
  Fourier transform.  Note that my X_k is are slightly different from the
  similarly named quantity in the documentation under "help fft";
  specifically, X_k = 1/fs*X(k).
 
  As usual for DFTs, MATLAB returns the Fourier-transformed data 
  with a screwy frequency order:
    vector element:   [ 1  2  ...  N/2-1  N/2    N/2+1            N/2+2   ... N-1  N ]
    frequency (df):   [ 0  1  ...  N/2-2  N/2-1  (N/2 or -N/2)   -N/2+1  ... -2   -1 ]
    e.g.:  F = [ 0:N/2 , -N/2+1:-1 ]'*df;
 
  Parseval's theorem and the decompositions of unity become 
 
  energy:  1/fs \sum_j h_j^2  =  1/T \sum_k |X_k|^2 
   power:  1/(T*fs) \sum_j h_j^2  =  1/T^2 \sum_k |X_k|^2 

    \delta_{k'k} = 1/N \sum_j exp{-i2\pi (k'-k)(j-1)/N}
 
  The discrete power spectrum is a measure of the mean power per unit
  frequency.  From the continous case

    \int df P(f) = 1/T \int dt h(t)^2

  we have in the discrete case  

    P_k = |X_k|^2/T

  where

    1/T \sum_k P_k  =  {total mean power}  

  ------------------------------------------------------------------------ 
