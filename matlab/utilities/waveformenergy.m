function [Egw, Egw_erg] = waveformenergy(r,h,fs)
% WAVEFORMENERGY - Energy in solar masses in a GW burst.
%
% [Egw, Egw_erg] = waveformenergy(r,hrss,f0)
%
% r       Scalar.  Distance to the source [parsec].
% h       One- or two-column matrix. Columns are the GW polarisation
%         timeseries [strain]. 
% fs      Scalar. Sampling rate of the GW timeseries [Hz].  
%
% Egw     Scalar.  Energy in GW [solar masses].
% Egw_erg Scalar.  Energy in GW [erg].
%
% WAVEFORMENERGY computes the energy in the GW burst by computing the
% waveform time derivatives numerically and using the formula for isotropic
% emission, from equations (6) and (9) of https://arxiv.org/pdf/1304.0210.pdf :
%
%   Egw = 4*pi*r^2 * c^3/(16*pi*G) * \int dt (|d(hp)/dt|^2+|d(hc)/dt|^2) 

% ---- Check number of input arguments.
error(nargchk(3, 3, nargin));

% ---- Check sizes of input arguments.
if length(r)>1
    error('Input argmuent r must be a scalar.');
end
if size(h,1)==1 | size(h,2)>2
    error('Input h must be a one- or two-column array.');
end
if length(fs)>1
    error('Input argmuent fs must be a scalar.');
end

% ---- Needed constants.
speedOfLight = 299792458;  %-- m/s
NewtonG = 6.67e-11;        %-- Newton's G (Nm^2/kg^2=m^3/kg/s^2).
secondsPerYear = 365.25*86400;              %-- s
parsec = 3.26*speedOfLight*secondsPerYear;  %-- m
solarMass = 1.988435e30;   %-- kg

% ---- Convert distance from parsecs to SI units.
r = r * parsec;

% ---- Extract the two GW polarisations from h.
if isvector(h) 
    hp = h(:);
    hc = zeros(size(hp));
else
    hp = h(:,1);
    hc = h(:,2);
end

% ---- Compute d(hp)/dt, d(hc)/dt.
dhp = gradient(hp,1/fs);
dhc = gradient(hc,1/fs);

% ---- Energy in GW, in units of solar masses.

Egw_erg = 4*pi*r^2 * speedOfLight^3/(16*pi*NewtonG) * sum(dhp.^2+dhc.^2)/fs;
Egw     = Egw_erg / (solarMass * speedOfLight^2);

% ---- Done.
return
