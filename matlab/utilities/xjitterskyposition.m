function [phi, theta] = xjitterskyposition(phi,theta,sigma,seed)
% XJITTERSKYPOSITION - Change sky positions by random angles.
%
% XJITTERSKYPOSITION takes as input a list of sky positions (points on the
% unit 2-sphere) and outputs a list of the same size in which each input
% position is replaced by a new position that is offset from the original 
% in a random direction.  The angular offset between input and output
% directions is Fisher- or lognormal-distributed. 
%
% usage: 
%
%  [phi, theta] = xjitterskyposition(phi,theta,sigma,seed)
% 
%  phi      Vector of azimuthal sky angles [rad].
%  theta    Vector of polar sky angles [rad].
%  sigma    Scalar or string determining jittering distribution; see below.
%  seed     Scalar. Seed to use for random number generator [OPTIONAL].
%
% When sigma contains a single element the offset angles follow a Fisher 
% distribution, and sigma is interpreted as the angular radius [deg] within 
% which 68% of offset angles are contained. If this radius is larger than
% 360 degrees a uniform distribution is used instead. When sigma is a 3-element 
% tilde-delimited string, a lognormal distribution is used.  The offset
% angles dOmega for the lognormal case are chosen as
%
%   dOmega =  pi/180* (sigma(1) + lognrnd(log(sigma(2)),sigma(3),N));
% When sigma is a 4-element tilde delimited string a fisher statistical
% and core + tail fisher systematic distribution is used
%   sigma=[stat_sigma sys_core_sigma fraction_core sys_tail_sigma]
%
% Procedure: 
% 1) Convert each sky position to Cartesian unit vector X.  
% 2) Select a random direction Y orthogonal to original sky position. 
% 3) Rotate X about Y by a random Fisher/lognormal-distributed angle.
%
% $Id$

% ---- Check and format input.
error(nargchk(3, 4, nargin, 'struct'))
if nargin == 3
  seed = 0;
end
if isstr(sigma)        
    sigma =  tildedelimstr2numorcell(sigma);
end
if length(sigma)~=1 && length(sigma)~=3 && length(sigma)~=4
    error('sigma must contain 1 or 3 elements.');
end
if ~isvector(phi) | ~isvector(theta)
 error('Inputs phi, theta must be vectors.');
end
if length(phi)>1 & length(theta)>1 & length(phi)~=length(theta)
    error('Input vectors phi, theta must be the same size or be scalars.');
elseif length(phi)>1 & length(theta)==1
    theta = theta * ones(size(phi));
elseif length(phi)==1 & length(theta)>1
    phi = phi * ones(size(theta));
end
% ---- Force column vectors
phi = phi(:);
theta = theta(:);

% ---- Number of sky positions.
N = length(phi);

% ---- Set the seed of uniform and gaussian number generators
rand('twister',seed)
randn('state',seed)

if length(sigma) == 1
  if any(sigma > 360)
    warning(['The provided 1-sigma error is greater than 360 degrees, using ' ...
             'a uniform penalty instead of a Fisher penalty']);
    dOmega = acos(2*rand(N,1)-1);
  elseif sigma > 0
    % change sigma from degrees to radians
    sigma = pi/180*sigma;
    % ---- Compute the approximate kappa to get the 68% containment for the 
    %      Fisher distribution.
    kappa = 1/(0.66 * sigma)^2;
    % ---- Verify that the approximation is not too bad.
    p1sigma = 0.68;
    containmentRadius = acos(...
        (kappa+log(1-p1sigma+p1sigma*exp(-2*kappa)))/kappa );
    if abs(containmentRadius-sigma) > 0.3 * sigma
        error(['Discrepency between desired and generated containment ' ...
            'radius is greater than 30%'])
    end
    if abs(containmentRadius-sigma) > 0.1 * sigma
        warning(['Discrepency between desired and generated containment ' ...
            'radius is greater than 10%'])
    end
    % ---- Generate random offsets.
    dOmega = fisher(kappa,N,1,seed);
  elseif sigma == 0
    dOmega = zeros(N,1);
  else
    error(['The value of the sky position error is smaller than zero.'])
  end
elseif length(sigma) == 4
  % format sigma=[stat_sigma sys_core_sigma fraction_core sys_tail_sigma]
  if any(sigma<=0) || sigma(3)>1
    error(['Injection parameters: ' num2str(sigma) ' are not correct. ' ...
           'NB: Zero values for errors are not allowed']);
  end
  % change sigma from degrees to radians
  iAngles = [1 2 4];
  sigma(iAngles) = pi/180*sigma(iAngles);
  sigmaStatSys = sqrt(sigma(1).^2+sigma([2 4]).^2);

  % ---- Compute the approximate kappa to get the 68% containment for the 
  %      Fisher distribution.
  kappa = 1./(0.66 * sigmaStatSys).^2;
  coreFraction = sigma(3);
  % ---- Verify that the approximation is not too bad.
  p1sigma = 0.68;
  containmentRadius = acos(...
      (kappa+log(1-p1sigma+p1sigma*exp(-2*kappa)))./kappa );
  if any(abs(containmentRadius-sigmaStatSys) > 0.3 * sigmaStatSys)
    error(['Discrepency between desired and generated containment ' ...
           'radius is greater than 30%'])
  end
  if any(abs(containmentRadius-sigmaStatSys) > 0.1 * sigmaStatSys)
    warning(['Discrepency between desired and generated containment ' ...
             'radius is greater than 10%'])
  end
  % ---- Generate random offsets.
  dOmegaStatSysCore = fisher(kappa(1),N,1);
  dOmegaStatSysTail = fisher(kappa(2),N,1);
  coreMask = rand(N,1) <= coreFraction;
  dOmega = dOmegaStatSysCore.*coreMask + dOmegaStatSysTail.*(1-coreMask);
elseif length(sigma) == 3
    dOmega =  pi/180* (sigma(1) + lognrnd(log(sigma(2)),sigma(3),N));
else
    error(['Unexpected length of the sigma parameter, sigma is ' num2str(sigma)])
end

% ---- Covert each (phi,theta) into a Cartesian unit row-vector.
X = CartesianPointingVector(phi,theta);

% ---- Random angles we will need later.
Psi = 2*pi*rand(N,1);

% ---- Jitter input sky positions.
for ii = 1:N
    % ---- Choose vector as starting point to construct rotation
    %      axis.  Precise choice is arbitrary except it can't be parallel
    %      to vector to be rotated.  Unit vector is most convenient.
    if (theta(ii)>0.01 & theta(ii)<(pi-0.01))
        Yhat = [0 0 1];
    else
        Yhat = [1 0 0];
    end
    x = X(ii,:);
    Yorthog = Yhat - sum(x.*Yhat)/norm(x)^2 * x;
    % ---- Rotate initial axis by random angle to get random direction
    %      orthogonal to x. 
    Y = RotateVector(Yorthog,x,Psi(ii));
    % ---- Rotate x about Y by jitter angle.
    x = RotateVector(x,Y,dOmega(ii));
    theta(ii) = acos(x(3));
    phi(ii) = atan2(x(2),x(1));
end

% ---- Fix range of phi to [0,2*pi).
k = find(phi<0);
phi(k) = phi(k) + 2*pi;

% ---- Done.
return
