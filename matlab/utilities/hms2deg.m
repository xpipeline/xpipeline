function [ra_deg,dec_deg] = hms2deg(ra_hms,dec_dms)
% HMS2DEG - function to convert right ascension and declination to degrees.
%
% usage:
%
% [ra_deg,dec_deg] = hms2deg(ra_hms,dec_dms)
%
% ra_hms    Vector. Components are right ascension in [hours,minutes,seconds]. 
% dec_dms   Vector. Components are declination in [degrees,minutes_of_arc,seconds_of_arc]. 
%
% ra_deg    Scalar. Right ascension in degrees.
% dec_deg   Scalar. Declination in degrees.
%
% For example, for the Vela pulsar we have 
%   Right Ascension: 08h 35m 20.65525s
%   Declination: −45° 10′ 35.1545″
%
% [ra_deg,dec_deg] = hms2deg([8, 35, 20.65525],[-45 10 35.1545])
%  >> ra_deg =
%      128.8360635416667
%     dec_deg =
%      -45.176431805555552
%
% $Id$

% ---- Check inputs.
narginchk(2,2)
if length(ra_hms)~=3 || length(dec_dms)~=3
    error('Input ra_hms,dec_dms must both be 3-component vectors.')
end

% ---- Convert inputs to degrees.
ra_deg = (ra_hms(1) + ra_hms(2)/60 + ra_hms(3)/3600)*15;
dec_deg = sign(dec_dms(1)) * (abs(dec_dms(1)) + dec_dms(2)/60 + dec_dms(3)/3600);

% ---- Done.
return


