function [ra, dec] = earthtoradec(phi,theta,gps)
% EARTHTORADEC - Convert Earth-based coord to right ascension, declination. 
%
% usage:
%
%     [ra, dec] = earthtoradec(phi,theta,gps)
%
%   phi     Vector of azimuthal angles in Earth-fixed coordinates [rad].
%   theta   Vector of polar angles in Earth-fixed coordinates [rad].
%   gps     Vector of gps times.
%
%   ra      Vector of right ascensions [deg].
%   dec     Vector of declinations [deg].
%
% Earth-based coordinates have the x-axis (phi=0) pointing from the Earth's
% center to the intersection of the prime meridian of Greenwich with the
% equator, the z-axis (theta=0) pointing from the Earth's center to the
% North pole, and the y-xais chosen to form a right-handed coordinate
% system. 
%
% Note that the input vectors ra, dec (and gps if it is not scalar) must be
% the same size.
%
% See also RADECTOEARTH, GPSTOGMST.
%
% Patrick J. Sutton <patrick.sutton@astro.cf.ac.uk>
%
% $Id$

% ---- Test number of arguments
error(nargchk(3,3,nargin));
% ---- Equal length vectors.
if (length(phi)~=length(theta))
    error('Azimuthal and polar vectors must be the same size.')
end
if (length(gps)>1 && length(gps)~=length(phi))
    error('Vector of gps times not the same length as position vectors.')
end

% ---- Convert everything to column vectors.
phi = phi(:);
theta = theta(:);
gps = gps(:);

% ---- Compute sidereal time (sec) at each event time.
gmst = GPSTOGMST(gps);
% ---- Convert to degrees
gmst_deg = gmst / 86400 * 360; 

% ---- Convert to vector if needed.
if length(gmst_deg)==1
    gmst_deg = gmst_deg * ones(size(phi));
end
    
% ---- Convert from radians to degrees.
phi = mod(phi,2*pi);  %-- wrap angles to [0,2*pi)
phi_deg = phi * 180 / pi;
theta_deg = theta * 180 / pi;

% ---- Compute ra,dec of targeted sky location, in degrees.
ra = phi_deg + gmst_deg;
ra = mod(ra,360);  %-- wrap angles to [0,360)
dec = 90 - theta_deg;

% ---- Done 
return
