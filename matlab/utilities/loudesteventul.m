function [ul90p,ul50p,efficiency,flareInjScale,flareDetection,ul95p,analysisEff] = ...
    loudesteventul(threshold,injection,injectionProcessedMask,injectionScale)
% LOUDESTEVENTUL - Compute a loudest event upper limit.
%
% usage:
%
%  [ul90p,ul50p,efficiency,injScale,detection,ul95p] = ...
%      loudesteventul(threshold,injection,mask,scale);
%
%  threshold      Scalar.  Significance threshold (e.g., significance of
%                 loudest on-source event that passes all vetoes).
%  injection      MxN struct array of injection triggers.
%  mask           MxN array.  Value of 1 (0) means corresponding injection 
%                 was (was not) analysed.  Non-analysed injections are ignored.
%  scale          Vector of length N.  Each element is the injection scale 
%                 for all injections in the corresponding column of the 
%                 injection variable.
%
%  ul90p          Scalar.  Injection scale at which efficiency = 0.9.
%  ul50p          Scalar.  Injection scale at which efficiency = 0.5.
%  efficiency     Vector of size Nx1.  Fraction of the M injections at each 
%                 scale that have a cluster above threshold.
%  analysisEff    Vector of size Nx1.  Fraction of the M injections at each 
%                 scale that have a cluster above threshold (DQ not applied).
%  injScale       Vector of length (N*M,1).  Contains injection scale for each 
%                 injection.  Used by FlareFit sigmoid fitting.
%  detection      Vector of length (N*M,1).  Contains 1 (0) if the 
%                 corresponding injection had a trigger louder (quieter) than 
%                 the threshold.  Used by FlareFit sigmoid fitting.
%  ul95p          Scalar.  Injection scale at which efficiency = 0.95.
%
% The interpolation to 50%, 90% and 95% efficiency uses the matlab interp1 function.
% The injScale and detection outputs are intended for passing to the FlareFit 
% sigmoid fitting routine.
%
% $Id$

% ---- Check input arguments.
error(nargchk(4,4,nargin));

% ---- Prepare storage.
nInjections = size(injectionProcessedMask,1);
nInjScales = size(injection,2);
efficiency = zeros(nInjScales,1);
analysisEff = zeros(nInjScales,1);
efficiencyPlusEps = zeros(nInjScales,1);  %-- temp variable.
flareDetection = [];
flareInjScale = [];

% ---- Loop over injection scales and injection number.  At each scale,
%      compute efficiency (fraction of processed injections that are above
%      threshold). 
% ---- This counter runs over all injections at all scales.  It is used for 
%      recording injection results for sending to the FlareFit function.
cnt = 1;
for iInjScale=1:nInjScales
    
    % ---- Zero out count of number of processed injections.
    processed = 0;
   
    % ---- Loop over each injection at this scale. 
    for iN = 1:nInjections

        % ---- Only count this injection if it was actually processed.
        if (1==injectionProcessedMask(iN,iInjScale))
            % ---- Increment count of number of injections processed.
            processed = processed + 1;
            % ---- See if there is one or more surviving triggers ABOVE
            %      threshold.  (Returns zero for empty injection array.)
            if any(injection(iN,iInjScale).significance ... 
                .* injection(iN,iInjScale).pass > threshold)
                % ---- Increment count of detected injections by 1.
                efficiency(iInjScale) = efficiency(iInjScale) + 1;
                flareDetection(cnt) = 1;
                flareInjScale(cnt) = injectionScale(iInjScale); 
                cnt = cnt + 1;
            else
                flareDetection(cnt) = 0;
                flareInjScale(cnt) = injectionScale(iInjScale); 
                cnt = cnt + 1;
            end
            if any(injection(iN,iInjScale).significance.* ...
                   injection(iN,iInjScale).passInjCoinc.* ...
                   injection(iN,iInjScale).passRatio.* ...
                   injection(iN,iInjScale).passFixedRatio > threshold)
                analysisEff(iInjScale) = analysisEff(iInjScale) + 1;
            end
        end
    end

    % ---- Re-normalize efficiency from number count to actual fraction.
    efficiency(iInjScale) = efficiency(iInjScale)/processed;
    analysisEff(iInjScale) = analysisEff(iInjScale)/processed;

    % ---- KLUDGE: If the efficiency is the same at different
    %      injection scales (e.g., saturated at 1) then interpolation
    %      for 90% efficiency may fail.  We therefore add a different
    %      tiny number to each efficiency.  Specifically, we add
    %      1e-10 * injectionScale so the tiny number is a monotonically 
    %      increasing function of amplitude.  This kludged efficiency 
    %      is only used for interpolation and is not output.
    %efficiencyPlusEps(iInjScale) = efficiency(iInjScale) + ...
    %    injectionScale(iInjScale)*1e-10; 

end

% ---- Display a warning message if the interpolation to efficiency=0.9
%      will fail.
if (min(efficiency)>0.9)
    disp(['For every injection scale more than 90% events are above ' ...
        'loudest background, try with lower injection scales']);
    ul90p = NaN;
elseif (max(efficiency)<0.9)
    disp(['For every injection scale less than 90% events are above ' ...
        'loudest background, try with higher injection scales']);
    ul90p = NaN;
else
    % ---- Find largest amplitude such that efficiency < 0.9 for all 
    %      smaller amplitudes.
    [ul90p] = upperbound(injectionScale,efficiency,0.9);
end

% ---- Display a warning message if the interpolation to efficiency=0.95
%      will fail.
if (min(efficiency)>0.95)
    disp(['For every injection scale more than 95% events are above ' ...
        'loudest background, try with lower injection scales']);
    ul95p = NaN;
elseif (max(efficiency)<0.95)
    disp(['For every injection scale less than 95% events are above ' ...
        'loudest background, try with higher injection scales']);
    ul95p = NaN;
else
    % ---- Find largest amplitude such that efficiency < 0.95 for all 
    %      smaller amplitudes.
    [ul95p] = upperbound(injectionScale,efficiency,0.95);
end

% ---- Display a warning message if the interpolation to efficiency=0.5
%      will fail.
% ---- I realise we could nest this in the statements directly above
%      but this way we keep things simple!
if (min(efficiency)>0.5)
    disp(['For every injection scale more than 50% events are above ' ...
        'loudest background, try with lower injection scales']);
    ul50p = NaN;
elseif (max(efficiency)<0.5)
    disp(['For every injection scale less than 50% events are above ' ...
        'loudest background, try with higher injection scales']);
    ul50p = NaN;
else
    % ---- Find largest amplitude such that efficiency < 0.5 for all 
    %      smaller amplitudes.
    [ul50p] = upperbound(injectionScale,efficiency,0.5);
end
    
% ---- Done.
return

