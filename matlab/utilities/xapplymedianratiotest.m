function [pass,medianys] = xapplymedianratiotest(likelihood,ratioArray, ...
    analysis,medianA,medianC,maxE)
% XAPPLYMEDIANRATIOTEST - apply thresholds to pairs of likelihoods.
% 
% usage: 
% 
% [pass,medianys] = xapplyratiotest(likelihood,ratioArray,analysis, ...
%     medianA,medianC,offSource) 
% 
% likelihood   Array of events, one per row.  Each column represents
%              one likelihood measure.
% ratioArray   Square array with the same number of columns as the 
%              likelihood variable. Each nonzero element is a threshold
%              to be applied to a pair of likehoods; see below.
% analysis     Struct containing fields ePlusIndex, ..., iNullIndex.
% medianA      Struct containing fit parameters from xgetrunningmedian.
% medianC      Struct containing fit parameters from xgetrunningmedian.
% maxE         Scalar.  Value of E at which median must go to the diagonal. 
%
% pass         Vector of 0s and 1s, with same number of rows as likelihood.
%              A 1 (0) indicates that the corresponding event passed (did
%              not pass) all of the likelihood ratio thresholds.
% medianys     Vector with the same number of rows as likelihood.  Each
%              element is the estimated median of I at the E value of the
%              event. 
%
% Notes:
% 1) Diagonal elements and zero elements of ratioArray are ignored.
% 2) Be careful!  Setting both ratioArray(i,j) and ratioArray(j,i) 
%    to values <-1 for given i,j will result in a test that cannot 
%    be passed!  This function does not check for redundant or 
%    inconsistent tests.
%
% ADD DESCRIPTION OF TEST.
%
% See xmedianbin, xmedianfit, xgetrunningmedian, xdeviation.
%
% $Id$

% ---- Checks.
error(nargchk(6, 6, nargin));

% ---- Number of likelihoods.
N = size(likelihood,2);
% ---- Exit gracefully if there are no events to apply tests to.
if N == 0
  pass = [];
  mediansys = [];
  return
end

% ---- More checks.
if ([N,N]~=size(ratioArray))
    error('ratioArray must be square with the same number of columns as likelihood.');
end

% ---- Initialize output pass variable.  Default value = 1 (pass).
pass = ones(size(likelihood,1),1);

% ---- Initialise medianys structure.
medianys       = [];
medianys.null  = [];
medianys.plus  = [];
medianys.cross = [];

% ---- Zero out diagonal elements of ratioArray.
for irow = 1:N
    ratioArray(irow,irow) = 0;
end

% ---- Note: 0/0 ratios give test (NaN>=number) which always evaluates 
%      to zero.  Turn off divideByZero warning.
warning('Off','MATLAB:divideByZero');

% ---- Apply consistency tests.
for irow = 1:N
    for icol = 1:N
        if (ratioArray(irow,icol))

            % ---- Don't know yet which likelihood pair we're going to
            %      test.  Initialize flags to 0 (false).
            doingNull  = 0;
            doingPlus  = 0;
            doingCross = 0;

            % ---- Figure out which likelihoods we are dealing with.
            if or(and((irow == analysis.eNullIndex),(icol == analysis.iNullIndex)),...
                  and((icol == analysis.eNullIndex),(irow == analysis.iNullIndex)))

                % ---- Null likelihood.
                indexE = analysis.eNullIndex;
                indexI = analysis.iNullIndex;
                A = medianA.null;
                C = medianC.null;
                doingNull  = 1;

            elseif or(and((irow == analysis.ePlusIndex),(icol == analysis.iPlusIndex)),...
                      and((icol == analysis.ePlusIndex),(irow == analysis.iPlusIndex)))
                  
                % ---- Plus likelihood.
                indexE = analysis.ePlusIndex;
                indexI = analysis.iPlusIndex;
                A = medianA.plus;
                C = medianC.plus;
                doingPlus  = 1;
       
            elseif or(and((irow == analysis.eCrossIndex),(icol == analysis.iCrossIndex)),...
                      and((icol == analysis.eCrossIndex),(irow == analysis.iCrossIndex)))
                  
                % ---- Cross likelihood.
                indexE = analysis.eCrossIndex;
                indexI = analysis.iCrossIndex;
                A = medianA.cross;
                C = medianC.cross;
                doingCross  = 1;

            else

                error('Unknown likelihood type!!!')

            end

            % ---- Calculate running median at E value for each event, and
            %      how far each event is above the median in I vs. E space. 
            [nstd, ys] = xdeviation(likelihood(:,indexE), ...
                likelihood(:,indexI), A, C, maxE);

            % ---- Store running median in output variable.
            if doingNull
                medianys.null = ys;  
            elseif doingPlus
                medianys.plus = ys;  
            elseif doingCross
                medianys.cross = ys;  
            end

            % ---- Threshold on distance from median.
            if (ratioArray(irow,icol) > 0)

                % ---- Two-sided cut.  Pass if abs(nstd) >= threshold. 
                %      Note that we get exactly the same results for an I/E
                %      threshold as for and E/I threshold.
                pass = pass.* (abs(nstd) >= ratioArray(irow,icol));

            elseif (ratioArray(irow,icol) < 0)
                
                % ---- One-sided cut.  Need to check if we want an I>E cut
                %      (pass if above the diagonal) or and E>I cut (pass if
                %      below the diagonal).  
                if any(irow == [analysis.eNullIndex analysis.ePlusIndex analysis.eCrossIndex])
                    % ---- E/I cut.  Pass if nstd <= threshold < 0. 
                    pass = pass.* (nstd <= ratioArray(irow,icol));
                else
                    % ---- I/E cut.  Pass if nstd >= abs(threshold). 
                    pass = pass.* (nstd >= abs(ratioArray(irow,icol)));
                end

            end  

        end
    end
end

% ---- Turn divideByZero warning back on.
warning('On','MATLAB:divideByZero');

% ---- Done
return

