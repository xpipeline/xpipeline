function out = extractclskymap(test,cl)
% EXTRACTCLSKYMAP - trim sky map to fixed confidence level containment
%
% usage:
%
%   out = extractclskymap(test,cl)
% 
%  test     Struct as output by LOADSKYMAPFILE.
%  cl       Scalar. Contaiment confidence level (0<cl<=1) to retain.
%
%  out      Struct. Identical to 'test' except test.map has been replaced with
%           the map restricted to the desired containment level cl.
%
% An error occurs if the specified cl value exceeds the maximum containment
% level of the input test.map.
%
% $Id$

% ---- Sanity check: verify that containment of input sky map equals or exceeds
%      requested containment.
if sum(test.map.prob) < cl
    error(['Requested containment (' num2str(cl) ') exceeds containment of ' ...
        'input sky map (' num2str(sum(test.map.prob)) ').']) 
end

% ---- Find the probability threshold value corresponding to the desired
%      containment level (eg 90%, 50%).
[sprobDensity,I] = sort(test.map.probDensity,'descend');
cprob = cumsum(test.map.prob(I));
idx_end = find(cprob>=cl,1);
probDensity_thresh = sprobDensity(idx_end);
idx = I(1:idx_end);

% ---- Construct a copy of the map struct containing only pixels inside this CL
%      contour. 
clmap = struct;
names = fieldnames(test.map);
for ii=1:length(names)
    x = getfield(test.map,names{ii});
    clmap = setfield(clmap,names{ii},x(idx));
end

% ---- Copy input map struct to output, replacing full CL map with restricted
%      map.
out = test;
out.map = clmap;

% ---- Done.
return

