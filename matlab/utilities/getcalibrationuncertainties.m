function [amplitude, phase, time] = getcalibrationuncertainties(gps_s,detectorListStr,frameTypeListStr)
% GETCALIBRATIONUNCERTAINTIES - return calibration uncertainties for detectors
%
% GETCALIBRATIONUNCERTAINTIES returns known the calibration uncertainties of the
% requested detectors for specified channels (calibration versions) and GPS times. 
%
% usage:
%
%  uncert = getcalibrationuncertainties(gpsTime,detectors,frameTypes)
%
%  gpsTime       Scalar. Return calibration uncertainties at this time [s].
%  detectors     String. Tilde separated list of detectors in the network, 
%                e.g., 'H1~L1~V1'  
%  frameTypes    String. Tilde separated list of frame types for the detectors, 
%                e.g.,  'H1_HOFT_C01,L1_HOFT_C01,V1Online'
%
%  amplitude     Struct. Contains the following fields, each of which contains a
%                vector with one element per detector.
%                  mu    - systematic offset: mean fractional error in the amplitude 
%                  sigma - one-sigma statistical uncertainty in the amplitude
%  phase         Struct. Contains the following fields, each of which contains a
%                vector with one element per detector.
%                  mu    - systematic offset: mean error in the phase [radians]
%                  sigma - one-sigma statistical uncertainty in the phase [radians]
%  time          Struct. Contains the following fields, each of which contains a
%                vector with one element per detector.
%                  mu    - systematic offset: mean error in the timing [sec]
%                  sigma - one-sigma statistical uncertainty in the timing [sec]
%
% Systematic offsets are interpreted as follows:
%   amplitude.mu : the true strain is h -> (1+amplitude.mu) * h; e.g.
%                  amplitude.mu = 1.05 means the true noise level is 5% higher. 
%   phase.mu     : the true phase of a given sample in the frequency domain is 
%                  phi -> phi + phase.mu
%   time.mu      : the true time of a given data sample is t -> t + time.mu.
% See also XAPPLYCALIBRATIONCORRECTIONS.
%
% See the code for details on the source of calibration uncertainties for each
% detector and epoch. If the detector or frametype is not recognised, or if the
% calibration uncertainties are not known at the specified gpsTime, the function
% returns these default uncertainties:
%                 amplitude.mu    = 0
%                 amplitude.sigma = 0.1    %-- 10%
%                 phase.mu        = 0 
%                 phase.sigma     = pi/18  %-- 10 deg
%                 time.mu         = 0
%                 time.sigma      = 0
%
% $Id$

% ---- Check input.
error(nargchk(3, 3, nargin, 'struct'))
if ~isscalar(gps_s)
    error('Input gpsTime must be a scalar.');
end
if ~ischar(detectorListStr)
    error('Input detectorList must be a string.');
end
if ~ischar(frameTypeListStr)
    error('Input frameTypeList must be a string.');
end

% ---- Convert detectorListStr into a cell array.
detectorList = tildedelimstr2numorcell(detectorListStr);

% ---- Convert frameTypeListStr into a cell array.
frameTypeList = tildedelimstr2numorcell(frameTypeListStr);

% ---- Dimensions.
nDetector = length(detectorList);

% ---- Prepare storage for uncertainties.
amplitude.mu    = zeros(1,nDetector);
amplitude.sigma = zeros(1,nDetector);
phase.mu        = zeros(1,nDetector);
phase.sigma     = zeros(1,nDetector); 
time.mu         = zeros(1,nDetector);
time.sigma      = zeros(1,nDetector);

% ---- Start and end times of first-generation LIGO/Virgo runs.
tStartS4   = 793130413; %-- ( )
tEndS4     = 795679213; %-- ( )
%
tStartS5   = 815011213; %-- (Nov 03 2005 00:00:00 UTC); % redefined to include GRB 051103 (815045155.190)
tStartS5official = 815155213; %-- (Nov 04 2005 16:00:00 UTC) Offical defn of tStartS5.
tStartVSR1 = 863557214; %-- (May 18 2007 21:00:00 UTC)
tEndVSR1   = 875250014; %-- (Oct 01 2007 05:00:00 UTC) 
%
tStartA5   = 875232014; %-- (Oct 01 2007 00:00:00 UTC)
tVMidA5    = 888364814; %-- (Mar 01 2008 00:00:00 UTC)
tEndA5     = 927849614; %-- (Jun 01 2009 00:00:00 UTC) 
%
tStartS6   = 931035615; %-- (Jul 07 2009 21:00:00 UTC)
tEndS6     = 971654415; %-- (Oct 21 2010 00:00:00 UTC) 
tStartVSR2onlineEpoch1 = tStartS6;
tStartVSR2onlineEpoch2 = 936637215; % -- (Sep 10 2009 17:00:00 UTC)
%
tStartVSR2 = tStartS6;  %-- (Jul 07 2009 21:00:00 UTC)
tEndVSR2   = 947023215; %-- (Jan 08 2010 22:00:00 UTC)
%
tStartVSR3 = 965599215; %-- (Aug 11 2010 22:00:00 UTC)
tEndVSR3   = 971654415; %-- (Oct 21 2010 00:00:00 UTC) 
%
tStartS6eVSR4 = 991170015; %-- (Jun 03 2011 21:00:00 UTC)
tEndS6eVSR4 = 999234015; %-- (Sep 05 2011 05:00:00 UTC)
%
tStartA6 = 999234015; %-- (Sep 05 2011 05:00:00 UTC)
tEndA6 = 1104105616; %-- (Jan 01 2015 00:00:00 UTC)

% ---- O1 GRB analysis; includes ER8B
tStartO1 = 1126051217; % -- (Sep 12 2015 00:00:00 UTC)
tEndO1 = 1136649617; % -- (January 12 2016 16:00:00 UTC)

% ---- O2 G298048 epoch
tStartG298048 = 1187000000; % -- (Thu Aug 17 10:13:02 GMT 2017)
tEndG298048   = 1187020000; % -- (Thu Aug 17 15:46:22 GMT 2017)

% ---- O2 Offline GRB analysis
tStartO2 = 1164556817; % -- (Nov 30, 2016 16:00:00 UTC)
tEndO2 = 1187733618; % -- (Aug 25, 2017 22:00:00 UTC)

% ---- O3b Offline GRB analysis. From https://wiki.ligo.org/LSC/JRPComm/ObsRun3
tStartO3b = 1256655618; % -- (Fri Nov 01 15:00:00 GMT 2019)
tEndO3b   = 1269363618; % -- (Fri Mar 27 17:00:00 GMT 2020)

% ---- O3 GEO-KAGRA analysis A
tStartO3GKa = 1270281618; %-- (April 07, 2020 08:00:00 UTC)
tEndO3GKa   = 1271026848; %-- (April 21, 2020 00:00:00 UTC)

% ---- O3 GEO-KAGRA analysis B
tStartO3GKb = 1271026848; %-- (April 07, 2020 08:00:00 UTC)
tEndO3GKb   = 1271462418; %-- (April 21, 2020 00:00:00 UTC)

% ---- Determine science run using gps_s of first injection.
if and((gps_s(1) >= tStartS4),gps_s(1)<=tEndS4)

    disp('Using calibration uncertainties from S4')
	
    % ---- We hardcode calibration uncertainties to their S4 values
    %      for H1, H2 and L1.  Here is the source:
    %
    %      H1, H2 & L1: The uncertainties are those published in:
    %      Calibration of the LIGO detectors for S4
    %      LIGO-T050262-01-D
    %      www.ligo.caltech.edu/docs/T/T050262-01.pdf 
    %
    %                   Magnitude       Phase
    %             IFO   Error (%)       Error (deg)
    %             H1       8              3
    %             H2       8              4
    %             L1       5              2
    %
    % We model the amplitude, phase, and time-delay errors as Gaussian
    % distributions.  We assume they are independent for each detector and each
    % GW injection; that is, we make independent draws from the Gaussian
    % distributions for each injection.

    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'h1'
                % ---- H1.
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.08;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 3 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
            case 'h2'
                % ---- H2.
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.08;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 4 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
            case 'l1'
                % ---- L1.
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.05;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 2 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
            otherwise
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.10;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
        end
    end

elseif and((gps_s(1) >= tStartS5),gps_s(1)<=tEndVSR1)

    disp('Using calibration uncertainties from S5/VSR1')

    % ---- For now, hardcode calibration uncertainties to their S5/VSR1 
    %      values (for LIGO and Virgo) or to the "10% and 10 deg" nominal 
    %      conservative numbers for any other detector.  Here are the sources:
    %
    %      LIGO: The uncertainties used are those for V3 h(t) with the known
    %      V3->V4 DC amplitude and time delay corrections applied, as
    %      described here:   
    %      https://wiki.ligo.org/Bursts/XPipelineS5CalibrationUncertainties
    %
    %      Virgo: The uncertainties are those for HrecV2 or HrecV3 h(t), taken 
    %      from Virgo note VIR-078A-08.  From the summary:
    %        "The HrecV2 or HrecV3 h(t) reconstruction for VSR1 is valid
    %        from 10Hz up to the Nyquist frequency of the channel used,
    %        i.e. up to 2048, 8192 or 10000Hz. For the beginning of the
    %        run (up to GPS=865178000, June 6th) the lower bound of the
    %        validity range is raised from 10 to 30Hz (see section 5.11). 
    %        In this validity range, the systematic error on the h(t)
    %        amplitude is 6% (see section 5.11).  The systematic error on
    %        the h(t) absolute timing  is 70 mrad below 1.9kHz and 6�s
    %        above. (see section 5.10)."
    %
    % We model the amplitude, phase, and time-delay errors as Gaussian
    % distributions.  We assume they are independent for each detector and each
    % GW injection; that is, we make independent draws from the Gaussian
    % distributions for each injection.

    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'h1'
                % ---- H1 cal v03-v04 errors: 12.0%/9.6deg/0us.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.120;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 9.6 * pi/180; 
                time.mu(jj) = 0e-6; 
                time.sigma(jj) = 0; 
            case 'h2'
                % ---- H2 cal v03-v04 errors: 14.1%/7.3deg/0us.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.141; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 7.3 * pi/180; 
                time.mu(jj) = 0e-6; 
                time.sigma(jj) = 0; 
            case 'l1'
                % ---- L1 cal v03-v04 errors: 14.0%/5.4deg/0us.  For time of
                %      GRB 051103 reset amplitude uncertainty to 25%.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.140; 
                if gps_s(1) < tStartS5official
                    amplitude.sigma(jj) = 0.250;                   
                end
                phase.mu(jj) = 0;
                phase.sigma(jj) = 5.4 * pi/180; 
                time.mu(jj) = 0e-6; 
                time.sigma(jj) = 0; 
            case 'v1'
                % ---- V1 cal errors: 6%/70mrad/0us. 
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.06; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 0.07;  %-- 4.0 deg
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
            otherwise 
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.10; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
        end
    end

elseif and((gps_s(1) >= tStartA5),gps_s(1)<=tEndA5)

    disp('Using calibration uncertainties from A5')

    % ---- We hardcode calibration uncertainties to their A5 values
    %      for H2 and V1, and the "10% and 10 deg" nominal 
    %      conservative numbers for G1.  Here are the sources:
    %      
    %      V2: The uncertainties are those forwarded by Loic Rolland.
    %      Mag < 30% for Feb 7, Mag < 40% for Mar 30.  Phase 300 mrad/140 usec
    %
    %      H2: The uncertainties are those published by Mike Landry: 
    %      http://www.ligo-wa.caltech.edu/~michael.landry/calibration/A5/rev/a5rev.html
    %
    %                	Magnitude	Phase 
    %             IFO 	Error (%)	Error (deg)
    %             H2 	  <23             <8
    %             V1      <40             <18
    %             G1      <10             <10
    %
    %      The A5 calibration for H2 used the S5 V4 calibration and 
    %      does not require any time delay correction.  The time delay uncertainty
    %      is 12usec: this includes the 9usec error of S5, plus an additional 
    %      2.75us systematic in A5.  See email from M Landry 6/21/2010.
    %
    %      G1: see email from J Leong, Oct 8 2010:
    %      "I will have to dig through some old emails about what we told Peter Kalmus but the 
    %      quick answer is that for absolute magnitude our h(t) frames are 20% off what they 
    %      should be.  To correct for this, multiply all data by 1.2.  This should make our 
    %      sensitivity worse.  As for error bars, I think our last agreement was 10% in magnitude 
    %      and 10deg in phase up to 3kHz or so."

    % We model the amplitude, phase, and time-delay errors as Gaussian
    % distributions.  We assume they are independent for each detector and each
    % GW injection; that is, we make independent draws from the Gaussian
    % distributions for each injection.
    % 
    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'h2'
                % ---- H2 cal errors: 23%/8deg/200us.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.23; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 8 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 12e-6; 
            case 'v1'
                if(gps_s(1) <= tVMidA5)       % Before Mar 1 2008
                    % ---- V1 cal errors: 30%/300mrad/140us. 
                    amplitude.mu(jj) = 0; 
                    amplitude.sigma(jj) = 0.3; 
                    phase.mu(jj) = 0;
                    phase.sigma(jj) = 0.3;  %-- 17.2 deg
                    time.mu(jj) = 0; 
                    time.sigma(jj) = 140e-6; 
                elseif (gps_s(1) >= tVMidA5)   % After Mar 1 2008
                    % ---- V1 cal errors: 40%/300mrad/140us. 
                    amplitude.mu(jj) = 0; 
                    amplitude.sigma(jj) = 0.4; 
                    phase.mu(jj) = 0;
                    phase.sigma(jj) = 0.3;  %-- 17.2 deg
                    time.mu(jj) = 0; 
                    time.sigma(jj) = 140e-6; 
                end
            otherwise 
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.10; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
        end
    end

elseif and((gps_s(1) >= tStartS6),gps_s(1)<=tEndS6)

    disp('Using calibration uncertainties from S6/VSR2')
    % ---- See <https://www.lsc-group.phys.uwm.edu/twiki/bin/view/DAC/S6VSR2Calibration>
    %      and email [S6 calibration uncertainties] by Gareth Jones, Aug 25th 2009.  
    % ---- For V1 also see Loic Rolland's reply to my email and 
    %      <http://wwwcascina.virgo.infn.it/DataAnalysis/Calibration/Reconstruction/Runs/VSR2/index.html>
    %      For V1 I have taken the larger uncertainties for each error from
    %      these two webpages.
    % ---- For VSR2 the V2 callibration the errors are taken from VIR-0190A-10;
    %      this is a preliminary budget.
    %      The final HrecV3 and HrecOnline errors are given in VIR-0340A-10.
    % ---- For VSR3 the preliminary HrecOnline and HrecV2 errors are
    %      given in <https://wwwcascina.virgo.infn.it/DataAnalysis/Calibration/Reconstruction/Runs/VSR3/index.html>
    %      Updated calibration page information for LIGO:
    %      <https://www.lsc-group.phys.uwm.edu/twiki/bin/view/DAC/S6Calibration>
    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'h1'
             switch frameTypeList{jj}
              case {'H1_LDAS_C00_L2','H1_LDAS_C02_L2'}
               % ---- H1 cal v00/v02 errors: 20%/10deg/20us.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.20;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 20e-6; 
              otherwise
               error(['Unrecognized frame type: ' ...
                      frameTypeList{jj}])
             end
            case 'h2'
                error('H2 not run during S6')
            case 'l1'
             switch frameTypeList{jj}
              case 'L1_LDAS_C00_L2'
                % ---- L1 cal v00 errors: 20%/10deg/100us.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.20; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 100e-6; 
              case 'L1_LDAS_C02_L2'
                % ---- H1 cal v00/v02 errors: 20%/10deg/20us.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.20;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 20e-6; 
              otherwise
               error(['Unrecognized frame type: ' ...
                      frameTypeList{jj}])
             end
            case 'v1'
             switch frameTypeList{jj}
               case 'HrecOnline'
                if  gps_s(1) < tStartVSR2onlineEpoch2
                  % ---- V1 cal errors: 15%/9deg/70us. 
                  amplitude.mu(jj) = 0; 
                  amplitude.sigma(jj) = 0.15; 
                  phase.mu(jj) = 0;
                  phase.sigma(jj) = 9 * pi/180;
                  time.mu(jj) = 0; 
                  time.sigma(jj) = 70e-6; 
                elseif (tStartVSR2onlineEpoch2 <= gps_s(1) && ...
                        gps_s(1) <= tEndVSR2)
                  % ---- V1 cal errors: 10%/9deg/20us
                  amplitude.mu(jj) = 0; 
                  amplitude.sigma(jj) = 0.10; 
                  phase.mu(jj) = 0;
                  phase.sigma(jj) = 9 * pi/180;
                  time.mu(jj) = 0; 
                  time.sigma(jj) = 20e-6; 
                elseif (gps_s(1) >= tStartVSR3 && gps_s(1) <= tEndVSR3)
                  % ---- V1 cal errors: 15%/9deg/10us
                  amplitude.mu(jj) = 0; 
                  amplitude.sigma(jj) = 0.15; 
                  phase.mu(jj) = 0;
                  phase.sigma(jj) = 150e-3;
                  time.mu(jj) = 0; 
                  time.sigma(jj) = 10e-6; 
                end
              case 'HrecV2'
               if gps_s(1) >= tStartVSR2 && gps_s(1) <= tEndVSR2
                 % ---- V1 cal errors: 7%/3.5deg/12us
                 amplitude.mu(jj) = 0; 
                 amplitude.sigma(jj) = 0.07; 
                 phase.mu(jj) = 0;
                 phase.sigma(jj) = 60e-3;
                 time.mu(jj) = 0; 
                 time.sigma(jj) = 12e-6;                
               elseif (gps_s(1) >= tStartVSR3 && gps_s(1) <= tEndVSR3)
                 % ---- V1 cal errors: 7%/3deg/8us
                 amplitude.mu(jj) = 0; 
                 amplitude.sigma(jj) = 0.07; 
                 phase.mu(jj) = 0;
                 phase.sigma(jj) = 50e-3;
                 time.mu(jj) = 0; 
                 time.sigma(jj) = 8e-6;                
               end
              case 'HrecV3'
               % ---- V1 cal errors: 5%/2.5deg/8us
               amplitude.mu(jj) = 0; 
               amplitude.sigma(jj) = 0.055; 
               phase.mu(jj) = 0;
               phase.sigma(jj) = 50e-3;
               time.mu(jj) = 0; 
               time.sigma(jj) = 8e-6;                
              otherwise
               error(['Unrecognized frame type: ' ...
                      frameTypeList{jj}])
             end
            otherwise 
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.10; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
        end
    end

elseif and((gps_s(1) >= tStartS6eVSR4),gps_s(1)<=tEndS6eVSR4)

    disp('Using calibration uncertainties from S6eVSR4')

    % ---- We hardcode calibration uncertainties to their S6eVSR4 values
    %      for V1, and the "10% and 10 deg" nominal 
    %      conservative numbers for G1.  Here are the sources:
    %      
    %      V1: The uncertainties are those forwarded by Loic Rolland.
    %      https://wwwcascina.virgo.infn.it/DataAnalysis/Calibration/Reconstruction/Runs/VSR4/index.html
    %      Details given in note number VIR-0704A-11 
    %
    %      G1: see GEO-HF logbook page 1246 for absolute magnitude.
    %      As for error bars, I think our last agreement was 10% in magnitude 
    %      and 10deg in phase up to 3kHz or so."
    %
    % We model the amplitude, phase, and time-delay errors as Gaussian
    % distributions.  We assume they are independent for each detector and each
    % GW injection; that is, we make independent draws from the Gaussian
    % distributions for each injection.

    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'v1'
                % ---- V1 cal errors: 7.5%/120mrad/8us. 
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.075; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 0.120;
                time.mu(jj) = 0; 
                time.sigma(jj) = 8e-6; 
            otherwise 
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.10; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
        end
    end

elseif and((gps_s(1) >= tStartA6),gps_s(1)<=tEndA6)

    disp('Using calibration uncertainties from A6')

    % ---- We hardcode calibration uncertainties to their A6 values
    %      conservative numbers for G1.  Here are the sources:
    %      
    %      G1: see GEO-HF logbook page 1246 for absolute magnitude.
    %      As for error bars, I think our last agreement was 10% in magnitude 
    %      and 10deg in phase up to 3kHz or so."
    %
    % We model the amplitude, phase, and time-delay errors as Gaussian
    % distributions.  We assume they are independent for each detector and each
    % GW injection; that is, we make independent draws from the Gaussian
    % distributions for each injection.

    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        % ---- Detector not recognized: use "10% and 10deg" for testing.
        amplitude.mu(jj) = 0; 
        amplitude.sigma(jj) = 0.10; 
        phase.mu(jj) = 0;
        phase.sigma(jj) = 10 * pi/180; 
        time.mu(jj) = 0; 
        time.sigma(jj) = 0; 
    end
    
elseif and((gps_s(1) >= tStartO1),gps_s(1)<=tEndO1)

    disp('Using calibration uncertainties from O1 (including ER8B) -- for C01 -- the very conservative numbers: 20% amplitude and 20deg phase')

    % ---- We hardcode calibration uncertainties to their O1 values
    %      Here are the sources:
    %      
    %      As for error bars specifically for C01 frames, the most conservative estimate is 20% in magnitude 
    %      and 20deg in phase up to 1kHz or so."
    %      Calibration page: https://wiki.ligo.org/Calibration/WebHome (reference added
    %      on 19 January 2016)
    %
    % We model the amplitude, phase, and time-delay errors as Gaussian
    % distributions.  We assume they are independent for each detector and each
    % GW injection; that is, we make independent draws from the Gaussian
    % distributions for each injection.

    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        % ---- Detector not recognized: use "10% and 10deg" for testing.
        amplitude.mu(jj) = 0;
        % ---- 10% amplitude
        amplitude.sigma(jj) = 0.20;
        phase.mu(jj) = 0;
        % ---- 10deg phase
        phase.sigma(jj) = 20 * pi/180;
        time.mu(jj) = 0;
        time.sigma(jj) = 0;
    end

elseif and((gps_s(1) >= tStartG298048),gps_s(1)<=tEndG298048)

    disp('Using calibration uncertainties from O2 epoch of G298048.');
    
    % ---- We hardcode calibration uncertainties to their O2 values
    %      at the time of G298048. For LIGO the numbers are taken from 
    %      https://alog.ligo-la.caltech.edu/EVNT/index.php?callRep=11928
    %           20-1024 Hz Max Mag [%]      20-1024 Hz Max Phase [deg] 
    %           ----------------------      --------------------------0
    %        H: +5.7121787e+00              +2.8512131e+00     
    %        L: +3.8143011e+00              +1.9320803e+00
    %      For Virgo the numbers are taken from ths email:
    %      https://sympa.ligo.org/wws/arc/cbc/2017-08/msg01341.html
    %      "For V1 we recommend using 10 % in amplitude and 100 mrad +150us
    %      in phase." 
    %      These should be accurate for the only versions of calibrated
    %      frames available as of the commit date (24 Aug 2017). Also the
    %      uncertainties could be made slightly more precise by modelling
    %      the offset mu and uncertainty sigma separately.
    
    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'h1'
                % ---- H1 cal errors: 5.7121787%/2.8512131deg/0us. 
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.057121787; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 2.8512131 * pi/180;
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
            case 'l1'
                % ---- L1 cal errors: 3.8143011%/120mrad/8us. 
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.038143011; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 1.9320803 * pi/180;
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
            case 'v1'
                % ---- V1 cal errors: 7.5%/120mrad/8us. 
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.10; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 0.100;
                time.mu(jj) = 0; 
                time.sigma(jj) = 150e-6; 
            otherwise 
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0; 
                amplitude.sigma(jj) = 0.10; 
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180; 
                time.mu(jj) = 0; 
                time.sigma(jj) = 0; 
        end
    end

elseif and((gps_s(1) >= tStartO2),gps_s(1)<=tEndO2)
    % ---- We hardcode calibration uncertainties to their O2 values
    %      taken from the pdf in G1800319:
    %
    %                  H1 mag/pha       L1 mag/pha
    %                 ------------    ------------
    %    All of O2:   2.6%/2.36deg    3.85%/2.15deg
    %
    %     For Virgo, Michal's email and  the reference VIR-0014A-18 (page 19) state:
    %     5.1% in magnitude, 40mrad in phase and 20us in time
    %
    % ----
    disp('Using calibration uncertainties O2');
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'h1'
                % ---- H1 cal errors: 2.6%/2.36deg. 
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.026;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 2.36 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
            case 'l1'
                % ---- L1 cal errors: 3.85%/2.15deg. 
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.0385;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 2.15 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
            case 'v1'
                % ---- V1 cal errors: 5.1%/40mrad/20us. 
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.051;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 0.04;
                time.mu(jj) = 0;
                time.sigma(jj) = 20e-6;
            otherwise
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.10;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
        end
    end
    
elseif and((gps_s(1) >= tStartO3b),gps_s(1)<=tEndO3b)

    % ---- We use the calibration uncertainties as specified at
    %      https://git.ligo.org/publications/O3/o3b-grb/-/wikis/Data-and-Calibration
    %      
    %      H1 @ 20-1024 Hz : 4.6% amplitude, 2.3 degrees phase
    %      L1 @ 20-1024 Hz : 8.3% amplitude, 5.2 degrees phase
    %      V1 :              5% amplitude, 35 mrad phase, 10 us delay
    %      By default this function will treat the V1 phase and delay
    %      errors as independent, as recommended by the Virgo calibration
    %      team. However, we are not able to account for the higher V1
    %      calibration uncertainties in the 46 Hz to 51 Hz range.
    %
    disp('Using calibration uncertainties for O3b');
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'h1'
                % ---- L1 cal errors: 4.6%/2.3deg. 
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.046;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 2.3 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
            case 'l1'
                % ---- L1 cal errors: 8.3%/5.2deg. 
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.083;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 5.2 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
            case 'v1'
                % ---- V1 cal errors: 5%/35mrad/10us. 
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.05;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 0.035;
                time.mu(jj) = 0;
                time.sigma(jj) = 10e-6;
            otherwise
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj) = 0;
                amplitude.sigma(jj) = 0.10;
                phase.mu(jj) = 0;
                phase.sigma(jj) = 10 * pi/180;
                time.mu(jj) = 0;
                time.sigma(jj) = 0;
        end
    end
    
elseif and((gps_s(1) >= tStartO3GKa), gps_s(1) <= tEndO3GKb)

    % ---- We hardcode calibration uncertainties to their O3 GEO-KAGRA
    %      values for K1 and G1.
    %
    %      For G1 we use the default 10% amplitude / 10 degrees phase. This was
    %      confirmed as reasonable by the GEO team, here:
    %      https://sympa.ligo.org/wws/arc/cbc/2021-01/msg00065.html
    %
    %      For K1 we use the largest amplitude and phase uncertainties in the 
    %      [20,4000] Hz band as determined by inspection of the files here:
    %      https://gwdoc.icrr.u-tokyo.ac.jp/cgi-bin/private/DocDB/ShowDocument?docid=12199 

    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        switch lower(detectorList{jj}(1:2))
            case 'k1'
                % ---- KAGRA
                % ---- K1 calibration uncertainties changed halfway through run.
                if gps_s(1) <= tEndO3GKa
                    disp('Using calibration uncertainties from O3 GEO-KAGRA a [2021 Apr 07-16].')
                    amplitude.mu(jj)    = 0;
                    amplitude.sigma(jj) = 0.1819;  % at 56 Hz
                    phase.mu(jj)        = 0;
                    phase.sigma(jj)     = 12.21 * pi/180;  % at 62 Hz
                    time.mu(jj)         = 0;
                    time.sigma(jj)      = 0;
                else
                    disp('Using calibration uncertainties from O3 GEO-KAGRA b [2021 Apr 16-21].')
                    amplitude.mu(jj)    = 0;
                    amplitude.sigma(jj) = 0.1609;  % at 56 Hz
                    phase.mu(jj)        = 0;
                    phase.sigma(jj)     = 10.40 * pi/180;  % at 62 Hz
                    time.mu(jj)         = 0;
                    time.sigma(jj)      = 0;
                end
            otherwise
                % -- Detector not recognized: use "10% and 10deg" for testing.
                amplitude.mu(jj)    = 0;
                amplitude.sigma(jj) = 0.10;
                phase.mu(jj)        = 0;
                phase.sigma(jj)     = 10 * pi/180;
                time.mu(jj)         = 0;
                time.sigma(jj)      = 0;
        end
    end

else
    
    warning(['Science run corresponding to GPS time of first injection (' ...
      num2str(gps_s(1)) ...
      ') not recognised. Assuming 10% amplitude and 10deg phase errors.'])
    % ---- Assign uncertainties for each detector.
    for jj = 1:nDetector
        % ---- Epoch not recognized: use "10% and 10deg" for testing.
        amplitude.mu(jj) = 0; 
        amplitude.sigma(jj) = 0.10; 
        phase.mu(jj) = 0;
        phase.sigma(jj) = 10 * pi/180; 
        time.mu(jj) = 0; 
        time.sigma(jj) = 0; 
    end
    
end % -- If S5, S6 etc.

% ---- Done.
return

