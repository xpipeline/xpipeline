function [numCuts] = xplotmedianratiocuts(ratioArray,medianE,medianI, ...
    analysis,likelihood,pairIdx,fout,writeText)
% XPLOTMEDIANRATIOCUTS - plot thresholds for median-racking test.
%
% usage:
%  
% [numCuts] = xplotmedianratiocuts(ratioArray,medianE,medianI, ...
%     analysis,likelihood,pairIdx,fout,writeText)
%  
% ratioArray    Square array with the same number of columns as the 
%               likelihood variable. Each nonzero element is a threshold
%               to be applied to the ratio of two likehoods; see below.
% medianys      Vector. Median value of each of the three incoherent likelihoods
%               for different values of the energy (coherent) likelihood.
% analysis      Structure. Contains parameters of data analysis, see 
%               xmakegrbwebpage
% likelihood
% pairIdx       Integer. Identifies which likelihoods in 
%               analysis.likelihoodType we will plot
% fout          File identifier of file to which we will direct text output
% writeText     Integer. Set to 1 to write text on veto ratios to fout 
%  
% numCuts       Integer. Number of ratio cut lines plotted, useful when
%               creating legends
%
% $Id$

% ---- Checks.
error(nargchk(8,8,nargin));

% ---- initialise number of cuts plotted to zero
numCuts = 0;

% ---- Status report, get name of likelihood (e.g., 'null') from
%      name field of analysis.likelihoodPairs
fprintf(1,['Plotting fit to median distribution of '...
    analysis.likelihoodPairs(pairIdx).name ' likelihoods... ']);

medE = getfield(medianE,analysis.likelihoodPairs(pairIdx).name);
medI = getfield(medianI,analysis.likelihoodPairs(pairIdx).name);

eLikeIdx = analysis.likelihoodPairs(pairIdx).energy;
iLikeIdx = analysis.likelihoodPairs(pairIdx).inc;

% ---- Do the relevant plotting.
if (ratioArray(eLikeIdx,iLikeIdx) > 0)   
   % E/I +ve 
   % ---- ratioArray is +ve we do a two sided test
   numCuts = 2; 

   % plot line    : 
   ra = ratioArray(eLikeIdx,iLikeIdx);
   Ipos = 0.5 * (2*medI + ra^2 + ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Ineg = 0.5 * (2*medI + ra^2 - ( 4*medI*ra^2 + ra^4 ).^0.5); 


   plot(medE,medI,'--r','LineWidth',2);
   plot(medE,Ipos,'--m','LineWidth',2);
   plot(medE,Ineg,'--m','LineWidth',2);

elseif (ratioArray(iLikeIdx,eLikeIdx) > 0) 
   % I/E +ve  
   % ---- ratioArray is +ve we do a two sided test
   numCuts = 2;

   % plot line    : 
   ra = ratioArray(iLikeIdx,eLikeIdx);
   Ipos = 0.5 * (2*medI + ra^2 + ( 4*medI*ra^2 + ra^4 ).^0.5); 
   Ineg = 0.5 * (2*medI + ra^2 - ( 4*medI*ra^2 + ra^4 ).^0.5); 

   plot(medE,medI,'--r','LineWidth',2);
   plot(medE,Ipos,'--m','LineWidth',2);
   plot(medE,Ineg,'--m','LineWidth',2);

elseif (ratioArray(eLikeIdx,iLikeIdx) < 0) 
   % E/I -ve 
   % ---- ratioArray is -ve we do a one sided test 
   numCuts = 1;

   % plot line    : 
   ra = abs(ratioArray(eLikeIdx,iLikeIdx));
   Ineg = 0.5 * (2*medI + ra^2 - ( 4*medI*ra^2 + ra^4 ).^0.5); 

   plot(medE,medI,'--r','LineWidth',2);
   plot(medE,Ineg,'--m','LineWidth',2);

elseif (ratioArray(iLikeIdx,eLikeIdx) < 0) 
   % I/E -ve  
   % ---- ratioArray is -ve we do a one sided test  
   numCuts = 1;

   % plot line    : 
   ra = abs(ratioArray(iLikeIdx,eLikeIdx));
   Ipos = 0.5 * (2*medI + ra^2 + ( 4*medI*ra^2 + ra^4 ).^0.5); 
   % ---- I started coding up lines that will add fixedRatioCuts to these plots
   %IposMed = 0.5 * (2*medI + ra^2 + ( 4*medI*ra^2 + ra^4 ).^0.5); 
   % plot line    : I = (E + delta) * ratio
   %IposFixed =(medE + fixedDeltaArray(iLikeIdx,eLikeIdx)) ...
   %   * abs(fixedRatioArray(iLikeIdx,eLikeIdx)),'--m','LineWidth',2);
   %IposMed = max(Ipos,IposFixed);

   plot(medE,medI,'--r','LineWidth',2);
   plot(medE,Ipos,'--m','LineWidth',2);

end

% ---- Status report.
fprintf(1,'done! \n');

return;
