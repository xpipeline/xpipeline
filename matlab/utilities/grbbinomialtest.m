function [Pmin_raw, Pmin, index, dPmin] = grbbinomialtest(localProb,Ndraws,Nmc) 
% GRBBINOMIALTEST - Perform binomial test a la S2-S3-S4 GRB search.
%
% GRBBINOMIALTEST tests a set of numbers on the interval [0,1] for
% consistency with being the tail of a larger set of given size drawn from
% a uniform distribution.  See Abbott et al., PHYSICAL REVIEW D 77, 062004
% (2008), Section V A.
% 
% usage:
%
%  [Pmin_raw, Pmin, index, dPmin] = grbbinomialtest(localProb,Ndraws,Nmc) 
%
%  localProb    Vector of numbers to test.
%  Ndraws       Scalar or vector.  If scalar, number of draws in the larger
%               set, of which localProb is the tail (lowest elements).  If
%               vector, then number of draws is the length of the vector
%               and the individual elements are taken as the number of
%               background trials for each draw (i.e., the possible
%               probabilities are restricted to multiples of 1/Ndraws(i)).
%  Nmc          Optional scalar.  Number of Monte Carlo trials to use in
%               computing Pmin.  Default 1e4.
%
%  Pmin_raw     Lowest cumulative binomial probability of the input set
%               localProb.  Note that this number does not account for the
%               trials factor when length(localProb)>1.
%  Pmin         Probability that the tail of length(localProb) of a set of
%               Ndraws uniformly distributed random numbers will give a
%               cumulative binomial probability less than or equal to
%               Pmin_raw.
%  index        Element of sort(localProb)for which the lowest cumulative
%               binomial probability Pmin_raw occurs.
%  dPmin        1 sigma uncertainty in Pmin, estimated from the square root
%               of the number of Monte Carlo trials giving lower Pmin_raw. 
%
% Example: Numbers to mimic the S2-S3-S4 result.
% Ndraws = 59;
% localProb = [ones(1,9) * 0.1035 ones(1,6)];  %-- 25 ms search
% localProb = [ones(1,9) * 0.1115 ones(1,6)];  %-- 100 ms search
%
% $Id$

% ---- Check for optional arguments
nargchk(2,3,nargin);
if nargin<3
    Nmc = 1e4;
end

% ---- Check to see if we should draw from a uniform distribution or
%      discrete approximations to it.
uniformDist = 1;
if length(Ndraws)>1
    Nbckgrd = Ndraws(:).';  %-- force row vector.
    Ndraws = length(Ndraws);
    uniformDist = 0;
end

% ---- Sort the local probabilities and count how many there are.  Force
%      row vector.
localProb = sort(localProb);
localProb = localProb(:).';
Ntail = length(localProb);

% ---- Cumulative binomial probability of getting (1+,2+,...Ntail+) events
%      this improbable.
P = 1 - binocdf([0:Ntail-1],Ndraws,localProb);
% ---- Lowest probability and which element it occurs for.
[Pmin_raw index] = min(P);

if uniformDist
    
    % ---- Background Monte Carlo to handle trials factor for N>1.
    % ---- Generate uniformly distributed local probabilities.
    localProbMC = sort(rand(Nmc,Ndraws),2);

else
    
    % ---- Each of the Ndraws background draws should be taken from the
    %      discrete distribution [0:1:Nbckgrd(ii)]/Nbckgrd(ii) rather than
    %      uniformly on [0,1]. 
    Narray = repmat(Nbckgrd,Nmc,1);
    localProbMC = (unidrnd(Narray+1)-1)./Narray;  %-- +1/-1 accounts for starting from 0.
    localProbMC = sort(localProbMC,2);

end

% ---- Keep N most significant values.
localProbMC = localProbMC(:,1:Ntail);
% ---- Cumulative binomial probability for each trial.
PMC = 1 - binocdf(repmat([0:Ntail-1],Nmc,1),Ndraws,localProbMC);
PminMC = min(PMC,[],2);
% ---- True significance of Pmin is fraction of Monte Carlo trials giving 
%      number that small or smaller.
Pmin = mean(PminMC <= Pmin_raw);
dPmin = (Pmin/Nmc)^0.5;

% ---- Done.
return
