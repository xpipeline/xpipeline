function segs = ComplementSegmentList(start,dur,t0,t1)  
% COMPLEMENTSEGMENTLIST - Return the complement of a segment list.
%
%  segs = ComplementSegmentList(start,dur,t0,t1)  
%
%  start    Start times of input segment list.  Must be time-ordered.
%  dur      Durations of input segment list.
%  t0       Earliest time to include in complement.  
%           Must have t0<start(1).
%  t1       Latest time to include in complement.  
%           Must have t1>start(end)+dur(end).
%
%  segs     Segment list [start,dur] of the closure of all times >=t0 and 
%           <=t1 which are not in the input segment list.  By closure, I 
%           mean that the boundaries of the input segments are also in the 
%           output segments.  All output segments have duration > 0.  
%
% original write: Patrick J. Sutton 2004.11.10
% $Id$

% ---- Construct segments.
segs(:,1) = [t0; (start+dur)];
segs(:,2) = [start; t1];
segs(:,3) = segs(:,2)-segs(:,1);
segs = segs(:,[1,3]);

% ---- Remove zero-length segments.
k = find(segs(:,end)==0);
segs(k,:) = [];

%----- Done
return

