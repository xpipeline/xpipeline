function xaddfiguretable(fout,plotName,figures_dirName,caption)
% XADDFIGURETABLE - Add a set of plots to the web page as a table.
%
% usage:
%
%    xaddfiguretable(fout,plotName,figures_dirName,caption)
%
%  fout             File handle for web page.
%  plotName         String or cell array of strings. Names of plots to put in
%                   the table.
%  figures_dirName  String. Name of figures directory.
%  caption          String or cell array of strings. Captions to be used to
%                   describe the current figure.
%
% $Id$

% ---- Checks.
error(nargchk(3,4,nargin));

% ---- Convert plotName to cell array, if necessary.
if (isstr(plotName))
    tempPlotName{1} = plotName;
    plotName = {};
    plotName{1} = tempPlotName{1}; 
end

% ---- If caption has not been given make them empty.
if nargin ==3
    for iPlot = 1:length(plotName)
        caption{iPlot} = ''; 
    end
end

% ---- Convert caption to cell array, if necessary.
if (isstr(caption))
    tempCaption{1} = caption;
    caption = {};
    caption{1} = tempCaption{1}; 
end

% ---- Check we have the same number of captions as plotNames
if length(caption) ~= length(plotName)
    error('Different numbers of captions and plotnames.');
end

% ---- Write portion of output html file that will display these plots.
fprintf(fout,'%s\n','<br><TABLE BORDER=0><TR>');
for iPlot = 1:length(plotName)
    fprintf(fout,'%s\n',['<TD ALIGN="left"><A HREF="' figures_dirName ...
        plotName{iPlot} '.png"><IMG SRC="' figures_dirName plotName{iPlot} ...
        '_thumb.png" WIDTH=300 '...
        'ALT="' caption{iPlot} '" TITLE="' caption{iPlot} '"></A></TD>']);
end
fprintf(fout,'%s\n','</TR></TABLE>');

return

