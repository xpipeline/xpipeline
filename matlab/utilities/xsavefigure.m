function xsavefigure(plotName,figures_dirName,figfiles_dirName)
% XSAVEFIGURE - Save the current plot in various file formats.
%
% usage:
%
%  xsavefigure(plotName,figures_dirName,figfiles_dirName)
%
% plotName          String to use in plot name when saving.
% figures_dirName   String. Name of figures dir
% figfiles_dirName  String. Name of figfiles dir
%
% $Id$

% ---- Checks.
error(nargchk(3,3,nargin));

% ---- Hardwired formatting.
plot_fullsize = [0 0 12 9];
plot_thumbsize = [0 0 6 4.5];
resolution_dpi = 150;  %-- matlab default is 150

% ---- Save current figure.
saveas(gcf,[figfiles_dirName plotName '.fig'],'fig')
set(gcf,'PaperUnits','inches','PaperPosition',plot_fullsize)
print('-dpng',['-r' num2str(resolution_dpi)],[figures_dirName plotName '.png']);
legend('off')
set(gcf,'PaperUnits','inches','PaperPosition',plot_thumbsize)
print('-dpng',['-r' num2str(resolution_dpi)],[figures_dirName plotName '_thumb.png']);
close

% ---- Done.
return
