function E = cosmoE(z)
%
% Hubble constant evolution factor: H(z) = H0 * E(z)
% see (astro-ph/9905116)

% MAGIC numbers from arXiv:1502.01589
Om = 0.308; % fraction of matter
Ol = 1 - Om; % fraction of dark energy

E = sqrt(Om*(1+z).^3 + Ol);
