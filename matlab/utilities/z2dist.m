function [r DH]= z2dist(z)
% Z2DIST - convert redshift to luminosity distance
%
% z2dist for details

% MAGIC numbers 
H0 = 67.8e3; % m/s/Mpc from 1502.01589
c = 299792458; % m/s

% compute redshift to distance relation ship on a grid of points
zGrid = [0 logspace(-3,1,1000)];
DH = c/H0;
H = cosmoE(zGrid)/DH;
dLumin = cumtrapz(zGrid,1./H).*(1+zGrid);

% interpolate to get the distance value at the desired points
r = interp1(zGrid,dLumin,z);