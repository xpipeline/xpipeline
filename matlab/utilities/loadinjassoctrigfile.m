function [injAssociatedTrigger, injectionProcessedMask, injectionScale, foundIdx, missedIdx] = loadinjassoctrigfile(fileName,padTriggers,threshold,nInjections)
% LOADINJASSOCTRIGFILE - load "associatedTriggers" file from X post-processing 
%
% usage:
%
% [injAssTrig, injProcMask, injScale, foundIdx, missedIdx] = ...
%     loadinjassoctrigfile(fileName,padTriggers,threshold,nInjections)
%
% fileName     String. Name of file to load.
% padTriggers  Optional Boolean. If true, replace empty elements of
%              injAssTrig array (those with no triggers) by a single
%              trigger with all-null values.  This is very handy for doing
%              column-wise manipulations of the triggers. Default false.
% threshold    Optional scalar.  If specified, used as a threshold on the
%              trigger significance when deciding if an injection is found
%              or missed (see below). Default 0.
% nInjections  Optional scalar.  If specified, and if padTriggers=true,
%              then output trigger array will be padded with empty triggers
%              as necessary to have nInjections rows. This option is
%              useful when the last injections in an injection log file
%              are not processed (e.g. no coincident data), so that the
%              associated trigger list is shorter than expected when
%              comparing to an injection log.
%
% injAssTrig   Struct array. Element (M,N) holds all the triggers associated
%              with injection M at injection scale injScale(N).
% injProcMask  Array. 1 if associated injection (M,N) was processed, 0 otherwise.
% injScale     Vector. Injection scale used for each column of injAssTrig.
% foundIdx     Cell array. Each element lists the indices of all "found" 
%              injections at the associated injScale. An injection is considered 
%              found if injProcMask==1 and trigger pass=1 and (optionally)
%              trigger significance > threshold. 
% missedIdx    Cell array. Each element lists the indices of all "missed" 
%              injections at the associated injScale. An injection is considered 
%              missed if injProcMask==1 and (trigger pass=0 or (optionally)
%              trigger significance<=threshold). 
%
% $Id$

% ---- Check inputs.
narginchk(1,4)

% ---- Defaults.
if nargin<3
    threshold = 0;
end
if nargin<2
    padTriggers = false;
end

% ---- Load file.
load(fileName);

% ---- Extract injection scales, injection processed mask for this wavefrom
%      injection set only.
injectionScale = injectionScale(iWave,:);
injectionProcessedMask = injectionProcessedMask{iWave};

% ---- Sort all matrices column-wise by injection scale.
[injectionScale, I] = sort(injectionScale);
injAssociatedTrigger = injAssociatedTrigger(:,I);
injectionProcessedMask = injectionProcessedMask(:,I);

if padTriggers
    
    % ---- Pad out missing injection rows with empty triggers ([]), if requested
    %      and required. The [] entries will then be replaced by null triggers
    %      below.
    if exist('nInjections','var') && isscalar(nInjections) ...
            && (nInjections>size(injAssociatedTrigger,1))    
        injAssociatedTrigger(nInjections,end).significance = [];
        injectionProcessedMask(nInjections,end) = 0;
    end
    
    % ---- Replace all empty injection array elements with a "null" trigger -- 
    %      all properties = 0, but non-empty. This is very handy for doing
    %      column-wise comparisons of injections.
    % ---- Find a non-empty trigger to act as the template. Start from the
    %      loudest injection scale. 
    N = numel(injAssociatedTrigger);
    ii = N;
    while ii>0 && isempty(injAssociatedTrigger(ii).significance)
        ii = ii - 1;
    end
    if ii==0
        warning('No injection triggers in file.');
    else
        % ---- Extract non-empty trigger as template.
        nulltrig = injAssociatedTrigger(ii);    
        names = fieldnames(nulltrig);
        for ii=1:length(names)
            nulltrig = setfield(nulltrig,names{ii},zeros(size(getfield(nulltrig,names{ii}))));
        end
        % ---- Walk through array, repalcing empty entries.
        for jj=1:N
            if isempty(injAssociatedTrigger(jj).significance)
                injAssociatedTrigger(jj) = nulltrig;
            end
        end
    end
    % ---- Determine indices of found and missed injections.
    foundIdx  = cell(1,length(injectionScale));
    missedIdx = cell(1,length(injectionScale));
    for jj = 1:length(injectionScale);
        ias = injAssociatedTrigger(:,jj);
        ipm = injectionProcessedMask(:,jj);
        foundIdx{jj}  = find(ipm & [ias.pass]'==1 & [ias.significance]'>threshold);
        missedIdx{jj} = find(ipm & ([ias.pass]'==0 | [ias.significance]'<=threshold));
    end

end

return
