function [theta, phi] = xtimedelaygrid(network,dt,verbose)
% XTIMEDELAYGRID - generate an all-sky grid based on time-delay errors
%
% XTIMEDELAYGRID generates an all-sky grid for a three-site nework such
% that for any possible sky position there is a grid point such that the
% maximum error in time delay along all baselines for that grid point is
% less than the user-specified value. 
%
% usage:
%
%   [theta, phi] = optimalskytiling(network,dt,verbose)
% 
%   network   Cell array of strings.  Each element is a detector site 
%             recognized by LOADDETECTORDATA.  Must have 3 elements.
%             Example: {'H','L','V'}
%   dt        Scalar.  Size of time delay steps along each baseline [s].
%   verbose   Optional flag.  If nonzero, produce plots and messages. 
%
%   theta     Vector of polar coordinates of sky grid points.
%   phi       Vector of azimuthal coordinates of sky grid points.
%
% The sky grid (theta,phi) are in Earth-fixed coordinates, as defined in 
% COMPUTEANTENNARESPONSE.
%
% WARNING: This function may not correctly cover the region where the plane
% of detectors intersects the celestial sphere (the boundary of allowed
% time-delay space).  
%
% $Id$

% ---- Check number of input arguments and assign any needed defaults.
narginchk(2, 3);
if (nargin<3)
    verbose = 0;
end

% ---- Speed of light (in m/s).
speedOfLight = 299792458;

% ---- Compute largest possible time delay.
dmax = 0;
for ii = 1:(length(network)-1)
    Det1 = LoadDetectorData(network{ii});
    for jj = (ii+1):length(network)
        Det2 = LoadDetectorData(network{jj});
        dmax12 = norm(Det1.V-Det2.V)/speedOfLight;
        dmax = max(dmax,dmax12);
    end
end
if verbose
    disp(['Maximum delay across network: ' num2str(dmax) ' seconds.'])
end

% ---- Construct square grid of time delay steps dt that span +/-dmax. 
N1 = ceil(dmax/dt);
[d12,d23] = meshgrid([-N1:N1],[-N1:N1]);
d12 = d12(:) * dt;
d23 = d23(:) * dt;
d31 = - (d12 + d23);

% ---- Construct sky grid.
warning off
[theta_zp,phi_zp,theta_zm,phi_zm,idx_keep] = xtimedelaytoskyposition([d12, -d31],network,verbose);
warning on
theta = [theta_zp ; theta_zm];
phi   = [phi_zp   ; phi_zm];
if verbose
    disp(['Grid contains ' num2str(length(theta)) ' points.'])
end

% ---- Done.
return


% OLD CODE FOLLOWS: Some of this may be useful for down-selecting to a grid
% that covers eg a GRB error box. Also, the "fac = [1,2];" code below is
% somethign I don't understand that may be worth looking at for optimising
% the grids.

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %         For each pair of sites calculate angle with GRB line of sight.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% for iPair = 1:length(pairNames)
% 
%    site1ToSite2_sec(iPair,:) = site1ToSite2(iPair,:) ./ speedOfLight;
% 
%    % ---- Some shorthand.
%    v1 = site1ToSite2_sec(iPair,:);
%    v2 = earthToGRB;
% 
%    % ---- Invert dot product to calculate opening angle. 
%    lambda_ctr_rad(iPair) = acos(dot(v1,v2)/(norm(v1)*norm(v2)));
% 
%    % ---- Lambda should be between 0 and pi.
%    if lambda_ctr_rad(iPair) > pi | lambda_ctr_rad(iPair) < 0
%       disp(['lambda_ctr_rad = ' num2str(lambda_ctr_rad(iPair))])
%       error('lambda_ctr_rad should be between 0 and pi.')
%    end
% 
%    % ---- Add (subtract) skyPosError to find extreme angles.
%    lambda_max_rad(iPair) = lambda_ctr_rad(iPair) + skyPosError_rad;
%    lambda_min_rad(iPair) = lambda_ctr_rad(iPair) - skyPosError_rad;
%   
%    % ---- Find range of time-delays that can be caused by variation in
%    %      GRB sky location.
%    delay_ctr(iPair) = cos(lambda_ctr_rad(iPair)) * norm(v1); 
% 
%    delay_max(iPair)     = cos(lambda_min_rad(iPair)) * norm(v1); 
%    delay_min(iPair)     = cos(lambda_max_rad(iPair)) * norm(v1); 
% 
%    if lambda_max_rad(iPair) > pi
%       disp('lambda_max_rad > pi')
%       delay_max(iPair) = max([delay_max(iPair) delay_min(iPair)]); 
%       delay_min(iPair) = -norm(v1);
%    end
% 
%    if lambda_min_rad(iPair) < 0
%       disp('lambda_min_rad < 0')
%       delay_min(iPair) = min([delay_max(iPair) delay_min(iPair)]); 
%       delay_max(iPair) = norm(v1);
%    end
% 
%    delay_range(iPair) = delay_max(iPair) - delay_min(iPair);
% 
% end
% 
% clear v1 v2
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %          Create vector of time-delays we wish to use.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % ---- We should be able to use fac=2 spacing but this leads to holes...
% if delay_range(2) < delay_range(1) 
%    fac = [1,2];
% else
%    fac = [2,1];
% end
% 
% for iPair = 1:length(pairNames)
%     delay_vec{iPair} = ...
%       [sort(delay_ctr(iPair)-fac(iPair)*delay_tol:-fac(iPair)*delay_tol:delay_min(iPair)),...
%       delay_ctr(iPair):fac(iPair)*delay_tol:delay_max(iPair)];
% 
%     % ---- Check we are not missing a point on the edge of our grid.
%     if abs(min(delay_vec{iPair}) - delay_min(iPair)) > 0.5 * fac(iPair) * delay_tol
%        delay_vec{iPair} = [delay_min(iPair), delay_vec{iPair}];
%     end
% 
%     if abs(delay_max(iPair) - max(delay_vec{iPair}))  > 0.5 * fac(iPair) * delay_tol
%        delay_vec{iPair} = [delay_vec{iPair} delay_max(iPair)];
%     end
% 
% end
% 
% % ---- If user only input 2 sites.
% if nSitesIn == 2
%    delay_vec{2} = delay_ctr(iPair);
% end 
% 
% delay_array= [];
% for idx1 = 1:length(delay_vec{1})
%    for idx2 = 1:length(delay_vec{2})
%       delay_array = [delay_array; -delay_vec{1}(idx1), -delay_vec{2}(idx2)];
%    end
% end
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %         Calculate sky positions for these time delays.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% [theta_grid_zp,phi_grid_zp,theta_grid_zm,phi_grid_zm] = ...
%    xtimedelaytoskyposition(delay_array,sites,verbose);
% 
% theta_grid_rad = [theta_grid_zp;theta_grid_zm];
% phi_grid_rad   = [phi_grid_zp;phi_grid_zm];
% 
% nNans = sum(isnan(theta_grid_rad));
% 
% % ---- Calculate time-delays between sites and Earth's centre for these
% %      sky positions.
% skyPosition_grid =  [theta_grid_rad, ...
%                      phi_grid_rad, ...
%                      zeros(length(theta_grid_rad),1), ...
%                      zeros(length(phi_grid_rad),1)];
% 
% deltaT_grid = computeTimeShifts( sites, skyPosition_grid );
% 
% % ---- Calculate inter-site delays.
% delay_grid = [];
% for iSite1 = 1:nSites-1
%    for iSite2 = iSite1+1:nSites
%       delay_grid = [delay_grid, ...
%          deltaT_grid(:,iSite2) - deltaT_grid(:,iSite1)];
%    end
% end
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %      Calculate bounds of the cone with opening angle of skyPosError
% %                          about earthToGRB
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % ---- errorVec is a vector with opening angle of skyPosError in the
% %      +ve theta direction. It is important to do this in the theta
% %      rather than phi direction.
% errorVec  = [sin(theta_ctr_rad + skyPosError_rad)*cos(phi_ctr_rad) ...  % x
%              sin(theta_ctr_rad + skyPosError_rad)*sin(phi_ctr_rad) ...  % y
%              cos(theta_ctr_rad + skyPosError_rad)];                     % z 
% 
% % ---- Sanity checking.
% opening_angle_rad = acos(dot(earthToGRB,errorVec) / ...
%    (norm(earthToGRB)*norm(errorVec)));
% if opening_angle_rad - skyPosError_rad > 10000* eps
%    error('Something has gone wrong!')
% end
% 
% % ---- Now we rotate errorVec about earthToGRB and record the theta and
% %      phi values for the bounds of this cone.
% psi = 0:pi/2000:2*pi;
% idx = 1;
% for idx = 1:length(psi)
%    errorCircle(idx,:) = RotateVector(errorVec,earthToGRB,psi(idx));
%    % ---- Convert directions to theta and phi co-ords.
%    theta_error_rad(idx) = acos(errorCircle(idx,3)/norm(errorCircle(idx,:)));
%    phi_error_rad(idx)   = atan2(errorCircle(idx,2),errorCircle(idx,1));
% end
% 
% [ra_error_deg, dec_error_deg] = ...
%    earthtoradec(phi_error_rad,theta_error_rad,gps);
% ra_error_deg = ra_error_deg - floor(ra_error_deg/360) * 360;
% 
% % ---- Calculate time-delays between sites and Earth's centre for these
% %      sky positions.
% skyPosition_error = [theta_error_rad', ...
%                      phi_error_rad', ...
%                      zeros(length(theta_error_rad),1), ...
%                      zeros(length(phi_error_rad),1)];
% 
% deltaT_error = computeTimeShifts( sites, skyPosition_error );
% 
% % ---- Calculate inter-site delays.
% delay_error = [];
% for iSite1 = 1:nSites-1
%    for iSite2 = iSite1+1:nSites
%       delay_error = [delay_error, ...
%          deltaT_error(:,iSite2) - deltaT_error(:,iSite1)];
%    end
% end
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %        Discard points which lay outside cone of skyPosError about GRB.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % ---- Find maximum angular spacing between adjacent points.
% %      We will extend the area of sky we search over by this
% %      angle to avoid edge effects. 
% %      Example of edge effects is the case where a sky location
% %      outside the circle defined by skyPosError has time delays
% %      that cover some region of parameter space within that circle. 
% 
% V_grid = CartesianPointingVector(phi_grid_rad,theta_grid_rad);
% 
% % ---- If this_dir lies within an opening angle of skyPosError
% %      of earthToGRB we will actually search this point for the
% %      GRB.
% theta_search_rad = []; 
% phi_search_rad = [];
% opening_angle_search_rad = []; 
% 
% % ---- Tunable parameter.
% angle_fac = 6;
% for idx = 1:length(theta_grid_rad)
%    % ---- Calculate opening angle between current sky position and 
%    %      GRB central position.
%    opening_angle_rad = acos(dot(V_grid(idx,:),earthToGRB)/...
%             (norm(V_grid(idx,:))*norm(earthToGRB)));
%    % ---- If opening angle less than skyPosError_rad we will retain
%    %      this sky position. 
%    if opening_angle_rad < skyPosError_rad
%       theta_search_rad = [theta_search_rad; theta_grid_rad(idx)];    
%       phi_search_rad   = [phi_search_rad; phi_grid_rad(idx)];    
%       opening_angle_search_rad = [opening_angle_search_rad; opening_angle_rad];
% 
%    % ---- If opening angle is within (angle_fac * skyPosError_rad) of
%    %      the central GRB position we will assess whether this sky position
%    %      has delay times which overlap any points within our sky area.
%    %      If so, we retain it.  
%    elseif opening_angle_rad < angle_fac * skyPosError_rad
% 
%       passFlag = 0;
%       for iPair = 1:length(pairNames)
%          passFlag = passFlag + ...
%          ((delay_error(:,iPair)+2*delay_tol >= delay_grid(idx,iPair)) & ...
%           (delay_error(:,iPair)-2*delay_tol <= delay_grid(idx,iPair)))';
%       end
% 
%       if max(passFlag) == length(pairNames)
%          theta_search_rad = [theta_search_rad; theta_grid_rad(idx)];    
%          phi_search_rad   = [phi_search_rad; phi_grid_rad(idx)];    
%          opening_angle_search_rad = [opening_angle_search_rad; ...
%             opening_angle_rad];
%       end
%    end  
% end % -- end loop over theta_grid_rad.

