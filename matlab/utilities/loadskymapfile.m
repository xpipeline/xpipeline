function out = loadskymapfile(skyMapFile)
% LOADSKYMAPFILE - load a fits file containing a LIGO-VIRGO-KAGRA formatted sky map 
%
% usage:
%
%   out = loadskymapfile(file)
%
%   file    String. Name of fits-formatted file.
%
%   out     Struct with fields 
%             fitsinfo - output of the fitsinfo() command
%             fitsdata - output of the fitsread() command
%             map      - struct containing map data. Fields:
%                    nuniq : Vector. "Uniq" pixel indices.
%                    order : Vector. HEALPix order of corresponding map pixel.
%                     ipix : Vector. HEALPix pixel number of corresponding map pixel.
%                      dec : Vector. Declination [deg] of corresponding pixel.
%                       ra : Vector. Right ascension [deg] of corresponding pixel.
%                     area : Vector. Area [steradians] of corresponding pixel.
%              probDensity : Vector. Probability density [steradian^-1] of corresponding pixel.
%                     prob : Vector. Probability of corresponding pixel.
%             derived  - struct containing derived data, mainly timing info. Fields:
%                       mjd: Modified Julian Day of the event
%                  gmst_deg: Greenwich Mean Sidereal Time [deg] of the event.
%                  gmst_sec: Greenwich Mean Sidereal Time [sec] of the event.
%                  ordering: HEALPix ordering scheme of the map. 
%                   dateStr: Date string of the event, e.g. '2019-04-25-08:18:05.020199'
%                   dateNum: Date number corresponding to dateStr rounded down
%                            to an integer second as determined by the datenum() function. 
%                       gps: GPS time [s] of the event.
%
% $Id$

% ---- Load info and raw data from fits file -----------------------------------

% ---- Load the sky localisation error box.
out.fitsinfo = fitsinfo(skyMapFile);
out.fitsdata = fitsread(skyMapFile,'binarytable');

% ---- Timing information ------------------------------------------------------

% ---- Extract MJD of event from the fits file.
idx = find(~cellfun(@isempty,strfind(out.fitsinfo.BinaryTable.Keywords(:,1),'MJD-OBS')));
out.derived.mjd = out.fitsinfo.BinaryTable.Keywords{idx,2}; 
clear idx

% ---- Convert MJD to GMST.
out.derived.gmst_deg = JD2GMST(2400000.5 + out.derived.mjd);
out.derived.gmst_sec = out.derived.gmst_deg / 15 * 3600;

% ---- Extract the UTC time of the event.
idx = find(~cellfun(@isempty,strfind(out.fitsinfo.BinaryTable.Keywords(:,1),'DATE-OBS')));
out.derived.dateStr = out.fitsinfo.BinaryTable.Keywords{idx,2};
clear idx

% ---- Convert UTC time to GPS time.
%      Format of dateStr is, eg, '2019-04-25T08:18:05.020199'. Convert to format
%      that can be fed to datenum(), dropping fractional seconds.
out.derived.dateStr(11) = '-';
out.derived.dateNum = datenum(out.derived.dateStr(1:19),'yyyy-mm-dd-HH:MM:SS');
% ---- Estimate the GPS time by counting the number of days since GPS time zero.
%      The "+18" offset accounts for leap years as of 28 May 2018, so the
%      conversion may be off by 1 or 2 seconds for events in the O1 - O3 eras.
%      See https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z0000019LYLSA2   
out.derived.gps = (out.derived.dateNum - datenum('1980-01-06'))*86400+18;

% ---- Map Data ----------------------------------------------------------------

% ---- Extract the ordering scheme used for the sky map.
idx = find(~cellfun(@isempty,strfind(out.fitsinfo.BinaryTable.Keywords(:,1),'ORDERING')));
out.derived.ordering = out.fitsinfo.BinaryTable.Keywords{idx,2};
clear idx

if strcmpi(out.derived.ordering,'NUNIQ')

    % ---- Extract HEALPix order and pixel number for each pixel.
    % idx = find(~cellfun(@isempty,strfind(cellfun(@num2str,out.fitsinfo.BinaryTable.Keywords(:,2)),'UNIQ')));
    % index = out.fitsinfo.BinaryTable.Keywords{idx,1};
    % index = str2num(index{6:end})
    index = 1;
    out.map.nuniq = out.fitsdata{index};
    Nsky = length(out.map.nuniq);
    clear idx index
    out.map.order = round((floor(log2(out.map.nuniq))-isodd(floor(log2(out.map.nuniq))))/2-1);
    out.map.ipix = round(out.map.nuniq - 4*4.^out.map.order);

    % ---- Use MEALPix pix2ang() to determine (ra,dec) of each pixel centre.
    out.map.dec = zeros(Nsky,1);
    out.map.ra  = zeros(Nsky,1);
    for unique_order = unique(out.map.order).'
        idx = find(out.map.order==unique_order);
        nSide = 2^unique_order;
        % ---- MEALPix indexes pixels from 1,...,N instead of 0,...,N-1 so add 1
        %      to ipix values when using pix2ang(). 
        azi_pol = cell2mat(transpose(pix2ang(nSide,out.map.ipix(idx)+1,'nest',true)));
        out.map.dec(idx)   = 90 - (azi_pol(1,:).')*180/pi;
        out.map.ra(idx)    = (azi_pol(2,:).')*180/pi;
        out.map.order(idx) = unique_order;
    end
    
    % ---- Area of each map pixel.
    out.map.area = 4*pi./(12*4.^out.map.order);
    
    % ---- Sky map probability density values.    
    % idx = find(~cellfun(@isempty,strfind(out.fitsinfo.BinaryTable.Keywords(:,2),'PROBDENSITY')));
    % index = out.fitsinfo.BinaryTable.Keywords{idx,1};
    % index = str2num(index{6:end})
    index = 2;
    out.map.probDensity = out.fitsdata{index};
    clear idx index

    % ---- Sky map probability values.
    out.map.prob = out.map.probDensity .* out.map.area;
    
else
    
    error('Script only set up to handle NUNIQ HEALPix ordering scheme.')
    
    % ---- Inspecting out.fitsinfo.BinaryTable.Keywords shows the following to be true:
    nest = true;              %-- nested indexing used (rather than ring indexing)
    Nsky = length(out.fitsdata{1});   %-- number of sky grid points
    out.map.prob = out.fitsdata{1};           %-- sky out.map probability values
    % ---- Use MEALPix package to compute spherical coordinates (in radians) of the
    %      nested-indexing pixels for a healpix grid of this size. Then immediately
    %      convert to ra,dec.
    nSide = (Nsky/12)^0.5;
    azi_pol = cell2mat(pix2ang(nSide,'nest',true));
    out.map.dec = 90 - (azi_pol(1,:).')*180/pi;
    out.map.ra  = (azi_pol(2,:).')*180/pi;
    out.map.order = log2(nSide) * ones(size(out.map.ra));
    out.map.area = 4*pi./(12*4.^out.map.order);
    % ---- Sky out.map probability values.
    out.map.probDensity = out.map.prob ./ out.map.area;
    
end

% ---- Compute pixel locations in Earth-fixed coordinates.
[out.map.phi, out.map.theta] = radectoearth(out.map.ra,out.map.dec,out.derived.gps);

% ---- Done.
return

