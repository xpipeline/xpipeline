function r = gwbeffrange(Egw,f,S,rho)
% GWBEFFRANGE - SenseMon-style effective range for a GWB source.
%
%   r = gwbeffrange(Egw,f,S,rho)
%
% Egw   Scalar or vector.  Energy in GWs [solar masses].
% f     Scalar or vector.  Central frequency of the GWB [Hz].  If both
%       hrss and f are vectors, they must have the same size.
% S     Scalar or vector.  Detector one-sided power spectrum [Hz^{-1}] at
%       the frequencies in f. 
% rho   Optional scalar.  Matched-filter SNR required for a detection.
%       Default 8.
%
% r     Scalar or column vector.  Distance to the source [pc].
%
% If any of Egw, f, S are vectors, then they must have the same size.  A
% scalar value of the other variables is then treated as a vector with
% identical elements.
%
% GWBRANGE computes the effective detection range for a given GWB source
% and interferometric detector, as defined in LIGO-P1000041-v2, "A Rule of
% Thumb for the Detectability of Gravitational-Wave Bursts", by P. Sutton: 
%
%   r = (G*Egw / (2*pi^2*c^3*S*f^2*rho^2))^0.5;
%
% This range assumes a narrowband signal, and is accurate to ~5% for 
% isotropic, elliptical, and linearly polarized emission models, as
% described in that document.
%
% $Id$

% ---- Check number of input arguments.
error(nargchk(3, 4, nargin));

% ---- Assign default arguments.
if nargin<4
    rho = 8;
end

% ---- Check sizes of input arguments.
Egw = Egw(:);
f = f(:);
S = S(:);
vlength = unique([length(Egw); length(f); length(S)]);
% ---- Check for any vector values, and if so resize scalars.
if vlength ~= 1
    % ---- Remove scalars and verify on one non-unity size is left.
    maxlength = setdiff(vlength,1);
    if numel(maxlength)>1
        error('Input arguments Egw, f, S must be scalars or vectors of the same size.');
    end
    % ---- Resize any scalars.
    if length(Egw)==1 
        Egw = Egw*ones(maxlength,1);
    end
    if length(f)==1 
        f = f*ones(maxlength,1);
    end
    if length(S)==1 
        S = S*ones(maxlength,1);
    end
end

% ---- Needed constants.
speedOfLight = 299792458;  %-- m/s
NewtonG = 6.67e-11;        %-- Newton's G (Nm^2/kg^2=m^3/kg/s^2).
secondsPerYear = 365.25*86400;              %-- s
parsec = 3.26*speedOfLight*secondsPerYear;  %-- m
solarMass = 1.988435e30;   %-- kg

% ---- Range to GWB source which gives desired hrss given Egw [pc].
%r = (Egw*NewtonG*solarMass/speedOfLight)^0.5 / (pi*parsec) ./ (hrss.*f0);
r = (NewtonG*Egw*solarMass ./ (2*pi^2*speedOfLight*S.*f.^2*rho^2)).^0.5;
r = r / parsec;

% ---- Done.
return
