function Yd = xdetrend(Y)
% XDETREND - remove linear trend so that first and last samples are zero.
%
% use:
%
%   Yd = xdetrend(Y)
%
% Y and Yd are vectors of the same length. If the first and/or last samples of Y
% are non-zero, XDETREND subtracts a linear trend such that the first and last
% samples of Yd are both zero. If both the first and last samples of Y are zero
% then Yd=Y. 
%
% $Id$

% ---- Checks.
if ~isvector(Y)
    error('Input Y must be a vector.');
end

% ---- Do we need to detrend?
yStart = Y(1);
yEnd   = Y(end);
if yStart | yEnd 
    % ---- Enforce row vector.
    if iscolumn(Y)
        columnVector = true;
        Y = Y.';
    end
    % ---- Number of samples.
    N = length(Y);
    % ---- Determine gradient and intercept of linear trend.
    grad = (yEnd-yStart)/(N-1);
    intercept = yStart;
    % ---- Remove linear trend.
    trend = grad * [0:N-1] + intercept;
    Yd = Y - trend;
    % ---- Return to column vector if required.
    if columnVector
        Yd = Yd.';
    end
else
    Yd = Y;
end

% ---- Done.
return
