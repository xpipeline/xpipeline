function [] = xwriteoffmatfilefromMVAascii(matFile_original, trainingFile, testingFile, matFile_new)
% XWRITEOFFMATFILEFROMMVAASCII - merge TMVA results into matlab on/off-source files
%
% XWRITEOFFMATFILEFROMMVAASCII reads the ascii files produced by TMVA and merges
% the classification result into pre-existing matlab-formatted trigger files for
% off-source or on-source triggers. The merging replaces the the original
% X-Pipeline significance with the value of the MVA classifier for each trigger.
% Triggers in the original matlab file which do not have an MVA result have
% their significance set to zero.
%
% usage:
%
% xwriteoffmatfilefromMVAascii(injDirOld, trainingFile, testingFile, injDirNew)
%
%  matFile_original String. Full path to on- or off-source .mat results file.
%  trainingFile     String. Full path to ascii file produced by TMVA for
%                   training triggers. This file must have two columns:
%                     MVAscore, triggerIdx.
%                   Use [] if no injections were used for training. 
%  testingFile      String. Full path to ascii file produced by TMVA for testing
%                   triggers.  Same format as trainingFile.
%  matFile_new      String. Full path to on- or off-source .mat results file 
%                   that will be created containing the MVA_ranking in place of
%                   the X-Pipeline significance. Can not be the same as
%                   matFile_original.
%
% $Id$

% ---- Checks.
error(nargchk(4,4,nargin));
%narginchk(4, 4);

% ---- Refuse to overwrite existing files.
if strcmp(matFile_original,matFile_new)
    error('Do not overwrite original mat file!')
end

% ---- Read MVAScore and triggerIdx from the TMVA files.
if ~isempty(trainingFile)

    % ---- Load background training triggers.
    background_training = load(trainingFile);

    % ---- Load background testing triggers.
    background_testing  = load(testingFile);

    % ---- Concatentate into a single list.
    MVAScore   = [background_training(:,1); background_testing(:,1)];
    triggerIdx = [background_training(:,2); background_testing(:,2)];

else
    
    % ---- Load background testing triggers.
    background_testing  = load(testingFile);
    
    MVAScore   = [background_testing(:,1)];
    triggerIdx = [background_testing(:,2)];

end

% % ---- Sort on triggerIdx.
% [triggerIdx_sorted, sortingIdx] = sort(triggerIdx);
% triggerIdx = triggerIdx_sorted;
% MVAScore_sorted = MVAScore(sortingIdx);
% MVAScore = MVAScore_sorted;

% ---- Load original matfile.
data = load(matFile_original);
Ntrig = length(data.cluster.significance);

% ---- Copy original X-Pipeline signficance to a new field. this is not strictly
%      necessary (since it is recorded as one of the likelihoods anyway), but
%      legacy TMVA code may be expecting this field to be present.
data.cluster.significance_xtrig = data.cluster.significance;

% ---- Zero out significance for all triggers, then reset to MVA score for those
%      triggers processed by TMVA. 
data.cluster.significance = zeros(Ntrig,1);
data.cluster.significance(triggerIdx) = MVAScore;

% ---- Save new matlab file.
save(matFile_new, '-struct', 'data');

% clusterNew.significance_xtrig = cluster.significance(triggerIdx,:);
% 
% % ---- Loop over cluster fields keeping only elements with
% %      job numbers used for testing or training (i.e., not
% %      discarded by DQ flags).
% names = fieldnames(cluster);
% for iN = 1:length(names)
%     field = getfield(cluster,names{iN});
%     clusterNew = setfield(clusterNew,names{iN},field(triggerIdx,:));
% end
% 
% % ---- Add in MVA score in place of significance.
% clusterNew.significance = MVAScore;
% 
% cluster = [];
% cluster = clusterNew;

% % ---- Save output.
% if onflag
%     save(matFile_new, ...
%         'cluster', ...
%         'analysisTimesCell', ...
%         'blockTime', ...
%         'detectorList', ...
%         'injectionScale', ...
%         'likelihoodType', ...
%         'maximumFrequency', ...
%         'minimumFrequency', ...
%         'sampleFrequency', ...
%         'skyPositions', ...
%         'transientTime', ...
%         'skyPositionsTimes', ...
%         'amplitudeSpectraCell', ...
%         'gaussianityCell', ...
%         'svnversion_xdetection' ...
%         );
% else
%     save(matFile_new, ...
%         'cluster', ...
%         'analysisTimesCell', ...
%         'blockTime', ...
%         'detectorList', ...
%         'injectionScale', ...
%         'likelihoodType', ...
%         'maximumFrequency', ...
%         'minimumFrequency', ...
%         'sampleFrequency', ...
%         'skyPositions', ...
%         'transientTime' ...
%         );
% end

% ---- Done.
return
