function plotbb(bb,offset,color,width)
% plotbb - plot bounding boxes
%
% usage:
%
%   plotbb(bb,offset,color,width)
%
% bb      Array with four columns. Bounding boxes to be plotted, one per row.
% offset  Scalar. Value to subtract from bb(:,1) values when plotting (e.g. a
%         fiducial GPS time).
% color   Optional string. EdgeColor to use for box lines, e,g, 'r'. 
% width   Optional string. LineWidth to use for box lines, e,g, 2.
%
% The bounding boxes will be plotted on the current figure.
%
% $Id$

% ---- Apply offset to start times.
bb(:,1) = bb(:,1) - offset;

% ---- Plot.
for ii=1:size(bb,1)
    if nargin>=4
        rectangle('position',bb(ii,:),'EdgeColor',color,'linewidth',width);
    elseif nargin>=3
        rectangle('position',bb(ii,:),'EdgeColor',color);
    else
        rectangle('position',bb(ii,:));
    end
    hold on
end
grid on
set(gca,'fontsize',16)

% ---- Done.
return


