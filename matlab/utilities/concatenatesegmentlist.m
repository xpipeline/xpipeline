function [seg_out, start, dur] = concatenatesegmentlist(seg_start, seg_dur);
% CONCATENATESEGMENTLIST - Concatenate a segment list.
%
%   [seg_out, start, dur] = concatenatesegmentlist(seg_start, seg_dur);
%
%   seg_start   Vector.  Start times of input segment list.
%   seg_dur     Vector.  Durations of input segment list.
%
%   seg_out     2-column array containing start and duration times 
%               of concatenated segment list. Equal to [start, dur].
%   start       Column vector of start times. 
%   dur         Column vector of durations. 
%
% The concatenated segment list is formed by joining overlapping segments. 
% This list will be sorted. The input list does not need to be sorted.
%
% original write: Patrick J. Sutton 2004.07.19
% $Id$

% ---- Check inputs.
if numel(seg_start) ~= numel(seg_dur)
    error('Input arguments seg_start and seg_dur must have the same number of elements.');
end
% ---- If inputs are empty, then do nothing.
if isempty(seg_start)
    seg_out = [];
    start   = [];
    dur     = [];
    return
end
% ---- Further checks of inputs.
if ~isvector(seg_start)
    error('Input argument seg_start must be a vector.');
end
if ~isvector(seg_dur)
    error('Input argument seg_dur must be a vector.');
end
% if ~isscalar(tolerance)
%     error('Input argument tolerance must be a scalar.');
% end
% if (tolerance < 0)
%     error('Input argument tolerance must be non-negative.');
% end

% ---- Force row vectors.
seg_start = seg_start(:);
seg_dur = seg_dur(:);

% ---- Remove any segments with zero duration.
k = find(0==seg_dur);
seg_start(k) = [];
seg_dur(k) = [];

%----- Sort input array by start time.
[seg_start,I] = sort(seg_start);
seg_dur = seg_dur(I);
seg_stop = seg_start + seg_dur;

% % ---- Logical marker for each row that should NOT be merged with the next row.
% notmerge = not(abs(seg_stop(1:end-1)-seg_start(2:end))<=tolerance);
% % ---- Indices of these rows.
% notindex = find(1==notmerge);
% 
% % ---- Indices into frames for start and stop times of each merged segment.
% kstart = [1;notindex+1];
% kend = [notindex;length(seg_start)];
% 
% % ---- Make merged segment list.
% seg_out(:,1) = seg_start(kstart); 
% seg_out(:,2) = seg_start(kend) + seg_dur(kend);
% seg_out(:,2) = seg_out(:,2) - seg_out(:,1);

% ---- Procedure:
%      1 find earliest segment start time (seg "ii") and append to output list
%      2 find all other segments with start times <= end(ii)
%      3 find latest end time of segments from 2
%      4 reset end time end(ii) to latest end time from 3
%      5 delete all segments from 4 
%      6 return to 2, repeating until step 2 returns an empty list
%      7 if any segments remain, return to 1; else exit.

% ---- (1) Move earliest segment onto output list.
start(1) = seg_start(1);
stop(1)  = seg_stop(1);
seg_start(1) = [];
seg_dur(1)   = [];
seg_stop(1)  = [];
ii = 1;

while ~isempty(seg_start)
    
    % ---- (2) Find all other segments with start times <= end(ii).
    idx = find(seg_start <= stop(ii));
    if isempty(idx)
        % ---- No overlapping segments to concatenate.
        %      Go on to next output segment.
        ii = ii + 1;
        start(ii) = seg_start(1);
        stop(ii)  = seg_stop(1);
        seg_start(1) = [];
        seg_dur(1)   = [];
        seg_stop(1)  = [];
    else
        % ---- We have overlapping segments to concatenate.
        % ---- (3) Find latest end time of these segments.
        latest = max(seg_stop(idx));
        % ---- (4) Reset end time end(ii) to latest end time.
        stop(ii) = max(stop(ii),latest);
        % ---- (5) Delete redundant segments.
        seg_start(idx) = [];
        seg_dur(idx)   = [];
        seg_stop(idx)  = [];
    end

end

% ---- Assign outputs.
start = start(:);
dur = stop(:)-start(:);
seg_out = [start dur];

% ---- Done.
return

