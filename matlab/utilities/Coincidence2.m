function [Coinc start dur] = Coincidence2(ref_time, ref_dur, test_time, test_dur);
% COINCIDENCE2: Find overlapping segments between two segment lists.
%
%  [Coinc start dur] = Coincidence2(X,dX,Y,dY);
%
% COINCIDENCE2 returns the intersection times and indices of time intervals
% X and Y which overlap.
% 
%   X     Start times of first segment list
%   dX    Durations of first segment list
%   Y     Start times of second segment list
%   dY    Durations of second segment list
% 
%   Coinc  Mx6 array, where M is the number of pairs of coincident segments 
%          between X and Y.  The columns are the start time of the
%          intersection of the coincident segments, the duration of the
%          intersection, the start time of the union of the intersecting
%          segments, the duration of the union of the intersecting
%          segments, the index of the X segment, and the index of the
%          coincident Y segment. 
%   start  Mx1 vector containing the start time of the intersection 
%          of the coincident segments (i.e., the first column of Coinc).
%   dur    Mx1 vector containing the duration of the intersection 
%          of the coincident segments (i.e., the second column of Coinc).
%
% Two segments or time intervals [X(i),X(i)+dX(i)], [Y(j),Y(j)+dY(j)] are 
% considered to overlap if both of the following conditions are satisfied:
%
%   X(i) <= Y(j)+dY(j)
%   Y(j) <= X(i)+dX(i)
%
% The input lists must be time-ordered according to the start time of the 
% intervals.  Segments within a list may be overlapping.
%
% HINT: The function is typically faster if the larger segment list is 
% the second (ie, "Y").
%
% Adapted from J. Sylvestre's "time_coin.m" for two-IFO coincidence.
%
% -- Patrick J. Sutton 2004.06.15 (v1)
%              updated 2004.08.07 (v2), 2004.08.30 (v3)
%              rewritten 2004.08.30 (vectorized for speed) 
% $Id$


% ---- Coincidence array.  Columns are start time of intersection, 
%      duration of intersection, start time of union of intersecting
%      interval, duration of union, indices of coincident events.
Coinc = [];

% ---- Make sure inputs are non-empty before doing anything.
if (max(size(ref_time))*max(size(test_time))>0)

    % ---- Loop over each interval in first list.
    for j=1:length(ref_time)

        % ---- Convenient shorthand.
        rt = ref_time(j);
        rd = ref_dur(j);
   
        % ---- Find all intervals from second list overlapping current
        %      interval from first list.  Overlap can be a single
        %      point.
        k = find( (rt <= test_time+test_dur) & (rt+rd >= test_time) );
        % ---- Intersection times
        start = max(rt,test_time(k));
        dur = min(rt+rd,test_time(k)+test_dur(k));
        dur = dur - start;
        % ---- Union times
        ustart = min(rt,test_time(k));
        udur = max(rt+rd,test_time(k)+test_dur(k));
        udur = udur - ustart;
        % ---- Coincidence pairs: store indices and intersection
        %      interval.
        Coinc = [Coinc; start dur ustart udur j*ones(length(k),1) k];
        
    end;

    % ---- Make sure doubles (if any) are time sorted by start time of intersection.
    if (max(size(Coinc))>0)
        [Y,I] = sort(Coinc(:,1));
        Y = Coinc(I,:);
        Coinc = Y;
    end
end

% ---- Fill remaining output.
start = [];
dur   = [];
if ~isempty(Coinc)
    start = Coinc(:,1);
    dur   = Coinc(:,2);
end

% ---- Done
return
