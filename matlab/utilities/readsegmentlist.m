function [segno start stop dur] = readsegmentlist(file,startTime,stopTime)
% READSEGMENTLIST - read a LIGO-formatted segment list file.
%
% [segno start stop dur] = readsegmentlist(file,startTime,stopTime)
%
%  file       String.  Name of segment list file.
%  startTime  Optional scalar.  Return only segments starting at or later
%             than this time.
%  stopTime   Optional scalar.  Return only segments ending at or before
%             this time.
%
%  segno    Vector.  Segment numbers. If not supplied in the input file then
%           these are just the row number of each segment in the input file.
%  start    Vector.  Segment start times (GPS sec).
%  stop     Vector.  Segment stop times (GPS sec).
%  dur      Vector.  Segment durations (sec).
%
% Both startTime and stopTime inputs must be supplied if either one is. Any
% segment intersecting these boundaries is truncated to fit within the interval 
% [startTime, stopTime].
%
% READSEGMENTLIST accepts files with 2, 3, or 4 columns. If 2 columns, they must
% be either start,stop or start,dur.  If 3 columns, they must be start,stop,dur. 
% If 4 columns they must be segno,start,stop,dur. An error occurs if any 
% start,stop,dur values are not consistent to within 0.001 sec.
%
% Patrick J. Sutton 2008.05.21

% ---- Validate number of input arguments.
error(nargchk(1,3,nargin,'struct'))
if nargin==2
    error([num2str(nargin) ' input arguments supplied. Must supply both ' ...
        'startTime and stopTime if either is supplied.']);
end

% ---- Read the file.
fid = fopen(file);
% -- force conversion of input to int32
% C = textscan(fid,'%d%d%d%d','commentStyle','#');
C = textscan(fid,'%f%f%f%f','commentStyle','#');
fclose(fid);
% ---- textscan returns NaN for empty %f fields.  Reset these to zero.
if any(isnan(C{3})) 
    C{3} = zeros(size(C{3}));
end
if any(isnan(C{4})) 
    C{4} = zeros(size(C{4}));
end

% ---- Check for 2 vs. 3 vs. 4 columns.
if (0==max(abs(C{3})) & 0==max(abs(C{4})))
    % ---- Only two columns supplied.  These will either be (start,stop) or
    %      (start,dur). 
    if all(C{1}<C{2})
        % ---- Start, stop times.
        start = C{1};
        stop = C{2};
        dur = stop - start;
    elseif all(C{1}>C{2})
        % ---- Start, dur times.
        start = C{1};
        dur = C{2};
        stop = start + dur;
    else
        error(['Format of input file not recognized: 2 columns but ' ...
            'not consistent with either start,stop or start,duration.']);
    end
    segno = [1:length(start)]';
elseif (0==max(abs(C{4})))
    % ---- Three columns supplied.  These must be start, stop, and duration.
    start = C{1};
    stop = C{2};
    dur = C{3};
    segno = [1:length(start)]';
    % ---- Sanity check.
    if any(abs(start+dur-stop)>1e-3)
        error(['Format of input file not recognized: 3 columns but ' ...
            'start-stop-duration values not consistent to within 1 ms.']);
    end        
elseif (max(abs(C{3}))>0 & max(abs(C{4}))>0 )
    % ---- All four columns supplied.
    segno = C{1};
    start = C{2};
    stop = C{3};
    dur = C{4};
    % ---- Check for consistency of start, duration, stop times to 1ms
    %      precision.
    if any(abs(start+dur-stop)>1e-3)
        error(['Format of input file not recognized: 4 columns but ' ...
            'segno-start-stop-duration values not consistent to within 1 ms.']);
    end
else
    error('Format of input file not recognized.');
end

% ---- Restrict returned segment list to [startTime, stopTime] interval, if
%      desired. 
if nargin==3
    Coinc = Coincidence2(startTime,stopTime-startTime,start,stop-start);
    % ---- Check for and remove any zero-duration intersections.
    idx = find(Coinc(:,2)==0);
    if ~isempty(idx)
        Coinc(idx,:) = [];
    end
    % ---- Keep only intersecting segments.
    start = Coinc(:,1);
    stop  = Coinc(:,1)+Coinc(:,2);
    dur   = Coinc(:,2); 
    segno = [1:length(start)]';
end

% ---- Done.
return
