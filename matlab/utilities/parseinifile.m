function [data, name, loc] = parseinifile(fileName)
% PARSEINIFILE - parse an x-pipeline .ini file
%
% usage:
%
%   [data, name, loc] = parseinifile(fileName)
%
% fileName  String. Name of .ini file to be parsed.
%
% data      Struct. Each field is one the the sections of the .ini file (indicated in the
%           .ini file by square brackets, e.g. [section_name]). Each of these fields is
%           itself a struct with fieldname,value pairs that are the entries of the
%           corresponding .ini file section, except that all fieldnames have been cast to
%           lowercase with any hyphens replaced by underscores (see below).
% name      Struct. Same fields as data, but the values are lowercase versions of the
%           original field names from the .ini file with any hyphens preserved. This can 
%           be useful for, e.g., reading files with names that are based on the fieldnames
%           from the .ini file.    
% loc       Struct. Same fields as data, but the values are lists of indices where the 
%           original field name in the .ini file contained a hyphen instead of an
%           underscore.  
%
% WARNING: The python ConfigParser stores key names as lowercase by default. We mimic this
% by casting struct fields within sections to lowercase.
% 
% WARNING: MatLab does not allow hyphens (minus signs) in the names of struct fields. This
% function replaces hyphens by underscores in the output field names.
%
% $Id$

% ---- Read the .ini file.
try
    fid = fopen(fileName,'r');
    lines = textscan(fid,'%s','delimiter','\n');
    fclose(fid);
    lines = lines{1};    
catch
    error(['Unable to open specified file ' fileName])
end

% ---- Parse lines. The textscan command should remove any leading whitespace on
%      each line, so we can check to see if the line is non-empty, then whether
%      the first character is ; (comment line), [ (start of new section), or
%      anything else (line of variable = value pair).
numSection = 0;
for ii=1:length(lines)
    if ~isempty(lines{ii})
        line = lines{ii};
        if strcmp(line(1),';')
            % ---- Comment line; ignore.
            ;
        elseif strcmp(line(1),'[')
            % ---- Start of new section. Extract the name and create this as a
            %      new field in the output struct.
            numSection = numSection + 1;
            idx = strfind(line,']');
            %sectionNameHyphens{numSection} = strfind(line(2:idx-1),'-');
            sectionName{numSection} = strrep(line(2:idx-1),'-','_');
            sectionData{numSection} = [];
            hyphenName{numSection}  = [];
            hyphenLoc{numSection}   = [];
        else
            % ---- fieldname = value line in the same section. 
            % ---- Get the python key / matlab fieldname, casting to lowercase, before
            %      replacing any hyphens. 
            idx       = strfind(line,'=');
            rawName   = lower(strtrim(line(1:idx-1)));
            % ---- Now replace any hyphens in the key with underscores.
            hyphenIdx = strfind(rawName,'-');
            argName   = strrep(rawName,'-','_');
            % ---- Get the python value / matlab field value.
            argValue  = strtrim(line(idx+1:end));
            sectionData{numSection} = setfield(sectionData{numSection},argName,argValue);
            hyphenName{numSection}  = setfield(hyphenName{numSection},argName,rawName);
            hyphenLoc{numSection}   = setfield(hyphenLoc{numSection},argName,hyphenIdx);
        end
    end
end

% ---- Pack data into a single output struct.
data = [];
name = [];
loc  = [];
for ii=1:numSection
    data = setfield(data, sectionName{ii}, sectionData{ii});
    name = setfield(name, sectionName{ii}, hyphenName{ii});
    loc  = setfield(loc,  sectionName{ii}, hyphenLoc{ii});
end

% ---- Done.
return
    
