function coincidencedutycycle(startTime,stopTime,ifo,file,fileDir,verbose)
% COINCIDENCEDUTYCYCLE - Compute duty cycle for coincident running. 
%
% usage:
%
%   coincidencedutycycle(startTime,stopTime,ifo,file,fileDir)
%
%
%  startTime    Scalar. Start time of run.
%  stopTime     Scalar. End time of run.
%  ifo          Cell array of detector names, e.g. {'H1','L1','V1'}.
%  file         Cell array of segment file names, e.g. {'S5-H1_cat1.txt', ...
%               'S5-L1_cat1.txt','V1-SEGMENTS_INJCAT1.txt'}. Ordering must
%               match that of ifo input.
%  fileDir      Optional string. Path to directory containing segment files.
%               Default './'. 
%  verbose      Optional flag. If true then function sends extra output to
%               stdout. Default false.
%
% COINCIDENCEDUTYCYCLE prints to stdout the duty cycles for each IFO and also
% the duty cycle at which at least two IFOs are operating.
%
% $Id$

% ---- Validate number of input arguments.
error(nargchk(4,6,nargin,'struct'))
if nargin<6
    verbose = false;
end
if nargin<5
    fileDir = './';
end

% ---- Read segments, restricting to desired interval, and report duty cycles.
disp('Duty cycles:');
for ii=1:length(ifo)
    [segno{ii} start{ii} stop{ii} dur{ii}] = readsegmentlist([fileDir file{ii}],startTime,stopTime);
    disp([ifo{ii} ': ' num2str(sum(dur{ii})/(stopTime-startTime)*100,3) '%']);
end

% ---- Coincidence duty factor: union of all pairwise coincidence segments.
start2X = [];
dur2X = [];
for ii=1:(length(ifo)-1)
    for jj=(ii+1):length(ifo)
        Coinc = Coincidence2(start{ii},dur{ii},start{jj},dur{jj});
        start2X{end+1} = Coinc(:,1);
        dur2X{end+1}   = Coinc(:,2);
        if length(start2X)==1
            startCoinc = Coinc(:,1);
            durCoinc   = Coinc(:,2);
        else
            [startCoinc,durCoinc] = segunion(startCoinc,durCoinc,start2X{end},dur2X{end});
        end
        if verbose
            disp([ifo{ii} ',' ifo{jj} ' coincidence: ' ...
                num2str(sum(dur2X{end})/(stopTime-startTime)*100,3) '%']);
            disp(['accumulated coincidence: ' num2str(sum(durCoinc)/(stopTime-startTime)*100,3) '%']);
        end
    end
end
disp(['2+ detector coincidence: ' num2str(sum(durCoinc)/(stopTime-startTime)*100,3) '%']);

% ---- Done.
return
