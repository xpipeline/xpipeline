function [isoLines baselineNames] = xisotimedelay(skyPositions, ...
                                               detectorList)
% xisotimedelay - sky directions with same time delay wrt to baselines
%
% usage:
%
% [isoLines baselineNames] = xisotimedelay(skyPositions, detectorList)
%
% skyPositions  Vector. [theta phi] sky position of a *single* point in earth
%               fixed coordinates. The iso time delay curve will pass
%               througth this sky position.
% detectorList  Cell Array of detector names. Name format is the same as
%               for LoadDetectorData
% isoLines      3D array. The dimensions are: baseline, point on the
%               iso curve, [theta phi]. For each baseline contains the
%               earth fixed coordinates of the iso time delay curve
%               passing through skyPositions.
% baselineNames Cell Array of baseline (pairs of detectors) names.

% Checks
error(nargchk(2,2,nargin))

if size(skyPositions,1) ~= 1
  error(['xisotimedelay support only one sky position at time, you provided ' ...
         ']' ...
         num2str(size(skyPositions,1)) ' sky positions']);
end

nDet = length(detectorList);
% If only one detector, of two detectors but both at Hanford site, then assign dummy isolines with theta,phi = NaN.
if (nDet <= 1) | (nDet==2 & strcmp(detectorList{1}(1),'H') & strcmp(detectorList{2}(1),'H'))
  warning(['Only ' num2str(nDet) ...
           ' detectors at 1 site, not enough for a baseline'])
  baselineNames{1} = ['none'];
  isoLines(1,1,1) = NaN;
  isoLines(1,1,2) = NaN;
  return
end

% initialize variables
theta = skyPositions(:,1);
phi = skyPositions(:,2);
z = cos(theta);
y = sin(theta).*sin(phi);
x = sin(theta).*cos(phi);

circleGrid = 0:1e-2:2*pi;

northPole = [0 0 1];
nBaselines = 0;

% Compute isocurve for each baseline in the network
for id1=1:nDet,
    for id2=id1+1:nDet,
	nBaselines = nBaselines + 1;
	d1 = LoadDetectorData(detectorList{id1});
	d2 = LoadDetectorData(detectorList{id2});
	baseline = d1.V-d2.V;
	baseline = baseline/sqrt(baseline'*baseline);
	
        angleToBaseline = acos(dot([x y z],baseline));
        isoCircleAtNorthPole = ...
            [ones(size(circleGrid)).*angleToBaseline; circleGrid];
        rotationAxis = cross(northPole,baseline);
        rotationAngle = acos(dot(northPole,baseline));
        V = CartesianPointingVector(...
            isoCircleAtNorthPole(2,:),isoCircleAtNorthPole(1,:))';
        Vrot = RotateVector(V,rotationAxis,rotationAngle);

        isoLines(nBaselines,:,1) = acos(Vrot(3,:));
        isoLines(nBaselines,:,2) = atan2(Vrot(2,:),Vrot(1,:));
        baselineNames{nBaselines} = [detectorList{id1} detectorList{id2}];

        % --- check the accuracy of the isoLine
        CP = xSP2CP(squeeze(isoLines(nBaselines,:,:)),...
                    {detectorList{id1},detectorList{id2}});
        if (max(CP)-min(CP))/max(abs(CP)) > 1e-5
          error(['Iso line accuracy is ' ...
                 num2str((max(CP)-min(CP))/max(abs(CP)))]);
        end
        
    end
end
