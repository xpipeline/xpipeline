function [a b] = linfit(x1,y1,x2,y2);
% LINFIT - Return coefficiencts of straight line through two points.
%
% usage: 
%   [a b] = linfit(x1,y1,x2,y2);
%
% x1,y1     Coordinates of first point.
% x2,y2     Coordinates of second point.
%
% a,b       Coefficients of equation for the straight line a*x+b*y=1
%           passing through (x1,y1) and (x2,y2):
%             a = ( y2-y1)/(x1*y2-x2*y1);
%             b = (-x2+x1)/(x1*y2-x2*y1);
%
% $Id$

% ---- Argument checks.
error(nargchk(4,4,nargin));
if ~(isscalar(x1) & isscalar(y1) & isscalar(x2) & isscalar(y2))
    error('Input arguments must be scalars.');
end

a = ( y2-y1)/(x1*y2-x2*y1);
b = (-x2+x1)/(x1*y2-x2*y1);

return
