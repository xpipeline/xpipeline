function [] = xcleaninjectionfiles(injDir, injGlob, verbose)
% XCLEANINJECTIONFILES - Write mat file for single loudest event
% coincident with each injection, and supplies a uniqIdx for each
% injection to use with MVA book keeping. Removes noise events
% coincident with injecions using the lowest injection amplitude
% as a sample of the noise.
%
%  injDir           String. Full path to dir containing injection results
%                   files.
%  injGlob          String. We search injDir for files with name:
%                   *<injGlob>*merged.mat
%  verbose          Optional flag. If non-zero dump infor to screen about 
%                   which triggers are dropped.
%
% $Id$

% ---- Checks.
error(nargchk(2, 3, nargin));

if nargin<3
    verbose = false
end

% ---- Get list of inj files in injDir.
injFiles = dir([injDir '/*' injGlob '*merged.mat']);

% ---- Kludge to find insp string
injName = injGlob(1:4)
% ---- Set size of coincidence window
if strcmp(lower(injGlob),'inspiral') || ...
    strcmp(lower(injName),'insp') || ...
    strcmp(lower(injGlob),'inspiralsmooth') || ...
    strcmp(lower(injGlob),'ninja') || ...
    strcmp(lower(injGlob),'lalinspiral')
    % ---- Larger coincidence window for inspirals.
    disp('Working with inspiral waveform, -5.0 + 0.1 s window')
    injCoincTimeWindow = [5.0 0.1];
else
    % ---- Default coincidence window.
    disp('Working with -0.1 +0.1 window')
    injCoincTimeWindow = [0.1 0.1];
end

onSourceTimeOffset = 0;

firstFile = 1;
uniqIdx = 1;

% ---- Loop over injFiles.
for iFile = 1:length(injFiles)

    % ---- Load noise
    fprintf(1,'Noise file: ')
    fprintf(1,'%s\n',injFiles(1).name)
    noise = load([injDir '/' injFiles(1).name]);

    % ---- Load injFile
    fprintf(1,'Injection file: ')
    fprintf(1,'%s\n',injFiles(iFile).name)
    inj = load([injDir '/' injFiles(iFile).name]);

    outputfile = [injDir '/cleaned-' injFiles(iFile).name]

    % ---- Number of injections. 
    Ninj = length(inj.clusterInj);
    if verbose 
        disp('Discarding the following triggers:');
    end

    % ---- Loop over injections.
    for injIdx = 1:Ninj 

        % ---- peakTime of injection.
        injPeakTime = inj.peakTime(injIdx);

        % ---- Check injection produces triggers
        if ~isempty(inj.clusterInj(injIdx).boundingBox)

            % ---- Bounding box of injection in the time-frequency plane.
            injectionBox = [...
                (injPeakTime-injCoincTimeWindow(1)-onSourceTimeOffset), ...
                inj.minimumFrequency, ...
                sum(injCoincTimeWindow), ...
                inj.maximumFrequency-inj.minimumFrequency];

            % ---- Flag triggers which overlap with the injectionBox.
            keepIdx = find(rectint(injectionBox,inj.clusterInj(injIdx).boundingBox)>0);
            data = xclustersubset(inj.clusterInj(injIdx), keepIdx);

            % ---- Cut injections using smallest injection scale as sample of noise.
            if length(data.significance)
                badIdx = [];
                keepIdx = 1:length(data.significance);
                if length(noise.clusterInj(injIdx).significance)
                    badIdx = find(sum(rectint(data.boundingBox, noise.clusterInj(injIdx).boundingBox),2)>0);
                    keepIdx = setdiff(1:length(data.significance), badIdx);
                end
                if length(badIdx)
                    if verbose
                        disp([num2str([injIdx,badIdx(:)']) ' (' num2str(length(data.significance)) ')']);
                    end
                    data = xclustersubset(inj.clusterInj(injIdx), keepIdx);
                end
            end

            % ---- Keep only loudest trigger for each injection at each injScale.
            if length(keepIdx)>1
                [s,triggerIdx] = max(data.significance);
                data = xclustersubset(data, triggerIdx);
            else
                triggerIdx = 1;
            end

            % ---- Only write data out if we have a surviving trigger.
            % ------------------------------------------------------------???
            % ---- Write to mat file instead
            % ------------------------------------------------------------???
            if 0 %length(keepIdx)
                disp('boo!');
                fprintf(1,'%10.2f ', data.significance);
                for iLike = 1:length(inj.likelihoodType)
                    fprintf(1,'%10.2f ', data.likelihood(:,iLike));
                end
                fprintf(1,'%d ', data.jobNumber);
                fprintf(1,'%d ', data.nPixels);
                fprintf(1,'%10.2f ', data.peakTime);
                fprintf(1,'%10.2f ', data.peakFrequency);
                fprintf(1,'%10.2f ', data.boundingBox(:,1));
                fprintf(1,'%10.2f ', data.boundingBox(:,2));
                fprintf(1,'%10.2f ', data.boundingBox(:,3));
                fprintf(1,'%10.2f ', data.boundingBox(:,4));
                fprintf(1,'%d ', injIdx);
                fprintf(1,'%d ', keepIdx(triggerIdx));
                fprintf(1,'%d ', uniqIdx);
                uniqIdx = uniqIdx + 1;
                fprintf(1,'%s \n', injFiles(iFile).name);
                disp('boo-boo!');
            end % -- length(keepIdx)

            inj.clusterInj(injIdx) = data;
            clear data, keepIdx;
        end % --- check injection produces triggers
    end % -- Loop over injections.

    % inj.analysisTimesCell
    save(outputfile, '-struct', 'inj')

    clear inj
end % -- Loop over injFiles

fprintf(1,'Done! \n')
