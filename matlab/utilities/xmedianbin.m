function [medX, medY] = xmedianbin(X,Y,minX,maxX,binWidth)
% XMEDIANBIN - bin (x,y) data and compute median x,y in each bin.
%
% usage:
%
%   [medX, medY] = xmedianbin(X,Y,minX,maxX,binWidth)
%
%   X         Vector.
%   Y         Vector.  Must be same length as X.
%   minX      Optional scalar.
%   maxX      Optional scalar.
%   binWidth  Optional scalar.
%
%   medX      Vector of median X values in each bin.
%   medY      Vector of median Y values in each bin.
%
% xmedianbin bins the X data into equally spaced bins.  For each bin it
% computes the median of all (X,Y) points that fall into the bin.  
%
% The binWidth variable specifies the bin width, with the first bin
% beginning at minX and the last bin not extending beyond maxX; i.e., the
% bin edges are [minX:binWidth:maxX].  If not specified then minX = min(X),
% maxX = max(X), and binWidth = 0.2.
%
% $Id$

% ---- Checks.
error(nargchk(4, 5, nargin));
if (nargin<5)
    % ---- Default bin width.
    binWidth = 0.2;
end
if (nargin<4)
    % ---- Maximum bin edge.
    maxX = max(X);
end
if (nargin<3)
    % ---- Minimum bin edge.
    minX = min(X);
end
if (~isscalar(minX) | ~isscalar(maxX) | ~isscalar(binWidth))
    error('Input arguments minX, maxX, binWidth must be scalars.');
end
if (~isvector(X) | ~isvector(Y))
    error('Input arguments X, Y must be vectors.');
end
if (length(X)~=length(Y))
    error('Input vectors X, Y must have the same length.');
end
if binWidth > (maxX-minX)
    error('binWidth must be less than range of X values.');
end

% ---- Create vector of bin edges.
bin = [minX:binWidth:maxX]';

% ---- Prepare storage for outout.
medX = zeros(length(bin)-1,1);
medY = zeros(length(bin)-1,1);

% ---- Loop over bins and compute medians.
for jj = 1:length(bin)-1
    kk = find(X>=bin(jj) & X<bin(jj+1));
    medX(jj) = median(X(kk));
    medY(jj) = median(Y(kk));
end

% ---- Check medX and medY for NaNs.
% ---- It is simpler to remove vector elements outside of the
%      for loop above.
if sum(isnan(medX))==length(medX)
    error('All medX are NaN!');
elseif sum(isnan(medX))
    warning('medX contains NaNs! We will remove these before continuing');
    nanIdx = isnan(medX);
    medX(nanIdx)=[]; 
    medY(nanIdx)=[]; 
end

% ---- Check medX and medY for NaNs.
if sum(isnan(medY))==length(medY)
    error('All medY are NaN!');
elseif sum(isnan(medY))
    warning('medY contains NaNs! We will remove these before continuing');
    nanIdx = isnan(medY);
    medX(nanIdx)=[]; 
    medY(nanIdx)=[]; 
end

% ---- Done.
return

