function h = plotskymap(map,projection)
% PLOTSKYMAP - plot sky map with some automatic formatting
%
% usage
%
%   map            Struct. 'map' field of a struct produced by LOADSKYMAPFILE.
%   projection     Optional string. Defines projection used for the plot. Must
%                  be one of 'rectangular' (default) or 'mollweid'. 
%
%   h              Figure handle.
%
% PLOTSKYMAP plots the sky map contained in the map input using the SCATTER
% function and some default formatting.
%
% $Id$

% ---- Checks.
narginchk(1,2)

% ---- Defaults.
if nargin<2
    projection = 'rectangular';
end

% ---- Create plot.
h = figure;

switch lower(projection)
    
    case 'mollweid'

        % ---- Is there a nicer colormap than this one?
        colormap('cool')

        % ---- Create a map struct. This code is lifted from xproject.m.
        mapstruct = defaultm(projection);
        % mapstruct.angleunits = 'radians'; %-- default is degrees
        mapstruct.origin = [0 0 0];
        mapstruct.falseeasting = 0;
        mapstruct.falsenorthing = 0;
        mapstruct.scalefactor = 1;

        % ---- Trickery to plot ra from 0->360 when mapping toolbox wants to use
        %      longitude from -180->180.
        map.ra(map.ra<0) = map.ra(map.ra<0)+360;
        lat = map.dec;
        lon = map.ra;
        lon(lon<=0) = lon(lon<=0)+180;
        lon(lon>0) = lon(lon>0)-180;
        [x, y] = mfwdtran(mapstruct,lat, lon); %-- trickery
        % [x, y] = mfwdtran(mapstruct, map.dec, map.ra);
        z = zeros(size(x));

        % ---- Display projection.
        axis off
        cla;
        triangulation = delaunay(x, y);
        handle = trisurf(triangulation, x, y, z, map.probDensity);
        shading interp;
        view([0 0 1]);
        axis image;
        axesm('MapProjection','mollweid','MapLatLimit',[-90 90],'Gcolor','black', ...
            'GLineWidth',0.5,'MLineLocation',[-180:30:180],'PLineLocation',30) ;
        gridm('on');
        c=colorbar('location','southoutside','box','on','color',[0 0 0]);
        c.Label.String='probability density (str^{-1})';
        % c.Limits=[0 500];
        % c.Ticks=0:50:500;
        % c.FontSize=12;
        set(gca','xdir','reverse')
        set(gca,'fontsize',16)
        % xlabel('right ascension [deg]')
        % ylabel('declination [deg]')
        pos = get(gcf,'position');
        set(gcf,'position',[pos(1:2) 1000 500])
        for lon=-150:30:150
            textm(-5,lon+5,[num2str(lon+180) '^o'],'color','black','fontsize',12); %-- trickery
            % textm(-5,lon+5,[num2str(lon) '^o'],'color','black','fontsize',12);
        end
        for lat=-60:30:60
            textm(lat,-180,[num2str(lat) '^o'],'color','black','fontsize',12);
            textm(lat,180,[num2str(lat) '^o'],'color','black','fontsize',12);
        end
        axis off %-- removes box around plot (and xlabel, ylabel)

        % % ---- Cool example code for doing lots of formatting:
        % figure(2); clf(2);
        % load moonalb % a 540x1080 matrix of values is loaded along with moonalbrefvec=[3;90;0]
        % moonalbrefvec(1)=3; % this is the subdivisions per degree - thus 180*3=540 & 3*360=1080
        % moonalbrefvec(2)=90; % NW lat value
        % moonalbrefvec(3)=180; % NW long value
        % mymap=colormap('jet'); mymap(1,1:3)=1;colormap(mymap);
        % axesm('MapProjection','mollweid','MapLatLimit',[-90 90],'Gcolor','white',...
        %     'GLineWidth',2.0,'MLineLocation',[-135 -90 -45 0 45 90 135],'PLineLocation',30) ; 
        % axis off;caxis([0 500]); 
        % grid on;
        % plabel('LabelFormat','none');
        % mlabel('on');
        % parallel='equator';
        % mlabel(parallel);
        % mlabel('FontColor','white');
        % mlabel('off');
        % gridm('on');
        % geoshow(moonalb, moonalbrefvec, 'DisplayType', 'texturemap');
        % plabel('off')
        % c=colorbar('location','southoutside','box','on','color',[0 0 0]);
        % c.Label.String='T_{sky} (K)';
        % c.Limits=[0 500];
        % c.Ticks=0:50:500;
        % c.FontSize=12;
        % textm(-5,-135,'3 h','color','white','fontsize',25);
        % textm(-5,-90,'6 h','color','white','fontsize',25);
        % textm(-5,-45,'9 h','color','white','fontsize',25);
        % textm(-5,0,'12 h','color','white','fontsize',25);
        % textm(-5,45,'15 h','color','white','fontsize',25);
        % textm(-5,90,'18 h','color','white','fontsize',25);
        % textm(-5,90,'18 h','color','white','fontsize',25);
        % textm(-5,135,'21 h','color','white','fontsize',25);

        
    case 'rectangular'
        
        % ---- Sort map so that high-resolution pixels are plotted after low-resolution pixels.
        [~,I] = sort(map.order);
        scatter(mod(map.ra(I),360),map.dec(I),[],map.probDensity(I),'filled');
        hold on

        % ---- Format plot.
        box(gca,'on');
        grid on
        colorbar
        set(gca','xdir','reverse')
        set(gca,'fontsize',16)
        xlabel('right ascension [deg]')
        ylabel('declination [deg]')
        pos = get(gcf,'position');
        set(gcf,'position',[pos(1:2) 1000 500])
        axis equal
        set(gca,'YTick',[-90:30:90])
        set(gca,'XTick',[0:30:360])
        axis([0 360 -90 90])
        
    otherwise
        
        close(h)
        error(['Projection type ' projection ' not recognised.'])

end

% ---- Done.
return

