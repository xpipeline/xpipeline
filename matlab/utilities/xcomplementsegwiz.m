function [] = xcomplementsegwiz(inputSegFile,outputSegFile)
% XCOMPLEMENTSEGWIZ - generate the complement of a segwizard segment. 
%
% usage:
%
%  xcomplementsegwiz(inputSegFile,outputSegFile)
%
%  inputSegFile   String. Name of a segwizard file.
%  outputSegFile  String. Name of output file.
%
% xcomplementsegwiz generates the complement of a segwizard segment file. 
% The output is in segwizard format; i.e, 4 columns of ASCII:
%     segment number (dummy value in outputSegFile)
%     gpsStart second of segment 
%     gpsEnd second of segment 
%     duration in seconds of segment 
% segwizard is available from the LIGOtools segments package.
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% open and read in input file
fsegin = fopen(inputSegFile,'r');
inData = textscan(fsegin, '%f %f %f %f','CommentStyle','#');
fclose(fsegin);

% fill segIn structure
segIn.segNumber  = inData{1};
segIn.gpsStart   = inData{2};
segIn.gpsEnd     = inData{3};
segIn.duration   = inData{4};

% choose start and end of complementary segment list
% to lie in the distant past and distant future
startTime = 0;
endTime   = 999999999;

% generate complement of segIn
temp = ComplementSegmentList(segIn.gpsStart,segIn.duration,startTime,endTime);

% fill segOut structure with complement of segIn
segOut.gpsStart = temp(:,1);
segOut.duration = temp(:,2);
segOut.gpsEnd = segOut.gpsStart + segOut.duration;
segOut.segNumber = 1:length(segOut.gpsStart);

% write out segOut to output file
fsegout = fopen(outputSegFile,'w');
for idx = 1:length(segOut.gpsStart)
    fprintf(fsegout,'%10d %10d %10d %10d\n',...
              segOut.segNumber(idx),...
              segOut.gpsStart(idx),...
              segOut.gpsEnd(idx),...
              segOut.duration(idx));
end
fclose(fsegout);

return;
