function [ra_search_deg,dec_search_deg,probabilities]=...
   xchoosetimedelays(ra_ctr_deg,dec_ctr_deg,gps,...
   sites,delay_tol,outputfile,verbose)
% XCHOOSESKYLOCATIONS - tile the sky along line of greatest time delay to cover a GRB error box
%
% XCHOOSESKYLOCATIONS - given a source (e.g. GRB) estimated sky location
% and error in sky location measurement, generate a list of sky locations
% which we should search over to keep time-delay errors below a given
% threshold.
%
% usage:
%
% [ra_search_deg,dec_search_deg,probabilities] = ...
%     xchooseskylocations1(ra_ctr_deg,dec_ctr_deg,gps,skyPosSigma_deg, ...
%     nSigma,sites,delay_tol,outputfile,verbose)
%
%  ra_ctr_deg        String. RA coord of centre of GRB sky pos error circle [deg].   
%  dec_ctr_deg       String. Dec coord of centre of GRB sky pos error circle [deg].    
%  gps               String. GPS time of GRB.
%  sites             String. Tilde-separated list of detector sites, 
%                    e.g., 'H~L~V'.
%  delay_tol         String. Maximum error in time delay [sec] allowed
%                    between any sky position in the error box and the
%                    closest grid point.  
%  outputfile        String, optional.  Name of output file containing
%                    grid. The file will contain four columns:
%                    1. theta   Polar angle [rad] in the range [0, pi] with 
%                       0 at the North Pole/ z axis.
%                    2. phi     Azimuthal angle [rad]in the range [-pi, pi)  
%                       with 0 at Greenwich / x axis.
%                    3. pOmega  A priori probability associated with each 
%                       sky position.
%                    4. dOmega  Solid angle associated with each sky position.
%                    This file follows the format of the skyPositions array
%                    as described in sinusoidalMap.m. Specify 'None' for no
%                    file to be produced.
%  verbose           String, optional.  If 1, plots are made.  Default 0.
%
%  ra_search_deg     Right ascensions [deg] of search grid points.    
%  dec_search_deg    Declinations [deg] of search grid points.
%  probabilities     Gaussian probability of each sky position. 
%
% Example:
%   [ra,dec,prob] = xchooseskylocations1('1','89','874780000', ...
%       'H1~L1','5e-5','skyPos.txt','1')  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input args
error(nargchk(5, 7, nargin));

% ---- Convert input variables to appropriate type.
ra_ctr_deg      = str2num(ra_ctr_deg);
dec_ctr_deg     = str2num(dec_ctr_deg);
gps             = str2num(gps);
sites           = tildedelimstr2numorcell(sites); 
delay_tol       = str2num(delay_tol);

if (nargin<7)
    verbose = 0;
else
    verbose = str2num(verbose);
end

if (nargin<6)
   outputfile = 'None';
end

if length(sites)~=2;
    error('Only suitable for calculation with two sites')
end

% ---- Speed of light (in m/s).
speedOfLight = 299792458;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Get cartesian Earth-based coordinates (m) of detector's 
%                         vertex for each site.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nSites = length(sites);
for iSite = 1:nSites
    det       = LoadDetectorData(sites{iSite}(1));
    siteVertex(iSite,:) = det.V';
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Construct vector joining each pair of sites.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

iPair = 1; 
iSite1 = 1;
for iSite1 = 1:nSites-1
    for iSite2 = iSite1+1:nSites
       % ---- Construct structure listing names and displacements between
       %      pairs of sites.
       pairNames{iPair} = [sites{iSite1} sites{iSite2}];
       site1ToSite2(iPair,:) = siteVertex(iSite2,:) -  siteVertex(iSite1,:);
       % ---- Baseline in units of seconds.
       site1ToSite2_sec(iPair,:) = site1ToSite2(iPair,:) ./ speedOfLight;
       iPair = iPair + 1;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Construct vector representing direction to GRB in Earth-centered coordinates.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Convert trigger ra,dec to phi,theta.
[phi_ctr_rad, theta_ctr_rad] = radectoearth(ra_ctr_deg,dec_ctr_deg,gps);

% ---- Construct the sky direction assumed from skyPosition_central. (Note that
% ---- quantity is a unit vector.)

earthToGRB = [sin(theta_ctr_rad).*cos(phi_ctr_rad)... % x
              sin(theta_ctr_rad).*sin(phi_ctr_rad)... % y
              cos(theta_ctr_rad)];                    % z

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         For each pair of sites calculate angle with GRB line of sight.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% First determine Number of Grid Points
delay_max = norm(site1ToSite2_sec);
numGridPoints =  ceil(delay_max / delay_tol) +1;

% Use the baseline of the detector pair that requires most
% conservative spacing of sky positions.

iPair = 1;
n = norm(site1ToSite2_sec(iPair,:));
v1 = site1ToSite2_sec(iPair,:)/n;
v2 = earthToGRB;

A = cross(v2,v1);

%%% Starting at the GRB sky position, add new sky positions by
%%% rotating vector along axis of greatest delay sensitivity

%%% First go in positive direction, starting from GRB

%%% Initialize first position at GRB location (note v2 is a unit vector)

skyPositions(1,:) = -v1;


for ii=2:numGridPoints

    if ii<numGridPoints
        theta_i = acos(1-(ii-1)*(2*delay_tol/delay_max));
    elseif ii==numGridPoints
        theta_i = pi;
    end
    if ~isreal(theta_i)
        ii
        theta_i
        error('feh');
    end
    skyPositions(ii,:) = RotateVector(skyPositions(1,:), A, theta_i);       
end


% save Cartesian coordinates for later
skyPositions_good = skyPositions;

% ---- Convert skyPositions from Cartesian coords to spherical coords.
theta = acos(skyPositions(:,3));
phi = atan2(skyPositions(:,2),skyPositions(:,1));

skyPositions(:,1) = theta;
skyPositions(:,2) = phi;

% ---- Some output, if desired.
if verbose
    fprintf(1,'We placed %d sky positions \n', size(skyPositions,1));
    fprintf(1,'Time delay between sky positions is %2.5f \n', 2*delay_tol);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Calculate probability of sky positions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- We assume that the probabilities of sky positions follow a 
%      normal distribution and fall of as the opening angle with the
%      central GRB positions increases.
% ---- We will rotate so that the North Pole on the skyPositions map
%      will point at the GRB. In the non-rotated skyPositions map
%      this enables us to use the polar angle theta as the opening
%      angle between a given skyPosition and the GRB.
% ---- The skyPosSigma is provided as the 68% containment radius. There is a
%      0.66 factor to convert it into the parameter sigma of the 2D gaussian
%      distribution of sky locations.
% skyPosSigma_rad = skyPosSigma_deg * (pi/180) * 0.66;
% theta_GRB = acos(earthToGRB(1,3));
% probabilities = normpdf(theta,theta_GRB,skyPosSigma_rad);
% % ---- Normalise probabilities to 1 at best sky position
% probabilities = probabilities / normpdf(theta_GRB,theta_GRB,skyPosSigma_rad);
% % ---- Replace the prob column in skyPositions array.
skyPositions(:,3) = 1.000000;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %                   Calculate solid angle of each sky position.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% dOmega will be sin(theta) dphi dtheta or something
% else if dphi is undefined for the 1-D grid
%

thetaRange = size(theta)

for i=1:thetaRange,

    if i==numGridPoints
        d_theta = theta(2)-theta(1);
    else
        d_theta = theta(i+1)-theta(i);
    end
    
    % ---- Determine differential solid angle.
    if theta(i) == 0 | theta(i) == pi
        dOmega = pi * (d_theta / 2).^2;
    else
%        dOmega = pi * sin(theta(i)) * d_theta;
        dOmega = sin(theta(i)) * d_theta.^2;
    end
 
end

skyPositions(:,4) = dOmega;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   Finished; plots for debugging.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V1 = CartesianPointingVector(phi,theta);
baseline = site1ToSite2_sec(iPair,:);

n = norm(baseline);

V2 = baseline/n;

if verbose

    figure(1); 
    plot3(V1(:,1),V1(:,2),V1(:,3),'r+','MarkerSize',8); 
    hold on;
    set(gca,'FontSize',14);
    line([0 V2(:,1)/(2*n)],[0 V2(:,2)/(2*n)],[0 V2(:,3)/(2*n)],'Color','k','Linewidth',2);
    line([0 earthToGRB(:,1)],[0 earthToGRB(:,2)],[0 earthToGRB(:,3)],'Color','b','LineWidth',2);
    plot3(earthToGRB(:,1),earthToGRB(:,2),earthToGRB(:,3),'gp','MarkerSize',20)
    grid;
    axis([-1 1 -1 1 -1 1]);
    xlabel('x');
    ylabel('y');
    zlabel('z');
    legend('Sky Positions','Detector Baseline Vector','Vector to GRB','GRB Position','Location','East');
    title('GRB Position and 1-Dimensional Grid of Sky Locations');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Convert back to ra,dec.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ra_search_deg, dec_search_deg] = earthtoradec(phi,theta,gps);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Write output file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~strcmp(outputfile,'None')
    dlmwrite(outputfile,skyPositions,'delimiter',' ','precision','%7.5f');
end