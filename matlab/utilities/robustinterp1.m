function YI = robustinterp1(X,Y,XI,METHOD)
% ROBUSTINTERP1 - wrapper to INTERP1 with checks on data for robustness
%
%   YI = robustinterp1(X,Y,XI,METHOD)
%
%   X       Vector of values of independent variable "x".
%   Y       Vector of values of dependent variable "y".  Must be of same length
%           as X.
%   XI      Vector of values of x at which y value is desired.
%   METHOD  Optional string specifying interpolation method.  Must be recognized
%           by INTERP1.  If not specified, default method of INTERP1 is used.
%
%   YI      Interpolated values of y at XI. 
%
% The vectors X,Y must have the same length but need not be the same size 
% (row/column vectors).  YI will be of the same size as XI.
%
% ROBUSTINTERP1 uses INTERP1 to estimate YI = Y(XI), but also applies checks
% to the data to avoid failures common to INTERP1.  ROBUSTINTERP1 ignores (X,Y) 
% pairs for which X or Y are Inf or NaN, and it discards (X,Y) pairs with 
% non-unique X values.  (For repeated X, the last entry is kept.)  The function
% returns NaN for any XI at which the interpolation fails.
%
% $Id$

% ---- Check input arguments.
% ---- Check for correct number of inputs.
error(nargchk(3,4,nargin));
% ---- Check for vector inputs.
if ~(isvector(X) && isvector(Y) && isvector(XI))
    error('Input arguments X,Y,XI must be scalars or vectors.')
end
% ---- Verify input X,Y have the same length.
if (length(X) ~= length(Y))
    error('Input arguments X,Y must have the same length.')
end

% ---- Force X,Y to be column vectors.
X = X(:);
Y = Y(:);

% ---- Drop any (X,Y) pairs containing NaNs/Infs. 
badIndex = find(isnan(X) | isnan(Y) | isinf(X) | isinf(Y));
X(badIndex) = [];
Y(badIndex) = [];

% ---- Keep only (X,Y) pairs with unique X values (required for interpolation).
%      For repeated X, the last entry is kept by default.  This also sorts the 
%      data by increasing X.  
[B,I] = unique(X);  

% ---- Interpolate to X=XI.
if (length(I)>1)
    try
        if nargin==3
            YI = interp1(X(I),Y(I),XI);
        else
            YI = interp1(X(I),Y(I),XI,METHOD);
        end
        if any(isinf(YI))
            badIndex = find(isinf(YI));
            YI(badIndex) = NaN;
        end
    catch
        % ---- Interpolation failed.  Return NaNs.
        YI = nan(size(XI));
    end
else
    % ---- Interpolation impossible.  Return NaNs.
    YI = nan(size(XI));
end

% ---- Done.
return

