function erg = msun2erg(msun)
% MSUN2ERG - Convert energy in solar masses to erg.
%
%   erg = msun2erg(msun)
%
% msun  Array.  Energy in units of solar masses. 
% erg   Array.  Energy in units of erg.  
%
% MSUN2ERG converts an energy in solar masses to erg.  The conversion
% formula is 
%
%  erg = msun * solarMass * speedOfLight^2 * 1e7;
%      = msun * 1.7871e+54

% ---- Check number of input arguments.
error(nargchk(1, 1, nargin));

% ---- Needed constants.
speedOfLight = 299792458;  %-- m/s
NewtonG = 6.67e-11;        %-- Newton's G (Nm^2/kg^2=m^3/kg/s^2).
secondsPerYear = 365.25*86400;              %-- s
parsec = 3.26*speedOfLight*secondsPerYear;  %-- m
solarMass = 1.988435e30;   %-- kg
ergPerJoule = 1e7; 

% ---- Convert to erg.
erg = msun * solarMass * speedOfLight^2 * ergPerJoule;

% ---- Done.
return
