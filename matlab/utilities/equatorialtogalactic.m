function [ell,b] = equatorialtogalactic(alpha,delta)
% EQUATORIALTOGALACTIC: Convert from equatorial to galactic sky coordinates.
%  
%   [ell,b] = equatorialtogalactic(alpha,delta)
%
% alpha     Vector. Right ascension in equatorial (sky-based) coordinates (rad).
% delta     Vector. Declination in equatorial coordinates (rad).
%
% ell       Galactic longitude (rad), from [0,2*pi).
% b         Galactic latitude (rad), from [-pi/2,pi/2].
%
% For definitions and discussion of the equatorial and galactic 
% cooridnate systems, see the LAL Software Document, Section 16.7, 
% "SkyCoordinates.h"
%
% -- first version: Patrick J. Sutton 2004.07.29

% ---- Constants defining galactic coordinate system.
%      See LAL Software Document, Section 16.7, "SkyCoordinates.h"
alpha_NGP = 192.8594813*pi/180;
delta_NGP = 27.1282511*pi/180;
ell_ascend = 33*pi/180;

% ---- Transformation from equatorial to galactic coordinates.
% ---- latitude
b = asin( cos(delta_NGP)*cos(delta).*cos(alpha-alpha_NGP) + sin(delta)*sin(delta_NGP) );
% ---- longitude, wrapped to [0,2*pi)
ell = atan2( sin(delta)*cos(delta_NGP) - cos(delta).*cos(alpha-alpha_NGP)*sin(delta_NGP) , cos(delta).*sin(alpha-alpha_NGP) ) + ell_ascend;
ell = mod(ell,2*pi);

% ---- Done
return
