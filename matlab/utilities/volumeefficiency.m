function volEff = volumeefficiency(injScale,effCurve,nominalDist,volumeRadius,mode)
% compute detection efficiency assuming uniform distribution within a
% given volume
%
%   injScale     Vector. injection scales at which detection efficiency
%                curve is sampled
%   effCurve     Vector. Detection efficiency curve.
%   nominalDist  Scalar. Nominal distance to which injection scale = 1
%                corresponds.
%   volumeRadius Scalar. Radius of volume within which potential sources
%                are distributed.
%   mode         String. Which universe geometry to use, see dist2z
%                    'linear' - Euclidian geometry
%                    'flat' - flat cosmology
%
%    for Euclidian case the following is computed:
%  intStart = nominalDist/volumeRadius
%  volEff = int_{intStart}^\infty 3*intStart^3*effCurve/injScale^4 d(injScale)

intStart = nominalDist/volumeRadius;

if intStart > injScale(end)
  volEff = 0;
else
  % interpolate eff curve for better resolution
  addGrid = 5e-2:1e-2:0.9;
  for iPoint = 1:length(addGrid)
    addInjScale(iPoint) = upperbound(injScale,effCurve, ...
                                          addGrid(iPoint));
  end
  startScale = log10(1.01*max(intStart,injScale(1)));
  endScale = log10(injScale(end)*0.99);
  if startScale < endScale
    moreScales = logspace(startScale,endScale,1000);
  else
    moreScales = [];
  end
  addInjScale(iPoint+(1:length(moreScales))) = moreScales;
  addGrid(iPoint+(1:length(moreScales))) = interp1(injScale,effCurve,moreScales);
  if intStart < injScale(1)*0.99
    smallScale = logspace(log10(intStart),log10(injScale(1)*0.99),100);
  else
    smallScale = [];
  end
  injScale = [injScale; addInjScale'; smallScale'];
  effCurve = [effCurve; addGrid'; zeros(size(smallScale'))];
  [injScale iSort] = sort(injScale);
  effCurve = effCurve(iSort);

  % check that spurious efficiency at low amplitude is not important
  effCurveSanity = effCurve;
  effCurveSanity(effCurveSanity<2e-2) = 0;

  if any(isnan(effCurve)) || any(isnan(injScale))
    warning('Problem, one of the points is nan');
  end

  % Get the total integration volume
  % intRange = logspace(log10(intStart),log10(intStart)+5,1000);
  % [z Jv] = dist2z(nominalDist./intRange, mode);
  % totVolume = trapz(intRange, Jv.*nominalDist./intRange.^2);
  
  % chop at integral start
  mask = injScale >= intStart;
  injScale = injScale(mask);
  effCurve = effCurve(mask);
  effCurveSanity = effCurveSanity(mask);
  [z Jv]= dist2z(nominalDist./injScale, mode);
  volEff = trapz(injScale, effCurve.*Jv.*nominalDist./injScale.^2,1)./...
           trapz(injScale, Jv.*nominalDist./injScale.^2,1);
  volEffSanity = trapz(injScale, effCurveSanity.*Jv.*nominalDist./injScale.^2,1)./...
      trapz(injScale, Jv.*nominalDist./injScale.^2,1);
           
  if length(injScale) == 1
    volEff = 0;
    volEffSanity = 0;
  end
  if volEff > 1.1
    warning(['Integretion yields absurdly high value: ' num2str(volEff)])
  end
  if abs(volEff-volEffSanity) > 2e-2
    warning(['Low efficiency part of curve is affecting the result: ' ...
             num2str(volEff) ' / ' num2str(volEffSanity)]);
  end
  if isnan(volEff)
    error('computed volume efficiency is nan!')
  end
  volEff = min(volEff,1);
end
