function [valid, rescale] = compareinjparams(At,Ap,Bt,Bp)
% COMPAREINJPARAMS - compare two sets of injection parameters 
%
% [valid, rescale] = compareinjparams(A,B)
%
% A,B      Cell arrays of injection parameters. Each element is of the form, eg, 
%          'chirplet!1.0e-21~0.003448~290~0~0~-1;1;linear'.
%
% valid = compareinjparams(At,Ap,Bt,Bp)
%
% At,Bt    Cell arrays of injection types. Each element is of the form, eg, 
%          'chirplet'.
% Ap,Bp    Cell arrays of injection parameters. Each element is of the form, eg, 
%          '1.0e-21~0.003448~290~0~0~-1;1;linear' corresponding to the matching
%          element of At or Bt. 
%
% valid    Boolean. True if each element A{i}==B{i} or At{i}==Bt{i} & Ap{i}==Bp{i}, 
%          except for differences in the amplitude or distance parameter (i.e.,
%          same injections at possibly different default amplitudes). False
%          otherwise. 
% rescale  Vector. Ratio of default amplitudes of injections in set B to set A.
%          Empty if sets have different sizes or types.
%
% COMPAREINJPARAMS ignores (only) differences in the amplitude parameter. 
%
% $Id$

% ---- Assign defaults.
rescale = [];

% ---- Check input arguments.
narginchk(2,4);
if nargin == 3
    error('Number of inputs must be 2 or 4; 3 supplied.');
end

% ---- Check input sizes.
if (length(At)~=length(Ap)) || (nargin == 4 && (length(At)~=length(Bt) || length(At)~=length(Bp)))
    error('Input cell arrays must all have the same size.')
    valid = false;
    return
end

% ---- If two inputs then split inputs into type and parameter arrays.
if nargin == 2
    % ---- Rename inputs to clarify their nature.
    A = At;
    B = Ap;
    clear At Ap
    for ii=1:length(A)
        % ---- Split parameter strings into waveform type and parameters
        %      (format: 'type!parameters')
        idx      = strfind(A{ii},'!');
        At{ii}   = A{ii}(1:idx-1);
        Ap{ii}   = A{ii}(idx+1:end);
        idx      = strfind(B{ii},'!');
        Bt{ii}   = B{ii}(1:idx-1);
        Bp{ii}   = B{ii}(idx+1:end);
    end
end

% ---- If any type differ then we exit immediately.
if prod(strcmp(At,Bt))==0
    valid = false;
    return
end
    
% ---- Otherwise check for differences in parameters.
idx = find(~strcmp(Ap,Bp));

% ---- Check for equality - if so, we are done.
if isempty(idx)
    valid = true;
    rescale = ones(size(Ap));
    return
end

% ---- We're still here? Then there must be some elements that differ. Loop over
%      them.
valid = true; %-- we'll change these upon finding a verified difference
rescale = ones(size(Ap));
for ii=1:length(idx)
    % ---- Determine which parameter controls amplitude. 
    [ampl_idx,freq_idx] = xmakewaveform(At{idx(ii)});
    % ---- Discard amplitude parameter for set 'a'.
    params = Ap{idx(ii)};  % -- shorthand
    params = tildedelimstr2numorcell(params);  %-- convert to array
    % ---- KLUDGE: for inspirals reset ampl_idx to last parameter to handle the
    %      case where the mass parameters have been randomised. 
    if strcmp(At{idx(ii)},'inspiral')
        ampl_idx = length(params);
    end
    ampl_A = params{ampl_idx};  %-- record amplitude
    params = params(setdiff(1:length(params),ampl_idx));  %-- drop amplitude element
    params_A = numorcell2tildedelimstr(params);  %-- convert back to string
    % ---- Repeat for set 'b'. Note that ampl_idx, freq_idx are the same as for A.
    params = Bp{idx(ii)};  % -- shorthand
    params = tildedelimstr2numorcell(params);  %-- convert to array
    ampl_B = params{ampl_idx};  %-- record amplitude
    params = params(setdiff(1:length(params),ampl_idx));  %-- drop amplitude element
    params_B = numorcell2tildedelimstr(params);  %-- convert back to string
    % ---- Compare.
    rescale(idx(ii)) = ampl_B/ampl_A;
    if ~strcmp(params_A,params_B)
        disp(['Discrepancy in parameters for injection set ' num2str(idx(ii)) ':'])
        disp([At{idx(ii)} '!' Ap{idx(ii)}])
        disp([Bt{idx(ii)} '!' Bp{idx(ii)}])
        valid = false;
    end
end

return
