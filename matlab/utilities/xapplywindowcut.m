function [pass, pass_bool] = xapplywindowcut(window,triggers,verbose)
% XAPPLYWINDOWCUT - Veto events lying outside specified on-source window.
% 
% The xapplywindowcut function passes any cluster which lies within a
% specified window about the center time of the block in which it occurred.
% Any cluster falling outside of this window fails the cut.
% 
% usage: 
% 
%   [pass, pass_bool] = xapplywindowcut(window,triggers,verbose) 
%
%   window      Structure with the following fields:
%                 window.offset - Positive scalar. Number of seconds before
%                   centerTime that the window begins.
%                 window.duration - Positive scalar. Duration of window in
%                   seconds.
%   triggers    Structure containing onSource, offSource, or injection
%               triggers. Must contain the standard X-Pipeline fields 
%               'boundingBox' and 'centerTime'.
%   verbose     Optional logical. If 1 (true), prints message about the 
%               number of triggers passed. Default 0 (false).
%
%   pass        Vector.  A value of 1 (0) means the corresponding event 
%               passed (failed) the cut.
%   pass_bool   Logical vector corresponding to pass.
%
% $Id$

% ---- Check input arguments.
narginchk(2,3);
if ~isfield(window,'offset') || ~isfield(window,'duration')
    error('Input struct window must have fields offset and duration')
end
if ~isfield(triggers,'boundingBox') || ~isfield(triggers,'centerTime') 
    error('Input struct triggers must have fields boundingBox and centerTime')
end
if nargin == 3 && (~islogical(verbose) || numel(verbose)~=1)
    error('Optional input verbose must be a logical scalar.');
end

% ---- Find all triggers that intersect window. Intersection at a single
%      point is sufficient to pass, e.g. if 
%        trigger end time = window start time
%      or 
%        trigger start time = window end time
%      then the pass flag will be set to unity for this trigger.
pass_bool = (triggers.boundingBox(:,1) <= triggers.centerTime - window.offset + window.duration) & ...
            (triggers.boundingBox(:,1) + triggers.boundingBox(:,3) >= triggers.centerTime - window.offset) ;
% ---- For historical reasons, convert the pass flag from a boolean to a
%      double.
pass = double(pass_bool);

% ---- Optional verbosity.
if nargin == 3 && verbose
    fprintf(1,'In total, keeping %d out of %d clusters  \n', ...
        sum(pass),length(pass));
end

% -- Done
return
