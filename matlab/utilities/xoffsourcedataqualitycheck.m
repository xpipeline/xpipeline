function [pass,deadtime,uniqueJobNumbers] = xoffsourcedataqualitycheck(...
    analysis,offSource,vetoSegs,verbosity)
% XOFFSOURCEDATAQUALITYCHECK - check data quality for grb analysis segments. 
%   
%  usage: 
%  
%  [pass, deadtime, uniqueJobNum] = xoffsourcedataqualitycheck(analysis,...
%      offSource,vetoSegs,verbosity) 
%
%  analysis         Structure created by xmakegrbwebpage.
%  offSource        Structure containing offSource triggers. Must contain
%                   the fields jobNumber, centerTime, unslidCenterTime, and
%                   timeOffsets.
%  vetoSegs         Struct array containing lists of veto segments (i.e.,
%                   'bad' times) for each detector:
%                     vetoSegs(thisIfo).gpsStart - Vector of GPS start 
%                       times of veto segments.
%                     vetoSegs(thisIfo).duration - Vector of segment 
%                       durations, in seconds.      
%  verbosity        String, either 'verbose' or 'quiet'.
%
%  pass             Vector of size (Number of offSource jobs x 1). A value
%                   of 0 (1) indicates that the corresponding offSource job
%                   fails (meets) the data quality criteria.
%  deadtime         Vector of size (Number of offSource jobs x 1). Lists
%                   the total deadtime [sec] occurring within the on-source
%                   window around unslid centerTime of the corresponding
%                   offSource job.
%  uniqueJobNum     Vector of unique jobNumbers. Each element is the job
%                   number of the corresponding element of the pass and
%                   deadtime outputs. 
%  
% XOFFSOURCEDATAQUALITYCHECK extracts the list of unique job numbers from
% the input trigger list (which may contain on-source or off-source
% triggers). For each unique jobNumber, XOFFSOURCEDATAQUALITYCHECK checks
% the amount of deadtime in the corresponding on-source window, as
% determined by the analysis struct and the user-supplied veto segment
% lists. A given on-source window has pass flag 0 if:
%    * there is any veto deadtime in the [-5,+1]s interval about the unslid
%      centerTime in any detector; and/or 
%    * the total deadtime in the on-source window is >= 5% of the window.
%
% Note that grb.py defines the analysis segments so that there are no cat
% 0,1 flags raised within the on-source interval about the unslid
% centerTime.  
%
% WARNING: XOFFSOURCEDATAQUALITYCHECK is intended for GRB searches but not
% for all-sky searches. Specifically, this function assumes that all
% triggers in a given off-source trial have the same time lags. This is
% true for GRB searches but not necessarily for all-sky searches.
% (Furthermore, there is no logical basis for applying the function for an
% all-sky search as the same data [with time shifts] is used for both the
% on- and off-source analyses.)
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Preliminaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for valid number and type of inputs. 
error(nargchk(4, 4, nargin));

% ---- Check that verbosity is correctly defined.
if ~( strcmp(verbosity,'verbose') | ...
      strcmp(verbosity,'quiet') )
    error('verbosity must be either verbose or quiet')
end

if strcmp(verbosity,'verbose')
    verboseFlag = 1;
else
    verboseFlag = 0;
end

% ---- Extract unique list of job numbers from the supplied triggers.
[uniqueJobNumbers iJobToTrig] =  unique(offSource.jobNumber);
nJobFilesProcessed = length(uniqueJobNumbers);

% ---- Initially assume all offSource jobs pass veto segment cuts.
% ---- Note that unlike other pass flags this has one element
%      per offSource job (rather than one per trigger etc.).
pass = ones(nJobFilesProcessed,1);

% ---- Loop over offSource trials ("jobs"), each job represents a unique
%      choice of segment centerTime and timeOffsets that is common to all
%      triggers from that offSource trial (for GRB searches; this is not
%      necessarily the case for all-sky searches).
for iJob = 1:nJobFilesProcessed

    % ---- Identify current jobNumber.
    thisJobNumber = uniqueJobNumbers(iJob); 

    % ---- Find indices of all triggers with the current jobNumber. 
    jobIdx = iJobToTrig(iJob);

    % ---- All triggers with current jobNumber will have the same
    %      centerTime (for GRB searches), extract this from the first
    %      trigger with current jobNumber.
    centerTime = offSource.centerTime(jobIdx(1));
    if (verboseFlag == 1)
        fprintf(1,'\n');
        fprintf(1,'Working on offSource job %d with centerTime %d \n',...
        thisJobNumber,centerTime);
    end

    % ---- Loop over ifos we have analysed.
    for iIfo = 1:length(analysis.detectorList)

        % ---- Before applying vetoSeg cuts we must unslide the triggers.
        %      For each ifo, after unsliding, every trigger with the 
        %      current jobNumber will have the same unslid centerTime.
        %      Simply use the first trigger with the current jobNumber
        %      to identify the unslid centerTime.
        %      Note that unslidCenterTime is ifo dependent.
        unslidCenterTime = offSource.unslidCenterTime(jobIdx(1),iIfo);

        if (verboseFlag == 1)
            fprintf(1,'Working on ifo %s with time offset %d,\n',...
                analysis.detectorList{iIfo},...
                offSource.timeOffsets(jobIdx(1),iIfo));
            fprintf(1,['Unslid center time: %d, ' ...
                       'on source interval: [%d %d], '... 
                       '6s interval [%d %d] \n'],...
                unslidCenterTime,...
                unslidCenterTime+analysis.onSourceBeginOffset,...
                unslidCenterTime+analysis.onSourceEndOffset,...
                unslidCenterTime-5,unslidCenterTime+1);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %  Check for cat2 flags raised in [-5,+1]s interval about unslid 
        %                        centerTime
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Find intersections between vetos and our clusters.
        % ---- Passing the shorter list to Coincidence2 first
        %      speeds it up. 
        coincOut=Coincidence2(unslidCenterTime-5,6,...
                    vetoSegs(iIfo).gpsStart,...
                    vetoSegs(iIfo).duration); 

        % ---- If there were any coincidences between our interval and the
        %      vetoSegs we must discard all triggers from this job. We don't
        %      count coincidences with zero duration (edge overlap).
        if ~isempty(coincOut) && any(coincOut(:,2)>0)
            pass(iJob) = 0;
            if (verboseFlag == 1)
                fprintf(1,['Time killed in [-5,+1]s interval about center '...
                           'time \n']);
                fprintf(1,'startTime     stopTime\n'); 
                fprintf(1,'--------------------------\n'); 
                for iCoinc = 1:length(coincOut(:,1));
                    fprintf(1,'%9.2f  %9.2f \n',... 
                        coincOut(iCoinc,1), ...
                        coincOut(iCoinc,1) + coincOut(iCoinc,2)); 
                end 
                fprintf(1,['Setting pass to zero \n']);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %  Find deadtime in on-source interval about unslid centerTime 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        goodBefore = -analysis.onSourceBeginOffset;
        duration   = analysis.onSourceEndOffset-analysis.onSourceBeginOffset;

        % ---- Find intersections between vetos and our clusters.
        % ---- Passing the shorter list to Coincidence2 first
        %      speeds it up.
        coincOut=Coincidence2(unslidCenterTime-goodBefore,duration,...
                    vetoSegs(iIfo).gpsStart,...
                    vetoSegs(iIfo).duration);

        % ---- Record livetimes; i.e. times not killed by vetoes. Record in
        %      [start,duration] format.
        if isempty(coincOut)
            %killedTimes{iJob,iIfo} = [0,0];
            liveTimes{iJob,iIfo}   = [unslidCenterTime-goodBefore,duration];
        else
            %killedTimes{iJob,iIfo} = [coincOut(:,1),coincOut(:,2)];
            liveTimes{iJob,iIfo}   = ...
            ComplementSegmentList(coincOut(:,1),coincOut(:,2),...
                unslidCenterTime-goodBefore,unslidCenterTime-goodBefore+duration);
        end

        % ---- Resliding liveTimes in order to measure how much of
        %      deadtime in offSource analysis segment.
        %      To do this we must SUBTRACT timeOffsets 
        reslidLiveTimes{iJob,iIfo} = [liveTimes{iJob,iIfo}(:,1) - ...
            offSource.timeOffsets(jobIdx(1),iIfo), ...
            liveTimes{iJob,iIfo}(:,2)];

    end % -- Loop over ifos

    % ---- For current job, find intersection of reslid liveTimes from all
    %      ifos.
    intersectReslidLiveTimes{iJob} = reslidLiveTimes{iJob,1};
    for iIfo = 2:length(analysis.detectorList)
        coincOut = Coincidence2(intersectReslidLiveTimes{iJob}(:,1),...
                            intersectReslidLiveTimes{iJob}(:,2),...
                            reslidLiveTimes{iJob,iIfo}(:,1),...
                            reslidLiveTimes{iJob,iIfo}(:,2));

        if isempty(coincOut)
            intersectReslidLiveTimes{iJob} = [0,0];
        else  
            intersectReslidLiveTimes{iJob} = [coincOut(:,1), coincOut(:,2)];
        end
    end

    % ---- For current job, find time killed by veto flags.
    finalKilledTimes{iJob} = ComplementSegmentList(...
            intersectReslidLiveTimes{iJob}(:,1),...
            intersectReslidLiveTimes{iJob}(:,2),...
            centerTime-goodBefore,centerTime-goodBefore+duration);
    deadtime(iJob) = sum(finalKilledTimes{iJob}(:,2));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Discard jobs with >=5% deadtime in on-source interval 
    %                     about centerTime 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if (verboseFlag == 1)
        fprintf(1,['Total time killed in [' num2str(analysis.onSourceBeginOffset) ...
            ',' num2str(analysis.onSourceEndOffset) ']s interval about '...
            'centre time: %9.2f \n'],...
            deadtime(iJob));
        fprintf(1,'startTime     stopTime\n'); 
        fprintf(1,'--------------------------\n'); 

        for iCoinc = 1:length(finalKilledTimes{iJob}(:,1));
                fprintf(1,'%9.2f  %9.2f \n',...
                    finalKilledTimes{iJob}(iCoinc,1), ...
                    finalKilledTimes{iJob}(iCoinc,1) + ...
                    finalKilledTimes{iJob}(iCoinc,2));
        end  
    end

    % ---- Threshold on deadtime in on-source window.
    killedThresh = 0.05*duration;

    % ---- Check total duration of killed times
    if deadtime(iJob) >= killedThresh;
        pass(iJob) = 0;
        if (verboseFlag == 1)
            fprintf(1,'More than %ds killed, setting pass to zero \n',...
                killedThresh);
        end
    end  

end % -- Loop over offSource jobs

% ---- Optional verbosity.
if (verboseFlag == 1)
    fprintf(1,'\n');
    fprintf(1,'jobNumbers of offSource segments failing DQ criteria: \n');
    disp(uniqueJobNumbers(find(pass == 0)));
    fprintf(1,'\n');
end
