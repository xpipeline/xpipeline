function [trigger, unslidTime, unslidCenterTime] = xunslidtime(trigger,blockTime,transientTime)
% XUNSLIDTIME - compute the real GPS time of time-slid events
%
% usage:
%
%  [trigger, unslidTime, unslidCenterTime] = xunslidtime(trigger,blockTime,transientTime)
%
%  trigger           X-Pipeline event trigger struct. Must contain the fields 
%                    timeOffsets and circTimeSlides (these are supplied by 
%                    default by XDETECTION).
%  blockTime         Scalar. blockTime used in trigger generation (specified in the 
%                    .ini file).
%  transientTime     Scalar. transientTime used in trigger generation (equal to 
%                    4*whiteningTime where whiteningTime is specified in the .ini file)..
%
%  trigger           Copy of the input struct with the additional fields unslidTime
%                    and unslidCenterTime.
%  unslidTime        MxN array, where M is the number of events (rows) in trigger 
%                    and N is the number of detectors.  Each element (I,J) contains 
%                    the unslid peakTime of event I in detector J.
%  unslidCenterTime  Array of the same size as unslidTime.  Each element (I,J) 
%                    contains the unslid centerTime corresponding to event I in 
%                    detector J.
%
% $Id$

% ---- Check input for required fields.
if not ( isfield(trigger,'timeOffsets') & isfield(trigger,'circTimeSlides') )
    error('Input struct must have the fields timeOffsets and circTimeSlides.');
end

% ---- Initialise storage.
unslidTime       = zeros(size(trigger.timeOffsets));
unslidCenterTime = zeros(size(trigger.timeOffsets));

% ---- Loop over detectors, computing unslid time for each.
for thisIfo = 1:size(unslidTime,2)
    % ---- The circular time slides are shifts of data so opposite to time
    %      offsets which are shifts in read data, they also need to be
    %      unwrapped with reference to the start of the time frequency map
    unslidTime(:,thisIfo) = ...
        mod(trigger.peakTime - trigger.circTimeSlides(:,thisIfo) ...
            - (trigger.startTime + transientTime), ...
            blockTime - 2*transientTime) ...
        + trigger.startTime + transientTime ...
        + trigger.timeOffsets(:,thisIfo);
    unslidCenterTime(:,thisIfo) = trigger.centerTime(:) ...
        + trigger.timeOffsets(:,thisIfo);
end

% ---- Add these fields to the output struct.
trigger.unslidTime       = unslidTime;
trigger.unslidCenterTime = unslidCenterTime;

% ---- Done.
return
