function [] = xwriteinjasciifiles(injDir, injGlob, injNumbers, outputdir, threshold,  injClean, injScale_threshold, verbose)
% XWRITEINJASCIIFILES - convert injection matlab trigger files to ascii for MVA analysis
%
% XWRITEINJASCIIFILES writes the specified triggers from a set of injection 
% matlab files to ascii for use in TMVA analysis. The converted triggers 
% include significance, likelihoods, time-frequency information, and
% jobNumbers of the selected injection triggers. The output file is a space-
% delimited ascii file with root-formatted header information. 
% 
% usage:
%
%  xwriteinjasciifiles(injDir, injGlob, injNumbers, outputdir, injClean, verbose)
%
%  injDir           String. Full path to directory containing injection results
%                   files.
%  injGlob          String. We search injDir for files with the name 
%                   *<injGlob>*merged.mat.
%  injNumbers       Vector or string. injNumbers whose triggers are to be converted.
%  outputdir       String. Name of output directory.
%  threshold        Triggers with significance below the threshold are not used. 
%  injClean         Optional flag (scalar, string, or logical). If true/nonzero, 
%                   "clean" injection triggers as explained below.  Default false.
%  verbose          Optional flag (scalar, string, or logical). If true/nonzero, 
%                   get extra output messages. Default false.
%
% The injNumbers, injClean, and verbose inputs may be supplied as strings
% as understood by STR2NUM.
%
% For each injection (at each injection scale) we select the trigger with the 
% largest signifiance within +/-0.1 sec of the injection peakTime (within 
% [-10.0,+0.1] sec for 'inspiral', 'insp', 'inspiralsmooth', 'ninja', 
% 'lalinspiral' waveforms).
%
% When the injClean flag is set, the injection triggers set is "cleaned" before 
% being converted.  The triggers for the smallest injection scale are assumed to
% be due to noise.  Any trigger from any injection scale that has a non-zero 
% overlap with any noise trigger (overlap > 0 as measured by RECTINT for the
% bounding boxes) is discarded (not written to an ascii file).
%
% The meaning of each column in the output file is usually clear from the header
% information. However, the last few columns require explanation:
%
%       injIdx:  line in injection file to which trigger corresponds
%   triggerIdx:  index (row number) of trigger in the clusterInj struct for this 
%                injIdx. Recall that more than one trigger is typically recorded
%                per injection. This is different from the meaning of triggerIdx
%                for on- and off-source triggers!
%      uniqIdx:  1,...,N in the order they appear in the output txt file
%
% $Id$

% ---- Checks.
error(nargchk(4,7,nargin));
%narginchk(4, 6);
'xwriteinjascii new training set'

% ---- Assign defaults.
if nargin<8
    verbose = false;
end
if nargin<7
    injScale_threshold = 10;
end

if nargin<6
    injClean = false;
end

% ---- Convert string inputs to numbers.
if ischar(injNumbers)
    injNumbers = str2num(injNumbers);
end
if ischar(injClean)
    injClean = str2num(injClean);
end
if ischar(verbose)
    verbose = str2num(verbose);
end
if ischar(threshold)
    threshold=str2num(threshold);
end
if ischar(injScale_threshold)
    injScale_threshold=str2num(injScale_threshold);
end

% --- Load on file to get center time for initial seed
on_matFile  = sprintf('%s/on_source_0_0_merged.mat',injDir);
on_data  = load(on_matFile);

% --- Get center time for analysis to generate random seed (line 499).
analysis_centerTime =  unique(on_data.cluster.centerTime);

% --- Set random seed for job selection (line 953) from the on file center time.
rand('seed',931316785+analysis_centerTime);
initialSeed = rand('seed');
disp(' ');
disp(['initial random seed: ' num2str(initialSeed)]);

fprintf(1,'Writing file %s ... \n',strcat(outputdir,'/',injGlob,'_test.txt'));
fprintf(1,'Writing file %s ... \n',strcat(outputdir,'/',injGlob,'_train.txt'));
threshold
injClean
injScale_threshold
% ---- Set coincidence window between injection time and triggers to be selected.
injCoincTimeWindow = [256 256];
onSourceTimeOffset = 0;

% ---- Flag for looping over files.
firstFile = true;
% ---- Unique counter for triggers.
uniqIdx_test = 1;
uniqIdx_train = 1;

% ---- These scalars should match the injection scales.
scalars = [0.0100,0.0147,0.0215,0.0316,0.0464,0.0681,0.1000,0.1468,0.2154, ...
            0.3162,0.4642,0.6813,1.0000,1.4678,2.1544,3.1623,4.6416,6.8129,10.0000];

% ---- Get list of all injection files in injDir matching the specified pattern.
injFiles = dir([injDir '/*' injGlob '*merged.mat']);

% ---- ASSUMING injection scales are sorted in ascending order, the
%      first file in the list (scale 0) will correspond to the
%      smallest injection scale.  Use these as the noise triggers.
noise = load([injDir '/' injFiles(1).name]);
if verbose 
% ---- Display information on triggers being rejected.
    disp('Triggers rejected by cleaning procedure (injIdx, triggerIdx):');
end

% ---- Loop over injection files.
for iFile = 1:length(injFiles)
    % ---- Load current injection file.
    if verbose
        fprintf(1,'%s\n',injFiles(iFile).name)
    end
    inj = load([injDir '/' injFiles(iFile).name]);

    % ---- If this is the first file we have seen, use it to find out
    %      info for the output file header.
    if firstFile
        % --- Assume every inj file has same number of injections
        nInjections = length(inj.injectionProcessedMask);
        % ---- Get random order of indices between 1 and nInjections.
        rand('seed',initialSeed + 22);
        injSeed = rand('seed');
        randInjIdx = randperm(nInjections);
        nInjectionsBy2 = ceil(nInjections/2);

        % ---- Use the first half of randomly ordered injections for tuning.
        SIG_training_jobs = sort(randInjIdx(1:nInjectionsBy2));

        % ---- Use the second half of randomly ordered injections for ul calc.
        SIG_testing_jobs = sort(randInjIdx(nInjectionsBy2+1:nInjections));

        % ---- Write header info to output file.
        fid = fopen(strcat(outputdir,'/',injGlob,'_test.txt'),'w');
        fprintf(fid,'#significance/F:');
        for iLike = 1:length(inj.likelihoodType)
            fprintf(fid,'%s/F:', inj.likelihoodType{iLike});
        end
        fprintf(fid,'jobNumber/I:');
        fprintf(fid,'nPixels/I:');
        fprintf(fid,'peakTime/F:');
        fprintf(fid,'peakFrequency/F:');
        fprintf(fid,'minTime/F:');
        fprintf(fid,'minFreq/F:');
        fprintf(fid,'deltaTime/F:');
        fprintf(fid,'deltaFreq/F:');
        fprintf(fid,'injIdx/I:');
        fprintf(fid,'triggerIdx/I:');
        fprintf(fid,'uniqIdx/I:');
        fprintf(fid,'filename/I:\n');

        % ---- Write header info to output file.
        fid2 = fopen(strcat(outputdir,'/', injGlob,'_train.txt'),'w');
        fprintf(fid2,'#significance/F:');
        for iLike = 1:length(inj.likelihoodType)
            fprintf(fid2,'%s/F:', inj.likelihoodType{iLike});
        end
        fprintf(fid2,'jobNumber/I:');
        fprintf(fid2,'nPixels/I:');
        fprintf(fid2,'peakTime/F:');
        fprintf(fid2,'peakFrequency/F:');
        fprintf(fid2,'minTime/F:');
        fprintf(fid2,'minFreq/F:');
        fprintf(fid2,'deltaTime/F:');
        fprintf(fid2,'deltaFreq/F:');
        fprintf(fid2,'injIdx/I:');
        fprintf(fid2,'triggerIdx/I:');
        fprintf(fid2,'uniqIdx/I:');
        fprintf(fid2,'filename/I:\n');

        firstFile = false;
    end

    % ---- Loop over testing injections to be converted.
    for injIdx = SIG_testing_jobs

        % ---- Extract peak time of this injection.
        injPeakTime = inj.peakTime(injIdx);

        % ---- Check that the injection produced triggers before attempting
        %      to convert the triggers. 
        if ~isempty(inj.clusterInj(injIdx).boundingBox)

            % ---- Copy injection triggers into a temporary struct for
            %      convenience ... "inj.clusterInj(injIdx)" is a bit of a mouthful.
            data = inj.clusterInj(injIdx);
            % ---- Define row vector triggerIdx to keep track of origin of each
            %      trigger as we start to delete some.
            triggerIdx = 1:length(data.significance);

            % ---- Injection coincidence ---------------------------------------
            
            % ---- Define a bounding box for the injection in the
            %      time-frequency plane. Use the full analysed frequency range
            %      rather than trying to define a waveform-specific
            %      frequency range.
            injectionBox = [...
                (injPeakTime-injCoincTimeWindow(1)-onSourceTimeOffset), ...
                inj.minimumFrequency, ...
                sum(injCoincTimeWindow), ...
                inj.maximumFrequency-inj.minimumFrequency];

            % ---- Keep only triggers which overlap with the injectionBox.
            keepIdx = find(rectint(injectionBox,inj.clusterInj(injIdx).boundingBox)>0);
            data = xclustersubset(inj.clusterInj(injIdx), keepIdx);
            triggerIdx = triggerIdx(keepIdx);

            % ---- Trigger cleaning --------------------------------------------
            
            % ---- Optionally clean trigger list, if any triggers have been kept. 
            if injClean && ~isempty(data.significance) && ~isempty(noise.clusterInj(injIdx).significance) && iFile<injScale_threshold
                %Find the bounding boxes for the noise triggers
                noise_boxes = noise.clusterInj(injIdx).boundingBox;
                % ---- Check whether any injection triggers overlap any
                %      noise triggers. (The sum checks for an overlap with
                %      _any_ of the noise triggers.)
                badIdx = find(ismember(inj.clusterInj(injIdx).boundingBox,noise_boxes,'rows'));
                keepIdx = setdiff(1:length(data.significance), badIdx);
                if ~isempty(badIdx)
                    if verbose 
                        % ---- Display information on triggers being rejected.
                        disp(num2str([injIdx, triggerIdx(badIdx)]));
                    end
                    data = xclustersubset(data, keepIdx);
                    triggerIdx = triggerIdx(keepIdx);
                end
            end
            
            % ---- Output loudest remaining trigger ----------------------------
            
            % ---- Keep only loudest trigger for each injection at each injScale.
            if length(data.significance)>1
                if injClean
                    [junk,keepIdx] = max(data.significance);
                    data = xclustersubset(data, keepIdx);
                    triggerIdx = triggerIdx(keepIdx);
                end
            end

            % ---- Write out data if we have a surviving trigger.
            if length(data.significance)
                scalar = scalars(iFile);
                if scalar<1 scalar=1;  end
                for idx = 1:length(data.significance)
                    if data.significance(idx)>(threshold*scalar)
                        fprintf(fid,'%12.4f ', data.significance(idx));
                        for iLike = 1:length(inj.likelihoodType)
                            fprintf(fid,'%12.4f ', data.likelihood(idx,iLike));
                        end
                        fprintf(fid,'%d ', data.jobNumber(idx));
                        fprintf(fid,'%d ', data.nPixels(idx));
                        fprintf(fid,'%12.4f ', data.peakTime(idx));
                        fprintf(fid,'%12.4f ', data.peakFrequency(idx));
                        fprintf(fid,'%12.4f ', data.boundingBox(idx,1));
                        fprintf(fid,'%12.4f ', data.boundingBox(idx,2));
                        fprintf(fid,'%12.4f ', data.boundingBox(idx,3));
                        fprintf(fid,'%12.4f ', data.boundingBox(idx,4));
                        fprintf(fid,'%d ', injIdx);
                        fprintf(fid,'%d ', triggerIdx(idx));
                        fprintf(fid,'%d ', uniqIdx_test);
                        fprintf(fid,'%s \n', injFiles(iFile).name);
                        uniqIdx_test = uniqIdx_test + 1;
                    end
                end %-- Loop over triggers in coinc window
            end

        end %-- check injection produces triggers

    end %-- loop over testing injections

    % ---- Loop over training injections to be converted.
    for injIdx = SIG_training_jobs

        % ---- Extract peak time of this injection.
        injPeakTime = inj.peakTime(injIdx);

        % ---- Check that the injection produced triggers before attempting
        %      to convert the triggers. 
        if ~isempty(inj.clusterInj(injIdx).boundingBox)

            % ---- Copy injection triggers into a temporary struct for
            %      convenience ... "inj.clusterInj(injIdx)" is a bit of a mouthful.
            data = inj.clusterInj(injIdx);
            % ---- Define row vector triggerIdx to keep track of origin of each
            %      trigger as we start to delete some.
            triggerIdx = 1:length(data.significance);

            % ---- Injection coincidence ---------------------------------------
            
            % ---- Define a bounding box for the injection in the
            %      time-frequency plane. Use the full analysed frequency range
            %      rather than trying to define a waveform-specific
            %      frequency range.
            injectionBox = [...
                (injPeakTime-injCoincTimeWindow(1)-onSourceTimeOffset), ...
                inj.minimumFrequency, ...
                sum(injCoincTimeWindow), ...
                inj.maximumFrequency-inj.minimumFrequency];

            % ---- Keep only triggers which overlap with the injectionBox.
            keepIdx = find(rectint(injectionBox,inj.clusterInj(injIdx).boundingBox)>0);
            data = xclustersubset(inj.clusterInj(injIdx), keepIdx);
            triggerIdx = triggerIdx(keepIdx);

            % ---- Trigger cleaning --------------------------------------------
            
            % ---- Optionally clean trigger list, if any triggers have been kept. 
            if injClean && ~isempty(data.significance) && ~isempty(noise.clusterInj(injIdx).significance) && iFile<injScale_threshold
                %Find the bounding boxes for the noise triggers
                noise_boxes = noise.clusterInj(injIdx).boundingBox;
                % ---- Check whether any injection triggers overlap any
                %      noise triggers. (The sum checks for an overlap with
                %      _any_ of the noise triggers.)
                badIdx = find(ismember(inj.clusterInj(injIdx).boundingBox,noise_boxes,'rows'));
                keepIdx = setdiff(1:length(data.significance), badIdx);
                if ~isempty(badIdx)
                    if verbose 
                        % ---- Display information on triggers being rejected.
                        disp(num2str([injIdx, triggerIdx(badIdx)]));
                    end
                    data = xclustersubset(data, keepIdx);
                    triggerIdx = triggerIdx(keepIdx);
                end
            end
            
            % ---- Output loudest remaining trigger ----------------------------
            
            % ---- Keep only loudest trigger for each injection at each injScale.
            if length(data.significance)>1
                if injClean
                    [junk,keepIdx] = max(data.significance);
                    data = xclustersubset(data, keepIdx);
                    triggerIdx = triggerIdx(keepIdx);
                end
            end

            % ---- Write out data if we have a surviving trigger.
            if length(data.significance)
                scalar = scalars(iFile);
                if scalar<1 scalar=1;  end
                for idx = 1:length(data.significance)
                    if data.significance(idx)>(threshold*scalar)
                        fprintf(fid2,'%12.4f ', data.significance(idx));
                        for iLike = 1:length(inj.likelihoodType)
                            fprintf(fid2,'%12.4f ', data.likelihood(idx,iLike));
                        end
                        fprintf(fid2,'%d ', data.jobNumber(idx));
                        fprintf(fid2,'%d ', data.nPixels(idx));
                        fprintf(fid2,'%12.4f ', data.peakTime(idx));
                        fprintf(fid2,'%12.4f ', data.peakFrequency(idx));
                        fprintf(fid2,'%12.4f ', data.boundingBox(idx,1));
                        fprintf(fid2,'%12.4f ', data.boundingBox(idx,2));
                        fprintf(fid2,'%12.4f ', data.boundingBox(idx,3));
                        fprintf(fid2,'%12.4f ', data.boundingBox(idx,4));
                        fprintf(fid2,'%d ', injIdx);
                        fprintf(fid2,'%d ', triggerIdx(idx));
                        fprintf(fid2,'%d ', uniqIdx_train);
                        fprintf(fid2,'%s \n', injFiles(iFile).name);
                        uniqIdx_train = uniqIdx_train + 1;
                    end
                end %-- Loop over triggers in coinc window
            end

        end %-- check injection produces triggers

    end %-- loop over training injections

end %-- loop over injection files

% ---- Close output file.
fclose(fid);
fclose(fid2);

% ---- Done.
if verbose
    fprintf(1,'Done! \n')
end
return
