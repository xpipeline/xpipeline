function msun = erg2msun(erg)
% ERG2MSUN - Convert energy in erg to solar masses.
%
%   msun = erg2msun(erg)
%
% erg   Array.  Energy in units of erg.  
% msun  Array.  Energy in units of solar masses. 
%
% ERG2MSUN converts an energy in erg to solar masses.  The conversion
% formula is 
%
%  msun = erg / (solarMass * speedOfLight^2 * 1e7);
%       = erg / 1.7871e+54

% ---- Check number of input arguments.
error(nargchk(1, 1, nargin));

% ---- Needed constants.
speedOfLight = 299792458;  %-- m/s
NewtonG = 6.67e-11;        %-- Newton's G (Nm^2/kg^2=m^3/kg/s^2).
secondsPerYear = 365.25*86400;              %-- s
parsec = 3.26*speedOfLight*secondsPerYear;  %-- m
solarMass = 1.988435e30;   %-- kg
ergPerJoule = 1e7; 

% ---- Convert to erg.
msun = erg / (solarMass * speedOfLight^2 * ergPerJoule);

% ---- Done.
return
