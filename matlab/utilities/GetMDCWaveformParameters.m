function [waveform,parameters] = GetMDCWaveformParameters(inj_name)
% GETMDCWAVEFORMPARAMETERS - Convert MDC name to xmakewaveform parameters.
% 
% GetMDCWaveformParameters - Parse the name of a waveform used in MDC 
% simulations and return the corresponding arguments used to construct 
% the waveform with the xmakewaveform function.
%
%   [type,params] = GetMDCWaveformParameters(inj_name)
%
%  inj_name   Cell array containing the waveform names; e.g. 
%             {'SG2000Q9','SG235Q8.9'}.  See below.
%
%  type       Cell array containing first argument of xmakewaveform 
%             (injection type).
%  params     Cell array containing second argument of xmakewaveform 
%             (injection parameters).
%
% This script reads a set of injection parameters for MDC injections 
% and generates the corresponding signal for the specified detector.
% The amplitude (or distance) is set to yield an hrss of 2.5e-21/sqrt(Hz).
%
% Waveform names are not case sensitive.  Recognized formats are Gaussians,
% sine-Gaussians, and Lazarus mergers, in the following format:
%   GA4.0     - 4 ms Gaussian
%   SG235Q8.9 - Q=9 sine-Gaussian at 235 Hz  
%   BH70      - 7 solar-mass (35+35) Lazarus black-hole merger (ciota=1)
% You can also use 'd' to stand for exactly one decimal point:
%   SG235Q8d9
%   GA0d5 
%   BH20d5 
% If a waveform is not recognized or is not available through the
% xmakewaveform function, the type for a null waveform (type='zero') is
% returned.
%
% initial write: Patrick J. Sutton 2005.01.09
%
% $Id$

% ---- Prepare storage for output;
waveform = cell(length(inj_name),1);
parameters = cell(length(inj_name),1);

% ---- Loop over injection names.
for iType=1:length(inj_name);

    % ---- Copy waveform type into char string.  Convert to lowercase for robustness.
    type = lower(inj_name{iType}); 
    type_prefix = type(1:2);  % -- first two char only

    % ---- Default amplitude
    hrss = 2.5e-21;

    % ---- Compute parameters of waveform.
    if (strcmp(type_prefix,'bh'))
        waveform{iType} = 'Lazarus';
        mass = type(3:end);
        if (~isempty(strfind(mass,'d')))
            index = strfind(mass,'d');
            mass(index) = '.';
        end
        mass = str2num(mass);
        % -- Note: need to set distance to get desired hrss.  For Lazarus 
        %    waveform with ciota = 1, find 
        %    hrss = (mass/100)^(3/2) * 1.2160e-19 / (distance)
        distance = (mass/100)^(3/2) * 1.2160e-19 / (hrss);
        ciota = 1;
        parameters{iType} = [num2str(mass) '~' num2str(distance) '~' num2str(ciota)];
    elseif (strcmp(type_prefix,'ga'))
        waveform{iType} = 'G';
        tau = type(3:end);
        if (~isempty(strfind(tau,'d')))
            index = strfind(tau,'d');
            tau(index) = '.';
        end
        tau = str2num(tau) * 1e-3;
        h0 = hrss*(pi*tau^2/2)^(-1/4);
        parameters{iType} = [num2str(h0) '~' num2str(tau)];
    elseif (strcmp(type_prefix,'sg'))
        waveform{iType} = 'SG';
        index = strfind(type,'q');
        f0 = str2num(type(3:index-1));
        Q = str2num(type(index+1:end));
        tau = Q / (2^0.5 * pi * f0);
        h0 = hrss*(pi/2/f0^2)^(-1/4);
        parameters{iType} = [num2str(h0) '~' num2str(tau) '~' num2str(f0)];
    % elseif (strcmp(type_prefix,'DS'))
    %     waveform{iType} = 'DS2P';
    %     index = strfind(type,'T');
    %     f0 = str2num(type(3:index-1));
    %     if (strcmp(type(end),'n'))
    %         tau = str2num(type(index+1:end-1)) * 1e-3;        
    %     %
    %     delta = 0;
    %     %-- Warning!  Not clear that this will be accurate for ciota~0.
    %     ciota = (hrssp/hrssc) - ( (hrssp/hrssc)^2 - 1 )^0.5;
    %     h0 = hrssp*2/(1+ciota^2);
    %     parameters = [h0, tau, f0, delta, ciota]; 
    % elseif (strcmp(type,'DS930T6n'))
    %     waveform = 'DS2P';
    %     f0 = 930;
    %     tau = 6e-3;
    %     delta = 0;
    %     %-- Warning!  Not clear that this will be accurate for ciota~0.
    %     ciota = (hrssp/hrssc) - ( (hrssp/hrssc)^2 - 1 )^0.5;
    %     ciota = -1*ciota;
    %     h0 = hrssp*2/(1+ciota^2);
    %     parameters = [h0, tau, f0, delta, ciota]; 
    % elseif (strcmp(type,'cusp_200'))
    %     waveform = 'cusp';
    %     parameters = 200;
    % elseif (strcmp(type,'cusp_500'))
    %     waveform = 'cusp';
    %     parameters = 500;
    % elseif (strcmp(type,'cusp_2000'))
    %     waveform = 'cusp';
    %     parameters = 2000;
    % elseif (strcmp(type_prefix,'DF'))
    %     DFM_type = ['signal_' type_chararray(5:10) '_R.dat'];
    %     waveform = 'DFM';
    %     parameters{1} = 1./hrss;
    %     parameters{2} = DFM_type; 
    % elseif (strcmp(type_prefix,'e1') || strcmp(type_prefix,'e2') || strcmp(type_prefix,'m1') || strcmp(type_prefix,'m2') || strcmp(type_prefix,'s1'))
    %     %----- Nasty kludge: the MDC log file names use 'd' in place of '.'.
    %     %      Have to convert back.
    %     index = (type_chararray == 'd'); 
    %     k = find(index==1);
    %     type_chararray(k) = '.';
    %     waveform = 'OB';
    %     parameters{1} = 1./hrss;
    %     parameters{2} = type_chararray; 
    % elseif (strcmp(type,'inspiral_10_10'))
    %     waveform = 'INSP';
    %     parameters = [];
    % elseif (strcmp(type,'onecycle_1000'))
    %     waveform = 'onecyclesine';
    %     parameters = [1,-1000];
    %     parameters = [h0, tau, f0];
    % elseif (strcmp(type_prefix,'s2'))
    %     %----- These are OB waveforms that are not available in our catalog;
    %     %      set reconstructed injection to zero.
    %     % [t,hp,hc] = MakeWaveform(waveform,parameters,T,[],fs);
    %     display('Requested waveform type not available through MakeWaveform.  Returning null waveform type.')
    %     waveform = 'zero';
    %     parameters = [];
    % elseif (strcmp(type,'whistle_1000_100_0d1'))
    %     waveform = 'whistle';
    %     parameters = [0.5,760,1260];
    % elseif (strcmp(type,'whistle_1000_10_0d1'))
    %     waveform = 'whistle';
    %     parameters = [0.5,975,1025];
    % elseif (strcmp(type,'whistle_1000_500_0d1'))
    %     waveform = 'whistle';
    %     parameters = [0.5,60,2400];
    % elseif (strcmp(type,'whistle_100_10_0d1'))
    %     waveform = 'whistle';
    %     parameters = [0.5,76,126];
    % elseif (strcmp(type,'whistle_250_100_0d1'))
    %     waveform = 'whistle';
    %     parameters = [0.5,50,530];
    % elseif (strcmp(type,'whistle_250_10_0d1'))
    %     waveform = 'whistle';
    %     parameters = [0.5,225,275];
    % elseif (strcmp(type,'whistle_1000_100_0d01'))
    %     waveform = 'whistle';
    %     parameters = [0.05,760,1260];
    % elseif (strcmp(type,'whistle_1000_500_0d001'))
    %     waveform = 'whistle';
    %     parameters = [0.005,70,2400];
    % elseif (strcmp(type,'whistle_1000_500_0d01'))
    %     waveform = 'whistle';
    %     parameters = [0.05,60,2400];
    % elseif (strcmp(type,'whistle_250_100_0d01'))
    %     waveform = 'whistle';
    %     parameters = [0.05,50,530];
    % elseif (strcmp(type,'WNB1_100_100_0d01'))
    %     waveform = 'WNB';
    %     parameters = [1,100,100,0.01];
    % elseif (strcmp(type,'WNB1_100_100_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,100,100,0.1];
    % elseif (strcmp(type,'WNB1_100_10_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,100,10,0.1];
    % elseif (strcmp(type,'WNB1_250_100_0d01'))
    %     waveform = 'WNB';
    %     parameters = [1,250,100,0.01];
    % elseif (strcmp(type,'WNB1_250_100_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,250,100,0.1];
    % elseif (strcmp(type,'WNB1_250_10_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,250,10,0.1];
    % elseif (strcmp(type,'WNB1_1000_1000_0d001'))
    %     waveform = 'WNB';
    %     parameters = [1,1000,1000,0.001];
    % elseif (strcmp(type,'WNB1_1000_1000_0d01'))
    %     waveform = 'WNB';
    %     parameters = [1,1000,1000,0.01];
    % elseif (strcmp(type,'WNB1_1000_1000_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,1000,1000,0.1];
    % elseif (strcmp(type,'WNB1_1000_100_0d01'))
    %     waveform = 'WNB';
    %     parameters = [1,1000,100,0.01];
    % elseif (strcmp(type,'WNB1_1000_100_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,1000,100,0.1];
    % elseif (strcmp(type,'WNB1_1000_10_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,1000,10,0.1];
    % elseif (strcmp(type,'WNB1_849_100_0d01'))
    %     waveform = 'WNB';
    %     parameters = [1,849,100,0.01];
    % elseif (strcmp(type,'WNB1_849_100_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,849,100,0.1];
    % elseif (strcmp(type,'WNB1_849_10_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,849,10,0.1];
    % elseif (strcmp(type,'WNB1_849_849_0d001'))
    %     waveform = 'WNB';
    %     parameters = [1,849,849,0.001];
    % elseif (strcmp(type,'WNB1_849_849_0d01'))
    %     waveform = 'WNB';
    %     parameters = [1,849,849,0.01];
    % elseif (strcmp(type,'WNB1_849_849_0d1'))
    %     waveform = 'WNB';
    %     parameters = [1,849,849,0.1];
    % elseif (strcmp(type_prefix,'ZM'))
    %     ZM_type = type_chararray(4:9);
    %     waveform = 'ZM';
    %     parameters{1} = 1./hrss;
    %     parameters{2} = ZM_type; 
    else
        display('Requested waveform type not available through MakeWaveform.  ')
        display('Returning null waveform type.')
        %error(['Waveform ' type{1} ' not recognized'])
    end

end  % -- end loop over injection names
    
%----- Done
return
