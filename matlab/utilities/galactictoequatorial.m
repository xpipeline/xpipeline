function [alpha,delta] = galactictoequatorial(ell,b)
% GALACTICTOEQUATORIAL: Convert from galactic to equatorial sky coordinates.
%  
%   [alpha,delta] = galactictoequatorial(ell,b)
%
% ell       Vector. Galactic longitude (rad).
% b         Vector. Galactic latitude (rad).
%
% alpha     Right ascension in equatorial (sky-based) coordinates (rad).
% delta     Declination in equatorial coordinates (rad).
%
% For definitions and discussion of the equatorial and galactic 
% cooridnate systems, see the LAL Software Document, Section 16.7, 
% "SkyCoordinates.h"
%
% -- first version: Patrick J. Sutton 2004.07.29

% ---- Sanity check: inputs should be in radians, so issue warning if values are
%      outside the expected ranges.
if any(abs(b)>pi/2 | ell>2*pi | ell<-pi) 
    warning('Input arguments outside expected ranges for inputs in radians')
end

% ---- Constants defining galactic coordinate system.
%      See LAL Software Document, Section 16.7, "SkyCoordinates.h"
alpha_NGP = 192.8594813*pi/180;
delta_NGP = 27.1282511*pi/180;
ell_ascend = 33*pi/180;

% ---- Transformation from galactic to equatorial coordinates.
% ---- declination
delta = asin( cos(b).*sin(ell-ell_ascend)*cos(delta_NGP) + sin(b)*sin(delta_NGP) );
% ---- right ascension, wrapped to [0,2*pi)
alpha = atan2( cos(b).*cos(ell - ell_ascend), sin(b)*cos(delta_NGP) - cos(b).*sin(ell-ell_ascend)*sin(delta_NGP) ) + alpha_NGP;
alpha = mod(alpha,2*pi);

% ---- Done
return
