function flag = isrealenough(x,threshold,name)
% ISREALENOUGH - test for data being almost real and non-NaN
%
% usage
%
%   flag = isrealenough(x)
%
% x             Array of complex data.
% threshold     Real scalar.
% name          Optional string. Name of data being tested; used in output
%               messages. 
%
% flag          Boolean. True if data obeys the condition 
%                 max(abs(imag(x))./abs(x)) < threshold 
%               and no values of x are NaN.
% $Id$

if nargin < 3
    name = 'x';
end

flag = true;
x = x(:);
if max(abs(imag(x))./abs(x)) >= threshold 
    warning(['max(abs(imag(' name '))./abs(' name ')) = ' num2str(max(abs(imag(x))./abs(x))) ]);
    flag = false;
elseif any(isnan(real(x)))
    warning(['NaN values for real(' name ')']);
    flag = false;
elseif any(isnan(imag(x)))
    warning(['NaN values for imag(' name ')']);
    flag = false;
end

return
