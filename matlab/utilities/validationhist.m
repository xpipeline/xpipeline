function [h,f] = validationhist(data,limits,name,NumBins,titleStr,uniformFlag)
% validationhist - histogramming function with added features
%
% usage:
%
% [h,f] = validationhist(data,limits,name,NumBins,titleStr,uniformFlag)
%
%  data         Vector of data to be histogrammed.
%  limits       2-component vector; lower and upper limits of bin (x) range.
%  name         String. Used to label x-axis.
%  numBins      Scalar. Number of bins to use.
%  titleStr     String. Used for figure title.
%  uniformFlag  Boolean. If true then assume data should be uniformly
%               distributed and add expected mean, Poisson error bars to the
%               plot. 
%
% $Id$

if nargin<6
    uniformFlag = false;
end
if nargin<5
    titleStr = '';
end
if nargin<4
    NumBins = 32;
end

f = figure; 
h = histogram(data,[limits(1):(diff(limits)/NumBins):limits(end)]);
grid on; hold on
xlabel(strrep(name,'_','\_'));
ylabel('number');
if ~isempty(titleStr)
    title(strrep(titleStr,'_','\_'));
end
if uniformFlag
    % ---- Plot mean and +/- 1 sigma.
    mu    = mean(h.Values);
    sigma = (mean(h.Values)).^0.5;
    plot(h.BinEdges([1,end])',mu*[1;1],'k-')
    plot(h.BinEdges([1,end])',(mu+sigma)*[1;1],'k--')
    plot(h.BinEdges([1,end])',(mu-sigma)*[1;1],'k--')
    legend('number','expected','expected +/- 1 sigma','location','southeast');
end
axis([limits get(gca,'ylim')])

return
