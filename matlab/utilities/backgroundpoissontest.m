function backgroundpoissontest(offSourceSelectedBeforeDQ,offSourceSelectedAfterAllCuts,makePlots,prc,Nmc)
% BACKGROUNDPOISSONTEST - test independence of circular time slides
%
% usage:
%
%  backgroundpoissontest(offSourceSelectedBeforeDQ,offSourceSelectedAfterAllCuts,makePlots,prc,Nmc)
%
%  offSourceSelectedBeforeDQ        Struct as produced by XMAKEGRBWEBPAGE. Off-source triggers before any cuts
%                                   (including data quality vetos) are applied.
%  offSourceSelectedAfterAllCuts    Struct as produced by XMAKEGRBWEBPAGE. Off-source triggers after all cuts
%                                   (including coherent and data quality vetos) are applied.
%  makePlots                        Optional Boolean. If true make plots. Default true.
%  prc                              Optional scalar. Percentage of loudest triggers to use for the 
%                                   test. Default 50.
%  Nmc                              Optional scalar. Number of Monte Carlo trials to use for estimating
%                                   properties of the Poisson distribution. Default 1e6.
%
% alternate usage:
%
%  backgroundpoissontest(fileName)
%
%  fileName                         String. Name of output file produced by XMAKEGRBWEBPAGE (e.g., the 
%                                   *closedbox.mat file). Off-source triggers will be read from this 
%                                   file and default values will be used for the other arguments.
%
% BACKGROUNDPOISSONTEST tests if the number of triggers produced by consecutive
% circular time slides are consistent with independent draws from a Poisson
% distribution. It does this as follows:
%
%   1. It determines the total number of unique circular time slides used, Ncirc.
%   2. It determines the total number of unique 'fakeJobNumber' values present in
%      the offSourceSelectedBeforeDQ. Each unique fakeJobNumber corresponds to a
%      unique combination of data block (line in event_off_source) and circular
%      time slide. See 
%        https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation/Searches/grb/jobnumberdescription
%   3. It applies a threshold to offSourceSelectedAfterAllCuts.significance to
%      keep only the loudest prc percent of the offSourceSelectedAfterAllCuts
%      triggers. These are the triggers used for the Poisson test.
%   4. It counts the number of surviving triggers for each fakeJobNumber.
%   5. It counts the difference in the number of triggers between consecutive
%      circular time slides for each data block. Consecutive circular time
%      slides have consecutive fakeJobNumber values, expect when  
%      mod(fakeJobNumber,Ncirc)==0, which indicates a switch to a new data block. 
%   6. It generates Nmc independent samples from a Poisson distribution with the
%      same mean rate at the mean trigger rate for the slides used in step 5 and 
%      computes the difference between consecutive samples (simulating ideal
%      independent consecutive circular slides). 
%   7. It reports the standard deviation in the differences from step 5
%      (measured triggers) and step 6 (ideal Poisson triggers). Similar values
%      are consistent with consecutive circular slides being independent. A
%      measured standard deviation smaller than the Poisson derived standard
%      deviation indicates that consecutive circular slides are correlated.
%
% BACKGROUNDPOISSONTEST reports summary numbers to the screen.
%
% $Id$


% ------------------------------------------------------------------------------
%    Process and validate command line arguments.
% ------------------------------------------------------------------------------

% ---- Check for sufficient command line arguments and assign defaults.
narginchk(1, 5);
if nargin == 1
    % ---- We've been pointed to a background .mat file. Load needed variables.
    fileName = offSourceSelectedBeforeDQ;
    clear offSourceSelectedBeforeDQ
    load(fileName,'offSourceSelectedBeforeDQ','offSourceSelectedAfterAllCuts');
end
if nargin < 5
    Nmc = 1e6;
end
if nargin < 4
    prc = 50;
end
if nargin < 3
    makePlots = true;
end


% ------------------------------------------------------------------------------
%    Count number of background trials, etc. from triggers before any cuts.
% ------------------------------------------------------------------------------

off = offSourceSelectedBeforeDQ;
%
Noff  = length(off.significance);
Nrjob = length(unique(off.realJobNumber));
Ncirc = length(unique(off.circTimeSlides,'rows'));
Nfake = length(unique(off.fakeJobNumber));
uniq_fake = unique(off.fakeJobNumber);
Nfake_exp = Nrjob*Ncirc/2;

% ---- Report to screen.
disp(['----------------------------------------------------------------------'])
disp(['Triggers before any cuts (including before data quality):'])
disp(['                triggers: ' num2str(Noff) ])
disp(['                  events: ' num2str(Nrjob) ])
disp(['             circ slides: ' num2str(Ncirc) ])
disp([' unique fake job numbers: ' num2str(Nfake) ])
disp(['   expected unique fakes: ' num2str(Nfake_exp) ]) 
if Nfake<Nfake_exp
    warning(['Missing some unique fakeJobNumber values even before any cuts.'])
end
disp(['----------------------------------------------------------------------'])


% ------------------------------------------------------------------------------
%    Switch to background of interest: triggers after all cuts.
% ------------------------------------------------------------------------------

off = offSourceSelectedAfterAllCuts;

% ---- Compute threshold for down-selecting to loudest triggers.
threshold = prctile(off.significance,100-prc);

% ---- Plot triggers before down-selection, with threshold.
if makePlots
    figure; histogram(log10(off.significance))
    set(gca,'fontsize',16)
    xlabel('log_{10}(significance)');
    ylabel('number of triggers');
    grid on; hold on
    ylim = get(gca,'ylim');
    plot(log10([threshold;threshold]),ylim(:),'m--','linewidth',2)
end

% ---- Down-select triggers.
off = xclustersubset(off,find(off.significance>threshold));
Noff = length(off.significance);

% ---- Count number of triggers per fakeJobNumber. (Each value of fakeJobNumber
%      corresponds to a unique 256 sec data block + circular time slide.)
[val,IA,IC] = unique(off.fakeJobNumber);
N = zeros(Nfake,1);
for ii=1:length(val)
    idx = find(uniq_fake==val(ii));
    N(idx) = sum(IC==ii);
end
if sum(N)~=length(off.significance)
    error('Mismatch in trigger numbers.')
end

% ---- uniq_fake are ordered. We only want to count the difference between
%      consecutive uniq_fake job numbers that differ by plus one (i.e. n->n+1)
%      and which aren't multiples of the number of circular slides.
% ---- Prepare storage.
cN     = nan(Nfake-1,1);
cNdiff = nan(Nfake-1,1);
for ii=1:Nfake-1
    if (uniq_fake(ii+1)==uniq_fake(ii)+1) && mod(uniq_fake(ii+1),Ncirc)~=0
        cN(ii)     = N(ii+1);
        cNdiff(ii) = N(ii+1)-N(ii);
    end
end
% ---- Remove remaining NaN values (these correspond to uniq-fake values we
%      couldn't use).
dropIdx = find(isnan(cN));
cN(dropIdx) = [];
cNdiff(dropIdx) = [];
% --- Mean rate.
lambda = mean(cN);
% --- Mean and standard deviation of the difference between consecutive lags.
Nstd = std(cNdiff);
Nmu = mean(cNdiff);

% ---- Report to screen.
disp(['Triggers after all cuts and significance threshold:'])
disp(['      % of triggers kept: ' num2str(prc) ])
disp(['  significance threshold: ' num2str(threshold) ])
disp(['      # of triggers kept: ' num2str(Noff) ])
disp(['         usable triggers: ' num2str(sum(cN)) ])
disp(['            Poisson rate: ' num2str(lambda)])
disp(['----------------------------------------------------------------------'])


% ------------------------------------------------------------------------------
%    Compare to independent draws from a Poisson distribution.
% ------------------------------------------------------------------------------

% ---- Make a large number of independent draws a Poisson distribution with this
%      mean.
R = poissrnd(lambda*ones(Nmc,1));
Rstd = std(diff(R));
Rmu = mean(diff(R));

% ---- Plots of the distributions.
if makePlots
    figure; 
    %
    subplot(2,2,1); 
    histogram(R,'Normalization','probability')
    grid on;
    title(['Poisson Distribution (' num2str(Nmc) ' samples)'])
    xlabel('number of clusters');
    ylabel('relative frequency');
    set(gca,'fontsize',16)
    %
    subplot(2,2,2); 
    histogram(diff(R),'Normalization','probability')
    grid on;
    title('Poisson Distribution - differences')
    xlabel('number difference: lag(i+1)-lag(i)');
    ylabel('relative frequency');
    legend([num2str(Rmu) ' +/- ' num2str(Rstd)]);
    set(gca,'fontsize',16)
    %
    subplot(2,2,3); 
    histogram(cN,'Normalization','probability')
    grid on;
    title(['Measured Distribution (' num2str(Noff) ' clusters)'])
    xlabel('number of clusters');
    ylabel('relative frequency');
    set(gca,'fontsize',16)
    %
    subplot(2,2,4); 
    histogram(cNdiff,'Normalization','probability')
    grid on;
    title('Measured Distribution - differences')
    xlabel('number difference: lag(i+1)-lag(i)');
    ylabel('relative frequency');
    legend([num2str(Nmu) ' +/- ' num2str(Nstd)]);
    set(gca,'fontsize',16)
end

% ---- Report to screen.
disp(['  Statistics of differences between consecutive lags: ']);
disp(['  Poisson distn mean & st.dev.: ' num2str(Rmu) ' +/- ' num2str(Rstd) '  ('  num2str(Nmc) ' samples)']);
disp(['       Measured mean & st.dev.: ' num2str(Nmu) ' +/- ' num2str(Nstd)]);
disp(['----------------------------------------------------------------------'])

