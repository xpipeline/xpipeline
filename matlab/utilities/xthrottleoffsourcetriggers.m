function xthrottleoffsourcetriggers(dirName,fileListString,outFile)
% xthrottleoffsourcetriggers - throttles down the number of triggers in an offsource merged file
%
% usage:
%
%   xinjunion(dirName,fileList)
%
%   dirName    String. Directory containing all files to be read.
%   fileList   String. List of merged results files to be read. 
%   outFile    Optional string. File to output merged clusters to. If not
%              specified, original input file is over-written. 
%
% $Id$


% --- set maximum number of triggers in the offsource file to be 1600000
% --- we noticed that the MVA analysis bit that converts numbers to root is unstable for
% --- larger numbers of triggers

maxOffEvent = 1600000;
maxOffEventTotal = 6e6;

% ---- Parse file list string into cell array of separate file names.
fileList = strread(fileListString,'%s');

% ---- Loop over all files to be merged.
for iFile = 1:length(fileList)
  fileName = [dirName '/' fileList{iFile}];
  s = load(fileName);
  cluster = s.cluster;
  mergedCluster = cluster;  
  clusterFields = fieldnames(cluster);
  firstEmptyCellIndex = 1;

  [junk Ic] = sort(cluster.significance(:,1),'descend');
  % ---- Keep at most the maxOffEvent loudest clusters.
  if (length(Ic)>maxOffEvent)
    Ic = Ic(1:maxOffEvent);
  end

  % ---- Loop over fields and keep seelcted events. Note that extracting
  %      data by field names is robust even if the field ordering in the 
  %      struct changes from one file to the next.
  for ii = 1:length(clusterFields)
    % ---- Get the data for this field.
    fieldValue = getfield(cluster,clusterFields{ii});
    mergedClusterField{ii}(firstEmptyCellIndex: ...
          firstEmptyCellIndex+length(Ic)-1,:) = fieldValue(Ic,:);
  end

  % ---- Update index of first empty cell.
  firstEmptyCellIndex = firstEmptyCellIndex+length(Ic);
  if (firstEmptyCellIndex+maxOffEvent>maxOffEventTotal)
   error(['Run out of space to add new off/on source events; ' ...
    'change total limit (' num2str(maxOffEventTotal) ...
    ') or throttle the output trigger rate.'])
  end

  for ii = 1:length(clusterFields)
    mergedClusterField{ii} = mergedClusterField{ii}(1:firstEmptyCellIndex-1,:);
    mergedCluster = setfield(mergedCluster,clusterFields{ii},mergedClusterField{ii});
  end

  s.cluster = mergedCluster; 

  % --- overwrite the file if not new name specified
  if nargin<3
      outFile = fileName;
  end
  save(outFile, '-struct', 's');

end

% ---- Done.
return
