function seg1not2 = subtractsegments(arg1,arg2,arg3,arg4,arg5)
% SUBTRACTSEGMENTS - subtract one segment list from another
%
% usage mode 1:
%
%   seg1not2 = subtractsegments(start1,dur2,start2,dur2,outFile)
%
%   start1    Vector. Start times of segments in list 1.
%   dur1      Vector. Durations of segments in list 1.
%   start2    Vector. Start times of segments in list 2.
%   dur2      Vector. Durations of segments in list 2.
%   fileOut   Optional string. If specified, subtracted segment list is written 
%             to a file with this name.
%
%   seg1not2  Array. First column is start time of segments in list "1 not 2".
%             Second column is duration of those segments.
%
% usage mode 2:
%
%   seg1not2 = subtractsegments(file1,file2,outFile)
%
%   file1     String. Name of file containing segment list 1 (four-column format).
%   file2     String. Name of file containing segment list 2 (four-column format).
%   fileOut   Optional string. If specified, subtracted segment list is written to
%             a file with this name.
%
%   seg1not2  As above.
%
% The output segment list is defined as AND(1,NOT(2)).
%
% $Id$

% ---- Default: no file output.
fileOut = '';
% ---- Check number of input arguments.
error(nargchk(2,5,nargin))
if nargin>=4
    start1 = arg1;
    dur1   = arg2;
    start2 = arg3;
    dur2   = arg4;
    if nargin==5
        fileOut = arg5;
    end
else
    % ---- Read segment files.
    [segno start1 stop dur1] = readsegmentlist(arg1);
    [segno start2 stop dur2] = readsegmentlist(arg2);
    clear segno stop    
    if nargin==3
        fileOut = arg3;
    end
end

% ---- Last time covered by either segment list.
maxTime = max(start1(end)+dur1(end),start2(end)+dur2(end));

% ---- Take complement of list to be subtracted.
tmp = ComplementSegmentList(start2,dur2,0,maxTime+1);
not2start = tmp(:,1);
not2dur = tmp(:,2);
%if any(not2dur==0)

% ---- Intersect "1" and "not2" lists.
C = Coincidence2(start1,dur1,not2start,not2dur);
seg1not2 = C(:,1:2);

% ---- Write segment list to output file if desired.
if ~isempty(fileOut)
    fid = fopen(fileOut,'w');
    header = '# seg_number start stop duration';
    fprintf(fid,'%s\n',header);
    Nseg = size(seg1not2,1);
    data = [[0:(Nseg-1)]' seg1not2(:,1) (seg1not2(:,1)+seg1not2(:,2)) seg1not2(:,2)];
    fprintf(fid,'%d %d %d %d\n',data');
    fclose(fid);
end

% ---- Done.
return
