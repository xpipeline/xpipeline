function [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal)
% GALACTICTOEQUATORIAL3D: Convert from 3D galactic to equatorial sky coordinates.
%  
%   [alpha,delta,r_earth] = galactictoequatorial3d(ell,b,r_gal)
%
% ell       Vector. Galactic longitude (rad).
% b         Vector. Galactic latitude (rad).
% r_gal     Vector. Distance from Galactic centre (kpc).
%
% alpha     Right ascension in equatorial (sky-based) coordinates (rad).
% delta     Declination in equatorial coordinates (rad).
% r_earth   Distance from Earth (kpc).
%
% For definitions and discussion of the equatorial and galactic 
% cooridnate systems, see the LAL Software Document, Section 16.7, 
% "SkyCoordinates.h"
%
% We assume the Earth is located at a distance of 8.5 kpc from the Galactic
% centre.
%
% $ID$
%
% See also GALACTICTOEQUATORIAL.

% ---- Convert input positions from spherical coordinates (ell,b,r_gal) to
%      Cartesian (x,y,z).
V_gal = CartesianPointingVector(ell,pi/2-b);
V_gal = V_gal .* repmat(r_gal,1,3);

% ---- Construct unit vectors for the x,y,z axes of the equatorial system in
%      galactic coordinates.
[x_axis_ell,x_axis_b] = equatorialtogalactic(0,0);
[y_axis_ell,y_axis_b] = equatorialtogalactic(pi/2,0);
[z_axis_ell,z_axis_b] = equatorialtogalactic(0,pi/2);
x_axis = CartesianPointingVector(x_axis_ell,pi/2-x_axis_b);
y_axis = CartesianPointingVector(y_axis_ell,pi/2-y_axis_b);
z_axis = CartesianPointingVector(z_axis_ell,pi/2-z_axis_b);

% ---- Shift origin to the position of the Earth in the Galaxy.
V_earth_gal = [-8.5 0 0]; %-- [kpc]
V_gal = V_gal - V_earth_gal;

% ---- X component of input positions in equatorial coordinates is the dot
%      product of the input position vector with the equatorial coordinate
%      x-axis unit vector, and similarly for Y, Z.
V_eq(:,3) = V_gal * z_axis';
V_eq(:,2) = V_gal * y_axis';
V_eq(:,1) = V_gal * x_axis';

% ---- Convert from Cartesian to spherical coordinates.
r_eq = (sum(V_eq.^2,2)).^0.5;
theta_eq = acos(V_eq(:,3)./r_eq);
delta = pi/2-theta_eq;
alpha = atan2(V_eq(:,2),V_eq(:,1));
alpha = mod(alpha,2*pi);

% ---- Done
return


% ------------------------------------------------------------------------------
%    Test code.
% ------------------------------------------------------------------------------

% % ---- Line to north Galactic pole.
% ell = 0 
% b = pi/2
% r_gal = 1e6;
% [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal);
% [alpha,delta]*180/pi
% r_eq
% %
% r_gal = 0;
% [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal);
% [alpha,delta]*180/pi
% r_eq
% %
% ell = 0 
% b = -pi/2 % south pole
% r_gal = 1e6;
% [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal);
% [alpha,delta]*180/pi
% r_eq
% 
% % ---- Line to Galactic centre.
% ell = 0 
% b = 0
% [alpha,delta] = galactictoequatorial(ell,b);
% [alpha,delta]*180/pi
% %
% ell = 0 
% b = 0
% r_gal = 0
% [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal);
% [alpha,delta]*180/pi
% r_eq
% %
% ell = pi 
% b = 0
% r_gal = 8.4
% [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal);
% [alpha,delta]*180/pi
% r_eq
% %
% ell = pi
% b = 0
% r_gal = 8.6
% [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal);
% [alpha,delta]*180/pi
% r_eq
% %
% ell = pi
% b = 0
% r_gal = 1e6
% [alpha,delta,r_eq] = galactictoequatorial3d(ell,b,r_gal);
% [alpha,delta]*180/pi
% r_eq
