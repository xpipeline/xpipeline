function x = fisher(k,N,M,seed)
% FISHER - generate random number according to fisher distribution
%
% FISHER - generate random number according to fisher distribution:
%
%                       k    
%          P(x) = ---------------  sin(x) exp( k cos(x) )
%                 exp(k) - exp(-k)
% usage:
% 
%  x = fisher(k,N,M,seed)
%
%  x      Matrix of size [N,M] containing generated random numbers
%  k      Scalar. Parameter of the fisher distribution
%  N      Scalar. Number of rows to generate
%  M      Scalar. Number of columns to generate.
%  seed   Scalar. Seed tu use for random number generation [OPTIONAL]

% The geration is done using a rayleigh distribution (2D gaussian
% marginalized over rotation), then a sharp cut at radius 2 and a
% coordinate transformation t = 2 sin(r/2)

error(nargchk(3,4,nargin))
if nargin == 4
  randn('state',seed)
end

x=zeros(N*M,1);
maxLoopNb = 100;
iLoop = 1;
curPosX = 1;
while iLoop < maxLoopNb && curPosX <= N*M
  r=random('rayl',sqrt(1/k),N*M,1);
  r=r(r<=2);
  t=2*asin(r/2);
  nValCopy=min(length(t),N*M-curPosX+1);
  x(curPosX:(curPosX+nValCopy-1))=t(1:nValCopy);
  curPosX=curPosX+nValCopy;
  iLoop = iLoop + 1;
end

if curPosX <= N*M
  error(['Maximal number of iteration (' num2str(maxLoopNb) ...
         ') to generate fisher numbers exceeded. Increase maxLoopNb,' ...
         'reduce N*M or modify generator.'])
elseif curPosX > N*M + 1
  error(['Something wrong happened, vector filling iterator: '...
         num2str(curPosX) ' exceed matrix after end position: ' num2str(N*M+1) ])
end

x = reshape(x,[N M]);

