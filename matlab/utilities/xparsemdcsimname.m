function [cFreq,lowerFreq,upperFreq,duration] = xparsemdcsimname(simName)
% XPARSEMDCSIMNAME - Extract useful info from MDC simName.
%
% usage:
%
%     [cFreq,lowerFreq,upperFreq,duration] = xparsemdcsimname(simName)
%
% Types of simName we can handle:
%
% SGC1000Q8d9, SGL1000Q8d9 (with various frequencies)
% WNB_100ms100to1000Hz (with various durations and frequencies)
% RDC_200ms1090Hz, RDL_200ms1090Hz (with various durations and frequencies) 
%
% $Id$

% ---- Checks.
error(nargchk(1,1,nargin));

if (strcmp(simName(1:2),'SG') == true)
   % ---- we have a sine gaussian
   % ---- naming convention is SG<L or C><cFreq>Q<Qvalue>d<dvalue>

   % ---- find 'Q' in simName 
   QIdx = strfind(simName,'Q');
   % ---- extract cFreq 
   cFreq     = str2num(simName(4:QIdx-1));
   lowerFreq = cFreq;
   upperFreq = cFreq;
   duration  = 0; %-- can I figure this out from Qvalue,dvalue

elseif (strcmp(simName(1:3),'WNB') == true) 
   % ---- we have a white noise burst
   % ---- naming convention is WNB_<duration>ms<lowerFreq>to<upperFreq>Hz

   % ---- find '_' in simName
   underscoreIdx = strfind(simName,'_');   
   % ---- find 'ms' in simName
   msIdx = strfind(simName,'ms');   
   % ---- find 'to' in simName
   toIdx = strfind(simName,'to');
   % ---- find 'Hz' in simName
   HzIdx = strfind(simName,'Hz');

   lowerFreq = str2num(simName(msIdx+2:toIdx-1)); 
   upperFreq = str2num(simName(toIdx+2:HzIdx-1)); 
   cFreq     = (lowerFreq + upperFreq) / 2;
   duration  = str2num(simName(underscoreIdx+1:msIdx-1)); 

elseif (strcmp(simName(1:2),'RD') == true)
   % ---- we have a ringdown
   % ---- naming convention is RD<L or C>_<duration>ms<cFreq>Hz

   % ---- find '_' in simName
   underscoreIdx = strfind(simName,'_');   
   % ---- find 'ms' in simName
   msIdx = strfind(simName,'ms');   
   % ---- find 'Hz' in simName
   HzIdx = strfind(simName,'Hz');
   
   cFreq     = str2num(simName(msIdx+2:HzIdx-1)); 
   lowerFreq = cFreq;
   upperFreq = cFreq;
   duration  = str2num(simName(underscoreIdx+1:msIdx-1)); 

else
   error('We have not recognised the type of simName')
end 		

% ---- Done.
return

