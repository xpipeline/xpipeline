function [Z,dZ] = segunion(X,dX,Y,dY)
% SEGUNION - compute the union of two segment lists
%
% usage:
%
%   [Z,dZ] = segunion(X,dX,Y,dY)
%
% SEGUNION returns the union of times in (X,dX) and (Y,dY).
%  
%  X     Vector of start times [sec] of first segment list.
%  dX    Vector of durations [sec] of first segment list.
%  Y     Vector of start times [sec] of second segment list.
%  dY    Vector of durations [sec] of second segment list.
%
%  Z     Vector of start times [sec] of output segment list.
%  dZ    Vector of durations [sec] of output segment list.
%
% The segments (Z,dZ) are the union of (X,dX) with (Y,dY); i.e,
% OR[(X,dX),(Y,dY)]. They will be column vectors regardless of the shape of the
% inputs.
%
% See also SEGDIFF, Coincidence2, ComplementSegmentList.

% ---- Sanity checks.
if length(X)~=length(dX)
    error('Start time and duration lists of different length for first input segments X,dX.');
end
if length(Y)~=length(dY)
    error('Start time and duration lists of different length for second input segments Y,dY.');
end

% ---- Force column vectors.
X  = X(:);
dX = dX(:);
Y  = Y(:);
dY = dY(:);

% ---- Compute the union of X with Y by running output through
%      concatenatesegmentlist; this also makes sure the segments are "clean".
tmp = concatenatesegmentlist([X;Y],[dX;dY]);
if ~isempty(tmp)
    Z  = tmp(:,1);
    dZ = tmp(:,2);
else
    Z  = [];
    dZ = [];
end
