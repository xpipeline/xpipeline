
% ---- utility code to apply scaling factors to XSphRad triggers when switching from one FFT analysis to another
% ---- VPredoi 2015

injScales = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
myFolders = {'output/simulation_adi-a_','output/simulation_adi-b_','output/simulation_adi-c_','output/simulation_adi-d_','output/simulation_adi-e_'};


for i=1:length(myFolders)
	for j=1:length(injScales)
		myFile = [myFolders{i} num2str(injScales(j)) '_0_0_merged.mat'];
		s=load(myFile);
                for m=1:length(s.clusterInj)
                  if ~isempty(s.clusterInj(m).likelihood)
                    fft = s.analysisTimesCell{1};
                    s.clusterInj(m).significance = (fft^2.0)*s.clusterInj(m).significance;
                    s.clusterInj(m).likelihood(:,1) = (fft^4.0)*s.clusterInj(m).likelihood(:,1);
                    s.clusterInj(m).likelihood(:,2) = (fft^2.0)*s.clusterInj(m).likelihood(:,2);
                    s.clusterInj(m).likelihood(:,3) = (fft^2.0)*s.clusterInj(m).likelihood(:,3);
                    s.clusterInj(m).likelihood(:,4) = (fft^2.0)*s.clusterInj(m).likelihood(:,4);
                    s.clusterInj(m).likelihood(:,5) = (fft^2.0)*s.clusterInj(m).likelihood(:,5);
                    s.clusterInj(m).likelihood(:,6) = (fft^2.0)*s.clusterInj(m).likelihood(:,6);
                    s.clusterInj(m).likelihood(:,7) = (fft^2.0)*s.clusterInj(m).likelihood(:,7);
                    s.clusterInj(m).likelihood(:,8) = 0.0*s.clusterInj(m).likelihood(:,8);
                    s.clusterInj(m).likelihood(:,9) = 0.0*s.clusterInj(m).likelihood(:,9);
                    s.clusterInj(m).likelihood(:,10) = (fft^2.0)*s.clusterInj(m).likelihood(:,10);
                    s.clusterInj(m).likelihood(:,11) = (fft^2.0)*s.clusterInj(m).likelihood(:,11);
                    s.clusterInj(m).likelihood(:,12) = (fft^2.0)*s.clusterInj(m).likelihood(:,12);
                    s.clusterInj(m).likelihood(:,13) = (fft^2.0)*s.clusterInj(m).likelihood(:,13);
                  end
                end
                save(myFile,'-struct','s');
		fprintf(1, 'Done reading, scaling likelihoods and saving %s\n', myFile);
	end
end
