function [inj, headerName, headerType] = readburstxmllogfile(fileName)
% readburstxmllogfile - read an xml formatted burst injection log file.
%
% usage: 
%
% [inj, names, types] = readburstxmllogfile(fileName)
%
% fileName  String. Name of the xml log file to be read.
%
% inj       Struct. Each field corresponds to one of the "Column" entries in the
%           xml file's sim_burst:table.
% names     Cell array of strings. Names of the Columns.
% types     Cell array of strings. Data types of the Columns.
%
%
% Empty entries (columns) in the burst xml file are returned as NaN (for numeric
% data types) or '' (for strings). All numeric data is returned as double
% arrays.
%
% $Id$

% ---- Open the file and read the first line to start the parsing.
fid = fopen(fileName);
tline = strtrim(fgetl(fid));

% ---- Read number of lines in the file as an upper limit on the number of
%      injections, so we can assign storage.
[status, result] = system(['wc -l ' fileName]);
numLines = strsplit(strtrim(result));
numLines = str2num(numLines{1});
if numLines>1e5
    disp(['Log file has ' num2str(numLines) ' lines. Parsing it may take several minutes.']);
end

% ---- String to keep track of where we are in the file.
status = 'pre-header';

% ---- Parse given line.
while ischar(tline)

    % ---- Remove any leading or trailing space.
    tline = strtrim(tline);
 
    switch status

        case 'pre-header'

            % ---- We've not yet reached the start of the sim_burst table.
            if strcmp(tline,'<Table Name="sim_burst:table">')
                status = 'header';
                headerName = cell(1);
                headerType = cell(1);
            end

        case 'header'
            
            if strcmp(tline(1:7),'<Column')
                % ---- Parse column header line.
                [headerName, headerType] = parsecolumnline(tline,headerName,headerType);
            elseif strcmp(tline(1:7),'<Stream')
                % ---- Finished reading columns and about to start the data array.
                [formatStr, strIdx, doubleIdx] = xmlformatstr(headerType);
                numCols = length(headerName);
                data = cell(numLines,numCols);
                dataRow = 0;
                status = 'data';
            end
            
        case 'data'
            
            % ---- Check that we haven't hit the end of the data array.
            if strcmp(tline(1:9),'</Stream>')
                status = 'finished';
            else
                dataRow = dataRow + 1;
                try 
                    % dataLine = parsedataline(tline,doubleIdx);
                    data(dataRow,:) = parsedataline(tline,numCols,doubleIdx);
                catch
                    dataRow
                    tline
                    parsedataline(tline,numCols,doubleIdx)
                end
            end
            
        case 'finished'
            
            break

    end
    
    tline = fgetl(fid);    
    
end
    
fclose(fid);

% ---- Delete unused rows in data cell array.
if dataRow < size(data,1)
    data = data(1:dataRow,:);
end

% ---- Convert cell array to struct.
inj = struct;
for ii=1:numCols
    if strfind(doubleIdx,ii)
        inj = setfield(inj,headerName{ii},cell2mat(data(:,ii)));
    else
        inj = setfield(inj,headerName{ii},data(:,ii));
    end
end


return


% ------------------------------------------------------------------------------
%   Helper functions.
% ------------------------------------------------------------------------------

function [headerName, headerType] = parsecolumnline(tline,headerName,headerType)

words = strsplit(tline(2:end-2));

if strcmp(words{1},'Column')

    % ---- For the first column the header cells will contain only [].
    if isempty(headerName{1}) & isempty(headerType{1})
        typeWords         = strsplit(strtrim(strrep(strrep(words{2},'"',' '),':',' ')));
        headerType{1}     = typeWords{end};
        nameWords         = strsplit(strtrim(strrep(strrep(words{3},'"',' '),':',' ')));
        headerName{1}     = nameWords{end};
    else
        typeWords         = strsplit(strtrim(strrep(strrep(words{2},'"',' '),':',' ')));
        headerType{end+1} = typeWords{end};
        nameWords         = strsplit(strtrim(strrep(strrep(words{3},'"',' '),':',' ')));
        headerName{end+1} = nameWords{end};
    end
    
end

return


% ------------------------------------------------------------------------------

function [formatStr, strIdx, doubleIdx] = xmlformatstr(headerType)

formatStr = '';
strIdx    = [];
doubleIdx = [];
for ii=1:length(headerType)
    % headerType{ii}
    switch headerType{ii}
        case {'lstring','char'}
            formatStr = [formatStr ' %s'];
            strIdx = [strIdx ii];
        % case {'int_4s','real_8','int_8u'}  %-- just use double for all numerics ...
        otherwise
            formatStr = [formatStr ' %f'];
            doubleIdx = [doubleIdx ii];
    end
end

return


% ------------------------------------------------------------------------------

function data = parsedataline(tline,numCols,doubleIdx)

% ---- Parse line.
idx = strfind(tline,',');
data = cell(1,length(idx));
for ii=1:length(idx)
    if ii==1
        data{ii} = tline(1:idx(ii)-1);
    else
        data{ii} = tline(idx(ii-1)+1:idx(ii)-1);
    end
    if strfind(doubleIdx,ii)
        % ---- Convert numeric data to doubles.
        data{ii} = str2double(data{ii});
    else
        % ---- Strip out double quotes from string data.
        data{ii} = strrep(data{ii},'"','');
    end
end
% ---- The last data line does not end with a comma, so the last element will
%      have been missed by the for loop above. Add it.
if length(idx) == numCols-1
    data{ii+1} = tline(idx(ii)+1:end);
    if strfind(doubleIdx,ii+1)
        data{ii+1} = str2double(data{ii+1});
    end
end

return



