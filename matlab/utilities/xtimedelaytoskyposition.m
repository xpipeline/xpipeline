function [theta_zp,phi_zp,theta_zm,phi_zm,idx_keep] = xtimedelaytoskyposition(delay,sites,verbose)
% XTIMEDELAYTOSKYPOSITION - inverts inter-site delay times to find the two
%   potential sky positions of the source in earth-based coordinates.  
%
% Usage: 
%
%  [theta_zp,phi_zp,theta_zm,phi_zm] = xtimedelaytoskyposition(delay,sites,verbose)
%
%  delay              Array: 
%                     First column contains delay time between site1
%                     and site2 (t_site2 - t_site1).  
%                     Second column contains delay time between site1
%                     and site3 (t_site3 - t_site1).  
%                     Other columns are ignored.
%  sites              Cell array. Ordered list of sites, e.g.,
%                     {'H','L','V'} where H is site1, L is site2, etc.
%                     Must have at least three sites.
%  verbose            Optional.  If 1, plots are made showing sky grid.
%                     Default 0.
%
%  Only the first two delays are used for the triangulation.  
%  For each set of delays we calculate the 2 sky positions that yield the
%  input time delays. The 2 points will be symmetric about the plane of
%  detectors. We define "above the plane" as the direction of 
%  cross(site2-site1,site3-site1).
%
%  theta_zp,phi_zp    Vectors. Earth based coordinates of source lying
%                     ABOVE plane of detectors. 
%  theta_zm,phi_zm    Vectors. Earth based coordinates of source lying
%                     BELOW plane of detectors. 
%
%  Note: When the input time delays lead to a non-physical sky position
%  (e.g. due to timing or roundoff errors) the code returns a ``best
%  guess'' position.  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input args
error(nargchk(2, 3, nargin));
if (nargin<3)
    verbose = 0;
end

% ---- Check for 3+ sites, 2+ delays.
if length(sites) < 3
    error('Need at least three sites for triangulation.');
end
if size(delay,2) < 2
    error('Need at least two time delays for triangulation.');
end

% ---- Keep only first two delays, first three sites.
delay = delay(:,1:2);
sites = sites(1:3);

% ---- Speed of light (in m/s).
speedOfLight = 299792458;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Get cartesian Earth-based coordinates (m) of detector 
%                         vertex for each site.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nSites = length(sites);
for iSite = 1:nSites
    % ---- The detector is matched by the first character of the channel name.
    det = LoadDetectorData(sites{iSite}(1));
    siteVertex(iSite,:) = det.V';
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Construct vector joining pairs of sites.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

iPair = 1;
iSite1 = 1; % -- site1
for iSite2 = 2:length(sites) % -- site2,3 
    % ---- Construct structure listing names and displacements between
    %      pairs of sites.
    pairNames{iPair} = [sites{iSite1} sites{iSite2}];
    site1ToSite2(iPair,:) = siteVertex(iSite2,:) -  siteVertex(iSite1,:);

    % ---- Calc max delay for site2-site1 and site3-site1.
    delay_max(iPair) = norm(site1ToSite2(iPair,:)) ./ speedOfLight;  

    iPair = iPair + 1;
end

delay_max_array = repmat(delay_max,size(delay,1),1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Find axes in plane of detectors in earth-based coordinates.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- x axis is along site1site2.  y axis is orthogonal to x and in
%      detector plane.  z axis is orthogonal to both.
prime_x = site1ToSite2(1,:) / norm(site1ToSite2(1,:));

% ---- z is perpendicular to site1site2 (x') and site1site3.
prime_z = cross(prime_x,site1ToSite2(2,:));
prime_z = prime_z / norm(prime_z);

% ---- y = z * x.
prime_y = cross(prime_z,prime_x);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         Project time delay onto unit circle in plane of detectors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- This value should lay between +/-1. 
proj = - delay ./ delay_max_array;


% ---- For simplicity define orthogonal axes in plane of unit
%      circle such that site1site2 is along x-axis, site1 at the -ve end.
% ---- Calculate angle between site1site2 (x) and site1site3.
% ---- Invert dot product to calculate opening angle. 
theta_rad = acos(dot(site1ToSite2(1,:),site1ToSite2(2,:))/...
  (norm(site1ToSite2(1,:))*norm(site1ToSite2(2,:))));

% ---- Projected onto unit circle in plane of detectors:
%      we know that to achieve the measured time delay between
%      site1site2 that our source must lie on a line described by
%      x = proj(1). 
%      Similarly, to achieve the measured time delay between
%      site1site3 our source must lie on a tangent to the vector joining
%      site1site3 at a point a distance proj(2) along it. 

% ---- Tangent to site1site3.
gradient = -tan(pi/2 - theta_rad);

% ---- y-intercept of tangent to site1site3.
intercept = proj(:,2) ./ sin(theta_rad);

% ---- Find intersection of tangents to site1site2 and site1site3.
%      i.e., point where tangent to HV crosses x = proj(1).
intersect_x = proj(:,1);
intersect_y = (gradient * proj(:,1)) + intercept;
intersect_z = (1 - intersect_x.^2 - intersect_y.^2).^0.5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    Some plots and debugging
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Unit circle.
psi = 0:360;
circ_x = sind(psi);
circ_y = cosd(psi);

if verbose == 1
    figure()
    clf;
    subplot(1,2,1)
    plot(intersect_x,intersect_y,'b+');
    hold on;
    title('unit circle in plane of ifos')
    plot(circ_x,circ_y,'r-');
    grid on;
    xlabel('x')
    ylabel('y')
    axis([-1.5 1.5 -1.5 1.5]);
    axis square
    legend('all points')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        KLUDGE: Handle case where points lie *outside* unit circle.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- If (intersect_x,intersect_y) lies outside unit circle then the
%      input delay times correspond to a non-physical sky position.
%      We report NaNs.

% ---- Soln1: simply discard points outside unit circle.
%      This seems to lead to drop in efficiency for some sky positions:
%      xtestchooseskylocations(253.217,-25,874780000.0,5,{'H','L','V'},1e-4,5000)
%      efficiency ~ 50%
idx = find((intersect_x.^2 + intersect_y.^2) > 1);
warning([num2str(length(idx)) ' points out of ' num2str(size(delay,1)) ' have unphysical delays.'])
intersect_x(idx) = [];
intersect_y(idx) = [];
intersect_z(idx) = [];
idx_keep = setdiff([1:size(delay,1)],idx);

% ---- Soln2: reflect points back into unit circle
% ---- Find point along radius that subtends same angle with x-axis as
%      test point.
%      This still seems to lead to drop in efficiency for some sky positions:
%      xtestchooseskylocations(253.217,-25,874780000.0,5,{'H','L','V'},1e-4,5000)
%      efficiency ~ 90%
% angle = atan2(intersect_y,intersect_x);
% x_rad = cos(angle); 
% y_rad = sin(angle); 
% idx = (intersect_x.^2 + intersect_y.^2 > 1);
% intersect_x(idx) = 2*x_rad(idx) - intersect_x(idx);
% intersect_y(idx) = 2*y_rad(idx) - intersect_y(idx);
% intersect_z(idx) = (1 - intersect_x(idx).^2 - intersect_y(idx).^2).^0.5;

% intersect_x1 =  intersect_x;
% intersect_x2 =  intersect_x;
% intersect_y1 =  intersect_y;
% intersect_y2 =  intersect_y;
% intersect_z1 =  intersect_z;
% intersect_z2 = -intersect_z;

% ---- Soln3: Nudge points back towards unit circle;
% % ---- Experiment
% idx = abs(intersect_x) > 1;
% intersect_x(idx) = 1 * sign(intersect_x(idx)); 
% idx = abs(intersect_y) > 1;
% intersect_y(idx) = 1 * sign(intersect_y(idx)); 
% 
% idx = (intersect_x.^2 + intersect_y.^2 > 1);
% intersect_x1(idx) = intersect_x(idx);
% intersect_y1(idx) = sign(intersect_y(idx)) .* (1-intersect_x(idx).^2 ).^0.5;
% intersect_z1(idx) = 0;
% 
% intersect_x2(idx) = sign(intersect_x(idx)) .* (1-intersect_y(idx).^2 ).^0.5;
% intersect_y2(idx) = intersect_y(idx);
% intersect_z2(idx) = 0;

% % ---- Soln4: Map points onto closest point on unit circle;
% radius = (intersect_x.^2 + intersect_y.^2).^0.5;
% idx = radius > 1;
% intersect_x(idx) = intersect_x(idx) ./ radius(idx);
% intersect_y(idx) = intersect_y(idx) ./ radius(idx);
% intersect_z(idx) = 0;
% 
% % ---- Make two copies of each point: one above plane and one below.
% intersect_x1 = intersect_x;
% intersect_x2 = intersect_x;
% intersect_y1 = intersect_y;
% intersect_y2 = intersect_y;
% intersect_z1 = intersect_z;
% intersect_z2 = -intersect_z;

% ---- Optional plot.
if verbose == 1
    subplot(1,2,2)
    plot([intersect_x],[intersect_y],'b+');
    hold on;
    title('unit circle in plane of ifos')
    plot(circ_x,circ_y,'r-');
    grid on;
    xlabel('x')
    ylabel('y')
    axis([-1.5 1.5 -1.5 1.5]);
    axis square
    legend('retained points')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Convert from detector-plane coordinates to Earth-based.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

source_earth_zp = intersect_x * prime_x + intersect_y * prime_y + intersect_z * prime_z;
source_earth_zm = intersect_x * prime_x + intersect_y * prime_y - intersect_z * prime_z;

% % ---- If we had points outside the unit circle that were nudged back
% %      in we are likely to have repeated sky position, we get rid
% %      of repeats here - prob a better way to do this.
% source_earth_zp = unique(source_earth_zp,'rows');
% source_earth_zm = unique(source_earth_zm,'rows');

% ---- Optional plot.
if verbose == 1
   figure();
   plot3(0,0,0,'k+'); 
   hold on;
   grid on;
   plot3(source_earth_zp(:,1),source_earth_zp(:,2),source_earth_zp(:,3),'b+'); 
   plot3(source_earth_zm(:,1),source_earth_zm(:,2),source_earth_zm(:,3),'r+'); 
   [xs,ys,zs]=sphere;
   surf(xs,ys,zs);  
   axis equal
end

% ---- Convert cartesian earth-based coordinates to theta, phi.
%      source_earth should have norm = 1;
theta_zp = acos(source_earth_zp(:,3));
phi_zp   = atan2(source_earth_zp(:,2),source_earth_zp(:,1)); 
theta_zm = acos(source_earth_zm(:,3));
phi_zm   = atan2(source_earth_zm(:,2),source_earth_zm(:,1)); 

% ---- Done.
return
