function [theta_earth, phi_earth] = optimalskytiling(network,dt,verbose)
% OPTIMALSKYTILING: Optimal timing-based sky tiling for 3-detector network.
%
% OPTIMALSKYTILING produces a tiling of the sky that is optimal for a
% three-site network, in the sense that it minimises the number of grid
% points while guaranteeing a maximum allowed timing error for any sky
% position.
% 
% usage:
%
%   [theta, phi] = optimalskytiling(network,dt,verbose)
% 
%   network   Cell array of strings.  Each element is a detector site 
%             recognized by LOADDETECTORDATA.  Must have 3 elements.
%             Example: {'H','L','V'}
%   dt        Scalar.  Maximum allowed timing error along any baseline [s].
%   verbose   Optional flag.  If 1, produce plots and messages to stdout. 
%             If 2, also save all internal variables to a file called
%             'optimalskytiling.mat' in the local directory.  (This is
%             useful for debugging ...).  Default 0.
%
%   theta     Vector of polar coordinates of sky grid points.
%   phi       Vector of azimuthal coordinates of sky grid points.
%
% The sky grid (theta,phi) are in Earth-fixed coordinates, as defined in 
% COMPUTEANTENNARESPONSE.
%
% WARNING: This function is not 100% robust.  For example, the code to
% handle the boundary of the sky in time-delay space can occasionally put a
% grid point at some wild time delay due to interpolation error, which then
% gets mapped to lie along the baseline of the first two detectors.  You
% should check the plots for any funny looking points before using the
% grid.
%
% Requires functions LOADDETECTORDATA, CARTESIANPOINTINGVECTOR, LINFIT, 
% ROTATEVECTOR.  
%
% $Id$

% Note to developers: This script uses internally "x", "y", "z" as
% shorthands for the rescaled time delays along the three baselines.  Don't
% confuse these with the (X,Y,Z) Cartesian coordinates you might use to
% describe the network orientation.


% -------------------------------------------------------------------------
%    Preparatory.
% -------------------------------------------------------------------------

% ---- Argument checks.
error(nargchk(2,3,nargin));
if nargin<3
    verbose = 0;
end
if ~isscalar(dt)
    error('Input dt must be a scalar.');
end
if length(network)~=3 || ~iscell(network)
    error('Input network must be a three-element cell array.');
end

% ---- Useful constants.
speedOfLight = 299792458;

% ---- Load detector information.  (Internally we'll call the three
%      detectors 'H', 'L', and 'V', but these are just arbitrary labels.) 
H = LoadDetectorData(network{1});
L = LoadDetectorData(network{2});
V = LoadDetectorData(network{3});

% ---- Baseline vectors [m].
bHLvec = L.V - H.V;
bLVvec = V.V - L.V;
bVHvec = H.V - V.V;
% ---- Baseline vectors [s].
bHLvec = bHLvec / speedOfLight;
bLVvec = bLVvec / speedOfLight;
bVHvec = bVHvec / speedOfLight;
% ---- Baseline lengths [s].
bHL = norm(bHLvec);
bLV = norm(bLVvec);
bVH = norm(bVHvec);
% ---- Unit baseline pointing vectors.
bHLhat = bHLvec / bHL;
bLVhat = bLVvec / bLV;
bVHhat = bVHvec / bVH;


% -------------------------------------------------------------------------
%    Construct boundary of sky in time-delay space.
% -------------------------------------------------------------------------

% ---- Choose coordinates such that H is at origin, L is on Z axis, V is in
%      X-Z plane.  Then sky points in the plane of the detectors are the
%      intersection of the unit sphere and the X-Z plane: phi=0, theta =
%      [0,pi] and phi = pi, theta = (0,pi).  
% ---- Polar angle of V location in this system.
theta_V = acos(- bHLhat' * bVHhat);
% ---- Angle for LV delays.
theta_LV = acos(bHLhat' * bLVhat);
%
dtr = 0.00005;
theta_ring = [0:dtr:2-dtr]'*pi;
%
delayHL = - bHL * cos(theta_ring);              % x := (t_L - t_H)/dt
delayLV = - bLV * cos(theta_ring - theta_LV);   % y := (t_V - t_L)/dt
delayVH =   bVH * cos(theta_ring - theta_V);    % z := (t_H - t_V)/dt

% ---- Convert ring time delays into hexagonal grid coordinates.  Work in
%      (x,y) since z redundant given x,y.
x_ring = delayHL / dt;
y_ring = delayLV / dt;
% ---- Corresponding (m,n) coordinates (not integers).
m_ring = (2*x_ring + y_ring)/3;
n_ring = ( -x_ring + y_ring)/3;
% ---- Separate exact (m,n) coordinates into integer and fractional parts.
m_I = round(m_ring);
n_I = round(n_ring);
m_f = m_ring - m_I;
n_f = n_ring - n_I;


% -------------------------------------------------------------------------
%    Find centers of all tiles that are crossed by the boundary.
% -------------------------------------------------------------------------

% ---- For most points that tile center is at (m_I,n_I).  Walk through
%      special cases and correct where necessary. 
m_boundary = m_I;
n_boundary = n_I;
ind = find((m_f-n_f<=0) & (m_f+2*n_f>1));
if ~isempty(ind)
    m_boundary(ind) = m_I(ind);
    n_boundary(ind) = n_I(ind)+1;
end    
ind = find((m_f-n_f>0) & (2*m_f+n_f>1));
if ~isempty(ind)
    m_boundary(ind) = m_I(ind)+1;
    n_boundary(ind) = n_I(ind);
end    
ind = find((m_f-n_f<=0) & (2*m_f+2*n_f<-1));
if ~isempty(ind)
    m_boundary(ind) = m_I(ind)-1;
    n_boundary(ind) = n_I(ind);
end    
ind = find((m_f-n_f>0) & (m_f+2*n_f<-1));
if ~isempty(ind)
    m_boundary(ind) = m_I(ind);
    n_boundary(ind) = n_I(ind)-1;
end    
% ---- Compute also boundary grid coordinates in delay space.
x_boundary = m_boundary - n_boundary; 
y_boundary = m_boundary + 2*n_boundary;


% -------------------------------------------------------------------------
%    Find boundary tiles with centers outside physical boundary.
% -------------------------------------------------------------------------

% ---- Find all grid points falling outside physically allowed range.
phi_boundary = atan2(y_boundary,x_boundary);
r_boundary = (x_boundary.^2+y_boundary.^2).^0.5;
phi_ring = atan2(y_ring,x_ring);
r_ring = (x_ring.^2+y_ring.^2).^0.5;
% ---- Interpolate r(phi) to r(phi_full).
r_interp = interp1(phi_ring,r_ring,phi_boundary);
indb_out = r_boundary > r_interp;


% -------------------------------------------------------------------------
%    Construct full list of all tiles on / within boundary.
% -------------------------------------------------------------------------

% ---- Now construct full grid (including some boundary points that lie
%      outside physically allowed time-delay range).
fullgrid = [];
n_min = min(n_boundary);
n_max = max(n_boundary);
for n = n_min:n_max
    ind = find(n_boundary==n);
    m_min = min(m_boundary(ind));
    m_max = max(m_boundary(ind));
    m = [m_min:m_max]';
    fullgrid = [fullgrid; m n*ones(size(m))];
end
m_full = fullgrid(:,1);
n_full = fullgrid(:,2);
clear fullgrid
% ---- Compute also full grid coordinates in delay space.
x_full = m_full - n_full; 
y_full = m_full + 2*n_full;


% -------------------------------------------------------------------------
%    Find all tiles in full list with centers outside boundary.
% -------------------------------------------------------------------------

% ---- Find all grid points falling outside physically allowed range.
phi_full = atan2(y_full,x_full);
r_full = (x_full.^2+y_full.^2).^0.5;
phi_ring = atan2(y_ring,x_ring);
r_ring = (x_ring.^2+y_ring.^2).^0.5;
% ---- Interpolate r(phi) to r(phi_full).
r_interp = interp1(phi_ring,r_ring,phi_full);
ind_out = find(r_full > r_interp);


% -------------------------------------------------------------------------
%    For each boundary tile outside the physical range, find a new tile
%    inside that still provides the required coverage.
% -------------------------------------------------------------------------

% ---- This is going to get messy.

% ---- Initialize storage for these "supplemental" (new) grid points.
x_supp = [];
y_supp = [];
% ---- Walk over tiles outside allowed range, one by one, and find
%      supplemental tile to replace it.
if ~isempty(ind_out)
    for ii = 1:length(ind_out)
        % [m_full(ind_out(ii)) n_full(ind_out(ii))]
        % ----Find all points on the ring which are in this tile.
        ind = find(m_boundary==m_full(ind_out(ii)) & n_boundary==n_full(ind_out(ii)));
        % ---- Kludgy bit of code to handle case where boundary wraps
        %      around from 2*pi->0.
        if (min(ind)==1 & max(ind)==length(m_boundary))
            jj = find(diff(ind)~=1);
            iprev = ind(jj+1)-1;
            inext = ind(jj)+1;
        elseif (min(ind)==1)
            iprev = length(m_boundary);
            inext = max(ind)+1;
        elseif (max(ind)==length(m_boundary))
            iprev = min(ind)-1;
            inext = 1;
        else
            iprev = min(ind)-1;
            inext = max(ind)+1;
        end
        % ---- Find boundary tiles on either side into which ring passes.
        m_prev = m_boundary(iprev);
        n_prev = n_boundary(iprev);
        m_next = m_boundary(inext);
        n_next = n_boundary(inext);
        % ---- Exact (x,y) coordinates of first point outside this tile on
        %      either side.
        x_prev = x_ring(iprev);
        y_prev = y_ring(iprev);
        x_next = x_ring(inext);
        y_next = y_ring(inext);
        % ---- Equation for straight line joining these two points:
        %      ax+by=1, where
        [a1, b1] = linfit(x_prev,y_prev,x_next,y_next);
        slope = -a1/b1;
        % ---- Walk through the special cases.
        diffs = [m_prev-m_full(ind_out(ii)) n_prev-n_full(ind_out(ii)) ...
            m_next-m_full(ind_out(ii)) n_next-n_full(ind_out(ii))];
        if (diffs==[1 0 -1 0]) | (diffs==[1 -1 0 -1]) | (diffs==[-1 0 1 0]) | (diffs==[-1 1 0 1])
            % disp('case 14/23/41/56.');
            % ---- Points on "\" symmetry axis.
            X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [-1/3; 2/3];
            X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [-1/3; 2/3];
        elseif (diffs==[1 0 1 -1]) | (diffs==[0 -1 0 1]) | (diffs==[-1 0 -1 1]) | (diffs==[0 1 0 -1])
            % disp('case 12/36/45/63.');
            % ---- Points on "-" symmetry axis.
            X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [2/3; -1/3];
            X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [2/3; -1/3];
        elseif (diffs==[1 -1 -1 1]) | (diffs==[0 -1 -1 0]) | (diffs==[-1 1 1 -1]) | (diffs==[0 1 1 0])
            % disp('case 25/34/52/61.');
            % ---- Points on "|" symmetry axis.
            X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [1/3; 1/3];
            X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [1/3; 1/3];
        elseif (diffs==[1 0 0 -1]) | (diffs==[-1 0 0 1])
            % disp('case 13/46.');
            if slope > 1
                % ---- Points on "\" symmetry axis.
                X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [-1/3; 2/3];
                X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [-1/3; 2/3];
            elseif slope < -2
                % ---- Points on "-" symmetry axis.
                X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [2/3; -1/3];
                X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [2/3; -1/3];
            else
                disp('Error with 13/46 slope.');
            end
        elseif (diffs==[1 -1 -1 0]) | (diffs==[-1 1 1 0])
            % disp('case 24/51.');
            if slope > 0
                % ---- Points on "\" symmetry axis.
                X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [-1/3; 2/3];
                X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [-1/3; 2/3];
            elseif slope < 0
                % ---- Points on "|" symmetry axis.
                X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [1/3; 1/3];
                X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [1/3; 1/3];
            else
                disp('Error with 24/51 slope.');
            end
        elseif (diffs==[0 -1 -1 1]) || (diffs==[0 1 1 -1])
            % disp('case 35/62.');
            if slope > -1
                % ---- Points on "|" symmetry axis.
                X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [1/3; 1/3];
                X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [1/3; 1/3];
            elseif slope < -1
                % ---- Points on "-" symmetry axis.
                X1 = [x_full(ind_out(ii)); y_full(ind_out(ii))] + [1 -1; 1 2] * [2/3; -1/3];
                X2 = [x_full(ind_out(ii)); y_full(ind_out(ii))] - [1 -1; 1 2] * [2/3; -1/3];
            else
                disp('Error with 35/62 slope.');
            end
        else
            disp('Error: unrecognized configuration of boundary crossing a tile.')
            disp(['  ii          = ' num2str(ii)]);
            disp(['  ind_out(ii) = ' num2str(ind_out(ii))]);
            disp(['  ind         = ' num2str(ind)]);
            disp(['  diffs       = ' num2str(diffs)]);
            break
        end
        % ---- Equation of symmetry axis.
        [a2, b2] = linfit(X1(1),X1(2),X2(1),X2(2));
        % ---- Intersection with ring boundary.
        x_int = ( b2-b1)/(a1*b2-a2*b1);
        y_int = (-a2+a1)/(a1*b2-a2*b1);
        x_supp = [x_supp; x_int];
        y_supp = [y_supp; y_int];
    end
end
m_supp = (2*x_supp + y_supp)/3;
n_supp = ( -x_supp + y_supp)/3;


% -------------------------------------------------------------------------
%    Final grid.
% -------------------------------------------------------------------------

% ---- In (m,n) space.
m_full(ind_out) = [];
m_grid = [m_full; m_supp];
n_full(ind_out) = [];
n_grid = [n_full; n_supp];

% ---- In time-delay (x,y) space.
x_full(ind_out) = [];
x_grid = [x_full; x_supp];
y_full(ind_out) = [];
y_grid = [y_full; y_supp];

% ---- In network-specialised coordinates; see
%      https://alexandria.astro.cf.ac.uk/dokuwiki/doku.php?id=collaborations:lvc:ptf:all_sky
% ---- Polar angle theta.  Do in roundabout way to be robust against
%      roundoff error giving imaginary angle.
% theta = acos(-x_full*dt/bHL);
arg = -x_full*dt/bHL;
% ---- Find points outside +/-1 and reset to +/-1.
bad = find(abs(arg)>1);
arg(bad)=arg(bad)/abs(arg(bad)); 
theta = acos(arg);
% ---- Now compute phi angle.
arg = - (bHL*(x_full+y_full)*dt - bVH*x_full*dt*cos(theta_V) ) ...
        ./ (bVH*(bHL^2-x_full.^2*dt^2).^0.5*sin(theta_V));
bad2 = find(abs(arg)>1);
arg(bad2)=arg(bad2)/abs(arg(bad2)); 
phi = acos(arg);
% ---- Original "bad" points are on the Z axis, so can use phi=0 for those.
phi(bad) = 0; 
% ---- Construct Cartesian unit vectors for each grid point.
V1 = CartesianPointingVector(phi,theta);  %-- half-sky
V2 = CartesianPointingVector(-phi,theta); %-- other half-sky
%
% ---- Handle ring/boundary tiles separately, since these are particularly
%      prone to roundoff errors. 
% theta_supp = acos(-x_supp*dt/bHL);
arg = -x_supp*dt/bHL;
% ---- Find points outside +/-1 and reset to +/-1.
bad = find(abs(arg)>1);
arg(bad)=arg(bad)/abs(arg(bad)); 
theta_supp = acos(arg);
% ---- Prone to roundoff error giving imaginary results.
% phi = acos(- (bHL*(x_supp+y_supp)*dt - bVH*x_supp*dt*cos(theta_V) ) ... 
%               ./(bVH*(bHL^2-x_supp.^2*dt^2).^0.5*sin(theta_V)) );
% ---- Kludgy, but should be more robust.
phi_supp = pi*ones(size(theta_supp));
k = find( ((x_supp+y_supp)*dt < -bVH*cos(theta_V)) | ...
           ( ...
              ((x_supp+y_supp)*dt > -bVH*cos(theta_V)) & ...
              ((x_supp+y_supp)*dt < -bVH*cos(pi-theta_V)) & ...
              (theta_supp > pi/2) ...
           ) ...
        );
phi_supp(k) = 0;
V0 = CartesianPointingVector(phi_supp,theta_supp);  %-- boundary ring

% ---- Pack into single array.
omega = [V1; V0; V2];

% ---- Rotate into standard Earth-fixed coordinates.
%      Normal vector to the detector plane in right-handed H->L->V sense is
%      in the y direction in our specialised coordinates.  Rotate grid so
%      this normal points in the right direction in standard coordinates.
% ---- Zenith in Earth-standard coordinates.
K = cross(bHLvec,bLVvec);
K = K/norm(K);
theta_K = acos(K(3));
phi_K = atan2(K(2),K(1));

Z1 = RotateVector(omega',[0;0;1],phi_K-pi/2);
newaxis = cross([cos(phi_K),sin(phi_K),0],[0,0,1]);
Z2 = RotateVector(Z1,newaxis,pi/2-theta_K);
%
oldHL = [cos(phi_K+pi)*sin(pi/2-theta_K),sin(phi_K+pi)*sin(pi/2-theta_K),cos(pi/2-theta_K)]';
newHL = bHLvec/bHL;
lastangle = mean(cross(oldHL,newHL) ./ K);
%
Z3 = RotateVector(Z2,K,lastangle);
Z3 = Z3';
%
theta_earth = acos(Z3(:,3));
phi_earth = atan2(Z3(:,2),Z3(:,1));

% -------------------------------------------------------------------------
%    Plots & stdout.
% -------------------------------------------------------------------------

if verbose
    
    disp(['Network: ']);
    disp(network);
    disp(['Maximum allowed timing error: ' num2str(dt) ' s.']);
    disp(['Number of grid points:']);
    disp(['  each hemisphere: ' num2str(size(V1,1))]);
    disp(['   detector plane: ' num2str(size(V0,1))]);
    disp(['            total: ' num2str(size(omega,1))]);

    % figure; set(gca,'fontsize',16);
    % plot(m_full,n_full,'g.');
    % grid on; hold on;
    % plot(m_boundary,n_boundary,'b.');
    % plot(m_full(ind_out),n_full(ind_out),'m.');
    % plot(m_ring,n_ring,'r-');
    % plot(m_supp,n_supp,'r+');
    % maxdim = max(abs([m_boundary;n_boundary]));
    % axis([-(maxdim+1) (maxdim+1) -(maxdim+1) (maxdim+1)])
    % axis square
    % xlabel('m');
    % ylabel('n');
    % 
    % figure; set(gca,'fontsize',16);
    % plot(x_full*dt,y_full*dt,'g.');
    % grid on; hold on;
    % plot(x_boundary*dt,y_boundary*dt,'b.');
    % plot(x_full(ind_out)*dt,y_full(ind_out)*dt,'m.');
    % plot(x_ring*dt,y_ring*dt,'r-');
    % plot(x_supp*dt,y_supp*dt,'r+');
    % maxdim = max(abs([x_full;y_full]*dt));
    % axis([-(maxdim*1.1) (maxdim*1.1) -(maxdim*1.1) (maxdim*1.1)])
    % axis square
    % xlabel('x*dt (s)');
    % ylabel('y*dt (s)');
    % 
    % figure; set(gca,'fontsize',16);
    % plot(m_grid,n_grid,'b.');
    % grid on; hold on;
    % plot(m_ring,n_ring,'r-');
    % maxdim = max(abs([m_grid;n_grid]));
    % axis([-(maxdim+1) (maxdim+1) -(maxdim+1) (maxdim+1)])
    % axis square
    % xlabel('m');
    % ylabel('n');

    figure; set(gca,'fontsize',16);
    plot(x_grid*dt,y_grid*dt,'b.');
    grid on; hold on;
    plot(x_ring*dt,y_ring*dt,'r-');
    maxdim = max(abs([x_grid;y_grid]*dt));
    axis([-(maxdim*1.1) (maxdim*1.1) -(maxdim*1.1) (maxdim*1.1)])
    axis square
    xlabel('t_2 - t_1 (s)');
    ylabel('t_3 - t_2 (s)');
    legend('grid points','time delay boundary','Location','NorthOutside')
    %legend('grid points','time delay boundary','Location','Best')

    figure; set(gca,'fontsize',16);
    plot3(V1(:,1),V1(:,2),V1(:,3),'b.')
    grid on; hold on; 
    plot3(V2(:,1),V2(:,2),V2(:,3),'b.')
    plot3(V0(:,1),V0(:,2),V0(:,3),'r.')
    axis square
    title('standard Earth-fixed coordinates')
    xlabel('x')
    ylabel('y')
    zlabel('z')

    % figure; set(gca,'fontsize',16);
    % plot(phi,theta,'b.');
    % grid on; hold on;
    % plot(-phi,theta,'g.');
    % plot(phi_supp,theta_supp,'r.');
    % axis([-4 4 -0.5 4])
    % xlabel('phi');
    % ylabel('theta');

    figure; set(gca,'fontsize',16);
    plot(phi_earth,theta_earth,'b.');
    grid on; hold on;
    axis([-pi pi 0 pi])
    set(gca,'ydir','reverse')
    xlabel('phi');
    ylabel('theta');
    title('standard Earth-fixed coordinates')

end

if verbose==2
    save optimalskytiling.mat
end

% ---- Done.
return
