function p = fisherpdf(k,x)
% FISHERPDF - pdf of the fisher distribution with parameter k
%
% FISHERPDF - returns the value of the probability density function of
%             the fisher distribution for parameter k at coordinate x
%
%                       k    
%          P(x) = ---------------  sin(x) exp( k cos(x) )
%                 exp(k) - exp(-k)
%
%  usage:
%
%  p = fisherpdf(k,x)
%
%  p   Scalar, Matrix. Computed value of the pdf, has the same size as k
%      or x.
%  k   Scalar, Matrix. Parameter of the fisher distributioni.
%      If both k and x are a non scalar matrix their size must be the same.
%  x   Scalar, Matrix. Value at which the pdf is computed.
%      If both k and x are a non scalar matrix their size must be the same.

error(nargchk(2,2,nargin))

p = k./(1-exp(-2*k)).*sin(x).*exp(k.*(cos(x)-1));
