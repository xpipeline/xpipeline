function savetightfigure(h,outfilename)
% SAVETIGHTFIGURE - Save figure without extra white space in margins. 
%
% usage:
%
%   savetightfigure(h,outfilename)
% 
%   h       Figure handle.
%   name    String.  Name of figure file to output.  Note that the figure
%           type is determined from the extension.
%
% Adapted from saveTightFigure.m, with these credits:
%   by ``a grad student"
%   http://tipstrickshowtos.blogspot.com/2010/08/how-to-get-rid-of-white-margin-in.html
%
% $Id $

% ---- Safety margin left in, to avoid clipping bounding box.  This value
%      may not be "safe" in general.
safety = 0.02;

% ---- Get the current axes.
ax = get(h, 'CurrentAxes');

% ---- Make it tight.
ti = get(ax,'TightInset');
% set(ax,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
set(ax,'Position',[ti(1) ti(2) 1-safety-ti(3)-ti(1) 1-safety-ti(4)-ti(2)]);

% ---- Adjust the paper size.
set(ax,'units','centimeters');
pos = get(ax,'Position');
ti = get(ax,'TightInset');
set(h, 'PaperUnits','centimeters');
set(h, 'PaperSize', [pos(3)+ti(1)+ti(3)+10*safety pos(4)+ti(2)+ti(4)+10*safety]);
set(h, 'PaperPositionMode', 'manual');
set(h, 'PaperPosition',[safety safety  pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);

% ---- Save it.
saveas(h,outfilename);

% ---- Done.
return
