function [] = xwriteinjmatfilefromMVAascii(injDirOld, trainingFile, testingFile, injDirNew)
% XWRITEINJMATFILEFROMMVAASCII - merge TMVA results into matlab injection files
%
% XWRITEINJMATFILEFROMMVAASCII reads the ascii files produced by TMVA and
% merges the classification result into pre-existing matlab-formatted
% trigger files for injections. The merging replaces the the original
% X-Pipeline significance with the value of the MVA classifier for each
% trigger. Triggers in the original matlab file which do not have an MVA
% result have their significance set to zero.
%
% usage:
%
% xwriteinjmatfilefromMVAascii(injDirOld, trainingFile, testingFile, injDirNew)
%
%  injDirOld      String. Full path to directory containing the original matlab 
%                 injection files.
%  trainingFile   String. Full path to ascii file output by TMVA for training
%                 injections. This file should have four columns:
%                   MVAScore, injIdx, triggerIdx, injmatfilename. 
%                 Use [], '', or '[]' if no injections were used for training. 
%  testingFile    String. Full path to ascii file output by TMVA for testing
%                 injections. The file format is identical to the trainingFile
%                 file. 
%  injDirNew      String. Full path to the directory where we will put the new
%                 injection matlab files. Can not be the same as injDirOld.
%
% Note: The injection processed mask is not changed, even for injections
% that did not go through MVA processing.  The only place this will cause
% problems is for the SIG TRAIN set, which when aggressively cleaned will
% be missing loud injections and will give erroneously small efficiencies.
% This is not a problem if we don't attempt to use any cuts in the web
% page.
%
% $Id$

% ---- Checks.
error(nargchk(4,4,nargin));
%narginchk(4, 4);

% ---- Error if going to overwrite existing files.
if strcmp(injDirOld,injDirNew)
   error('Do not overwrite original mat files!')
end

% ---- Check for empty training file. This might be input as the empty numeric 
%      array [], and empty string '', or (the tricky one, from the command line)
%      the string '[]'.
if isstr(trainingFile) && strcmp(trainingFile,'[]')
    trainingFile = '';
end

% ---- Concatenate ranking MVAScore, triggerIdx injmatfilename.
if isempty(trainingFile) %-- works for both [] and ''

  % ---- Load signal testing triggers.
  disp('Converting testing triggers only.');
  fid = fopen(testingFile,'r');
  signal_testing  = textscan(fid,'%f%d%d%s');
  fclose(fid);

  MVAScore       = [signal_testing{1}];
  injIdx         = [signal_testing{2}];
  triggerIdx     = [signal_testing{3}];
  injmatfilename = [signal_testing{4}];

else

  disp('Converting both training and testing triggers.');
  % ---- Load signal training triggers.
  fid = fopen(trainingFile,'r');
  signal_training = textscan(fid,'%f%d%d%s');
  fclose(fid);

  % ---- Load signal testing triggers.
  fid = fopen(testingFile,'r');
  signal_testing  = textscan(fid,'%f%d%d%s');
  fclose(fid);

  MVAScore       = [signal_training{1}; signal_testing{1}];
  injIdx         = [signal_training{2}; signal_testing{2}];
  triggerIdx     = [signal_training{3}; signal_testing{3}];
  injmatfilename = [signal_training{4}; signal_testing{4}];

end

% ---- Get list of unique injmatfilenames. Note that injection files
%      containing no surviving triggers (e.g. at very low injection scales
%      do not appear in this list.
injmatfilename_unique = unique(injmatfilename);

% ---- Check that list of injection file names matches expected pattern.  
%      We want to process ALL the files, including those for injection scales so
%      small that no triggers survived. Otherwise the post-processing will fail
%      due to missing injection files. 
for iFile = 1:length(injmatfilename_unique)
    % ---- Dissect each file name.
    fileName = injmatfilename_unique{iFile};
    % ---- Verify that the file name has the expected structure 
    %      'simulation_' <glob> '_' <injectionscale> '_0_0_merged.mat',
    %      e.g. 'simulation_inspcsgcombinedwaveform_1_0_0_merged.mat'.
    errormsg = ['Injection file name does not have expected structure, '...
        'e.g. simulation_inspcsgcombinedwaveform_7_0_0_merged.mat.'];
    % ---- First locate all the underscores and verify there are five.
    idx = strfind(fileName,'_');
    % ---- Verify the first part matches 'simulation_'.
    if ~strcmp(fileName(1:11),'simulation_')
        error(errormsg);
    end
    % ---- Verify the last part matches '_0_0_merged.mat'.
    if ~strcmp(fileName(end-14:end),'_0_0_merged.mat')
        error(errormsg);
    end
    % ---- Extract "glob".
    injGlob = fileName((idx(1)+1):(idx(end-3)-1));
    % ---- Extract injection scale.
    injScale = str2num(fileName((idx(end-3)+1):(idx(end-2)-1)));
    if isempty(injScale) || ~isnumeric(injScale)
        error(errormsg);
    end
    % ---- Verify all file names common (including glob) except for the
    %      injection scale. This form for fileNameStart is robust if <glob>
    %      contains underscores. 
    fileNameStart = fileName(1:idx(end-3));
    fileNameEnd = fileName(idx(end-2):end);
    if iFile==1
        fileNameStart1 = fileNameStart;
        fileNameEnd1 = fileNameEnd;
    else
        if ~strcmp(fileNameStart,fileNameStart1) | ...
        ~strcmp(fileNameEnd,fileNameEnd1)
            error('File name format not common among all files.');
        end
    end
end
% ---- Now get complete listing of ALL files from the injDirOld directory that
%      match this pattern, not just the ones that happen to have triggers
%      processed by TMVA. (The awk bit is to ensure that just the file name
%      is written, without the injDirOld directory part.)
[status,result] = system(['ls ' injDirOld '/' fileNameStart1 '*' fileNameEnd1 ' | awk -F/ ''{print $NF}'' ']);
% ---- The directory listing in 'result' is a single string including line
%      breaks. Split into a cell array with one line (file) per element.
dirFileList = regexp(result, '[\f\n\r]', 'split');
% ---- The last entry may be empty; delete if empty.
if isempty(dirFileList{end})
    dirFileList = dirFileList(1:end-1);
end
% ---- Verify that all of our "unique" files appears in this directory listing.
for iFile = 1:length(injmatfilename_unique)
    if isempty(cell2mat(strfind(dirFileList,injmatfilename_unique{iFile})))
        error(['Requested file not found in injDirOld: ' injmatfilename_unique{iFile}]);
    end
end
% ---- From now on loop over this "complete" list of files.
injmatfilename_unique = dirFileList;
% ---- Verify that the set of injection scales in our directory listing
%      constitute the set [0:1:(N-1)] where N is the number of scales.
injScaleNumberList = [];
for iFile = 1:length(injmatfilename_unique)
        injScaleNumberList(end+1) = str2num(strrep(strrep( ...
            injmatfilename_unique{iFile},fileNameStart1,''),fileNameEnd1,''));
end
if ~isempty(setdiff([0:(length(injmatfilename_unique)-1)] , injScaleNumberList))
    errormsg = sprintf(['List of file name injection scales does not match expected injection scales.\n' ...
                        '  file name scales: ' num2str(injScaleNumberList) '\n' ...
                        '   expected scales: ' num2str([0:(length(injmatfilename_unique)-1)])]);
    error(errormsg);
end

% ---- Loop over unique injmatfilenames.
for iFile = 1:length(injmatfilename_unique)

    fprintf(1,'Working on %s ... \n', injmatfilename_unique{iFile});

    % ---- Load inj mat files. Loading into a struct (data) instead of directly
    %      into the workspace is safer in case a variable name in the file
    %      matches a name currently used in the workspace.  Also, we can more
    %      easily save the output file without forgetting any of the variables.
    data = load([injDirOld '/' injmatfilename_unique{iFile}]);

    % ---- Pull out values corresponding to injmatfilename_unique{iFile}.
    mask = [];
    mask = strcmp(injmatfilename_unique{iFile},injmatfilename);
    MVAScore_iFile   = MVAScore(mask);
    injIdx_iFile     = injIdx(mask);
    triggerIdx_iFile = triggerIdx(mask);

    % ---- Loop over ALL injections in the file.  For each, replace the
    %      significance of any triggers with the MVA-computed signficance. 
    %      If we have no MVA result then set the significance to zero.  Note
    %      that we do not delete any of the original X-Pipeline triggers.
    nInj = size(data.clusterInj,1);
    for iInj = 1:nInj
        % ---- Check to see if we have any triggers to process.
        Ntrig = length(data.clusterInj(iInj).significance);
        if Ntrig
            % ---- Zero out original X-Pipeline significances.
            data.clusterInj(iInj).significance = zeros(Ntrig,1);
            % ---- Check to see if we have any MVA results for this injection.
            idx = find(injIdx_iFile == iInj);
            if ~isempty(idx)
                data.clusterInj(iInj).significance(triggerIdx_iFile(idx)) = MVAScore_iFile(idx);
            end
        end
    end
    
    % ---- Save new matlab file.
    save([injDirNew '/' injmatfilename_unique{iFile}], '-struct', 'data');

    % % ---- ALTERNATIVE PROCEDURE: Keep only triggers with an MVA result.
    % % ---- Loop over unique values of injIdx_iFile (i.e., over all injection
    % %      numbers that we want to modify for this particular file).  For each
    % %      value:  
    % %        find all triggers
    % %        xclustersubset extract those trigIdx from cluster array
    % %        overwrite sign with MVA score
    % %        overwrite original cluster array element with these trigs
    % injIdx_iFile_unique = unique(injIdx_iFile);
    % for ii = 1:length(injIdx_iFile_unique)
    %     idx = find(injIdx_iFile == injIdx_iFile_unique(ii));
    %     triggers = xclustersubset(clusterInj(injIdx_iFile_unique(ii)),triggerIdx_iFile(idx));
    %     triggers.significance = MVAScore_iFile(idx);
    %     clusterInj(injIdx_iFile_unique(ii)) = triggers;
    % end
    % warning('need to zero out all non-processed injection triggers!');
    
    % % ---- ALTERNATIVE PROCEDURE: Original version.
    % % ---- Sort on injIdx_iFile.
    % [injIdx_iFile_sorted, sortingIdx] = sort(injIdx_iFile);
    % triggerIdx_iFile_sorted = triggerIdx_iFile(sortingIdx);
    % MVAScore_iFile_sorted   = MVAScore_iFile(sortingIdx); 
    % 
    % % ---- Loop over injections.
    % for iInj = 1:length(injIdx_iFile_sorted)
    % 
    %     iInjIdx = injIdx_iFile_sorted(iInj);
    % 
    %     names = fieldnames(clusterInj);
    % 
    %     % ---- Loop over clusterInj fieldnames.
    %     %      WARNING: This loop leave untouched any injections for which 
    %     %      no trigger was found in the +/-0.1 sec coincidence interval
    %     %      for the injections.  In these cases all the noise triggers 
    %     %      will be left in the .mat files, with their original X-Pipeline
    %     %      significance.  They should have not effect on the post-processing
    %     %      because the injectionProcessedMask is set to zero for these.
    %     %      --> Clean up, eventually.
    %     %      --> use xclustersubset for this and related codes.
    %     for iN = 1:length(names)
    % 
    %         if strcmp(names{iN},'significance')
    %             field = getfield(clusterInj(iInjIdx),names{iN});
    %             clusterInj(iInjIdx) = setfield(clusterInj(iInjIdx),names{iN},MVAScore_iFile_sorted(iInj));
    %         else
    %             field = getfield(clusterInj(iInjIdx),names{iN});
    %             clusterInj(iInjIdx) = setfield(clusterInj(iInjIdx),names{iN},field(triggerIdx_iFile_sorted(iInj),:));
    %         end
    %     end % -- Loop over fieldnames.
    % 
    % end % -- Loop over inj.
    % 
    % temp = zeros(size(injectionProcessedMask));
    % temp(injIdx_iFile_sorted) = 1;
    % injectionProcessedMask = temp;

end % -- Loop over injFiles.

% ---- Done.
return
