%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Code to test density of grid points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This code snippet has been copied from the now-deprecated function xchooseskylocations1. 
% It should be combined with testSkyGrid.m to make a coprehensive test suite for grids 
% from xmakeskygrid.m.

%%%  We want the 1-D grid to be sufficiently dense that for any sky position
%%%  within the error box, there will be at least one grid point such that
%%%  the difference in time delay between the sky position and the grid point
%%%  is less than the specified delay tolerance.  Here, we generate a random
%%%  set of sky positions and check that the time delay for each is well-
%%%  accounted for by the 1-D grid.

if verbose >= 2

    baseline = site1ToSite2_sec(maxPair,:);

    N=3000;

    for i=1:N
      testPos_theta(i) = rand * skyPosError_rad;
        if abs(testPos_theta(i)) > pi
            testPos_theta(i) = 2*pi - testPos_theta(i);
            phi_start = pi;
        else
            phi_start=0;
        end

        testPos_phi(i) = rand*2*pi + phi_start;
        if testPos_phi(i) > 2*pi
            testPos_phi(i) = mod(testPos_phi(i),2*pi);
        end
    end

    % make sure these are column vectors
    testPos_phi = testPos_phi(:);
    testPos_theta = testPos_theta(:);

    % convert to Cartesian coordinates
    V = [sin(testPos_theta).*cos(testPos_phi), sin(testPos_theta).*sin(testPos_phi), cos(testPos_theta)];

    % Our grid of test positions is centered about the north pole (0,0,1) - we need
    % to rotate the grid to cover the patch of sky around the GRB.

    v0 = [0 0 1];
    v_perp = cross(v0,earthToGRB);
    psi= acos(dot(v0,earthToGRB)/(norm(v0)*norm(earthToGRB))); 

    for iVec = 1:size(V)
        testPositions(iVec,:) = RotateVector(V(iVec,:),v_perp,psi);   
    end

    % Now calculate the time delay at each test position
    % The delay is equal to the max delay times the cosine of the angle from the detector baseline to the GRB vector

    for ii=1:N
        v1 = testPositions(ii,:);

	delay_new = norm(baseline) * dot(v1,baseline)/(norm(v1)*norm(baseline));
        diff(ii) = 2*delay_tol;

        for jj=1:length(skyPositions_Cartesian)

            delay = norm(baseline) * dot(skyPositions_Cartesian(jj,:),baseline)/(norm(skyPositions_Cartesian(jj,:))*norm(baseline));
            diff_new = abs(delay_new - delay);

            if(diff_new < diff(ii))
                diff(ii) = diff_new;
            end

        end
    end

    diff = diff(:);

    spacing = delay_tol/50;

    x = 0:spacing:2*delay_tol;

    xmax = max(diff);

    margin = delay_tol / xmax;

    figure(2);
    n = hist(diff,x);
    hist(diff,x);
    hold on;
    set(gca,'FontSize',14);
    line([delay_tol delay_tol],[0 1.2*max(n)],'Color','k','LineStyle','--');
    axis([0 1.3*delay_tol 0 1.1*max(n)]);
    xlabel_string = ['Time difference (sec)'];
    title('Histogram of difference in delay between test positions and best sky grid position');
    legend('Histogram results','Delay tolerance','Location','NorthEast');
    xlabel(xlabel_string);
    saveas(gcf,'delay_hist.png','png');
    close(2);

    figure(3);
    plot3(testPositions(:,1),testPositions(:,2),testPositions(:,3),'r+','MarkerSize',1);
    hold on;
    grid on;
    set(gca,'FontSize',14);
    plot3(skyPositions_Cartesian(:,1),skyPositions_Cartesian(:,2),skyPositions_Cartesian(:,3),'b+','MarkerSize',6);
    plot3(earthToGRB(:,1),earthToGRB(:,2),earthToGRB(:,3),'kp','MarkerSize',20);
    line([0 earthToGRB(:,1)],[0 earthToGRB(:,2)],[0 earthToGRB(:,3)],'Color','k','LineWidth',2);
    title('Map of random sky positions to measure time delay tiling');
    legend('Random Test Positions','1-D Grid of Sky Positions','GRB Position','Vector to GRB','Location','East');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    axis([-1 1 -1 1 -1 1]);
    saveas(gcf,'testPositions.png','png');
    axis square
    close(3);

end

