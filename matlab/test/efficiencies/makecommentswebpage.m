function makecommentswebpage(SN,network,run,loudestEvent)
% $Id$

% ---- Save input arguments to temporary file.
save('temp.mat','SN','network','run','loudestEvent')

% ---- Set options for plot_efficiency script. First run: no inclination
%      correction.
correctIncl = 0;
% ---- Run script.
plot_efficiency
% ---- Set and make output directories.
topDir = ['plots/' SN '_' network{1} '/'];
outDir = ['plots/' SN '_' network{1} '/unifiota/'];
[status,result] = system(['mkdir -p ' outDir]);
% ---- Write html report to topDir.
[status,result] = system(['cat header.html > ' topDir 'efficiencies.html']);
[status,result] = system(['cat ' fileName ' >> ' topDir 'efficiencies.html']);
% ---- Move image files to outDir.  
[status,result] = system(['mv *.png *.fig *.pdf ' outDir]);

% ---- Clear variables, rerun with inclination correction.
close all; clear;
load temp.mat
correctIncl = 1;
plot_efficiency
% ---- Set and make output directories.
topDir = ['plots/' SN '_' network{1} '/'];
outDir = ['plots/' SN '_' network{1} '/unifciota/'];
[status,result] = system(['mkdir -p ' outDir]);
% ---- Write html report to topDir.
[status,result] = system(['cat middle.html >> ' topDir 'efficiencies.html']);
[status,result] = system(['cat ' fileName ' >> ' topDir 'efficiencies.html']);
[status,result] = system(['cat tail.html >> ' topDir 'efficiencies.html']);
% ---- Move image files to outDir.  
[status,result] = system(['mv *.png *.fig *.pdf ' outDir]);

% ---- Clean up.
[status,result] = system(['rm temp.mat ' fileName]);


return
