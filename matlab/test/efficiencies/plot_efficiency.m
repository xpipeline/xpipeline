% ---- Matlab script to produce efficency curves for the optically
%      triggered supernova search.
%
% $Id$

% Input: Select the following variables below: 
%     SN             e.g.: 'SN2007gr';
%     correctIncl    either 0 or 1
% You may also need to specify the 'network', 'run', and 'loudestEvent'
% variables if they are commented out below for your SN of choice. 
%
% Output: a _lot_ of plots and some hrss numbers dumped to screen.


% -----------------------------------------------------------------------------
%    User-specifiable parameters.
% -----------------------------------------------------------------------------

% ---- Pipeline to process. Default to X.
if ~exist('pipeline','var')
    pipeline = 'x';
end

% ---- Select supernova to process.
SN = 'SN2007gr';
% SN = 'SN2011dh';

% ---- Script options.
correctIncl = 0;

% ---- Plot options.
savePlots = false;
lineWidth = 2;
lineStyle = {'b-o','g-s','r-*','m->','c-<','y-+','k-s'};

% ---- Base directories for matlab packages.
matappsDir   = '/Users/psutton/Documents/matapps/';
xpipelineDir = '/Users/psutton/Documents/xpipeline/branches/linear/';
injDir       = '/Users/psutton/Documents/xpipeline/branches/linear/test/';
dataDir      = '/Users/psutton/Documents/xpipeline/branches/linear/test/efficiencies/data/';

% -----------------------------------------------------------------------------
%    SN info.
% -----------------------------------------------------------------------------

switch(lower(SN))
    
    case 'sn2007gr'
        
        % ---- Networks, in order of preference.
        % ---- Associated X-Pipeline run numbers for final closed-box results.
        % ---- Loudest event signficance for upper limit estimation and
        %      associated estimated p value. 
        network = {'H1H2L1V1'}; run = 14; loudestEvent =  64.6368; p = 0.00168209;
%         network = {'H1H2L1'};   run =  9; loudestEvent =  77.0654; p = 0.00168209;
%         network = {'H1H2V1'};   run = 11; loudestEvent =  74.0876; p = 0.00165426;
%         network = {'H1H2'};     run = 11; loudestEvent =  35.6827; p = 0.00177462;
%         network = {'L1V1'};     run = 15; loudestEvent =  215.077; p = 0.00165426;
        % ---- Waveform families to test.
%         mdc = {'mueller5', 'numerical3', 'piro5', 'rotbar5', 'sgel2', 'sglin2'};
%         mdc = {'numerical3', 'sgel2', 'sglin2'};
        mdc = {'numerical3'};

    case 'sn2011dh'
        
%         % ---- Networks, in order of preference.
%         network = {'G1V1'};
%         % ---- Associated X-Pipeline run numbers for final closed-box results.
%         run = [11];
%         % ---- Loudest event signficance for upper limit estimation.
%         loudestEvent = [53.2827]; 
%         % ---- Estimated p value
%         p = 0.00177462;
        % ---- Waveform families to test.
        mdc = {'mueller_long', 'numerical_long', 'piro_long', 'rotbar_long', 'sgel_long', 'sglin_long'};
        % mdc = {'piro_long'};

end

% ---- Distinct waveforms within each MDC set. 
%      Extract with a command like this: 
%        awk '{print $7}' injection_mueller5.txt | awk -F~ '{print $3}' | sort -u
%      shortTypes is an
%      arbitrary human-readable label used for plots and output file names. 
types = {};
shortTypes = {};
nominalDistance = {};
nominalHrss = {};
scale1hrss = {};

for imdc = 1:length(mdc)

    switch lower(mdc{imdc}(1:4))
        
            % Explanation of amplitude/distance scales:
            % -----------------------------------------
            %
            % scale1hrss: This is the hrss of the injection if the
            %   injection scale is unity (i.e., it is the raw hrss amplitude
            %   of the injection in the MDC frame).
            % 
            % nominalDistance, nominalHrss: This pair of variables gives
            %   the hrss of the waveform for the specified physical
            %   distance. This pairs allows us to determine the distance
            %   corresponding to any other hrss. (The sgel, sglin waveforms
            %   have no distance scale.)
            %
            % The scale1hrss values were determined by examining the
            % BurstMDC log files, as described at 
            %   https://wiki.ligo.org/Bursts/XOpticalSNMDCParameters
            %
            % The (nominalDistance, nominalHrss) pairs were determined by
            % examining the original source files for each waveform type.
            % Sarah Gossan has verified that the raw mueller, piro, and
            % rotbar waveforms were all generated at 10 kpc. For the
            % numerical waveforms I manually verified that versions in
            % osnsearch_waveforms/original/ were each supplied at 10 kpc by
            % plotting and comparing to published papers or downloading the
            % original sources.  I then measured the hrss values directly.
            % For the review I suggest someone independently double-check
            % the (nominalDistance, nominalHrss) pairs -- an error here
            % would directly screw up our quoted upper limits!
            
        case 'muel'
            % ---- For mueller, use only the beginning of the waveform
            %      name depends on the viewing angle.  This requires some
            %      special handling below. 
            types{end+1} = {...
                'Waveforms/L15-3', ...
                'Waveforms/N20-2', ...
                'Waveforms/W15-4'};
            shortTypes{end+1}      = {'L15-3','N20-2','W15-4'};
            nominalDistance{end+1} = [10 10 10]; %-- kpc
            nominalHrss{end+1}     = 1.0e-22 * [0.2028 0.1406 0.1525]; %-- somewhat arbitrary; this is median over all viewing angles.
            scale1hrss{end+1}      = 1.0e-22 * [0.2028 0.1406 0.1525]; %-- somewhat arbitrary; this is median over all viewing angles.

        case 'nume'
            types{end+1} = { ...
                'Waveforms/processed_signal_s15a2o05_ls-plus.txt', ...
                'Waveforms/processed_signal_s15a2o09_ls-plus.txt', ...
                'Waveforms/processed_signal_s15a3o15_ls-plus.txt', ...
                'Waveforms/processed_s15-time-rhplus_matter-plus.txt', ...
                'Waveforms/processed_s15.0.h-plus.txt'};
            shortTypes{end+1}      = {'s15a2o05','s15a2o09','s15a3o15','s15-Yakunin','S15-Ott'};
            nominalDistance{end+1} = [10 10 10 10 10]; %-- kpc
            nominalHrss{end+1}     = [4.5545e-21, 7.7734e-21, 1.1659e-20, 2.2660e-22, 7.3876e-22];
            scale1hrss{end+1}      = [1.4343e-22, 2.4489e-22, 3.5960e-22, 2.2660e-22, 7.3876e-22];
    
        case 'piro'
            
            types{end+1} = {...
                'Waveforms/piroM10.0eta0.3fac0.2-plus.txt;Waveforms/piroM10.0eta0.3fac0.2-cross.txt', ...
                'Waveforms/piroM10.0eta0.6fac0.2-plus.txt;Waveforms/piroM10.0eta0.6fac0.2-cross.txt', ...
                'Waveforms/piroM5.0eta0.3fac0.2-plus.txt;Waveforms/piroM5.0eta0.3fac0.2-cross.txt', ...
                'Waveforms/piroM5.0eta0.6fac0.2-plus.txt;Waveforms/piroM5.0eta0.6fac0.2-cross.txt'};
            shortTypes{end+1}      = {'M10.0eta0.3fac0.2','M10.0eta0.6fac0.2','M5.0eta0.3fac0.2','M5.0eta0.6fac0.2'};
            nominalDistance{end+1} = [10 10 10 10]; %-- kpc
            nominalHrss{end+1}     = 1.0e-18 * [0.1139 0.4437 0.04028 0.1569];    
            scale1hrss{end+1}      = 1.0e-18 * [0.0806 0.3137 0.0285  0.1110];
        
        case 'rotb'
            
            types{end+1} = {...
                'Waveforms/outM0p2L60R10f400t100-plus.txt;Waveforms/outM0p2L60R10f400t100-cross.txt', ...
                'Waveforms/outM0p2L60R10f400t1000-plus.txt;Waveforms/outM0p2L60R10f400t1000-cross.txt', ...
                'Waveforms/outM0p2L60R10f800t100-plus.txt;Waveforms/outM0p2L60R10f800t100-cross.txt', ...
                'Waveforms/outM1p0L60R10f400t100-plus.txt;Waveforms/outM1p0L60R10f400t100-cross.txt', ...
                'Waveforms/outM1p0L60R10f400t1000-plus.txt;Waveforms/outM1p0L60R10f400t1000-cross.txt', ...
                'Waveforms/outM1p0L60R10f800t25-plus.txt;Waveforms/outM1p0L60R10f800t25-cross.txt'};
            shortTypes{end+1}      = {'M0p2L60R10f400t100','M0p2L60R10f400t1000','M0p2L60R10f800t100',...
                'M1p0L60R10f400t100','M1p0L60R10f400t1000','M1p0L60R10f800t25'};
            nominalHrss{end+1}     = 1.0e-19 * [ 0.1310 0.4144 0.5242 0.6551 2.072 1.309 ];    
            nominalDistance{end+1} = [10 10 10 10 10 10]; %-- kpc
            scale1hrss{end+1}      = 1.0e-18 * [ 0.0093 0.0293 0.0371 0.0463 0.1465 0.0926 ];

        case 'sgel'
            
            types{end+1} = {...
                'Waveforms/SG1304Q8d9.txt;Waveforms/CG1304Q8d9.txt', ...
                'Waveforms/SG235Q8d9.txt;Waveforms/CG235Q8d9.txt'};
            shortTypes{end+1}      = {'1304Q8d9','235Q8d9'};
            nominalDistance{end+1} = [NaN, NaN]; 
            nominalHrss{end+1}     = [NaN, NaN];
            scale1hrss{end+1}      = [1e-21, 1e-21];

        case 'sgli'

            types{end+1} = {...
                'Waveforms/SG1304Q8d9.txt', ...
                'Waveforms/SG235Q8d9.txt'};
            shortTypes{end+1}      = {'1304Q8d9','235Q8d9'};
            nominalDistance{end+1} = [NaN, NaN]; 
            nominalHrss{end+1}     = [NaN, NaN];
            scale1hrss{end+1}      = [1e-21, 1e-21];

    end
    
end

% -----------------------------------------------------------------------------
%    Setup.
% -----------------------------------------------------------------------------

% ---- Set matlab path.
addpath([matappsDir 'packages/simulation/BurstMDC/trunk']);
addpath([matappsDir 'releases/utilities/misc/src/']);
addpath([xpipelineDir 'searches/grb']);
addpath([xpipelineDir 'searches/sn']);
addpath([xpipelineDir 'utilities']);
addpath([xpipelineDir 'share']);

% ---- Open file for text report.
fileName = [SN '_results.txt'];
fid = fopen(fileName,'w');

% -----------------------------------------------------------------------------
%    Process injection data.
% -----------------------------------------------------------------------------

% ---- Prepare storage.
failidx = cell(length(network),length(mdc));
sp      = cell(length(network),length(mdc));
iPM     = cell(length(network),length(mdc));

% ---- loop over MDC sets.
for imdc = 1:length(mdc)
    
    fprintf(fid,'%s\n','--------------------------------------------------------------');
    fprintf(fid,'%s\n',[SN ' - ' mdc{imdc}]);
    fprintf(fid,'%s\n','--------------------------------------------------------------');
    
    % ---- Open and parse BurstMDC formatted log file. Put into same format
    %      (variable names) as expected from X-Pipeline log file.
    injFile = [injDir '/' 'BurstMDC-' SN '_' mdc{imdc} '-Log.txt'];
    [nInj,mdcField,mdcData] = rdburstmdclog(injFile);
    gps_s  = floor(mdcData.EarthCtrGPS);
    gps_ns = round(1e9*(mdcData.EarthCtrGPS-gps_s));
    phi    = mdcData.External_phi;
    theta  = acos(mdcData.External_x);
    psi    = mdcData.External_psi;
    % ---- Extract injection data.
    hrssp  = mdcData.SimHpHp.^0.5;
    hrssc  = mdcData.SimHcHc.^0.5;
    % hrsspc = mdcData.SimHpHc;
    name   = mdcData.GravEn_SimID;
    ciota  = mdcData.Internal_x;
    iota   = acos(ciota);

    % % ---- Get injection parameters from injection log file.
    % injParameters = readinjectionfile(['injection_' mdc{imdc} '.txt']);
    % [~, gps_s, gps_ns, phi, theta, psi, tmp_name, tmp_parameters] = ... 
    %     parseinjectionparameters(injParameters);
    % % ---- The name and parameters are output as cell arrays
    % %      of cells. Clean up this madness! 
    % nInj = length(gps_s);
    % name = cell(nInj,1);
    % hrssp = zeros(nInj,1);
    % hrssc = zeros(nInj,1);
    % parameters = cell(nInj,1);
    % itype = zeros(nInj,1);
    % for kk = 1:nInj
    %     parameters{kk} = tmp_parameters{kk}{1};  %parameters{kk}
    %     tmp = tildedelimstr2numorcell(parameters{kk}); %tmp
    %     hrssp(kk) = tmp{1}; %tmp{1}
    %     hrssc(kk) = tmp{2}; %tmp{2}
    %     name{kk} = tmp{3}; %tmp{3}
    %     if strcmpi(mdc{imdc}(1:4),'muel')
    %         tmp_type = find(strcmp(types{imdc},tmp{3}(1:15)));
    %     else
    %         tmp_type = find(strcmp(types{imdc},tmp{3}));
    %     end
    %     if ~isempty(tmp_type)
    %         itype(kk) = tmp_type; %tmp_type
    %     end        
    % end
    % clear tmp_name tmp_parameters tmp_type kk injParameters

    % ---- Loop over injections and extract amplitude and waveform
    %      information.
    itype = zeros(nInj,1);
    for iInj = 1:nInj
        % ---- Assign a numerical label (1,2,...) to each injection by
        %      waveform type. 
        if strcmpi(mdc{imdc}(1:4),'muel')
            tmp_type = find(strcmp(types{imdc},name{iInj}(1:15)));
        else
            tmp_type = find(strcmp(types{imdc},name{iInj}));
        end
        if ~isempty(tmp_type)
            itype(iInj) = tmp_type;
        end        
    end

    % ---- Prepare figures.
    h0 = figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
    for jj=1:1%max(itype)
        eval(['h' num2str(jj) ' = figure; set(gca,''fontsize'',16); set(gcf,''Color'',[1 1 1]);'])
        set(gcf,'position',[100         400        1400         420]);
        eval(['d' num2str(jj) ' = figure; set(gca,''fontsize'',16); set(gcf,''Color'',[1 1 1]);'])
        set(gcf,'position',[100         400        1400         420]);
    end
    
    % ---- Loop over networks.
    for inet = 1:length(network)

        switch lower(pipeline)
            
            case 'cwb'
                
                % ---- 
                disp('KLUDGE: setting cwb threshold to 0.5.');
                loudestEvent(inet) = 0.5;
                
                % ---- Get list of injection scales processed (which also
                %      fixes the file names).
                cwbDir = ['data_cwb/SN2007gr_SN2011dh_triggers_rho3.5/' ...
                    SN '_' network{inet} '_' mdc{imdc} '/'];
                cmdstr = ['ls ' cwbDir 'scale_factor_* | ' ...
                    'awk -F_ ''{print $NF}'' | sort -n > cwbinjscales.txt'];
                [status,result] = system(cmdstr);
                injectionScale = load('cwbinjscales.txt');
                nScale = length(injectionScale);
            
                % ---- Verify that injection scales are the same as for all
                %      previous networks.
                if imdc==1 & inet==1 
                    iS = injectionScale;
                else
                    if injectionScale ~= iS
                        error(['Injection scales have changed for network ' network{inet} '.']);
                    end
                end
                
                % ---- Read all the results files.  One file per MDC set.
                %      Columns: injection_proc_mask, significance.
                injectionProcessedMask = zeros(nInj,nScale);
                cwbSignificance        = zeros(nInj,nScale);
                for iScale=1:nScale
                    tmpcwb = load([cwbDir 'scale_factor_' num2str(injectionScale(iScale))]);
                    injectionProcessedMask(:,iScale) = tmpcwb(:,1);
                    cwbSignificance(:,iScale)        = tmpcwb(:,2);
                end
                
                % ---- Must have same injProcessedMask value for all scales.
                for iInj = 1:nInj
                    if std(injectionProcessedMask(iInj,:))
                        warning(['Variation in injProc mask for mdc ' num2str(imdc) ', inj ' num2str(iInj) '. Ignoring this injection.']);
                        injectionProcessedMask(iInj,:) = 0;
                    end
                end

                % ---- Assign storage for significance of loudest surviving trigger
                %      for each injection.
                sp{inet,imdc} = cwbSignificance;
                iPM{inet,imdc} = injectionProcessedMask(:,1);
                                
            case 'x'
        
                % ---- Load associatedinjection file for this network.
                load([dataDir '/' SN '-' network{inet} '_run' num2str(run(inet)) '_closedboxassociatedTriggers_' mdc{imdc} '.mat']);

                % ---- In the SN search all MDC sets are processed by X-Pipeline
                %      with the same injection scale. This lets us simplify the
                %      analysis. 
                % ---- Verify that injection scales are the same for all MDCs
                %      for this network.
                if std(injectionScale,0,1) > 1e-8 
                    error(['Injection scales are different for the different MDCs for ' SN]);
                else
                    injectionScale = injectionScale(1,:);
                end
                % ---- Verify that injection scales are the same as for all
                %      previous networks.
                if imdc==1 & inet==1 
                    iS = injectionScale;
                else
                    if injectionScale ~= iS
                        error(['Injection scales have changed for network ' network{inet} '.']);
                    end
                end        

                % ---- Check to see if associated trigger list goes all the way to
                %      the last injection in the injection file. If the last
                %      injection(s) were not processed (e.g. no coincident data)
                %      then the associated trigger list will be shorter than nInj.
                %      In this case pad injectionProcessedMask with zeros.
                if size(injectionProcessedMask{iWave},1)<nInj
                    injectionProcessedMask{iWave}(nInj,:) = 0;
                end
                
                % ---- Assign storage for significance of loudest surviving trigger
                %      for each injection.
                sp{inet,imdc} = zeros(nInj,length(injectionScale));

                % ---- Storage for checks on associated injection triggers.
                failidx{inet,imdc} = [];

                % ---- Loop over associated injections. Run checks and extract
                %      significance of loudest surviving trigger for each injection.
                for iInj = 1:size(injAssociatedTrigger,1);
                    % ---- Must have same injProcessedMask value for all scales.
                    if std(injectionProcessedMask{iWave}(iInj,:))
                        error(['Variation in injProc mask for mdc ' num2str(imdc) ', inj ' num2str(iInj) '.']);
                    end
                    % ---- Must have exactly zero or one triggers per injection.
                    for iScale = 1:size(injAssociatedTrigger,2);
                        if (length(injAssociatedTrigger(iInj,iScale).pass)>1)
                            warning(['Multiple triggers for inj ' num2str(iInj) ', scale ' num2str(iScale) '.']);
                        end
                        % ---- Track which triggers fail cuts.
                        if (length(injAssociatedTrigger(iInj,iScale).pass)==1 && injAssociatedTrigger(iInj,iScale).pass==0)
                            failidx{inet,imdc} = [failidx{imdc}; iInj, iScale];
                        end
                        % ---- Pull out just significance * pass value for each injection
                        %      into ordinary array.
                        if (length(injAssociatedTrigger(iInj,iScale).pass)==1)
                            sp{inet,imdc}(iInj,iScale) = injAssociatedTrigger(iInj,iScale).significance * injAssociatedTrigger(iInj,iScale).pass;
                        else
                            sp{inet,imdc}(iInj,iScale) = 0;
                        end
                    end
                end
                % ---- The injection scales are not necessarily sorted.  This is
                %      irritating, especially for plotting.  Sort.
                [injectionScale, I] = sort(injectionScale);
                sp{inet,imdc} = sp{inet,imdc}(:,I);

                % ---- Reduce inj proc mask to single vector for each mdc type.
                %      Note that the injectionProcessedMask is non-zero only for
                %      injections used for UL estimation.  Injections used for
                %      tuning have a mask of 0. So we don't need to worry about 
                %      separating the training and testing triggers.
                iPM{inet,imdc} = injectionProcessedMask{iWave}(:,1);
                
        end

        % ---- Assign flag to correct inclination angle distribution.
        if inet==1 & correctIncl
            % ---- Reset matlab random number generator seed to a value
            %      based on the SN and MDC set names. This will give us the
            %      same set of random numbers for a given SN-MDC
            %      combination every time we rerun this script.
            s = sum(double([SN mdc{imdc}])); 
            rng(s);
            % ---- Select surviving injections randomly.
            inclpass{imdc} = rand(length(iota),1) <= sin(iota);            
        end
        
        % ---- Optionally correct for inclination.
        if correctIncl
            iPM{inet,imdc} = iPM{inet,imdc} .* inclpass{imdc};
        end
        
        % ---- At this point sp contains 0 for any trigger that was not
        %      processed or which failed a cut. It contains a non-zero
        %      value only for injections that were analysed and for which
        %      there is a trigger that survives all cuts. 
        % ---- We have NOT yet applied a threshold on significance. Do that
        %      now and plot efficiency of processed injections.
        figure(h0);
        effAll{inet,imdc} = mean(sp{inet,imdc}((iPM{inet,imdc}>0),:)>loudestEvent(inet),1);
        effAll{inet,imdc} = effAll{inet,imdc} + eps*[1:length(injectionScale)]; %-- shift effs by infinitesimal amount to guarantee all different (for interpolation)
        % hrssULAll{inet,imdc} = 10.^robustinterp1(effAll{inet,imdc},log10(injectionScale),[0.5 0.9]);
        % fprintf(fid,'%s\n',[SN ' - ' network{inet} ' - ' mdc{imdc} ' hrss50/90: ' num2str(hrssULAll{inet,imdc}(1)) ' / ' num2str(hrssULAll{inet,imdc}(2)) ]);   
        semilogx(injectionScale,mean(sp{inet,imdc}((iPM{inet,imdc}>0),:)>loudestEvent(inet),1),lineStyle{inet},'linewidth',2,'MarkerFaceColor',lineStyle{inet}(1));
        grid on; hold on
        % semilogx(hrssULAll{inet,imdc},[0.5,0.9],'ro','MarkerFaceColor','r');

        % ---- Efficiency by waveform type.
        for jj=1:max(itype)
            % ---- Efficiency vs. hrss.
            % eval(['figure(h' num2str(jj) ')']);
            figure(h1);
            eval(['eff{inet,imdc,' num2str(jj) '} = mean(sp{inet,imdc}((iPM{inet,imdc}>0)&(itype==' num2str(jj) '),:)>loudestEvent(inet),1);']);
            % ---- Shift effs by infinitesimal amount to guarantee all
            %      different (for interpolation).
            % eval(['eff{inet,imdc,' num2str(jj) '} = eff{inet,imdc,' num2str(jj) '} + eps*[1:length(injectionScale)];']); 
            eval(['hrssUL{inet,imdc,' num2str(jj) '} = 10.^robustinterp1(eff{inet,imdc,' num2str(jj) '}+eps*[1:length(injectionScale)],log10(injectionScale*scale1hrss{' num2str(imdc) '}(' num2str(jj) ')),[0.5 0.9]);']);
            eval(['fprintf(fid,''%s\n'',[SN '' - '' network{inet} '' - '' mdc{imdc} '' - '' shortTypes{imdc}{' num2str(jj) '} '' hrss50/90: '' num2str(hrssUL{inet,imdc,' num2str(jj) '}(1)) '' / '' num2str(hrssUL{inet,imdc,' num2str(jj) '}(2)) ]);']);
            eval(['semilogx(injectionScale*scale1hrss{' num2str(imdc) '}(' num2str(jj) '),eff{inet,imdc,' num2str(jj) '},lineStyle{' num2str(jj) '},''linewidth'',2,''MarkerFaceColor'',lineStyle{' num2str(jj) '}(1));']);
            grid on; hold on
            %
            % ---- Efficiency vs. distance (astrophysical waveforms only).
            if ~isnan(nominalDistance{imdc}(jj))
                distance = nominalHrss{imdc}(jj)*nominalDistance{imdc}(jj)/scale1hrss{imdc}(jj) ./ injectionScale;
                % eval(['figure(d' num2str(jj) ')']);
                figure(d1);
                eval(['semilogx(distance,eff{inet,imdc,' num2str(jj) '},lineStyle{' num2str(jj) '},''linewidth'',2,''MarkerFaceColor'',lineStyle{' num2str(jj) '}(1));']);
                grid on; hold on
            end

        end
        
    end

    % ---- Finish plots.
    figure(h0);
    legend(network,2)
    xlabel('injection scale')
    ylabel('efficiency') 
    title(strrep([SN ' - ' mdc{imdc}],'_','\_'))
    set(gca,'ylim',[0,1])
    saveas(h0,[SN '-' network{inet} '-' mdc{imdc} '_effAll.png'],'png')
    saveas(h0,[SN '-' network{inet} '-' mdc{imdc} '_effAll.fig'],'fig')
    %
    clear legendTexth
    for jj=1:max(itype)
        legendTexth{jj} = [shortTypes{imdc}{jj} ' - ' num2str(hrssUL{inet,imdc,jj}(1)) ' / ' num2str(hrssUL{inet,imdc,jj}(2))];
        % eval(['figure(h' num2str(jj) ')']);
        figure(h1);
        eval(['semilogx(hrssUL{inet,imdc,' num2str(jj) '},[0.5,0.9],''ro'',''MarkerFaceColor'',''r'');']);
        xlabel('hrss amplitude (Hz^{-1/2})')
        ylabel('efficiency')
        set(gca,'ylim',[0,1])
        % eval(['title([SN '' - '' mdc{imdc} '' - '' shortTypes{imdc}{' num2str(jj) '}])']);
        eval(['title(strrep([SN '' - '' mdc{imdc}],''_'',''\_''))']);
        % figBaseName = [SN ' - ' network{inet} ' - ' mdc{imdc} '_eff'];
        % eval(['saveas(h' num2str(jj) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-hrss_'' shortTypes{imdc}{jj} ''.fig''],''fig'')']);
        % eval(['saveas(h' num2str(jj) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-hrss_'' shortTypes{imdc}{jj} ''.png''],''png'')']);
    end
    legend(legendTexth,'Location','EastOutside')
    %
    clear legendTextd
    for jj=1:max(itype)
        % eval(['figure(d' num2str(jj) ')']);
        eval(['figure(d' num2str(1) ')']);
        legendTextd{jj} = [shortTypes{imdc}{jj} ' - ' num2str(hrssUL{inet,imdc,jj}(1)) ' / ' num2str(hrssUL{inet,imdc,jj}(2))];
        xlabel('distance (kpc)')
        ylabel('efficiency')
        set(gca,'ylim',[0,1])
        % eval(['title([SN '' - '' mdc{imdc} '' - '' shortTypes{imdc}{' num2str(jj) '}])']);
        eval(['title(strrep([SN '' - '' mdc{imdc}],''_'',''\_''))']);
        % eval(['saveas(d' num2str(jj) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-dist_'' shortTypes{imdc}{jj} ''.fig''],''fig'')']);
        % eval(['saveas(d' num2str(jj) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-dist_'' shortTypes{imdc}{jj} ''.png''],''png'')']);

    end
    legend(legendTextd,'Location','EastOutside')
    if savePlots
        eval(['figure(h' num2str(1) ')']);
        eval(['saveas(h' num2str(1) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-hrss.fig''],''fig'')']);
        % eval(['saveas(h' num2str(1) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-hrss.png''],''png'')']);
        % eval(['export_fig ' SN '-' network{inet} '-' mdc{imdc} '_eff-vs-hrss.pdf'])
        eval(['export_fig ' SN '-' network{inet} '-' mdc{imdc} '_eff-vs-hrss.png'])
        eval(['figure(d' num2str(1) ')']);
        eval(['saveas(d' num2str(1) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-dist.fig''],''fig'')']);
        % eval(['saveas(d' num2str(1) ',[SN ''-'' network{inet} ''-'' mdc{imdc} ''_eff-vs-dist.png''],''png'')']);
        % eval(['export_fig ' SN '-' network{inet} '-' mdc{imdc} '_eff-vs-dist.pdf'])
        eval(['export_fig ' SN '-' network{inet} '-' mdc{imdc} '_eff-vs-dist.png'])
    end
    
end
clear imdc

% ---- Close file for text report.
fprintf(fid,'%s\n','--------------------------------------------------------------');
fclose(fid);



