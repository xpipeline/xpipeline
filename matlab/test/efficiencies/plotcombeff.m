function plotcombeff(SN,mdc,plotType,pipeline,corrIncl,applyTrainingCut,applyWindowCut)
% PLOTCOMBEFF - plot supernova search efficencies, combining cWB and X results
%
% usage
%
% plotcombeff(SN,mdc,plotType,pipeline,corrIncl,training,windowCut)
%
% SN        String. One of 'SN2007gr', 'SN2011dh'.
% mdc       String specifying a known MDC set for the given SN; e.g.: 'rotbar5'.
%           See SNMDCINFO.
% plotType  String. One of 'distance', 'hrss', 'injsc'. Determines whether to
%           plot efficiency vs. injection scale, hrss, or distance. 
% pipeline  String. One of 'all', 'X', 'cWB', 'OR'. Plot all or only one of X,
%           cWB, OR efficiencies. 
% corrIncl  String. Apply inclination correction.  Default true.
% training  String. Use only injections from the X-Pipeline training set, as
%           defined by XSNINDEX. Default true.
% windowCut String. Ignore injection times outside strict on-source window.
%           Default false. NOT IMPLEMENTED. 
%
% Output: plots and some hrss numbers dumped to a text file.
%
% For pipeline = 'all' we will plot each distinct waveform type in a separate
% figure, with the X, cWB, and OR efficiencies on a single figure. This is
% intended for showing X+cWB+OR efficiencies together for comparison. For single
% pipelines all waveforms from an MDC set will go onto a single figure. 
%
% $Id $


% -----------------------------------------------------------------------------
%    Checks and defaults.
% -----------------------------------------------------------------------------

narginchk(4,7)

% ---- What kind of efficiency to compute.
% ---- Apply inclination correction?
if nargin < 5
    corrIncl = true;
end
% ---- Use only injections from the X-Pipeline training set?
if nargin < 6
    applyTrainingCut = true;
end
% ---- Ignore injection times outside strict on-source window. NOT IMPLEMENTED.
if nargin < 7
    applyWindowCut = false;
end


% -----------------------------------------------------------------------------
%    Setup.
% -----------------------------------------------------------------------------

% ---- Plot style options.
savePlots = true;
lineWidth = 2;
lineStyle = {'b-o','g-s','r-*','m->','c-<','y-+','k-s'};

% ---- Base directories for matlab packages.
matappsDir   = '/Users/psutton/Documents/matapps/';
xpipelineDir = '/Users/psutton/Documents/xpipeline/branches/linear/';
injDir       = '/Users/psutton/Documents/xpipeline/branches/linear/test/';
dataDir      = '/Users/psutton/Documents/xpipeline/branches/linear/test/efficiencies/ascii/';

% ---- Set matlab path.
addpath([matappsDir 'packages/simulation/BurstMDC/trunk']);
addpath([matappsDir 'releases/utilities/misc/src/']);
addpath([xpipelineDir 'searches/grb']);
addpath([xpipelineDir 'searches/sn']);
addpath([xpipelineDir 'utilities']);
addpath([xpipelineDir 'share']);


% -----------------------------------------------------------------------------
%    Fetch info on the parameters for this MDC set.  
% -----------------------------------------------------------------------------

[types, shortTypes, nominalDistance, nominalHrss, scale1hrss] = snmdcinfo(mdc);


% -----------------------------------------------------------------------------
%    Read efficiency data.
% -----------------------------------------------------------------------------

% ---- Determine injection scales to loop over.
%load cwbinjscales.txt
cwbinjscales = [0.001, 0.00316, 0.01, 0.01778, 0.03162, 0.05623, 0.1, 0.13335, ...
    0.17783, 0.23714, 0.31623, 0.4217, 0.56234, 0.74989, 1, 1.3335, 1.7783, ...
    2.3714, 3.1623, 4.217, 5.6234, 7.4989, 10, 13.335, 17.783, 23.714, 31.623, ... 
    56.234, 100, 316.23, 1000];
injScale = cwbinjscales(:);
nScale = numel(injScale);

% for ii = [1,nScale]
for ii = 1:nScale
    % ---- Open file with text report.
    fileName = [dataDir mdc '_' SN '_' num2str(injScale(ii)) '.txt'];
    disp(['Reading file ' fileName '.']);
    data = load(fileName);
    % The function produces an ascii file containing summary data on each injection.
    % There is one row per injection, with columns:
    %    1. injection number 
    %    2. waveform type (see SNMDCINFO)
    %    3. inclpass (inclination correction pass flag)
    %    4. X - injection processed mask
    %    5. X - significance 
    %    6. X - weighted significance 
    %    7. X - detected flag
    %    8. cWB - injection processed mask
    %    9. cWB - significance 
    %   10. cWB - weighted significance 
    %   11. cWB - detected flag
    %   12. or(x-mask,cwb-mask)
    %   13. or(x-det,cwb-det)
    % ---- Select pipeline(s) of interest.
    switch lower(pipeline)
        case 'all'
            effIdx = [7,11,13];
        case 'x'
            effIdx = [7];
        case 'cwb'
            effIdx = [11];
        case 'or'
            effIdx = [13];
        otherwise
            error(['Pipeline combination ' pipeline ' not recognised.']);
    end
    nPipeline = length(effIdx);
    if applyTrainingCut
        % ---- Keep only injections intended to be used for UL estimation. (This
        %      includes injections not actually processed due to lack of
        %      coincidence time.).  
        disp('Restricting to X-Pipeline testing injections only.');
        [idxTuning, idxUL] = xsnindex(SN);
        data = data(idxUL,:);
    end
    if corrIncl
        % ---- Delete injections to be discarded.
        data(find(data(:,3)==0),:) = [];
    end
    if ii==1
        % ---- Extract distinct waveform numbers from the data array.
        iType = data(:,2);
        % ---- Determine number of distinct waveform types in this MDC set.
        nType = length(unique(iType));
        % ---- Assign storage.
        eff  = cell(nType,1);
        for jj=1:nType
            eff{jj}  = zeros(nScale,nPipeline);
        end
        is50 = zeros(nType,nPipeline);
        is90 = zeros(nType,nPipeline);
    end
    % % ---- Check fraction of injections processed.
    % mean(data(:,[4,8,12]),1)
    for jj=1:nType
        % % ---- Check number and fraction of injections processed.
        % sum(data(iType==jj,[4,8,12]),1)
        % mean(data(iType==jj,[4,8,12]),1)
        % ---- Find the fraction of detected injections of this type.        
        eff{jj}(ii,:) = mean(data(iType==jj,effIdx),1);
     end
end


% -----------------------------------------------------------------------------
%    Interpolate efficiencies.
% -----------------------------------------------------------------------------

for jj=1:nType
    for kk=1:nPipeline
        % ---- Interpolate efficiencies to find 59%, 90% limits.  Add an
        %      infinitesimal shift to the efficiencies to guarantee all are
        %      different (for interpolation).
        is50(jj,kk) = 10.^robustinterp1(eff{jj}(:,kk)+eps*[1:length(injScale)]',log10(injScale),0.5);
        is90(jj,kk) = 10.^robustinterp1(eff{jj}(:,kk)+eps*[1:length(injScale)]',log10(injScale),0.9);
    end
end

switch plotType
    case 'injsc'
        UL50 = is50;
        UL90 = is90;
    case 'distance'
        for jj=1:nType
            UL50 = repmat(nominalDistance(:),1,nPipeline) ./ is50;
            UL90 = repmat(nominalDistance(:),1,nPipeline) ./ is90;
        end
    case 'hrss'
        for jj=1:nType
            UL50 = repmat(scale1hrss(:),1,nPipeline) .* is50;
            UL90 = repmat(scale1hrss(:),1,nPipeline) .* is90;
        end
end    


% -----------------------------------------------------------------------------
%    Make plots.
% -----------------------------------------------------------------------------

% ---- Prepare figures.
clear legendText
for jj=1:nType
    if (jj==1) || (nPipeline > 1)    
        % ---- Create figure.
        eval(['h' num2str(jj) ' = figure; set(gca,''fontsize'',16); set(gcf,''Color'',[1 1 1]);'])
        eval(['figure(h' num2str(jj) ')']);
        set(gca,'XMinorTick','on','XMinorGrid','on');
    end
    % ---- Create legend.
    if nPipeline == 3
        % ---- Create a legend for a new figure.
        legendText = {['X (' num2str(UL50(jj,1)) ')/(' num2str(UL90(jj,1)) ')'], ...
            ['cWB (' num2str(UL50(jj,2)) ')/(' num2str(UL90(jj,2)) ')'], ...
            ['OR (' num2str(UL50(jj,3)) ')/(' num2str(UL90(jj,3)) ')']};
        lineStyle = {'-s','-s','-s','-s','-s','-s','-s','-s','-s'};
    elseif nPipeline == 1
        % ---- Append to the legend for a single figure.
        legendText{jj} = [shortTypes{jj} ' (' num2str(UL50(jj,1)) ')/(' num2str(UL90(jj,1)) ')'];        
    end
    % ---- Plot efficiencies.
    switch plotType
        case 'injsc'
            semilogx(injScale,eff{jj},lineStyle{jj},'linewidth',2);
            xlabel('injection scale');
            grid on; hold on
            % plot(UL50(jj,:),0.5*ones(size(UL50(jj,:))),'o');
            % plot(UL90(jj,:),0.9*ones(size(UL90(jj,:))),'o');
            axis([1e-3 1e+3 get(gca,'ylim')]);
        case 'distance'
            semilogx(nominalDistance(jj)./injScale,eff{jj},lineStyle{jj},'linewidth',2);
            xlabel('distance (kpc)');
            grid on; hold on
            % plot(UL50(jj,:),0.5*ones(size(UL50(jj,:))),'o');
            % plot(UL90(jj,:),0.9*ones(size(UL90(jj,:))),'o');
            title([SN ' - ' mdc ' - ' shortTypes{jj}]);
            axis([10*1e-3 10*1e+3 get(gca,'ylim')]);
        case 'hrss'
            semilogx(scale1hrss(jj).*injScale,eff{jj},lineStyle{jj},'linewidth',2);
            xlabel('hrss (Hz^{-1/2})');
            grid on; hold on
            % plot(UL50(jj,:),0.5*ones(size(UL50(jj,:))),'o');
            % plot(UL90(jj,:),0.9*ones(size(UL90(jj,:))),'o');
            title([SN ' - ' mdc ' - ' shortTypes{jj}]);
    end
    % ---- Plot formatting.
    if nPipeline > 1
        title([SN ' - ' mdc ' - ' shortTypes{jj}]);
    else
        title([SN ' - ' mdc ' - ' pipeline]);
    end
    legend(legendText,'Location','EastOutside')
    ylabel('efficiency');
    axis([get(gca,'xlim') 0 1])
    pos = [206   536   865   420]; %-- enlarged in x dir
    set(gcf,'position',pos)
    ax  = [0.1300    0.1286    0.4582    0.7964]; %-- default axes size
    set(gca,'position',ax)

end


% -----------------------------------------------------------------------------
%    Write text file with summary info.
% -----------------------------------------------------------------------------

% ---- Dump summary information to a text file.
fid = fopen([pipeline '_' SN '_' mdc '_eff-vs-' plotType '.txt'],'w');
if nPipeline == 3
    fprintf(fid,'%s\n',['                 ' plotType ' limits:       50%     90%    ']);
    fprintf(fid,'%s\n','waveform               X    cWB   OR      X    cWB   OR   ');
    formatStr = '%g\t%g\t%g\t%g\t%g\t%g\n';
else
    fprintf(fid,'%s\n',['waveform          ' plotType ' limits:       50%     90%    ']);
    formatStr = '%g\t%g\n';
end
for jj=1:nType
    fprintf(fid,'%s\t',shortTypes{jj});
    fprintf(fid,formatStr,[UL50(jj,:) UL90(jj,:)]);
end
fclose(fid);


% -----------------------------------------------------------------------------
%    Save plots.
% -----------------------------------------------------------------------------

% ---- Save plots.
if savePlots

    % ---- How many plots are there?
    if nPipeline==1
        nPlot = 1;
    else
        nPlot = nType
    end
    
    % ---- Loop over the plots and save each.
    for jj=1:nPlot
        eval(['figure(h' num2str(jj) ')']);
        if nPipeline==1
            baseName = [pipeline '_' SN '_' mdc];
        else
            baseName = [pipeline '_' SN '_' mdc '_' shortTypes{jj}];
        end
        saveas(gcf,[baseName '_eff-vs-' plotType '.fig'],'fig');
        % saveas(gcf,[baseName '_eff-vs-' plotType '.png'],'png');
        eval(['export_fig ' baseName '_eff-vs-' plotType '.pdf'])
    end
    
end


% -----------------------------------------------------------------------------

return
