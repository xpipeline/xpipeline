function [types, shortTypes, nominalDistance, nominalHrss, scale1hrss] = snmdcinfo(mdc)
% SNMDCINFO - return information on MDCs used in optically triggered SN search
%
% usage:
% 
% [types, shortTypes, nominalDistance, nominalHrss, scale1hrss] = snmdcinfo(mdc)
%
%   mdc         String.  Must be one of 'muel*', 'nume*', 'piro*', 'rotb*',
%               'sgel*', 'sgli*', where the asterisk indicates that only the
%               first four letters of the string are use. Case-insensitive.
%
%   types       Cell array of strings. The elements list the unique waveform
%               names used by BurstMDC for the different waveform types in the
%               mdc set. 
%   shortTypes  Cell array of strings. Each element is an arbitrary
%               human-readable label that may be useful for plots and output
%               file names.   
%   scale1hrss  Array. This is the hrss of an injection of the corresponding
%               type when the injection scale is unity (i.e., it is the raw hrss
%               amplitude of the injection in the MDC frame).
%   nominalDistance, nominalHrss: Array. This pair of variables gives the hrss
%               of the waveform of the corresponding type for the specified
%               physical distance (in kpc). This pair allows us to determine the
%               distance corresponding to any other hrss. (The sgel, sglin
%               waveforms have no distance scale.)
%
% The scale1hrss values were determined by examining the BurstMDC log files, as
% described at
%
%   https://wiki.ligo.org/Bursts/XOpticalSNMDCParameters
%
% The (nominalDistance, nominalHrss) pairs were determined by examining the
% original source files for each waveform type. Sarah Gossan has verified that
% the raw mueller, piro, and rotbar waveforms were all generated at 10 kpc. For
% the numerical waveforms I manually verified that versions in
% osnsearch_waveforms/original/ were each supplied at 10 kpc by plotting and
% comparing to published papers or downloading the original sources.  I then
% measured the hrss values directly. For the review I suggest someone
% independently double-check the (nominalDistance, nominalHrss) pairs -- an
% error here would directly screw up our quoted upper limits!
%
% $Id$

% ---- Checks.
narginchk(1,1)

% ---- Distinct waveforms within each MDC set. 
%      Extracted from X-Pipeline injection files with a command like this: 
%        awk '{print $7}' injection_mueller5.txt | awk -F~ '{print $3}' | sort -u
%      shortTypes is an arbitrary human-readable label used for plots and output
%      file names.  
switch lower(mdc(1:4))

    case 'muel'
        % ---- For mueller, use only the beginning of the waveform
        %      name depends on the viewing angle.  This requires some
        %      special handling below. 
        types = {...
            'Waveforms/L15-3', ...
            'Waveforms/N20-2', ...
            'Waveforms/W15-4'};
        shortTypes      = {'L15-3','N20-2','W15-4'};
        nominalDistance = [10 10 10]; %-- kpc
        nominalHrss     = 1.0e-22 * [0.2028 0.1406 0.1525]; %-- somewhat arbitrary; this is median over all viewing angles.
        scale1hrss      = 1.0e-22 * [0.2028 0.1406 0.1525]; %-- somewhat arbitrary; this is median over all viewing angles.

    case 'nume'
        types = { ...
            'Waveforms/processed_signal_s15a2o05_ls-plus.txt', ...
            'Waveforms/processed_signal_s15a2o09_ls-plus.txt', ...
            'Waveforms/processed_signal_s15a3o15_ls-plus.txt', ...
            'Waveforms/processed_s15-time-rhplus_matter-plus.txt', ...
            'Waveforms/processed_s15.0.h-plus.txt'};
        shortTypes      = {'s15a2o05','s15a2o09','s15a3o15','s15-Yakunin','S15-Ott'};
        nominalDistance = [10 10 10 10 10]; %-- kpc
        nominalHrss     = [4.5545e-21, 7.7734e-21, 1.1659e-20, 2.2660e-22, 7.3876e-22];
        scale1hrss      = [1.4343e-22, 2.4489e-22, 3.5960e-22, 2.2660e-22, 7.3876e-22];

    case 'piro'

        types = {...
            'Waveforms/piroM10.0eta0.3fac0.2-plus.txt;Waveforms/piroM10.0eta0.3fac0.2-cross.txt', ...
            'Waveforms/piroM10.0eta0.6fac0.2-plus.txt;Waveforms/piroM10.0eta0.6fac0.2-cross.txt', ...
            'Waveforms/piroM5.0eta0.3fac0.2-plus.txt;Waveforms/piroM5.0eta0.3fac0.2-cross.txt', ...
            'Waveforms/piroM5.0eta0.6fac0.2-plus.txt;Waveforms/piroM5.0eta0.6fac0.2-cross.txt'};
        shortTypes      = {'M10.0eta0.3fac0.2','M10.0eta0.6fac0.2','M5.0eta0.3fac0.2','M5.0eta0.6fac0.2'};
        nominalDistance = [10 10 10 10]; %-- kpc
        nominalHrss     = 1.0e-18 * [0.1139 0.4437 0.04028 0.1569];    
        scale1hrss      = 1.0e-18 * [0.0806 0.3137 0.0285  0.1110];

    case 'rotb'

        types = {...
            'Waveforms/outM0p2L60R10f400t100-plus.txt;Waveforms/outM0p2L60R10f400t100-cross.txt', ...
            'Waveforms/outM0p2L60R10f400t1000-plus.txt;Waveforms/outM0p2L60R10f400t1000-cross.txt', ...
            'Waveforms/outM0p2L60R10f800t100-plus.txt;Waveforms/outM0p2L60R10f800t100-cross.txt', ...
            'Waveforms/outM1p0L60R10f400t100-plus.txt;Waveforms/outM1p0L60R10f400t100-cross.txt', ...
            'Waveforms/outM1p0L60R10f400t1000-plus.txt;Waveforms/outM1p0L60R10f400t1000-cross.txt', ...
            'Waveforms/outM1p0L60R10f800t25-plus.txt;Waveforms/outM1p0L60R10f800t25-cross.txt'};
        shortTypes      = {'M0p2L60R10f400t100','M0p2L60R10f400t1000','M0p2L60R10f800t100',...
            'M1p0L60R10f400t100','M1p0L60R10f400t1000','M1p0L60R10f800t25'};
        nominalHrss     = 1.0e-19 * [ 0.1310 0.4144 0.5242 0.6551 2.072 1.309 ];    
        nominalDistance = [10 10 10 10 10 10]; %-- kpc
        scale1hrss      = 1.0e-18 * [ 0.0093 0.0293 0.0371 0.0463 0.1465 0.0926 ];

    case 'sgel'

        types = {...
            'Waveforms/SG1304Q8d9.txt;Waveforms/CG1304Q8d9.txt', ...
            'Waveforms/SG235Q8d9.txt;Waveforms/CG235Q8d9.txt'};
        shortTypes      = {'1304Q8d9','235Q8d9'};
        nominalDistance = [NaN, NaN]; 
        nominalHrss     = [NaN, NaN];
        scale1hrss      = [1e-21, 1e-21];

    case 'sgli'

        types = {...
            'Waveforms/SG1304Q8d9.txt', ...
            'Waveforms/SG235Q8d9.txt'};
        shortTypes      = {'1304Q8d9','235Q8d9'};
        nominalDistance = [NaN, NaN]; 
        nominalHrss     = [NaN, NaN];
        scale1hrss      = [1e-21, 1e-21];

end

% ---- Done.
return
