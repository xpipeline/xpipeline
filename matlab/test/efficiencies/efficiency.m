function eff = efficiency(Ninj,Ndet,method)
% EFFICIENCY - estimate efficiency using one of several methods.
%
%
% Ninj      Scalar. Number of injections.
% Ndet      Scalar. Number of detections.
% method    Optional string. Controls method used to estimate efficiency.
%           Recognised values are:
%             'point' - eff = Ndet/Ninj.
%             'bayesianlower90' - Bayesian 90% credible lower bound on
%                 efficiency assuming a uniform prior. 
%           Default 'point'.
%
% eff       Scalar. Estimated efficiency. 
% 
% $Id$

% ---- Check input arguments.
narginchk(2,3)
if ~isscalar(Ninj)
    error('Input Ninj must be a scalar.');
end
if ~isscalar(Ndet)
    error('Input Ndet must be a scalar.');
end
if nargin>2 
    if ~isstr(method)
        error('Input method must be a string.');
    end
else
    method = 'point';
end

switch lower(method)
    
    case 'point'
        
        eff = Ndet/Ninj;
        
    case 'bayesianlower90'
        
        % ---- Need a better name ... ! Bayesian 90% credible lower bound for
        %      efficiency, following Marc Paterno, "Calculating Efficiencies and
        %      their Uncertainties", 2003; note and related software available
        %      from home.fnal.gov/~paterno/images/effic.pdf.  See also G. Cowan,
        %      "Error analysis for efficiency", July 28, 2008
        %      (ATLASStatisticsFAQ wiki), and D. Casadei, "How to measure
        %      efficiency", July 12, 2009 (ATLASStatisticsFAQ wiki).

        % ---- Select equally spaced trial values of efficiency. Scale number up
        %      for cases of very low efficiency to avoid NaN results when
        %      interpolating. A default of 1000 is plenty for most cases.
        Nbin = max(10*round(Ninj/max(Ndet,1)),1000);
        eff_test = ([1:Nbin]'-0.5)/Nbin;
        % ---- Compute log(P) where P is the probability distribution at each
        %      trial value of efficiency.
        logP = gammaln(Ninj+2) - gammaln(Ndet+1) - gammaln(Ninj-Ndet+1) + Ndet*log(eff_test) + (Ninj-Ndet)*log(1-eff_test); 
        % ---- Compute log(cumulative distribution).
        logCumP = zeros(size(logP));
        logCumP(1) = logP(1);
        for ii=2:length(logP)
            logCumP(ii) = sumlogexp(logCumP(ii-1),logP(ii));
        end
        % ---- Normalise cumulative distribution by bin width.
        logCumP = logCumP - log(Nbin);
        % ---- Interpolate to estimate efficiency at which cumulative
        %      distribution is above 0.1 (for 90% credible lower bound).
        eff = robustinterp1(exp(logCumP),eff_test,0.1);        
        
    otherwise
        
        error(['Requested method ' method ' not recognised.']);
        
end
