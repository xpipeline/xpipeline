function c = logsumexp(a, b)
% Computes log(exp(a) + exp(b)) for matrices a and b while guarding against
% overfow if exp(a) and or exp(b) are very large. 
% Written by Antony Searle.
% Updated by Michal Was for 10% speed improvement

d = max(a, b);
d(d == -Inf) = 0;

%c = log(exp(a - d) + exp(b - d)) + d;
c = log(1 + exp(a + b - d - d)) + d;

function shiftedPixIdx = shiftpixel(pixIdx, shift, tfMapLength)
% computes the new linear indices of pixels after they are shifted
shiftedPixIdx = pixIdx  + shift;
% wrap pixels that go beyond the left or right edge of the TF map
shiftedPixIdx = shiftedPixIdx + ...
    tfMapLength*((shiftedPixIdx<=0) - (shiftedPixIdx>tfMapLength));

return

