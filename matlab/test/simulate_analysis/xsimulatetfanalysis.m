function signif = xsimulatetfanalysis(network,signal,analysis)
% network   Struct with fields detectorList,S,minimumFrequency,dF (from ASD
%           file)
% signal    Strut with fields h,fs,injectionScales
% analysis  Struct with fields bpp,skyGrid,verbose,Tfft
%
% analysis for a single input signal, FFT length, all sky positions.
%
% $Id$


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Procedure:
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  for all sky grid positions do 
%    Compute antenna response functions for the network.
%    Compute unit vectors of dominant frame.
%    Compute projection operators Pab in dominant frame.
%    Compute (phase?) delays relative to first detector site.

%  for max(injectionScale) do
%    for each grid point do
%      apply time delay to signal
%      compute signal spectrogram
%      for each injectionScale
%        apply bpp threshold - record map of pixels above threshold with scale 
%                factor of how far above threshold they are   
%      compute all likelihoods for each grid point
%    determine grid point of max likelihood
%    return likelihoods for grid point of max likelihood


%  =========================================================================
%    Preparatory.
%  =========================================================================

% ---- Number of sky grid points.
Nsky = length(analysis.skyGrid.theta);

% ---- Number of detectors.
Nifo = length(network.detectorList);

% ---- Frequencies at which S is sampled.
network.fS = [0:(length(network.S)-1)]'*network.dF;

% ---- Length of FFT [samples].
Nfft = signal.fs * analysis.Tfft;

% ---- Default overlap used in X-Pipeline. 
Noverlap = Nfft/2;

% ---- Default window used in X-Pipeline. 
wind = modifiedhann(signal.fs * analysis.Tfft);

% % ---- Frequency vectors.
% signal.df = 1/analysis.Tfft;
% signal.f  = [0:signal.df:signal.fs/2]';
% signal.Nf = length(signal.f);
% Nf = signal.Nf;
% 


%  ========================================================================
%  compute whitened signal timeseries
%  compute time delays
%  for injectionScale==1 do
%    for each grid point do
%      apply time delay to signal
%      compute signal spectrogram
%      for each injectionScale
%        apply bpp threshold - record map of pixels above threshold with scale 
%                factor of how far above threshold they are   
%      compute all likelihoods for each grid point
%    determine grid point of max likelihood
%    return likelihoods for grid point of max likelihood
%  ========================================================================


%  ========================================================================
%    Compute whitened signal (noise-free data).
%  =========================================================================

% ---- Noise-weighted injection returned from xoptimalsnr for largest injection
%      scale.
whitenedData = zeros(size(signal.h));
maxScale = max(signal.injectionScales);
for a = 1:Nifo
    [SNR, ~, ~, ~, ~, ~, ~, ~, ~, ~, whitenedData(:,a)] = xoptimalsnr( ...
        maxScale*signal.h(:,a),0,signal.fs,network.S(a,:,1),network.minimumFrequency,network.dF);
    % whitenedData(:,a) = whitenedData(:,a) * (2 * 4096)^0.5;
    % SNR
    % norm(whitenedData(:,a))
    % norm(whitenedData(:,a))/4096^0.5
    % figure; plot([0:256*4096-1]'/4096,whitenedData(:,a))
end


%  ========================================================================
%    Compute sky grid delays.
%  =========================================================================

% ---- Compute time delays of grid points wrt centre of Earth.
gridDelay = computeTimeShifts(network.detectorList, [analysis.skyGrid.theta, analysis.skyGrid.phi]);

% ---- Reset to delay wrt first detector.
gridDelay = gridDelay - gridDelay(:,1);

% ---- Convert to samples.
gridDelaySamples  = round(gridDelay * signal.fs);
gridDelayFraction = gridDelay * signal.fs - gridDelaySamples;


%  =========================================================================
%    Loop over search grid points.
%  =========================================================================

warning('Not sure if delay is being applied with the correct sign!')

nScales = length(signal.injectionScales);

% ---- The selected TF map pixels for each detector for each sky position will
%      be stored in a single large array, with one row per sky position. This
%      will allow us to perform the coherent analysis for all sky positions
%      simultaneously later using matrix operations.
maxNpix  = 1000; %-- maximum number of pixels retained for each detector
maxNused = 0;    %-- maximum number of pixels actually retained 
tfmapAll = zeros(Nsky,maxNpix,Nifo); %-- storage arrays
idxAll   = zeros(Nsky,maxNpix);
irowAll  = zeros(Nsky,maxNpix);
icolAll  = zeros(Nsky,maxNpix);
fAll     = zeros(Nsky,maxNpix);
maskAll  = zeros(Nsky,maxNpix,nScales);

for isky = 1:Nsky

    % ---- Loop over detectors.
    delayedWhitenedData = zeros(size(whitenedData));
    for a = 1:Nifo
        % ---- Apply time delay to nearest sample. (Correct for fractional delay
        %      later.)
        delayedWhitenedData(:,a) = circshift(whitenedData(:,a),gridDelaySamples(isky,a));
        % ---- Compute signal spectrogram in each detector.
        [tfmap(:,:,a),tfmapF] = spectrogram(delayedWhitenedData(:,a),wind,Noverlap,Nfft,signal.fs);
        % ---- Apply sub-sample time delay.        
        delayVector = exp(1i*2*pi*tfmapF*gridDelayFraction(isky,a));
        for ii=1:size(tfmap,1)
            tfmap(ii,:,a) = tfmap(ii,:,a) * exp(1i*2*pi*tfmapF(ii)*gridDelayFraction(isky,a));
        end
    end
    figure; plot([0:1/signal.fs:(size(delayedWhitenedData,1)-1)/signal.fs]',delayedWhitenedData);
    
    % ---- Select TF pixels to analyse.
    if isfield(analysis,'bpp')
        % ---- Keep pixels above bpp percentile of Gaussian background. 
        % ---- bpp threshold for the sum of Nifo Gaussian noise streams each
        %      normalised so that the per-detector average energy in each pixel
        %      is unity:  
        thresh = chi2inv(analysis.bpp/100,2*Nifo)/2;
        % ---- Use total energy spectrogram to select TF pixels.
        totalEnergyMap = sum(abs(tfmap).^2,3);
        totalEnergy = sum(totalEnergyMap(:));
        % [mean(energymap(:)), std(energymap(:)), max(energymap(:)), sum(energymap(:))]
        % ---- Find pixels above threshold, including average noise contribution
        %      of 1 per detector per pixel.
        idx = find(totalEnergyMap+Nifo > thresh);
        % ---- Always keep at least two pixels.
        if length(idx) < 2
            disp('KLUDGE: retaining 2 loudest pixels.');
            [smap,idx] = sort(totalEnergyMap(:),'descend');
            idx = idx(1:2);
        elseif length(idx) > maxNpix
            disp(['KLUDGE: restricting to ' num2str(maxNpix) ' loudest pixels.']);
            [smap,idx] = sort(totalEnergyMap(:),'descend');
            idx = idx(1:maxNpix);
        end
        % ---- Number of pixels kept and their row & column indices.
        Npix = length(idx);
        [irow,icol] = ind2sub(size(totalEnergyMap),idx);
        % ---- Define mask indicating which of these pixels are still black at
        %      lower injection scales.
        submap = totalEnergyMap(idx);
        mask = zeros(length(idx),length(signal.injectionScales));
        for iscale=1:length(signal.injectionScales)
            rescale = signal.injectionScales(iscale) / maxScale;
            mask(:,iscale) = rescale^2 * submap + Nifo > thresh;
        end
%         disp(['Retaining pixels by scale:']);
%         sum(mask,1)
%         % ---- Fractional SNR2 accumulated.
%         SNR2 = sum(submap(:))/totalEnergy;
%         disp(['Approx fractional SNR^2 at max scale: ' num2str(SNR2)]);
    else
        warning('This section does not define all variables needed later. An error is coming!')
        % ---- Set threshold to retain only the Npix loudest pixels.
        Npix = 15;
        thresh = prctile(totalEnergyMap(:),100*(1-Npix/numel(totalEnergyMap)));
        idx = find(totalEnergyMap > thresh);
        [irow,icol] = ind2sub(size(totalEnergyMap),idx);
    end
%     disp(['Retaining ' num2str(length(idx)) ' pixels.']);
    if Npix > maxNused
        maxNused = Npix;
    end

    % ---- Copy tfmap and associated data into master arrays covering all sky positions.
    for a = 1:Nifo
        tmp = squeeze(tfmap(:,:,a));
        tfmapAll(isky,1:length(idx),a) = tmp(idx); %tfmap(irow,icol,a);
    end
    idxAll(isky,1:length(idx))     = idx(:).';
    irowAll(isky,1:length(idx))    = irow(:).';
    icolAll(isky,1:length(idx))    = icol(:).';
    fAll(isky,1:length(idx))       = tfmapF(irow);
    for iscale = 1:nScales
        maskAll(isky,1:length(idx),iscale) = mask(:,iscale);
    end

end


% ---- Trim un-needed storage from master arrays.
if maxNused < maxNpix
    cutIdx = maxNused+1:maxNpix;
    tfmapAll(:,cutIdx,:) = [];
    idxAll(:,cutIdx)     = [];
    irowAll(:,cutIdx)    = [];
    icolAll(:,cutIdx)    = [];
    fAll(:,cutIdx)       = [];
    maskAll(:,cutIdx,:)  = [];
end
maxNpix = maxNused;


% ---- Frequency vectors.
% signal.df = 1/analysis.Tfft;
% signal.f  = [0:signal.df:signal.fs/2]';
% signal.Nf = length(signal.f);
Nf = maxNpix; %-- number of frequency bins


%  =========================================================================
%    Compute antenna response functions over the sky grid.
%  =========================================================================

% ---- Compute antenna responses.
[Fp, Fc, Fb] = antennaPatterns(network.detectorList, [analysis.skyGrid.theta, analysis.skyGrid.phi]);

% ---- Noise-spectrum weighting factor for each detector and frequency bin.
% w = zeros(Nifo,Nf);
w = zeros(Nsky,Nifo,Nf);
for a = 1:Nifo
    disp('KLUDGE: using only first measured spectrum');
    w(:,a,:) = sqrt((Nfft/2)*interp1(network.fS',network.S(a,:,1),fAll)).^(-1);
end

% ---- Noise-weighted antenna responses.  Dimensions are (sky position) x
%      (detector) x (frequency bin).
Fpw = Fp .* w;
Fcw = Fc .* w;
Fbw = Fb .* w;

% ---- Convert to dominant polarization frame. (Dominant frame values are
%      denoted with lowercase f.)
fpw = zeros(Nsky,Nifo,Nf);
fcw = zeros(Nsky,Nifo,Nf);
fbw = zeros(Nsky,Nifo,Nf);
for j = 1:Nf
    [fpw(:,:,j), fcw(:,:,j)] = convertToDominantPolarizationFrame( Fpw(:,:,j), Fcw(:,:,j) );
    fbw = Fbw;
end


%  =========================================================================
%    Compute unit vectors "e" in dominant polarisation frame.
%  =========================================================================

% ---- Note: The circ and loghbayesiancirc likelihoods are defined as follows.
%        E^{R,L} = |d_+ +/- id_x|^2 / (f_+^2+f_x^2)    ; DPF, d_+ := f_+ \cdot d, etc.
%                = [|d_+|^2 + |d_x|^2 -/+ 2Im(d_+^* d_x)] / (f_+^2+f_x^2) 
%                = \sum_{a,b} d_a^* hat{e}^*_a hat{e}_b d_b   
%      where hat{e}^{R,L} = f_+ +/- i f_x / (f_+^2+f_x^2)^0.5
%        E_circ := max(E^R, E^L)
%        E_circ_bayes = 1/2 [ A/(1+A) * E^{R,L} - ln(1+A) ] 
%        A := \sigma^2 * (f_+^2+f_x^2) / 2
%        E_LHBC := ln( \sum_sigma exp{ max(E_circ_bayes^R , E_circ_bayes^L) } )
sumfp2 = sum(fpw.^2,2);
sumfc2 = sum(fcw.^2,2);
sumfb2 = sum(fbw.^2,2);
irat = sqrt(-1);
for a = 1:Nifo
    ewp{a} = squeeze(fpw(:,a,:)./sqrt(sumfp2));
    ewc{a} = squeeze(fcw(:,a,:)./sqrt(sumfc2));  
    ewb{a} = squeeze(fbw(:,a,:)./sqrt(sumfb2));
    ewright{a} = squeeze((fpw(:,a,:) + irat * fcw(:,a,:))./sqrt(sumfp2+sumfc2));
    ewleft{a}  = squeeze((fpw(:,a,:) - irat * fcw(:,a,:))./sqrt(sumfp2+sumfc2));
end


%  =========================================================================
%    Compute projection operators Pab in dominant polarisation frame.
%  =========================================================================

for a = 1:Nifo
    for b = 1:Nifo
        Pp{a,b}     = ewp{a}.*ewp{b};                  % P plus
        Pc{a,b}     = ewc{a}.*ewc{b};                  % P cross
        Pb{a,b}     = ewb{a}.*ewb{b};                  % P scalar
        Pright{a,b} = conj(ewright{a}).*ewright{b};
        Pleft{a,b}  = conj(ewleft{a}).*ewleft{b};
        if a==b
            Ptot{a,b} = ones(Nsky,Nf);
        else
            Ptot{a,b} = zeros(Nsky,Nf);
        end
    end
end


%  =========================================================================
%    Compute likelihoods for all sky positions.
%  =========================================================================

% ---- Prior amplitudes for Bayesian likelihoods.
%      ssigma: in xtimefrequencymap this is set to 10.^[-23:0.5:-21].
%      However, that code uses the convention S = S(f) (continuous) rather
%      than S = S[k] (discrete), where S[k] = fs*S(f). Therefore the
%      weighted antenna responses differ as 
%        Fpw_xtfm = Fp / S(f)^0.5 = fs^0.5 * Fpw_paper
%      To compensate, we must use ssigma = fs^0.5 * 10.^[-23:0.5:-21].  
ssigma = signal.fs^0.5*10.^[-23:0.5:-21];
% ssigma = 10.^[-19:0.5:-17]; %-- this gave better performance in some early tests

% ---- Initialize storage.
Ep     = zeros(Nsky,nScales); %-- plus energy
Ec     = zeros(Nsky,nScales); %-- cross energy
Eright = zeros(Nsky,nScales); %-- right-circularly polarised energy
Eleft  = zeros(Nsky,nScales); %-- left-circularly polarised energy
Etot   = zeros(Nsky,nScales); %-- total energy
Icirc  = zeros(Nsky,nScales); %-- Note: Il = Ir = Icirc 
Ip     = zeros(Nsky,nScales); %-- plus inc
Ic     = zeros(Nsky,nScales); %-- plus inc
Eb     = zeros(Nsky,nScales); %-- scalar (breathing mode) energy
Ib     = zeros(Nsky,nScales);
% ---- Bayesian statistics are initialised to -Inf for correct
%      behaviour when marginalising with logsumexp().
Elhbcirc = -inf(Nsky,nScales); %-- loghbayesiancirc statistic
Elhb     = -inf(Nsky,nScales); %-- loghbayesian statistic
Elhbb    = -inf(Nsky,nScales); %-- loghbayesian - scalar (breathing mode) statistic

for iScale=1:nScales
    
    % ---- Temporary variables needed for computing Bayesian likelihoods. These hold
    %      summed-over-pixels likelihoods for each ssigma value.
    Elhbcirc_tmp = zeros(Nsky,2*length(ssigma));
    Elhb_tmp     = zeros(Nsky,length(ssigma));
    Elhbb_tmp    = zeros(Nsky,length(ssigma));

    % ---- Loop over frequencies.
    for j = 1:Nf

        % ---- Initialise temporary variables used in constructing
        %      Bayesian likelihoods. These are reset for every pixel.
        tmpp = 0;
        tmpc = 0;
        tmpr = 0;
        tmpl = 0;
        tmpb = 0;
        % ---- Compute projection likelihoods and their incoherent
        %      counterparts by looping over detector indices. 
        %      Note: size(Pp{1,1}) = Nsky x Nf
        rescale = signal.injectionScales(iscale) / maxScale;        
        for a = 1:Nifo
            for b = 1:Nifo
                Ep(:,iScale)     = Ep(:,iScale)     + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pp{a,b}(:,j)    .* tfmapAll(:,j,b);
                Ec(:,iScale)     = Ec(:,iScale)     + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pc{a,b}(:,j)    .* tfmapAll(:,j,b);
                Eright(:,iScale) = Eright(:,iScale) + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pright{a,b}(:,j).* tfmapAll(:,j,b);
                Eleft(:,iScale)  = Eleft(:,iScale)  + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pleft{a,b}(:,j) .* tfmapAll(:,j,b);
                Etot(:,iScale)   = Etot(:,iScale)   + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Ptot{a,b}(:,j)  .* tfmapAll(:,j,b);
                Eb(:,iScale)     = Eb(:,iScale)     + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pb{a,b}(:,j)    .* tfmapAll(:,j,b); 
                %
                tmpp   = tmpp   + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pp{a,b}(:,j)    .* tfmapAll(:,j,b);
                tmpc   = tmpc   + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pc{a,b}(:,j)    .* tfmapAll(:,j,b);
                tmpr   = tmpr   + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pright{a,b}(:,j).* tfmapAll(:,j,b);
                tmpl   = tmpl   + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pleft{a,b}(:,j) .* tfmapAll(:,j,b);
                tmpb   = tmpb   + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pb{a,b}(:,j)    .* tfmapAll(:,j,b);
            end
            Ip(:,iScale)    = Ip(:,iScale)    + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pp{a,a}(:,j)    .* tfmapAll(:,j,a);
            Ic(:,iScale)    = Ic(:,iScale)    + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pc{a,a}(:,j)    .* tfmapAll(:,j,a);
            Icirc(:,iScale) = Icirc(:,iScale) + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pright{a,a}(:,j).* tfmapAll(:,j,a);
            Ib(:,iScale)    = Ib(:,iScale)    + rescale^2 .* maskAll(:,j,iScale) .* conj(tfmapAll(:,j,a)) .* Pb{a,a}(:,j)    .* tfmapAll(:,j,a);
        end
        % ---- Apply Bayesian priors (amplitude ssigma, polarisation) to likelihoods
        %      and store for later marginalisation after summing over all pixels. 
        % ---- Loop over prior amplitudes. Recall size(sumfp2) = Nsky x 1 x Nf, etc.
        for iii=1:length(ssigma)
            % ---- loghbayesiancirc running sum over pixels. Columns
            %      iii=1:length(ssigma) hold the right polarisation
            %      likelihoods, columns length(ssigma)+1:2*length(ssigma) 
            %      hold the left ones.
            A = ssigma(iii)^2 * squeeze(sumfp2(:,1,j)+sumfc2(:,1,j))/2;
            Elhbcirc_tmp(:,iii) = Elhbcirc_tmp(:,iii) + 1/2 * ( A./(1+A) .* tmpr - log(1+A) );
            Elhbcirc_tmp(:,iii+length(ssigma)) = Elhbcirc_tmp(:,iii+length(ssigma)) ...
                + 1/2 * ( A./(1+A) .* tmpl - log(1+A) );
            % ---- loghbayesian running sum over pixels.
            trtempp = (squeeze(sumfp2(:,1,j))*ssigma(iii)^2);
            trtempc = (squeeze(sumfc2(:,1,j))*ssigma(iii)^2);
            Elhb_tmp(:,iii) = Elhb_tmp(:,iii) + 0.5 * ( ...
                       (1 + (trtempp).^-1).^(-1) .* tmpp ...
                     + (1 + (trtempc).^-1).^(-1) .* tmpc ...
                     - log((1 + trtempp)) - log(1+trtempc) ...
                    );
            % ---- loghbayesianscalar running sum over pixels.
            trtemp  = (squeeze(sumfb2(:,1,j))*ssigma(iii)^2);
            Elhbb_tmp(:,iii) = Elhbb_tmp(:,iii) + 0.5 * ( ...
                (1 + (trtemp).^-1).^(-1) .* tmpb - log(1 + trtemp) );
        end

    end  %-- loop over frequencies

    % ---- Bayesian likelihoods: marginalise over amplitude prior (all) and
    %      polarisations (circ).
    for iii = 1:length(ssigma)
        Elhbcirc(:,iScale) = logsumexp(Elhbcirc(:,iScale),Elhbcirc_tmp(:,iii));
        Elhbcirc(:,iScale) = logsumexp(Elhbcirc(:,iScale),Elhbcirc_tmp(:,iii+length(ssigma)));
        Elhb(:,iScale)     = logsumexp(Elhb(:,iScale),Elhb_tmp(:,iii));
        Elhbb(:,iScale)    = logsumexp(Elhbb(:,iScale),Elhbb_tmp(:,iii));
    end
    % ---- Bayesian likelihoods: apply sky prior (all).
    Elhbcirc(:,iScale) = Elhbcirc(:,iScale) + log(analysis.skyGrid.prob); 
    Elhb(:,iScale)     = Elhb(:,iScale)     + log(analysis.skyGrid.prob);
    Elhbb(:,iScale)    = Elhbb(:,iScale)    + log(analysis.skyGrid.prob);

end

% ---- Check that all computed likelihoods are real up to numerical error
%      (tol=1e-9), and cast off imaginary parts.
tol = 1e-9;
if isrealenough(Etot,tol,'Etot');           Etot   = real(Etot);        end
if isrealenough(Ep,tol,'Ep');               Ep     = real(Ep);          end
if isrealenough(Ip,tol,'Ip');               Ip     = real(Ip);          end
if isrealenough(Ec,tol,'Ec');               Ec     = real(Ec);          end
if isrealenough(Ic,tol,'Ic');               Ic     = real(Ic);          end
if isrealenough(Eright,tol,'Eright');       Eright = real(Eright);      end
if isrealenough(Eleft,tol,'Eleft');         Eleft  = real(Eleft);       end
if isrealenough(Icirc,tol,'Icirc');         Icirc  = real(Icirc);       end
if isrealenough(Elhbcirc,tol,'Elhbcirc');   Elhbcirc = real(Elhbcirc);  end
if isrealenough(Eb,tol,'Eb');               Eb     = real(Eb);          end
if isrealenough(Ib,tol,'Ib');               Ib     = real(Ib);          end
if isrealenough(Elhb,tol,'Elhb');           Elhb   = real(Elhb);        end
if isrealenough(Elhbb,tol,'Elhbb');         Elhbb  = real(Elhbb);       end
% ---- Compute other desired likelihoods using known relationships.
Ecirc  = max([Eright,Eleft],[],2);
warning('Ecirc calculation may not correctly account for nScales.')
En     = Etot - Ep - Ec;
In     = Etot - Ic - Ip;
Ecircn = Etot - Ecirc - En;
Icircn = Etot - Icirc - In;
Enb = Etot - Eb;
Inb = Etot - Ib;
% ---- Assign event significance.
detectionStat = 'loghbayesiancirc';
switch detectionStat
    case 'loghbayesianscalar'
        signif = Elhbb;
    case 'loghbayesiancirc'
        signif = Elhbcirc;
    case 'loghbayesian'
        signif = Elhb;
    case 'circenergy'
        signif = Ecirc;
    case 'plusenergy'
        signif = Ep;
    case 'standard'
        signif = Ep+Ec;
    otherwise
        error(['Detection statistic ' detectionStat ' not recognised.']);
end

% % ---- Compute experimental sky pointing statistics.
% testStatN  = Ecirc.*(1 - Ecircn./Icircn);
% testStatC  = Ecirc.*(1 - Icirc./Ecirc);
% testStatCN = Ecirc.*(1 - Ecircn./Icircn).*(1 - Icirc./Ecirc);
% %testStatCN = Ecirc.*(1 - Ecircn./Icircn).*(1 - (Icirc./Ecirc-1/length(detectorList)));

% ---- Determine grid position of maximum desired likelihood.
[val,rowind] = max(signif);
colind = 1:nScales;
ind = sub2ind(size(signif),rowind,colind);
% [signif(ind) ; Ep(ind) ; Ip(ind)]

% pointingName = analysis.pointingName;
% eval(['pointingStat = ' pointingName ';']);
% [val,ind] = max(pointingStat);

% ---- Record recovered sky position and likelihoods for this
%      position.
% recInj{iwf}.theta{kk}(iInj) = Theta(ind);
% recInj{iwf}.phi{kk}(iInj)   = Phi(ind);
% recInj{iwf}.likelihood{kk}(iInj,:) = [Elhbcirc(ind) Ecirc(ind) Icirc(ind) ...
%     Ecircn(ind) Icircn(ind), En(ind), In(ind), Elhb(ind), ...
%     Ep(ind), Ip(ind), Ec(ind), Ic(ind), Etot(ind), ...
%     Elhbb(ind), Eb(ind), Ib(ind), Enb(ind), Inb(ind)];
% recInj{iwf}.likelihoodType = {'loghbayesiancirc','circenergy','circinc', ...
%     'circnullenergy','circnullinc','nullenergy','nullinc',...
%     'loghbayesian','plusenergy', ...
%     'plusinc','crossenergy','crossinc','totalenergy',...
%     'loghbayesianscalar','scalarenergy','scalarinc',...
%     'scalarnullenergy','scalarnullinc'};
% % ---- KLUDGE: missing likelihoods:
% %      standard,energyitf1,energyitf2,energyitf3,
% %      skypositiontheta,skypositionphi


return

            
            
            
%         % ---- Optionally make some plots.
%         if analysis.makeSkyPlots
% 
%             Vinj = CartesianPointingVector(injPhi,injTheta);
%             Vrec = CartesianPointingVector(recInj{iwf}.phi{kk},recInj{iwf}.theta{kk});
%             %
%             tag = ['_' num2str(1000*analysis.delayTol) 'ms'];
%             %
%             figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
%             hist(acos(sum(Vinj.*Vrec,2))*180/pi);
%             xlabel('Angular error [deg]');
%             ylabel('number of injections');
%             title([pointingName ...
%                    ' (' waveform.type ',' waveform.parameters{iwf} ')' ...
%                    ', injSc: ' num2str(kk) ...
%                    ', med err = ' num2str(median(acos(sum(Vinj.*Vrec,2))*180/pi)) ' deg']);
%             saveas(gcf,[pointingName '_hist' tag '.png'],'png');
%             %
%             figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
%             for iInj=1:Ninj
%                 plot([injPhi(iInj);recInj{iwf}.phi{kk}(iInj)],[injTheta(iInj);recInj{iwf}.theta{kk}(iInj)],'-+');
%                 grid on; hold on;
%             end
%             xlabel('phi [rad]');
%             ylabel('theta [rad]');
%             title(['pointing statistic: ' pointingName]);
%             xlim = get(gca,'xlim');
%             ylim = get(gca,'ylim');
%             [phi_center, theta_center] = radectoearth(trigger.ra,trigger.dec,trigger.gps);
%             [isoLines baselineNames] = xisotimedelay([theta_center, phi_center], detectorList);
%             lineStyle = {'gx','rx','mx','cx','kx'};
%             for iLine=1:size(isoLines,1)
%                 idx = find(isoLines(iLine,:,2)>=xlim(1) & isoLines(iLine,:,2)<=xlim(2) ...
%                     & isoLines(iLine,:,1)>=ylim(1) & isoLines(iLine,:,1)<=ylim(2) );
%                 plot(isoLines(iLine,idx,2),isoLines(iLine,idx,1),lineStyle{iLine});
%                 grid on; hold on;
%                 legendText{iLine} = [baselineNames{iLine} ' (' lineStyle{iLine} ')'];
%             end
%             legend(legendText);
%             saveas(gcf,[pointingName '_scat' tag '.png'],'png');
%             %
%             figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
%             % plot(recInj{iwf}.likelihood{kk}(:,2)./recInj{iwf}.likelihood{kk}(:,3),recInj{iwf}.likelihood{kk}(:,4)./recInj{iwf}.likelihood{kk}(:,5),'.');
%             plot(recInj{iwf}.likelihood{kk}(:,3)./recInj{iwf}.likelihood{kk}(:,2),recInj{iwf}.likelihood{kk}(:,4)./recInj{iwf}.likelihood{kk}(:,5),'.');
%             IcOEc   = prctile(recInj{iwf}.likelihood{kk}(:,3)./recInj{iwf}.likelihood{kk}(:,2),[50 90]); 
%             EcnOIcn = prctile(recInj{iwf}.likelihood{kk}(:,4)./recInj{iwf}.likelihood{kk}(:,5),[50 90]); 
%             legend(['median position: (' num2str(IcOEc(1)) ',' num2str(EcnOIcn(1)) ')']);
%             xlim = get(gca,'xlim');
%             ylim = get(gca,'ylim');
%             if xlim(end)<=1 & ylim(end)<=1
%                 axis([0 1 0 1]);
%             end
%             grid on; hold on;
%             plot([0;1],[EcnOIcn(2);EcnOIcn(2)],'m--','linewidth',2);
%             plot([IcOEc(2);IcOEc(2)],[0;1],'m--','linewidth',2);
%             % xlim = get(gca,'xlim');
%             % ylim = get(gca,'ylim');
%             % plot([1;xlim(2)],[1;1],'m--','linewidth',2);
%             % plot([1;1],[1;ylim(1)],'m--','linewidth',2);
%             title(['pointing statistic: ' pointingName]);
%             % xlabel('Ecirc./Icirc');
%             xlabel('Icirc./Ecirc');
%             ylabel('Ecircnull./Icircnull');
%             saveas(gcf,[pointingName '_cuts' tag '.png'],'png')
% 
%         end %-- if analysis.makeSkyPlots
% 
% %         % ---- Efficiency using all cuts.
% %         % ---- Hardwire to case of circular polarisation cut analysis.
% %         pass1 = applycut(En,In,cutThresholds(1),cutTypes{1});
% %         pass2 = applycut(Ecirc,Icirc,cutThresholds(2),cutTypes{2});
% %         pass3 = applycut(Ecircn,Icircn,cutThresholds(3),cutTypes{3});
% %         pass4 = applycut(En,In,cutThresholds(4),cutTypes{4});
% %         pass5 = applycut(Ecirc,Icirc,cutThresholds(5),cutTypes{5});
% %         pass6 = applycut(Ecircn,Icircn,cutThresholds(6),cutTypes{6});
% %         pass7 = (signif > detectionThreshold) ;
% %         eff(kk) = eff(kk) + 1/Nwf * mean( ...
% %             pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 ...
% %             );
% %         disp('Energies: [En,Ecirc,Ecircn]');
% %         [En,Ecirc,Ecircn]
% %         disp('Energies: [In,Icirc,Icircn]');
% %         [In,Icirc,Icircn]
% %         disp('Energies: Elhbcirc');
% %         Elhbcirc
% %         switch analysis.type
% %             case 'scalar'
% %                 % ---- This bit does not work!
% %                 eff(kk) = mean( ...
% %                        (abs(nsigmanb) > cutThresholds(1)) ...
% %                      & (abs(nsigmab)  > cutThresholds(2)) ...
% %                      & (abs(fixedrnb) > cutThresholds(4)) ...
% %                      & (abs(fixedrb)  > cutThresholds(5)) ...
% %                      & (signif > cutThresholds(7))  ...
% %                 );
% %             case 'circ'
% %                 pass1 = applycut(En,In,cutThresholds(1),cutTypes{1});
% %                 pass2 = applycut(Ecirc,Icirc,cutThresholds(2),cutTypes{2});
% %                 pass3 = applycut(Ecircn,Icircn,cutThresholds(3),cutTypes{3});
% %                 pass4 = applycut(En,In,cutThresholds(4),cutTypes{4});
% %                 pass5 = applycut(Ecirc,Icirc,cutThresholds(5),cutTypes{5});
% %                 pass6 = applycut(Ecircn,Icircn,cutThresholds(6),cutTypes{6});
% %                 pass7 = (signif > detectionThreshold) ;
% %                 eff(kk) = eff(kk) + 1/Nwf * mean( ...
% %                     pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 ...
% %                     );
% %             case 'linear'
% %                 pass1 = applycut(En,In,cutThresholds(1),cutTypes{1});
% %                 pass2 = applycut(Ep,Ip,cutThresholds(2),cutTypes{2});
% %                 pass3 = applycut(Ec,Ic,cutThresholds(3),cutTypes{3});
% %                 pass4 = applycut(En,In,cutThresholds(4),cutTypes{4});
% %                 pass5 = applycut(Ep,Ip,cutThresholds(5),cutTypes{5});
% %                 pass6 = applycut(Ec,Ic,cutThresholds(6),cutTypes{6});
% %                 pass7 = (signif > cutThresholds(7)) ;
% %                 eff(kk) = eff(kk) + 1/Nwf * mean( ...
% %                     pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 ...
% %                     );
% %         end
% 
%         % ---- Efficiency using all cuts.
%         % ---- Hardwire to case of circular polarisation cut analysis.
%         pass1 = applycut(recInj{iwf}.likelihood{kk}(:,6),recInj{iwf}.likelihood{kk}(:,7),cutThresholds(1),cutTypes{1});
%         pass2 = applycut(recInj{iwf}.likelihood{kk}(:,2),recInj{iwf}.likelihood{kk}(:,3),cutThresholds(2),cutTypes{2});
%         pass3 = applycut(recInj{iwf}.likelihood{kk}(:,4),recInj{iwf}.likelihood{kk}(:,5),cutThresholds(3),cutTypes{3});
%         pass4 = applycut(recInj{iwf}.likelihood{kk}(:,6),recInj{iwf}.likelihood{kk}(:,7),cutThresholds(4),cutTypes{4});
%         pass5 = applycut(recInj{iwf}.likelihood{kk}(:,2),recInj{iwf}.likelihood{kk}(:,3),cutThresholds(5),cutTypes{5});
%         pass6 = applycut(recInj{iwf}.likelihood{kk}(:,4),recInj{iwf}.likelihood{kk}(:,5),cutThresholds(6),cutTypes{6});
%         disp('KLUDGE: using first likelihood as detection statistic.');
%         pass7 = (recInj{iwf}.likelihood{kk}(:,1) > detectionThreshold) ;
%         eff(kk) = eff(kk) + 1/Nwf * mean( ...
%             pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 ...
%             );
% 
%         % ---- Keep copy of efficiency for this waveform.
%         effAll{iwf}(kk)  = mean( pass1 & pass2 & pass3 & pass4 & pass5 & pass6 & pass7 );
%         passAll{iwf,kk} = [pass1 pass2 pass3 pass4 pass5 pass6 pass7];
%         
%         
%     end  %-- loop over injection scales
    
%     % ---- Scatter plots of (Ip,Ep), (Ic,Ec), (In,En), overlaid on web-page
%     %      scatter plots if available. 
%     if analysis.scatterplots
% 
%         if plotOpen==false 
%             filename = [pathToFigs '/off_scatter_' waveform.set{iwf} '_' waveform.name '_plusenergy_plusinc_connected.fig'];
%             if exist(filename)==2
%                 fig1 = open(filename);
%                 set(gcf,'position',[200 500 560 420]);
%             else
%                 display(['File not found: ' filename ]);
%                 fig1 = figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
%             end
%             filename = [pathToFigs '/off_scatter_' waveform.set{iwf} '_' waveform.name '_crossenergy_crossinc_connected.fig'];
%             if exist(filename)==2
%                 fig2 = open(filename);
%                 set(gcf,'position',[200 500 560 420]);
%             else
%                 display(['File not found: ' filename ]);
%                 fig2 = figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
%             end
%             filename = [pathToFigs '/off_scatter_' waveform.set{iwf} '_' waveform.name '_nullenergy_nullinc_connected.fig'];
%             if exist(filename)==2
%                 fig3 = open(filename);
%                 set(gcf,'position',[200 500 560 420]);
%             else
%                 display(['File not found: ' filename ]);
%                 fig3 = figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
%             end
%             plotOpen = true;
%         end
%         figure(fig1);
%         hold on; grid on; legend off
%         scatter(Ep,Ip,[],log10(signif));
%         figure(fig2);
%         hold on; grid on; legend off
%         scatter(Ec,Ic,[],log10(signif));
%         figure(fig3);
%         hold on; grid on; legend off
%         scatter(En,In,[],log10(signif));
%         
%     end  %-- loop over injection scales
   
