clear ; %close all

% ---- https://ldas-jobs.ligo.caltech.edu/~jordan.palamos/GRB_O3/O3a/GRB190613A_O3a_pt2_swift/GRB190613A_O3a_pt2_swift_closedbox.shtml
% -bash-4.2$ cat /home/jordan.palamos/GRB_O3/runs/O3A_pt2/GRB190613A/grb.param 
%    grb.py  -p grb_offline.ini -i H1 -i L1 -i V1 -s  -c /home/jordan.palamos/GRB_O2_cleaned/input/waveforms/ 
%    --long-inj  --disable-fast-injections  -g 1244434056 -r 182.5522 -d 67.24435 -n GRB190613A -e 0.0368849833333 
%    --injdistrib 0.0368849833333 --end-offset 20.745922 --big-mem 2500
baseDir = '/Users/psutton/Documents/xpipeline/trunk/matlab/test/simulate_analysis/GRB190613A/';
matFile = [baseDir 'GRB190613A_O3a_pt2_swift_closedbox.mat'];
asdFile = [baseDir 'GRB190613A_on_source_0_0_ASD.mat'];
trigger.err = 0.036884983333;
%
% matFile = '/Users/psutton/Desktop/GRB200415367_r5794_closedbox.mat';
% asdFile = '/Users/psutton/Desktop/on_source_0_0_ASD.mat';
% % ---- Sky position uncertainty (stat + sys).
% trigger.err = 5.67194851881; 
% ---- Load matFile.
data = load(matFile);

disp('KLUDGES!');
data.vetoMethod = 'alphaLinCutCirc';
data.tunedPrePassCuts = [-1.2 -1.57 -1.27];

% ---- Construct trigger struct.
% ---- GPS time of the trigger.
trigger.gps = data.analysis.gpsCenterTime;
% ---- Compute original ra,dec of the trigger: grid probability = 1 there.
idx = find(data.analysis.skyPositions(:,3)==1);
idx = idx(end);
[trigger.ra,trigger.dec] = earthtoradec(data.analysis.skyPositions(idx,2), ...
    data.analysis.skyPositions(idx,1),data.analysis.skyPositionsTimes(idx));
warning(['trigger.err hardwired to ' num2str(trigger.err) ' deg.']);

% ---- Network.
detectorsCell = data.analysis.detectorList;

% ---- Set coherent consistency cuts. 
if    strcmp(data.vetoMethod,   'alphaLinCutCirc') ...
    & strcmp(data.nullVetoType, 'nullIoverE') ...
    & strcmp(data.plusVetoType, 'circEoverI') ...
    & strcmp(data.crossVetoType,'circnullIoverE')
    %                null           circ           circnull       null           circ            circnull
    cutTypes      = {'alphaIoverE', 'alphaEoverI', 'alphaIoverE', 'ratioIoverE', 'ratioEoverI', 'ratioIoverE'};
else
    error('Only set up to handle alphaLinCutCirc vetos.');
end
cutThresholds = [data.vetoNullRange data.vetoPlusRange data.vetoCrossRange data.tunedPrePassCuts];

% ---- Detection stat and threshold.
disp('KLUDGE!');
% tmp = sort(data.offLoudestEvent.significance);
% idx = ceil(length(idx)*data.percentile_ulCalc/100);
% detectionThreshold = tmp(idx)
% detectionStat      = data.detectionStat
% detectionStat      = 'circenergy';
detectionStat      = 'loghbayesiancirc'
detectionThreshold = 16.9621
% warning('Using incorrect detection statistic.');                
warning('Using hardwired detection threshold.');                
clear tmp idx


% adi-b 1~-0.99773
% bns 1.3135~1.0669~0.93399~1
% sgc300q9 1e-20~0.00333~300~0~0~0.99776
% sgc500q9 1e-20~0.002~500~0~0~0.99884
% sgc1000q9 1e-20~0.001~1000~0~0~0.9976
% sgc2000q9 1e-20~0.0005~2000~0~0~0.99652
% adi-b 	    1.26089     2.00717	0	44.5589	16.9501	47.674
% bns 	    1.47722     2.23089	0	20.616	5.9892	21.4683
% sgc300q9 	0.577254	0.788697	0	14.742	6.04847	15.9345
% sgc500q9 	0.296386	0.42239	0	13.6242	3.83668	14.1541
% sgc1000q9 	0.376684	0.62607	0	22.9175	2.49667	23.0531
% sgc2000q9 	1.23045     2.87299	0	62.0079	3.19943	62.0904
ii = 0;
% % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'sgc70Q9';
% waveform.type{ii}          = 'chirplet';
% waveform.parameters{ii}    = '1.0e-22~0.0143~70~0~0~1';
% % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'sgc100Q9';
% waveform.type{ii}          = 'chirplet';
% waveform.parameters{ii}    = '1.0e-22~0.01~100~0~0~1';
% % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'sgc150q9';
% waveform.type{ii}          = 'chirplet';
% waveform.parameters{ii}    = '1.0e-22~0.00667~150~0~0~1';
% % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'sgc300q9';
% waveform.type{ii}          = 'chirplet';
% waveform.parameters{ii}    = '1.0e-22~0.00333~300~0~0~1';
% % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'bns';
% waveform.type{ii}          = 'inspiral';
% waveform.parameters{ii}    = '1.4~1.4~1~10';
% % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'nsbh';
% waveform.type{ii}          = 'inspiral';
% waveform.parameters{ii}    = '1.4~10~1~20';
% % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'adi-a';
% waveform.type{ii}          = 'adi-a';
% waveform.parameters{ii}    = '10~1';
% % % -----------------------------------------------------------------------
ii = ii+1;
waveform.set{ii}           = 'adi-b';
waveform.type{ii}          = 'adi-b';
waveform.parameters{ii}    = '20~1';
% % % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'adi-c';
% waveform.type{ii}          = 'adi-c';
% waveform.parameters{ii}    = '10~1';
% % % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'adi-d';
% waveform.type{ii}          = 'adi-d';
% waveform.parameters{ii}    = '10~1';
% % % -----------------------------------------------------------------------
% ii = ii+1;
% waveform.set{ii}           = 'adi-e';
% waveform.type{ii}          = 'adi-e';
% waveform.parameters{ii}    = '10~1';
% % -----------------------------------------------------------------------
% ---- These properties are applied to all waveform sets:
waveform.polarisation  = 'elliptical';
waveform.maxIncl       = 0;
waveform.name          = '****';
waveform.ul50          = 1.6614;
waveform.ul90          = 2.19915;


pathToFigs = '';
% analysis      Struct. Required fields:
%                 pixelselection: one of 'bpp', 'matchbw', 'maxsnr', 'minNpix'
%                 mode: one of 'spectrogram', 'emulatefft', 'fft'
%                 type: one of 'GR', 'scalar'
%                 usenoise: true or false
%                 verbose: true or false
%                 spectrumfile: Name of file containing averaged noise spectra
%                   for all of the detectors. 
%                 N_initial: Initial length of the signal timeseries, in samples.
%                 fs: Scalar. Sample rate to use for simulation [Hz].
%               Optional fields:
%                 catalogDirectory: optional string specifying location of
%                   catalog directory for use by XMAKEWAVEFORM. If not supplied a
%                   default value is assumed. Default
%                   '/Users/psutton/Documents/xpipeline/branches/linear/utilities/waveforms/'.
%                 scatterplots: true or false. If true, scatter plots of
%                 coherent energies are made. Default true.
%                 skyGridFile: string. If specified, then sky grid is read from
%                   this file.
% analysis.pixelselection = 'bpp';
% analysis.tfftselection  = 'matchbw';
% analysis.tfftselection  = 'maxsnr';
% analysis.tfftselection  = 'minNpix';    
analysis.mode           = 'spectrogram';
analysis.type           = 'GR';
analysis.usenoise       = true;
analysis.verbose        = true;
% analysis.spectrumfile   = '/Users/psutton/Desktop/GRB200409A_r5781test3_closedbox.mat';
% analysis.spectrumfile   = '/Users/psutton/Desktop/on_source_0_0_ASD.mat';
analysis.fs             = data.analysis.sampleFrequency; % for making waveforms, etc.
analysis.N_initial      = analysis.fs * 8;
analysis.catalogDirectory =  '/Users/psutton/Documents/xpipeline/branches/waveforms/';
analysis.delayTol       = 5e-4;
analysis.testSkyPos     = true;
analysis.Ninj           = 100;
analysis.testSkyLocalisation = true;
analysis.makeSkyPlots   = false;
analysis.scatterplots   = false;
analysis.makeFinalPlots = false;
analysis.injectionScales = data.injectionScaleSorted(1,:); % assumes same for all wfs
analysis.spectrumfile    = asdFile;
analysis.sampleFrequency = data.analysis.sampleFrequency; % for reading spectra from file
analysis.pointingName    = 'Elhbcirc';

%
% eff           Vector. Predicted efficiencies at corresponding injection scales.
% scale         Vector. Injection scale for each efficiency.
% effAll        Cell array. Each element is a vector of predicted efficiencies
%               for the corresponding waveform in waveform.parameters.
%

[effAll, passAll, injectionScales, recInj] = xsimulategrbanalysis(trigger, ...
    detectorsCell,cutTypes,cutThresholds,detectionStat,detectionThreshold, ...
    waveform,pathToFigs,analysis);

figure;
for ii = 1:length(waveform.set)
    % ---- Estimate injection scale for 50% and 90% efficiency. This is a bit
    %      fiddly.
    X = injectionScales;
    Y = effAll{ii};
    idx = find(Y==0);
    Y(idx(1:end-1)) = [];
    X(idx(1:end-1)) = [];
    idx = find(Y==1);
    X(idx(2:end)) = [];
    Y(idx(2:end)) = [];
    is50(ii) = 10^robustinterp1(Y,log10(X),0.5);
    is90(ii) = 10^robustinterp1(Y,log10(X),0.9);

    % ---- Plot.
    semilogx(injectionScales,effAll{ii},'-o','linewidth',2)
    grid on; hold on
    xlabel('injection scale');
    ylabel('efficiency');
    legendText{ii} = [waveform.set{ii} ' ('  num2str(is50(ii),3) '/' num2str(is90(ii),3) ')' ];
end
legend(legendText,'Location','northwest');
set(gca,'fontsize',16)
