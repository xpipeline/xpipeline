function [pass]= applycut(E,I,threshold,cutType)
%
%
% 
if size(I)~=size(E)
    error('Inputs I and E must be the same size.');
end

switch lower(cutType)

    case 'alphaeoveri'

        nsigma = 2*(E - I)./((I+E).^0.8) + 1;

    case 'alphaiovere'

        nsigma = 2*(I - E)./((I+E).^0.8) + 1;

    case 'ratioiovere'

        nsigma = I./E;

    case 'ratioeoveri'

        nsigma = E./I;

    otherwise 

        error('cutType not recognised.');

end

% ---- Prepare output.
if threshold > 0 

    pass = abs(nsigma) > threshold;

elseif threshold < 0 

    pass = nsigma > abs(threshold);

elseif threshold == 0

    pass = logical(ones(size(E)));

else

    error('How did we get here? Is threshold not a real scalar?');

end


