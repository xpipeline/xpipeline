% XSIMULATEANALYSISWRAPPER - wrapper script for XSIMULATEANALYSIS
%
% $Id$

% -------------------------------------------------------------------------
%    Preparatory.
% -------------------------------------------------------------------------

% % ---- Recall the ordering of cut thresholds as presented on web pages:
% %      alpha-null, alpha-plus/circ, alpha-cross/circnull, 
% %      ratio-null, ratio-plus/circ, ratio-cross/circnull
% %      So include one of the following with your trigger info:
% % ---- Linear polarisation cuts.
% cutTypes = {'alphaIoverE','alphaEoverI','alphaEoverI', ...
%              'ratioIoverE','ratioEoverI','ratioEoverI'};
% % ---- Circular polarisation cuts.
% cutTypes = {'alphaIoverE','alphaEoverI','alphaIoverE', ...
%              'ratioIoverE','ratioEoverI','ratioIoverE'};


% -------------------------------------------------------------------------
%    External trigger.
% -------------------------------------------------------------------------

% % ---- Triggers values for SN2007gr. See https://wiki.ligo.org/Bursts/XOpticalSN2007gr
% trigger.ra  = 40.8666;
% trigger.dec = 37.3458;
% trigger.err = 0;
% trigger.gps = [870768590,871215278]; %-- or [gps_min,gps_max]

% % ---- Triggers values for fake GRB at sky position of SN2007gr. 
% trigger.ra  = 40.8666;
% trigger.dec = 37.3458;
% trigger.err = 10;
% trigger.gps = round(mean([870768590,871215278]));

% ---- Triggers values for northern sky patch of G184098/GW150914.  From 
%      https://ldas-jobs.ligo.caltech.edu/~dhoak/grbs/ER8/GW150914/G184098_2/G184098_r5010xT1/G184098_r5010xT1_openbox.shtml
trigger.ra  = 133.0;
trigger.dec = 6.3;
trigger.err = 10;
trigger.gps = 1126259461;
cutTypes = {'alphaIoverE','alphaEoverI','alphaIoverE', ...
             'ratioIoverE','ratioEoverI','ratioIoverE'};
cutThresholds = [0,  -1.5,  -2, 0, 0, 0, 0];
detectionStat = 'loghbayesiancirc';
detectionThreshold = 9.1;
% detectionThreshold = 0;
% warning('using zero threshold on significance');
network = 'H1L1'; 
path_to_figfiles = './';
path_to_wffiles  = './';


% -------------------------------------------------------------------------
%    Select waveforms to process.
% -------------------------------------------------------------------------
 
clear waveform
iset = 0;

% ---- Uncomment as many of these sets as you wish.
iset = iset + 1;
waveform(iset).set = 'sgc100Q9';
waveform(iset).type = 'chirplet';
waveform(iset).parameters{1} = '1.0e-22~0.01~100~0~0~1';
waveform(iset).polarisation = 'elliptical';
waveform(iset).maxIncl = 5;
waveform(iset).name = 'junk';
waveform(iset).ul50 = 2.48;
waveform(iset).ul90 = NaN;

iset = iset + 1;
waveform(iset).set = 'sgc150Q9';
waveform(iset).type = 'chirplet';
waveform(iset).parameters{1} = '1.0e-22~0.006667~150~0~0~1';
waveform(iset).polarisation = 'elliptical';
waveform(iset).maxIncl = 5;
waveform(iset).name = 'junk';
waveform(iset).ul50 = [0.939];
waveform(iset).ul90 = [1.607];

iset = iset + 1; 
waveform(iset).set = 'sgc300Q9';
waveform(iset).type = 'chirplet';
waveform(iset).parameters{1} = '1.0e-22~0.003333~300~0~0~1';
waveform(iset).polarisation = 'elliptical';
waveform(iset).maxIncl = 5;
waveform(iset).name = 'junk';
waveform(iset).ul50 = [1.11];
waveform(iset).ul90 = [20.2];

iset = iset + 1; 
waveform(iset).set = 'insp1414';
waveform(iset).type = 'inspiral';
waveform(iset).parameters{1} = '1.35~1.35~1~10';
waveform(iset).polarisation = 'elliptical';
waveform(iset).maxIncl = 30;
waveform(iset).name = 'junk';
waveform(iset).ul50 = [5.47];
waveform(iset).ul90 = [NaN];

iset = iset + 1; 
waveform(iset).set = 'insp1450';
waveform(iset).type = 'inspiral';
waveform(iset).parameters{1} = '1.35~5~1~20';
waveform(iset).polarisation = 'elliptical';
waveform(iset).maxIncl = 30;
waveform(iset).name = 'junk';
waveform(iset).ul50 = [4.94];
waveform(iset).ul90 = [6.64];

analysis.injectionScales = [0.0100,0.0147,0.0215,0.0316,0.0464,0.0681,0.1000,0.1468,0.2154,0.3162,0.4642,0.6813,1.0000,1.4678,2.1544,3.1623,4.6416,6.8129,10.0000,14.6780,21.5443,31.6228,46.4159,68.1292,100.0000];
% analysis.injectionScales = 10.^[0:0.25:2]
% analysis.injectionScales = 10.^[0:0.5:2]


% -------------------------------------------------------------------------
%    Set up analysis struct.
% -------------------------------------------------------------------------

% ---- Sample frequency and maximum timeseries duration.
analysis.fs = 4096;
analysis.N_initial = 16*analysis.fs;
% ---- Choose one of these TF analysis options.
% analysis.pixelselection = 'bpp';
% analysis.pixelselection = 'matchbw';
analysis.pixelselection = 'maxsnr';
% analysis.pixelselection = 'minNpix';
% ---- Choose one of these TF analysis options.
analysis.mode = 'spectrogram';
% analysis.mode = 'emulatefft'; analysis.pixelselection = 'foo';
% analysis.mode = 'fft';
% ---- Set each of these as desired.
analysis.type = 'circ';
% analysis.type = 'linear';
% analysis.type = 'scalar';
% ---- More required fields.
analysis.usenoise = true;
analysis.verbose = false;
analysis.testSkyLocalisation = true;
analysis.delayTol = 0.0005; 
% ---- Optional fields.
analysis.Ninj = 250;
analysis.catalogDirectory = '/Users/psutton/Documents/xpipeline/branches/linear/utilities/waveforms/';
% analysis.spectrumfile = 'averaged_spectra.mat';
% analysis.skyGridFile = 'sky_positions_0.txt';
% ---- Plotting options.
analysis.scatterplots = false;
analysis.makeFinalPlots = false;
analysis.makeSkyPlots = false;
analysis.testSkyPos = true;
% ---- Select one of these.
analysis.pointingName = 'Elhbcirc';
% analysis.pointingName = 'Ecirc';
% analysis.pointingName = 'testStatN';
% analysis.pointingName = 'testStatC';
% analysis.pointingName = 'testStatCN';

% ---- Accuracy of the various methods:
%      numerical: matchbw excellent, maxsnr, minNpix equiv and very good, bpp
%        somewhat poorer but okay
%      piro: matchbw hrss too low, maxsnr bang on, bpp even better, minNpix too
%        high; fft unworkable (too many pixels) 
%      rotbar: all shit, bpp best, then maxsnr, minNpix, fft equiv, better than
%        matchbw 
%      sgel: maxsnr and minNpix equiv, better than matchbw (for SG235) and bpp
%        (both)
%      sglin: as sgel, minNpix slightly better
%
% % ---- Comments on performance of various TF options for SN2007gr analysis.
% % ---- Works for spectrogram (Tfft=1/16, Npix=10), iS but not for hrss -> first inj has hrss =
% %      9.3523e-24! hrss curve off by this much - consistent. OKAY
% waveform.set{iset} = 'numerical3';
% 
% % ---- Works for spectrogram (Tfft=1/16, Npix=10), iS but not for hrss -> first inj has hrss =
% %      1.7068e-20! hrss curve off by this much - consistent. OKAY
% waveform.set{iset} = 'piro5';
% 
% % ---- Works for spectrogram (Tfft=1/16, Npix=10), iS but not for hrss -> first inj has hrss =
% %      1.8369e-20! hrss curve off by this much - consistent. OKAY 
% waveform.set{iset} = 'rotbar5';
%
% % ---- Works for non-spectrogram, hrss and iS. Works for spectrogram with
% %      default params (Tfft = 1/16, Npix=10). Efficincies only slightly lower
% %      than with better optimised Tfft (1/32,1/128):
% %      spec:   0         0         0    0.3675    0.8550    0.9125    0.9500    0.8850
% %      opt:    0         0         0    0.4200    0.8600    0.9275    0.9675    0.9000
% waveform.set{iset} = 'sgel2';
%
% % ---- Works for non-spectrogram, hrss and iS. Works for spectrogram with
% %      default params (Tfft = 1/16, Npix=10). Efficincies only slightly lower
% %      than with better optimised Tfft (1/32,1/128): 
% %      spec:    0         0         0    0.3150    0.7400    0.8350    0.8700    0.7975
% %      opt:     0         0         0    0.3525    0.7725    0.8350    0.8525    0.8050
% waveform.set{iset} = 'sglin2';



% -------------------------------------------------------------------------
%    Perform analysis.
% -------------------------------------------------------------------------

% ---- Pull network string apart into cell array of detectors.
clear detectorsCell
for ii=1:length(network)/2
    detectorsCell{ii} = network((ii-1)*2+[1:2]);
end

% ---- Loop over waveform sets.
for iset = 1:length(waveform);
    [eff{iset},scale{iset},effAll{iset}] = xsimulateanalysis(trigger, ...
        detectorsCell, cutTypes, cutThresholds, detectionStat, detectionThreshold, ...
        waveform(iset), path_to_figfiles, analysis);

    % ---- Plot efficiencies.
    figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
    semilogx(analysis.injectionScales,effAll{iset}{1},'-o','markerfacecolor','b','linewidth',2);
    grid on; hold on;
    axis([get(gca,'xlim'), 0 1])
    xlabel('injection scale');
    ylabel('efficiency');
    title([waveform(iset).type ' - ' waveform(iset).parameters{1}]);
    
%     filename = [path_to_wffiles 'SN2007gr-' network '-' waveform.set{iset} '_eff-vs-hrss.fig'];
%     fig0 = open(filename);
%     hold on
%     lineStyle = {'b-.+','g-.+','r-.+','m-.+','c-.+','y-.+'};
%     [~, shortTypes, ~, scale1hrss] = snmdcinfo(waveform.set{iset});
%     disp(['Predicted hrss50/hrss90 upper limits: ']);
%     for iNwf=1:length(waveform.parameters{iset})
%         ulPred = scale1hrss(iNwf)*10.^robustinterp1(effAll{iNwf},log10(scale),[0.5 0.9]);
%         disp(['  ' shortTypes{iNwf} ': ' num2str(ulPred(1)) ' / ' num2str(ulPred(2)) ]);
%         semilogx(scale1hrss(iNwf)*scale,effAll{iNwf},lineStyle{iNwf},'DisplayName',['(pred) ' shortTypes{iNwf} ' (' num2str(ulPred(1)) ')/(' num2str(ulPred(2)) ')'])
%     end
%     legend('off')
%     legend(gca,'show','Location','EastOutside')        
%     saveas(fig0,['eff_' network '-' waveform.set{iset} '_eff-vs-hrss.png'],'png')

end

