function signif = xreproducegrbanalysis(runDir,autoDir,boxType)
% XREPRODUCEGRBANALYSIS - attempt to reproduce X-Pipeline analysis results from first principles
%
% usage:
%
% [effAll, passAll, injScales, recInj] = xreproducegrbanalysis(runDir,autoDir,boxType)
%
% runDir        String. Absolute path to first-stage run directory (i.e., where
%               grb.py was run). 
% autoDir       String. Absolute path to second-stage run directory (i.e., the 
%               auto-web directory from xgrbwebpage.py). 
% boxType       Optional string. One of 'closedbox', 'openbox', or 'ulbox'.
%               Default 'closedbox'.
%
% effAll        Cell array. Each element is a vector of predicted efficiencies
%               for the corresponding waveform in waveform.parameters at the
%               injection scales in injectionScales. 
% passAll       Cell array. Each element is an array of pass values for the
%               injections of the corresponding waveform (row index) and
%               injection scale (column index). The pass value arrays have
%               one row for each injection, and 7 columns. The first six
%               columns are the pass flags for the coherent consistency
%               tests. The seventh is the pass flag for significance louder
%               than the loudest event threshold.
% injScales     Vector. Injection scales used for all waveforms.
% recInj        Recovered injections
%
% XREPRODUCEGRBANALYSIS reads the X-Pipeline input parameters (trigger
% information, injection logs, etc.) and post-processing cuts for an already
% performed GRB analysis and attempts to reproduce the efficiency results from a
% quasi-first-principles analysis. It uses the sky location, measured noise
% spectra, and tuned consistency cut thresholds to estimate the detection
% efficiency as a function of amplitude for each of the waveform types tested.
% This can be compared to the measured analysis results as a validation of the
% search.
% 
% $Id$




% Procedure:  
%
% - Construct network:
%   Read network of detectors from input/channels.txt
%   Read sky position files from input/
%   Read noise spectra from on_source ASD file in output/
%
% - Read injection data:
%   Read injection scales from .ini file
%   Determine waveform sets by scanning input/ for injection log files.
%   For each waveform set 
%       Read inj proc mask, assoc trigs from associated trigs .mat file
%       Read log file from input/
%       For each inj 
%           if processed:
%               Generate injection timeseries.
%               for each scale
%                   for each sky grid point
%                       estimate likelihoods (simulate time-frequncy analysis)
%                   select "best" grid point, record simulated event cluster
%                   apply coherent cuts
%                   apply significance threshold
%       Compute efficiencies.
%       Overlay simulated triggers, efficiencies to web page plots.

% 3. [BIG] Write wrapper script with capability to read a grb run directory and
%    pull out trigger data, injections, etc. from grb.param, ini file, web page,
%    etc.   
% 4. Test on a Swift GRB for which X works well, determine TF analysis model
%    that best predicts likelihoods and time-frequency properties of injections
%    (eg FFT length, nPixels, duration, bw).
% 5. Repeat 4. with optimised TF analysis to (hopefully) verify efficiency
%    modelling for standard SGC, inspiral waveforms.
% 6. Test on some Fermi/IPN GRBs with large error boxes, attempt to verify sky
%    localisation behaviour.
% 7. Repeat 6. with different pointing statistics and try to find a better
%    statistic. 
% 8. Repeat 5. for a SN analysis to (hopefully) verify efficiency modelling for
%    that case too. Note that SN waveforms may require different TF analysis.
%
% Notes: 
%
% The different TF thresholding procedures (pixelselection and mode fields of
% the analysis struct) vary the hrss curves by tens of percent or more.
% Different choices give better efficiency matches for different waveforms. This
% could use more investigation!

% ---- KLUDGE: hardwired parameters.
verbose = true;
catDir  = '/Users/psutton/Documents/xpipeline/branches/waveforms/';

% ---- Check inputs.

% ---- Ensure directories end with '/'.
if ~strcmp(runDir(end),'/')
    runDir(end+1) = '/';
end
if ~strcmp(autoDir(end),'/')
    autoDir(end+1) = '/';
end

% ---- Assign default arguments.
if nargin < 3
    boxType = 'closedbox';
end

% ---- Find and read the .ini file.
[status,result] = system(['cat ' runDir 'grb.param ']);
result = split(result);
for ii=1:length(result)
    if strcmp(result{ii},'-p') || strcmp(result{ii},'--params-file')
        iniFile = [runDir result{ii+1}];
    end
end; clear ii
[iniData, iniHyphens] = parseinifile(iniFile);

% ---- Extract detector network from channel file. We could also get this from
%      the ASD file and so drop dependency on the channel file.
[channelNames, channelVirtualNames, frameTypes, detectorList] = readchannelfile([runDir 'input/channels.txt']);

% ---- Read the sky search grids. splitlines() splits the string into a cell
%      by newlines; the last element is always empty. In this case the second
%      last element is input/sky_positions_trigger_time.txt which is not of
%      interest.
[status,result] = system(['ls -1 ' runDir 'input/sky_positions_*.txt']);
result = splitlines(result);
for ii = 1:(length(result)-2)
    [searchgrid{ii}.theta,searchgrid{ii}.phi,searchgrid{ii}.prob,searchgrid{ii}.area] = readskypositionfile([runDir 'input/sky_positions_0.txt']);
end; clear ii

% disp('KLUDGE: delete me!')
% searchgrid{1}.theta = searchgrid{1}.theta(1);
% searchgrid{1}.phi   = searchgrid{1}.phi(1);
% searchgrid{1}.prob = searchgrid{1}.prob(1);
% searchgrid{1}.area = searchgrid{1}.area(1);

% ---- Load detector noise spectra.
spectra = load([runDir 'output/on_source_0_0_ASD.mat']);
% ---- Variable spectra.S is actually the ASD not PSD. Square it to avoid
%      confusion later.
spectra.S = (spectra.S).^2;
% ---- Frequencies at which spectra are sampled.
spectra.f = (0:spectra.dF:spectra.sampleFrequency/2)';
% ---- Network-averaged noise spectrum.
spectra.Savg = (mean(squeeze(sum((spectra.S).^(-2),1)),2)).^(-0.5);
if verbose
    figure; loglog(spectra.f,spectra.S(1,:,1)); 
    grid on; hold on; 
    for a=2:length(detectorList)
        loglog(spectra.f,spectra.S(a,:,1)); 
    end
    loglog(spectra.f,spectra.Savg);
end

% ---- Extract injection scales from .ini file. (This version only works for a
%      common set of injection scales for all waveform sets.) 
injectionScales = str2num(iniData.injection.injectionScales);
% % ---- Grep the line out of the file, split on the '=', discard the endline, and
% %      convert to a numeric array.
% % THIS WILL FAIL IF THE WORD injectionScales OCCURS MORE THAN ONCE IN THE FILE, EG IN COMMENTS.
% [status,result] = system(['grep injectionScales ' iniFile]);
% idx = strfind(result,'=');
% result = splitlines(result(idx+1:end));
% injectionScales = str2num(result{1})

% ---- Determine waveform sets from .ini file.
waveformSet = fieldnames(iniData.waveforms);
% ---- Replace underscores by hyphens where needed to get correct file names.
for iset=1:length(waveformSet)
    if ~isempty(getfield(iniHyphens.waveforms,waveformSet{iset}))
        oldName = waveformSet{iset};
        oldName(getfield(iniHyphens.waveforms,waveformSet{iset})) = '-';
        waveformSet{iset} = oldName;
    end
end; clear iset
% % ---- Determine waveform sets by scanning input/ for injection log files.
% [status,result] = system(['ls -1 input/injection_*.txt']);
% result = splitlines(result);
% for ii = 1:(length(result)-1)
%     idx = strfind(result{ii},'.');
%     waveformSet{ii} = result{ii}(17:idx-1);
% end

% ---- Figure out GRB name and user tag from the xgrbwebpage.param file. We will
%      need these for predicting file names.
[~, args] = parseparamfile([autoDir 'xgrbwebpage.param']);
for ii=1:size(args,1)
    if strcmp(args{ii,1},'-g') || strcmp(args{ii,1},'--grb-name')
        grbName = args{ii,2};
    elseif strcmp(args{ii,1},'-u') || strcmp(args{ii,1},'--user-tag')
        userTag = args{ii,2};
    end
end; clear ii

% ---- Other common paramaters for handling all waveform sets.
padTriggers = true;
threshold   = 0;
nInjections = str2num(iniData.injection.injectionInterval);
if nInjections<0
    nInjections = -nInjections;
else
    error('Function is only set up to handle standard case of nInjections<0.');
end

% ---- Loop over waveform sets.
for iset = 1:1%length(waveformSet)

    warning('only processing first set of injections, for testing / debugging')

    if verbose
        disp(' ')
        disp(['Processing injection set ' num2str(iset) ' of ' num2str(length(waveformSet)) '...']);
    end
    
    % ---- Load trigger information from "associatedTriggers" file.
    injAssTrigFileName = [autoDir grbName '_' userTag '_' boxType 'associatedTriggers_' waveformSet{iset} '.mat']
    [injAssTrig{iset}, injProcMask{iset}, injScale{iset}, foundIdx{iset}, missedIdx{iset}] = ...
          loadinjassoctrigfile(injAssTrigFileName,padTriggers,threshold,nInjections);
    disp(['Loaded ' num2str(size(injAssTrig{iset},1)) ' triggers at ' num2str(size(injAssTrig{iset},2)) ' injection scales.'])
    
    % ---- Read injection log file.
    injLogFileName{iset} = [runDir 'input/injection_' waveformSet{iset} '.txt']
    injParams{iset} = parseinjectionparameters(readinjectionfile(injLogFileName{iset}),verbose);

    % ---- Loop over processed injections.
    procMask = injProcMask{iset};
%     for idx = find(procMask(:,1).')
%         disp(['Processing injection ' num2str(idx)]);
    idx = find(procMask(:,1).');
    for idx = idx(1:1)
        disp('KLUDGE: Only processing first injection.');
        
        % ---- Generate injection timeseries. 
        %      KLUDGE: use the approximation that the injection is in the centre of 
        %      the analysis block. Later we will alter the code to account for
        %      the possibility of clipping of long duration injections.
        %      APPROXIMATION: use the final sample frequency for all detectors.
        blockTime  = str2num(iniData.parameters.blockTime);
        disp('KLUDGE: using 8 sec blockTime');
        blockTime = 8;
        startTime  = injParams{iset}.gps_s(idx) - blockTime/2;
        sampleRate = str2num(iniData.parameters.sampleFrequency);
        injData = xinjectsignal(startTime,blockTime,channelNames,sampleRate*ones(size(detectorList)),injLogFileName{iset},0,idx,'catalogDirectory',catDir);
        injData = cell2mat(injData.');
        figure; plot([0:1/sampleRate:blockTime-1/sampleRate]',injData);

        % ---- Signal struct input for xsimulatetfanalysis.
        signal.h               = injData;
        signal.fs              = sampleRate;
        signal.injectionScales = injectionScales;

        % ---- Signal struct input for xsimulatetfanalysis.
        analysis.bpp           = 99; 
        analysis.skyGrid       = searchgrid{end};
        disp('KLUDGE: Using only last search grid.')
        analysis.verbose       = verbose;
        analysis.Tfft          = str2num(iniData.parameters.analysisTimes)
        % analysis.Tfft              = 1/32;
        % ---- Optionally reset Tfft to match the signal.
        analysis.optimalTfft       = false;
        analysis.optimalTfftMetric = 'minNpix';
        if analysis.optimalTfft
            if analysis.verbose
                disp(['Setting time-frequency analysis parameters.']);
            end
            % ---- Use network-averaged spectrum and signal in first detector.
            analysis.Tfft = optimaltfft(signal.h(:,1),signal.fs,spectra.Savg,spectra.minimumFrequency, ...
                spectra.dF,analysis.optimalTfftMetric,analysis.verbose);
        end
        disp('KLUDGE: Testing only one FFT length.')
        % ---- Simulate the time-frequency analysis of this injection.
        for iTfft=1:1%length(analysis.Tfft)
            tmp_analysis = analysis;
            tmp_analysis.Tfft = tmp_analysis.Tfft(iTfft)
            signif{iTfft} = xsimulatetfanalysis(spectra,signal,tmp_analysis);
        end
        
    end
    
end; clear iset


% ---- Done.
return




    



% =========================================================================
%    Make plots.
% =========================================================================

if analysis.makeFinalPlots
    % ---- Plot predicted efficiency, overlaid on actual measured search
    %      efficiency if available.
    filename = dir([pathToFigs 'eff_' waveform.set{iwf} '.fig']);
    if exist([pathToFigs filename.name],'file')==2
        fig4 = open([pathToFigs filename.name]);
    else
        save workspace.mat
        error('Agh!');
        % figure; set(gca,'fontsize',16); set(gca,'xscale','log')
    end
    % ---- Remake plot in temrs of injection scale instead of hrss, since the web
    %      page gets the hrss wrong in the case of mixed waveform sets. Injection 
    %      scale is more robust.
    [X,Y] = datafromplot(gca);
    close(fig4);
    %
    % axis(gca);
    % grid on; hold on
    % semilogx(hrss_eff(1:(end-1)),eff(1:(end-1)),'b-o','markerfacecolor','g')
    % title(waveform.set{iwf})
    % xlabel('hrss amplitude (Hz^{-1/2})')
    % ylabel('efficiency')
    % legend('meas','meas UL','meas 50 %','alpha-stat',4)
    % 
    % ---- This bit is hardcoded for the OSN search ...
    disp(' ');
    disp('KLUDGE: using injection scales hard-coded for the OSN search.');
    injectionScalesMDC = [0.00100,0.00316,0.01000,0.01778,0.03162,0.05623,0.10000,0.13335,0.17783, ...
        0.23714,0.31623,0.42170,0.56234,0.74989,1.00000,1.33352,1.77828,2.37137, ...
        3.16228,4.21697,5.62341,7.49894,10.00000,13.33521,17.78279,23.71374, ...
        31.62278,56.23413,100.00000,316.22777,1000.00000];
    keep = [];
    for ii=1:length(Y)
        if numel(Y{ii})==numel(injectionScalesMDC)
            keep(end+1) = ii;
        end
    end
    if length(keep)~=2
        save workspace.mat
        error('More than two plots in efficiency figure with expected number of elements.');
    end
    % ---- Figure out which efficiency is without DQ vetoes (the higher one).
    if any(Y{keep(1)}>Y{keep(2)})
        y_noveto = Y{keep(1)};
        y = Y{keep(2)};
    else
        y_noveto = Y{keep(2)};
        y = Y{keep(1)};
    end
    % ---- Fresh plot.
    figure; set(gca,'fontsize',16); set(gca,'xscale','log')
    semilogx(injectionScalesMDC,y,'k-o','markerfacecolor','k','linewidth',2)
    grid on; hold on
    semilogx(injectionScalesMDC,y_noveto,'b-')
    semilogx(injectionScales(1:end-1),eff(1:end-1),'g-+','linewidth',2)
    legend('measured','measured (no vetoes)','predicted',2)
    xlabel('injection scale')
    ylabel('efficiency')
    title(waveform.set{iwf})
    axis([1e-3 1e3 0 1])
    saveas(gcf,['eff_' waveform.set{iwf} '_' siteStr '.png'],'png')
%     saveas(gcf,['eff_' waveform.set{iwf} '_' siteStr '_'  analysis.type '.png'],'png')

    % ---- Set output variables.
    injectionScales = injectionScales(1:end-1);
    eff = eff(1:end-1);
    for ii=1:Nwf
        effAll{ii} = effAll{ii}(1:end-1);
    end
end

% ---- Dump workspace for debugging.
if analysis.verbose
    save workspace.mat
end

% ---- Done.
return



% =========================================================================
%    Helper functions.
% =========================================================================

function flag = isrealenough(x,threshold,name)
% ISREALENOUGH - test for data being almost real and non-NaN
%
% usage
%
%   flag = isrealenough(x,threshold,name)
%
% x             Array of complex data.
% threshold     Real scalar.
% name          String. Name of data being tested. Used for output messages
%               (any(isnan())). 
%
% flag          Boolean. True if data obeys the conditions 
%               max(abs(imag(x))./abs(x)) < threshold and no values of x are
%               NaN.
flag = true;
x = x(:);
if max(abs(imag(x))./abs(x)) >= threshold 
    warning(['max(abs(imag(' name '))./abs(' name ')) = ' num2str(max(abs(imag(x))./abs(x))) ]);
    flag = false;
elseif any(isnan(real(x)))
    warning(['NaN values for real(' name ')']);
    flag = false;
elseif any(isnan(imag(x)))
    warning(['NaN values for imag(' name ')']);
    flag = false;
end
return
