% This is a script for modelling the sensitivity of the coherent WaveBurst
% pipeline during the first year of the S5 run.
% 
% Usage:
%
% You need to define the following quantities:
%
%   network     String.  Currently only handles 'H1H2L1' (case-insensitive).
%   waveform    String.  Currently one of 'HGA2', 'HWNB2', 'SG1', 'SG5', or
%               'SG6' (case-insensitive). 
%
% Then simply call the script in Matlab.  For example,
% 
% clear; network = 'H1H2L1'; waveform = 'HGA2'; MC_cWB_S5
% clear; network = 'H1H2L1'; waveform = 'SG1'; MC_cWB_S5
% clear; network = 'H1H2L1'; waveform = 'SG5'; MC_cWB_S5
% clear; network = 'H1H2L1'; waveform = 'SG6'; MC_cWB_S5
% clear; network = 'H1H2L1'; waveform = 'HWNB2'; MC_cWB_S5
% 
% author: Patrick J. Sutton (patrick.sutton@astro.cf.ac.uk)


% -------------------------------------------------------
%    Preliminaries.
% -------------------------------------------------------

% ---- Format output to stodout.
format compact


% -------------------------------------------------------
%    Monte Carlo parameters.
% -------------------------------------------------------

% ---- Number of Monte Carlo injections at each injection amplitude.
Nmc = 1e4;


% -------------------------------------------------------
%    Pick network to analyse
% -------------------------------------------------------

switch lower(network)

    case 'h1h2l1'

        % ---- LIGO S5 analysis details.
        % ---- Overall scale factor to allow for calibration uncertainty.
        calFactor = 1.0;  %-- no cal uncertainty applied.        
        
        % ---- Number of detectors.
        K = 3;
        
        switch lower(waveform)
            
            % ---- Measured hrss points for the LIGO first-year S5
            %      analysis. From  
            %        http://tier2.ihepa.ufl.edu/~klimenko/waveburst/S5/ 
            %          coherent/s5run47/firstS5/s5_L1H1H2_r47zero1.html  

            case 'hga2'
                
                % ---- Gaussians.
                %      columns: duration (ms)  hrss@50%  hrss@90%
                results = [
                    0.05  2.38e-21  1.16e-20         
                    0.1   1.66e-21  7.75e-21         
                    0.25  1.11e-21  5.07e-21         
                    0.5   9.36e-22  4.34e-21         
                    1.0   9.76e-22  4.57e-21         
                    2.5   1.80e-21  8.42e-21         
                    4.0   4.57e-21  2.12e-20         
                    6.0   1.67e-20  7.42e-20         
                    8.0   7.05e-20  3.01e-19 ];            
                f = 127./results(:,1);  % bw = 95.8/results(:,1);
                hmeas50 = results(:,end-1);
                hmeas90 = results(:,end);
                tau = results(:,1)*1e-3;
                clear results
                twoPolarizations = 0;

            case 'hwnb2'
                
                % --- WNBs.
                %   fmin   bw   dur  hrss@50%  hrss@90%
                results = [
                     100   10  0.10  8.18e-22  1.97e-21
                     100  100  0.01  6.12e-22  1.48e-21
                     100  100  0.1   7.15e-22  1.56e-21
                     250   10  0.10  5.41e-22  1.25e-21
                     250  100  0.01  6.93e-22  1.62e-21
                     250  100  0.10  8.02e-22  1.69e-21
                    1000   10  0.10  1.95e-21  4.67e-21
                    1000  100  0.01  2.06e-21  4.67e-21
                    1000 1000  0.001 3.15e-21  6.92e-21
                    1000  100  0.10  2.59e-21  5.40e-21
                    1000 1000  0.01  3.56e-21  7.36e-21
                    1000 1000  0.1   5.98e-21  1.39e-20 ];
                f = results(:,1) + 0.5*results(:,2);
                hmeas50 = results(:,end-1);
                hmeas90 = results(:,end);
                clear results
                twoPolarizations = 1;
        
            case 'sg1'

                % ---- Q9 sine-Gaussians.
                %      Columns: f, hrss50%, hrss90%
                results = [
                       70   2.40e-21  1.05e-20      
                      100   9.56e-22  4.25e-21      
                      153   5.94e-22  2.64e-21      
                      235   5.75e-22  2.48e-21      
                      361   1.03e-21  4.48e-21      
                      554   1.16e-21  5.07e-21      
                      849   1.78e-21  7.92e-21      
                      945   2.00e-21  9.25e-21      
                     1053   2.31e-21  1.02e-20      
                     1172   2.51e-21  1.15e-20      
                     1304   2.87e-21  1.27e-20      
                     1451   3.25e-21  1.44e-20      
                     1615   3.49e-21  1.57e-20      
                     1797   3.99e-21  1.79e-20      
                     2000   5.07e-21  2.30e-20 ];
                f = results(:,1);
                hmeas50 = results(:,end-1);
                hmeas90 = results(:,end);
                clear results
                twoPolarizations = 0;
                

            case 'sg5'
                
                % ---- Q3 sine-Gaussians.
                %      columns: waveform  hrss@50%  hrss@90%
                results = [
                    70  1.91e-21  8.87e-21      
                   100  9.56e-22  4.57e-21      
                   153  6.45e-22  2.96e-21      
                   235  6.32e-22  2.87e-21      
                   361  9.08e-22  4.16e-21      
                   554  1.23e-21  5.74e-21      
                   849  1.91e-21  8.69e-21      
                   945  2.15e-21  9.84e-21      
                  1053  2.38e-21  1.19e-20      
                  1172  2.72e-21  1.29e-20      
                  1304  3.08e-21  1.41e-20      
                  1451  3.46e-21  1.65e-20      
                  1615  3.95e-21  1.79e-20      
                  1797  4.57e-21  2.10e-20      
                  2000  5.34e-21  2.47e-20 ];
                f = results(:,1);
                hmeas50 = results(:,end-1);
                hmeas90 = results(:,end);
                clear results
                twoPolarizations = 0;

            case 'sg6'
                
                % ---- Q100 sine-Gaussians.
                %      columns: waveform  hrss@50%  hrss@90%
                results = [
                      70  2.64e-21  1.17e-20    
                     100  9.97e-22  4.34e-21    
                     153  6.12e-22  2.72e-21    
                     235  5.81e-22  2.51e-21    
                     361  1.02e-21  4.39e-21    
                     554  1.06e-21  4.52e-21    
                     849  1.52e-21  6.92e-21    
                     945  1.80e-21  7.52e-21    
                    1053  2.08e-21  8.69e-21    
                    1172  2.28e-21  9.74e-21    
                    1304  2.43e-21  1.05e-20    
                    1451  2.75e-21  1.17e-20    
                    1615  2.96e-21  1.22e-20    
                    1797  3.56e-21  1.52e-20    
                    2000  3.79e-21  1.69e-20 ];
                f = results(:,1);
                hmeas50 = results(:,end-1);
                hmeas90 = results(:,end);
                clear results
                twoPolarizations = 0;
  
            otherwise

                error('Requested waveform info not available.')

        end

        % ---- Load run-averaged noise spectra.
        data = load('noise.txt');
        fdata = data(:,1); L1 = data(:,2); H1 = data(:,3); H2 = data(:,4);        
        % ---- Interpolate measured amplitude spectra to central
        %      frequencies of the injections.
        ASD = 10.^interp1(fdata,log10(H1),f);
        ASD = [ASD, 10.^interp1(fdata,log10(H2),f)];
        ASD = [ASD, 10.^interp1(fdata,log10(L1),f)];
        % clear fdata data H1 H2 L1
        clear data
        
        % ---- Threshold on rho_eff for event generation, from results
        %      page. KLUDGE: A small fraction of the observation time used
        %      a higher threshold for f<200 Hz; ignore that for the moment.
        %      (Easily incorporated later.)
        sqrt_rho_eff_min = 4.25*ones(size(f));
        index = find(f<200);
        sqrt_rho_eff_min(index) = 5.2;
        rho_eff_min = sqrt_rho_eff_min.^2;

    otherwise
        
        error('Requested network info not available.')

end


% % -------------------------------------------------------
% %    Compute optimally-oriented SNRs for hrss=1e-21.
% % -------------------------------------------------------
% 
% fs = 16384;
% T = 1/(fdata(2)-fdata(1));
% dF = fdata(2)-fdata(1);
% 
% % ---- Prepare storage for SNR and related info.
% SNRp = zeros(length(hmeas50),K);
% SNRc = zeros(length(hmeas50),K);
% hrssp = zeros(length(hmeas50),K);
% hrssc = zeros(length(hmeas50),K);
% fcent = zeros(length(hmeas50),K);
% 
% % ---- Loop over waveforms in the set.
% for ifreq = 1:length(f);
% 
%     switch lower(waveform)
% 
%         case 'hga2'
% 
%             % ---- Gaussians.
%             % ---- Make timeseries waveform with "unit" amplitude.
%             [t,h] = xmakewaveform('G',[1 tau(ifreq)],T,T/2,fs);
%             % ---- Compute hrss amplitude of "unit" waveform.
%             [junk, hrss0] = xoptimalsnr(h,0,fs);
%             % ---- Compute SNR, other properties of waveform rescaled to
%             %      hrss amplitude = 1e-21.  Do for each detector.
%             [SNR, h_rss, h_peak, Fchar, bw] = xoptimalsnr( ...
%                 1e-21/hrss0*h,0,fs,H1.^2,min(fdata),dF,min(fdata),max(fdata));
%             SNRp(ifreq,1) = SNR;
%             hrssp(ifreq,1) = h_rss;
%             fcent(ifreq,1) = Fchar;
%             [SNR, h_rss, h_peak, Fchar, bw] = xoptimalsnr( ...
%                 1e-21/hrss0*h,0,fs,H2.^2,min(fdata),dF,min(fdata),max(fdata));
%             SNRp(ifreq,2) = SNR;
%             hrssp(ifreq,2) = h_rss;
%             fcent(ifreq,2) = Fchar;
%             [SNR, h_rss, h_peak, Fchar, bw] = xoptimalsnr( ...
%                 1e-21/hrss0*h,0,fs,L1.^2,min(fdata),dF,min(fdata),max(fdata));
%             SNRp(ifreq,3) = SNR;
%             hrssp(ifreq,3) = h_rss;
%             fcent(ifreq,3) = Fchar;
% 
%         otherwise
% 
%             disp('warning: Broadband treatment of other waveforms not implemented yet.');
% 
%     end
% 
% end


% -------------------------------------------------------
%    Predicted coherent WaveBurst sensitivity
% -------------------------------------------------------

% ---- Range of hrss values to test.
% ---- Strain scale factors Igor used in WB runs.
hrss = 2.5e-21 * [ 0.03779, 0.053238, 0.0750, 0.106, 0.150, 0.211, ...
    0.298, 0.422, 0.596, 0.841, 1.19, 1.68, 2.37, 3.35, 4.73, 6.68, ...
    9.44, 13.3, 18.736, 26.395, 37.184, 52.383, 73.794 ].';
hpred50 = zeros(length(f),1);
hpred90 = zeros(length(f),1);
eff = zeros(length(hrss),length(f));

% ---- Loop over injection central frequencies.
for ifreq = 1:length(f);

    % ---- Loop over hrss values.
    for ih = 1:length(hrss)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Prepare storage.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % snr2_all = zeros(Nmc,length(hrss),length(f));
        % redEcor_all = zeros(Nmc,length(hrss),length(f));
        % cc_all = zeros(Nmc,length(hrss),length(f));
        % rho_eff_all = zeros(Nmc,length(hrss),length(f));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Generate random signal parameters, detector responses.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % ---- Sky positions and polarization angles for each injection.
        phi = 2*pi*rand(Nmc,1);
        ctheta = 2*rand(Nmc,1)-1;
        theta = acos(ctheta);
        psi = pi*rand(Nmc,1);

        % ---- Compute antenna responses in source polarization frame.
        [FpH FcH] = ComputeAntennaResponse(phi,theta,psi,'H');
        [FpL FcL] = ComputeAntennaResponse(phi,theta,psi,'L');

        % ---- Convert noise-weighted antenna responses to the dominant
        %      polarization frame. 
        [FpDP FcDP psiDP] = convertToDominantPolarizationFrame( ...
            [FpH/ASD(ifreq,1), FpH/ASD(ifreq,2), FpL/ASD(ifreq,3)], ...
            [FcH/ASD(ifreq,1), FcH/ASD(ifreq,2), FcL/ASD(ifreq,3)] );

        % ---- Compute unit vectors in DPF.
        epDP = FpDP .* repmat(sum(FpDP.^2,2).^(-0.5),[1,K]);
        ecDP = FcDP .* repmat(sum(FcDP.^2,2).^(-0.5),[1,K]);
        epsilon = sum(FcDP.^2,2)./sum(FpDP.^2,2);

        % ---- Compute hrss signal amplitudes in the source frame.
        if (twoPolarizations)
            % ---- All two-polarization waveforms had equal hrss values in
            %      the two polarizations. 
            hp_s = hrss(ih)/2.^0.5;
            hc_s = hp_s;
        else
            hp_s = hrss(ih);
            hc_s = 0;
        end
        % ---- Compute hrss signal amplitudes in the DPF.
        % ---- CHECK: Make sure we treat sign of psiDP correctly.
        hp_DP = cos(2*psiDP)*hp_s - sin(2*psiDP)*hc_s;
        hc_DP = sin(2*psiDP)*hp_s + cos(2*psiDP)*hc_s;

        % ---- Construct single-pixel data for each detector (i.e.,
        %      assuming signal fits into one wavelet pixel).
        % ---- Rescaling of noise.  Setting to 0 has little effect.
        noiseWeight = 0; 
        % ---- CHECK: Input h amplitudes are hrss, really should be using
        %      \tilde(h).  Check for possible scaling error.
        data = FpDP.*repmat(hp_DP,[1,K]) + FcDP.*repmat(hc_DP,[1,K]) ...
            + noiseWeight * randn(Nmc,K) ;

        % ---- Compute regularized "unit vector" in DPF.
        % ---- Total energy (incoherent statistic) -- needed for regulator.
        Etot = sum(data.^2,2);
        % ---- Regularization factor.  
        %      KLUDGE: We use Etot to estimate the total energy in the
        %      time-frequency pixel.  If the GW really spreads across
        %      multiple pixels, then the E used for regularization is
        %      smaller than Etot for the burst.  This increases the effect 
        %      of the regulator.
        delta = 0.01;
        gamma = 2;
        regFactor2 = sum(FcDP.^2,2) + (delta + gamma./Etot)*sum(ASD(ifreq,:).^(-1),2);
        ecDPreg = FcDP .* repmat(regFactor2.^(-0.5),[1,K]);

        % ---- Diagnostic: Compute sum-squared SNR in network due to the
        %      two polarizations.  Assume they are uncorrelated.
        % ---- Note: with convention used to define "data" variable (using
        %      hrss amplitude of the waveform, rather than strain
        %      amplitude), the sum-squared SNR of a matched filter will be:
        % snr2_all(:,ih,ifreq) = 2*sum(data.*conj(data),2);


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Construct likelihood operator (matrix).
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Estimate angle lambda between principle component frame and
        %      DPF using simulated data.  The regularized ecross is used.
        lambda = atan2(sum(ecDPreg.*data,2),sum(epDP.*data,2));

        % ---- Regularized likelihood operator components: outer products
        %      of eplus and (regularized) ecross vectors.  For each sky
        %      position these are KxK matrices.  Write as matrices with KxK
        %      columns, one row per sky position, for computational
        %      convenience.  
        epDP_epDPT = [ ...
            epDP(:,1).*epDP(:,1) epDP(:,1).*epDP(:,2)  epDP(:,1).*epDP(:,3) ...
            epDP(:,2).*epDP(:,1) epDP(:,2).*epDP(:,2)  epDP(:,2).*epDP(:,3) ...
            epDP(:,3).*epDP(:,1) epDP(:,3).*epDP(:,2)  epDP(:,3).*epDP(:,3) ]; 
        epDP_ecDPregT = [ ...
            epDP(:,1).*ecDPreg(:,1) epDP(:,1).*ecDPreg(:,2)  epDP(:,1).*ecDPreg(:,3) ...
            epDP(:,2).*ecDPreg(:,1) epDP(:,2).*ecDPreg(:,2)  epDP(:,2).*ecDPreg(:,3) ...
            epDP(:,3).*ecDPreg(:,1) epDP(:,3).*ecDPreg(:,2)  epDP(:,3).*ecDPreg(:,3) ]; 
        ecDPreg_epDPT = [ ...
            ecDPreg(:,1).*epDP(:,1) ecDPreg(:,1).*epDP(:,2)  ecDPreg(:,1).*epDP(:,3) ...
            ecDPreg(:,2).*epDP(:,1) ecDPreg(:,2).*epDP(:,2)  ecDPreg(:,2).*epDP(:,3) ...
            ecDPreg(:,3).*epDP(:,1) ecDPreg(:,3).*epDP(:,2)  ecDPreg(:,3).*epDP(:,3) ]; 
        ecDPreg_ecDPregT = [ ...
            ecDPreg(:,1).*ecDPreg(:,1) ecDPreg(:,1).*ecDPreg(:,2)  ecDPreg(:,1).*ecDPreg(:,3) ...
            ecDPreg(:,2).*ecDPreg(:,1) ecDPreg(:,2).*ecDPreg(:,2)  ecDPreg(:,2).*ecDPreg(:,3) ...
            ecDPreg(:,3).*ecDPreg(:,1) ecDPreg(:,3).*ecDPreg(:,2)  ecDPreg(:,3).*ecDPreg(:,3) ]; 

        % ---- Likelihood operator in principle component frame.
        Lnorm = sum(epDP.^2,2) .* cos(lambda).^2 ...
            + sum(ecDPreg.^2,2) .* sin(lambda).^2 ...
            + 2 * sum(epDP.*ecDPreg,2) .* sin(lambda) .* cos(lambda);
        lambda = repmat(lambda,[1,K^2]);
        Loper = epDP_epDPT .* cos(lambda).^2 ...
            + ecDPreg_ecDPregT .* sin(lambda).^2 ...
            + (epDP_ecDPregT + ecDPreg_epDPT) .* sin(lambda) .* cos(lambda);
        Loper = Loper ./ repmat(Lnorm,[1,K^2]);
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Compute likelihood and related statistics.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % ---- Prepare storage.
        Lm = zeros(Nmc,1);   %-- "total likelihood statistic"
        Einc = zeros(Nmc,1); %-- incoherent energy (diagonal terms)
        Ecor = zeros(Nmc,1); %-- correlated energy (off-diagonal terms)
        for ii = 1:K
            for jj = 1:K
                Lm = Lm + data(:,ii).*Loper(:,K*(ii-1)+jj).*data(:,jj);
            end
            Einc = Einc + data(:,ii).*Loper(:,K*(ii-1)+ii).*data(:,ii);
        end
        Ecor = Lm - Einc;
        % ---- Null energy.
        Enull = Etot - Lm; 
        % ---- KLUDGE: Not including penalty factor or any constraint that
        %      Enull >= 0.  In fact, for weak injections find negative
        %      Enull values occur.  
        cc = Ecor ./ (Enull + abs(Ecor));
        % ---- ASIDE: Record cc values for 150 Hz injections (SG1 set).
        if (ifreq==3)
            cc150{ih} = cc;
        end
        %
        % ---- Compute reduced correlated energy.
        redEcor = zeros(Nmc,1);
        for ii = 1:K-1
            for jj = ii+1:K
                numer = data(:,ii).*Loper(:,K*(ii-1)+jj).*data(:,jj);
                denom = (data(:,ii).*Loper(:,K*(ii-1)+ii).*data(:,ii) .* ...
                         data(:,jj).*Loper(:,K*(jj-1)+jj).*data(:,jj)).^0.5 ; 
                redEcor = redEcor + 2 * numer .* abs(numer) ./ denom ;
            end
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Compute effective correlated SNR per detector.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % % ---- Effective correlated SNR statistic.  Missing factor 1/2?
        % rho_eff = cc.^2 .* Ecor / K;
        
        % ---- Effective correlated SNR statistic based on reduced
        %      correlated energy.
        rho_eff = cc .* redEcor / K;
        % ---- Manually "kill" any event with cc < 0.5 (includes all cases
        %      where Ecor < 0). 
        k = find(cc<0.5);
        rho_eff(k) = 0;

        % % ---- Record main results.
        % redEcor_all(:,ih,ifreq) = redEcor;
        % cc_all(:,ih,ifreq) = cc;
        % rho_eff_all(:,ih,ifreq) = rho_eff;

        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Efficiency.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Detection efficiency.
        eff(ih,ifreq) = sum(rho_eff>=rho_eff_min(ifreq))/length(rho_eff);

        % % ---- KLUDGE
        % if (ih==3 && ifreq==3)
        %     save workspace.mat
        % end

    end

    % ---- Interpolate for 50% and 90% efficiency.  Include the calFactor
    %      here, but not in the efficiency curve..
    hpred50(ifreq) = calFactor * robustloginterp(eff(:,ifreq),hrss,0.5);
    hpred90(ifreq) = calFactor * robustloginterp(eff(:,ifreq),hrss,0.9);

end

% ---- Zero out any points where interpolation fails (gives -1).
kk = find(hpred50==-1);
hpred50(kk) = 0;
kk = find(hpred90==-1);
hpred90(kk) = 0;

% ---- Clean up.
% clear FL FH FG rho_eff cc ctheta psi phi theta snr2 ih ifreq 

disp(' ');
disp(['Comparison of measured and predicted hrss values for cWB']);
disp(['    MDC set: ' waveform]);
disp(' ');
disp('                                 hmeas50                       hmeas90 ');
disp('      freq   hmeas50   hpred50   -------   hmeas90   hpred90   ------- ');
disp('     (kHz)   (1e-21)   (1e-21)   hpred50   (1e-21)   (1e-21)   hpred90 ');
disp('    ------    ------   -------   -------   -------   -------   ------- ');
disp([f*1e-3 hmeas50*1e21 hpred50*1e21 hmeas50./hpred50 hmeas90*1e21 hpred90*1e21 hmeas90./hpred90])
%
% ---- Use only non-failed results to compute mean agreement.
kk = find(hpred50);
mean50 = 1/100*round(mean(hmeas50(kk)./hpred50(kk))*100);
std50 = 1/100*round(std(hmeas50(kk)./hpred50(kk))*100);
kk = find(hpred90);
mean90 = 1/100*round(mean(hmeas90(kk)./hpred90(kk))*100);
std90 = 1/100*round(std(hmeas90(kk)./hpred90(kk))*100);
disp(['                            mean: ' num2str(mean50) ' +/- ' num2str(std50) ... 
      '                 ' num2str(mean90) ' +/- ' num2str(std90) ]);

% ---- Make plot.
figure; set(gca,'fontsize',16)
loglog(fdata,[H1 H2 L1],'linewidth',2);
grid on ; hold on;
loglog(f,hmeas50,'b+');
loglog(f,hmeas90,'r+');
loglog(f,hpred50,'bo');
loglog(f,hpred90,'ro');
loglog(f,ASD,'k.');
axis([1e1 3e3 1e-23 1e-19])
xlabel('frequency (Hz)')
ylabel('amplitude (Hz^{-1/2})')
legend('H1 noise','H2 noise','L1 noise','hrss50 - meas','hrss90 - meas','hrss50 - pred','hrss90 - pred','Location','EastOutside')
title(['Pred and Meas hrss values for cWB:' waveform])

