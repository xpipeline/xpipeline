% Unit test for checking that xdecimatecluster is working as advertised 

% generate clusters, the 7 first columns describe the cluster and are not
% used for decimation. The 8th column will contain the "likelihood" and
% clusters will be decimated according to that ranking.
nClusters = 1000;
clusterArray=rand(nClusters,8);


clusterProportion = 0.1;

% Test 1 - proportion in between bounds
lowerLimit = floor(nClusters*clusterProportion*0.5);
upperLimit = ceil(nClusters*clusterProportion*1.5);
[decimatedClusters decimationMask] = ...
    xdecimatecluster(clusterArray,clusterProportion,...
                     lowerLimit,upperLimit,{'loghbayesiancirc'});
if sum(decimationMask) ~= size(decimatedClusters,1) || ...
  size(decimatedClusters,1) ~= 100
  error('Test 1 failed')
else
  disp('Test 1 passed')
end

% Test 2 - proportion below bounds
lowerLimit = floor(nClusters*clusterProportion*1.5);
upperLimit = ceil(nClusters*clusterProportion*2.5);
[decimatedClusters decimationMask] = ...
    xdecimatecluster(clusterArray,clusterProportion,...
                     lowerLimit,upperLimit,{'loghbayesiancirc'});
if sum(decimationMask) ~= size(decimatedClusters,1) || ...
  size(decimatedClusters,1) ~= 150
  error('Test 2 failed')
else
  disp('Test 2 passed')
end

% Test 3 - proportion above bounds
lowerLimit = floor(nClusters*clusterProportion*0.25);
upperLimit = ceil(nClusters*clusterProportion*0.5);
[decimatedClusters decimationMask] = ...
    xdecimatecluster(clusterArray,clusterProportion,...
                     lowerLimit,upperLimit,{'loghbayesiancirc'});
if sum(decimationMask) ~= size(decimatedClusters,1) || ...
  size(decimatedClusters,1) ~= 50
  error('Test 3 failed')
else
  disp('Test 3 passed')
end

% Test 2 - proportion below bounds, equal bounds
lowerLimit = floor(nClusters*clusterProportion*1.5);
upperLimit = ceil(nClusters*clusterProportion*1.5);
[decimatedClusters decimationMask] = ...
    xdecimatecluster(clusterArray,clusterProportion,...
                     lowerLimit,upperLimit,{'loghbayesiancirc'});
if sum(decimationMask) ~= size(decimatedClusters,1) || ...
  size(decimatedClusters,1) ~= 150
  error('Test 4 failed')
else
  disp('Test 4 passed')
end

% Test 5 - proportion above bounds, equal bounds
lowerLimit = floor(nClusters*clusterProportion*0.5);
upperLimit = ceil(nClusters*clusterProportion*0.5);
[decimatedClusters decimationMask] = ...
    xdecimatecluster(clusterArray,clusterProportion,...
                     lowerLimit,upperLimit,{'loghbayesiancirc'});
if sum(decimationMask) ~= size(decimatedClusters,1) || ...
  size(decimatedClusters,1) ~= 50
  error('Test 5 failed')
else
  disp('Test 5 passed')
end

% Test 6 - ask for more triggers than available
lowerLimit = 2000; 
upperLimit = 2000;
[decimatedClusters decimationMask] = ...
    xdecimatecluster(clusterArray,clusterProportion,...
                     lowerLimit,upperLimit,{'loghbayesiancirc'});
if sum(decimationMask) ~= size(decimatedClusters,1) || ...
  size(decimatedClusters,1) ~= 1000
  error('Test 6 failed')
else
  disp('Test 6 passed')
end

