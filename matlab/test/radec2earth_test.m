% test unitarity of earth fixed <-> radec conversions

function radec2earth_test

N = 1e4;
GPS = 9e9+2e9*rand(N,1);
phi = rand(N,1)*2*pi;
theta = rand(N,1)*pi;

[ra dec] = earthtoradec(phi,theta,GPS);
[newPhi newTheta] = radectoearth(ra, dec, GPS);

if any(abs(wrapCircle(phi-newPhi)) + abs(wrapHalfCircle(theta-newTheta)) > 1e-5)
  disp('Unity test failed')
end

disp(['Max deviation is: ' ...
      num2str(max(abs(wrapCircle(phi-newPhi)) + abs(wrapHalfCircle(theta-newTheta))))])


ra = rand(N,1)*360;
dec = rand(N,1)*180-90;

[phi theta] = radectoearth(ra, dec, GPS);
[newRA newDec] = earthtoradec(phi,theta,GPS);

newRA = newRA/180*pi;
ra = ra/180*pi;
newDec = newDec/180*pi;
dec = dec/180*pi;

if any(abs(wrapCircle(ra-newRA)) + abs(wrapHalfCircle(dec-newDec)) > 1e-5)
  disp('Unity test failed')
end

disp(['Max deviation is: ' ...
      num2str(max(abs(wrapCircle(ra-newRA)) + abs(wrapHalfCircle(dec-newDec))))])


function y = wrapCircle(x)
y = min(mod(x,2*pi),mod(2*pi-x,2*pi));

function y = wrapHalfCircle(x)
y = min(mod(x,pi),mod(pi-x,pi));
