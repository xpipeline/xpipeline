% MDC_to_X: Script to convert the BurstMDC injection logs for the ccSN
% search to the format used by X-Pipeline's injection engine. 
%
% For info on the MDCs please see https://wiki.ligo.org/Bursts/SNMDCInfoPage .
%
% Note that as of June 2013, several different versions of the frames have
% been made, and these are distributed somewhat irregularly across a set of
% directories.  For ease of processing, this script accesses the burst MDC
% frames and logs via a set of symbolic links that follow a regular pattern
% of directory names.
%
% usage: specify the variable SN (one of 'SN2007gr', 'SN2008ax',
% 'SN2008bk', or 'SN2011dh') and run the script.
% Note that you need to have ligotools enabled to use the frgetvect function.
%
% $Id$
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Set path for CIT.
addpath ~/matapps/packages/simulation/BurstMDC/trunk
addpath ~/matapps/releases/utilities/misc/src/
addpath ~/xpipeline/branches/linear/searches/grb
addpath ~/xpipeline/branches/linear/searches/sn
addpath ~/xpipeline/branches/linear/utilities/
addpath ~/xpipeline/branches/linear/share/

% ---- Location of X-Pipeline waveform catalog file.
catDir = '/home/psutton/xpipeline/branches/linear/utilities/waveforms/';


% -------------------------------------------------------------------------
% Specify MDC set(s) to be translated.
% -------------------------------------------------------------------------

switch SN

    case 'SN2007gr'

        % ---- SN2007gr.
        baseName = '/home/psutton/xpipeline/branches/linear/test/SNMDCs/SN2007gr/';
        % frameSetAll = {'mueller5','numerical3','piro5','rotbar5','SGel2','SGlin2'};
        frameSetAll = {'piro5','rotbar5'};
        % ---- Note: The last two frames in these sets have duration 844 sec. We
        %      skip these for simplicity of scripting.  Also, mueller4 set does
        %      not have frames before 8709XXXXXX; the try statement in frame
        %      reading should save us from trouble when attemting to read those
        %      non-existent frames. 
        frameStart = 870768590; 
        frameEnd   = 871212590;
        frameDur   = 1000;

    case 'SN2008ax';
        
        % ---- SN2008ax.
        baseName = '/home/psutton/xpipeline/branches/linear/test/SNMDCs/SN2008ax/';
        % ---- Note: The last two frames in these sets have duration 522 sec. We
        %      skip these for simplicity of scripting.
        % frameSetAll = {'mueller4','numerical3','piro5','rotbar5','SGel2','SGlin2'};
        % frameStart = 888545390;
        % frameEnd   = 888574390;
        % ---- Note: The last two frames in these sets have duration 932 sec. We
        %      skip these for simplicity of scripting.
        frameSetAll = {'mueller_long','numerical_long','piro_long','rotbar_long','SGel_long','SGlin_long'};
        frameStart = 888467630;
        frameEnd   = 888573630;
        frameDur   = 1000;

    case 'SN2008bk';

        % ---- SN2008bk.
        baseName = '/home/psutton/xpipeline/branches/linear/test/SNMDCs/SN2008bk/';
        % frameSetAll = {'mueller5','numerical5','piro5','rotbar5','SGel2','SGlin2'};
        % frameStart = 889531214;
        % frameEnd   = 890394214;
        %frameSetAll = {'mueller_long','numerical_long','piro_long','rotbar_long','SGel_long','SGlin_long'};
        frameSetAll = {'numerical_long','piro_long','rotbar_long','SGel_long','SGlin_long'};
        frameStart = 889444814;
        frameEnd   = 890447814;
        frameDur   = 1000;

    case 'SN2011dh';

        % ---- SN2011dh.
        baseName = '/home/psutton/xpipeline/branches/linear/test/SNMDCs/SN2011dh/';
        % frameSetAll = {'mueller4','numerical3','piro5','rotbar5','SGel2','SGlin2'};
        % frameStart = 990863175;
        % frameEnd   = 990907175;
        % frameSetAll = {'mueller_long','numerical_long','piro_long','rotbar_long','SGel_long','SGlin_long'};
        % frameSetAll = {'mueller_long','piro_long','rotbar_long','SGel_long','SGlin_long'};
        frameSetAll = {'numerical_long'};
        frameStart = 990780783;
        frameEnd   = 990909783;
        frameDur   = 1000;

end

for iSet = 1:length(frameSetAll)
    
    frameSet = frameSetAll{iSet}
    
    % ---- Log file to be translated, and output file name.
    frameTime = frameStart:frameDur:frameEnd ;
    for iframe = 1:length(frameTime)
    timeString = num2str(frameTime(iframe));
    frameName{iframe} = [baseName SN '_' frameSet '/GHLV-' SN '_' frameSet ...
      '-' timeString(1:4) '/GHLV-' SN '_' frameSet '-' num2str(frameTime(iframe)) ...
      '-' num2str(frameDur) '.gwf'];
    end
    mdcLogFileName = [baseName SN '_' frameSet '/BurstMDC-' SN '_' frameSet '-Log.txt'];
    xLogFileName   = ['X-' SN '-' frameSet '-Log.txt']; 
    compFileName   = ['X-' SN '-' frameSet '-Log.mat']; 


    % -------------------------------------------------------------------------
    % Translate MDC sets.
    % -------------------------------------------------------------------------

    disp(['Processing ' mdcLogFileName]);

    % ---- Convert entire log file.
    startTime = 0;
    blockTime = Inf;

    % ---- Extract injection parameters from BurstMDC log.
    [injectionGPS_s, injectionGPS_ns, injectionPhi, injectionTheta, ...
        injectionPsi, injectionType, injectionParam] ...
        = getsnmdcinjectionparams(startTime, blockTime, mdcLogFileName);

    % ---- Record total number of injections.
    nInj = length(injectionGPS_s);

    % ---- Pack together in format expected by xmakegwbinjectionfile.
    for ii=1:nInj
        signal{ii,1} = injectionType{ii};
        signal{ii,2} = injectionParam{ii};  
    end

    % ---- MDC convention for peak time is 0.5 samples later than X convention.
    %      Since most channels are sampled at 16384 Hz, use that sample rate to
    %      correct peak time.
    peakTime = injectionGPS_s + 1e-9*injectionGPS_ns - 0.5/16384;

    % ---- Write X-formatted log file.
    xmakegwbinjectionfile(xLogFileName,peakTime,0,[injectionTheta,injectionPhi], ...
        injectionPsi,signal,'injectionMode','loop');


    % -------------------------------------------------------------------------
    % Compare MDC frame data to X on-the-fly simulation data.
    % -------------------------------------------------------------------------

    % ---- Prepare storage.
    maxcc   = -1*ones(nInj,1);
    ind     =     nan(nInj,1);
    maxferr = -1*ones(nInj,1);
    maxferrind =  nan(nInj,1);

    % ---- Specify test channel.
    % detectors   = {'H1'};
    % channelName = 'H1:GW-H';
    detectors   = {'G1'};
    channelName = 'G1:GW-H';
    fs = 16384;

    % ---- Loop over frames.
    % disp('kludge: testing last frame only');
    % for iframe = length(frameTime):length(frameTime)  %-- test last frame only
    for iframe = 1:length(frameTime)

        try

            % ---- Read injection from MDC frame.
            allMDCData = frgetvect(frameName{iframe},channelName,frameTime(iframe),frameDur,0);
            % ---- Find all injections with peak time in this frame.
            %      KLUDGE: might miss an injection in last ~second of frame.
            allInj = find(injectionGPS_s>frameTime(iframe) & injectionGPS_s<(frameTime(iframe)+frameDur-1));

            % ---- Loop over injections.
            for iInj = 1:length(allInj)

                % ---- Keep +/-4 sec of MDC data around each injection.
                injidx = allInj(iInj)
                start =  max(floor(peakTime(injidx)) - frameTime(iframe) - 4 , 0);
                dur = 8; 
                %      KLUDGE: might run off end of data set ...
                sampidx = start*fs + [1:dur*fs]';
                dataMDC = allMDCData(sampidx);

                % ---- Make injection data with X-Pipeline.
                dataX = xinjectsignal(frameTime(iframe)+start,dur,detectors,fs,xLogFileName, ...
                0,injidx,'catalogDirectory',catDir);
                dataX = cell2mat(dataX);

                % ---- Compute maximum overlap, allowing for time offset.
                % disp(['For first injection:']);
                %
                cc = innerproduct(dataMDC/norm(dataMDC),dataX/norm(dataX),fs);
                [maxcc(injidx),ind(injidx)] = max(abs(cc));
                % disp(['Maximum cross-correlation: ' num2str(maxcc)]);
                % disp(['  at time lag of ' num2str(ind-1) ' samples.']);
                %
                % maxferr(injidx) = max(abs(dataX-dataMDC))/max(abs(dataMDC));
                % disp(['Max fractional error at zero lag: ' num2str(maxferr)]);
                null = 1 + norm(dataMDC)^2/norm(dataX)^2 ...
                - 2 * innerproduct(dataX,dataMDC,fs)/norm(dataX)^2;
                [maxferr(injidx),maxferrind(injidx)] = min(null);
                if maxferr(injidx) > 1e-4
                    % ---- Dump data for debugging.
                    dumpname = ['datadump_' frameSet '_' num2str(injidx) '.mat'];
                    save(dumpname,'dataMDC','dataX');
                end
            end

        catch

            disp(['Failed to read frame ' frameName{iframe} ]);

        end

    end

    % ---- Save matlab file containing comparison summary data.
    clear allMDCData sampidx
    save(compFileName)

end
