
% ---- Make a single-cycle sine wave in 1 sec of data.
fs = 16384;
T = 1;
f0 = 200;
h0 = 1e-21;
h = zeros(T/2*fs,1);
t = [0:1/fs:1/f0]';
h = [h; h0*sin(2*pi*f0*t)];
h(T*fs,1) = 0;
t = [0:T*fs-1]'/fs;

% ---- Plot waveform.
figure; set(gca,'fontsize',16)
plot(t,h,'-+','linewidth',2);
grid on; hold on
xlabel('time (sec)')
ylabel('strain')
axis([0.49 0.51 get(gca,'ylim')])

% ---- Compute aLIGO design curve at positive frequencies only.
df = 1/T;
f = [df:df:fs/2]';
warning off
S = SRD('aligo-zdhp',f);
warning on
% ---- Noise spectrum at waveform central frequency.
S0 = interp1(f,S,f0);

% ---- hrss and SNR are easy to estimate analytically:
hrss = h0 * 1/(2*f0)^0.5
% snr = (2*hrss^2 / S0)^0.5 = h0 (f0*S0)^(-0.5);
snr = h0 * (f0*S0)^(-0.5);
disp(['Analytic estimates:']); 
disp(['    hrss = ' num2str(hrss)]); 
disp(['    snr  = ' num2str(snr)]); 


% ---- Compute SNR against LIGO design.
[snrX, hrssX, h_peak, Fchar, bw, Tchar, dur] = xoptimalsnr(h,0,fs,S,f(1),df);
disp(['X-Pipeline estimates:']); 
disp(['    hrss = ' num2str(hrssX)]); 
disp(['    snr  = ' num2str(snrX)]); 

