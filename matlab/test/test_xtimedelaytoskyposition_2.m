

numMC = 1000;

ra = 360 * rand(numMC,1);   
dec = 180 * rand(numMC,1) - 90;
gps = 800000000 + 200000000 * rand(numMC,1);

ra = 253.217
dec = -25
gps = 874780000.0

sites = {'H','L','V'};

% ---- Convert to earth-based coords.
[phi, theta] = radectoearth(ra,dec,gps);

% ---- skyPositions.
% The four columns have the following meanings:
%   theta   Polar angle in the range [0, pi] with 0 at the North Pole/ z axis.
%   phi     Azimuthal angle in the range [-pi, pi) with 0 at Greenwich / x axis.
%   pOmega  A priori probability associated with each sky position.
%   dOmega  Solid angle associated with each sky position.

skyPositions = [theta, phi, zeros(length(theta),1), zeros(length(theta),1)];

deltaT = computeTimeShifts( sites, skyPositions );

delay_12 = deltaT(:,2) - deltaT(:,1)
delay_13 = deltaT(:,3) - deltaT(:,1)

[theta_zp,phi_zp,theta_zm,phi_zm] = xtimedelaytoskyposition([delay_12,delay_13],sites)

[ra_zp, dec_zp] = earthtoradec(phi_zp,theta_zp,gps);
[ra_zm, dec_zm] = earthtoradec(phi_zm,theta_zm,gps);
   
fac = 1e7;
pass_zp = abs(ra - ra_zp) < fac*eps & abs(dec - dec_zp) < fac*eps;
pass_zm = abs(ra - ra_zm) < fac*eps & abs(dec - dec_zm) < fac*eps;

ra_error  = pass_zp .* abs(ra - ra_zp)   + pass_zm .* abs(ra - ra_zm);
dec_error = pass_zp .* abs(dec - dec_zp) + pass_zm .* abs(dec - dec_zm);

max(ra_error)
max(dec_error)
