disp('Test of equivalence between xmakeskygrid with gridtype = ''line'' and xchooseskylocations1.')
disp(' ')
oldDir = '/home/patrick.sutton/xpipeline/branches/python3/matlab/utilities/'
newDir = '/home/patrick.sutton/xpipeline/branches/o3b-python3/matlab/utilities/'
disp(' ')
disp('Common parameters for all tests:')
nSigma = '2'
network = 'H~L~V'
delayTol = '5e-4'
outputFile = 'None'
gridtype = 'line'
verbose = '0'
Ntest = 1000;
disp(['Simulating ' num2str(Ntest) ' sky error circles with random properties ...']);

fail = 0;
pass = 0;
tolerance = 1e-12;
for ii=1:Ntest

    % ---- Generate random error circle.
    ra  = num2str(rand(1)*360);
    dec = num2str((pi/2-acos(2*rand(1)-1))*180/pi);
    gps = num2str(1234500000 + round(rand(1)*86400));
    sigma_deg = num2str(rand(1)*10);

    % ---- Original xchooseskylocations1 code.
    startDir = cd(oldDir); 
    [old.ra,old.dec,old.prob] = xchooseskylocations1(ra,dec,gps,...
        sigma_deg,nSigma,network,delayTol,outputFile,verbose);

    % ---- New xmakeskygrid code. Turn off warning in code about bug in
    %      calculation of probabilities using algorithm in
    %      xchooseskylocations1.
    cd(newDir); 
    warning off
    [new.ra,new.dec,new.prob] = xmakeskygrid(ra,dec,gps, ...
        sigma_deg,nSigma,network,delayTol,outputFile,gridtype,verbose);
    warning on
    
    % ---- Check for discrepancies in grid positions.
    if ( norm(old.ra-new.ra)>tolerance || norm(old.dec-new.dec)>tolerance )
        fail = fail + 1;
    else
        pass = pass + 1;
    end

end

disp('... finished.') 
disp(['sky grid from to two codes are equivalent to a tolerance of ' ...
    num2str(tolerance) ' in ' num2str(pass) ' of ' num2str(Ntest) ' cases.'])
disp(['Failure rate: ' num2str(fail/Ntest)])

