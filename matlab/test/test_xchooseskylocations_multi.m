function [efficiency,numSearch,ra_ctr_deg,dec_ctr_deg,gps] = ...
   xmultitestchooseskylocations(skyPosError_deg,numSigma,sites,delay_tol_sec,numTest,numSky)

% XMULTITESTCHOOSESKYLOCATIONS - Function to test performance of xchooseskylocations.
% Runs xtestchooseskylocations many times.
%
% usage:
% [efficiency,numSearch,ra_ctr_deg,dec_ctr_deg,gps] = ...
% xmultitestchooseskylocations(skyPosError_deg,numSigma,sites,delay_tol_sec,numTest,numSky); 
%
%  skyPosError_deg   Double. 1-sigma error in GRB sky pos. This will be 
%                    multiplied by numSigma (see below) to define a radius 
%                    around the central point within which we will search for 
%                    GRBs.
%  numSigma          Integer. Determines sky area about central GRB position we
%                    will try and cover (see skyPosError_deg). 
%  sites             String. Tilde-separated list of detector sites, 
%                    e.g., 'H~L~V'.
%                    2 or 3 sites must be listed.
%  delay_tol_sec     Double. Maximum error in time delay we will allow.
%  numTest           Integer. Number of fake sky positions to generate when
%                    testing the search grid.
%  numSky            Number of fake GRB sky positions to generate search grids
%                    for.
%
%  efficiency        Double. Fraction of test sky positions that were captured
%                    using our search grid.
%  numSearch         Integer. Number of sky positions in our search grid.
%  ra_ctr_deg        Vector. List of sky coords for fake GRB sky positions we have
%                    generated search grids for.
%  dec_ctr_deg       Vector. List of sky coords for fake GRB sky positions we have
%                    generated search grids for.
%  gps               Vector. List GPS times for fake GRBs we have generated search 
%                    grids for.
%
% [efficiency,numSearch,ra_ctr_deg,dec_ctr_deg,gps] = ...
%    xmultitestchooseskylocations(5,3,'H~L~V',5e-4,1000,10)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input args
error(nargchk(6, 6, nargin));

rand('seed',54321);

ra_ctr_deg =  360 * rand(numSky,1);
dec_ctr_deg = acosd(2*rand(numSky,1)-1)-90;
%dec_ctr_deg = 180 * rand(numSky,1) - 90;
gps         = 800000000 + 100000000 * rand(numSky,1);

verbose = 0;

for iSky = 1:numSky
   fprintf(1,'Skp pos %d of %d \n',iSky,numSky);
   [efficiency(iSky),numSearch(iSky)] = xtestchooseskylocations2(...
      ra_ctr_deg(iSky),dec_ctr_deg(iSky),gps(iSky),...
      skyPosError_deg,numSigma,sites,delay_tol_sec,numTest,verbose);
end


min(efficiency)
