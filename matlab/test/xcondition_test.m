% XCONDITION_TEST: Script to test XCONDITION.
%
% This script tests XCONDITION by applying it to sample data.  It then
% compares the power spectrum reported by XCONDITION to that estimated by
% PWELCH, and to the design spectrum (if known).  It also check to see if
% the conditioned data is actually white with the appropriate
% normalization.
%
% Instructions: Specify the variables fmin, fmax, analysisTimes, IFO, and
% optionally fs and file, as below.  Then run the script.  The output
% consists entirely of plots.
%
% This script calls the helper function TESTPSD.
%
% Patrick.Sutton at astro.cf.ac.uk
%
% $Id$

% ---- Analysis band.
fmin = 48;
fmax = 2000;

% ---- Sampling rate
fs = 16384;
% ---- Data duration (for simulated data).
T = 32;

% ---- FFT durations to test.
% analysisTimes = [1/4, 1/32];
analysisTimes = [1/4];

% ---- Specify data source.  Uncomment ONE of the following.
% ---- File containing real data to be processed.
% file = 'xcondition/test_data/H1_822627330-256.dat'; fs = 16384; IFO = 'LIGO';
% file = 'xcondition/test_data/H2_822627330-256.dat'; fs = 16384; IFO = 'LIGO';
% file = 'xcondition/test_data/L1_822627330-256.dat'; fs = 16384; IFO = 'LIGO';
% file = 'V_HrecV3_931186077-256.mat'; fs = 4096; IFO = 'Virgo';
file = 'L_L1_LDAS_C02_L2_931186077-256.mat'; fs = 16384; IFO = 'LIGO';
% ---- Design noise curve to simulate.  Must be recognized by the function SRD.
% IFO = ['white' num2str(fs)];
% IFO = 'LIGO';
% IFO = 'VIRGO';

% lineAmpl = [1e-21 1e-21 1e-21 1e-21];
% % lineFreq = [101.347483 150.84736 249.9736262 348.475657];
% lineFreq = [401.347483 500.84736 599.9736262 699.736362];
% phaseJitter = [1 0 0 0];   %-- cumulative number of cycles offset (fixed).
% amplJitter = [0 0 0.1 0];  %-- fractional point-by-point jitter.
% Tstep = [Inf Inf Inf 0.5];   %-- zero-out line after this time (in units of duration of data stream).

tag = 'test5-L1-real';
savePlots = 1;


% -------------------------------------------------------------------------
%    Preliminaries.
% -------------------------------------------------------------------------

lineStyle = {'b-','g-','r-','m-'};


% -------------------------------------------------------------------------
%    Get data and condition it.
% -------------------------------------------------------------------------

if exist('file')==1 && exist('IFO')==1 && exist('fs')==1
    % ---- Load data.
    % n = load(file);
    load(file,'data');
    n = data;
    clear data
    T = length(n)/fs; 
    dataType = 'real';
elseif exist('IFO')==1
    % ---- Make simulated noise across a band slightly larger than target
    %      analysis band.
    warning off;
    n = SimulatedDetectorNoise(IFO,T,fs,fmin-16,fmax+16);
    warning on;
    dataType = 'simulated';
else
    error('Must specify at least one of file, IFO.');
end

% ---- Add a line with specified amplitude & phase jitter (to broaden it).
if exist('lineAmpl')==1 && ~isempty(lineAmpl)
    t = [0:length(n)-1]' / length(n) * T;
    h0 = figure; set(gca,'fontsize',16);
    for iline = 1:length(lineAmpl)
        phase = 2*pi*t*lineFreq(iline);
        phasenoise = randn(size(t));
        phasenoise = cumsum(phasenoise)/sum(phasenoise)*2*pi*phaseJitter(iline);
        ampl = lineAmpl(iline)*(1+randn(size(t))*amplJitter(iline)); 
        ind = find(t>Tstep(iline)*T);
        if ~isempty(ind)
            ampl(ind) = 0;
        end
        line = ampl.*sin(phase+phasenoise); %-- 150 Hz
        n = n + line;
        plot(t(1:fs/16),line(1:fs/16),lineStyle{iline});
        hold on
    end
    grid on
end
clear t phase phasenoise line iline

% ---- Call xcondition.  The following xcondition parameters have the
%      values used in GRB searches.  Don't change these if you want to test
%      the GRB analysis. 
whiteningTime = 1; 
verboseFlag = 0; 
trainingMode = 'search';
[cdata, aS, analysisLength, transientLength, normalizationFactor] = ...
    xcondition(n, fs, fmin, analysisTimes, whiteningTime, fs, verboseFlag, trainingMode);


% -------------------------------------------------------------------------
%    Verify xcondition PSD estimation.
% -------------------------------------------------------------------------

for ii = 1:length(analysisTimes)

    % ---- Compare xcondition PSD to pwelch, design.  Note that for LIGO &
    %      Virgo design noises, the Welch PSD tends to be very badly
    %      corrupted by leakage at low frequencies.  
    [h1, h2, ratioMean, ratioStd] = testpsd(aS{ii}.^2,n,fs,IFO,fmin,fmax);
    figure(h1)
    title(['analysis time = ' num2str(analysisTimes(ii)) ' s'])
    figure(h2)
    title(['analysis time = ' num2str(analysisTimes(ii)) ' s'])
    if isempty(strfind(lower(IFO),'white'))
        figure(h1); axis([10 1e4 1e-23 1e-20])
        figure(h2); axis([10 1e4 0 2])
    end
    
end


% -------------------------------------------------------------------------
%    Verify xcondition output timeseries.
% -------------------------------------------------------------------------

% ---- Rescale conditioned data to unity variance (ignoring loss of
%      bandwidth; see XCONDITION).   This is for ease of comparison
%      to the expected white spectrum.
%      PSD for comparison to the expected white spectrum.
cdata = cdata*(fs*analysisTimes(1))^(0.5);
% ---- Chop transient portions.
cdata = cdata(transientLength+1:end-transientLength);
% ---- Cut down to length = power of 2 (required by xcondition).
cdata = cdata(1:2^(nextpow2(length(cdata))-1));

% ---- Use xcondition to estimate the PSD of the conditioned data.
[junk, aSc] = xcondition(cdata, fs, fmin, analysisTimes, ...
    whiteningTime, fs, verboseFlag, trainingMode); 

for ii = 1:length(analysisTimes)

    % ---- Test xcondition-estimated PSD of the conditioned data.
    [h3, h4, ratioMean, ratioStd, Pw, Fw] = testpsd(aSc{ii}.^2,cdata,fs,['white' num2str(fs)],fmin,fmax);
    Pw = [0;Pw];
    figure(h3); title(['analysis time = ' num2str(analysisTimes(ii)) ' s']); set(gca,'yscale','linear')
    figure(h3); axis([10 1e4 0 2e-2])
    % figure(h3); axis([10 1e4 get(gca,'ylim')])
    figure(h4); title(['analysis time = ' num2str(analysisTimes(ii)) ' s'])
    figure(h4); axis([10 1e4 0 2])
    [h5, h6, ratioMean, ratioStd] = testpsd(Pw,cdata,fs,['white' num2str(fs)],fmin,fmax);
    figure(h6); set(gca,'xscale','linear'); axis([10 1.1*fmax 0 2])
    
end


% -------------------------------------------------------------------------
%    Save plots if desired.
% -------------------------------------------------------------------------

if savePlots
    figure(h1); saveas(gcf,[tag '_1_h-spectra'],'png');
    figure(h2); saveas(gcf,[tag '_2_h-spectra-ratios'],'png');
    figure(h3); saveas(gcf,[tag '_3_cond-data-spectra'],'png');
    figure(h4); saveas(gcf,[tag '_4_cond-spectra-ratios-1'],'png');
    figure(h6); saveas(gcf,[tag '_5_cond-spectra-ratios-2'],'png');    
end

