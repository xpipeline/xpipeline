% This script demonstrates the 'opt_reduction' option for the function
% xmakeskygrid.m (r6386).

close all; clear 

% ---- Set details of sky grid required: three overlapping circles for the
%      HLV network.
ra_ctr_deg_all  = '102~107~125';
dec_ctr_deg_all = '-2~22~15';
sigma_deg       = '7~10~10';
nSigma          = '2';
gps             = '1234567890';
sites           = 'H~L~V';
delay_tol       = '1e-3';
outputfile      = 'None';
gridtype        = 'circular';
optimisation    = '2'  % '1' or '2'

% ---- Split string lists into vectors.
ra_ctr_deg_vec  = tildedelimstr2numorcell(ra_ctr_deg_all);
dec_ctr_deg_vec = tildedelimstr2numorcell(dec_ctr_deg_all);
sigma_deg_vec   = tildedelimstr2numorcell(sigma_deg);
% ---- Loop over each circle, construct a separate grid, and plot.
for ii=1:length(ra_ctr_deg_vec)
    % ----Construct a grid for this circle (no optimisation).
    [ra{ii},dec{ii}] = xmakeskygrid(num2str(ra_ctr_deg_vec(ii)), ...
        num2str(dec_ctr_deg_vec(ii)),gps,num2str(sigma_deg_vec(ii)), ...
        nSigma,sites,delay_tol,outputfile,gridtype,'0','0');
    Ngrid(ii) = length(ra{ii});
    % ---- Plot this grid.
    if ii==1
        figure;
        pointStyle = {'b.','g.','r.','mo'};
    end
    plot(ra{ii},dec{ii},pointStyle{ii},'markersize',10);
    hold on
end
% ---- Now construct combined grid for all circles with desired
%      optimisation.
[ra{end+1},dec{end+1}] = xmakeskygrid(ra_ctr_deg_all,dec_ctr_deg_all, ...
    gps,sigma_deg, nSigma,sites,delay_tol,outputfile,gridtype,'0',optimisation);
Ngrid(end+1) = length(ra{end});
% ---- Plot combined grid.
plot(ra{end},dec{end},pointStyle{end},'markersize',12);
% ---- Format and save plot.
axis square
grid on 
xlabel('ra [deg]');
ylabel('dec [deg]');
set(gca,'fontsize',16)
legend('medium density','high density','low density')
title(['grid optimisation: ' optimisation ' (' num2str(sum(Ngrid(1:end-1))) ...
    ' points -> ' num2str(Ngrid(end)) ' points)'])
saveas(gcf,['grid_optimisation_' optimisation '_test.png'],'png')
