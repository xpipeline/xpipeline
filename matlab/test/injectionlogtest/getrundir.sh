#!/bin/bash

# getrundir: script to construct an ASCII file listing GRB run directories and 
# other useful info for the S6/VSR2-3 bursts GRB search.
#
# Usage:
#
#  ./getrundir.sh weburl.txt
#
# Here weburl.txt is a file listing the URLs of the closed-box web pages for 
# all GRBs of interest.  I constructed that file by copying the URLs from 
# https://wiki.ligo.org/Bursts/S6VSR23GRBrerun .  The output is a file 
# grb_info.txt with 1 row per GRB and columns
#
#   GRB name
#   run directory on atlas
#   network
#   GPS trigger time 
#   ra (deg)
#   dec (deg)
#   1-sigma size for sky grid (deg)
#   injection sky position distribution parameters 
#   T90 time (sec) -- if known
#
# The script also outputs files named network.txt, rundir.txt, sky.txt, and 
# webfile.txt. See https://wiki.ligo.org/Bursts/S6VSR2GRBInjectionLogTests 
# for more info.

# Convert list of web page urls into web page files including absolute paths on atlas.
rm -f webfile.txt
while read url
do 
  echo ${url} | sed 's/https:\/\/atlas.atlas.aei.uni-hannover.de\/~mwas/\/home\/mwas\/WWW/g' >> webfile.txt
done < ${1}

# Extract from web page files the list of GRB names and run directories.
rm -f rundir.txt
while read file
do 
  grep "<title>" ${file} | grep -v Error | awk '{print $2}' | sed 's/auto_web//g' | awk -F/ '{print $7, $0}' >> rundir.txt
done < webfile.txt

# Extract network from closed-box summary text files in auto_web directory.
rm -f network.txt
while read grb rundir
do 
  head -n 2 ${rundir}/auto_web/*closedbox_summary.txt | tail -n 1 | awk '{print $2}' > .network.tmp
  while read network
  do 
    echo "${grb} ${network}" >> network.txt
  done < .network.tmp
done < rundir.txt

# Extract info on GRB GPS time & error box from 
#   /home/mwas/GRBrerun/grbList/grbListGPS.txt 
# Columns of this file are
#   1. grbname
#   2. trigger_time
#   3. ra
#   4. decl
#   5. skyposerr
#   6. injdistrib
#   7. endOffset
rm -f sky.txt
while read grb rundir
do 
  echo "${grb}" | sed 's/GRB//g' > .number.tmp
  while read number 
  do 
    grep "${number} " /home/mwas/GRBrerun/grbList/grbListGPS.txt > .sky.tmp
  done < .number.tmp
  awk '{print "GRB" $0}' .sky.tmp >> sky.txt  
done < rundir.txt

# Combine all required info into a single file.
join rundir.txt network.txt > .joined.tmp
join .joined.tmp sky.txt > grb_info.txt

# Delete temporary files.
rm .joined.tmp .network.tmp .number.tmp .sky.tmp

