% name = 'SGC150';
% targetnetwork = 'L1V1';

% nameALL = {'SGC100','SGC150','SGC300'};
targetnetworkALL = {'H1L1V1','H1L1','H1V1','L1V1'};
nameALL = {'NSBH','BNS'};
% targetnetworkALL = {'L1V1'};

for iii=1:length(nameALL)
    name = nameALL{iii};
    for jjj=1:length(targetnetworkALL)
        targetnetwork = targetnetworkALL{jjj};

        % -------------------------------------------------------------------------

        % ---- All, Swift, & Fermi GRBs.
        load(['data/' name '_' targetnetwork '_all.mat']);
        swift = load(['data/' name '_' targetnetwork '_swift.mat']);
        fermi = load(['data/' name '_' targetnetwork '_fermi.mat']); 

        lineWidth = 2;
        figure1 = figure('PaperType','A4', ...%'PaperSize',[11.69 8.268],...
            'PaperOrientation','landscape');
        set(gca,'fontsize',16);

        % ---- Plots.
        nGRB = length(desirednetwork)
        nGRBswift = length(swift.desirednetwork)
        nGRBfermi = length(fermi.desirednetwork)

        % ---- Time offset from GRB.
        plotname = 'toffset';
        mu = sum(ntoffset_good)/(660/10);
        sig = mu.^0.5*nGRB^0.5;
        subplot(3,4,1)
        stairs(toffsetbin-0.5*(toffsetbin(2)-toffsetbin(1)),ntoffset_all,'linewidth',lineWidth); 
        grid on; hold on
        stairs(toffsetbin-0.5*(toffsetbin(2)-toffsetbin(1)),ntoffset_good,'g-','linewidth',lineWidth); 
        plot([-600;60],[mu;mu],'k-','linewidth',lineWidth);
        plot([-600;60],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        axis tight
        title('time offset');
        xlabel('injection time - GRB time (s)');
        ylabel('number of injections');
        legend('all','11 min',2)
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.fig'],'fig');
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.png'],'png');

        % ---- Nanosec field in injection time.
        plotname = 'nanosec';
        mu = nInjection/(100)*nGRB;
        sig = mu.^0.5*nGRB^0.5;
        subplot(3,4,2)
        stairs(nanosecbin,nnanosec_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;1e9],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;1e9],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('nanosec injection time');
        xlabel('inj time - round(inj time) (nanosec)');
        ylabel('number of injections');
        legend(detector)
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.fig'],'fig');
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.png'],'png');

        % ---- Pairwise timing difference at COE due to miscalibration.
        plotname = 'timediff';
        mu = nInjection/(length(deltatbin))*nGRB;
        sig = mu.^0.5*nGRB^0.5;
        subplot(3,4,3)
        stairs(deltatbin,ndeltat_all,'linewidth',lineWidth); grid on
        grid on; hold on
        plot([0;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('pairwise timing difference');
        xlabel('COE delay between each detector pair')
        ylabel('number of injections');
        if size(ndeltat_all,2)==1 
            legend([targetnetwork(1:2) '-' targetnetwork(3:4)])
        elseif size(ndeltat_all,2)==3 
            legend([targetnetwork(1:2) '-' targetnetwork(3:4)], ...
                [targetnetwork(1:2) '-' targetnetwork(5:6)], ...
                [targetnetwork(3:4) '-' targetnetwork(5:6)])
        end
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.fig'],'fig');
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.png'],'png');

        % ---- Polarization angle distribution.
        plotname = 'polarization';
        mu = nInjection/(length(psibin))*nGRB;
        sig = mu.^0.5*nGRB^0.5;
        subplot(3,4,4)
        stairs(psibin,npsi_all,'linewidth',lineWidth); grid on
        grid on; hold on
        plot([0;pi],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;pi],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        axis([0 pi get(gca,'ylim')]);
        title('polarization angle');
        xlabel('polarization angle (rad)');
        ylabel('number of injections');
        legend(detector)
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.fig'],'fig');
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.png'],'png');

        % ---- Swift --------------------------------------------------------------
        % ---- Radial distance from error box center.
        plotname = 'radial';
        mu = swift.nInjection/(length(swift.radbin))*nGRBswift;
        sig = mu.^0.5*nGRBswift^0.5;
        subplot(3,4,5)
        stairs(swift.radbin,swift.nrad_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('radial distance from error box center');
        xlabel('radial distribution');
        ylabel('number of injections');
        legend('Swift')
        % ---- Azimuthal position of jittered injection.
        plotname = 'azimuthal';
        mu = swift.nInjection/(length(swift.azibin))*nGRBswift;
        sig = mu.^0.5*nGRBswift^0.5;
        subplot(3,4,6)
        stairs(swift.azibin,swift.nazi_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;2*pi],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;2*pi],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        axis([0 2*pi get(gca,'ylim')]);
        title('azimuthal position of jittered injection');
        xlabel('azimuthal distribution');
        ylabel('number of injections');
        legend('Swift')
        % ---- Fermi --------------------------------------------------------------
        % ---- Radial distance from error box center.
        plotname = 'radial';
        mu = fermi.nInjection/(length(fermi.radbin))*nGRBfermi;
        sig = mu.^0.5*nGRBfermi^0.5;
        subplot(3,4,7)
        stairs(fermi.radbin,fermi.nrad_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('radial distance from error box center');
        xlabel('radial distribution');
        ylabel('number of injections');
        legend('Fermi')
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.fig'],'fig');
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.png'],'png');
        % ---- Azimuthal position of jittered injection.
        plotname = 'azimuthal';
        mu = fermi.nInjection/(length(fermi.azibin))*nGRBfermi;
        sig = mu.^0.5*nGRBfermi^0.5;
        subplot(3,4,8)
        stairs(fermi.azibin,fermi.nazi_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;2*pi],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;2*pi],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        axis([0 2*pi get(gca,'ylim')]);
        title('azimuthal position of jittered injection');
        xlabel('azimuthal distribution');
        ylabel('number of injections');
        legend('Fermi')
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.fig'],'fig');
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.png'],'png');
        % ----=---------------------------------------------------------------------

        % ---- Amplitude jittering of injections.
        plotname = 'amplitude';
        mu = nInjection/(length(amplbin))*nGRB;
        sig = mu.^0.5*nGRB^0.5;
        subplot(3,4,9)
        stairs(amplbin,nampl_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('amplitude jittering');
        xlabel('relative amplitude');
        ylabel('number of injections');
        legend(detector)
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.fig'],'fig');
        % saveas(gcf,[name '_' targetnetwork '_' plotname '.png'],'png');

        % ---- Inclination jittering of injections.
        plotname = 'inclination';
        mu = nInjection/(sum(nincl_all(:,1)>0))*nGRB;  %-- approximate
        sig = mu.^0.5*nGRB^0.5;
        % subplot(5,2,)
        % stairs(inclbin,nincl_all+0.5,'linewidth',lineWidth); grid on
        % grid on; hold on
        % plot([-1;1],[mu;mu],'k-','linewidth',lineWidth);
        % plot([-1;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        % title('inclination jittering');
        % xlabel('inclination (rad)');
        % ylabel('number of injections');
        % legend(detector)
        %
        subplot(3,4,10)
        %subplot(1,2,1)
        stairs(inclbin,nincl_all+0.5,'linewidth',lineWidth); grid on
        grid on; hold on
        plot([-1;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([-1;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('inclination jittering');
        xlabel('inclination (rad)');
        ylabel('number of injections');
        legend(detector)
        if maxincl==30
            axis([-1 -0.8 get(gca,'ylim')])
        end
        %subplot(1,2,2)
        subplot(3,4,11)
        stairs(inclbin,nincl_all+0.5,'linewidth',lineWidth); grid on
        grid on; hold on
        plot([-1;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([-1;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('inclination jittering');
        xlabel('inclination (rad)');
        ylabel('number of injections');
        legend(detector)
        if maxincl==30
            axis([0.8 1 get(gca,'ylim')])
        end

        % ---- Save plot.  Force size on screen to match A4 paper in landscape
        %      mode, then force printed output to match on-screen size.
        set(gcf,'PaperPositionMode','auto')
        set(gcf,'Units','centimeters');
        set(gcf,'position',[1 2 29.7 21])
        set(gcf,'Units','pixels');
        saveas(gcf,['plots/' name '_' targetnetwork '.fig'],'fig');
        saveas(gcf,['plots/' name '_' targetnetwork '.png'],'png');

        lineWidth = 2;
        figure2 = figure('PaperType','A4', ...%'PaperSize',[11.69 8.268],...
            'PaperOrientation','landscape');
        set(gca,'fontsize',16);

        % ---- Mass parameters of injections.
        plotname = 'mass1';
        mu = nInjection/(length(massbin))*nGRB;
        sig = mu.^0.5*nGRB^0.5;
        subplot(1,2,1)
        stairs(massbin,nmass1_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('mass jittering');
        xlabel('mass 1 cumulative distribution');
        ylabel('number of injections');
        legend(detector)
        %
        plotname = 'mass2';
        mu = nInjection/(length(massbin))*nGRB;
        sig = mu.^0.5*nGRB^0.5;
        subplot(1,2,2)
        stairs(massbin,nmass2_all,'linewidth',lineWidth);
        grid on; hold on
        plot([0;1],[mu;mu],'k-','linewidth',lineWidth);
        plot([0;1],[mu-sig,mu+sig;mu-sig,mu+sig],'k--','linewidth',lineWidth);
        title('mass jittering');
        xlabel('mass 2 cumulative distribution');
        ylabel('number of injections');
        legend(detector)

        % ---- Save plot.  Force size on screen to match A4 paper in landscape
        %      mode, then force printed output to match on-screen size.
        set(gcf,'PaperPositionMode','auto')
        set(gcf,'Units','centimeters');
        set(gcf,'position',[1 2 14.85 7])
        set(gcf,'Units','pixels');
        saveas(gcf,['plots/' name '_' targetnetwork '_mass.fig'],'fig');
        saveas(gcf,['plots/' name '_' targetnetwork '_mass.png'],'png');

        close all

        % -------------------------------------------------------------------------

    end
    
end



