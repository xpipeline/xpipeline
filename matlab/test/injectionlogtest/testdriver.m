% ---- Matlab script to test injection logs for S6/VSR2-3 GRB search.
%     
%      This script is set up to work on atlas.
%
%      The input GRB list should be prepared by running getrundir.sh, and must 
%      be in chronological order.  You will need separate copies of the 
%      grb_info file containing all GRBs, Fermi GRBs, and Swift GRBs.
%      (You can generate Fermi-only and Swift-only copies of grb_info.txt by 
%      downloading https://ldas-jobs.ligo.caltech.edu/~xpipeline/S6/grb/online/triggers/currentgrbs_html_S6_1101.txt
%      and using "grep" and "join" to extract the Fermi and Swift GRBs and 
%      combining those with grb_info.txt.)
%      Select either inspirals or chirplets for the test below and run at the 
%      command line in matlab.  Place the resulting .mat files in a subdirectory 
%      called "data/" and then run testinjectionlog_plots.m or 
%      testinjectionloginspiral_plots.m to make the plots.

% ---- Set matlab path
addpath ~/xpipeline/trunk/misc/        
addpath ~/xpipeline/trunk/share/       
addpath ~/xpipeline/trunk/utilities/   
addpath ~/xpipeline/trunk/searches/grb/

format compact

% -----------------------------------------------------------------------------
%    User-specifiable parameters.
% -----------------------------------------------------------------------------

grbfileCELL = {'grb_info.txt','grb_info_Swift.txt','grb_info_Fermi.txt'};
grbsetCELL = {'all','swift','fermi'};

% ---- To test inspiral injections, uncomment the following block.
if (1)
    injfileCELL = {'injection_insp1410incljitter30.txt','injection_insp1414incljitter30.txt'}; 
    waveformCELL = {'inspiral','inspiral'}; 
    auxparamsCELL = {'',''};
    maxinclCELL = {30,30};
    f0CELL = {155,155};
    nameCELL = {'NSBH','BNS'};
end

% ---- To test chirplet injections, uncomment the following block.
if (0)
    injfileCELL = {'injection_csg100q9incljitter5.txt','injection_csg150q9incljitter5.txt','injection_csg300q9incljitter5.txt'};
    waveformCELL = {'chirplet','chirplet','chirplet'};
    auxparamsCELL = {'1.000e-02~1.000e+02~0.000e+00~0.000e+00','6.667e-03~1.500e+02~0.000e+00~0.000e+00','3.333e-03~3.000e+02~0.000e+00~0.000e+00'};
    maxinclCELL = {5,5,5}; 
    f0CELL = {100,150,300};
    nameCELL = {'SGC100','SGC150','SGC300'}; 
end

targetnetworkCELL = {'H1L1V1','H1L1','H1V1','L1V1'};

batch = 1;

% ---- Loop over waveforms, networks, and GRB sets.
for iname = 1:length(injfileCELL)
    injfile   = injfileCELL{iname};
    waveform  = waveformCELL{iname};
    auxparams = auxparamsCELL{iname};
    maxincl   = maxinclCELL{iname};
    f0        = f0CELL{iname};
    name      = nameCELL{iname};

    for inet = 1:length(targetnetworkCELL)
        targetnetwork = targetnetworkCELL{inet};

        for igc = 1:length(grbsetCELL)
            grbfile = grbfileCELL{igc};
            grbset  = grbsetCELL{igc};

            % ---- Run script.
            if strcmp(waveform,'inspiral')
                testinjectionloginspiral
            else
                testinjectionlog
            end

            % ---- Clear out old variables to avoid clashes.
            clear Omega
            clear Omega_grb
            clear Omega_np
            clear X
            clear X_old
            clear X_old_1
            clear X_old_4
            clear Y
            clear Y_old
            clear Y_old_1
            clear Y_old_4
            clear a
            clear alpha
            clear amplbin
            clear amplparam
            clear ans
            clear auxParamsCorrect
            clear auxparamsmat
            clear azi
            clear azi_old
            clear azibin
            clear b
            clear blockSize
            clear c
            clear cal
            clear cdf
            clear coslambda
            clear currentParameters
            clear dec
            clear delta
            clear deltat
            clear deltatbin
            clear desirednetwork
            clear detector
            clear dir
            clear dphase
            clear dphi
            clear dtime
            clear dx
            clear endoffset
            clear fermi
            clear fid
            clear gps
            clear grb
            clear grbinfo
            clear iCell
            clear icdf
            clear icdf_old
            clear idet
            clear igrb
            clear ii
            clear iinj
            clear inclbin
            clear inclparam
            clear ind
            clear injdistn
            clear injectionFileName
            clear injectionParameters
            clear jParam
            clear k_core
            clear k_tail
            clear kappa
            clear lambda
            clear lineWidth
            clear mass* nmass* mu
            clear nDetector
            clear nGRB
            clear nGRBfermi
            clear nGRBswift
            clear nInjection
            clear nampl
            clear nampl_all
            clear nanosec
            clear nanosec_old
            clear nanosecbin
            clear nazi
            clear nazi_all
            clear ndeltat
            clear ndeltat_all
            clear network
            clear nincl
            clear nincl_all
            clear nnanosec
            clear nnanosec_all
            clear npsi
            clear npsi_all
            clear nrad
            clear nrad_all
            clear ntoffset
            clear ntoffset_all
            clear ntoffset_good
            clear otherparams
            clear out
            clear p_core
            clear p_tail
            clear params
            clear pdf
            clear phi
            clear phi_grb
            clear plotname
            clear processedVSR3GRB
            clear psi
            clear psi_old
            clear psibin
            clear ra
            clear radbin
            clear rawampl
            clear rawampl_old
            clear rawincl
            clear rawincl_old
            clear sig
            clear sigma_core
            clear sigma_stat
            clear sigma_tail
            clear sigmat
            clear skyposerr
            clear startTime
            clear swift
            clear tStartVSR3
            clear theta
            clear theta_grb
            clear tinj
            clear toffset
            clear toffsetbin
            clear waveformCorrect
            clear x

        end

    end

end

