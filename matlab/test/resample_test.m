
% ---- Standard data duration.
T = 256;
% ---- Standard input sampling rate.
sampleFrequencies = 16384;
% ---- Standard output sampling rate.
sampleFrequency = 1024;

% ---- Test newest or reviewed version of resampling code?
newversion = 0;

% ---- Waveform properties.
if (0)
    wf_type = 'inspiral';
    wf_params = '1.4~1.4~0~10';
    wf_T = 16;
    wf_T0 = 15;
end
if (1)
    wf_type = 'inspiral';
    wf_params = '1.4~10.0~0~20';
    wf_T = 16;
    wf_T0 = 15;
end
if (0)
    % ---- Sanity check: this signal is out-of-band, so we get a tiny
    %      residual in the resampled stream.
    wf_type = 'chirplet';
    wf_params = '1e-21~0.001~1000~0~0~0';
    wf_T = 1;
    wf_T0 = 0.5;
end    

% -------------------------------------------------------------------------
%    Make data.
% -------------------------------------------------------------------------

% ---- Number of data samples before/after resampling.
N0 = T*sampleFrequencies;
N  = T*sampleFrequency;

% ---- Make data stream
channelNumber = 1;
data{1} = zeros(N0,1);
[t,h] = xmakewaveform(wf_type,wf_params,wf_T,wf_T0,sampleFrequencies);
clear t
data{1}(N0/2+[1:length(h)]) = h;
% ---- Keep a copy (resampler overwrites original variable).
data0{1} = data{1};

% -------------------------------------------------------------------------
%    Resample data.
% -------------------------------------------------------------------------

if newversion
    % ---- Resampling: code copied directly from xcondition r3745.
    %      Identical to older version except calls downsample() instead of 
    %      resample() when upSampleFactor = 1 (our nominal test case).

    % ---- Factors relating sample frequencies.
    [upSampleFactor, downSampleFactor] = rat(sampleFrequency / ...
        sampleFrequencies(channelNumber));
    
    % ---- The anti-aliasing filter used by default in the matlab resample
    %      function is weak, with an amplitude attenuation of about 0.5 at
    %      the new Nyquist frequency. The anti-aliasing filter constructed
    %      below has much stronger attenuation.
    % ---- Design anti-alias filter.
    filterOrder = 2 * 256 * max(upSampleFactor, downSampleFactor);
    filterCutoff = 0.99 / max(upSampleFactor, downSampleFactor);
    filterFrequencies = [0 filterCutoff filterCutoff 1];
    filterMagnitudes = [1 1 0 0];
    filterCoefficients = upSampleFactor * ...
        firls(filterOrder, filterFrequencies, filterMagnitudes) .* ...
        hanning(filterOrder + 1)';

    % ---- Resample data, use faster implementation if no upsample needed
    if upSampleFactor == 1
      filterCoefficients = [filterCoefficients(filterOrder/2+1:end) ...
                          zeros(1,length(data{channelNumber})-filterOrder-1) ...
                          filterCoefficients(1:filterOrder/2)]';
      data{channelNumber} = downsample(ifft(fft(data{channelNumber}).* ...
                                            conj(fft(filterCoefficients))),downSampleFactor);
    else
      data{channelNumber} = resample(data{channelNumber}, upSampleFactor, ...
                                     downSampleFactor, filterCoefficients);
    end

else
    % ---- Resampling: code copied directly from xcondition r3444.
    
    % ---- Factors relating sample frequencies.
    [upSampleFactor, downSampleFactor] = rat(sampleFrequency / ...
        sampleFrequencies(channelNumber));
    
    % ---- The anti-aliasing filter used by default in the matlab resample
    %      function is weak, with an amplitude attenuation of about 0.5 at
    %      the new Nyquist frequency. The anti-aliasing filter constructed
    %      below has much stronger attenuation.
    % ---- Design anti-alias filter.
    filterOrder = 2 * 256 * max(upSampleFactor, downSampleFactor);
    filterCutoff = 0.99 / max(upSampleFactor, downSampleFactor);
    filterFrequencies = [0 filterCutoff filterCutoff 1];
    filterMagnitudes = [1 1 0 0];
    filterCoefficients = upSampleFactor * ...
        firls(filterOrder, filterFrequencies, filterMagnitudes) .* ...
        hanning(filterOrder + 1)';

    % ---- Resample data.
    data{channelNumber} = resample(data{channelNumber}, upSampleFactor, ...
        downSampleFactor, filterCoefficients);

end

% -------------------------------------------------------------------------
%    Make plots.
% -------------------------------------------------------------------------

% ---- Plot original & resampled data.
t0 = [0:N0-1]'/sampleFrequencies;
t  = [0:N-1]'/sampleFrequency;
fig1 = figure; set(gca,'fontsize',16);
set(gcf,'Position',[360   280   560*2   840]);
subplot(3,2,1)
plot(t0,data0{1})
hold on; grid on
plot(t,data{1},'g-')
xlabel('time (s)'); ylabel('strain')
title('time series')
axis([0 T get(gca,'ylim')])
subplot(3,2,3)
plot(t0,data0{1},'linewidth',2)
hold on; grid on
plot(t,data{1},'g-','linewidth',2)
xlabel('time (s)'); ylabel('strain')
axis([T/2-0.05 T/2+0.05 -max(abs(h)) max(abs(h))])
subplot(3,2,5)
plot(t0,data0{1},'linewidth',2)
hold on; grid on
plot(t,data{1},'g-','linewidth',2)
xlabel('time (s)'); ylabel('strain')
axis([T/2+wf_T0-0.05 T/2+wf_T0+0.05 -max(abs(h)) max(abs(h))])
legend('original','resampled')
clear t0 t

% ---- Data is only nonzero in the middle.  FFT in one block.  (Apply
%      scaling factor for resmapled data to correct for change in 1/N
%      normalization of FFT.)
fdata0 = fft(data0{1});
fdata0 = abs(fdata0(1:N0/2+1));
f0 = [0:N0/2]'/T;
fdata  = fft(data{1})*downSampleFactor/upSampleFactor;
fdata  = abs(fdata(1:N/2+1));
f  = [0:N/2]'/T;

% ---- Plot spectra.
% fig2 = figure; set(gca,'fontsize',16);
% set(gcf,'Position',[360   480   560   560]);
subplot(3,2,2)
loglog(f0,fdata0,'linewidth',2)
grid on; hold on;
loglog(f,fdata,'g-','linewidth',2)
xlabel('frequency (Hz)'); ylabel('amplitude (arb)');
axis([1 sampleFrequencies/2 get(gca,'ylim')])
title('spectra')
subplot(3,2,4)
semilogy(f0,fdata0,'linewidth',2)
grid on; hold on;
semilogy(f,fdata,'g-','linewidth',2)
plot(sampleFrequency/2*[1;1],[get(gca,'ylim')]','k--')
xlabel('frequency (Hz)'); ylabel('amplitude (arb)');
axis([sampleFrequency/2*[0.9 1.1] get(gca,'ylim')])

% ---- Create textbox.
msg = sprintf('%s%s\n%s%s\n%s%d\n%s%d\n%s%d','wf\_type: ',wf_type,'wf\_params: ',wf_params, ...
    'input sample rate: ',sampleFrequencies, ...
    'output sample rate: ',sampleFrequency, ...
    'newest xcondition (1/0): ',newversion);
annotation(gcf,'textbox',[0.57 0.12 0.33 0.20],'String',{msg});



