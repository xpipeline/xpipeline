function [h1, h2, ratioMean, ratioStd, Pw, F] = testpsd(P,data,fs,IFO,fmin,fmax)
% TESTPSD - Compare input power spectrum to that from pwelch.
%
% TESTPSD compares an input PSD to that returned by running pwelch on the
% specified data, and also to a design curve, if specified.
%
% usage:
%
%  [h1, h2, ratioMean, ratioStd, Pw, F] = testpsd(P,data,fs,IFO,fmin,fmax)
%
%  P            Vector.  Input PSD to be tested.
%  data         Vector. Time-series data which P describes.
%  fs           Scalar.  Sampling rate of data [Hz].
%  IFO          Optional string.  Design curve as understood by SRD.
%  fmin,fmax    Optional scalars.  If both are specified, then ratio
%               calculations are restricted to this frequency range.  
%
%  h1           Handle to a figure.
%  h2           Handle to a figure.
%  ratioMean    Mean of ratio of input AMPLITUDE spectrum to pwelch spectrum.
%  ratioStd     Standard deviation of ratio of input AMPLITUDE spectrum to
%               pwelch spectrum. 
%  Pw           Vector.  Power spectrum from pwelch.
%  F            Vector.  Frequencies at which Pw is sampled [Hz].
%
% The input PSD "P" must be sampled from 0 to Nyquist.
%
% Patrick.Sutton at astro.cf.ac.uk
%
% $Id$

% ---- Assign default arguments.
if nargin<4
    IFO = [];
end
if nargin<6 || isempty(fmin) || isempty(fmax)
    fmin = 0;
    fmax = fs/2;
end
    
% ---- Figure out frequency sampling of input PSD.
dF = (fs/2)/(length(P)-1);
F = [0:dF:fs/2]';

% ---- Welch PSD with hann window.
w = hann(fs/dF);
[Pw,Fw] = pwelch(data,w,[],[],fs);

if (F~=Fw)
    disp('Error: frequencies of pwelch and input test PSDs do not match:'); 
    F(:).'
    Fw(:).'
    error('Exiting.');
end

% ---- Drop DC.
F(1) = [];
P(1) = [];
Pw(1) = [];

% ---- Plot PSDs.
h1 = figure; set(gca,'fontsize',16)
loglog(F,[P,Pw].^0.5);
hold on
grid on
xlabel('frequency (Hz)');
ylabel('strain noise amplitude (Hz^{-1/2})');
legendText{1} = 'test PSD';
legendText{end+1} = 'pwelch PSD';
%
if ~isempty(IFO)
    % ---- Theoretical PSD
    warning off
    Psrd = SRD(IFO,F);
    warning on
    loglog(F,Psrd.^0.5,'k-'); 
    legendText{end+1} = 'design';
end
%
if fmin>0 && fmax<fs/2
    xlimits = xlim;
    ylimits = ylim;
    loglog([fmin;fmin],ylimits(:),'k--');
    loglog([fmax;fmax],ylimits(:),'k--');
    legendText{end+1} = 'test band';
end
%
legend(legendText)

% ---- Compute "ratio" outputs.
ind = find(F>=fmin & F<=fmax);
if isempty(IFO)
    ratioMean = [mean((P(ind)./Pw(ind)).^0.5)];
    ratioStd  = [ std((P(ind)./Pw(ind)).^0.5)];
else
    ratioMean = [mean((P(ind)./Pw(ind)).^0.5); mean((P(ind)./Psrd(ind)).^0.5)];
    ratioStd  = [ std((P(ind)./Pw(ind)).^0.5);  std((P(ind)./Psrd(ind)).^0.5)];
end
ratioMeanStr = num2str(round(100*ratioMean)/100);
ratioStdStr  = num2str(round(100*ratioStd)/100);

% ---- Plot ratio of input PSD to welch PSD, and also to to design PSD (if
%      any). 
clear legendText
h2 = figure; set(gca,'fontsize',16)
semilogx(F,(P./Pw).^0.5);
hold on
grid on
xlabel('frequency (Hz)');
ylabel('ratio of noise amplitudes');
legendText{1} = ['test PSD/ pwelch (' ratioMeanStr(1,:) ' +/- ' ratioStdStr(1,:) ')'];
%
if ~isempty(IFO)
    semilogx(F,(P./Psrd).^0.5,'k-'); 
    legendText{end+1} = ['test PSD/ design (' ratioMeanStr(2,:) ' +/- ' ratioStdStr(2,:) ')'];
end
%
if nargin>=6 && ~isempty(fmin) && ~isempty(fmax)
    xlimits = xlim;
    ylimits = ylim;
    semilogx([fmin;fmin],ylimits(:),'k--');
    semilogx([fmax;fmax],ylimits(:),'k--');
    legendText{end+1} = 'test band';
end   
%
legend(legendText)

% ---- Done.
return

