% testing that the fastlabel is working correctly, should display only zeroes
% if everything works

nTimeBins = 1000;
nFreqBins = 500;
map = rand(nFreqBins,nTimeBins);

for thresh = [0.5 0.9 0.99]
  bwmap = map > thresh;
  pixTime = repmat((1:nTimeBins),[nFreqBins 1]);
  pixFreq = repmat((1:nFreqBins)',[1 nTimeBins]);
  coordList = [pixTime(bwmap) pixFreq(bwmap)];
  fastLabelledList = fastlabel(coordList,[nTimeBins nFreqBins],8);
  labelledMap = bwlabel(bwmap);
  max(abs(labelledMap(bwmap)-fastLabelledList))
end
