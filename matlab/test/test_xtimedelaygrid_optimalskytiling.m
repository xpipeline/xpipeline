% test_xtimedelaygrid_optimalskytiling - script to test and compare optimalskytiling and xtimedelaygrid

network = {'H','L','V'};
dt = 5e-4;
verbose = 1;


% -------------------------------------------------------------------------
%   optimalskytiling
% -------------------------------------------------------------------------

% ---- Construct the grid.
disp(' ')
disp('Calling optimalskytiling ...')
% [opt.theta, opt.phi] = optimalskytiling(network,dt,verbose
warning('Using delay tolerance 1.55 lower than specified for optimalskytiling.')
[opt.theta, opt.phi] = optimalskytiling(network,dt/1.55,verbose);

% ---- Test time-delay coverage of the grid.
disp('Testing optimalskytiling ...')
opt.minDelay = xtestskygrid(10000,[opt.theta,opt.phi],[pi/2,0],pi,network);

disp('... finished with optimalskytiling.')


% -------------------------------------------------------------------------
%   xtimedelaygrid
% -------------------------------------------------------------------------

% ---- Construct sky grid.
disp(' ')
disp('Calling xtimedelaygrid ...')
[del.theta, del.phi] = xtimedelaygrid(network,dt,verbose);

% ---- Test time-delay coverage of the grid.
disp('Testing xtimedelaygrid ...')
del.minDelay = xtestskygrid(10000,[del.theta,del.phi],[pi/2,0],pi,network);

disp('... finished with xtimedelaygrid.')
disp(' ')


% -------------------------------------------------------------------------
%   Plots.
% -------------------------------------------------------------------------

figure;
plot(opt.phi,cos(opt.theta),'b.')
grid on
hold on
plot(del.phi,cos(del.theta),'r.')
legend(['optimalskytiling (' num2str(length(opt.theta)) ' points)'], ...
       ['xtimedelaytoskyposition (' num2str(length(del.theta)) ' points)'])
xlabel('phi')
ylabel('cos(theta)')
set(gca,'fontsize',16)
figure;
histogram(opt.minDelay/dt);
xlabel('min(max(delay/dt))')
ylabel('Number of trials')
title('optimalskytiling')
set(gca,'fontsize',16)
figure;
histogram(del.minDelay/dt);
xlabel('min(max(delay/dt))')
ylabel('Number of trials')
title('xtimedelaytoskyposition')
set(gca,'fontsize',16)

