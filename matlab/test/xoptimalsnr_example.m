
% ---- Make some injections to test. Notes: 
%      1) The X-Pipeline functions will expect the duration to be an
%         integer number of seconds that is a power of 2.  
%      2) The sample rate should also be a power of 2.
fs = 16384;
T = 1;
T0 = T/2;
name{1} = 'chirplet'; params{1} = [1e-21,1/100,100];
name{2} = 'cusp';     params{2} = [1e-21,100];
[t,hp1,hc1] = xmakewaveform(name{1},params{1},T,T0,fs);
[t,hp2,hc2] = xmakewaveform(name{2},params{2},T,T0,fs);

% ---- Plot waveforms.
figure; set(gca,'fontsize',16)
subplot(2,1,1)
plot(t,[hp1, hc1],'linewidth',2);
grid on; hold on
xlabel('time (sec)')
ylabel('strain')
title(name{1})
legend('plus','cross')
axis([0.4 0.6 get(gca,'ylim')])
subplot(2,1,2)
plot(t,[hp2, hc2],'linewidth',2);
grid on; hold on
xlabel('time (sec)')
ylabel('strain')
title(name{2})
legend('plus','cross')
axis([0.4 0.6 get(gca,'ylim')])

% % ---- Load some waveforms to test.
% % ---- The two waveforms should have the same sample rate and number of
% %      samples.  If not, resample and zero-pad as needed.  These two
% %      waveforms from the SN MDC sets will work.
% load osnsearch.mat
% disp(['waveform 1 name and number of samples:']);
% name{1}
% size(hp{1})
% disp(['waveform 4 name and number of samples:']);
% name{4}
% size(hp{4})
% %
% % ---- These waveforms are defined with the crazy amplitude of hrss=1.
% %      Rescale to somethign sensible (1e-21).
% h1 = 1e-21*hp{1};
% h4 = 1e-21*hp{4};
%
% % ---- Determine the duration of the waveforms. The X-Pipeline functions
% %      will expect the duration to be an integer 
% %      number of seconds that is a power of 2.
% T = length(h1)/fs

% ---- Now generate noise curve for SNR and match calculation.  Notes:
%      1) Frequency spacing must be 1/T.
%      2) Frequencies must be positive and <= Nyquist. It's usually
%         simplest to just use the full available frequency range, as shown
%         below. 
df = 1/T;
f = [df:df:fs/2]';
% ---- Compute initial LIGO design curve at positive frequencies only.
warning off
S = SRD('LIGO',f);
warning on

% ---- Select frequency range of interest for the analysis.  Notes:
%      1) fmin must be >= f(1).
%      2) fmax must be <= f(end).
fmin = f(1);
fmax = f(end);
% ---- Compute SNR against initial LIGO design.  Note that SNR, h_rss,
%      Fchar, bw are all computed in the frequency domain, restricted to
%      the range [fmin,fmax]. 
disp(['waveform 1: ' name{1}]);
[SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] = xoptimalsnr([hp1 hc1],0,fs,S,f(1),df,fmin,fmax)
disp(['waveform 2: ' name{2}]);
[SNR, h_rss, h_peak, Fchar, bw, Tchar, dur] = xoptimalsnr([hp2,hc2],0,fs,S,f(1),df,fmin,fmax)

% ---- Compute some inner products.  We'll use the plus polarisations as
%      an example.
h1h2 = innerproduct(hp1,hp2,fs,S,f(1),df,fmin,fmax,1);
title([name{1} ' x ' name{2}])
h1h1 = innerproduct(hp1,hp1,fs,S,f(1),df,fmin,fmax,1);
title([name{1} ' x ' name{1}])
h2h2 = innerproduct(hp2,hp2,fs,S,f(1),df,fmin,fmax,1);
title([name{2} ' x ' name{2}])
% ---- Compute the maximum match between the waveforms.
match = max(abs(h1h2))/(h1h1(1) * h2h2(1))^0.5
[value,index] = max(abs(h1h2));
disp(['Plus polarisation: maximum match of ' num2str(match) ...
    ' at delay of ' num2str(index-1) ...
    ' samples (' num2str((index-1)/fs) ' sec)']);


