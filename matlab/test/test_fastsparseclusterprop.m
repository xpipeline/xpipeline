% test_fastsparseclusterprop.m: script to test fastsparseclusterprop and 
% fastlabel functions against clusterTFmapNew.m. 
% Patrick Sutton <patrick.sutton@astro.cf.ac.uk>
%
% Usage: simply run the script after ensuring that the required functions 
% are in your matlab path, e.g.:
%   addpath ~/opt/xpipeline/branches/linear/r4095/share/xpipeline/matlab/
% Here is example output:
%
% >> fastcpp_code_test
% Generated 3 likelihood maps of size 50x100 pixels.
% Found 328 clusters of size up to 8 pixels.
% Testing fastlabel against clusterTFmapNew:
%     Labelled maps are identical.
% Testing fastsparseclusterprop against clusterTFmapNew:
%     Cluster likelihoods are identical.
%     Clusters are identical.
% Done.

% ---- Make two random maps of size 50x100 each.  The first map will be the one %      used for thresholding.
E = rand(50,100,3);
% ---- Map threshold: make lower than normal so we have lots of clusters 
%      to test.
E0 = 0.90;

% ---- Users should not need to edit below this line ...
disp(['Generated ' num2str(size(E,3)) ' likelihood maps of size ' num2str(size(E,1)) 'x' num2str(size(E,2)) ' pixels.']);

% ---- Previously reviewed clustering function, with connected-8 clustering.
[clusterArray, clusterStruct, labelledMap] = clusterTFmapNew(E,1,'connected',E0,8);
disp(['Found ' num2str(size(clusterArray,1)) ' clusters of size up to ' num2str(max(clusterArray(:,7))) ' pixels.']);

% ---- Test new fastlabel code.  
%      Note that inputs are in the order dimension(1) = time, 
%      dimension(2) = freq, which is opposite to the default order from 
%      matlab's fft() function and used in X-Pipeline. 
[row,col] = find(E(:,:,1)>E0);
ind = sub2ind([size(E,1),size(E,2)],row,col);
fastLabelledMap = fastlabel([col,row],[size(E,2),size(E,1)],8);

% ---- Non-zero labelledMap values should be the same as fastlabelledMap.
disp('Testing fastlabel against clusterTFmapNew:');
if isequal(fastLabelledMap,labelledMap(labelledMap>0))
    disp('    Labelled maps are identical.');
else
    warning('    Mismatch in labelled maps!');
end

% ---- Test new fastclusterprop.cpp.  First have to extract black pixels from 
%      each likelihood map - somewhat tortured.
disp('Testing fastsparseclusterprop against clusterTFmapNew:');
clear Eslice Eslicepix Epix
Eslice = E(:,:,1);
Eslicepix = Eslice(ind);
Epix(:,1,1) = Eslicepix(:);
for ii = 2:size(E,3)
    Eslice = E(:,:,ii);
    Eslicepix = Eslice(ind);
    Epix(:,1,ii) = Eslicepix(:);
end
%      (i) test only likelhoods
fastClusterArray = fastsparseclusterprop(fastLabelledMap, Epix);
if max(abs(fastClusterArray(:,end)-clusterArray(:,end)))>1e-10
    warning('    Mismatch in cluster likelihoods.');
else
    disp('    Cluster likelihoods are identical.');
end
%      (ii) include TF properties
fastClusterArray = fastsparseclusterprop(fastLabelledMap, Epix, col, row);
if max(max(abs(fastClusterArray-clusterArray)))>1e-10
    warning('    Mismatch in clusters.');
else
    disp('    Clusters are identical.');
end

disp('Done.');

