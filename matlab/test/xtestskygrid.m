function [minDelay theta phi] = xtestskygrid(nTrials,skyGrid,skyPos,skyErr,sites)
% XTESTSKYGRID - test the time-delay coverage of a sky grid
% 
% [minDelay theta phi] = xtestskygrid(nTrials,skyGrid,skyPos,skyErr,sites)
% 
% nTrials      Scalar. Number of random locations in error box to test.
% skyGrid      Array. List of sky positions in the grid, in Earth-fixed
%              coordinates "theta" and "phi" [rad].
% skyPos       Vector. Sky position of the external trigger in Earth-fixed
%              coordinates "theta" and "phi" [rad].
% skyErr       Scalar. Maximum skyErr of the error box covered by the
%              skyGrid [rad].
% sites        Cell array of strings. List of names of detectors for which
%              the skyGrid is constructed.
%
% minDelay     Vector of length nTrials. Minimum over grid points of the
%              maximum over sites of the timing error between each trial
%              sky position and the skyGrid.
% theta        Vector of length nTrials. Polar coordinate of each test
%              point [rad]. 
% phi          Vector of length nTrials. Azimuthal coordinate of each test
%              point [rad]. 
% 
% $Id$


% -------------------------------------------------------------------------
%   Construct array of test positions.
% -------------------------------------------------------------------------

% ---- Extract Earth-fixed coordinates of external trigger.
theta0 = skyPos(1);
phi0   = skyPos(2);

% ---- Unit vector pointing towards the external trigger.
earthToGRB = CartesianPointingVector(phi0,theta0);

% ---- Generate trial sky positions. They are uniformly distributed in
%      cos(theta) up to a maximum cos(SkyErr). Later we will rotate to
%      center the trial positions on the external trigger location.
theta = acos((1-cos(skyErr))*rand(nTrials,1) + cos(skyErr));
phi   = rand(nTrials,1)*2*pi;
% ---- Convert from spherical coords to cartesian for convenience.
V = CartesianPointingVector(phi,theta);

% ---- Construct vector which we will rotate skyPositions around.
v1 = [0 0 1];
v2 = earthToGRB;
v3 = cross(v1,v2);

% ---- Angle which we will rotate skyPositions about v3 by.
psi = acos(dot(v1,v2)/(norm(v1)*norm(v2)));

% ---- Loop over all skyPositions rotating them by psi about v3.
for iVec = 1:size(V,1)
    Vrot(iVec,:) = RotateVector(V(iVec,:),v3,psi);   
end

% ---- Convert rotated skyPositions back to spherical coords.
theta = acos(Vrot(:,3));
phi   = atan2(Vrot(:,2),Vrot(:,1));


% -------------------------------------------------------------------------
%   Compute delays compared to all grid points.
% -------------------------------------------------------------------------

% ---- Compute time delays for each test position.
dtSky  = computeTimeShifts(sites,[theta phi]);
% ---- Compute time delays for each grid position.
dtGrid = computeTimeShifts(sites,skyGrid);

% ---- For ease of calculation, reshape both delay arrays to 
%      nTrials x nGrid x nSites .
nGrid  = size(skyGrid,1);
nSites = length(sites);
dtSky  = repmat(reshape(dtSky,[nTrials 1 nSites]),[1 nGrid 1]);
dtGrid = repmat(reshape(dtGrid,[1 nGrid nSites]),[nTrials 1 1]);

% ---- Loop over all pairs of detectors (baselines), computing difference
%      in delays along that baseline between all test positions and all
%      grid points. 
iPair=1;
for iSite1=1:nSites-1
    for iSite2=iSite1+1:nSites
        dist(:,:,iPair) = (dtSky(:,:,iSite1)  - dtSky(:,:,iSite2)) - ...
                          (dtGrid(:,:,iSite1) - dtGrid(:,:,iSite2));
        iPair=iPair+1;
    end
end

% ---- First maximise along all baselines to get worst-case error. Then
%      minimise over grid points.
minDelay = squeeze(min(max(abs(dist),[],3),[],2)) ;

% ---- Done.
return

