% -------------------------------------------------------------------------
%    Binomial test script: Compute and plot thresholds required for
%    3-sigma or 5-sigma effect in the binomial test.
% -------------------------------------------------------------------------

% ---- Decide what we want to do.
computeThresholds = 0;
makePlots = 1;

% ---- HEN trigger data.
Ndraws = 158;                 %-- Number quoted in paper for binomial test.
Ntail = ceil(0.05*Ndraws);    %-- test 5% lowest p values (as in paper). 

% ---- Detection & evidence thresolds.
p5sigma = 2*normcdf(-5,0,1); %-- 5.733e-7
p3sigma = 2*normcdf(-3,0,1); %-- 2.700e-3

% ---- Number of binomial values to keep in each test run.
bkeep = 1000;

% ---- Largest number of Monte Carlo trials to perform in one go.
Nmc = 1e5;

format compact 

% -------------------------------------------------------------------------
%    Compute binomial probability needed for a given signficance in the
%    binomial test.
% -------------------------------------------------------------------------

if computeThresholds
    
    % ---- Generate MANY sets of binomial probabilities according to the null 
    %      hypothesis.
    for ii = 1:1000

        disp(['iteration ' num2str(ii)]);

        % ---- Background Monte Carlo to handle trials factor for N>1.
        %      Generate uniformly distributed local probabilities.
        localProb = sort(rand(Nmc,Ndraws),2);
        % ---- Keep Ntail most significant values.
        localProb = localProb(:,1:Ntail);

        % ---- Cumulative binomial probability for each trial.
        binProb = 1 - binocdf(repmat([0:Ntail-1],Nmc,1),Ndraws,localProb);
        minBinProb = sort(min(binProb,[],2));
        b = minBinProb(1:bkeep);

        save(['minBinProb_' num2str(ii) '.mat'],'b');
    end

    % ---- Extract 3-sigma and 5-sigma thresholds.
    ball = zeros(1000*bkeep,1);
    for ii = 1:1000

        load(['minBinProb_' num2str(ii) '.mat']);
        ball((ii-1)*1000+[1:1000])  = b;

    end
    ball = sort(ball);
    Nkeep = length(ball);

    disp(['% ---- 3-sigma analysis: ----------------------------------']);
    p3sigma = 2*normcdf(-3,0,1);
    disp(['Probability for 3-sigma excursion: ' num2str(p3sigma)]);
    ind3 = floor(p3sigma*100*Nkeep);
    disp(['Binomial probability for 3-sigma excursion: ' num2str(ball(ind3))]);
    disp(['Fractional uncertainty: 1 +/- ' num2str(diff(ball(ind3+floor(ind3^0.5)*[-1,1]) / ball(ind3)) / 2)]);
    disp(['% ---- 5-sigma analysis: ----------------------------------']);
    p5sigma = 2*normcdf(-5,0,1);
    disp(['Probability for 5-sigma excursion: ' num2str(p5sigma)]);
    ind5 = floor(p5sigma*100*Nkeep);
    disp(['Binomial probability for 5-sigma excursion: ' num2str(ball(ind5))]);
    disp(['Fractional uncertainty: 1 +/- ' num2str(diff(ball(ind5+floor(ind5^0.5)*[-1,1]) / ball(ind5)) / 2)]);
    disp(['% ---------------------------------------------------------']);

    % ---- You should get an output that looks like this:
    %
    % % ---- 3-sigma analysis: ----------------------------------
    % Probability for 3-sigma excursion: 0.0026998
    % Binomial probability for 3-sigma excursion: 0.00045524
    % Fractional uncertainty: 1 +/- 0.0019868
    % % ---- 5-sigma analysis: ----------------------------------
    % Probability for 5-sigma excursion: 5.733e-07
    % Binomial probability for 5-sigma excursion: 8.4136e-08
    % Fractional uncertainty: 1 +/- 0.07391
    % % ---------------------------------------------------------

end

% ---- Old version of threshold calculation.
if (0)

    % ---- Threshold for FAP = 0.01.
    Nmc = 1e6;
    [P1, P2, P2e] = binomialtestthreshold(Ndraws,Ntail,0.01,Nmc,1)
    % ---- You should get these results:
    % P1  = 0.001836725046883;
    % P2  = 0.001836698351193;
    % P2e = 1.927001630125380e-05; %-- fractional uncertainty = 1.0%

    % ---- Threshold for FAP = 3-sigma.
    Nmc = 1e6;
    [P1, P2, P2e] = binomialtestthreshold(Ndraws,Ntail,p3sigma,Nmc)
    % ---- You should get these results:
    % P1  = 4.587656557491177e-04;
    % P2  = 4.587602200506469e-04;
    % P2e = 1.030981958477506e-05;  %-- fractional uncertainty = 2.2%

    % ---- Threshold for FAP = 5-sigma.  We need about 7e8 simulations to get
    %      enough statistics.  We to split this up into batches of 7e6 to avoid
    %      hitting virtual memory.
    Nmc = 7e6;
    Nrun = 100;
    P1  = zeros(Nrun,1);
    P2  = zeros(Nrun,1);
    P2e = zeros(Nrun,1);
    I   = zeros(Nrun,1);
    IL  = zeros(Nrun,1);
    IU  = zeros(Nrun,1);
    for ii=11:Nrun
        [P1(ii), P2(ii), P2e(ii), IL(ii), I(ii), IU(ii)] = ...
            binomialtestthreshold(Ndraws,Ntail,p5sigma,Nmc,1);
        save p5sigma.mat  %-- in case matlab crashes
    end
    % ---- You should get these results:
    % mean(P1)  = 9.6350e-08
    % mean(P2)  = 8.8981e-08
    % mean(P2e) = 4.1217e-08
    % std(P1)   = 4.2867e-08
    % std(P2)   = 4.2198e-08
    % std(P2e)  = 2.0218e-08
    % mean(P2e)/length(P2)^0.5 = 4.9263e-09
    % fractional uncertainty = 4.9263e-09 / 8.8981e-08 = 5.5%  
    % ALMOST CERTAINLY UNDERESTIMATED.

end

% -------------------------------------------------------------------------
%    Plot.
% -------------------------------------------------------------------------

if makePlots
    
    % ---- Load measured p values for low-frequency analysis.
    p = load('~/Documents/GWHEN/binomial/p_lf.txt');
    p = sort(p);

    % ---- Interpolate to compute measured p value at each point in
    %      tail required to hit threshold binomial probability.
    P2 = [4.552e-04, 8.41e-08];  %-- hardwired from previous section.
    pthreshold = zeros(Ntail,length(P2));
    localProb = 10.^[-11:0.01:0];
    for ii = 1:length(P2)
        for jj = 1:Ntail
            P = 1 - binocdf(jj-1,Ndraws,localProb);
            pthreshold(jj,ii) = robustinterp1(P,localProb,P2(ii));
        end
    end

    % ---- Plot everything.
    figure; set(gca,'fontsize',16);
    % ---- Null hypothesis.
    loglog([1/Ndraws;1],[1;Ndraws],'k--');
    grid on; hold on
    % ---- Thresholds.
    loglog(pthreshold(:,1),[1:size(pthreshold(:,1))]','k--o')
    loglog(pthreshold(:,2),[1:size(pthreshold(:,2))]','k-o','linewidth',2,'markerfacecolor','k')
    % ---- Plot measured p values.
    loglog(p,[1:length(p)]','b-o','markerfacecolor','b','linewidth',2)
    % ---- Add red dot for most significant measured deviation from null
    %      hypothesis.
    Nmc = 1e4;
    [Pmin_raw, Pmin, index, dPmin] = grbbinomialtest(p(1:Ntail),Ndraws,Nmc);
    disp(['Significance of LF binomial test statistic: ' num2str(Pmin)]);
    plot(p(index),index,'ro','markerfacecolor','r');
    % ---- Formatting.
    legend('null hypothesis','3-sigma excursion','5-sigma excursion','measured',['FAP = ' num2str(Pmin,'%4.2f')],2)
    axis([1e-10 1 0.9 (length(p)+0.5)])
    xlabel('probability p');
    ylabel('number of HENs');
    saveas(gcf,'binomialTest_LF_full.fig','fig');
    savetightfigure(gcf,'binomialTest_LF_full.pdf');

    % ---- Plot for paper.
    figure; set(gca,'fontsize',16);
    loglog([1/Ndraws;1],[1;Ndraws],'k--');
    grid on; hold on
    loglog(pthreshold(:,2),[1:size(pthreshold(:,2))]','k-o','markerfacecolor','k','linewidth',2)
    loglog(p,[1:length(p)]','b-o','markerfacecolor','b','linewidth',2)
    plot(p(index),index,'ro','markerfacecolor','r');
    legend('null hypothesis','5-sigma excursion','measured',2)
    axis([1e-10 1 0.9 (length(p)+0.5)])
    xlabel('probability p');
    ylabel('number of HENs');
    saveas(gcf,'binomialTest_LF.fig','fig');
    savetightfigure(gcf,'binomialTest_LF.pdf');
    
    % --------------------------------------------------------------------

    % ---- Load measured p values for high-frequency analysis.
    p = load('~/Documents/GWHEN/binomial/p_hf.txt');
    p = sort(p);
    Ndraws = 14;
    Ntail = 1;

    % ---- Interpolate to compute measured p value at each point in
    %      tail required to hit threshold binomial probability.
    P2 = [p3sigma, p5sigma];  %-- hardwired for Ntail = 1 trials.
    pthreshold = zeros(Ntail,length(P2));
    localProb = 10.^[-11:0.01:0];
    for ii = 1:length(P2)
        for jj = 1:Ntail
            P = 1 - binocdf(jj-1,Ndraws,localProb);
            pthreshold(jj,ii) = robustinterp1(P,localProb,P2(ii));
        end
    end
    
    % ---- Plot everything.
    figure; set(gca,'fontsize',16);
    % ---- Null hypothesis.
    loglog([1/Ndraws;1],[1;Ndraws],'k--');
    grid on; hold on
    % ---- Thresholds.
    loglog(pthreshold(:,1),[1:size(pthreshold(:,1))]','k--o')
    loglog(pthreshold(:,2),[1:size(pthreshold(:,2))]','k-o','linewidth',2,'markerfacecolor','k')
    % ---- Plot measured p values.
    loglog(p,[1:length(p)]','b-o','markerfacecolor','b','linewidth',2)
    % ---- Add red dot for most significant measured deviation from null
    %      hypothesis.    
    Nmc = 1e4;
    [Pmin_raw, Pmin, index, dPmin] = grbbinomialtest(p(1:Ntail),Ndraws,Nmc);
    disp(['Significance of HF binomial test statistic: ' num2str(Pmin)]);
    plot(p(index),index,'ro','markerfacecolor','r');
    % ---- Formatting.
    legend('null hypothesis','3-sigma excursion','5-sigma excursion','measured',['FAP = ' num2str(Pmin,'%4.2f')],2)
    axis([1e-8 1 0.9 (length(p)+0.5)])
    xlabel('probability p');
    ylabel('number of HENs');
    saveas(gcf,'binomialTest_HF_full.fig','fig');
    savetightfigure(gcf,'binomialTest_HF_full.pdf');

    % ---- Plot for paper.
    figure; set(gca,'fontsize',16);
    loglog([1/Ndraws;1],[1;Ndraws],'k--');
    grid on; hold on
    loglog(pthreshold(:,2),[1:size(pthreshold(:,2))]','k-o','markerfacecolor','k','linewidth',2)
    loglog(p,[1:length(p)]','b-o','markerfacecolor','b','linewidth',2)
    plot(p(index),index,'ro','markerfacecolor','r');
    legend('null hypothesis','5-sigma excursion','measured',2)
    axis([1e-8 1 0.9 (length(p)+0.5)])
    xlabel('probability p');
    ylabel('number of HENs');
    saveas(gcf,'binomialTest_HF.fig','fig');
    savetightfigure(gcf,'binomialTest_HF.pdf');
   
end

