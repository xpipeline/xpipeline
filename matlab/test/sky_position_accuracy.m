% SKY_POSITION_ACCURACY: script to study sky position reconstruction in xsphrad.
%
% To run the script, set the 'trigFile', 'injFile', 'scale', and 'threshold'
% variables as below.
%
% $Id$

close all; clear 

% ------------------------------------------------------------------------------
%    Specify triggers to study.
% ------------------------------------------------------------------------------

% ---- From https://ldas-jobs.ligo.caltech.edu/~valeriu/AllskyBurst_S6D/S6D_H1L1V1_FirstStage/
trigFile  = 'S6D0-H1L1V1_S6D0_p50_50_closedboxassociatedTriggers_adi-b.mat';
injFile   = 'HLV-injection_adi-b.txt';
scale     = 12;
% threshold = 409.42;
threshold = 0;

% % ---- From https://ldas-jobs.ligo.caltech.edu/~valeriu/AllskyBurst_S6D/S6D_H1L1_FirstStage/
% trigFile  = 'S6D_H1L1_FirstStage_run1_closedboxassociatedTriggers_adi-b.mat';
% injFile   = 'HL-injection_adi-b.txt';
% scale     = 22;         %-- injection scale to use
% threshold = 0;%128.47;  %-- threshold to apply (loudest event significance)

% ---- Hardcoded likelihoods for sphrad analysis.
likelihoodType = {'clusterenergy','standardenergy','standardinc','plusenergy','plusinc','crossenergy','crossinc','nullenergy','nullinc','circenergy','circinc','skypositiontheta','skypositionphi'};

% ------------------------------------------------------------------------------
%    Set up network baselines.
% ------------------------------------------------------------------------------

H = LoadDetectorData('H');
L = LoadDetectorData('L');
V = LoadDetectorData('V');
bHL = (H.V-L.V)/3e8;
bHV = (H.V-V.V)/3e8;
bLV = (L.V-V.V)/3e8;

% ------------------------------------------------------------------------------
%    Load triggers and injection file.
% ------------------------------------------------------------------------------

[~, gps_s, gps_ns, phi, theta, psi, name, parameters] = parseinjectionparameters(readinjectionfile(injFile));
Ninj = length(gps_s);
[injAssTrig, injProcMask, injScale, foundIdx, missedIdx] = loadinjassoctrigfile(trigFile,true,threshold,Ninj);

% ------------------------------------------------------------------------------
%    Extract recovered sky positions for all "found" injections.
% ------------------------------------------------------------------------------

% ---- Storage for recovered sky positions.
phi_rec   = zeros(length(foundIdx{scale}),1);
theta_rec = zeros(length(foundIdx{scale}),1);
Vrec      = zeros(length(foundIdx{scale}),3);
Vinj      = zeros(length(foundIdx{scale}),3);

% ---- Extract full likelihood array of all triggers at this injection scale.
%      Injections with missed / non-processed triggers will have zeroes for
%      likelihoods.
likelihood = reshape([injAssTrig(:,scale).likelihood],length(likelihoodType),[]).';

% ---- Restrict to found triggers.
idx = foundIdx{scale};
% ---- Recovered properties.
likelihood = likelihood(idx,:);
theta_rec = likelihood(:,find(strcmp('skypositiontheta',likelihoodType)));
phi_rec = likelihood(:,find(strcmp('skypositionphi',likelihoodType)));
Vrec = CartesianPointingVector(phi_rec,theta_rec);
% ---- Injected properties.
phi_inj = phi(idx);
theta_inj = theta(idx);
Vinj = CartesianPointingVector(phi_inj,theta_inj);
clear phi theta
% ---- Also keep other injected properties for possible future use.
gps_s = gps_s(idx);
gps_ns = gps_ns(idx);
psi = psi(idx);
name = name{idx};
parameters = parameters{idx};

% ------------------------------------------------------------------------------
%    Make plots.
% ------------------------------------------------------------------------------

% ---- Angular offset between injection and reconstructed location.
delta = acos(sum(Vinj.*Vrec,2))*180/pi;
delta_bin = [0:10:180]';
delta_n = histc(delta,delta_bin);
figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
stairs(delta_bin,delta_n,'linewidth',2);
xlabel('angular offset (deg)');
ylabel('count');
grid on; hold on
axis([0 180 get(gca,'ylim')])
title(injFile)

% ---- Delay along HL baseline.
delayHL_inj = Vinj*bHL;
delayHL_rec = Vrec*bHL;
figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
plot(delayHL_inj,delayHL_rec-delayHL_inj,'.')
xlabel('injected HL delay (s)');
ylabel('HL delay offset: rec - inj (s)');
grid on; hold on
% axis equal
% axis(1e-3*[-10 10 -20 20])

delayHV_inj = Vinj*bHV;
delayHV_rec = Vrec*bHV;
figure; set(gca,'fontsize',16); set(gcf,'Color',[1 1 1]);
plot(delayHV_inj,delayHV_rec-delayHV_inj,'.')
xlabel('injected HV delay (s)');
ylabel('HV delay offset: rec - inj (s)');
grid on; hold on
% axis equal
% axis(1e-3*[-27 27 -54 54])

