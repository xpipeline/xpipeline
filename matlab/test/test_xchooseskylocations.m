function [efficiency,numSearch,passFlag,maxPassFlag,delay_search,delay_test,numFound] = ...
   xtestchooseskylocations(ra_ctr_deg,dec_ctr_deg,gps,...
   skyPosError_deg,numSigma,sites,delay_tol_sec,numTest,verbose);

% XTESTCHOOSESKYLOCATIONS - Function to test performance of xchooseskylocations.
%   For each test GRB sky location we construct a list of points to search using
%   xchooseskylocations. We then create a list of fake GRB sky positions and
%   check that these points are captured by our list of search points. 
%
% usage:
% [efficiency,numSearch,passFlag,maxPassFlag,delay_search,delay_test] = ...
% xtestchooseskylocations(ra_ctr_deg,dec_ctr_deg,gps,...
%   skyPosError_deg,numSigma,sites,delay_tol_sec,numTest,verbose); 
%
%  ra_ctr_deg        Double. RA coord of centre of GRB sky pos error circle.        
%  dec_ctr_deg       Double. Dec coord of centre of GRB sky pos error circle.        
%  gps               Double. GPS time of GRB.
%  skyPosError_deg   Double. 1-sigma error in GRB sky pos. This will be 
%                    multiplied by numSigma (see below) to define a radius 
%                    around the central point within which we will search for 
%                    GRBs.
%  numSigma          Integer. Determines sky area about central GRB position we
%                    will try and cover (see skyPosError_deg). 
%  sites             String. Tilde-separated list of detector sites, 
%                    e.g., 'H~L~V'.
%                    2 or 3 sites must be listed.
%  delay_tol_sec     Double. Maximum error in time delay we will allow.
%  numTest           Integer. Number of fake sky positions to generate when
%                    testing the search grid.
%  verbose           Integer. 1 for verbose output, 0 otherwise.
%
%  efficiency        Double. Fraction of test sky positions that were captured
%                    using our search grid.
%  numSearch         Integer. Number of sky positions in our search grid.
%
% xtestchooseskylocations(1,89,874780000.0,5,3,'H1~L1~V1',1e-4,10000,1); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check number of input args
error(nargchk(9, 9, nargin));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Generate sky positions to search over.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rand('seed',12345);

% ---- Call xchooseskylocations to generate list of sky positions to search.
[ra_search_deg,dec_search_deg, probabilities] = xchooseskylocations(...
   num2str(ra_ctr_deg),num2str(dec_ctr_deg),num2str(gps),...
   num2str(skyPosError_deg),num2str(numSigma),sites,...
   num2str(delay_tol_sec),'skyPos.txt',num2str(verbose));

numSearch = length(ra_search_deg);
gps_search = ones(length(numSearch),1) * gps;
sites = tildedelimstr2numorcell(sites);

% ---- Convert search sky positions to earth based coords. 
[phi_search_rad, theta_search_rad] = radectoearth(...
   ra_search_deg,dec_search_deg,gps_search);

% ---- Define skyPositions array that we will pass to ComputeTimeShifts.m.
%      This array is defined as follows (from sinusoidalMap.m)
% The four columns have the following meanings:
%   theta   Polar angle in the range [0, pi] with 0 at the North Pole/ z axis.
%   phi     Azimuthal angle in the range [-pi, pi) with 0 at Greenwich / x axis.
%   pOmega  A priori probability associated with each sky position.
%   dOmega  Solid angle associated with each sky position.
skyPosition_search = [theta_search_rad, ...
                      phi_search_rad, ...
                      zeros(numSearch,1), ...
                      zeros(numSearch,1)];

% ---- Calculate time-delays between sites and Earth's centre for these
%      sky positions.
deltaT_search = computeTimeShifts( sites, skyPosition_search );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Generate sky positions to test.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Convert GRB central positions to earth based coords. 
[phi_ctr_rad, theta_ctr_rad] = radectoearth(ra_ctr_deg,dec_ctr_deg,gps);

% ---- Use Uniform for generating probabilities by brute force.
testMethod = 'Uniform'; 
%testMethod = 'Gaussian';

sigma_rad = skyPosError_deg * pi/180;

switch testMethod

    case 'Gaussian'

        % ---- We will generate positions gaussianly about GRB central position with
        %      sigma = sigma_rad
        

        % ---- Generate sky location points to test.
        [phi_test_rad, theta_test_rad] = xjitterskyposition(...
            phi_ctr_rad*ones(numTest,1),...
            theta_ctr_rad*ones(numTest,1),...
            sigma_rad);
        % ---- Some of these points will be outside our cone.

    case 'Uniform'
        
        % ---- We will generate sky positions about the GRB central position uniform in area.
        psi = 2*pi*rand(numTest,1);
        dOmegaMax = sigma_rad * numSigma;  
        dOmega = asin(rand(numTest,1) * sin(dOmegaMax));
 
        % ---- Construct vector pointing from Earth to GRB central position. 
        earthToGRB = CartesianPointingVector(phi_ctr_rad,theta_ctr_rad);
        % ---- Create temp vectors with opening angle dOmega from earthToGRB. 
        tempVec    = CartesianPointingVector(phi_ctr_rad,theta_ctr_rad + dOmega);
      
        for iTest = 1:numTest
            % ---- Rotate temp vector by random angle psi. 
            testVec(iTest,:)    = RotateVector(tempVec(iTest,:),earthToGRB,psi(iTest))';
        end
        
        phi_test_rad   = atan2(testVec(:,2), testVec(:,1)); 
        theta_test_rad = acos(testVec(:,3));
        
end

skyPosition_test = [theta_test_rad, ...
                    phi_test_rad, ...
                    zeros(length(theta_test_rad),1), ...
                    zeros(length(phi_test_rad),1)];

% ---- Calculate time-delays between sites and Earth's centre for these
%      sky positions.
deltaT_test = computeTimeShifts( sites, skyPosition_test );



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Calculate inter-site delays.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nSites = length(sites);
iPair = 1;

delay_search = [];
delay_test   = [];
for iSite1 = 1:nSites-1
   for iSite2 = iSite1+1:nSites
      pairName{iPair} = [sites{iSite1} sites{iSite2}];

      delay_search = [delay_search, ... 
         deltaT_search(:,iSite2) - deltaT_search(:,iSite1)];
      delay_test = [delay_test, ...
         deltaT_test(:,iSite2) - deltaT_test(:,iSite1)];

      iPair = iPair + 1;
   end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Perform test.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

passFlag       = zeros(numTest,numSearch);
passFlagSearch = zeros(numSearch,numTest);
numFound       = zeros(numSearch,1);

% ---- Figure out max possible score.
maxScore = 0;
for iPair = 1:length(pairName)
   maxScore = maxScore + 2^(iPair-1);
end


% ---- Loop over test sky positions.
for iTest = 1:numTest

   % ---- Loop over site pairs.
   for iPair = 1:length(pairName)

      passFlag(iTest,:) = passFlag(iTest,:) + 2^(iPair-1) * ...
         ((delay_search(:,iPair)+delay_tol_sec >= delay_test(iTest,iPair)) & ...
         (delay_search(:,iPair)-delay_tol_sec <= delay_test(iTest,iPair)))'; 

   end % -- Loop over site pairs.
   
end % -- Loop over test points.



% ---- Loop over sky positions to search.
for iSearch = 1:numSearch
   
   % ---- Loop over site pairs.
   for iPair = 1:length(pairName)

      passFlagSearch(iSearch,:) = passFlagSearch(iSearch,:) + 2^(iPair-1) * ...
         ((delay_search(iSearch,iPair)+delay_tol_sec >= delay_test(:,iPair)) & ...
          (delay_search(iSearch,iPair)-delay_tol_sec <= delay_test(:,iPair)))'; 

   end % -- Loop over site pairs.
    
   numFound(iSearch) = sum( passFlagSearch(iSearch,:) == maxScore);
   
end % -- Loop over search points.

probabilities_new = numFound / sum(numFound);
max(probabilities_new)
min(probabilities_new)

figure()
plot(probabilities,probabilities_new,'b+');
xlabel('Gaussian pdf');
ylabel('measured probabilties');

% ---- Maximise over sky positions searched.
maxPassFlag = max(passFlag,[],2);

efficiency = sum(maxPassFlag==maxScore) / length(maxPassFlag);
disp(['Efficiency = ' num2str(efficiency)])

iMiss = (maxPassFlag < maxScore);
theta_missed_rad = theta_test_rad(iMiss);    
phi_missed_rad   = phi_test_rad(iMiss);    
delay_missed     = delay_test(iMiss,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Make plots.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- After calculating ra we ensure it lies between 0 and 360 degrees.
[ra_test_deg, dec_test_deg] = earthtoradec(phi_test_rad,theta_test_rad,gps);
ra_test_deg = ra_test_deg - floor(ra_test_deg/360) * 360;

[ra_missed_deg, dec_missed_deg] = earthtoradec(phi_missed_rad,theta_missed_rad,gps);
ra_missed_deg = ra_missed_deg - floor(ra_missed_deg/360) * 360;

if verbose == 1;

figure()
clf;
plot(ra_test_deg,dec_test_deg,'g+','MarkerSize',5)
hold on
grid;
%plot(ra_search_deg,dec_search_deg,'ro')
scatter(ra_search_deg,dec_search_deg,80,probabilities_new);
colorbar;
plot(ra_ctr_deg,dec_ctr_deg,'rp','MarkerSize',20)
plot(ra_missed_deg,dec_missed_deg,'kx','MarkerSize',10)
legend(['test: ' num2str(length(ra_test_deg))],...
       ['search: ' num2str(length(ra_search_deg))],...
       'GRB estimate',...
       ['missed: ' num2str(length(ra_missed_deg))])
title(['efficiency = ' num2str(efficiency*100) '%' ])
xlabel('ra');
ylabel('dec');
axis square
hold off;

if length(sites) == 3
   figure()
   clf;

   subplot(1,2,1)
   plot(delay_test(:,1),delay_test(:,2),'g+','MarkerSize',5)
   hold on;
   grid;
   plot(delay_search(:,1),delay_search(:,2),'ro')
   plot(delay_missed(:,1),delay_missed(:,2),'kx','MarkerSize',10)
   legend(['test: ' num2str(length(ra_test_deg))],...
          ['search: ' num2str(length(ra_search_deg))],...
          ['missed: ' num2str(length(ra_missed_deg))])
   title(['efficiency = ' num2str(efficiency*100) '%' ])
   xlabel(['delay between ' pairName{1}]);
   ylabel(['delay between ' pairName{2}]);

   subplot(1,2,2)
   plot(delay_test(:,1),delay_test(:,3),'g+','MarkerSize',5)
   hold on;
   grid;
   plot(delay_search(:,1),delay_search(:,3),'ro')
   plot(delay_missed(:,1),delay_missed(:,3),'kx','MarkerSize',10)
   legend(['test: ' num2str(length(ra_test_deg))],...
          ['search: ' num2str(length(ra_search_deg))],...
          ['missed: ' num2str(length(ra_missed_deg))])
   title(['efficiency = ' num2str(efficiency*100) '%' ])
   xlabel(['delay between ' pairName{1}]);
   ylabel(['delay between ' pairName{3}]);

   figure()
   clf;

   %plot3(delay_test(:,1),delay_test(:,2),delay_test(:,3),'g+','MarkerSize',5)
   hold on;
   grid;
   plot3(delay_search(:,1),delay_search(:,2),delay_search(:,3),'ro')
   plot3(delay_missed(:,1),delay_missed(:,2),delay_missed(:,3),'kx','MarkerSize',10)
   legend(['test: ' num2str(length(ra_test_deg))],...
          ['search: ' num2str(length(ra_search_deg))],...
          ['missed: ' num2str(length(ra_missed_deg))])
   title(['efficiency = ' num2str(efficiency*100) '%' ])
   xlabel(['delay between ' pairName{1}]);
   ylabel(['delay between ' pairName{2}]);
   zlabel(['delay between ' pairName{3}]);

end
end
