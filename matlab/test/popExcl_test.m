% assumes variables as saved by xbatchwebsimplesummary.m 
% uses effCurve, injScale, nominalDist, nGRBs

nMC = 1e4;

iWave = 2;

% --- choose points along the fit of the real curve
Npoints = 1000;
Rset = logspace(0.7,2.3,Npoints); % in Mpc
Zset = dist2z(Rset,'linear');
Fset = ((Zset/4.2e-2).^2.7+2.3/153).*(0.8+0.4*rand(1,Npoints));

% -- reshape to vector
Fset = Fset(:);
Rset = Rset(:);

if length(Fset) ~= length(Rset)
  error('Provide a consistent number of (F,R) pairs')
end

for iSet = 1:length(Fset)
  F = Fset(iSet);
  R = Rset(iSet);
  maskLocal = rand(nGRBs,nMC)<F;
  dist = R*rand(nGRBs,nMC).^(1/3); % uniform in volume up to R
  % ---- find efficiency for each distance (GRB and trial changing)
  for iGRB = 1:nGRBs
    fixedDistEff(iGRB,:) = ...
        interp1([-10 squeeze(log10(injScale(iGRB,iWave,:)))' 10],...
                [0 squeeze(effCurve(iGRB,iWave,:))' max(squeeze(effCurve(iGRB,iWave,:)))],...
                log10(nominalDist(iWave)./dist(iGRB,:)));
  end
  % ---- Set non local GRBs to 0 detection efficiency
  fixedDistEff(not(maskLocal)) = 0;
  % ---- See in which GRB we have found an event
  aboveInAny = any(fixedDistEff > rand(nGRBs,nMC),1);
  anyEff(iSet) = sum(aboveInAny)/nMC;
end
  
