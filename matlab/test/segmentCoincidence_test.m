% unit test which checks the functions that look at intersection between
% DQ segments and triggers work correctly 
% The two functions tested are Coincidence2.m and fastcoincidence2.cpp

Nseg = 1e3;
Nevt = 1e4;

X = randperm(Nseg)';
dX = rand(Nseg,1);
Y = rand(Nevt,1)*Nseg;
dY = rand(Nevt,1);

coinc = Coincidence2(X, dX, Y, dY);
fastcoinc = fastcoincidence2([X dX],[Y dY]);

if size(fastcoinc,1) ~= size(coinc,1) || sum(sum(abs(fastcoinc-coinc(:,[5 6]))))
  disp('Test failed, the two functions disagree')
else
  disp('Test passed, the two functions agree')
  disp(['Found ' num2str(size(fastcoinc,1)) ' coincidences'])
  disp(['Killed ' num2str(length(unique(fastcoinc(:,2)))) ...
        ' events out of ' num2str(Nevt)])
end

disp('Check that error catching is done correctly')
X = randperm(Nseg)';
dX = 2*rand(Nseg,1);
fastcoinc = fastcoincidence2([X dX],[Y dY]);
