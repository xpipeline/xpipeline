% $Id$

% baseDir = '/Users/psutton/Documents/xpipeline/branches/linear/test/';
baseDir = './';

% file = {'X-SN2007gr-mueller5-Log.mat', 'X-SN2007gr-numerical3-Log.mat', ...
%         'X-SN2007gr-piro5-Log.mat', 'X-SN2007gr-rotbar5-Log.mat', ...
%         'X-SN2007gr-SGel2-Log.mat', 'X-SN2007gr-SGlin2-Log.mat'};

% file = {'X-SN2008ax-mueller4-Log.mat', 'X-SN2008ax-numerical3-Log.mat', ...
%         'X-SN2008ax-piro5-Log.mat', 'X-SN2008ax-rotbar5-Log.mat', ...
%         'X-SN2008ax-SGel2-Log.mat', 'X-SN2008ax-SGlin2-Log.mat'};
% file = {'X-SN2008ax-mueller_long-Log.mat', 'X-SN2008ax-numerical_long-Log.mat', ...
%         'X-SN2008ax-piro_long-Log.mat', 'X-SN2008ax-rotbar_long-Log.mat', ...
%         'X-SN2008ax-SGel_long-Log.mat', 'X-SN2008ax-SGlin_long-Log.mat'};

% file = {'X-SN2008bk-mueller5-Log.mat', 'X-SN2008bk-numerical5-Log.mat', ...
%         'X-SN2008bk-piro5-Log.mat', 'X-SN2008bk-rotbar5-Log.mat', ...
%         'X-SN2008bk-SGel2-Log.mat', 'X-SN2008bk-SGlin2-Log.mat'};
% file = {'X-SN2008bk-mueller_long-Log.mat', 'X-SN2008bk-numerical_long-Log.mat', ...
%         'X-SN2008bk-piro_long-Log.mat', 'X-SN2008bk-rotbar_long-Log.mat', ...
%         'X-SN2008bk-SGel_long-Log.mat', 'X-SN2008bk-SGlin_long-Log.mat'};

% file = {'X-SN2011dh-mueller4-Log.mat','X-SN2011dh-numerical3-Log.mat',...
%         'X-SN2011dh-piro5-Log.mat','X-SN2011dh-rotbar5-Log.mat', ...
%         'X-SN2011dh-SGel2-Log.mat','X-SN2011dh-SGlin2-Log.mat'};
% file = {'X-SN2011dh-SGel_long-Log.mat','X-SN2011dh-mueller_long-Log.mat', ...
%         'X-SN2011dh-piro_long-Log.mat','X-SN2011dh-SGlin_long-Log.mat', ...
%         'X-SN2011dh-numerical_long-Log.mat','X-SN2011dh-rotbar_long-Log.mat'};
file = {'X-SN2011dh-numerical_long-Log.mat'};

for ifile = 1:length(file)

    % ---- Load matlab comparison file.
    load([baseDir file{ifile}]);
    basefile = file{ifile}(1:end-4);

    % ---- Diagnostic info: antenna responses and injection name.
    hp = zeros(length(injectionType),1);
    hc = zeros(length(injectionType),1);
    name = cell(length(injectionType),1);
    for ii = 1:length(injectionType)
        z = tildedelimstr2numorcell(injectionParam{ii});
        hp(ii,1) = z{1};
        hc(ii,1) = z{2};
        name{ii,1} = z{3};
    end
        
    % ---- Make plots.
    figure; set(gca,'fontsize',20)
    pos = get(gcf,'position');
    set(gcf,'position',[pos(1:2) 1.5*pos(3:4)]);
    subplot(2,2,1)
    k = find(~isnan(ind)); %-- only plot tested injections
    hist(maxcc(k))
    grid on
    title(basefile)
    xlabel('max(|cc|)');
    ylabel('count');
    legend(['median = ' num2str(median(maxcc(k)))],2)
    k2 = find(ind>dur*fs/2);  %-- wrap delay to negative values
    inds = ind-1;
    inds(k2) = inds(k2)-dur*fs;
    %
    subplot(2,2,3)
    if length(unique(inds(k)))==1
        hist(inds(k))
    else
        hist(inds(k),[min(inds):1:max(inds)])
    end
    grid on
    xlabel('offset (samples)');
    ylabel('count');
    %
    subplot(2,2,2)
    % hist(maxferr(k))
    % title(basefile)
    % grid on
    % xlabel('min(|fractional error|^2)');
    % ylabel('count');
    % legend(['median = ' num2str(median(maxferr(k)))],1)
    k2 = find(maxferrind>dur*fs/2);
    inds = -(maxferrind-1);  %-- extra -1 because MDC_to_X reverses order of arguments in second call to innerproduct() ... whoops!
    inds(k2) = inds(k2)-dur*fs;
    [Fp Fc] = ComputeAntennaResponse(injectionPhi,injectionTheta, injectionPsi,'G1');
    Frms = ((Fp.*hp).^2+(Fc.*hc).^2).^0.5;
    loglog(Frms(k),maxferr(k),'.')
    grid on; hold on
    xlabel('(Fp.*hp).^2+(Fc.*hc).^2).^{0.5}')
    ylabel('min(|fractional error|^2)');
    title(basefile)
    legend(['median = ' num2str(median(maxferr(k)))],4)
    %
    subplot(2,2,4)
    if length(unique(inds(k)))==1
        hist(inds(k))
    else
        hist(inds(k),[min(inds):1:max(inds)])
    end
    grid on
    xlabel('offset (samples)');
    ylabel('count');
    
    % ---- Save plots.
    saveas(gcf,[basefile '.png'],'png');

end

% % ---- Helpful code snippet for debugging Mueller waveforms.
% clear z
% idx = [];
% N = length(name)
% for ii=1:N
% z = strfind(name{ii},'L15');
%     if ~isempty(z)
%         idx = [idx ii];
%     end
% end
% length(idx)
% idxL15 = idx;
% 
% idx = [];
% N = length(name)
% for ii=1:N
% z = strfind(name{ii},'W15');
%     if ~isempty(z)
%         idx = [idx ii];
%     end
% end
% length(idx)
% idxW15 = idx;
% 
% idx = [];
% N = length(name)
% for ii=1:N
% z = strfind(name{ii},'N20');
%     if ~isempty(z)
%         idx = [idx ii];
%     end
% end
% length(idx)
% idxN20 = idx;
