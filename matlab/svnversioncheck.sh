#!/bin/bash

# creates executbale which returns svnversion of archive
# (c) 2008 Gareth Jones <gareth.jones@astro.cf.ac.uk>
# Licensed under GPL v3 or later

usage() {
    echo "Usage: $(basename $0) REPORT_VERSION_SCRIPT"
}

rm svnversion.txt
rm report-svnversion.sh
svn info . | gawk -F '/' 'BEGIN{ORS=""}/URL/{print $6 "/" $7 " "}' > svnversion.txt
svnversion . >> svnversion.txt
echo 'svnversion: ' `cat svnversion.txt`
if [ `gawk '{if( $2 ~ /[^[:digit:]]/) print $2}' svnversion.txt` ]
	then echo WARNING: svn version is mixed, this is not ideal for analysis runs
	echo WARNING: using \"svn update\" to update the svn archive
 fi

echo 'echo ' `cat svnversion.txt` > report-svnversion.sh
chmod +x report-svnversion.sh

