This directory contains input files that allow you to verify that your compiled 
copy of xdetection works. The input files are

  channels.txt
  events.txt
  framecache.txt
  gaussian.txt
  parameters.txt

See https://trac.ligo.caltech.edu/xpipeline/wiki/Documentation for details on 
these files.

You can run the test job on the command line as follows:

  xdetection parameters.txt 0 . 1

(Be careful not to include the slash “/“ on the end of the output directory — 
this will cause the job to fail!) You’ll see job status information output to 
the screen. If successful, the final message will be of the form

  writing results...
  18 seconds elapsed
  Pipeline ends
  2016-07-28 07:16:16

and you’ll find the output file results_0_1.mat in your directory.

The test job included a loud Gaussian injection into simulated data at GPS time 
700000060. To see it you can examine the results_0_1.mat file in matlab. Some 
example commands are below.

The output clusters are stored in the variable likelihoodMapCell. This command 
finds the cluster with the largest number of pixels (which should be the 
injection):

>> [VAL,IDX] = max(likelihoodMapCell{1}{1}(:,7))
VAL =
    85
IDX =
   102

The first three columns are the start, peak, and end times:
>> round(likelihoodMapCell{1}{1}(IDX,1:3))
ans =
   700000060   700000060   700000060

The next three are the lowest, peak, and highest frequency, followed by the 
number of pixels:
>> round(likelihoodMapCell{1}{1}(IDX,4:7))
ans =
    32   177   672    85

The remaining columns are the likelihoods:
>> likelihoodType
likelihoodType =
    'standard'
    'nullenergy'
    'nullinc'
    'skypositiontheta'
    'skypositionphi'

>> round(likelihoodMapCell{1}{1}(IDX,8:10))
ans =
       93561        2380       26018

>> likelihoodMapCell{1}{1}(IDX,11:12)
ans =
    0.1532   -2.8798


