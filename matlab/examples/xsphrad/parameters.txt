analysisTimes:1
blockTime:128
channelFileName:channels.txt
eventFileName:events.txt
frameCacheFile:framecache.txt
injectionFileName:gaussian.txt
injectionScale:10
sphradParameterFile:sphradparameters.txt
likelihoodtype:clusterenergy,standardenergy,standardinc,plusenergy,plusinc,crossenergy,crossinc,nullenergy,nullinc,circenergy,circinc,skypositiontheta,skypositionphi
maximumFrequency:1024
minimumFrequency:64
offsetFraction:0.5
outputType:sphrad
sampleFrequency:2048
skyCoordinateSystem:earthfixed
skyPositionList:[2]
makesimulatednoise:LIGO
keepsphradtextfiles:1
verboseFlag:1
