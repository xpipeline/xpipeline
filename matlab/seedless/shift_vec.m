function aligned_vector = shift_vec(vector, target_freqs, fpeak, fcenter)

% Frequency shift
fshift = fcenter / fpeak;
false_freqs = target_freqs * fshift;
aligned_vector = interp1(false_freqs, vector, target_freqs);


