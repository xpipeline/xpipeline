function [max_cluster,t,f] = ...
   seedless(likelihoodMap,analysisTime,sampleFrequency,frequencyBand,outputDirectory,seedlessParams)

tic;

integrationLength = analysisTime*sampleFrequency;

tfmap = likelihoodMap;

% ---- Nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% ---- Vector of one-sided frequencies
oneSidedFrequencies = nyquistFrequency * ( 0 : integrationLength / 2 ) / ...
                     ( integrationLength / 2 );

% ---- Frequencies in desired analysis band
frequencyIndex = find( (oneSidedFrequencies>=frequencyBand(1)) & ...
    (oneSidedFrequencies<=frequencyBand(2)) );
inbandFrequencies = oneSidedFrequencies(frequencyIndex);

% ---- Number of in-band frequency bins
numberOfFrequencyBins = length(inbandFrequencies);

t = (1:size(tfmap,2))*analysisTime/2;
f = inbandFrequencies;

params = [];
params = stochtrackDefaults(params);
params.doStochtrack = true;

lines = textread(seedlessParams,'%s','delimiter','\n');
for i = 1:length(lines)
   line = lines{i};
   lineSplit = regexp(line,':','split');
   if strcmp(lineSplit{1},'doGPU')
      params.doGPU = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'doParallel')
      params.doParallel = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'T')
      params.stochtrack.T = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'F')
      params.stochtrack.F = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'mindur')
      params.stochtrack.mindur = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'doPCA')
      params.stochtrack.doPCA = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'pca_catalogfile')
      params.stochtrack.pca.catalogfile = lineSplit{2};
   elseif strcmp(lineSplit{1},'pca_type')
      params.stochtrack.pca.type = lineSplit{2};
   elseif strcmp(lineSplit{1},'doExponential')
      params.stochtrack.doExponential = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'doBezier')
      params.stochtrack.doBezier = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'doECBC')
      params.stochtrack.doECBC = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'doCBC')
      params.stochtrack.doCBC = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'doRModes')
      params.stochtrack.doRModes = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1},'savePlots')
      params.savePlots = str2num(lineSplit{2});
   elseif strcmp(lineSplit{1}, 'plotDir')
      outputDirectory = lineSplit{2};
   elseif strcmp(lineSplit{1},'norm')
      params.stochtrack.norm = lineSplit{2};
   end
end

% stochtrack normalization for March 7 mwc version of stochtrack.m, imported
% to this directory on May 3
params.stochtrack.doSeed = 1;
params.stochtrack.seed = 1;
params.stochtrack.vlong = 0;
params.gpu.precision = 'double';
params.ifo1 = 'H1';
params.ifo2 = 'L1';
params.stochtrack.stochsky = 0;
params.stochtrack.cbc.allsky = 0;
params.stochtrack.doRamp = 0;
params.stochtrack.saveMat = params.savePlots;
params.segmentDuration = t(2) - t(1);
params.stochtrack.extra_pixels = 41;

%params.savePlots = 1;
%params.plotdir = 'plots';
%outputDirectory = 'plots/';

%tmin = 1.6; tmax = 2;
%indexes = find(t > tmin & t <= tmax);
%t = t(indexes);
%tfmap = tfmap(:,indexes);

if params.savePlots
   h = figure;
   imagesc(t,f,real(tfmap));
   set(gca,'YDir','normal');
   xlabel('Time [s]');
   ylabel('Frequency [Hz]');
   cbar = colorbar;
   ylabel(cbar, 'Likelihood');
   set(h,'Units','Inches');
   pos = get(h,'Position');
   set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
   print('-dpng',[outputDirectory '/map.png']);
   print('-depsc2',[outputDirectory '/map.eps']);
   print('-dpdf',[outputDirectory '/map.pdf']);
   close;

   figure;
   imagesc(t,f,real(log10(tfmap)));
   set(gca,'YDir','normal');
   xlabel('Time [s]');
   ylabel('Frequency [Hz]');
   print('-dpng',[outputDirectory '/map_nocolorbar.png']);
   print('-depsc2',[outputDirectory '/map_nocolorbar.eps']);
   print('-dpdf',[outputDirectory '/map_nocolorbar.pdf']);
   close;

   allvals = sort(tfmap(:),'descend');
   midval = allvals(round(0.5*length(allvals)));

   tfmap_50 = tfmap;
   tfmap_50(tfmap_50 < midval) = NaN;
   figure;
   imagesc(t,f,real(tfmap_50));
   set(gca,'YDir','normal');
   xlabel('Time [s]');
   ylabel('Frequency [Hz]');
   colorbar;
   print('-dpng',[outputDirectory '/map50.png']);
   print('-depsc2',[outputDirectory '/map50.eps']);
   print('-dpdf',[outputDirectory '/map50.pdf']);
   close;

   allvals = sort(tfmap(:),'descend');
   midval = allvals(round(0.01*length(allvals)));

   tfmap_1 = tfmap;
   tfmap_1(tfmap_1 < midval) = NaN;
   figure;
   imagesc(t,f,real(tfmap_1));
   set(gca,'YDir','normal');
   xlabel('Time [s]');
   ylabel('Frequency [Hz]');
   colorbar;
   print('-dpng',[outputDirectory '/map1.png']);
   print('-depsc2',[outputDirectory '/map1.eps']);
   print('-dpdf',[outputDirectory '/map1.pdf']);
   close;

end

if params.stochtrack.doPCA
   if strcmp(params.stochtrack.pca.type,'PMNS')
      params.stochtrack.pca.catalog = load(params.stochtrack.pca.catalogfile);
      params.stochtrack.pca.min_var1 = 2048;
      params.stochtrack.pca.max_var1 = 4000;
      params.stochtrack.pca.min_var2 = -100;
      params.stochtrack.pca.max_var2 = 100;
      params.stochtrack.pca.num_var1 = 40;
      params.stochtrack.pca.num_var2 = 40;

      %params.stochtrack.pca.min_var1 = 3250;
      %params.stochtrack.pca.max_var1 = 3500;

      %fpeaks = 3254;
      %timefreq_betas = -31.4786;
      %params.stochtrack.pca.min_var1 = fpeaks(1)-1;
      %params.stochtrack.pca.max_var1 = fpeaks(1)+1;
      %params.stochtrack.pca.min_var2 = timefreq_betas(1,1) - 0.001;
      %params.stochtrack.pca.max_var2 = timefreq_betas(1,1) - 0.001;
   elseif strcmp(params.stochtrack.pca.type,'Dimmelmeier') || strcmp(params.stochtrack.pca.type,'Murphy')
      params.stochtrack.pca.catalog = load(params.stochtrack.pca.catalogfile);
      params.stochtrack.pca.min_var1 = 0;
      params.stochtrack.pca.max_var1 = 1;
      params.stochtrack.pca.min_var2 = 0;
      params.stochtrack.pca.max_var2 = 1;
      params.stochtrack.pca.num_var1 = 20;
      params.stochtrack.pca.num_var2 = 20;
   end
end

map.f = f;
map.segstarttime = t;
map.snr = tfmap;
map.sigma = ones(size(map.snr));
map.y = tfmap;
map.z = ones(size(map.snr));

% parallel options
params = initParallel(params);
params.plotdir = outputDirectory;
params.stochtrack.matfile = [outputDirectory '/out'];

params.stochtrack.doTranspose = 0;

if params.stochtrack.doPCA
   % break up into 8s segments (50% overlap)
   tbound = 0:4:t(end);
   %tbound = tbound(1:3);
   %for i = 1:length(tbound)-2
   for i = 1
      tmin = tbound(i); tmax = tbound(i+2);
      indexes = find(t > tmin & t <= tmax);
      map.f = f;
      map.segstarttime = t(indexes);
      map.snr = tfmap(:,indexes);
      map.sigma = ones(size(map.snr));
      map.y = tfmap(:,indexes);
      map.z = ones(size(map.snr));
      max_cluster = stochtrack(map,params);
      snrs(i) = max_cluster.snr_gamma;
      rmapFull = zeros(size(tfmap));
      rmapFull(:,indexes) = max_cluster.rmap;
      max_cluster.rmap = rmapFull;
      rmapHFull = zeros(size(tfmap));
      rmapHFull(:,indexes) = max_cluster.rmapH;
      max_cluster.rmapH = rmapHFull;
      reconMaxFull = zeros(size(tfmap));
      reconMaxFull(:,indexes) = max_cluster.reconMax;
      max_cluster.reconMax = reconMaxFull;
      max_clusters{i} = max_cluster;
   end
   [maxsnr,index] = max(snrs);
   max_cluster = max_clusters{index};
else
   max_cluster = stochtrack(map,params);
end

max_cluster.reconMax = gather(max_cluster.reconMax);
max_cluster.sigma_gamma = gather(max_cluster.sigma_gamma);
max_cluster.y_gamma = gather(max_cluster.y_gamma);

if params.savePlots

   figure;
   imagesc(t,f,real(log10(tfmap)));
   set(gca,'YDir','normal');
   xlabel('Time [s]');
   ylabel('Frequency [Hz]');
   %xlim([1.6 1.8]);
   %ylim([1000 4000]);
   print('-dpng',[outputDirectory '/map_nocolorbar_rmapH.png']);
   print('-depsc2',[outputDirectory '/map_nocolorbar_rmapH.eps']);
   print('-dpdf',[outputDirectory '/map_nocolorbar_rmapH.pdf']);
   close;

   figure;
   rmapH = max_cluster.rmapH;
   rmapH(rmapH == 0) = NaN;
   imagesc(t,f,rmapH);
   set(gca,'YDir','normal');
   xlabel('Time [s]');
   ylabel('Frequency [Hz]');
   %xlim([1.6 1.8]);
   %ylim([1000 4000]);
   print('-dpng',[outputDirectory '/rmapH.png']);
   print('-depsc2',[outputDirectory '/rmapH.eps']);
   print('-dpdf',[outputDirectory '/rmapH.pdf']);
   close;
end

if params.savePlots
   matFile = [outputDirectory '/data.mat'];
   save(matFile,'max_cluster','map','params');
   save([outputDirectory '/all_variables.mat'])
end

toc;

