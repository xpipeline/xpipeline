function gM = gArray(M, params)
% function gM = gArray(M, params)

if params.doGPU
  gM = gather(M);
else 
  gM = M;
end

return
