function gM = gOnes(a, b, params)
% function gM = gOnes(a, b, params)

if params.doGPU
  gM = gpuArray.ones(a,b,params.gpu.precision);
else 
  gM = ones(a,b);
end

return
