function [yvals,xvals,H,ttvals,track_data] = stochtrack_tracks(map, params, aa);
% function max_cluster = run_stochtrack(map, params)
%
% INPUT:
%    map  - STAMP map struct.
%    params - struct containing parameters for STAMP search.
%
% OUTPUT:
%    max_cluster (STRUCT)
%        max_cluster.nPix -> number of pixels in the largest cluster
%        max_cluster.snr_gamma -> SNR of the largest cluster
%        max_cluster.y_gamma -> Y (GW estimator) of largest cluster
%        max_cluster.sigma_gamma -> sigma of the above estimator
%        max_cluster.reconMax -> 2D array containing weight numbers for
%                                pixels contained the largest cluster and 0's
%                                at the location of other pixels
% Written by E Thrane and M Coughlin
% Contact: ethrane@ligo.caltech.edu, michael.coughlin@ligo.org
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% map is MxN: M is the # of frequency bins and N is the number of segments
[M,N] = size(map.snr);

% define maxdur to be N (the map time dimension) unless otherwise specified
try
   params.stochtrack.maxdur;
catch
   params.stochtrack.maxdur = N;
end

% define maxband to be M (the same as the frequency dimension unless otherwise
% specified
try
  params.stochtrack.maxband;
catch
  params.stochtrack.maxband = M;
end

% stochtrack parameters-------------------------------------------------------
% number of trials
T = params.stochtrack.T;
% number of trials in for loop
F = params.stochtrack.F;
% minimum track duration (IN PIXELS)
mindur = params.stochtrack.mindur;
% maximum track duration (IN PIXELS)
maxdur = params.stochtrack.maxdur;
%-----------------------------------------------------------------------------

% initialize yvals array
yvals = gZeros(N,T,params);
xvals = gColon(1,1,N,params);
ttvals = gArray(map.segstarttime,params); 
H = gOnes(N,T,params);
d_foft = gOnes(N,T,params);
track_data = [];

if params.stochtrack.doCBC

  LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

  % Generate masses and associated variables
  %m1 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;
  %m2 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;

  %ms = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %    + params.stochtrack.cbc.min_mass;
  ms = gLinspace(params.stochtrack.cbc.min_mass,params.stochtrack.cbc.max_mass,F,params)'; 

  % choose mass based on loop value and set m1 and m2 equal to one another
  thism = ms(aa);
  m1 = thism * gOnes(T,1,params); 
  m2 = m1;

  track_data.m1 = m1;
  track_data.m2 = m2;

  % Calculate mass ratio, chirp mass, and total mass
  eta = (m1.*m2)./((m1+m2).^2);
  % Convert mc from solar masses to seconds
  mc = (m1 + m2) .* (eta.^(3/5)) * LAL_MTSUN_SI;
  Mtot = (m1 + m2) .* LAL_MTSUN_SI;

  % Set these variables to map size
  t = repmat(gColon(1,1,N,params)' * (params.segmentDuration/2), 1, size(yvals,2));
  eta = repmat(eta, 1, size(yvals,1))';
  mc = repmat(mc, 1, size(yvals,1))';
  tc = repmat((params.segmentDuration/2) * N*gOnes(T,1,params), 1, size(yvals,1))';
  Mtot = repmat(Mtot, 1, size(yvals,1))';

  % Make end of track slightly before coalescence time (so that f(t) does not go to infinity there)
  t(end,:) = N* (params.segmentDuration/2) - (params.segmentDuration/4);

  % Compute reduced time variable
  Tp = t .* (eta.^(8/5)) ./ (5*mc);
  Tc = tc .* (eta.^(8/5)) ./ (5*mc);
  TcminusT = Tc-Tp;
  TcminusT(TcminusT < 0) = 0;

  % Compute h vs. time
  hoft = (TcminusT.^(-1/4)).^2;
  hoft(isinf(hoft)) = 0;
  hoft(end-10:end,:) = hoft(end-10,1);
  sumhoft = sum(hoft);
  sumhoft = repmat(sumhoft', 1, size(yvals,1))';
  H = hoft ./ sumhoft;

  % Compute frequency vs. time
  %foft = (eta.^(3/5) ./ (8*pi*mc)) .* (TcminusT).^(-3/8) .* (1 + ...
  %   (((743/2688) + (11/32).*eta).*(TcminusT).^(-1/4)) - ...
  %   ((3*pi/10)*(TcminusT).^(-3/8)) + ((1855099/14450688) + ...
  %   (56975/258048).*eta + (371/2048).*(eta.^2)) .* (TcminusT).^(-1/2));

  % Equation A1 from http://arxiv.org/abs/0803.0226 (3.5 PN order, no spin terms)

  eg=0.5772156649015328606;
  tauoft = (eta./(5*Mtot)) .* (tc-t);
  tau0 = (eta./(5*Mtot)) .* (tc);

  p0 = 1; p1 = 0;
  p2 = (743/2688) + (11/32)*eta; p3 = -(3/10) * pi;
  p4 = (1855099/14450688) + (56975/258048)*eta + (371/2048)*eta.^2;
  p5 = (-(7729/21504) + (3/256)*eta)*pi;
  p6 = -(720817631400877/288412611379200) + (107/280)*eg + (53/200)*pi^2 - (107/2240)*log(tauoft/256) + ...
    ((25302017977/4161798144) - (451/2048)*pi^2)*eta - (30913/1835008)*eta.^2 + (235925/1769472)*eta.^3;
  p7 = (-(188516689/433520640) - (28099/57344)*eta + (122659/1290240)*eta.^2)*pi;

  phioft = -(1./(4*Mtot)) .* ((p0.*tauoft.^(-(3+0)/8)) + (p1.*tauoft.^(-(3+1)/8)) + (p2.*tauoft.^(-(3+2)/8)) + ...
    (p3.*tauoft.^(-(3+3)/8)) + (p4.*tauoft.^(-(3+4)/8)) + (p5.*tauoft.^(-(3+5)/8)) + ...
    (p6.*tauoft.^(-(3+6)/8)) + (p7.*tauoft.^(-(3+7)/8)));

  foft = phioft ./ (-2*pi);

  d_foft(2:end,:) = foft(2:end,:) - foft(1:end-1,:);
  d_foft(d_foft<1) = 1;
  d_foft = round(d_foft);

  response = interp1(map.f,map.filter_response,foft(:,1));
  response(isnan(response)) = 1;
  response = repmat(response, 1, size(yvals,2));
  H = H.*response;
  sumhoft = sum(H);
  sumhoft = repmat(sumhoft', 1, size(H,1))';
  H = H ./ sumhoft;

  % Convert frequency to map indices
  foft = foft - params.fmin + 1;

  % Do DF calculation (adds pixels to end of track associated with extra pixels track passes
  % through in a given segment
  if params.stochtrack.doDF
     this_d_foft = d_foft(:,1);
     % Choose columns that have df>1 (as they need pixels added)
     xvals_extra = []; yvals_extra = []; H_extra = [];  tt_extra = [];
     indexes = find(this_d_foft > 1);
     
     % Add extra pixels for each column that needs it
     for kk = 1:length(indexes)
        bb = indexes(kk);
        cc = gColon(2,1,this_d_foft(bb),params);
        ii = gOnes(1,this_d_foft(bb)-1,params);

        % Add extra pixels to the end
        xvals_extra = [xvals_extra xvals(bb)*ii];
        yvals_extra = [yvals_extra foft(bb,1)+cc-1];
        H_extra = [H_extra H(bb,1)*ii];
        tt_extra = [tt_extra ttvals(bb)*ii];
     end

     % Add arrays to the end of original
     xvals_extra = xvals_extra;
     yvals_extra = repmat(yvals_extra', 1, size(yvals,2))';
     H_extra = repmat(H_extra', 1, size(yvals,2))';

     xvals = [xvals xvals_extra];
     foft = [foft; yvals_extra'];
     H = [H; H_extra'];
     ttvals = [ttvals tt_extra];

     % Sort by time
     [xvals,index] = sort(xvals,'ascend');
     foft = foft(index,:);
     H = H(index,:);
     ttvals = ttvals(index);

     % Set expanded arrays as ones used
     foft(foft>M) = NaN;
  end

  yvals = foft;

  % Choose coalescense times: the smallest possible stop time is given by 
  % minstop.  The largest possible stop time is N = the last pixel in the map.
  if T < size(yvals,1)-params.stochtrack.cbc.minstop
     nstops = (N-params.stochtrack.cbc.minstop).*gRand(T,1,params) + ...
       params.stochtrack.cbc.minstop;
     nstops = round(nstops);
  else
     nstops = gColon(params.stochtrack.cbc.minstop,1,N,params);
     nstops = padarray(nstops',T-length(nstops),N,'post');
  end
  track_data.nstops = nstops;

  nstops = repmat(nstops, 1, size(yvals,1))';

  % Move waveform to correct starting index
  xvals = repmat(xvals', 1, size(yvals,2));
  xvals = mod(xvals + nstops - 1,N) + 1;

  % remove values below 0
  H(yvals<0) = 0;
  yvals(yvals<0) = NaN;

  % Remove any portion of the waveform after the maximum
  xvals_max_index = xvals(end,:);
  xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
  H(xvals > xvals_max_index) = 0;
  yvals(xvals > xvals_max_index) = NaN;

elseif params.stochtrack.doECBC

  LAL_MTSUN_SI = 4.9254923218988636432342917247829673e-6; %/**< Geometrized solar mass, s. = LAL_MSUN_SI / LAL_MPL_SI * LAL_TPL_SI */

  % Generate masses and associated variables
  %m1 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;
  %m2 = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %   + params.stochtrack.cbc.min_mass;

  %ms = (params.stochtrack.cbc.max_mass-params.stochtrack.cbc.min_mass).*gRand(T,1,params) ...
  %    + params.stochtrack.cbc.min_mass;
  ms = gLinspace(params.stochtrack.cbc.min_mass,params.stochtrack.cbc.max_mass,F,params)';

  % choose mass based on loop value and set m1 and m2 equal to one another
  thism = ms(aa);
  m1 = thism * gOnes(T,1,params);
  m2 = m1;

  % Calculate mass ratio, chirp mass, and total mass
  eta = (m1.*m2)./((m1+m2).^2);
  % Convert mc from solar masses to seconds
  mc = (m1 + m2) .* (eta.^(3/5)) * LAL_MTSUN_SI;
  Mtot = (m1 + m2) .* LAL_MTSUN_SI;

  % Set these variables to map size
  t = repmat(gColon(1,1,N,params)' * (params.segmentDuration/2), 1, size(yvals,2));
  eta = repmat(eta, 1, size(yvals,1))';
  mc = repmat(mc, 1, size(yvals,1))';
  tc = repmat((params.segmentDuration/2) * N*gOnes(T,1,params), 1, size(yvals,1))';
  Mtot = repmat(Mtot, 1, size(yvals,1))';

  % Make end of track slightly before coalescence time (so that f(t) does not go to infinity there)
  t(end,:) = N* (params.segmentDuration/2) - (params.segmentDuration/4);

  % Compute reduced time variable
  Tp = t .* (eta.^(8/5)) ./ (5*mc);
  Tc = tc .* (eta.^(8/5)) ./ (5*mc);
  TcminusT = Tc-Tp;
  TcminusT(TcminusT < 0) = 0;

  % Compute frequency vs. time
  f0 = (2*pi*(params.fmin/2)*Mtot).^(2/3);

  eccentricities = linspace(params.stochtrack.cbc.min_eccentricity,...
    params.stochtrack.cbc.max_eccentricity,params.stochtrack.cbc.n_eccentricity);

  xvalsAll = gZeros(params.stochtrack.max_pixels,length(eccentricities)*T,params);
  yvalsAll = gZeros(params.stochtrack.max_pixels,length(eccentricities)*T,params);
  HAll = gZeros(params.stochtrack.max_pixels,length(eccentricities)*T,params);
  nstopsAll = gZeros(1,length(eccentricities)*T,params);
  eccAll = gZeros(1,length(eccentricities)*T,params);

  track_data.m1 = thism*gOnes(1,length(eccentricities)*T,params);
  track_data.m2 = thism*gOnes(1,length(eccentricities)*T,params);

  for ee = 1:length(eccentricities)

    ecc = eccentricities(ee);
    if ecc == 0
       ecc = 1e-5;
    end

    xvals = gColon(1,1,N,params);

    tau = 9.829*3.15569e7*1e6*(1/((params.fmin/2)*3600)).^(8/3) *(1./(m1+m2)).^(2/3) .* (1./((m1.*m2)./(m1+m2))).*(1-ecc).^(7/2);
    C0 = f0*((1 - ecc^2)/((ecc^(12/19))*(304 + 121*ecc).^(870/2299)))^(-1);
    A=(5*(31046))./(172 * 2^(2173/2299) * 19^(1118/2299) * eta .* C0.^4);
    B = ((ecc).^(48/19)).*A.*Mtot;
    BminusT = t(end) - t + (params.segmentDuration/4);
    eccoft = ((BminusT)./(A.*Mtot)).^(19/48);
    %eccoft(end,:) = 1e-10;
    X = C0.*((1 - eccoft.^2)./((eccoft.^(12/19)).*(304 + 121.*eccoft.^2).^(870/2299)));
    X(X<0) = 0;
    foft = (X.^(3/2))./(2*pi.*Mtot); 

    % Orbital frequency is 1/2 GW frequency
    foft = 2 * foft;

    d_foft(2:end,:) = foft(2:end,:) - foft(1:end-1,:);
    d_foft(d_foft<1) = 1;
    d_foft = round(d_foft);

    % Compute h vs. time
    hoft = (TcminusT.^(-1/4)).^2;
    hoft(isinf(hoft)) = 0;
    hoft(end-10:end,:) = hoft(end-10,1);
    sumhoft = sum(hoft);
    sumhoft = repmat(sumhoft', 1, N)';
    H = hoft ./ sumhoft;

    response = interp1(map.f,map.filter_response,foft(:,1));
    response(isnan(response)) = 1;
    response = repmat(response, 1, size(yvals,2));
    H = H.*response;
    sumhoft = sum(H);
    sumhoft = repmat(sumhoft', 1, size(H,1))';
    H = H ./ sumhoft;

    % Convert frequency to map indices
    foft = foft - params.fmin + 1;

    % Do DF calculation (adds pixels to end of track associated with extra pixels track passes
    % through in a given segment
    if params.stochtrack.doDF
       this_d_foft = d_foft(:,1);
       % Choose columns that have df>1 (as they need pixels added)
       xvals_extra = []; yvals_extra = []; H_extra = [];  tt_extra = [];
       indexes = find(this_d_foft > 1);

       % Add extra pixels for each column that needs it
       for kk = 1:length(indexes)
          bb = indexes(kk);
          cc = gColon(2,1,this_d_foft(bb),params);
          ii = gOnes(1,this_d_foft(bb)-1,params);

          % Add extra pixels to the end
          xvals_extra = [xvals_extra xvals(bb)*ii];
          yvals_extra = [yvals_extra foft(bb,1)+cc-1];
          H_extra = [H_extra H(bb,1)*ii];
          tt_extra = [tt_extra ttvals(bb)*ii];
       end

       % Add arrays to the end of original
       xvals_extra = xvals_extra;
       yvals_extra = repmat(yvals_extra', 1, size(yvals,2))';
       H_extra = repmat(H_extra', 1, size(yvals,2))';

       xvals = [xvals xvals_extra];
       foft = [foft; yvals_extra'];
       H = [H; H_extra'];
       ttvals = [ttvals tt_extra];

       % Sort by time
       [xvals,index] = sort(xvals,'ascend');
       foft = foft(index,:);
       H = H(index,:);
       ttvals = ttvals(index);

       % Set expanded arrays as ones used
       foft(foft>M) = NaN;
    end

    yvals = foft;

    % Choose coalescense times: the smallest possible stop time is given by
    % minstop.  The largest possible stop time is N = the last pixel in the map.
    if T < size(yvals,1)-params.stochtrack.cbc.minstop
       nstops = (N-params.stochtrack.cbc.minstop).*gRand(T,1,params) + ...
         params.stochtrack.cbc.minstop;
       nstops = round(nstops);
    else
       nstops = gColon(params.stochtrack.cbc.minstop,1,N,params);
       nstops = padarray(nstops',T-length(nstops),N,'post');
    end
    nstopsHold = nstops;

    nstops = repmat(nstops, 1, size(yvals,1))';

    % Move waveform to correct starting index
    xvals = repmat(xvals', 1, size(yvals,2));
    xvals = mod(xvals + nstops - 1,N) + 1;

    % remove values below 0
    H(yvals<0) = 0;
    yvals(yvals<0) = NaN;

    % Remove any portion of the waveform after the maximum
    xvals_max_index = xvals(end,:);
    xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
    H(xvals > xvals_max_index) = 0;
    yvals(xvals > xvals_max_index) = NaN;

    indexes = (ee-1)*T+1:ee*T;
    indexes_2 = params.stochtrack.max_pixels-size(yvals,1)+1:params.stochtrack.max_pixels; 
    xvalsAll(indexes_2,indexes) = xvals; yvalsAll(indexes_2,indexes) = yvals;
    HAll(indexes_2,indexes) = H;
    nstopsAll(indexes) = nstopsHold;
    eccAll(indexes) = ecc;

  end

  xvals = xvalsAll; yvals = yvalsAll; H = HAll;

  track_data.nstops = nstopsAll;
  track_data.eccentricities = eccAll;

elseif params.stochtrack.doRModes

  % Generate masses and associated variables
  %f0 = (params.stochtrack.rmodes.max_f0-params.stochtrack.rmodes.min_f0).*gRand(T,1,params) ...
  %   + params.stochtrack.rmodes.min_f0;
  f0s = gColon(params.stochtrack.rmodes.min_f0,1,params.stochtrack.rmodes.max_f0,params)';

  if F > length(f0s)
     f0s = padarray(f0s,F-length(f0s),params.stochtrack.rmodes.max_f0,'post');
  end

  % choose mass based on loop value and set m1 and m2 equal to one another
  min_alpha = log10(params.stochtrack.rmodes.min_alpha);
  max_alpha = log10(params.stochtrack.rmodes.max_alpha);

  alphas = gLinspace(min_alpha,max_alpha,params.stochtrack.rmodes.nalpha,params)';
  alphas = 10.^alphas;

  thisf0 = f0s(aa);
  f0 = repmat(thisf0, 1, size(yvals,1))';

  xvalsHold = xvals;

  xvalsAll = gZeros(N,length(alphas)*T,params); yvalsAll = gZeros(N,length(alphas)*T,params);
  HAll = gZeros(N,length(alphas)*T,params); alphasAll = gOnes(1,length(alphas)*T,params);

  for bb = 1:length(alphas)
    alpha = alphas(bb);

    alpha = repmat(alpha, 1, size(yvals,1))';
    t = gColon(1,1,N,params)' * (params.segmentDuration/2);

    k=-1.8*alpha.^2*10^-21 ;
    fOwen= 1./(f0.^(-6) - 6.*k.*t).^(1/6);
    I=-(1./(5*k)).*(f0.^(-6)-6*k.*t).^(5/6);        % This is the definite integral of f(t) calculated at t
    int=I+(1./(5*k)).*(f0.^(-6)).^(5/6);           % This is the integral at t minus the integral at 0
    foft = fOwen;

    % Compute h vs. time
    hoft = fOwen.^6;
    hoft(isinf(hoft)) = 0;
    sumhoft = sum(hoft);
    sumhoft = repmat(sumhoft', 1, size(yvals,1))';
    H = hoft ./ sumhoft;
    H = repmat(H, 1, size(yvals,1));

    % Convert frequency to map indices
    foft = foft - params.fmin + 1;
    yvals = foft;
    yvals = repmat(yvals, 1, size(yvals,1));

    % Choose coalescense times: the smallest possible stop time is given by
    % minstop.  The largest possible stop time is N = the last pixel in the map.
    if T < size(yvals,1)
     nstops = N.*gRand(T,1,params);
     nstops = round(nstops);
    else
     nstops = gColon(1,1,N,params);
     nstops = padarray(nstops',T-length(nstops),N,'post');
    end
    track_data.nstops = nstops;

    nstops = repmat(nstops, 1, size(yvals,1))';

    % Move waveform to correct starting index
    xvals = repmat(xvalsHold', 1, size(yvals,1));
    xvals = mod(xvals + nstops - 1,N) + 1;

    % remove values below 0
    H(yvals<0) = 0;
    yvals(yvals<0) = NaN;

    % Remove any portion of the waveform after the maximum
    xvals_max_index = xvals(end,:);
    xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
    H(xvals > xvals_max_index) = 0;
    yvals(xvals > xvals_max_index) = NaN;

    indexes = (bb-1)*T+1:bb*T;
    xvalsAll(:,indexes) = xvals; yvalsAll(:,indexes) = yvals; 
    HAll(:,indexes) = H; alphasAll(indexes) = alphas(bb);

  end

  xvals = xvalsAll; yvals = yvalsAll; H = HAll; alpha = alphasAll;

  track_data.f0 = repmat(thisf0,1,length(alphas)*T);
  track_data.alpha = alpha;

elseif params.stochtrack.doExponential

  % Generate masses and associated variables
  %f0 = (params.stochtrack.rmodes.max_f0-params.stochtrack.rmodes.min_f0).*gRand(T,1,params) ...
  %   + params.stochtrack.rmodes.min_f0;
  f0s = gColon(params.stochtrack.exponential.min_f0,1,params.stochtrack.exponential.max_f0,params)';

  if F > length(f0s)
     f0s = padarray(f0s,F-length(f0s),params.stochtrack.exponential.max_f0,'post');
  end

  % choose mass based on loop value and set m1 and m2 equal to one another
  min_alpha = params.stochtrack.exponential.min_alpha;
  max_alpha = params.stochtrack.exponential.max_alpha;

  alphas = gLinspace(min_alpha,max_alpha,params.stochtrack.exponential.nalpha,params)';
  thisf0 = f0s(aa);
  f0 = repmat(thisf0, 1, size(yvals,1))';

  xvalsHold = xvals;

  xvalsAll = gZeros(N,length(alphas)*T,params); yvalsAll = gZeros(N,length(alphas)*T,params);
  HAll = gZeros(N,length(alphas)*T,params); alphasAll = gOnes(1,length(alphas)*T,params);

  for bb = 1:length(alphas)
    alpha = alphas(bb);

    alpha = repmat(alpha, 1, size(yvals,1))';
    t = gColon(1,1,N,params)' * (params.segmentDuration/2);
    foft = f0 .* t.^alpha;

    % Compute h vs. time
    hoft = ones(size(foft));
    hoft(isinf(hoft)) = 0;
    sumhoft = sum(hoft);
    sumhoft = repmat(sumhoft', 1, size(yvals,1))';
    H = hoft ./ sumhoft;
    H = repmat(H, 1, size(yvals,1));

    % Convert frequency to map indices
    foft = foft - params.fmin + 1;
    yvals = foft;
    yvals = repmat(yvals, 1, size(yvals,1));

    % Choose coalescense times: the smallest possible stop time is given by
    % minstop.  The largest possible stop time is N = the last pixel in the map.
    if T < size(yvals,1)
     nstops = N.*gRand(T,1,params);
     nstops = round(nstops);
    else
     nstops = gColon(1,1,N,params);
     nstops = padarray(nstops',T-length(nstops),N,'post');
    end
    track_data.nstops = nstops;

    nstops = repmat(nstops, 1, size(yvals,1))';

    % Move waveform to correct starting index
    xvals = repmat(xvalsHold', 1, size(yvals,1));
    xvals = mod(xvals + nstops - 1,N) + 1;

    % remove values below 0
    H(yvals<0) = 0;
    yvals(yvals<0) = NaN;

    % Remove any portion of the waveform after the maximum
    xvals_max_index = xvals(end,:);
    xvals_max_index = repmat(xvals_max_index', 1, size(yvals,1))';
    H(xvals < xvals_max_index) = 0;
    yvals(xvals < xvals_max_index) = NaN;

    indexes = (bb-1)*T+1:bb*T;
    xvalsAll(:,indexes) = xvals; yvalsAll(:,indexes) = yvals;
    HAll(:,indexes) = H; alphasAll(indexes) = alphas(bb);

  end

  xvals = xvalsAll; yvals = yvalsAll; H = HAll; alpha = alphasAll;

  track_data.f0 = repmat(thisf0,1,length(alphas)*T);
  track_data.alpha = alpha;

elseif params.stochtrack.doPCA

  xvalsHold = xvals;

  xvalsAll = gZeros(N,T,params); yvalsAll = gZeros(N,T,params);
  HAll = gZeros(N,T,params); f0sAll = gOnes(1,T,params);

  % Get data from catalog
  data = params.stochtrack.pca.catalog.file;

  timefreq_times = data.timefreq_times;
  timefreq_frequencies = data.timefreq_frequencies;
  timefreq_principal_components = data.timefreq_principal_components;

  if strcmp(params.stochtrack.pca.type,'PMNS')
    timefreq_mean = data.timefreq_mean; timefreq_scales = data.timefreq_scales;
  end

  % Reconstruct magnitude tf-map
  if strcmp(params.stochtrack.pca.type,'Dimmelmeier') || strcmp(params.stochtrack.pca.type,'Murphy')
    var1s = gLinspace(params.stochtrack.pca.min_var1,params.stochtrack.pca.max_var1,...
      params.stochtrack.pca.num_var1,params)';
    var2s = gLinspace(params.stochtrack.pca.min_var2,params.stochtrack.pca.max_var2,...
      params.stochtrack.pca.num_var2,params)';
    [X,Y] = meshgrid(var1s,var2s);
    X = X(:); Y = Y(:);

    beta1 = X(aa); beta2 = Y(aa);

    %beta1 = (params.stochtrack.pca.max_var1-params.stochtrack.pca.min_var1)* ...
    %  rand() + params.stochtrack.pca.min_var1;
    %beta2 = (params.stochtrack.pca.max_var2-params.stochtrack.pca.min_var2)* ...
    %  rand() + params.stochtrack.pca.min_var2;
    timefreq_pca_1 = squeeze(timefreq_principal_components(:,:,1));
    timefreq_pca_2 = squeeze(timefreq_principal_components(:,:,2));
    timefreq_reconstruction = beta1 * abs(timefreq_pca_1) + beta2 * abs(timefreq_pca_2);

    timefreq_reconstruction = interp1(timefreq_frequencies,timefreq_reconstruction,map.f);

    %timefreq_reconstruction = abs(timefreq_reconstruction);
    timefreq_reconstruction(isnan(timefreq_reconstruction)) = 0;

    beta1 = repmat(beta1, 1, N)';
    beta2 = repmat(beta2, 1, N)';
    track_data.var1 = beta1;
    track_data.var2 = beta2;

  elseif strcmp(params.stochtrack.pca.type,'PMNS')

    var1s = gLinspace(params.stochtrack.pca.min_var1,params.stochtrack.pca.max_var1,...
      params.stochtrack.pca.num_var1,params)';
    var2s = gLinspace(params.stochtrack.pca.min_var2,params.stochtrack.pca.max_var2,...
      params.stochtrack.pca.num_var2,params)';
    [X,Y] = meshgrid(var1s,var2s);
    X = X(:); Y = Y(:);

    f0 = X(aa); beta = Y(aa);

    %f0 = (params.stochtrack.pca.max_var1-params.stochtrack.pca.min_var1)* ...
    %  rand() + params.stochtrack.pca.min_var1;
    f0 = round(f0);
    %beta = (params.stochtrack.pca.max_var2-params.stochtrack.pca.min_var2)* ...
    %  rand() + params.stochtrack.pca.min_var2;

    % Build Reconstruction
    % timefreq_betas is an NxN matrix of projection coefficients where rows
    % correspond to the waveform being projected and columns correspond to the
    % principal component.
    aligned_timefreq_reconstruction = zeros(size(timefreq_mean));

     % XXX: setting Npcs=1 here but keeping the loop for generality
     npcs=1;

     % Sum the PCs
     for n = 1:npcs
       aligned_timefreq_reconstruction = aligned_timefreq_reconstruction + ...
        beta * squeeze(timefreq_principal_components(n,:,:));
     end

     % De-center (add the mean)
     aligned_timefreq_reconstruction = aligned_timefreq_reconstruction + timefreq_mean;

      % De-align (rescale the frequency)
      timefreq_frequencies = double(timefreq_frequencies);
      timefreq_scales = double(timefreq_scales);
      f0 = double(f0);
      timefreq_reconstruction = dealign_tfmap(aligned_timefreq_reconstruction, ...
        timefreq_scales, timefreq_frequencies, f0);

      timefreq_reconstruction = interp1(timefreq_frequencies,timefreq_reconstruction,map.f);
      dt = map.segstarttime(2) - map.segstarttime(1);
      timecut = timefreq_times(1):dt:timefreq_times(end);

      dt_ratio = ceil(dt/(timefreq_times(2)-timefreq_times(1)));
      fun = @(block_struct) nansum(block_struct.data(:));
      timefreq_reconstruction = blockproc(timefreq_reconstruction,[1 dt_ratio],fun);      
      timefreq_times = timecut;

      %timefreq_reconstruction = abs(timefreq_reconstruction);
      timefreq_reconstruction(isnan(timefreq_reconstruction)) = 0;
      timefreq_reconstruction_sum = sum(abs(timefreq_reconstruction),1);
      timefreq_reconstruction_cumsum = cumsum(timefreq_reconstruction_sum);
      timefreq_reconstruction_cumsum = timefreq_reconstruction_cumsum / max(timefreq_reconstruction_cumsum);
 
      [junk,timefreq_reconstruction_cumsum_1] = min(abs(timefreq_reconstruction_cumsum-0.01));
      [junk,timefreq_reconstruction_cumsum_99] = min(abs(timefreq_reconstruction_cumsum-0.99));
      timefreq_reconstruction = timefreq_reconstruction(:,timefreq_reconstruction_cumsum_1:timefreq_reconstruction_cumsum_99);
      timefreq_times = timefreq_times(timefreq_reconstruction_cumsum_1:timefreq_reconstruction_cumsum_99);

      f0 = repmat(f0, 1, N)';
      beta = repmat(beta, 1, N)';
      track_data.var1 = f0;
      track_data.var2 = beta;

  end
  timefreq_reconstruction = abs(timefreq_reconstruction);

  TT = gColon(0,1,length(timefreq_times)-1,params);
  FF = gColon(0,1,M-1,params);
  [TT,FF] = meshgrid(TT,FF);
  TT = TT(:)'; FF = FF(:)'; timefreq_reconstruction = timefreq_reconstruction(:)';

  %t = gColon(0,1,M-1,params);
  t = TT;
  t = repmat(t,N,1);

  %foft = gColon(1,1,M,params); 
  foft = FF;
  foft =  repmat(foft', 1, N)';

  % Compute h vs. time
  hoft = timefreq_reconstruction';
  hoft(isinf(hoft)) = 0;
  sumhoft = sum(hoft);
  H = hoft ./ sumhoft;
  H = repmat(H, 1, N)';

 % Convert frequency to map indices
  yvals = foft;

  % Move waveform to correct starting index
  xvals = t;
  nstops = gColon(0,1,N-1,params);

  track_data.t0 = nstops;

  nstops = repmat(nstops, length(TT), 1)';
  xvals = mod(xvals + nstops - 1,N) + 1;

  % remove values below 0
  H(yvals<0) = 0;
  yvals(yvals<0) = NaN;

  xvals = xvals';
  yvals = yvals';
  H = H';

end

return;
