function params = stochtrackDefaults(params)
% function params = clusterDefaults(params)
% Sets parameters for cluster search.  Eric: this file was last modified on
% March 14.  These values represent our current best guess for the most
% sensitive values for LGRB signals in 1s x 1Hz maps.  Thus users should be %
% ware: these values are only "defaults" for a particular analysis.
%
% Routine written by Shivaraj Kandhasamy.
% Contact: shivaraj@lphysics.umn.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% run stochtrack
params.doStochtrack = true;
% number of trials in memory
params.stochtrack.T = 100000;
% trial multiplier in for-loop (avoids increasing memory requirements)
params.stochtrack.F = 20;
% minimum track length (in bins)
params.stochtrack.mindur = 100;
% over-write noise and inject a monochromatic signal
params.stochtrack.demo = false;
% normalization scheme
params.stochtrack.norm = 'npix';
% do stochbank
params.stochtrack.doStochbank = false;
% transpose map
params.stochtrack.doTranspose = false;
% do bezier
params.stochtrack.doBezier = true;
% H weighting factor
params.stochtrack.alpha = 1;
% DF allowing for multiple bins per column
params.stochtrack.doDF = 0;
% do cbc
params.stochtrack.doCBC = false;
params.stochtrack.doECBC = false;
params.stochtrack.cbc.min_mass = 1.0;
params.stochtrack.cbc.max_mass = 5.0;
params.stochtrack.cbc.min_eccentricity = 0;
params.stochtrack.cbc.max_eccentricity = 1;
params.stochtrack.cbc.n_eccentricity = 11;
% do rmodes
params.stochtrack.doRModes = false;
params.stochtrack.rmodes.min_f0 = 600.0;
params.stochtrack.rmodes.max_f0 = 1550.0;
params.stochtrack.rmodes.min_alpha = 1e-4;
params.stochtrack.rmodes.max_alpha = 1;
% do sigma from PSD file
params.stochtrack.doSigmaFromPSD = false;
params.stochtrack.psdFile = 'ZERO_DET_high_P_psd.txt';
% pixel cut
params.stochtrack.doPixelCut = false;
params.stochtrack.pixel_threshold = 10;
params.stochtrack.doBurstegardCut = false;

% extra pixels for DF
params.stochtrack.extra_pixels = 0;

params.stochtrack.triggerGPS = -1;

params.stochtrack.doSeed = 0;

return;
