function outputmap = align_tfmap(timefreqmap, scales, freqs, fpeak)
   
%%%    """
%%%    The inverse of align_tfmap(); simply switches the order of the scaling
%%%    operation
%%%    """

outputmap = timefreqmap;
%[junk,index] = min(abs(freqs-fpeak));
%peak_scale = scales(index);
peak_scale = interp1(freqs,scales,fpeak);

%%% shift columns

[xsize,ysize] = size(outputmap);
outputmap = shift_vec(outputmap, scales, 0.25*max(scales), peak_scale);

