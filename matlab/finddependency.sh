#!/bin/bash

# This script scans a user-supplied set of directories for .m files, 
# then checks to see if any of these file names appear in the specified
# target file.  This may be useful for figuring out file dependencies.
#
# usage:
#
#   finddependency.sh target dirlistfile verbose
#
# target       file to be searched for dependencies
# dirlistfile  file containing a list of directories to be searched for 
#              .m files, one directory per line 
# verbose      optional; if non-empty then extra verbosity 

# ---- Rename input arguments.
target=$1
dirlistfile=$2
verbose=$3

echo "----------------------------------------------------"
echo "  Looking for dependencies of ${target}"
echo "----------------------------------------------------"

# ---- Loop over directories to be searched.
while read dir 
do 
  # ---- Collect list of .m files in this directory.
  echo "processing ${dir}"
  ls -1 ${dir}/*.m > dep_filelist.txt 
  # ---- Parse list of files into function names, with .m extension stripped.
  awk -F/  '{print $NF}' dep_filelist.txt | sed 's/\.m//g' > dep_functionlist.txt
  # ---- Scan target for this function, reporting only when dependencies are 
  #      found.
  while read function 
  do 
    if [[ -n $(grep -n ${function} ${target}) ]]; then
        echo "    found dependence: ${dir}/${function}"
        if [ -n "${verbose}" ]; then
            grep -n ${function} ${target}
        fi 
    fi
  done < dep_functionlist.txt 
done < ${dirlistfile}

# ---- Clean up.
rm dep_filelist.txt dep_functionlist.txt

