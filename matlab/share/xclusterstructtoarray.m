function clusterArray = xclusterstructtoarray(clusterStruct)
% XCLUSTERSTRUCTTOARRAY - convert x-pipeline events from struct to array format
%
% usage:
%
% clusterArray = xclusterstructtoarray(clusterStruct);
%
% clusterStruct     Struct with fields 'boundingBox', 'peakTime',
%                   'peakFrequency', 'nPixels', 'likelihood' in the usual
%                   x-pipeline format. Any other fields are ignored (they are
%                   not defined in the array format for x-pipeline events).
%
% clusterArray      Array of event clusters as produced by clusterTFmapNew.
%
% This function is a wrapper for xclusterpropertiestoarray. See clusterTFmapNew.
%
% $Id$

% ---- Argument checks.
narginchk(1,1);

clusterArray = xclusterpropertiestoarray(clusterStruct.boundingBox, ...
    clusterStruct.peakTime, clusterStruct.peakFrequency, ...
    clusterStruct.nPixels, clusterStruct.likelihood);

% ---- Done.
return

