
// author: michal.was@ligo.org

#include "math.h"
#include "mex.h"   
#include<cstdio>
#include<queue>
#include<cmath>
#include<string>
#include<iostream>

using namespace std;



void process(const int nPixels, const int nKeptLabels, const double *labels, const double *keptLabels,  double *keptMask)
{

  // check that keptLabels is sorted, there can't be more labels than pixels
  double curLabel = 0;
  for ( int iLabel = 0; iLabel < nKeptLabels ; iLabel++)
    {
      if ( curLabel >= keptLabels[iLabel] )
	{
	  mexErrMsgTxt("Kept labels is not sorted");
	  return;
	}
      curLabel = keptLabels[iLabel];
    }

  for ( int iPix = 0 ; iPix < nPixels ; iPix++)
    {
      // labels start at 1 instead of 0
      int minLabel = 0;
      int maxLabel = nKeptLabels-1;
      // do a binary search over the kept labels to see if label of iPix is on the list to be kept
      while ( maxLabel >= minLabel )
	{
	  int midLabel = (minLabel+maxLabel)/2;
	  if( keptLabels[midLabel] == labels[iPix] )
	    {
	      // found label of iPix in the kept list, mark it down
	      // into the keptMask
	      keptMask[iPix] = 1;
	      break;
	    }
	  else if ( keptLabels[midLabel] < labels[iPix] )
	    {
	      minLabel = midLabel + 1;
	    }
	  else
	    {
	      maxLabel = midLabel - 1;
	    }
	}
    }

  return;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  //Declaration
  const mxArray *labelArray;
  const mxArray *keptLabelArray;
  const double *labels;
  const double *keptLabels;
  double *keptMask;
  int nDims;
  const int *dimLabels;
  const int *dimKeptLabels;

  //Copy to pointer
  labelArray=prhs[0];
  keptLabelArray=prhs[1];

  // transform arrays to doubles
  labels=mxGetPr(labelArray);
  keptLabels=mxGetPr(keptLabelArray);

  // Number of dimesnion and size
  nDims=mxGetNumberOfDimensions(labelArray);
  dimLabels=mxGetDimensions(labelArray);
  const int nPixels=dimLabels[0];
  dimKeptLabels=mxGetDimensions(keptLabelArray);
  const int nKeptLabels= dimKeptLabels[0];

  // allocate output matrix, the matrix is filled with zeros
  plhs[0] = mxCreateDoubleMatrix(nPixels, 1, mxREAL); 
  keptMask = mxGetPr(plhs[0]);

  // fill the label
  process(nPixels, nKeptLabels, labels, keptLabels, keptMask);

  return;
}
