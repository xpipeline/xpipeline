// It is assumed that the input format is: [x y dx dy significance]
// For purposes of algorithm representation we interpret
// significance as height, so superclustering is just looking for
// bounding boxes that are not covered by any other


#include "math.h"
#include "mex.h"   
#include "stdio.h"

bool equal(double a, double b) {
  return a == b;
}

bool intersectSeg(double begA, double deltaA, double begB, double deltaB) { // Segments are parallel
  if((begA < begB || equal(begA, begB)) && begB < (begA + deltaA))
    return true;
  if((begB < begA || equal(begA, begB)) && begA < (begB + deltaB)) 
    return true; 
  return false;
}

bool intersectRec(const int N, const double* matrice, int i, int j) {
  bool result = true;
  if(!intersectSeg(matrice[i], matrice[i+2*N], matrice[j], matrice[j+2*N])) // Checking x-coordinate
    result = false;
  if(!intersectSeg(matrice[i+N], matrice[i+3*N], matrice[j+N], matrice[j+3*N])) // Checking y-coordinate
    result = false;
  return result;
}

void process(const int N, const double* rectangles, double* uncoveredMask) { 
  // fills an array uncoveredMask, such that T[i] = 1, if the i-th rectangle is uncovered, = 0 otherwise 
  for(int i=0; i<N; i++) 
    uncoveredMask[i] = true;
  for(int i=0; i<N; i++)
    for(int j=i+1; j<N; j++){  
      if(intersectRec(N, rectangles, i, j)) {
	if(equal(rectangles[i+4*N], rectangles[j+4*N]))
	  uncoveredMask[j] = false;
	else if(rectangles[i+4*N] < rectangles[j+4*N])
	  uncoveredMask[i] = false;
	else
	  uncoveredMask[j] = false;
      }
    }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //Declaration
  const mxArray *rectanglesArray;
  const double *rectangles;
  double *uncoveredMask;
  int nDims;
  const int *dimArray;

  //Copy to pointer
  rectanglesArray=prhs[0];
  
  // transform arrays to doubles
  rectangles=mxGetPr(rectanglesArray);
  
  // Number of dimesnion and size
  nDims=mxGetNumberOfDimensions(rectanglesArray);
  if (2 != nDims)  {
    printf("%s\n","Error the number of dimension for rectangles is not 2 ");
    return ;
  }
  dimArray=mxGetDimensions(rectanglesArray);
  const int colLen=dimArray[0];
  const int rowLen=dimArray[1];

  // allocate output matrix, the matrix is filled with zeros
  plhs[0] = mxCreateDoubleMatrix(colLen, 1, mxREAL); 
  uncoveredMask = mxGetPr(plhs[0]);

  process(colLen, rectangles, uncoveredMask);

  return;
}
    
