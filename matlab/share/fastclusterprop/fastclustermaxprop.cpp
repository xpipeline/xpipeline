// fast replacement for regionprops.m :
// input : lablledMap, likelihoodMap
// output: clusterArray
// for details see clusterTFmapNew 


#include "math.h"
#include "mex.h"   
#include "stdio.h"

inline double max(double a,double b) 
{
  if(a > b) {return a;}
  return b;
}

inline double min(double a,double b) 
{
  if(a < b) {return a;}
  return b;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //Declaration
  const mxArray *labelledMapArray;
  const mxArray *likelihoodMapArray;
  const double *labelledMap;
  const double *likelihoodMap;
  double *clusterArray;
  int nDims;
  const int *dimArray;

  //Copy to pointer
  labelledMapArray=prhs[0];
  likelihoodMapArray=prhs[1];
  
  // transform arrays to doubles
  labelledMap=mxGetPr(labelledMapArray);
  likelihoodMap=mxGetPr(likelihoodMapArray);
  
  // Number of dimesnion and size
  nDims=mxGetNumberOfDimensions(likelihoodMapArray);
  if (3 != nDims && 2 != nDims)  {
    printf("%s\n","Error the number of dimension for likelihoodMap is not 2 or 3");
    return ;
  }
  dimArray=mxGetDimensions(likelihoodMapArray);
  int colLen=dimArray[0];
  int rowLen=dimArray[1];
  int nLikelihoods;
  if( 3== nDims) {
    nLikelihoods=dimArray[2]; }
  else {
    nLikelihoods=1;}
    


  int nClusters=0;
  for(int i=0;i<rowLen;i++){
    for(int j=0;j<colLen;j++){
      nClusters=(int)max(double(nClusters),labelledMap[i*colLen + j]);
    }
  }

  // allocate output matrix, the matrix is filled with zeros
  plhs[0] = mxCreateDoubleMatrix(int(nClusters), 7+nLikelihoods, mxREAL); 
  clusterArray = mxGetPr(plhs[0]);

  for(int i=0;i<rowLen;i++){
    for(int j=0;j<colLen;j++){
      int label=int(labelledMap[i*colLen + j])-1;
      if( -1 == label) {continue;}
      // compute min t
      if(clusterArray[(0*nClusters)+label]>0){
	clusterArray[(0*nClusters)+label] = 
	  min(clusterArray[(0*nClusters)+label], i+0.5);
      }
      else {clusterArray[(0*nClusters)+label] = i+0.5;}
      //compute mean t
      clusterArray[(1*nClusters)+label] = 
	( clusterArray[(1*nClusters)+label] * clusterArray[(7*nClusters)+label] +
	  (i+1) * likelihoodMap[0 + i*colLen + j]) /
	( clusterArray[(7*nClusters)+label] + likelihoodMap[0 + i*colLen + j]);
      // compute max t
      clusterArray[(2*nClusters)+label] = 
	max(clusterArray[(2*nClusters)+label], i+1.5);
      // compute min f;
      if(clusterArray[(3*nClusters)+label]>0){
	clusterArray[(3*nClusters)+label] = 
	  min(clusterArray[(3*nClusters)+label], j+0.5);
      }
      else {clusterArray[(3*nClusters)+label] = j+0.5;}
      //compute mean f
      clusterArray[(4*nClusters)+label] = 
	( clusterArray[(4*nClusters)+label] * clusterArray[(7*nClusters)+label] +
	  (j+1) * likelihoodMap[0 + i*colLen + j]) /
	( clusterArray[(7*nClusters)+label] + likelihoodMap[0 + i*colLen + j]);
      //compute max f
      clusterArray[(5*nClusters)+label] = 
	max(clusterArray[(5*nClusters)+label], j+1.5);
      //compute Area
      clusterArray[(6*nClusters)+label] = clusterArray[(6*nClusters)+label]+1;
      //compute sum for each likelihood
      for(int k=0;k<nLikelihoods;k++) {
	clusterArray[((7+k)*nClusters)+label]=
	  max(clusterArray[((7+k)*nClusters)+label],
	      likelihoodMap[k*colLen*rowLen + i*colLen + j]);
      }
     
    }
  }

  return;
}
    
