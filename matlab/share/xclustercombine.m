function [Y,removedFields] = xclustercombine(X,likelihoodTypes,triggerSets,verbose)
% XCLUSTERCOMBINE - combine multiple clusters into a single cluster
%
% usage:
%
%  [Y,F] = xclustercombine(X,L,S,V)
%
%  X      Struct of X-pipeline triggers.
%  L      Cell array of strings. Names of the likelihoods in X.
%  S      Cell array of vectors. Each element is a set of indices listing one
%         set of triggers in X to be combined.
%  V      Optional boolean. Additional verbosity if true. Default true.
%
%  Y      Struct of X-pipeline triggers. There is one combined trigger in Y for 
%         each element (set) in S; see below for details on how triggers are 
%         combined. Any triggers in X _not_ listed in S are dropped, so triggers
%         in X that are not clustered with any others should appear in S as 
%         one-lement sets (e.g. S{i} = [j]) if they are to be retained.
%  F      Cell array. List of fields removed from X in creating Y.
% 
% $Id$

% ---- Rules for combining struct fields across triggers:
%             boundingBox [REQUIRED] - output is smallest bouding box containing all input bounding boxes
%                peakTime [REQUIRED] - significance-weighted average of constituent triggers
%              unslidTime [OPTIONAL] - significance-weighted average of constituent triggers
%           peakFrequency [REQUIRED] - significance-weighted average of constituent triggers
%                 nPixels [REQUIRED] - sum
%              likelihood [REQUIRED] - for all likelhoods except skyposition*: sum.  For skyposition* use value of highest-significance constituent trigger
%            significance [REQUIRED] - sum
%               listIndex [REQUIRED] - use value of highest-significance constituent trigger
%            analysisTime [REQUIRED] - use value of highest-significance constituent trigger
%             timeOffsets [REQUIRED] - must be the same for all constituent triggers else error
%          circTimeSlides [REQUIRED] - must be the same for all constituent triggers else error
%               startTime [REQUIRED] - use value of highest-significance constituent trigger
%              centerTime [REQUIRED] - use value of highest-significance constituent trigger
%        unslidCenterTime [REQUIRED] - use value of highest-significance constituent trigger
%               jobNumber [REQUIRED] - must be the same for all constituent triggers else error
%           fakeJobNumber [REQUIRED] - use value of highest-significance constituent trigger
%           realJobNumber [REQUIRED] - use value of highest-significance constituent trigger
%                    pass [OPTIONAL] - if defined must be same for all constituent triggers else error
%                   pass* [OPTIONAL] - min() of all all constituent triggers
%      Any other fields are ignored...?

% ---- Checks on inputs.
narginchk(3,4);

% ---- Assign defaults if needed.
if nargin<4
    verbose = true;
end

% ---- Number of trigger sets.
Nset = length(triggerSets);

% ---- It is common that triggerSets contains many single-trigger entries. 
%      These are most efficiently handled by using xclustersubset to copy them
%      directly to the output array as one large set. So we first identify all
%      single-trigger sets and copy the indices of the corresponding triggers
%      into a separate array.
% ---- Number of triggers in each set.
Ntrig = zeros(Nset,1);
for ii=1:Nset
    Ntrig(ii) = length(triggerSets{ii});
end
% ---- Sets with 1 or 2p ("two-plus") triggers.
idx1  = find(Ntrig==1);
idx2p = find(Ntrig>1);
% ---- Extract the indices of all single-trigger cases into a single vector.
singleSets  = cell2mat(triggerSets(idx1));
Nsingle = length(idx1);
% ---- Delete single-trigger cases from triggerSets.
triggerSets = triggerSets(idx2p);
clear Ntrig idx2p idx1

% ---- Initialise storage for output. The simplest way is to copy 
%      length(triggerSets) input triggers into the output, which ensures all the 
%      fields are defined and enough rows of storage are initialised to hold all
%      the multiple-trigger sets. (This also works if length(triggerSets)=0.) 
%      We'll append the single-trigger sets at the end.
Y = xclustersubset(X,1:length(triggerSets));

% ---- Get a list of all fields in the trigger struct.
fields = fieldnames(X);

% ---- Fields that will be ignored (removed from output list).
ignoredFields = [];

% ---- Identify some special cases needed later.
Nlike = length(likelihoodTypes);
skyIdx = find(strcmp(likelihoodTypes,'skypositiontheta') | strcmp(likelihoodTypes,'skypositionphi'));
likeIdx = setdiff(1:Nlike,skyIdx);

% ---- Loop over trigger sets with multiple triggers. (We will append the 
%      single-trigger cases after).
for iset = 1:length(triggerSets)
    % ---- Extract triggers in this set.
    trig = xclustersubset(X,triggerSets{iset});
    % ---- Determine index of highest significance trigger.
    [~,maxSigIdx] = max(trig.significance);
    % ---- Loop over fields and compute combined quantities.
    for ifield = 1:length(fields)
        switch fields{ifield}
            case 'analysisTime'
                % ---- analysisTime [REQUIRED] - use value of highest-significance constituent trigger
                Y.analysisTime(iset)  = trig.analysisTime(maxSigIdx);
            case 'boundingBox'
                % ---- boundingBox [REQUIRED] - output is smallest bouding box containing all input bounding boxes
                Y.boundingBox(iset,:) = [min(trig.boundingBox(:,1)) ...
                                         min(trig.boundingBox(:,2)) ...
                                         max(trig.boundingBox(:,1)+trig.boundingBox(:,3)) - min(trig.boundingBox(:,1)) ...
                                         max(trig.boundingBox(:,2)+trig.boundingBox(:,4)) - min(trig.boundingBox(:,2)) ];
            case 'centerTime'
                % ---- centerTime [REQUIRED] - use value of highest-significance constituent trigger
                Y.centerTime(iset)    = trig.centerTime(maxSigIdx);
            case 'circTimeSlides'
                % ---- circTimeSlides [REQUIRED] - must be the same for all constituent triggers else error
                Y.circTimeSlides(iset,:) = trig.circTimeSlides(1,:);
                if size(unique(trig.circTimeSlides,'rows'),1) ~= 1
                    error('Cannot combine triggers with different circTimeSlides.')
                end
            case 'fakeJobNumber'   
                % ---- fakeJobNumber [REQUIRED] - use value of highest-significance constituent trigger
                Y.fakeJobNumber(iset) = trig.fakeJobNumber(maxSigIdx);
            case 'jobNumber'
                % ---- jobNumber [REQUIRED] - must be the same for all constituent triggers else error
                Y.jobNumber(iset)     = trig.jobNumber(1);
                if length(unique(trig.jobNumber)) ~= 1
                    error('Cannot combine triggers with different jobNumber values.')
                end
            case 'likelihood'
                % ---- likelihood [REQUIRED] - for all likelihoods except skyposition*: sum.  
                %      For skyposition* use value of highest-significance constituent trigger.
                Y.likelihood(iset,likeIdx) = sum(trig.likelihood(:,likeIdx));
                Y.likelihood(iset,skyIdx)  = trig.likelihood(maxSigIdx,skyIdx);
            case 'listIndex'
                % ---- listIndex [REQUIRED] - use value of highest-significance constituent trigger
                Y.listIndex(iset)     = trig.listIndex(maxSigIdx);
            case 'nPixels'
                % ---- nPixels [REQUIRED] - sum
                Y.nPixels(iset)       = sum(trig.nPixels);
            case 'pass'
                % ---- pass [OPTIONAL] - must be the same for all constituent triggers else error
                Y.pass(iset)          = trig.pass(1);
                if length(unique(trig.pass)) ~= 1
                    error('Cannot combine triggers with different pass values.')
                end
            case 'passFixedRatio'
                % ---- pass* [OPTIONAL] - min() of all constituent triggers
                Y.passFixedRatio(iset)        = min(trig.passFixedRatio);
            case 'passFrequencyVetoSegs'
                Y.passFrequencyVetoSegs(iset) = min(trig.passFrequencyVetoSegs);
            case 'passRatio'
                Y.passRatio(iset)             = min(trig.passRatio);
            case 'passTimeVetoSegs'
                Y.passTimeVetoSegs(iset)      = min(trig.passTimeVetoSegs);
            case 'passVetoSegs'
                Y.passVetoSegs(iset)  = min(trig.passVetoSegs);
            case 'passWindow'
                Y.passWindow(iset)   = min(trig.passWindow);
            case 'peakFrequency'
                % ---- peakFrequency [REQUIRED] - significance-weighted average of constituent triggers
                Y.peakFrequency(iset) = sum(trig.peakFrequency .* trig.significance)/sum(trig.significance);
            case 'peakTime'
                % ---- peakTime [REQUIRED] - significance-weighted average of constituent triggers
                Y.peakTime(iset)      = sum(trig.peakTime .* trig.significance)/sum(trig.significance);
            case 'realJobNumber'   
                % ---- realJobNumber [REQUIRED] - use value of highest-significance constituent trigger
                Y.realJobNumber(iset) = trig.realJobNumber(maxSigIdx);
            case 'significance'
                % ---- significance [REQUIRED] - sum
                Y.significance(iset)  = sum(trig.significance);
            case 'startTime'
                % ---- startTime [REQUIRED] - use value of highest-significance constituent trigger
                Y.startTime(iset)     = trig.startTime(maxSigIdx);
            case 'timeOffsets'
                % ---- timeOffsets [REQUIRED] - must be the same for all constituent triggers else error
                Y.timeOffsets(iset,:) = trig.timeOffsets(1,:);
                if size(unique(trig.timeOffsets,'rows'),1) ~= 1
                    error('Cannot combine triggers with different timeOffsets.')
                end
            case 'unslidCenterTime'
                % ---- centerTime [REQUIRED] - use value of highest-significance constituent trigger
                Y.unslidCenterTime(iset,:) = trig.unslidCenterTime(maxSigIdx,:);
            case 'unslidTime'
                % ---- unslidTime [REQUIRED] - significance-weighted average of constituent triggers
                Y.unslidTime(iset,:)  = sum(diag(trig.significance)*trig.unslidTime,1)/sum(trig.significance);
            otherwise
                % ---- KLUDGE: other fields are ignored for the moment ...
                if verbose && iset == 1
                    warning(['Ignoring field ' fields{ifield} '. Removing from output triggers.'])
                    ignoredFields = [ignoredFields, ifield];
                end 
        end
    end %-- loop over fields
end %-- loop over sets

if Nsingle>0 
    % ---- We have unpaired triggers to append to the output list. Extract them
    %      from the input list with xclustersubset, then merge them into the 
    %      output list with xclustermerge.
    singles = xclustersubset(X,singleSets);
    Y = xclustermerge(Y,singles);
end

% ---- Remove ignored fields.
removedFields = fields(ignoredFields);
Y = rmfield(Y,removedFields);

% ---- Done.
return

