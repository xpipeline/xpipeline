function skyPositions = sinusoidalMap(angularResolution,maxTheta,doPole,doOptimal)
% SINUSOIDALMAP Tile the sky with a specified angular resolution.
%
% SINUSOIDALMAP returns a grid of points in spherical coordinates that
% covers the sky with (close to) the minimum number of points required to
% ensure a specified angular resolution.
%
% usage:
%
%   skyPositions = sinusoidalMap(angularResolution,maxTheta,doPole,doOptimal)
%
%   angularResolution    Desired angular resolution of tiling [radians].
%   maxTheta             Optional scalar. Largest value of polar coordinate
%                        that needs to be covered by the grid [radians].
%                        Default: pi.
%   doPole               Optional flag.
%                        1 : begin placement at North Pole.
%                        0 : begin placement 1/2 angularResolution from
%                            North Pole (default).
%   doOptimal            Optional flag.
%                        1 : use angularResolution for theta spacing.
%                        0 : divide polar range into integer number of
%                            theta intervals (default).  Neater but less
%                            efficient. 
%
%   skyPositions         Array of sky positions (see below).
%
% The resulting sky positions are returned as an Nx4 matrix with the form
%
%   [theta phi pOmega dOmega]
%
% The four columns have the following meanings:
%
%   theta   Polar angle (radians) in the range [0, pi] with 0 at the North
%           Pole/ z axis. 
%   phi     Azimuthal angle (radians) in the range [-pi, pi) with 0 at
%           Greenwich / x axis. 
%   pOmega  A priori probability associated with each sky position.  Sums
%           to unity.
%   dOmega  Solid angle associated with each sky position.  Sums to
%           fraction of sky covered by grid.
%
% Here theta and phi define a unique point on the sky and pOmega and dOmega
% are used when performing integrals over the sky.
%
% If angularResolution >= pi, one single grid point covers the entire sky.
% In this skyPositions is a single point at the North Pole. 
%
% See also XTILESKY and MINIMALGRID.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

% ---- Check number of input arguments, assign default values.
error(nargchk(1, 4, nargin));
if (nargin<4)
    doOptimal = 0;
end

if (nargin<3)
    doPole = 0;
end

if (nargin<2)
    maxTheta = pi;
end

% ---- Initialize result vectors.
thetas = [];
phis = [];
dOmegas = [];

% ---- Catch case where angular resolution is larger than pi -- then one
%      single grid point covers the entire sky.  (E.g.: H1-H2 network in
%      GRB searches.)  In this case force output to be a single point at
%      the North Pole. 
if angularResolution >= pi
    skyPositions = [0 0 1 4*pi];
    return
end

% ---- Determine theta spacing.
if doOptimal
    thetaStep = angularResolution;    
else
    % ----- Determine number of thetas.
    numberOfThetas = ceil(maxTheta / angularResolution);
    % ---- Determine theta spacing.
    thetaStep = maxTheta / numberOfThetas;
end

% ---- Set range of theta.
if doPole
    % ---- Place first point at the north pole.
    thetaRange = [0 : thetaStep : maxTheta + thetaStep/2];  
else
    % ---- Place first point a half grid space away from the north pole.
    if doOptimal
        thetaRange = [thetaStep/2 : thetaStep : maxTheta + thetaStep/2];
    else
        thetaRange = [thetaStep/2 : thetaStep : maxTheta];
    end
end
% ---- Check for any theta values > pi and replace by pi.
if any(thetaRange>pi)
    index = find(thetaRange>pi);
    thetaRange(index) = [];
    thetaRange(end+1) = pi;
end

% ---- Loop over thetas, assigning phi values.
for theta = thetaRange,

    % ---- Determine number of phis.
    numberOfPhis = max(ceil(2 * pi * sin(theta) / angularResolution), 1);

    % ---- Determine phi spacing.
    phiStep = 2 * pi / numberOfPhis;

    % ---- Determine differential solid angle.
    if theta == 0 | theta == pi
        dOmega = 4 * pi * sin(0.25 * thetaStep).^2;
    else
        dOmega = 4 * pi / numberOfPhis * sin(theta) * sin(0.5 * thetaStep);
    end

    % ---- Append new thetas.
    thetas = [thetas theta * ones(1, numberOfPhis)];

    % ---- Append new phis.
    phis = [phis (-pi + phiStep / 2) : phiStep : (pi - phiStep / 2)];

    % ---- Append new differential solid angles.
    dOmegas = [dOmegas dOmega * ones(1, numberOfPhis)];
  
end  %-- end loop over theta

% ---- Force row vectors.
thetas = thetas(:);
phis = phis(:);
dOmegas = dOmegas(:);

% ---- Uniform a priori probability distribution function.
pOmegas = dOmegas / sum(dOmegas);

% ---- Construct resulting sky position matrix.
skyPositions = [thetas phis pOmegas dOmegas];

% ---- Return to calling function.
return
