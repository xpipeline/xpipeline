function [wFp, wFc, wFpDP, wFcDP, wFpTimeFrequencyMap, wFcTimeFrequencyMap, ...
   Mpp, Mcc, timeFrequencyMap] = xcalctimefreqmaps(...
   numberOfTimeBins, numberOfFrequencyBins, numberOfChannels, numberOfSegments, ...
   Fp, Fc, skyPositionNumber, conditionedData, windowData, amplitudeSpectra, ...
   integrationLength, offsetFraction, frequencyIndex, inbandFrequencies, ...
   residualTimeShifts, segmentIndices, integerTimeShiftLengths, timeFrequencyMap)

% XCALCTIMEFREQMAPS Helper function for xbayesiantimefrequencymap.m 
% Construct time-frequency map of the detector maps for current
% skyPosition.
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               compute detector responses
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Whitened responses for each detector, frequency x detector. 
wFp = repmat(Fp(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ amplitudeSpectra;
wFc = repmat(Fc(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ amplitudeSpectra;

% ---- Convert to dominant polarization (DP) frame.
[wFpDP, wFcDP, psiDP] = convertToDominantPolarizationFrame(wFp,wFc);

% ---- Matrix M_AB components.  These are the dot products of wFp, with
%      themselves and each other, for each frequency, computed in the 
%      DP frame.
Mpp = sum(wFpDP.*wFpDP,2);
Mcc = sum(wFcDP.*wFcDP,2);
epsilon = Mcc./Mpp;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           residual phase shifts for this sky position 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

residualTimeShiftPhases = exp(sqrt(-1) * 2 * pi * ...
    inbandFrequencies' * residualTimeShifts(skyPositionNumber,:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          construct time-frequency maps for each channel   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Note that due to the time shifts the edges of the time-frequency
%      map will contain small amounts of data from the transient 
%      periods.

% ---- Loop over detectors.  
% for channelNumber = 1 : numberOfChannels
% ---- Skip the first detector since it is used
%      as the reference position, and therefore will always be at zero
%      delay.  The advantage is that we only have to FFT its data once.
for channelNumber = 2 : numberOfChannels

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                      FFT data                               %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Start and stop indices of the segment
    segmentStartIndex = segmentIndices(1, 1) ...
        + integerTimeShiftLengths(skyPositionNumber,channelNumber);
    segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2) ...
        + integerTimeShiftLengths(skyPositionNumber,channelNumber);

    % ---- Separate-FFT-for-every-delay version
    if (offsetFraction==1)
        % ---- FFT with no overlapping - this code is faster than the more 
        %      general version below.
        % ---- Extract data for this detector.
        data = conditionedData(segmentStartIndex:segmentStopIndex,channelNumber);
        % ---- Reshape into array of size integrationLength x numberOfTimBins, 
        %      so that each segment to be FFTed occupies one column.
        dataArray = reshape(data,integrationLength, numberOfTimeBins);
        % ---- Apply window and FFT.
        timeFrequencyMapFull{channelNumber} = fft( windowData .* dataArray );
    else
        % ---- Reshape the data into rectangular array, where consecutive
        %      columns contain data according to the requested overlap. 
        for j=1:1/offsetFraction
            % ---- Find start and stop indices.
            offsetStartIndex = segmentStartIndex + integrationLength*(j-1)*offsetFraction;
            offsetStopIndex = segmentStopIndex + integrationLength*(j-1)*offsetFraction;
            % ---- Extract data for this detector.
            data = conditionedData(offsetStartIndex:offsetStopIndex,channelNumber);
            % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
            %      so that each segment to be FFTed occupies one column.
            dataArray = reshape(data,integrationLength,[]);
            % ---- Time bins in full TF map that these segments correspond to.
            timeBins = [j:1/offsetFraction:numberOfTimeBins];
            % ---- Copy these FFTs into TF map for this channel.
            timeFrequencyMapFull{channelNumber}(:,timeBins) = ...
                fft( windowData(:,1:size(dataArray,2)) .* dataArray );
        end
    end

    % ---- Retain only positive frequencies.
    timeFrequencyMap{channelNumber} = ...
        timeFrequencyMapFull{channelNumber}(frequencyIndex,:);

    % ---- Apply residual time shift.
    timeFrequencyMap{channelNumber} = timeFrequencyMap{channelNumber} ...
        .* repmat(residualTimeShiftPhases(:,channelNumber),[1,numberOfTimeBins]); 

end % ---- loop over channels 


% ---- Compute antenna-response weighted time-frequency maps.
channelNumber = 1;

wFpTimeFrequencyMap = timeFrequencyMap{channelNumber} .* ...
    repmat(wFpDP(:,channelNumber),[1,numberOfTimeBins]); 
wFcTimeFrequencyMap = timeFrequencyMap{channelNumber} .* ...
    repmat(wFcDP(:,channelNumber),[1,numberOfTimeBins]); 
for channelNumber = 2 : numberOfChannels
    wFpTimeFrequencyMap = wFpTimeFrequencyMap + ...
        timeFrequencyMap{channelNumber} .* ...
        repmat(wFpDP(:,channelNumber),[1,numberOfTimeBins]); 
    wFcTimeFrequencyMap = wFcTimeFrequencyMap + ... 
        timeFrequencyMap{channelNumber} .* ...
        repmat(wFcDP(:,channelNumber),[1,numberOfTimeBins]);
end

