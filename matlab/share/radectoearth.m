function [phi, theta] = radectoearth(ra,dec,gps)
% radectoearth - Convert from right ascension and declination to
% Earth-based coordinates.
%
%   [phi, theta] = radectoearth(ra,dec,gps)
%
% ra     Vector of right ascensions (degrees).
% dec    Vector of declinations (degrees).
% gps    Vector of GPS times (seconds). 
%
% phi    Vector of azimuthal angles in Earth-based coordinates (radians, 
%        -pi <= phi < pi).
% theta  Vector of polar angles in Earth-based coordinates (radians, 
%        0 <= theta <= pi).
%
% Earth-based coordinates have the x-axis (phi=0) pointing from the Earth's
% center to the intersection of the prime meridian of Greenwich with the
% equator, the z-axis (theta=0) pointing from the Earth's center to the
% North pole, and the y-xais chosen to form a right-handed coordinate
% system. 
%
% Note that the input vectors ra, dec must be the same size. If gps is a scalar,
% the same GPS time is used for converting each sky position. Conversely, if ra
% and dec are scalars, the same (ra,dec) values will be used with each gps time.
%
% See also EARTHTORADEC, GPSTOGMST.
%
% initial write: Patrick J. Sutton 2006.10.26
%
% $Id$

% ---- Error checks.
% ---- Test number of arguments.
error(nargchk(3,3,nargin));
% ---- Convert everything to column vectors.
ra = ra(:);
dec = dec(:);
gps = gps(:);
% ---- Equal length vectors.
if (length(ra)~=length(dec))
    error('Right ascension and declination vectors must be the same size.')
end
% ---- Verify allowed sizes:
%        ra,dec,gps - all scalar (do nothing)
%        ra,dec - common vectors, gps - scalar (expand gps to vector)
%        ra,dec - scalar, gps - vector (expand ra,dec to vectors)
%        ra,dec, gps - all common vectors (verify common size)
if (length(ra)>1 && length(gps)==1)
    gps = gps * ones(size(ra));
elseif (length(ra)==1 && length(gps)>1)
    ra = ra * ones(size(gps));
    dec = dec * ones(size(gps));
elseif (length(gps)>1 && length(ra)>1 && length(gps)~=length(ra))
    error('Vector of gps times not the same length as vector of right ascensions.')
end

% ---- Compute sidereal time (sec) at each event time.
gmst = GPSTOGMST(gps);
% ---- Convert to degrees
gmst_deg = gmst / 86400 * 360; 

% ---- Compute Earth-based coordinates of targeted sky location, in degrees.
phi_deg = ra - gmst_deg;
theta_deg  = 90 - dec;

% ---- Convert to radians.
phi = phi_deg * 2 * pi / 360;
phi = mod(phi+pi,2*pi)-pi;
theta = theta_deg * 2 * pi / 360;
    
% ---- Done
return

