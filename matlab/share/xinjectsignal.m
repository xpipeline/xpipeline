function [injectionData, gps_s, gps_ns, phi, theta, psi] = xinjectsignal( ...
    startTime,blockTime,channel,sampleRate,injectionFileName, ...
    noAntResp,injectionNumber,varargin)
% xinjectsignal: Create simulated data streams containing simulated signals 
% for the specified network of gravitational-wave detectors.
%
%  [injData, gps_s, gps_ns, phi, theta, psi] = xinjectsignal( ...
%    startTime,blockTime,channel,sampleRate,injFileName, ...
%    noAntResp,injNumber,varargin)
%
%  startTime    Scalar.  Start time of the data being analysed.
%  blockTime    Scalar.  Duration (s) of the data being analysed.
%  channel      Cell array of strings.  channel{j} holds the name of the
%               jth detector channel being analysed.  The detector name is
%               inferred from the first character of the channel name, and
%               must be one of the names recognized by LoadDetectorData.
%  sampleRate   length(channel)x1 vector.  Sample rates corresponding to
%               channels.
%  injFileName  String.  Name of file specifying simulated signals to be
%               injected.
%  noAntResp    Scalar.  Optional.  If nonzero then do not apply the
%               antenna responses to the injected signals. This is intended
%               for injecting non-GW signals, such as glitches, for which
%               the standard antenna responses are not meaningful. Invoking
%               this flag will cause the code to use Fp = 1 with all other
%               responses set to zero.
%  injNumber    Array (positive integer).  Optional.  If nonzero then only
%               the injections specified on rows "injNumber" of the
%               injection file are injected.  This over-rides the default
%               behaviour, which is to inject all injections in the file 
%               (including those outside the interval 
%               [startTime,startTime+blockTime]!).
%  varargin     Optional 'name','value' pairs.  Recognized options are:
%                 'doNotInject',logical 
%                   If set to true, waveform is not injected; injection
%                   parameters are calculated only. 
%                 'catalogDirectory',string
%                   Absolute path to the directory containing astrophysical
%                   waveform catalogs (e.g., DFM, OB, or ZM).  If not
%                   supplied when constructing such a waveform,
%                   xmakewaveform will attempt to load the appropriate
%                   catalog from a hard-coded location.    
%                 'waveSpeed',scalar
%                   Speed of propagation of the wave [m/s]. Defaults to
%                   c=299792458, the speed of light. Useful for injecting
%                   non-GW signals.
%                 'hrss',scalar
%                   Rescale all injections to this root-sum-square (RSS)
%                   amplitude; see xmakewaveform.
%
%  injData      length(channel)x1 cell array of column vectors.
%               injectionData{j} is a column vector holding the signal data
%               for the jth detector.
%  gps_s        Vector of peak time (GPS seconds, integer part) of each 
%               injection at the center of the Earth.   
%  gps_ns       Vector of peak time (GPS seconds, nanosecond part) of each
%               injection at the center of the Earth.   
%  phi          Vector of azimuthal sky coordinate of each injection
%               (radians, Earth-fixed coordinates).   
%  theta        Vector of polar sky coordinate of each injection 
%               (radians Earth-fixed coordinates).   
%  psi          Vector of polarization angle of each injection  
%               (radians Earth-fixed coordinates).   
%
% The coordinate system is explained in ComputeAntennaResponse. It is
% assumed that the sky positions in the injection file are supplied in this
% coordinate system.  For glitch injections the reported times and angles
% are those for the injection into the first detector.
%
% Any portion of any signal which lies outside the interval 
% [startTime, startTime+blockTime] will be clipped. Otherwise, XINJECTSIGNAL 
% will inject any signal of duration up to blockTime without clipping. 
% The edges of long-duration signals will be tapered with
% a hann window to avoid turn-on/off transients.
%
%  initial version: Patrick J. Sutton 2005.07.10
%
% $Id$


% ---- Speed of light (m/s).
speedOfLight = 299792458;

% ---- Check for optional arguments.
doNotInject=0;
catalogDirectory='';
rescaleHrss=0;
if (nargin>7 && length(varargin))
    % ---- Make sure they are in pairs.
    if (length(varargin) == 2*floor(length(varargin)/2))
        % ---- Parse them.
        index = 1;
        while index<length(varargin)
            switch lower(varargin{index})
                case 'donotinject'
                    doNotInject = varargin{index+1};
                case 'catalogdirectory'
                    catalogDirectory = varargin{index+1};
                case 'wavespeed'
                    speedOfLight = varargin{index+1};
                case 'hrss'
                    rescaleHrss = varargin{index+1};
                otherwise
                    error(['Property value ' varargin{index} ' not recognized.']);
            end
            index = index + 2;
        end
    else
        error('Optional arguments must appear in pairs: property_name, property_value.');
    end
end
if (nargin<7)
     injectionNumber = 0;
end
if (nargin<6)
     noAntResp = 0;
end

% ---- Parse channel list and load info on corresponding detectors.
nDetector = length(channel);
for j=1:nDetector
    detector{j,1} =  LoadDetectorData(channel{j}(1));
end

% ---- Read injection file and extract parameters of all requested
%      injections (all if injectionNumber==0). 
injectionParameters = readinjectionfile(injectionFileName,injectionNumber);

% ---- Number of injections.
nInjection = size(injectionParameters,1);

% ---- Separate each line (a string) into separate elements in a cell 
%      array.  For actual GWB injections (instead of glitches) elements for
%      jParam > 7 may be empty.
for jParam=1:(7*nDetector)
    for iCell=1:nInjection
        [currentParameters{jParam}{iCell} injectionParameters{iCell}] = ...
            strtok(injectionParameters{iCell});
    end
end

% ---- Loop over detectors and construct simulated signals. 
for jDetector=1:nDetector
    % ---- Make timeseries data for given detector.
    injectionData{jDetector,1} = zeros(blockTime*sampleRate(jDetector),1);
    % ---- Loop over simulated signals.
    for jInjection=1:nInjection
        % ---- Extract parameters for current injection.
        if (isempty(currentParameters{(jDetector-1)*7+1}{jInjection}))
            % ---- Use parameters for first detector instead of current
            %      detector.
            offset = 0;
        else
            % ---- Use parameters for current detector.
            offset = (jDetector-1)*7;
        end
        gps_s  = str2num(currentParameters{offset+1}{jInjection});
        gps_ns = str2num(currentParameters{offset+2}{jInjection});
        phi    = str2num(currentParameters{offset+3}{jInjection});
        theta  = str2num(currentParameters{offset+4}{jInjection});
        psi    = str2num(currentParameters{offset+5}{jInjection});
        GWB_type   =     currentParameters{offset+6}{jInjection};
        GWB_params =     currentParameters{offset+7}{jInjection};
        % ---- If WNB we would like to choose a different seed for each
        %      injection. We add the current injectionNumber to the
        %      seed so that each injection in the log file gets a
        %      unique seed.
        if strcmpi(GWB_type,'wnb')
            elements = tildedelimstr2numorcell(GWB_params);
            if length(elements) == 4
                elements(5) = injectionNumber(jInjection);
            elseif length(elements) == 5
                elements(5) = elements(5) + injectionNumber(jInjection);
            else
                errormsg = ['Too many parameters (' num2str(length(elements)) ...
                    ') for injection number ' num2str(injectionNumber(jInjection)) ...
                    ' of type WNB.'];
                error(errormsg);
            end
            GWB_params = [num2str(elements(1)) '~' num2str(elements(2)) ...
                      '~' num2str(elements(3)) '~' num2str(elements(4)) ...
                      '~' num2str(elements(5))];
        end 

        % ---- Unit vector pointing towards source.
        omega = [sin(theta).*cos(phi) ; sin(theta).*sin(phi) ; cos(theta)];

        % ---- Time delay for incoming signal wrt center of Earth 
        %      (t_det - t_coe).
        delay = -detector{jDetector}.V'*omega/speedOfLight;

        % ---- Compute antenna responses.
        if (noAntResp)
            % ---- Use trivial values.
            Fp = 1;
            Fc = 0;
            Fb = 0;
        else
            % ---- Compute antenna response, including polarization angle.
            [Fp Fc Fb] = ComputeAntennaResponse(phi,theta,psi,detector{jDetector}.d);
        end

        % ---- Make injection data in a several-second snippet.  This will
        %      work for any signal with duration less than snippetPad; for
        %      longer signals some clipping may occur.  Clipping will
        %      definitely occur for injections longer than snippetDuration.   
        snippetPad = blockTime;
        snippetDuration = 2*snippetPad + 1;
        % ---- Peak Time of signal relative to startTime.
        peakTime = gps_s+1e-9*gps_ns+delay;  
        % ---- Waveform plus and cross polarizations, in wave frame.
        if(~doNotInject)
            if strcmp(lower(GWB_type),'ninja')
                % ---- Need to call special code to inject ninja waveforms.
                [hp, t] = xmakeninjawaveform(GWB_params, injectionNumber(jInjection), ...
                    channel{jDetector}, sampleRate(jDetector), peakTime, ...
                    snippetPad, snippetDuration);
                % ---- KLUDGE: Reset Fp = 1, Fc = 0 = Fb and generate dummy
                %      timesereis for hc, hb. This is because the 
                %      xmakeninjawaveform code generates the detector
                %      strain including the antenna response, but the code
                %      below expects the polarsations separately.
                hc = zeros(size(hp));
                hb = zeros(size(hp));
                Fp = 1;
                Fc = 0;
                Fb = 0;
            elseif(~isempty(catalogDirectory) & rescaleHrss)
                [t,hp,hc,hb] = xmakewaveform(GWB_type,GWB_params,snippetDuration, ...
                    snippetPad+peakTime-floor(peakTime),sampleRate(jDetector),...
                    'catalogDirectory',catalogDirectory,'hrss',rescaleHrss);                
            elseif(rescaleHrss)
                [t,hp,hc,hb] = xmakewaveform(GWB_type,GWB_params,snippetDuration, ...
                    snippetPad+peakTime-floor(peakTime),sampleRate(jDetector),...
                    'hrss',rescaleHrss);
            elseif(~isempty(catalogDirectory))
                [t,hp,hc,hb] = xmakewaveform(GWB_type,GWB_params,snippetDuration, ...
                    snippetPad+peakTime-floor(peakTime),sampleRate(jDetector),...
                    'catalogDirectory',catalogDirectory);
            else
                [t,hp,hc,hb] = xmakewaveform(GWB_type,GWB_params,snippetDuration, ...
                    snippetPad+peakTime-floor(peakTime),sampleRate(jDetector));
            end
            % ---- Apply taper to start and end of snippet to avoid
            %      discontinuous turn-on/off of very long signals.  Use
            %      1-sec window.
            fs = sampleRate(jDetector);
            taper = hann(fs);
            hp(1:fs/2) = hp(1:fs/2) .* taper(1:fs/2);
            hc(1:fs/2) = hc(1:fs/2) .* taper(1:fs/2);
            hb(1:fs/2) = hb(1:fs/2) .* taper(1:fs/2);
            hp(end-fs/2+1:end) = hp(end-fs/2+1:end) .* taper(end-fs/2+1:end);
            hc(end-fs/2+1:end) = hc(end-fs/2+1:end) .* taper(end-fs/2+1:end);
            hb(end-fs/2+1:end) = hb(end-fs/2+1:end) .* taper(end-fs/2+1:end);
            % ---- Reset t to GPS time.
            t = t + floor(peakTime)-snippetPad;
            % ---- Sample indices.
            injectionSamples = [1:length(t)]';
            % ---- Drop samples which fall outside the desired time interval.
            k = find( (t<startTime) | (t>=startTime+blockTime) );
            if (~isempty(k))
                injectionSamples(k) = [];
                t(k) = [];
                hp(k) = [];
                hc(k) = [];
                hb(k) = [];
            end
            % ---- Make sure injectionSamples is not empty so we don't crash
            if(isempty(injectionSamples))
                continue;
            end
            % ---- Combine with antenna response to give signal.
            injectionSamples = injectionSamples ...
                + round((floor(peakTime)-snippetPad-startTime)*sampleRate(jDetector));
            injectionData{jDetector,1}(injectionSamples) = ...
                injectionData{jDetector,1}(injectionSamples) ...
                + Fp*hp + Fc*hc + Fb*hb;
        end
    end
end

% ---- Record for output the peak time and sky angles for each of the
%      injections.  For glitches record only the parameters for the first
%      detector.
% ---- Prepare storage.  Re-use variable names....
gps_s = zeros(nInjection,1);
gps_ns = zeros(nInjection,1);
phi = zeros(nInjection,1);
theta = zeros(nInjection,1);
psi = zeros(nInjection,1);
% ---- Loop over simulated signals and record peak time, sky angles.
for jInjection=1:nInjection
    gps_s(jInjection)  = str2num(currentParameters{1}{jInjection});
    gps_ns(jInjection) = str2num(currentParameters{2}{jInjection});
    phi(jInjection)    = str2num(currentParameters{3}{jInjection});
    theta(jInjection)  = str2num(currentParameters{4}{jInjection});
    psi(jInjection)    = str2num(currentParameters{5}{jInjection});
end

% ---- Done
return
