function data = xresample(data, inFrequency, outFrequency)
% XRESAMPLE Resample time series data.
%
% XRESAMPLE resamples time series data. It is a wrapper to RESAMPLE, using
% hard-coded filter parameters copied from XCONDITION@5849.
%
% usage:
%
% fdata = xresample(data, inFrequency, outFrequency);
%
%   data                 Vector of input time series data.
%   inFrequency          Scalar. Sample frequency of input data [Hz].
%   outFrequency         Scalar. Sample frequency of output data [Hz].
%
%   fdata                Column vector of resampled time-domain data.  
%
% $Id$


% ------------------------------------------------------------------------------
%    process and validate command line arguments
% ------------------------------------------------------------------------------

% ---- Check for sufficient command line arguments and assign defaults.
narginchk(3, 3);

% ---- Force one dimensional (column vector) data array.
data = data(:);


% ------------------------------------------------------------------------------
%    resample data
% ------------------------------------------------------------------------------

% ---- Factors relating sample frequencies.
[upSampleFactor, downSampleFactor] = rat(outFrequency / inFrequency);

% ---- The anti-aliasing filter used by default in the matlab resample
%      function is weak, with an amplitude attenuation of about 0.5 at
%      the new Nyquist frequency. The anti-aliasing filter constructed
%      below has much stronger attenuation.
% ---- Design anti-alias filter.
filterOrder = 2 * 256 * max(upSampleFactor, downSampleFactor);
filterCutoff = 0.99 / max(upSampleFactor, downSampleFactor);
filterFrequencies = [0 filterCutoff filterCutoff 1];
filterMagnitudes = [1 1 0 0];
filterCoefficients = upSampleFactor * ...
    firls(filterOrder, filterFrequencies, filterMagnitudes) .* ...
    hanning(filterOrder + 1)';

% ---- Resample data, use faster implementation if no upsample needed
if upSampleFactor == 1
    filterCoefficients = [filterCoefficients(filterOrder/2+1:end) ...
                          zeros(1,length(data)-filterOrder-1) ...
                          filterCoefficients(1:filterOrder/2)]';
    data = downsample(ifft(fft(data).* conj(fft(filterCoefficients))),downSampleFactor);
else
    data = resample(data, upSampleFactor, downSampleFactor, filterCoefficients);
end

% ---- Ensure column format.
data = data(:);


% ---- Done.
return


