function [channelNames, channelVirtualNames, frameTypes, detectorList] = readchannelfile(channelFileName)
% READCHANNELFILE - read channel information from a file
%
% usage:
%
% [channelNames, virtualNames, frameTypes, detectorList] = readchannelfile(channelFileName)
%
% channelFileName   String. Name of channel file to be read.
%
% channelNames      Cell array of strings. Each element is the name of the
%                   channel to be read for the corresponding detector in
%                   detectorList.  
% virtualNames      Cell array of strings. Each element is the channel name that
%                   X-Pipeline is to use for determining detector location and
%                   orientation. This is used to simulate one detector using
%                   data from another. 
% frameTypes        Cell array of strings. Each element is the frame type of the
%                   corresponding channel in channelNames.
% detectorList      Cell array of strings. Each element is the (virtual)
%                   detector for the corresponding channel, as determined from
%                   virtualNames.

%----- Check and read channelFileName.
if (~isequal(channelFileName,''))

    %----- Read channel data into cell array.
    fileID = fopen(channelFileName);
    channelLines = textscan(fileID,'%s','delimiter','\n');
    channelLines = channelLines{1};
    fclose(fileID);
    
    %----- Number of channels.
    nChLines = length(channelLines);
    
    %----- Initialize variables.
    frameTypes = {};
    channelNames = {};
    channelVirtualNames = channelNames;

    %----- Loop over channels.
    for iChLine = 1:nChLines
        
        %----- Split on whitespace into separate elements of a cell array.
        lineContents = textscan(channelLines{iChLine},'%s');
        lineContents = lineContents{1};
        %----- Number of channel parameters.
        nChParams = length(lineContents);
        
        %----- Parse lineContents.  Format:
        %      2 params:
        %         channelName frameType
        %      3 params:
        %         channelName frameType channelVirtualName
        %      4 params:
        %         channelName frameType glitchChannelName glitchFrameType
        %      5 params:
        %         channelName frameType glitchChannelName glitchFrameType 
        %         channelVirtualName
        channelNames{iChLine} = lineContents{1};
        channelVirtualNames{iChLine} = channelNames{iChLine};
        frameTypes{iChLine} = lineContents{2};
        if (nChParams == 3)
            channelVirtualNames{iChLine} = lineContents{3};
        end

    end
   
else 

    error('No channel list file specified.  Exiting.')
 
end

% ---- Read detector name from first two characters of virtual channel name.
for iDetector=1:length(channelVirtualNames)
    tmp = textscan(channelVirtualNames{iDetector},'%s','delimiter',':');
    tmp = tmp{1};
    detectorList{iDetector} = tmp{1};
end

% ---- Done.
return

