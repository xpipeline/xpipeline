function [t hp hc freq a1 a2 phi shift] = lalinspiral_doc(parameters)
% LALINSPIRAL - Use LAL libraries to generate inspiral waveforms
%
% usage:
%
% [t hp hc freq a1 a2 phi shift] = lalinspiral(parameters)
%
% parameters  Cell array of waveform parameters
% {fs, mass1,mass2,iota,dist,generator,f_lower,spin1x,spin1y,spin1z,spin2x,spin2y,spin2z,coa_phase}
% where
%        fs = sampling frequency [Hz] (default 8192)  
%     mass1 = component mass 1 [solar masses] (default 1.4)
%     mass2 = component mass 2 [solar masses] (default 1.4)
%      iota = initial orbital inclination angle [rad] (default 0)
%      dist = distance [Mpc] (default 1)
% generator = generator family name as understood by LAL 
%             (default "TaylorT2threePN")
%   f_lower = waveform starting frequency [Hz] (default 40), 
%             start of waveform is cut off if it is longer than T0 
%    spin1x = adimensional spin of object 1 along x axis (default 0)
%    spin1y = adimensional spin of object 1 along y axis (default 0)
%    spin1z = adimensional spin of object 1 along z axis (default 0)
%    spin2x = adimensional spin of object 2 along x axis (default 0)
%    spin2y = adimensional spin of object 2 along y axis (default 0)
%    spin2z = adimensional spin of object 2 along z axis (default 0)
% coa_phase = inspiral phase at coalescence time [rad] (default 0)
%
% The full array is not mandatory, omitted parameters will be filled
% with default values. Physical spin of object is spin*mass^2*G/c, the
% adimensional spin is usually between 0 and 1 (1 correspond to the naked
% singularity limit)
%
%
%    t       Times at which the waveform is sampled, starting from zero.
%    hp      Waveform values in the plus polarization, in strain.  
%    hc      Waveform values in the cross polarization, in strain.
%    freq    Instanteneous frequency, in Hz.
%    a1      A1 amplitude, in strain
%    a2      A2 amplitude, in strain
%    phi     phase function, in radians
%    shift   polarization shift, in radians
%
% The relation between hp, hc and other phases and amplitudes is
%
%    hp = a1*cos(phi)*cos(shift) - a2*sin(phi)*sin(shift)
%    hc = a1*cos(phi)*sin(shift) + a2*sin(phi)*cos(shift)
%
% For more details on output parameters see documentation of
% SimulateCoherentGW in LAL Software Documentation. It is available at
% https://www.lsc-group.phys.uwm.edu/daswg/projects/lalsuite.html 

%#mex
%#external
