function data = xtaper(data, sampleFrequency, zeroTime, windowTime)
% XTAPER smoothly tapers to zero the ends of time series data.
%
% XTAPER applies a simple window to the beginning and end of a timeseries to
% taper the ends smoothly to zero. This may be helpful when filtering data to
% avoid filter transients.
%
% Code copied from XCONDITION@5849.
%
% usage:
%
%   fdata = xtaper(data, sampleFrequency, zeroTime, windowTime);
%
%   data                 Vector of input time series data.
%   sampleFrequency      Scalar. Sampling rate [Hz] of the data. 
%   zeroTime             Optional scalar. Time [s] to be zeroed at the beginning
%                        and end of the input timeseries. Default 0.5.
%   windowTime           Optional scalar. Time [s] to be windowed smoothly from
%                        zero to unity at the beginning and end of the input 
%                        timeseries using half a HANN window. Default 0.5.
%
%   fdata                Column vector of tapered time-domain data.  
%
% The default action is to zero out the first 0.5s, and suppress the next 0.5s
% with half a  HANN window. The same windowing in reverse is applied to the last
% 1s as well.  
%   
% $Id$

% ------------------------------------------------------------------------------
%    process and validate command line arguments
% ------------------------------------------------------------------------------

% ---- Check for sufficient command line arguments and assign defaults.
narginchk(2, 4);
if nargin < 4
    windowTime = 0.5;
end
if nargin < 3
    zeroTime = 0.5;
end

% ---- Number of samples to zero at each end.
Nz = sampleFrequency * zeroTime;
% ---- Number of samples to window at each end.
Nw = sampleFrequency * windowTime;
% ---- Sanity check.
if 2*(Nz+Nw)>length(data)
    error('Input time series is shorter than requested window length.')
end

% ---- Force one dimensional (column vector) data array.
data = data(:);


% ------------------------------------------------------------------------------
%    taper data
% ------------------------------------------------------------------------------

% ---- Suppress data edges, smoothly.  This window zeroes out the first 
%      zeroTime seconds, and suppresses the next windowTime seconds.    
hpfWindow = [zeros(Nz,1); hann(2*Nw); zeros(Nz,1)]; 
data(1:(Nz+Nw)) = data(1:(Nz+Nw)) .* hpfWindow(1:(Nz+Nw));
data(end-(Nz+Nw)+1:end) = data(end-(Nz+Nw)+1:end) .* hpfWindow((Nz+Nw)+1:end);


% ---- Done.
return

