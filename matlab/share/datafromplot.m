function [X,Y,C,S,name] = datafromplot(gca)
% datafromplot - extract data from all children of a plot axes.
%
% [X,Y] = datafromplot(gca)
%
%  gca  Handle to current axis (you can use the gca command directly as the
%       input argument). 
%
%  X    Cell array.  Each element is a column vector of x-axis data from
%       one child of 'gca'.
%  Y    Cell array.  Each element is a column vector of y-axis data from
%       one child of 'gca', matching the corresponding element of X.
%  C    Cell array.  Each element is a column vector of color data from
%       one child of 'gca', matching the corresponding element of X.  If
%       there is no color data for the corresponding element of X, then the
%       element of C will be [].
%  S    Cell array.  Each element is a column vector of size data from
%       one child of 'gca', matching the corresponding element of X.  If
%       there is no size data for the corresponding element of X, then the
%       element of S will be [].
%  name Cell array. Each element is a string giving the name of the child.

% ---- Get data from the plot.
h = get(gca,'children');

% ---- Prepare output data storage.
X = cell(1,length(h));
Y = cell(1,length(h));
C = cell(1,length(h));
S = cell(1,length(h));
name = cell(1,length(h));
 
% ---- Loop over children.
for jj = 1:length(h)

    % ---- Grab data for this child.
    try
        x = get(h(jj),'XData');
        y = get(h(jj),'YData');
    catch
        x = [];
        y = [];
    end
    
    % ---- Sort data and format into column vectors.
    x = x(:);
    y = y(:);
    [x,ind] = sort(x);
    y = y(ind);

    % ---- Try to get color data (if any).
    try 
        c = get(h(jj),'CData');
        c = c(:);    
        c = c(ind);
    catch
        c = [];
    end
    
    % ---- Try to get size data (if any).
    try 
        s = get(h(jj),'SizeData');
        s = s(:);    
        s = s(ind);
    catch
        s = [];
    end

    % ---- Try to get display name data (if any).
    try 
        dispName = get(h(jj),'DisplayName');
    catch
        dispName = '';
    end
    
    % ---- Pack into output cell array.
    X{jj} = x;
    Y{jj} = y;
    C{jj} = c;
    S{jj} = s;
    name{jj} = dispName;

end


% ---- Done.
return
