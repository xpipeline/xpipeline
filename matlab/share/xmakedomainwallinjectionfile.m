function xmakedomainwallinjectionfile(fileName,peakTime,peakTimeOffset,position,polarization,signal,detectors,tag1,val1,tag2,val2)
% xmakedomainwallinjectionfile - make injection file for domain wall signals

% Notes: This is trickier than standard GWB injections as the wall moves at
% v<<c. So injectins with xinjectsignal need to use the v=/=c optional
% input. Also the signal morphology depends on the IFO length and the
% angles of incidence, so we can't compute effective h+ and hx and just
% multiply by antenna responses.

% XMAKEDOMAINWALLINJECTIONFILE makes an injection file containing
% parameters that specify dark matter domain wall signals to be injected by
% XINJECTSIGNAL.
%
%   xmakedomainwallinjectionfile(fileName,peakTime,peakTimeOff,position, ...
%       polarization,signal,detectors,'parameter1',value1,...)
%
% fileName      String.  Name of output file.
% peakTime      Vector.  Peak time of injections at the center of the Earth
%               (GPS seconds).
% peakTimeOff   Scalar or vector of same size as peakTime.  Offset of
%               peakTime [s] used (I think) for off-source injections.  Set
%               to zero for on-source injections.
% position      Two-column array [polar angle, azimuthal angle] or string, 
%               specifying the sky direction of the GWB source.
% polarization  Vector or string, specifying the polarization angle of the
%               GWB signal.
% signal        Two-column cell array of strings.  Each row specifies one 
%               type of signal to be injected: signal{:,1} is a GWB "type"
%               recognized by xmakewaveform; signal{:,2} is a
%               tilde-delimited string with valid "params" for that signal
%               type or formatted for random parameter generation (see below).
% detectors     Cell array of strings. List of detectors in the network.
%               Only the first letter is used to identify the detector.
%               Needed for constructing the effective strain signal.
% 'parameter1',value1  Optional parameter-value string pairs.
%
% xmakedomainwallinjectionfile creates an injection file in the form understood by
% xinjectsignal.  Each line of this file completely specifies a single GWB
% injection.  There is one injection for each time in the peakTime
% argument.
%
% The signal input array second column must contain a tilde-delimited string.
% This string may contain with valid parameters for that signal type, or it can 
% specify that parameters can be generated randomly. See the helper function 
% ASSIGNPARAMETER for details on the formatting fro random parameter generation.
%
% There are two modes for assigning parameters to an injection, controlled
% by the optional parameter 'injectionMode'.  In the default mode
% ('random') each GWB injection is constructed by randomly drawing
% position, polarization, and signal values from the input arrays.  If the
% 'injectionMode' argument is specified as 'loop', then the injections are
% constructed by drawing parameters in order from the position,
% polarization, and signal arrays, looping repeatedly through the arrays as
% necessary.
%
% The other optional parameter is 'skyCoordinateSystem', with recognized
% values 'earthfixed' (default, for Earth-based coordinates in radians) and
% 'radec' (for right-ascension and declination, in degrees).
%
% The arguments 'position' and 'polarization' can also be strings, in which
% case parameter values are generated randomly.  For position recognized 
% values are: 
%   'isotropic' - sources are distributed isotropically over the sky
% For polarization recognized values are:
%   'uniform' - angles are distributed uniformly over [0,pi) 
%
% The output file will have one injection per row with 7 columns per
% detector in the network. Each set of 7 columns are as follows:
%   peak time at center of Earth (sec, GPS)
%   peak time at center of Earth (nanosec, GPS)
%   azimuthal sky position (radians, Earth-centered coordinates)
%   polar sky position (radians, Earth-centered coordinates)
%   polarization angle (radians, Earth-centered coordinates)
%   waveform type (string) as recognized by xmakewaveform
%   waveform parameter (string) as recognized by xmakewaveform
%
% Sample code for running this function:
%     fileName = 'test2.txt';
%     peakTime = 900000000 + [0:100:1000]';
%     peakTimeOffset = 0 ;
%     position = 'isotropic'
%     polarization = 'uniform'
%     signal{1,1} = 'domainwall-q';
%     signal{1,2} = '1e-18~1;1000;log~2e5;4e5;linear';
%     detectors = {'H','L','V'};
%     xmakedomainwallinjectionfile(fileName,peakTime,peakTimeOffset,position,polarization,signal,detectors)
%
% $Id$

% ---- Check for a valid number of command line arguments.
if ~((nargin==7) | (nargin==9) | (nargin==11))
    error('Wrong number of input arguments.');
end

% ---- Force peakTime to be a column vector.
peakTime = peakTime(:);

% ---- Set defaults for optional arguments, then parse optional arguments.
skyCoordinateSystem = 'earthfixed';
injectionMode = 'random';
if (nargin>=8) 
    switch lower(tag1)
        case 'skycoordinatesystem'
            skyCoordinateSystem = val1;
        case 'injectionmode'
            injectionMode = val1;
        otherwise
            error(['Argument not recognized.'])
    end
end
if (nargin>=10) 
    switch lower(tag2)
        case 'skycoordinatesystem'
            skyCoordinateSystem = val2;
        case 'injectionmode'
            injectionMode = val2;
        otherwise
            error(['Argument not recognized.'])
    end
end


% ---- Number of injections.
nInjection = length(peakTime);


% ---- Select sky position of injections.
% ---- First figure out if user has supplied a set of sky positions, or a
%      string.
if (isstr(position))
    switch position
        case 'isotropic'
            % ---- Sources distributed isotropically over the sky.
            theta = acos(2*rand(nInjection,1)-1);
            phi = 2*pi*rand(nInjection,1);
            position = [theta,phi];
        otherwise
            error(['position string not a recognized type.'])
    end
else
    switch injectionMode
        case 'random'
            % ---- Randomly select elements of input position array.
            index = unidrnd(size(position,1),nInjection,1);
            position = position(index,:);
        case 'loop'
            index = mod([1:nInjection]-1,size(position,1))+1;
            position = position(index,:);
        otherwise
            error(['injectionMode value not recognized.'])
    end
    % ---- Convert from right ascension and declination to Earth-based
    %      coordinates, if necessary.  (Need to do after selecting specific
    %      sky position for each injection, since conversion depends on
    %      sidereal time at injection.)
    switch lower(skyCoordinateSystem)
        case 'radec'
            % ---- Input sky coordinates are [right ascension, declination]
            %      in degrees.  Convert to Earth-fixed coordinates, in radians.
            [phi, theta] = radectoearth(position(:,1),position(:,2),peakTime+peakTimeOffset);
            position = [theta, phi];
        case 'earthfixed'
            ;
        otherwise
            error(['Value of skyCoordinateSytstem not recognized.']);
    end
end

% ---- Select polarization angle of injections.
% ---- First figure out if user has supplied a set of polarizations, or a
%      string.
if (isstr(polarization))
    switch polarization
        case 'uniform'
            % ---- Polarizations distributed uniformly.
            polarization = pi*rand(nInjection,1);
        otherwise
            error(['position string not a recognized type.'])
    end
else
    switch injectionMode
        case 'random'
            % ---- Randomly select elements of input position array.
            index = unidrnd(size(polarization,1),nInjection,1);
            polarization = polarization(index,:);
        case 'loop'
            index = mod([1:nInjection]-1,size(polarization,1))+1;
            polarization = polarization(index,:);
        otherwise
            error(['injectionMode value not recognized.'])
    end
end

    
% ---- GWB type.
% ---- Storage for signal type and parameters.
GWB_type = cell(nInjection,1);
GWB_params = cell(nInjection,1);
% ---- Select elements of input "signal" cell array.
switch injectionMode
    case 'random'
        % ---- Randomly select elements of input position array.
        index = unidrnd(size(signal,1),nInjection,1);
    case 'loop'
        index = mod([1:nInjection]-1,size(signal,1))+1;
    otherwise
        error(['injectionMode value not recognized.'])
end
for jInjection=1:nInjection
    GWB_type{jInjection,1}   = signal{index(jInjection),1};
    GWB_params{jInjection,1} = assignparameter(signal{index(jInjection),2});
end


% ---- Modify arrival time to compensate for v<c nature of the signal.
%
%      xinjectsignal assumes the signal to propagate at the speed of light.
%      There is an optional argument to reset the speed, but xdetection is
%      not set up to use it. So a standard xdetection run won't inject
%      domain wall signals properly. We compensate by resetting the COE
%      arrival times in the injection file. 
%
% ---- Speed of light (m/s).
speedOfLight = 299792458;
%
% ---- Speed of the domain wall.
speedOfWall = zeros(nInjection,1);
for jInjection=1:nInjection
    tmp = tildedelimstr2numorcell(GWB_params{jInjection,1});
    speedOfWall(jInjection) = tmp(3);
end
%
% ---- Position of each detector.
for iDet=1:length(detectors)
    detector{iDet,1} = LoadDetectorData(detectors{iDet});
end
%
% ---- Unit vector pointing towards source (one row per injection).
omega = [sin(theta).*cos(phi) , sin(theta).*sin(phi) , cos(theta)];
%
% ---- Reset "COE" arrival times separately for each detector to compensate
%      for speedOfWall. Time delay for incoming signal is defined wrt
%      center of Earth (t_det - t_coe). 
peakTime = repmat(peakTime,1,length(detectors));
for iDet=1:length(detectors)
    delayLight(:,iDet) = - (omega * detector{iDet}.V) ./ speedOfLight;
    delayWall(:,iDet)  = - (omega * detector{iDet}.V) ./ speedOfWall;
    peakTime(:,iDet)   = peakTime(:,iDet) + delayWall(:,iDet) - delayLight(:,iDet);
    % ---- Collect numeric data into single handy array.
    numData{iDet} = [floor(peakTime(:,iDet)), round(1e9*(peakTime(:,iDet)-floor(peakTime(:,iDet)))), ...
        position(:,2), position(:,1), polarization];
end


% ---- Write master injection log file.
fid = fopen(fileName,'w');

for jInjection=1:nInjection

    % ---- Write signal type and parameters to output file.
    for iDet = 1:length(detectors)
        fprintf(fid,'%d %d %e %e %e ',numData{iDet}(jInjection,:)');
        fprintf(fid,'%s ',GWB_type{jInjection});
        fprintf(fid,'%s~%e~%e~%s ',GWB_params{jInjection},theta(jInjection),phi(jInjection),detectors{iDet});
    end

    fprintf(fid,'\n');

end

% ---- Close injection log file.
fclose(fid);


% ---- Write separate injection log files for each detector (to allow the
%      domain wall analysis to be run in single-detector mode).
for iDet = 1:length(detectors)

    fid = fopen([detectors{iDet} '_' fileName],'w');

    for jInjection=1:nInjection
        % ---- Write signal type and parameters to output file.
        fprintf(fid,'%d %d %e %e %e ',numData{iDet}(jInjection,:)');
        fprintf(fid,'%s ',GWB_type{jInjection});
        fprintf(fid,'%s~%e~%e~%s ',GWB_params{jInjection},theta(jInjection),phi(jInjection),detectors{iDet});
        fprintf(fid,'\n');
    end
    
    fclose(fid);

end


% ---- Done.
return;


% ------------------------------------------------------------------------------
%    Helper function for generating random parameter values. 
% ------------------------------------------------------------------------------

function parameters = assignparameter(parameters,seed)
% ASSIGNPARAMETER - helper function for xmakedomainwallinjectionfile
%
% ASSIGNPARAMETER reads tilde-delimited strings used to hold injection
% parameters and searches for parameters that match the format used to indicate
% parameters to be selected at random from a specified distribution.  
% ASSIGNPARAMETER generates values for these parameters from the appropriate
% distributions and returns a new tilde-delimited parameters string containing 
% these values.
%
% usage:
%
% parametersNew = assignparameter(parameters,seed)
%
% parameters     Tilde-delimited string containing parameters for one injection.  
% seed           Optional scalar. Seed for random number generation.
%
% parametersNew  Tilde-delimited string containing parameters for one injection.
%                Identical to parameters for any element that does not contain a
%                semicolon (;). Elements which contain a semicolon are replaced
%                by a random number generated from a distribution determined by
%                the value of the original element.
% 
% Here are the recognised element formats that indicate that a random parameter
% is to be generated, and the corresponding distributions used:
%
% xmin;xmax;linear : uniform on [xmin,xmax]
%     x = (xmax-xmin)*rand + xmin;
%
% xmin;xmax;log    : uniform in log(x) on [xmin,xmax]
%     x = exp((log(xmax)-log(xmin))*rand + log(min));
%
% xmin;xmax;alpha;pow : power-law distribution p(x) = constant * x^alpha on [xmin,xmax]
%     x = exp((log(xmax)-log(xmin))*rand + log(min));
%
% x1;...;x10;mass  : 2D Gaussian distributed within limits.
%     Input parameters are: mean, standard deviation, minimal and maximal 
%     mass for the first compact object, then the second compact 
%     object, and finally the minimal and maximal total mass
%     for the binary. All masses are in solar masses.
%
% s1;...;s6;spin   : spins with uniformly distributed magnitude and isotropic
% direction within limits.  
%      The first three input parameters are for the first compact object
%      at the inspiral start: the minimal spin magnitude, the
%      maximal spin magnitude, the minimal value of the cosine
%      of the tilt angle with respect to the orbital axis. The
%      other three components are the same parameters for the
%      second compact object. All spins are given by the
%      adimensional spin parameter.
%
% $Id$ 

% ------------------------------------------------------------------------------
% % ---- Code snippet for testing random-parameter generation.
% peakTime = [1:1000]; peakTimeOff = zeros(size(peakTime)); 
% position = 'isotropic';
% polarization = 'uniform';
% fileName = 'test.txt'; 
% % ---- Uncomment the distribution you wish to test.
% signal = {'foo','0;1;linear'};
% % signal = {'foo','0.001;0.01;log'};
% % signal = {'foo','1;100;-1;pow'}; %-- equivalent to '1;100;log'
% % signal = {'foo','1;100;2;pow'};
% % signal = {'foo','1.4;0.1;1;3;5;1;4;10;6;12;mass'};
% % signal = {'foo','0;1;0.9;0.5;0.9;0.8;spin'};
% xmakedomainwallinjectionfile(fileName,peakTime,peakTimeOff,position,polarization,signal);
% ------------------------------------------------------------------------------


% ---- Check to see if parameter string contains a semicolon (;) -- this
%      indicates the parameter is to be jittered.  
if isempty(strfind(parameters,';'));    

    % ---- There is nothing to be done. Return parameters string un-modified.
    return

else
    
    % ---- At least one parameter element contains a ';', which indicates it is
    %      to be jittered. Split parameters into a cell array, check each
    %      element, and randomize elements as necessary.

    % ---- Set the random number generator seed based on the name of
    %      the input file.
    if nargin>1
        randn('state',seed)
        % randn('state',sum(inFile))
    end

    % ---- Split parameters into a cell array by '~'.
    parametersCell = tildedelimstr2numorcell(parameters); 
    % ---- Initialise a new cell array to hold the final randomised parameters.
    parametersCellNew = cell(size(parametersCell)); 

    % ---- Loop over elements of parametersCell, checking each for a ';'.
    for jj = 1:length(parametersCell)
        % ---- Extract current element.        
        x = parametersCell{jj};
        % ---- Check for a ';'.
        idx = strfind(x,';');
        if isempty(idx)
            % ---- No jittering - do nothing.
            parametersCellNew{jj} = x;
        else
            % ---- "Jitter" parameter; i.e., select random value for the
            %      parameter. The distribution to be used depends on how many
            %      semicolons we have and on the last data value.
            % ---- Separate string into components.
            a = cell(length(idx)+1,1);
            b = 1;
            ii = 0;
            while ~isempty(x)
                ii = ii + 1;
                [a{ii},x] = strtok(x,';');
            end
            % ---- Pull off last value a jittering mode.
            jitterMode = lower(a{end});
            a = a(1:(end-1));
            % ---- Everyting else should be convertable to numbers.
            a = str2double(a);
            % ---- Check length of a.
            if length(a)==2
                % ---- Select parameter from a uniform distribution between two limits,
                %      in linear or log space.
                switch jitterMode
                    case 'linear'
                        parametersCellNew{jj} = (a(2)-a(1))*rand(1)+a(1);
                    case 'log'
                        parametersCellNew{jj} = exp((log(a(2))-log(a(1)))*rand(1)+log(a(1)));
                    otherwise
                        error(['Jittering mode ' jitterMode ' not recognised.']);
                end
            elseif length(a)==3
                switch jitterMode
                    case 'pow'
                        % ---- Compute cumulative distribution function for 
                        %      p(x) = k x^alpha on xmin <= x <= xmax.
                        xmin  = a(1);
                        xmax  = a(2);
                        alpha = a(3);
                        if alpha == -1
                            % ---- alpha = -1:
                            %      p = k x^(-1) ; xmin <= x <= xmax
                            %      c = \int_xmin^x p 
                            %        = k ln(x)|^x_xmin
                            %        = k ln(x/xmin)
                            %      c(xmax) = 1 -> 1 = k ln(xmax/xmin)
                            %                  -> k = 1 / [ln(xmax/xmin)
                            %      c = ln(x/xmin) / ln(xmax/xmin)  ... same as 'log'
                            %      x = c^-1(R) =   exp(xmin*ln(xmax/xmin) * R)
                            %                  =   (xmax/xmin)^xmin * exp(R)
                            parametersCellNew{jj} = (xmax/xmin)^xmin * exp(rand(1));
                        else
                            % ---- alpha ~= -1:
                            %      p = k x^alpha ; xmin <= x <= xmax
                            %      c = \int_xmin^x p 
                            %        = k [x^(alpha+1) - xmin^(alpha+1)]/(alpha+1)
                            %      c(xmax) = 1 -> 1 = k [xmax^(alpha+1)-xmin^(alpha+1)] / (alpha+1)                         
                            %                  -> k = (alpha+1) / [xmax^(alpha+1)-xmin^(alpha+1)]
                            %      c = [x^(alpha+1) - xmin^(alpha+1)] / [xmax^(alpha+1)-xmin^(alpha+1)]
                            %      x = c^-1(R) = {[xmax^(alpha+1)-xmin^(alpha+1)] * R + xmin^(alpha+1) }^(1/(alpha+1))
                            % 
                            parametersCellNew{jj} = ((xmax^(alpha+1)-xmin^(alpha+1)) * rand(1) + xmin^(alpha+1) )^(1/(alpha+1));
                        end
                    otherwise
                        error(['Jittering mode ' jitterMode ' not recognised.']);
                end
            elseif length(a)==6
                % ---- Select parameter from a distribution used for binary
                %      component spins. 
                %      Input parameters should be a tilde-delimited string. The
                %      first three components are for the first compact object
                %      at the inspiral start: the minimal spin magnitude, the
                %      maximal spin magnitude, the minimal value of the cosine
                %      of the tilt angle with respect to the orbital axis. The
                %      other three components are the same parameters for the
                %      second compact object. All spins are given by the
                %      adimensional spin parameter.
                % ---- Check that the spin parameters are sensible.
                if a(1) > a(2) || a(4) > a(5) || any(a>1) || ...
                        any(a([1 2 4 5])<0) || any(a([3 6])<-1)
                    error(['Spin distribution parameters ' num2str(a) ' do not make sense.']);
                end
                % ---- Reset random number generator for backwards compatibility
                %      with previous function xjitterspin.
                if nargin > 1
                    rand('twister',seed);
                end
                % ---- Generate random spin parameters for first compact object.
                magS1 = a(1)+(a(2)-a(1))*rand;
                thetaS1 = acos((1-a(3))*rand+a(3));
                phiS1 = 2*pi*rand;
                s1x = magS1*sin(thetaS1)*cos(phiS1);
                s1y = magS1*sin(thetaS1)*sin(phiS1);
                s1z = magS1*cos(thetaS1);
                % ---- Generate random spin parameters for second compact object.
                magS2 = a(4)+(a(5)-a(4))*rand;
                thetaS2 = acos((1-a(6))*rand+a(6));
                phiS2 = 2*pi*rand;
                s2x = magS2*sin(thetaS2)*cos(phiS2);
                s2y = magS2*sin(thetaS2)*sin(phiS2);
                s2z = magS2*cos(thetaS2);
                % ---- Output paramters.
                parametersCellNew{jj} = [num2str(s1x) '~' num2str(s1y) '~' num2str(s1z) '~' ...];
                                         num2str(s2x) '~' num2str(s2y) '~' num2str(s2z)];
            elseif length(a)==10
                % ---- Select parameter from a distribution used for binary
                %      component masses.                 
                %      Input parameters should be a tilde-delimited string with 
                %      components: mean, standard deviation, minimal and maximal 
                %      mass for the first compact object, then the second compact 
                %      object, and finally the minimal and maximal total mass
                %      for the binary. All masses are in solar masses.
                % ---- Separate mass parameters.
                m1 = a(1:4);
                m2 = a(5:8);
                mt = a(9:10);
                % ---- Generate random mass parameters.
                newm1 = 0;
                newm2 = 0;
                k = 0;
                while k<100 && ( newm1 < m1(3) || newm1 > m1(4) || ...
                                 newm2 < m2(3) || newm2 > m2(4) || ...
                                 newm1+newm2 < mt(1) || newm1+newm2 > mt(2))
                    newm1 = m1(1) + m1(2)*randn(1);
                    newm2 = m2(1) + m2(2)*randn(1);
                    k = k + 1;
                end
                if k >= 100
                    error(['The mass jittering did not converenge. ' ...
                           'Are the jittering parameters consistent?']);
                end
                parametersCellNew{jj} = [num2str(newm1) '~' num2str(newm2)];
            else
                error(['Jittering requires input of 2, 6, or 10 numerical parameters.']);
            end

        end
    end
    
    % ---- Convert new parameters from cell array to tilde-delimited string,
    %      over-writing original input parameters variable. 
    parameters = numorcell2tildedelimstr(parametersCellNew);
    
end
