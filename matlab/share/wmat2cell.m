function m = wmat2cell(m, extratest)
% WMAT2CELL Wrapper for commonly encountered form of mat2cell
%
% The following two calls are completely equivalent:
%
%   if extratest && ~iscell(m),
%     m =  mat2cell(m, size(m, 1), size(m, 2));
%   end
%
%   m = wmat2cell(m, extratest);
%
% If extratest is not specified, it is taken to be true.
%
% See also MAT2CELL.

% Leo C. Stein <lstein@ligo.caltech.edu>

% $Id: wmat2cell.m 2984 2010-05-05 13:17:07Z spxme1 $

narginchk(1,2);

if nargin < 2
  extratest = true;
end

if extratest && ~iscell(m)
  m = mat2cell(m, size(m,1), size(m,2));
end
