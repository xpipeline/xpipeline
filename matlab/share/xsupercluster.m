function [superClusters, boundingBox, nPixels, likelihood, significance, ...
    listIndex] = xsupercluster(clusterArrayCell, likelihoodType, detectionStat, method)
% xsupercluster - merge separate lists of X-Pipeline event clusters.
%
% usage:
%
%   [Z, BB, nP, LV, SI, LI] = xsupercluster(X,L,S,M)
%
% X    Cell array. Each element is one list of events to be merged. See below.
% L    Cell array. Each element is a string giving the name of the
%      corresponding likelihood given in X.
% S    String. Name of likelihood to use as the detection statistic (i.e. loudness)
% M    String. Name of superclustering method to use:
%        'standard' compares all the pairs of bounding boxes _between_ all the
%          pairs of input event lists.
%        'fastquadratic' (coded in mex file) combines all the event lists into
%          one and compares all the pairs of bounding boxes _within_ the 
%          combined event list. The result can be slightly different than those
%          from the 'standard' method. Note that this method works even for 
%          length(X)==1.
%        'fast' (coded in mex file) uses a fancy algorithm based on a line sweep 
%          and binary search trees, which lowers the complexity from N^2 to 
%          ~NlogN. Should give _exactly_ the same result as 'fastquadratic'.
%          Note that this method works even for length(X)==1.
%
% Z    Array. List of merged event clusters ("superclusters") in array (not
%      struct) format.
% BB   Array. Each row is the bounding box of the event in the corresponding row
%      of Z.
% nP   Vector. Each row is the number of pixels in the event in the 
%      corresponding row of Z.
% LV   Array. Each row is the likelihood values of the event in the
%      corresponding row of Z.
% SI   Vector. Each row is the significance of the event in the corresponding
%      row of Z.
% LI   Vector. Each row is the list index of the event in the corresponding
%      row of Z. (The list index is the index of the analysisTimes variable in 
%      XDETECTION.M for which this event was generated; i.e., it tells which
%      FFT length was used to generate the event.)
%
% Algorithm: Event clusters whose time-frequency bounding boxes overlap (defined
% as overlap area > 0) are combined into a single event.  The "supercluster" 
% event is the constituent event with the largest detection statistic 
% significance (i.e., clusters overlapping a louder cluster are dropped).
%
% This function uses the flat array format for events.  Each element of X
% is a 2D array in which each row represents one event.  The columns are
% as described in clusterTFmapNew.

% $Id$

% ---- Check number of inputs and assign defaults.
narginchk(3,4)
if nargin == 3
    method = 'fast';
end

% ---- Check if we need to track the list index (if it is requested by the
%     user).
if nargout > 1
    trackListIndex = true;
else
    trackListIndex = false;
end

% ---- Remove cells that have empty cluster arrays, needed by standard
%      method or to keep track of input list.
if strcmp(lower(method),'standard') || trackListIndex
    iCellClean = 0;
    clusterArrayClean={};
    for iCell = 1:length(clusterArrayCell)
        if not(isempty(clusterArrayCell{iCell}))
            iCellClean = iCellClean + 1;
            clusterArrayClean{iCellClean} = clusterArrayCell{iCell};
            % ---- Keep track of what was the original cell.
            listIndexCell{iCellClean} = iCell*ones(size(clusterArrayCell{iCell},1),1);
        end
    end
    clusterArrayCell = clusterArrayClean;
    clear clusterArrayClean;
end


% ---- Preparatory.
superClusters = [];
boundingBox = [];
nPixels = [];
likelihood = []; 
significance = [];
if trackListIndex
    listIndex = [];
end


switch lower(method)

    case {'fastquadratic','fast'}

        % ---- Dump all events into one array, this simplify the processing, but
        %      events with overlapping bounding boxes (but not clusters) from the
        %      same event list will also be superclustered
        clusterArrayCell = vertcat(clusterArrayCell{:});
        if isempty(clusterArrayCell)
            return
        end

        % ---- Record the event list from which each event comes
        if trackListIndex
            listIndexCell = vertcat(listIndexCell{:});
        end

        % ---- NOTE: For the fast method, all variables below with "Cell" in the
        %      name are actually normal arrays. These variables used to be cell
        %      arrays for the "standard" method below, and have not been renamed.

        % ---- Convert cluster data from flat array format to separate 
        %      variables format.
        [boundingBoxCell, centralTimeCell, centralFrequencyCell, nPixelsCell, ...
            likelihoodCell] = xclusterarraytoproperties(clusterArrayCell);

        % ---- Compute significance of detection statistic for each event in each 
        %      list.
        % ---- Find the desired detection statistic in the list of likelihoods
        iDetStat = find(strcmp(detectionStat,likelihoodType));
        significanceCell = likelihoodsignificance(likelihoodCell(:,iDetStat), ...
                               likelihoodType{iDetStat},nPixelsCell);

        % ---- Supercluster events.
        switch lower(method)
            case 'fast'
                % ---- Remove redundant clusters and keep only the loudest of exact
                %      matches and the keep only the top (loudest) overlapping
                %      rectangles.
                % ---- Find the order of significances.
                [sortedSig sortedSigOrder] = sort(significanceCell,'ascend');
                % ---- Take the loudest (last in sorted order) bounding box from
                %      those that are exactly equal.
                [uniqueBB uniqueBBorder] = unique(boundingBoxCell(sortedSigOrder,:),'rows','last');
                % ---- Keep loudest overlapping rectangles.
                acceptCellUnique = logical(fastsupercluster([uniqueBB sortedSig(uniqueBBorder)]));
                % ---- Trace back to the indices of the rectangles before the 
                %      sorting and uniqueness step.
                acceptCell = sortedSigOrder(uniqueBBorder(acceptCellUnique));
            case 'fastquadratic'
                acceptCell = logical(fastquadraticsupercluster([boundingBoxCell significanceCell]));
            otherwise
                error(['Unrecognized superclustring method: ' method])
        end

        % ---- Copy accepted clusters to output.
        boundingBox = boundingBoxCell(acceptCell,:);
        centralTime = centralTimeCell(acceptCell,:);
        centralFrequency = centralFrequencyCell(acceptCell,:);
        nPixels = nPixelsCell(acceptCell,:);
        likelihood = likelihoodCell(acceptCell,:);
        significance = significanceCell(acceptCell,:);
        if trackListIndex
            listIndex = listIndexCell(acceptCell,:);
        end
        % ---- Combine separate variables into single array.
        superClusters = xclusterpropertiestoarray(boundingBox, centralTime, ...
                            centralFrequency, nPixels, likelihood);

    case 'standard'

        % ---- Number of event lists.
        N = length(clusterArrayCell);

        if (N>=1)

            % ---- Convert cluster data from flat array format to separate 
            %      variables format.
            for ii = 1:N
                [boundingBoxCell{ii}, centralTimeCell{ii}, centralFrequencyCell{ii}, ...
                    nPixelsCell{ii}, likelihoodCell{ii}] ...
                    = xclusterarraytoproperties(clusterArrayCell{ii});
            end

            % ---- Make sure each set of clusters has the same number of likelihoods.
            numberOfLikelihoods = size(likelihoodCell{1},2);
            for ii = 2:N
                if (size(likelihoodCell{ii},2) ~= numberOfLikelihoods)
                    error(['Each element of input cell array must have the ' ...
                        'same number of columns: ' num2str(size(likelihoodCell{ii},2)) ...
                          ' ~= ' num2str( numberOfLikelihoods)]);
                end
            end

            % ---- Compute significance of detection statistic for each event in each 
            %      list.
            % ---- Use first likelihood in likelihoodType as the detection statistic
            for ii = 1:N
                significanceCell{ii} = likelihoodsignificance(likelihoodCell{ii}(:,1), ...
                    likelihoodType{1},nPixelsCell{ii});
            end

            % ---- Indices of event in each list to be dropped and kept.
            rejectCell = cell(1,N);
            acceptCell = cell(1,N);

            % ---- Mark for rejection clusters that overlap louder clusters from 
            %      another list.
            for ii = 1:(N-1)
                for jj = (ii+1):N
                    area = rectint(boundingBoxCell{ii},boundingBoxCell{jj});
                    [reject1, reject2] = indexesToReject(significanceCell{ii}, ...
                        significanceCell{jj}, (area > 0));
                    rejectCell{ii} = union(rejectCell{ii},reject1);
                    rejectCell{jj} = union(rejectCell{jj},reject2);
                end
            end

            % ---- Count clusters that are to be kept.
            numberKept = 0;
            for ii = 1:N
                acceptCell{ii} = setdiff(1:length(significanceCell{ii}),rejectCell{ii});
                numberKept = numberKept + length(acceptCell{ii});
            end

            % ---- Copy accepted clusters to output.
            boundingBox = zeros(numberKept,4);
            centralTime = zeros(numberKept,1);
            centralFrequency = zeros(numberKept,1);
            nPixels = zeros(numberKept,1);
            likelihood = zeros(numberKept,numberOfLikelihoods);
            significance = zeros(numberKept,1);
            listIndex = zeros(numberKept,1);
            istart = 0;  %-- temporary counter
            iend = 0;  %-- temporary counter
            for ii = 1:N
                if ~isempty(acceptCell{ii})
                    istart = iend + 1;
                    iend = iend + length(acceptCell{ii});
                    boundingBox(istart:iend,:) = boundingBoxCell{ii}(acceptCell{ii},:);
                    centralTime(istart:iend,:) = centralTimeCell{ii}(acceptCell{ii},:);
                    centralFrequency(istart:iend,:) = centralFrequencyCell{ii}(acceptCell{ii},:);
                    nPixels(istart:iend,:) = nPixelsCell{ii}(acceptCell{ii},:);
                    likelihood(istart:iend,:) = likelihoodCell{ii}(acceptCell{ii},:);
                    significance(istart:iend,:) = significanceCell{ii}(acceptCell{ii},:);
                    listIndex(istart:iend,:) = listIndexCell{ii}(acceptCell{ii},:);
                end
            end
            % ---- Combine separate variables into single array.
            superClusters = xclusterpropertiestoarray(boundingBox, centralTime, ...
                centralFrequency, nPixels, likelihood);

        end  %-- if N>=1

    otherwise
        
        error(['Unrecognized superclustering method: ' method])

end

% ---- Done.
return


% NOT FUNCTIONAL: this code is to handle the case where the input 
% events are in struct format rather than a flat array.

% Each element of the cell array X must have the following fields:
% boundingBox (to determine which events overlap) and significance 
% (to determine which to keep).  If significance is not defined, 
% then the following fields must be defined so that significance
% can be computed: likelihood,likelihoodType,size.  In this case the
% significance field is added to the output event list.
%
% $Id$

% % ---- Number of event lists.
% N = length(X);

% % ---- Compute significance of detection statistic for each event in each 
% %      list, if not already defined.
% for ii = 1:N
%     if ~isfield(X{ii},'significance')
%         % ---- Check that fields required to compute significance are defined.
%         if (isfield(X{ii},'likelihood') & isfield(X{ii},'likelihoodType') & isfield(X{ii},'size'))
%             X{ii}.significance = likelihoodsignificance(X{ii}.likelihood,X{ii}.likelihoodType{1},X{ii}.size);
%         else
%             error(['Input event lists must contain field significance or ' ...
%                 'fields likelihood, likelihoodType, size']);
%         end
%     end
% end

% % ---- Number of events in each list.
% nEvents = zeros(1,N);
% for ii = 1:N
%     nEvents(ii) = size(X{ii}.likelihood,1);
% end

% % ---- Indices of event in each list to be dropped.
% rejectCell = cell(1,N);

% % ---- Mark for rejection clusters that overlap louder clusters from another
% %      list.
% for ii = 1:(N-1)
%     for jj = (ii+1):N
%         area = rectint(X{ii}.boundingBox,X{jj}.boundingBox);
%         [reject1, reject2] = indexesToReject(significance{ii}, significance{jj}, (area > 0));
%         rejectCell{ii} = union(rejectCell{ii},reject1);
%         rejectCell{jj} = union(rejectCell{jj},reject2);
%     end
% end

% % ---- Copy surviving events to output list.
% acceptCell = cell(1,N);
% for ii = 1:N
%     acceptCell{ii} = setdiff([1:nEvents{ii}],rejectCell{ii});
% end

% names = fieldnames(X{1});
% for jj = 1:length(names)
%     data = 
% end


