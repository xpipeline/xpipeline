function [filt] = lineremoval(data,srate,fline)
ts=1/srate;
%tau=2.2;
%gain=(9*ts/tau)*(9*ts/tau); 
%tau_2=0.0000014*tau;
%fline=wanderfind(data,srate,5,40,2000); 
if exist('filt')==1
    clear filt
end

for n=1:length(fline)
    if fline(n)>1000
        tau=1.1;
    else
        tau=2.2;
    end
    gain=(9*ts/tau)*(9*ts/tau); 
    tau_2=0.0000014*tau;
    if exist('filt')==1
        fdata=filt';
%         fprintf('A: %i\n',length(fdata))
    else
        fdata=data;
%         fprintf('B: %i\n',length(fdata))
    end
    [b,a]=cheby1(1,3,[(fline(n)-20)/(srate/2) (fline(n)+20)/(srate/2)]);
    fdata=filter(b,a,fdata);% elliptic filter
    sv=miwave_fulltune_construct( ...
				  srate, ... % sampling rate
				  tau, ... % response time
				  fline(n), ... % starting frequency 
				  1, ... % when to turn on locking
				  gain, ... 
				  tau_2);   % feedback time constant

    [amp,freq,filt,sv]=...
    miwave_fulltune_iterate( ...
			     fdata,length(fdata),sv);
    filt=filter(a,b,filt);
%track(n,:) = fdata - filt';
%filt=filt';
end
