function [Y,triggerSet] = xsuperclustercombine(X,likelihoodTypes,dT,dF,combineFlag)
% XSUPERCLUSTERCOMBINE - supercluster x-pipeline trigger list by combining triggers
%
% usage:
%
%  [Y,sets] = xsuperclustercombine(X,L,dT,dF,flag)
%
%  X      X-Pipeline cluster struct. Must have (at least) fields 'boundingBox' 
%         and 'significance'.
%  L      Cell array of strings. Names of the likelihoods in X.
%  dT     Optional scalar. Time coincidence window [sec] to be used in computing
%         cluster overlaps. Default 0.
%  dF     Optional scalar. Freq coincidence window [Hz] to be used in computing
%         cluster overlaps. Default 0.
%  flag   Optional boolean. If true then overlapping clusters will be combined;
%         otherwise they won't (Y=X; useful for testing overlaps). Default true.
%
%  Y      X-Pipeline cluster struct composed of clusters obtained by combining 
%         clusters from X that have non-zero time-frequency overlap. The 
%         combination is done by XCOMBINECLUSTERS.
%  sets   Cell array. Each element is a vector of indices of triggers (rows) in
%         X that are clustered together.
%
% Two triggers are clustered together if their bounding boxes have non-zero 
% overlap as defined by RECTINT. When computing overlap the duration and 
% bandwidth of the bounding boxes are increased by dT and dF.
%
% If X contains only 0 or 1 triggers, then Y=X and sets={[]} or sets={[1]}.
%
% The combination of overlapping triggers is performed by XCLUSTERCOMBINE.
%
% $Id$ 

% ---- Checks on inputs.
narginchk(2,5);

% ---- Assign defaults if needed.
if nargin<5
    combineFlag = true;
end
if nargin<4
    dF = 0;
end
if nargin<3
    dT = 0;
end

% ---- Number of input triggers.
N = size(X.boundingBox,1);

% ---- Only proceed if we have more than one input trigger.
if N>1

    % ---- Add coincidence window to temporary copy of the bounding boxes.
    boundingBox      = X.boundingBox;
    boundingBox(:,3) = boundingBox(:,3) + dT;
    boundingBox(:,4) = boundingBox(:,4) + dF;

    % ---- Determine all overlap areas.
    area = triu(rectint(boundingBox,boundingBox),1);

    % ---- Determine all pairs [I,J] of overlaps.
    [I,J] = ind2sub([N,N],find(area));

    % ---- Group all connected overlapping pairs.
    icluster = 0; %-- counter of clusters
    while ~isempty(I)
        iN = I(1);
        % ---- Find all pairs containing trigger iN.
        idx = find(I==iN | J==iN);
        if ~isempty(idx)
            % ---- Trigger iN has been paired with at least one other. Collect all 
            %      pairs containing iN into one set and remove them from the master
            %      list.
            icluster = icluster + 1;
            triggerSet{icluster} = unique([I(idx);J(idx)]);
            I(idx) = [];
            J(idx) = [];
            % ---- Collect all pairs containing the _other_ triggers in this set.
            checkedTriggers = iN;
            otherTriggers = setdiff(triggerSet{icluster},checkedTriggers);
            while ~isempty(otherTriggers) && ~isempty(I)
                idx = find(I==otherTriggers(1) | J==otherTriggers(1));
                if ~isempty(idx)
                    triggerSet{icluster} = unique([triggerSet{icluster};I(idx);J(idx)]);
                    I(idx) = [];
                    J(idx) = [];
                end
                checkedTriggers = [checkedTriggers,otherTriggers(1)];
                otherTriggers = setdiff(triggerSet{icluster},checkedTriggers);
            end
        end    
    end

    % ---- If we have had anyoverlapping triggers then 'triggerSet' will be a
    %      variable in our workspace. Then we must call xclustercombine.
    if exist('triggerSet','var')

        % ---- Find all triggers not paired with any other.
        unpairedTrigger = setdiff([1:N],cell2mat([triggerSet(:)]));

        % ---- Add unpaired triggers as single-trigger "sets".
        for ii = 1:length(unpairedTrigger)
            triggerSet{end+1} = unpairedTrigger(ii);
        end

        % ---- We now have all triggers clustered into sets. Pass into the function
        %      that will combine the triggers.
        if combineFlag
            Y = xclustercombine(X,likelihoodTypes,triggerSet);
        else
            Y = X;
        end

    else

        % ---- We have no overlapping triggers, so there is no need to combine
	%      any. Copy the input trigger list to the output and ensure 
        %      triggerSet is defined.
        Y = X;
        triggerSet = mat2cell([1:N],1,ones(1,N));

    end

elseif N==1

    Y = X;
    triggerSet = {[1]};

elseif N==0

    Y = X;
    triggerSet = {[]};

end

% ---- Done.
return


