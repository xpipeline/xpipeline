function [Fp, Fc, Fb] = antennaPatterns(channels, skyPositions)
%antennaPatterns        Calculate the antenna coefficients for the
%                       detectors associated with the list of channels at
%                       the given sky position.
%
%    [Fp, Fc, Fb] = antennaPatterns(channels, [theta, phi])
%
% Inputs:
%    channels    Vector cell array of string channel names, or vector cell
%                array of 3x3 matrices of detector response tensors.
%    theta       Column vector of polar angles measured from z-axis.
%    phi         Column vector of angles measured in xy-plane with
%                positive defined as counter-clockwise as viewed from
%                the positive z-axis.
%
% Outputs:
%    Fp          Matrix of size (n sky positions)x(n channels)
%    Fc          Matrix of size (n sky positions)x(n channels)
%    Fb          Matrix of size (n sky positions)x(n channels)
%
% See also    ComputeAntennaResponse
%
% Revision History
%  8 July 2005    Leo Stein     Initial write, based on Antony's antenna
%
% $Id$

% Derived variables

nSkyPositions = size(skyPositions,1);

theta = skyPositions(:,1);
phi   = skyPositions(:,2);
psi   = zeros(nSkyPositions,1);

nChannels = length(channels);

% Prepare storage for output
Fp = zeros(nSkyPositions, nChannels);
Fc = zeros(nSkyPositions, nChannels);
Fb = zeros(nSkyPositions, nChannels);

% If channels elements are strings then keep only first two characters
if (ischar(channels{1}))
    for iChannel = 1:nChannels
        channels{iChannel} = channels{iChannel}(1:min(2,length(channels{iChannel})));
    end
end

for iChannel = 1:nChannels
    % The detector is matched by the first character of the channel name.
    [ Fp(:,iChannel) Fc(:,iChannel) Fb(:,iChannel)] = ...
                     ComputeAntennaResponse(phi, theta, psi,...
                                    channels{iChannel});
end
