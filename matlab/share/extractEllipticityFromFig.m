function [freq,ell] = extractEllipticityFromFig(filepath)
    % ---- Helper function to read figfile and output freq and
    %      ellipticity data points.
    % ---- Clear variables.
    X=[];
    Y=[];
    C=[];
    S=[];
    handle = open(filepath);
    [X,Y,C,S] = datafromplot(gca);
    close(handle);
    % ---- For xpipeline Fratio figfiles we expect freq and ell
    %      data to be in 1st child.
    freq = X{1};
    ell  = Y{1};
    % ---- Do some sanity checking.
    if length(freq) ~= length(ell)
        error('freq and ell should both have same number of elements')
    end
end

