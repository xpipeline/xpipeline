function significance = likelihoodsignificance(likelihood,likelihoodType, ...
    numberOfFrequencyBins,numberOfDetectors)
%
% likelihoodsignificance - Compute statistical significance of measured
% likelihood values, assuming Gaussian noise statistics (chi-squared
% statistics). 
%
%   significance = likelihoodsignificance(likelihood,likelihoodType, ...
%       numberOfPixels,numberDetectors)
%
%   likelihood      Vector of likelihood values.
%   likelihoodType  String describing type of likelihood.  Recognized
%                   values are 'crossenergy', 'hardconstraint', 
%                   'nullenergy', 'plusenergy', 'standard', 'totalenergy'.
%   numberOfPixels  Vector or scalar with number of pixels or frequency
%                   bins used for the calculation of likelihood.  Must be
%                   the same size as likelihood, unless a scalar, in which
%                   case the same value is use for all likelihoods.
%   numberDetectors Scalar.  Number of detectors in network.  Needed if
%                   'nullenergy' or 'totalenergy' likelihood is used, 
%                   otherwise optional and ignored.
%
% The significances are computed assuming a single trial in Gaussian noise,
% assuming chi-squared statistics:
%
%   significance = -log(1-chi2cdf(2*likelihood,DOF));
% 
% Here DOF (degrees of freedom) is 2 * N_f * N_p, where N_f is the number 
% of frequency bins or pixels in the event, and N_p is the number of
% dimensions in the projection to compute the likelihood (1 for hard, 2 for
% standard, numberOfDetectors-2 for null, and numberOfDetectors for total).
% A series expansion is used to compute the significance if ch12cdf fails;
% see logIncompleteGammaSeries. 
% 
% Note that this "significance" ignores any trials factor from scanning
% over the sky or other parameters (such as the polarization angle and
% scaling factor for the elliptic likelihood).  Also, the DOF is computed
% assuming a non-aligned detector network.
%
% For nullenergy, it is assumed that the detector network is non-aligned; 
% i.e., that the number of null streams is numberOfDetectors-2.
%
% For unrecognized likelihood types, the return value is the input
% likelihood. 
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              preparatory                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Checks.
narginchk(3,4);
if ~isvector(likelihood)
    error('likelihood must be a vector.')
end
if ~isvector(numberOfFrequencyBins)
    error('numberOfPixels must be a vector or scalar.')
end
if (length(numberOfFrequencyBins)>1 && ...
        length(numberOfFrequencyBins)~=length(likelihood))
    error('If a vector, numberOfPixels must be the same length as likelihood.')
end

% ---- Enforce column vectors.
likelihood = likelihood(:);
numberOfFrequencyBins = numberOfFrequencyBins(:);

% ---- Replace zero likelihoods (which occur if injection is not analysed)
%      by NaNs, so they will be ignored.
likelihood(likelihood==0) = NaN;

% ---- Other preliminaries.
numberOfLikelihoods = length(likelihood);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     determine degrees of freedom                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Degrees of freedom.
switch likelihoodType
    case 'standard'
        projectedDimensions = 2;
    case {'crossenergy', 'hardconstraint', 'plusenergy'}
        projectedDimensions = 1;
    case 'totalenergy'
        if (isempty(numberOfDetectors))
            error(['Must specify numberOfDetectors for likelihoodType totalenergy.']); 
        else
            projectedDimensions = numberOfDetectors;
        end
    case 'nullenergy'
        if (isempty(numberOfDetectors))
            error(['Must specify numberOfDetectors for likelihoodType nullenergy.']); 
        else
            projectedDimensions = numberOfDetectors - 2;
        end
    otherwise
        % ---- Un-recognized likelihood; return original likelihood as 
        %      significance.
        significance = likelihood;
        return
end
DOF = numberOfFrequencyBins * 2 * projectedDimensions;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          compute signficances                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Estimated signficance of the event, assuming chi-squared statistics.
Pchi2cdf = chi2cdf(2*likelihood,DOF);
warning('off','MATLAB:log:logOfZero');
significance = log(1-Pchi2cdf);
warning('on','MATLAB:log:logOfZero');
% ---- For significance <~ -23 (prob <~ 1e-10) start to see problems with
%      round-off error.  E.g.: interpolation for ROCs fails because rounded
%      significances equal.  Switch to series expansion approximation for
%      significance.
index = find(significance<log(1e-10));
% ---- Input arguments: 1/2 quantity that is chi-sq disted, 1/2 DOF.
seriesSignificance = logIncompleteGammaSeries(likelihood,DOF/2);
significance(index) = seriesSignificance(index);
% ---- Switch to positive signs for significance: larger positive = more
%      significant.
significance = -significance;

% ---- Done.
return
