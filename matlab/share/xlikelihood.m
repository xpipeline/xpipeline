function [likelihoodNumerator, likelihoodDenominator, ...
          meanTheta, meanPhi, mostLikelyTheta, mostLikelyPhi] = ...
    xlikelihood(skyPositions, nullEnergyMap, totalEnergyMap, ...
                degreesOfFreedom, numberOfChannels)
% XLIKELIHOOD Compute the Bayesian likelihood ratio for detection
%
% XLIKELIHOOD applies Bayesian logic to determine the likelihoods that a
% gravitational-wave burst is or is not present in the data.  XLIKELIHOOD
% returns the numerator and denominator of the likelihood ratio.  A detection is
% claimed if this ratio exceeds a prespecified threshold.
%
% XLIKELIHOOD also computes two different estimates of the sky position of
% potential bursts.  One is the mean of the a posteriori probability
% distribution for sky position and the other is the most likely sky position.%
%
% usage:
%
%   [likelihoodNumerator, likelihoodDenominator, ...
%    meanTheta, meanPhi, mostLikelyTheta, mostLikelyPhi] = ...
%   xlikelihood(skyPositions, nullEnergyMap, totalEnergyMap, ...
%               degreesOfFreedom, numberOfChannels);
%
%   skyPositions           matrix of sky positions
%   skyMap                 sky map returned by XMAPSKY
%   residualMap            sky map returned by XMAPSKY
%   degreesOfFreedom       number of frequency bins in each band
%   numberOfChannels       number of channels under study
%
%   likelihoodNumerator    numerator of sky-integrated likelihood ratio
%   likelihoodDenominator  denominator of sky-integrated likelihood ratio
%   meanTheta              mean a posteriori geocentric colatitude estimate
%   meanPhi                mean a posteriori geocentric longitude estimate
%   mostLikelyTheta        most likely geocentric colatitude estimate
%   mostLikelyPhi          most likely geocentric longitude estimate
%
% The tested sky positions should be provided as a four column matrix with the
% form
%
%   [thetas phis pOmegas dOmegas].
%
% The four columns have the following meaning.
%
%   theta   geocentric colatitude in the range [0, pi] with 0 at the North Pole.
%   phi     geocentric longitude in the range [-pi, pi) with 0 at Greenwich.
%   pOmega  a priori probability associated with each sky position
%   dOmega  differential solid angle associated with each sky position
%
% The null and total energy sky maps are three dimensional arrays returned by
% XMAPSKY that have the form
%
%   nullEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   totalEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex).
%
% The resulting likelihood ratio numerator and denominator are two dimensional
% arrays that have the form
%
%   likelihoodNumerator(frequencyBandIndex, elementIndex)
%   likelihoodDenominator(frequencyBandIndex, elementIndex).
%
% The resulting sky position estimates have the form
%
%   meanTheta(frequencyBandIndex, elementIndex)
%   meanPhi(frequencyBandIndex, elementIndex)
%   mostLikelyTheta(frequencyBandIndex, elementIndex)
%   mostLikelyPhi(frequencyBandIndex, elementIndex).
%
% See also XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, XMAPSKY, XINTERPRET,
% and XPIPELINE.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

% check for sufficient command line arguments
error(nargchk(5, 5, nargin));

% validate sky positions
if size(skyPositions, 2) ~= 4,
  error('sky positions must be a four column matrix [theta phi pOmega dOmega]');
end
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% number of null sums
numberOfNullSums = numberOfChannels - 2;

% number of frequency bands
numberOfFrequencyBands = size(nullEnergyMap, 1);

% number of elements
numberOfElements = size(nullEnergyMap, 2);

% reshape null and total energy maps to permit matrix operations
nullEnergyMap = permute(nullEnergyMap, [3 2 1]);
totalEnergyMap = permute(totalEnergyMap, [3 2 1]);

% a priori probability associated with each sky position
pOmega = skyPositions(:, 3) * ones(1, numberOfElements);

% differential solid angle associated with each sky position
dOmega = skyPositions(:, 4) * ones(1, numberOfElements);

% compute likelihood ratio numerator by integrating over the sky
likelihoodNumerator = zeros(numberOfFrequencyBands, numberOfElements);
for frequencyBandNumber = 1 : numberOfFrequencyBands,  
  priorLikelihood = exp(-nullEnergyMap(:, :, frequencyBandNumber) / 2) * ...
      sqrt(2 * pi).^(-numberOfNullSums * degreesOfFreedom(frequencyBandNumber));
  likelihoodNumerator(frequencyBandNumber, :) = ...
      sum(priorLikelihood .* pOmega .* dOmega, 1);
end

% compute likelihood ratio denominator by integrating over the sky
likelihoodDenominator = zeros(numberOfFrequencyBands, numberOfElements);
for frequencyBandNumber = 1 : numberOfFrequencyBands,  
  priorLikelihood = exp(-totalEnergyMap(:, :, frequencyBandNumber) / 2) * ...
      sqrt(2 * pi).^(-numberOfChannels * degreesOfFreedom(frequencyBandNumber));
  likelihoodDenominator(frequencyBandNumber, :) = ...
      sum(priorLikelihood .* pOmega .* dOmega, 1);
end

% estimate mean and most likely sky positions
theta = skyPositions(:, 1);
phi = skyPositions(:, 2);
x = sin(theta) .* cos(phi);
y = sin(theta) .* sin(phi);
z = cos(theta);
x = x * ones(1, numberOfElements);
y = y * ones(1, numberOfElements);
z = z * ones(1, numberOfElements);
meanX = zeros(numberOfFrequencyBands, numberOfElements);
meanY = zeros(numberOfFrequencyBands, numberOfElements);
meanZ = zeros(numberOfFrequencyBands, numberOfElements);
mostLikelyTheta = zeros(numberOfFrequencyBands, numberOfElements);
mostLikelyPhi = zeros(numberOfFrequencyBands, numberOfElements);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  priorLikelihood = exp(-nullEnergyMap(:, :, frequencyBandNumber) / 2) * ...
      sqrt(2 * pi).^(-numberOfNullSums * degreesOfFreedom(frequencyBandNumber));
  meanX(frequencyBandNumber, :) = ...
      sum(x .* priorLikelihood .* pOmega .* dOmega, 1);
  meanY(frequencyBandNumber, :) = ...
      sum(y .* priorLikelihood .* pOmega .* dOmega, 1);
  meanZ(frequencyBandNumber, :) = ...
      sum(z .* priorLikelihood .* pOmega .* dOmega, 1);
  [ignore, skyIndices] = min(nullEnergyMap(:, :, frequencyBandNumber) - ...
                             totalEnergyMap(:, :, frequencyBandNumber));
  mostLikelyTheta(frequencyBandNumber, :) = theta(skyIndices)';
  mostLikelyPhi(frequencyBandNumber, :) = phi(skyIndices)';
end
meanTheta = pi / 2 - atan2(meanZ, sqrt(meanX.^2 + meanY.^2));
meanPhi = atan2(meanY, meanX);

% return to calling function
return
