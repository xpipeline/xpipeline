function injectionParameters = readinjectionfile(injectionFileName,requestedInjections)
% readinjectionfile - Read simulated signal parameters from a file.
%
%    parameters = readinjectionfile(fileName,injNumbers)
%
%  fileName     String.  Name of file specifying simulated signals to be
%               injected.
%  injNumbers   Optional vector of natural numbers. If supplied, then
%               parameters are returned only for injections on these rows
%               of the injection file; any other injections in the file are
%               ignored. If not specified then all injections in the file
%               are returned. Input values of 0 of [] (default) also cause 
%               all injections to be returned.
%
%  parameters   Cell array of strings.  Each string contains the parameters
%               for a single GWB or glitch simulation.
%
% $Id$

% ---- Check number of arguments and assign defaults.
narginchk(1, 2)
if (nargin<2) 
    requestedInjections = [];
end

% ---- Scan files contents into cell array of strings, one string per line
%      in file.
injectionParameters = dataread('file', injectionFileName,'%s','delimiter','\n');
nInjection = size(injectionParameters,1);

% ---- Verify that requestedInjections has one of the following values:
%        [] - inject everything
%        0  - inject everything
%        [M,N,...] where M,N,...>0 - inject rows M, N, ... 
if isempty(requestedInjections)
    ;
elseif length(requestedInjections)==1 && requestedInjections==0
    ;
elseif length(requestedInjections)>=1 && all(requestedInjections>0)
    ;
else
    msg = ['  Invalid requestedInjections value for file ' injectionFileName '.' newline ...
           '  Injection number(s) requested: ' num2str(requestedInjections) newline ...
           '  Allowed values are: [] (only), 0  (only), or a vector of natural numbers <= ' num2str(nInjection) '.'];
    error(msg);
end

% ---- Restrict to requested injections, if user has requested only
%      specified injections to be returned. 
if ~isempty(requestedInjections) & requestedInjections~=0
    % ---- Check that file contains requested injections:
    %      requestedInjections values must be positive integers not
    %      exceeding the number of rows in the file.
    if ~all(requestedInjections>0 & requestedInjections<=nInjection & floor(requestedInjections)==requestedInjections)    
        msg = ['Invalid requestedInjections value. ' newline ...
               '  Injection number(s) requested: ' num2str(requestedInjections) newline ...
               '  Allowed values for ' injectionFileName ' are 1:' num2str(nInjection) '.'];
        error(msg);
    end
    % ---- Extract requested injections.
    temp_injectionParameters = injectionParameters(requestedInjections);
    clear injectionParameters
    injectionParameters = temp_injectionParameters;
end

% ---- Done.
return
