function [ul90,nameCell] = extractULForCut(webdir,pluscut,crosscut,nullcut)
% extractULForCut -  finds tuning UL for given cut value
%
%   webdir           path to auto_web dir
%   pluscut          cut values 
%   crosscut
%   nullcut
%
%   ul90             vector of ULs
%   nameCell         names of waveforms

matFiles = [];
matFiles = dir([webdir '/*vetoTest.mat']);

if isempty(matFiles)
   error('no mat files, check path provided or glob pattern used in this function')
end

% ---- Loop over files stopping when we find the one containing cuts we are 
%      interested in.
foundFile = 0;
fidx = 1;
while foundFile == 0

   filepath = [webdir '/' matFiles(fidx).name];
   load(filepath,'nameCell',...
      'vetoPlusRangePlots',...
      'vetoCrossRangePlots',...
      'vetoNullRangePlots',...
      'ulVector90Plots');

   cutIdx = find((vetoPlusRangePlots  == pluscut) & ...
                 (vetoCrossRangePlots == crosscut) & ...
                 (vetoNullRangePlots  == nullcut));

   if isempty(cutIdx)
      clear filpath nameCell vetoPlusRangePlots vetoCrossRangePlots vetoNullRangePlots ulVector90Plots
   else
      foundFile = 1;
   end

   % ---- Iterate count idx.
   fidx = fidx  + 1;

   if fidx > length(matFiles)
       error('None of the vetoTest.mat files contains your combination of cuts') 
   end
end

disp(filepath)

ul90 = ulVector90Plots(cutIdx,:);
