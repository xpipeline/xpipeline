function CP = xSP2CP(skyPositions, channelNames)
% xSP2CP    Turns sky positions into cos(theta baseline) space
%
% Usage:
%    CP = xSP2CP(skyPositions, channelNames)
%
%    skyPositions    Sky Positions in [theta phi] coordinates
%    channelNames    Cell array of names of detectors
%
%    CP              nDet*(nDet-1)/2 columns, each row is a position
%
% See also LoadDetectorData

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$


theta = skyPositions(:,1);
phi = skyPositions(:,2);

nPos = length(theta);

z = cos(theta);
y = sin(theta).*sin(phi);
x = sin(theta).*cos(phi);

nDet = length(channelNames);

CP = zeros(nPos,nDet*(nDet-1)/2);

cnt = 1;

for id1=1:nDet,
    for id2=id1+1:nDet,
	d1 = LoadDetectorData(channelNames{id1});
	d2 = LoadDetectorData(channelNames{id2});
	baseline = d1.V-d2.V;
	baseline = baseline/sqrt(baseline'*baseline);
	
        CP(:,cnt) = [x y z]*baseline;
	cnt = cnt + 1;
    end
end
