function Y = isodd(N)
% ISODD - Returns 1 if the input is a positive odd integer, 0 otherwise.
%
% usage:
%
%   Y = isodd(N)
%
%  N    Numeric array.
%  
%  Y    Logical array of same size as N, with value 1 (0) if the
%       corresponding element of N is (is not) an odd positive integer.  
%
% Note that ISODD tests the value of N, not the data storage type (i.e., it
% does not check for integer type).
%
% See also iseven.

% ---- Check for sufficient command line arguments.
narginchk(1, 1);

% ---- Verify that N is a numeric array.
if ~isnumeric(N)
    error('N must be a numeric array.');
end

% ---- Verify that N is odd.
Nbase = (N-1)/2;
Y = (Nbase == round(Nbase)) & N>0;

return
