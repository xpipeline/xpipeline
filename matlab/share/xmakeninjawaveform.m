function [h, t] = xmakeninjawaveform(mdcXMLfile, injNumber, channel, ... 
    sampleRate, peakTime, snippetPad, snippetDuration)
% xmakeninjawaveform - Create simulated data stream containing ninja signal.
%
%  [h, t] = xmakeninjawaveform(mdcXMLfile, injNumber, channel, ... 
%               sampleRate, peakTime, snippetPad, snippetDuration)
%
%  mdcXMLfile   String.  Name of xml file specifying ninja signals to be
%               injected.
%  injNumber    Whole numnber.  Specifies which row of the injection file
%               is to be used for the injection. A value of 0 will cause
%               all injections in the file to be used.
%  channel      String.  Detector being analysed.  The detector name is
%               inferred from the first two characters (only), and
%               must be one of the names recognized by LoadDetectorData.
%  sampleRate   Scalar.  Sample rate of the detector.
%  peakTime     Scalar.  GPS peak time of the injection at the detector [sec].
%  snippetPad       Scalar. Positive integer as used in xinjectsignal.
%  snippetDuration  Scalar. Positive integer as used in xinjectsignal.
%
%  h            Detector strain including antenna responses (Fp*hp+Fc*hc).
%  t            Times at which the waveform is sampled, starting from zero [sec].
%
% This function uses lalapps_mdc_ninja to generate a frame file containing
% a CBC injection timeseries on-the-fly, then reads the frame and returns
% the timeseries. THIS FUCNTION IS OLD AND PROBABLY NO LONGER WORKS. 
%
% $Id$

warning('Calling xmakeninjawaveforms. THIS FUNCTION IS OLD AND PROBABLY NO LONGER WORKS. CAVEAT EMPTOR.');

% ---- Check that we are only trying to generate a single injection.
if length(injNumber) > 1
    error(['For ninja injections only one or all injections can be done at a time']);
end

% ---- lalapps_mdc_ninja needs the whole (or most?) of the waveform to fit
%      into to the generated frame. Pad the data snippet by a long time to
%      ensure it works correctly.  
inspPad = 240; %[sec]

% ---- Generate a temporary scratch directory.
[o tmpPath]=system('mktemp -d');
% ---- Strip trailing newline.
tmpPath = tmpPath(1:end-1);

% ---- If the user has specified a nonzero injection number, then generate
%      a temporary file containing just that injection.
if injNumber
    system(['selectRowXml.py -f ' mdcXMLfile ' -r ' num2str(injNumber) ' -o ' tmpPath '/INJ.xml']);
    mdcXMLfile = [tmpPath '/INJ.xml'];
end

% ---- Have a go!
try

    % ---- Generate frame at high sampling rate.
    genSampleRate = max(16384,sampleRate);
    % ---- Generate MDC frame.
    itfName = channel(1:2);
    system(['lalapps_mdc_ninja --injection-file ' mdcXMLfile ...
            ' --gps-start-time ' num2str(floor(peakTime)-snippetPad-inspPad)  ...
            ' --gps-end-time ' num2str(floor(peakTime)+1+snippetPad) ...
            ' --sample-rate ' num2str(genSampleRate)  ...
            ' --ifo ' itfName ...
            ' --write-frame  --frame-type ONTHEFLY ' ...
            ' --fr-out-dir ' tmpPath ...
            ' --injection-type approximant']);

    % ---- Generate a framecache to use for reading the frame.
    fid=fopen([tmpPath '/tmpframecache.txt'],'w');
    fprintf(fid,'GHLTV ONTHEFLY %d %d %d %s\n', ...
        floor(peakTime)-snippetPad-inspPad, floor(peakTime)+1+snippetPad, ...
        snippetDuration+inspPad,tmpPath);
    fclose(fid);

    % ---- Read data from frame.
    [data, ~, readTimeStamps] = readframedata( ...
        loadframecache([tmpPath '/tmpframecache.txt']), [itfName ':STRAIN'], ...
        'ONTHEFLY', floor(peakTime)-snippetPad, floor(peakTime)+1+snippetPad, ...
        [], 1);

    % ---- Resample data to given interferometer sampling rate.
    % ---- Factors relating sample frequencies.
    [upSampleFactor, downSampleFactor] = rat(sampleRate / genSampleRate); 

    % ---- The following code is copied from xcondition.m.
    % ---- The anti-aliasing filter used by default in the matlab resample
    %      function is weak, with an amplitude attenuation of about 0.5 at
    %      the new Nyquist frequency. The anti-aliasing filter constructed
    %      below has much stronger attenuation.
    % ---- Design anti-alias filter.
    filterOrder = 2 * 256 * max(upSampleFactor, downSampleFactor);
    filterCutoff = 0.99 / max(upSampleFactor, downSampleFactor);
    filterFrequencies = [0 filterCutoff filterCutoff 1];
    filterMagnitudes = [1 1 0 0];
    filterCoefficients = upSampleFactor * ...
        firls(filterOrder, filterFrequencies, filterMagnitudes) .* ...
        hanning(filterOrder + 1)';

    % ---- Resample data and time stamps.
    data = resample(data, upSampleFactor, downSampleFactor, filterCoefficients);
    if upSampleFactor == 1
        readTimeStamps = readTimeStamps(1:downSampleFactor:length(readTimeStamps));
    else
        error(['Resampling the time by a not integer number is not currently supported']);
    end
    
    h = data';

    % ---- Check whether read time stamps are correct.
    readTimeSteps = unique(readTimeStamps(2:end)-readTimeStamps(1:end-1));
    % ---- Protect againt roundoff false alarms (?).
    if max(readTimeSteps) > min(readTimeSteps)*(1+1e-2)
        error(['Time steps are not unique, probably some dropped samples. The ' ...
            'list of time steps is: ' num2str(readTimeSteps)])
    end
    % ---- Generate time as expected for output.
    t = readTimeStamps + snippetPad - floor(peakTime);

catch
    
    warning('Attempt to generate ninja injection failed! Returning null timeseries.');
    h = zeros(sampleRate*snippetDuration,1);
    t = [0:sampleRate*snippetDuration-1]'/sampleRate;
    
end

% ---- Clean up temporary files.
system(['rm -rf ' tmpPath]);

% ---- Done.
return
