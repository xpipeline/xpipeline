function [likelihoodTimeFrequencyMap, skyPositionIndex, ...
    detectorSpectrogram, internalAngleIndex] = ...
    xtimefrequencymap(channelNames, conditionedData, sampleFrequency, ...
        amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
        transientLength, frequencyBand, windowType, likelihoodType, ...
        outputType, verboseFlag, circTimeSlides, ...
        blackPixelPrctile, genClusteringThresh, ... 
        decimateRate, detectionStat, injectionPeakIdx, seedlessParams)
% XTIMEFREQUENCYMAP Construct time-frequency map of the likelihood ratio
% for GWB signal hypothesis vs. noise hypothesis, maximized over signal
% parameters.
% 
% XTIMEFREQUENCYMAP computes a select set of likelihood ratios or
% statistics at each time-frequency pixel for a requested set of sky
% positions.  For example, the standard likelihood is, roughly speaking,
% the natural logarithm of the ratio of the probability of the observed
% data being due to a GWB plus stationary Gaussian background noise to the
% probability of the data being due to stationary Gaussian background noise
% only.  When testing multiple sky positions, the likelihood at each
% time-frequency pixel in the returned map is the likelihood measured for
% that pixel for the sky position (and any other parameters) that gave the
% largest total likelihood for that time, summed over all frequency bins
% (or smallest for null energy). 
%
% Optionally, xtimefrequencymap will return event clusters identified in
% the time-frequency maps instead of the maps directly.
%
% Usage:
% 
% [likelihoodMap, skyPositionIndex, detectorSpectrogram, internalAngleIndex] = ...
%     xtimefrequencymap(channelNames, conditionedData, sampleFrequency, ...
%         amplitudeSpectra, skyPositions, integrationLength, offsetLength, ...
%         transientLength, frequencyBand, windowType, likelihoodType, ...
%         outputType, verboseFlag, circTimeSlides, ...
%         blackPixelPrctile, genClusteringThresh, decimateRate, detectionStat, injectionPeakIdx)
%
%    channelNames         Cell array of channel name strings.
%    conditionedData      Matrix of time-domain conditioned data, with one
%                         column per channel (detector).
%    sampleFrequency      Scalar.  Sampling frequency of conditioned data
%                         [Hz].
%    amplitudeSpectra     Matrix of amplitude spectral densities of the
%                         detector noise [1/sqrt(Hz)], with one column per
%                         channel.  Must be sampled at frequencies 
%                         [0:1/integrationLength:1/2]*sampleFrequency.
%    skyPositions         Matrix of sky positions [radians] to be tested.
%                         First column is polar angle and second column is
%                         azimuthal angle, both in radians in Earth-based
%                         coordinates.
%    integrationLength    Scalar.  Number of samples of data to use for 
%                         each Fourier transform.  Must be a power of 2.
%    offsetLength         Scalar.  Number of samples between start of 
%                         consecutive FFTs.  Must be a power of 2.
%    transientLength      Scalar.  Duration of conditioning transients, in
%                         samples.  This is the number of samples at the
%                         beginning and end (rows of conditionedData) to
%                         ignore when computing likelihood maps.
%    frequencyBand        Two-component vector.  Elements are minimum and 
%                         maximum frequencies (Hz) to include in the
%                         time-frequency maps.
%    windowType           Optional string.  Window type to be used in FFTs;
%                         one of 'none', 'bartlett', 'hann', or
%                         'modifiedhann'.  Default 'modifiedhann'.
%    likelihoodType       String or cell array specifying types of 
%                         likelihoods to compute.  Recognized values are
%                         'aglitchhardconstraint', 'ampenergy', 'ampinc', 
%                         'ampnullenergy', 'ampnullinc', 'bandwidth', 
%                         'bayesian1e-21', 'bayesian1e-22', 'bayesian1e-23', 
%                         'bayesian3e-22', 'bayesian3e-23', 'bg', 'circenergy', 
%                         'circinc', 'circnullenergy', 'circnullenergyreg', 
%                         'circnullinc', 'circnullincreg', 'crossenergy', 
%                         'crossinc', 'dev', 'duration', 'elinear', 'elinearnull', 
%                         'elliptic', 'energyitf1', 'energyitf2', 'energyitf3', 
%                         'energyitf4', 'geometric', 'glitch', 'glitchhardconstraint', 
%                         'glitchstandard', 'H1H2nullenergy', 'H1H2nullinc', 
%                         'hardconstraint', 'ilinear', 'ilinearnull', 
%                         'incoherentenergy', 'loghbayesian', 'loghbayesiancirc', 
%                         'loghbayesianscalar', 'maxglitchhardconstraint', 
%                         'maxglitchstandard', 'nullenergy', 'nullinc', 'plusenergy', 
%                         'plusinc', 'powerlaw', 'powerlawfixed', 'psi', 
%                         'scalarenergy', 'scalarinc', 'scalarnullenergy', 
%                         'scalarnullinc', 'skypositionphi', 'skypositiontheta', 
%                         'softconstraint', 'standard', 'timeshift1', 'timeshift2', 
%                         'totalenergy', 'unbiasedsoft'.
%    outputType           If non-zero, then likelihoodMap output variable 
%                         will contain event clusters rather than
%                         likelihood maps.  See below.
%    verboseFlag          Boolean flag to control output of status messages
%                         (default 0).
%    circTimeSlides       Array. Time slides step (in seconds) to use for
%                         internal circular time slides.
%    blackPixelPrctile    Scalar. Percentage of TF map pixels to discard in
%                         thresholding.  E.g.: 99 or 99.7.
%    genClusteringThresh  Vector (for generalised clustering) or cell array (for
%                         gating): 
%                           If a vector, it can have one or two elements. If the
%                             the first element (required) is 0, then no 
%                             generalised clustering is applied. If in (0,1], 
%                             then generalised clustering with 24 connectivity
%                             (pixels in a 5x5 grid are neighbors) is applied to 
%                             all clusters. If >1, then clusters with mean value 
%                             per pixel < black_pixel_threshold * genClusteringThresh(1) 
%                             are discarded before 24 generalized clustering. 
%                             The optional second element can be used to specify
%                             the connectivity. Allowed values are 8 (3x3 grid; 
%                             i.e., next-nearest neighbors), 24 (5x5 grid, 
%                             default), 48 (7x7 grid), 80 (9x9 grid).
%                           If a cell array, there must be one element per 
%                             detector. Each element is an array of gates for 
%                             that detector as formatted by AUTOGATE.m. An array
%                             may be empty if there are no gates for that 
%                             detector.
%    decimateRate         Scalar. Sets the rate [Hz] of triggers kept after
%                         decimation. The decimation is done independently 
%                         for each analysis time and sky position. 
%                         Required for all cases except outputType 
%                         'timefrequencymap' and 'seedless'. 
%    detectionStat        String. Name of likelihood type to be used for 
%                         ranking triggers in decimation and superclustering.
%                         Required for all cases except outputType 
%                         'timefrequencymap'. 
%    injectionPeakIdx     Scalar. Location in samples of injection or glitch 
%                         peak time. If supplied, then a maximum of only 32 sec
%                         of the time-frequency map (centred at the specified 
%                         time) is processed. Required only for outputType 
%                         'injectionclusters', 'glitch'.
%
%    likelihoodMap        Cell array. For outputType = '1', 'followup',
%                         'sparseclusters','seedless', cell array has a
%                         single element which is a numeric array of event
%                         clusters. For 'clusters', 'injectionclusters',
%                         'glitch', cell array has multiple elements, each
%                         of which is a numeric array of event clusters for
%                         one circular time slide. For 'timefrequencymap',
%                         cell array has a single element which is a 3D
%                         array of time-frequency maps for all the
%                         likelihood types tested (time x frequency x
%                         likeihood type) maximized over the sky (except
%                         for the null energies, which are minimised).
%    skyPositionIndex     Array.  Index in skyPositions array for which 
%                         the likelihood at the corresponding time in 
%                         likelihoodMap is computed.  Not meaningful if
%                         'outputType' is non-zero.
%    detectorSpectrogram  Array.  Time-frequency maps of the energy in the  
%                         individual detectors, with no time shifts.
%                         size(detectorSpectrogram,3)=length(channelNames).
%    internalAngleIndex   Array.  Index in internalAngles array (an 
%                         internal xtimefrequencymap variable) for which 
%                         the likelihood at the corresponding time in 
%                         likelihoodMap is computed.  Nonzero and
%                         meaningful only for elliptic - type likelihoods
%                         with outputType==0. 
%
% The conditioned data should be provided as a matrix of time-domain data
% with each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of
% one-sided frequency domain data at a frequency resolution corresponding
% to the desired integration length and with each channel in a separate
% column.
%
% The desired sky positions should be provided as a two column matrix of
% the form [theta phi], where theta is the geocentric colatitude running
% from 0 at the North pole to pi at the South pole and phi is the
% geocentric longitude in Earth fixed coordinates with 0 on the prime
% meridian.
%
% Options for reporting clusters instead of time-frequency maps:
%
%   outputType == 1 
%     Output is largest single cluster for each sky direction tested.  Then
%     size(likelihoodMap) = number of sky positions x (7+number of
%     likelihood types).  Columns are:
%       1) start time of cluster relative to start of data (sec)
%       2) weighted central time of cluster relative to start of data (sec)
%       3) end time of cluster relative to start of data (sec)
%       4) minimum frequency of cluster (Hz)
%       5) weighted peak frequency of cluster (Hz)
%       6) maximum frequency of cluster (Hz)
%       7) number of pixels in the cluster
%       8+) likelihood values for the cluster, in the order of
%           likelihoodType.
%     See clusterTFmapNew for more details.
%
%   outputType == 'sphericalclusters', 'expintclusters', 'squareintclusters':
%     Output is ???
%
% In the present implementation, the detector site of the first channel is
% used as the reference position when performing time shifts.  Therefore,
% when comparing likelihood maps to features in the data from individual
% detectors, the appropriate time shift is the time delay between the first
% detector and the detector being considered.
%
% See also XDETECTION, XMAPSKY, XREADDATA, XINJECTSIGNAL, XCONDITION,
% XTILESKY, and XINTERPRET, ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Stephen Poprocki    poprocki@caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Michal Was          michal.was@ens.fr

% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      process command line arguments                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for sufficient command line arguments
error(nargchk(13, 20, nargin));

% ---- Set column indexes of clusterArray.  These control the ordering of
%      cluster properties when outputType > 0. 
minTcol=1; meanTcol=2; maxTcol=3;
minFcol=4; meanFcol=5; maxFcol=6;
nPixelsCol=7;
likelihoodColOffset = 7;

% ---- Convert likelihoodType to cell array if necessary
if ischar(likelihoodType)
    temp_likelihoodType = likelihoodType;
    clear likelihoodType
    likelihoodType{1} = temp_likelihoodType;
end
% ---- Number of likelihood types to test
numberOfLikelihoods = length(likelihoodType);
% ---- Check that each likelihoodType is a recognized type
for jLikelihoodType = 1:numberOfLikelihoods
    if (~( strcmp(likelihoodType{jLikelihoodType},'aglitchhardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'ampenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'ampinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'ampnullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'ampnullinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'bandwidth') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian1e-21') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian3e-22') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian1e-22') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian3e-23') ...
        || strcmp(likelihoodType{jLikelihoodType},'bayesian1e-23') ...
        || strcmp(likelihoodType{jLikelihoodType},'bg') ...
        || strcmp(likelihoodType{jLikelihoodType},'circenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'circinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'circnullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'circnullenergyreg') ...
        || strcmp(likelihoodType{jLikelihoodType},'circnullinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'circnullincreg') ...
        || strcmp(likelihoodType{jLikelihoodType},'crossenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'crossinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'dev') ...
        || strcmp(likelihoodType{jLikelihoodType},'duration') ...
        || strcmp(likelihoodType{jLikelihoodType},'elinear') ...
        || strcmp(likelihoodType{jLikelihoodType},'elinearnull') ...
        || strcmp(likelihoodType{jLikelihoodType},'elliptic') ...
        || strcmp(likelihoodType{jLikelihoodType},'energyitf1') ...
        || strcmp(likelihoodType{jLikelihoodType},'energyitf2') ...
        || strcmp(likelihoodType{jLikelihoodType},'energyitf3') ...
        || strcmp(likelihoodType{jLikelihoodType},'energyitf4') ...
        || strcmp(likelihoodType{jLikelihoodType},'geometric') ...
        || strcmp(likelihoodType{jLikelihoodType},'glitch') ...
        || strcmp(likelihoodType{jLikelihoodType},'glitchhardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'glitchstandard') ...
        || strcmp(likelihoodType{jLikelihoodType},'H1H2nullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'H1H2nullinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'hardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'ilinear') ...
        || strcmp(likelihoodType{jLikelihoodType},'ilinearnull') ...
        || strcmp(likelihoodType{jLikelihoodType},'incoherentenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'loghbayesian') ...
        || strcmp(likelihoodType{jLikelihoodType},'loghbayesiancirc') ...
        || strcmp(likelihoodType{jLikelihoodType},'loghbayesianscalar') ...
        || strcmp(likelihoodType{jLikelihoodType},'maxglitchhardconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'maxglitchstandard') ...
        || strcmp(likelihoodType{jLikelihoodType},'nullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'nullinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'plusenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'plusinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'powerlaw') ...
        || strcmp(likelihoodType{jLikelihoodType},'powerlawfixed') ...
        || strcmp(likelihoodType{jLikelihoodType},'psi') ...        
        || strcmp(likelihoodType{jLikelihoodType},'scalarenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'scalarinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'scalarnullenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'scalarnullinc') ...
        || strcmp(likelihoodType{jLikelihoodType},'skypositionphi') ...
        || strcmp(likelihoodType{jLikelihoodType},'skypositiontheta') ...
        || strcmp(likelihoodType{jLikelihoodType},'softconstraint') ...
        || strcmp(likelihoodType{jLikelihoodType},'standard') ...
        || strcmp(likelihoodType{jLikelihoodType},'timeshift1') ...
        || strcmp(likelihoodType{jLikelihoodType},'timeshift2') ...
        || strcmp(likelihoodType{jLikelihoodType},'totalenergy') ...
        || strcmp(likelihoodType{jLikelihoodType},'unbiasedsoft') ...
        ))
        error(['likelihoodType ' likelihoodType{jLikelihoodType} ...
            ' is not a recognized type']);
    end
end

% ---- Force one dimensional cell array for channel list
channelNames = channelNames(:);

% ---- Number of detectors
numberOfChannels = length(channelNames);

% ---- Verify that number of data streams matches number of detectors
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% ---- Block length in samples
blockLength = size(conditionedData, 1);

% ---- Verify that integration length is an integer power of two
if bitand(integrationLength, integrationLength - 1),
  error('integration length is not an integer power of two');
end

% --- for glitch output type reuse skyPositions as time delay variable,
%     for glitch studies we only want to scan an array of time delays
if strcmp(outputType,'glitch')
  if size(skyPositions,2) ~= numberOfChannels
    error(['In "glitch" output type, the number of time delay columns, ' ...
           'which are put into the "skyPositions" variable,  must ' ...
           'be equal to the number of channels']);
  end
  pOmega = ones(size(skyPositions,1));
else
  % ---- We expect skyPosition to have either 2 or 4 columns
  %      containing:
  %      theta   Polar angle in the range [0, pi] with 0 at the 
  %              North Pole/ z axis.
  %      phi     Azimuthal angle in the range [-pi, pi) with 0 at 
  %              Greenwich / x axis.
  %      pOmega  A priori probability associated with each sky 
  %              position. [Optional] 
  %      dOmega  Solid angle associated with each sky position.
  %              [Optional]
  %      (From sinusoidalMap.)
  
  % ---- Verify that skyPositions array has at least two columns
  if size(skyPositions, 2) < 2,
      error('sky positions must be a at least a two column matrix');
  elseif size(skyPositions, 2) == 2
      if verboseFlag
        warning('Setting pOmega to unity')
      end
      pOmega = ones(size(skyPositions(:, 1)));
  elseif size(skyPositions,2) == 4
      pOmega = skyPositions(:, 3); 
  else
      error('Unknown format of skyPositions, expect either 2 or 4 cols.')
  end
  
  % ---- Verify that skyPositions array has at least two columns
  if size(skyPositions, 2) < 2,
    error('sky positions must be a at least a two column matrix');
  end
  % ---- Verify that skyPositions polar coordinates are in range [0,pi].
  if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
    error('theta outside of [0, pi]');
  end
  % ---- Verify that skyPositions azimutal coordinates are in range [-pi,pi)
  if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
    error('phi outside of [-pi, pi)');
  end
end

% ---- Number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% ---- Nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% ---- Vector of one-sided frequencies
oneSidedFrequencies = nyquistFrequency * ( 0 : integrationLength / 2 ) / ...
                     ( integrationLength / 2 );

% ---- Frequencies in desired analysis band
frequencyIndex = find( (oneSidedFrequencies>=frequencyBand(1)) & ...
    (oneSidedFrequencies<=frequencyBand(2)) );
inbandFrequencies = oneSidedFrequencies(frequencyIndex);

% ---- Number of in-band frequency bins
numberOfFrequencyBins = length(inbandFrequencies);

% ---- Truncate amplitude spectra to desired frequency band
amplitudeSpectra = amplitudeSpectra(frequencyIndex,:);

% ---- Determine if genClusteringThresh input is specifying generalised
%      clustering, gating, or neither.
if iscell(genClusteringThresh)
    % ---- Input actually holds information on gates to be applied. Rename
    %      variable appropriately and set genClusteringThresh to indicate no
    %      generalised clustering.
    allGates = genClusteringThresh;
    genClusteringThresh = 0;  
end

% ---- Add second element to genClusteringThresh if there isn't one already.
if not(strcmp(outputType,'timefrequencymap') || strcmp(outputType,'seedless'))  
    if length(genClusteringThresh) == 1
        genClusteringThresh(2) = 24;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               partition block into overlapping segments                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Calculate the indices of the endpoints of the segments of length
%      integrationLength with no overlapping.  (Overlapping will be imposed 
%      later when FFTing.) The variable segmentIndicesFull will cover the
%      entire TF map except for start and end transient intervals, while
%      segmentIndices will cover the subset of the TF map that is analysed.
switch outputType
    case {1, 'followup'}
        % ---- Analyse only +/- 2 seconds around center time.
        segmentIndices = chunkIndices(blockLength, integrationLength, ...
            (blockLength-4*sampleFrequency)/2, integrationLength);
    case {'injectionclusters','glitch'}
        % ---- Analyse only 32 seconds centered on the injection bin. Use
        %      the same bin edges as the full time frequency map analysis,
        %      and select the nearest (center-time wise) chunks to the
        %      injection bin. If the injection is near the border of the
        %      timefrequency map, the chunk will be assymetrically set, to
        %      keep the total number of chunks constant.
        segmentIndicesFull = chunkIndices(blockLength, integrationLength, ...
                                 transientLength, integrationLength);
        segmentIndices = segmentIndicesFull;
        [Y sortedIndices] = sort(abs(injectionPeakIdx - (segmentIndices(:,2) + segmentIndices(:,1))/2), 1, 'ascend');
        % ---- Select bins totalling 32 sec of analysed data. (Note that
        %      the segments are not overlapping.)
        nKeepChunks = 32*sampleFrequency/integrationLength;
        if ( round(nKeepChunks)==nKeepChunks && nKeepChunks <= size(segmentIndices,1) )
            segmentIndicesKept = segmentIndices(sortedIndices(1:nKeepChunks),:);
            % ---- Put the indices back into chronological order.
            [Y sortedIndices] = sort(segmentIndicesKept(:,1),1,'ascend');
            segmentIndices = segmentIndicesKept(sortedIndices,:);
        else
            error(['Something is wrong, the number of possible chunks is: ' ...
                num2str(size(segmentIndices,1)) ...
                ' and the wanted number of chunks is: ' ...
                num2str(nKeepChunks)]);
        end
    otherwise
        % ---- Analyse all data except for start and end transient intervals.
        segmentIndicesFull = chunkIndices(blockLength, integrationLength, ...
                                 transientLength, integrationLength);
        segmentIndices = segmentIndicesFull;
end

% ---- Number of segments
numberOfSegments = size(segmentIndices, 1);

% ---- Fractional offset of consecutive segments
offsetFraction = offsetLength/integrationLength;

% ---- Number of time bins in time-frequency maps.
%      NOTE THAT WE DO NOT ANALYSE THE LAST ELEMENT; this is convenient for
%      handling overlapping.  The 'max' test covers the case in which there 
%      is only one segment.
numberOfTimeBins = max(numberOfSegments-1,1)/offsetFraction;

% ---- Record info on physical dimensions of time-frequency maps for later
%      use.
% ---- The time stamp of this data point is the central time of the first
%      time bin to be analysed.
firstTimeBinCenterIndex = segmentIndices(1, 1) + integrationLength/2;
% ---- Convert to time relative to startTime of data (which is not known to
%      xtimefrequencymap).
firstTimeBinCenterTime = (firstTimeBinCenterIndex - 1)/ sampleFrequency;
% ---- Time-frequency map dimension info as used by clusterTFmapNew:
%        [central time of first bin, ...
%         spacing between consecutive time bins, ...
%         central frequency of first bin, ...
%         spacing between consecutive frequency bins ]
mapDim = [ firstTimeBinCenterTime, ...
    offsetLength / sampleFrequency, ... 
    inbandFrequencies(1), ...
    sampleFrequency / integrationLength ];
clear firstTimeBinCenterIndex firstTimeBinCenterTime


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              choose "internal angle" values to test                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- This variable is used for elliptic-type likelihoods only.

% ---- Loop over unknown parameters: relative amplitude 
%      (-1<=eta<=1) and polarisation (0<=psi<pi/2).
%      internalAngles = [X(:) Y(:)]; % (psi,eta)
%
% ---- Use very coarse set of 10 points for computational speed.   
%      This set has been tested for overlap with elliptically polarized
%      GWBs with isotropic sky distribution, uniform cos(inclination_angle)
%      distribution, and uniform polarization angle distribution.  It was
%      found to have a mean SNR^2 overlap of 0.9317+/-0.0002.  The best
%      set of 10 found to date has a mean overlap of approximately 0.976.
% internalAngles = [ ...
%     0       1   ; ...
%     0       0.5 ; ...
%     0.5     0.5 ; ...
%     0       0   ; ...
%     0.25    0  ; ...
%     0.5     0  ; ...
%     0.75    0  ; ...
%     0       -0.5 ; ...
%     0.5     -0.5 ; ...
%     0       -1 ...
%     ];
% internalAngles(:,1) = internalAngles(:,1) * pi/2;
%
% ---- Use quasi-regular grid of 32 points in (psi, eta) space.  This set 
%      has been tested for overlap with elliptically polarized GWBs with 
%      isotropic sky distribution, uniform cos(inclination_angle)
%      distribution, and uniform polarization angle distribution.  It was
%      found to have a mean SNR^2 overlap of 0.99098+/-0.00002.  The best
%      set of 32 found to date has a mean overlap of approximately 0.993.
internalAngles = [];
IA_Neta = 10;
IA_maxNumberOfPsi = 5; 
IA_eta = -1:2/(IA_Neta-1):1;
IA_theta = acos(IA_eta);
IA_numberOfPsi = floor(IA_maxNumberOfPsi * sin(IA_theta));
for j=1:length(IA_eta)
    if (IA_numberOfPsi(j)>0)
        IA_dpsi = (pi/2) / IA_numberOfPsi(j);
        IA_psi = [0:IA_dpsi:pi/2]';
        IA_psi(end) = [];  % psi=pi/2 is redundant
        internalAngles = [internalAngles; IA_psi , IA_eta(j)*ones(length(IA_psi),1) ];
    else
        internalAngles = [internalAngles; 0 , IA_eta(j) ];
    end
end
%
numberOfInternalAngles = size(internalAngles,1);

% ---- Pre-compute coefficients in elliptic projection operator.
%      Elliptic projection operator is conj(F)/norm(F) where 
%          conj(F) = (\cos(2\psi)+i\eta\sin(2\psi)) F_+ + (\sin(2\psi)-i\eta\cos(2\psi)) F_x
%                  = ( complex1 ) F_+ + ( complex2 ) F_x
%      where 
% complex1 = cos(2*internalAngles(:,1)) + sqrt(-1)*sin(2*internalAngles(:,1)) .* internalAngles(:,2);
% complex2 = sin(2*internalAngles(:,1)) - sqrt(-1)*cos(2*internalAngles(:,1)) .* internalAngles(:,2);
%      The normalization is 
%          norm(F)^2 = (\cos(2\psi)^2+\eta^2\sin(2\psi)^2) F_+^2 
%              + (\sin(2\psi)^2+\eta^2\cos(2\psi)^2) F_x^2
%              = ( complex1 ) F_+^2 + ( complex2 ) F_x^2
%      where 
% const1 = cos(2*internalAngles(:,1)).^2 + (sin(2*internalAngles(:,1)) .* internalAngles(:,2)).^2;
% const2 = sin(2*internalAngles(:,1)).^2 + (cos(2*internalAngles(:,1)) .* internalAngles(:,2)).^2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       preload detector information                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Loop over detectors.
for iDet = 1:numberOfChannels
    % ---- The detector is matched by the first character of the channel name.
    detData = LoadDetectorData(channelNames{iDet}(1));
    % ---- Extract the position vector
    rDet(iDet,:) = detData.V';
    % ---- Extract the detector response tensors
    dDet{iDet}   = detData.d;
end

% ---- Check for aligned network (i.e., check to see if network consists
%      solely of H1 and H2).
alignedNetwork = 0;
if (numberOfChannels==2) 
    if (strcmp(channelNames{1}(1:2),'H1') && strcmp(channelNames{2}(1:2),'H2')) || ...
       (strcmp(channelNames{1}(1:2),'H2') && strcmp(channelNames{2}(1:2),'H1'))
        alignedNetwork = 1;
    end
end

% ---- For single detector or 'glitch' output type network is aligned by construction
if numberOfChannels==1 || strcmp(outputType,'glitch')
  alignedNetwork = 1;
end

% ---- Check to see if network contains H1 and H2 (possibly in addition to other 
%      detectors). 
indexH1 = strmatch('H1',channelNames);
indexH2 = strmatch('H2',channelNames);
if (~isempty(indexH1) & ~isempty(indexH2))
    H1H2network = 1;
else
    H1H2network = 0;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             precompute detector responses, time delays                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Compute antenna responses and time delays for each detector and sky
%      position.  Arrays are size numberOfSkyPositions x
%      numberOfChannels.
if strcmp(outputType,'glitch')
  Fp = ones(numberOfSkyPositions,numberOfChannels);
  Fc = zeros(numberOfSkyPositions,numberOfChannels);
  timeShifts = skyPositions;
else
  [Fp, Fc, Fb] = antennaPatterns(dDet, skyPositions);
  timeShifts = computeTimeShifts(rDet, skyPositions);
end

% ---- RESET REFERENCE POSITION TO FIRST DETECTOR
timeShifts = timeShifts - repmat(timeShifts(:,1),[1 size(timeShifts,2)]);
timeShiftLengths = timeShifts * sampleFrequency;
integerTimeShiftLengths = round(timeShiftLengths);
% residualTimeShiftLengths = timeShiftLengths - integerTimeShiftLengths;
residualTimeShifts = (timeShiftLengths - integerTimeShiftLengths)/sampleFrequency;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       assign storage                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Storage for likelihood time-frequency maps and other output.
likelihoodTimeFrequencyMap = zeros(numberOfFrequencyBins, numberOfTimeBins, numberOfLikelihoods);
skyPositionIndex = zeros(1, numberOfTimeBins, numberOfLikelihoods);
internalAngleIndex = zeros(1, numberOfTimeBins, numberOfLikelihoods);
% internalAngleIndex = zeros(1, numberOfTimeBins);

switch outputType
    case {1, 'followup'}
        % ---- Array holding properties of "loudest" cluster at each sky
        %      position.  
        loudestCluster = zeros(numberOfSkyPositions, likelihoodColOffset + numberOfLikelihoods);
    case {'clusters','injectionclusters','glitch','seedless'}
        % ---- Cell array holding all clusters for each sky position (!).
        loudestCluster = {};        
    case 'sparseclusters'
        % ---- Array holding loudest clusters from any sky position in each
        %      1-sec interval. 
        %      KLUDGE: This bit of code assumes we are analysing the entire
        %      time interval except the start and end transient intervals.
        %      A more robust code would be based purely on the
        %      segmentIndices or mapDim variables.
        loudestCluster = zeros((blockLength-2*transientLength)/sampleFrequency, ...
            likelihoodColOffset + numberOfLikelihoods);
    case 2
        % ---- Output has something to do with spherical harmonics ???
        nSph=80;
        Ym0 = zeros(numberOfChannels,numberOfChannels,nSph,numberOfSkyPositions);
        sphercialCoef=zeros(numberOfFrequencyBins, numberOfTimeBins, nSph, numberOfLikelihoods);
end

% ---- Assign storage for time-frequency maps of single-detector data.
timeFrequencyMapFull = cell(numberOfChannels,1); %-- contains all frequencies
timeFrequencyMap = cell(numberOfChannels,1); %-- contains only non-negative frequencies
detectorSpectrogram = zeros(numberOfFrequencyBins,numberOfTimeBins,numberOfChannels);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             precompute spherical harmonics                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmpi(outputType,'sphericalclusters') 
    for iDet=1:numberOfChannels
       for jDet=iDet+1:numberOfChannels
           rotatedSkyPos=rotateSky(rDet(iDet,:),rDet(jDet,:),skyPositions);
           for l=0:nSph-1
               tmp = legendre(l,cos(rotatedSkyPos(:,1)),'norm');
               Ym0(iDet,jDet,l+1,:)=tmp(1,:);
               Ym0(jDet,iDet,l+1,:)=tmp(1,:);
           end
       end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Precompute energy percentiles for full time frequency map.            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if sum(strcmp(likelihoodType,'powerlaw'))

    % ---- FFT the entire data stream to be analysed for each detector.
    %      Note that we drop the last segment to make overlapping easier. 
    %      (If there is only one segment it is not dropped!)
    %      Start and stop indices for the FFT are the start and stop
    %      indices of the first and last segments to be analysed:
    numberOfSegmentsFull = size(segmentIndicesFull, 1);
    numberOfTimeBinsFull = max(numberOfSegmentsFull-1,1)/offsetFraction;
    segmentStartIndex = segmentIndicesFull(1, 1);
    segmentStopIndex = segmentIndicesFull(max(numberOfSegmentsFull-1,1), 2);

    % ---- Construct window.
    switch windowType
        case 'none'
            windowData = ones(integrationLength, 1);
        case 'bartlett'
            windowData = bartlett(integrationLength);
        case 'hann'
            windowData = hann(integrationLength);
        case 'modifiedhann'
            windowData = modifiedhann(integrationLength);
        otherwise
            disp('Unknown windowType. Using none.');
            windowData = ones(integrationLength, 1);
    end
    % ---- Rescale window to have unity mean square.
    windowMeanSquare = mean(windowData.^2);
    windowData = windowData / windowMeanSquare^0.5;
    % ---- Make window data into array matching size of data to be FFTed.
    windowData = repmat(windowData,[1,max(numberOfSegmentsFull-1,1)]);

    % ---- Loop over detectors and FFT the data from each.  Use overlapping as
    %      requested.
    for channelNumber = 1 : numberOfChannels

        if (offsetFraction==1)
            % ---- FFT with no overlapping - this code is faster than the more 
            %      general version below.
            % ---- Extract data for this detector.
            data = conditionedData(segmentStartIndex:segmentStopIndex,channelNumber);
            % ---- Reshape into array of size integrationLength x numberOfTimeBinsFull, 
            %      so that each segment to be FFTed occupies one column.
            dataArray = reshape(data,integrationLength, numberOfTimeBinsFull);
            % ---- Apply window and FFT.
            timeFrequencyMapFull{channelNumber} = fft( windowData .* dataArray );
        else
            % ---- Reshape the data into rectangular array, where consecutive
            %      columns contain data according to the requested overlap. 
            for j=1:1/offsetFraction
                % ---- Find start and stop indices.
                offsetStartIndex = segmentStartIndex + integrationLength*(j-1)*offsetFraction;
                offsetStopIndex = segmentStopIndex + integrationLength*(j-1)*offsetFraction;
                % ---- Extract data for this detector.
                data = conditionedData(offsetStartIndex:offsetStopIndex,channelNumber);
                % ---- Reshape into array of size integrationLength x numberOfTimeBinsFull, 
                %      so that each segment to be FFTed occupies one column.
                dataArray = reshape(data,integrationLength,[]);
                % ---- Time bins in full TF map that these segments correspond to.
                timeBins = [j:1/offsetFraction:numberOfTimeBinsFull];
                % ---- Copy these FFTs into TF map for this channel.
                timeFrequencyMapFull{channelNumber}(:,timeBins) = ...
                    fft( windowData .* dataArray );
            end
        end

        % ---- Extract in-band frequencies.
        timeFrequencyMap{channelNumber} = ...
            timeFrequencyMapFull{channelNumber}(frequencyIndex,:);

        % ---- Save gaussianity measure (number of time bins intentionally
        %      limited to shorter time range used to produce the likelihood
        %      maps).
        gaussianity{channelNumber} = ...
            squeeze(prctile(abs(timeFrequencyMap{channelNumber}),99,2)...
                    ./median(abs(timeFrequencyMap{channelNumber}),2)).^2;

    end
    clear timeFrequencyMap timeFrequencyMapFull

end %-- powerlaw needed 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             initialize time-frequency maps at zero delay                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- FFT the entire data stream to be analysed for each detector.
%      Note that we drop the last segment to make overlapping easier.  (If
%      there is only one segment it is not dropped!)
%      Start and stop indices for the FFT are the start and stop indices of
%      the first and last segments to be analysed:
segmentStartIndex = segmentIndices(1, 1);
segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2);

% ---- Construct window.
switch windowType
    case 'none'
        windowData = ones(integrationLength, 1);
    case 'bartlett'
        windowData = bartlett(integrationLength);
    case 'hann'
        windowData = hann(integrationLength);
    case 'modifiedhann'
        windowData = modifiedhann(integrationLength);
    otherwise
        disp('Unknown windowType. Using none.');
        windowData = ones(integrationLength, 1);
end
% ---- Rescale window to have unity mean square.
windowMeanSquare = mean(windowData.^2);
windowData = windowData / windowMeanSquare^0.5;
% ---- Make window data into array matching size of data to be FFTed.
windowData = repmat(windowData,[1,max(numberOfSegments-1,1)]);

% ---- Loop over detectors and FFT the data from each.  Use overlapping as
%      requested.
for channelNumber = 1 : numberOfChannels

    if (offsetFraction==1)
        % ---- FFT with no overlapping - this code is faster than the more 
        %      general version below.
        % ---- Extract data for this detector.
        data = conditionedData(segmentStartIndex:segmentStopIndex,channelNumber);
        % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
        %      so that each segment to be FFTed occupies one column.
        dataArray = reshape(data,integrationLength, numberOfTimeBins);
        % ---- Apply window and FFT.
        timeFrequencyMapFull{channelNumber} = fft( windowData .* dataArray );
    else
        % ---- Reshape the data into rectangular array, where consecutive
        %      columns contain data according to the requested overlap. 
        for j=1:1/offsetFraction
            % ---- Find start and stop indices.
            offsetStartIndex = segmentStartIndex + integrationLength*(j-1)*offsetFraction;
            offsetStopIndex = segmentStopIndex + integrationLength*(j-1)*offsetFraction;
            % ---- Extract data for this detector.
            data = conditionedData(offsetStartIndex:offsetStopIndex,channelNumber);
            % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
            %      so that each segment to be FFTed occupies one column.
            dataArray = reshape(data,integrationLength,[]);
            % ---- Time bins in full TF map that these segments correspond to.
            timeBins = [j:1/offsetFraction:numberOfTimeBins];
            % ---- Copy these FFTs into TF map for this channel.
            timeFrequencyMapFull{channelNumber}(:,timeBins) = ...
                fft( windowData .* dataArray );
        end
    end

    % ---- Extract in-band frequencies.
    timeFrequencyMap{channelNumber} = ...
        timeFrequencyMapFull{channelNumber}(frequencyIndex,:);

    % ---- Save the squared magnitudes as detectorSpectrograms.    
    detectorSpectrogram(:,:,channelNumber) = ...
        real(timeFrequencyMap{channelNumber}).^2 + ...
        imag(timeFrequencyMap{channelNumber}).^2;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   begin loop over sky positions                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Note: When comparing different sky positions, we keep all pixels in
%      any time bin for which the likelihood summed over that time bin is 
%      larger than the current largest summed likelihood recorded for that 
%      time bin for any other sky position.
% ---- Prepare storage for largest summed-over-frequency likelihoods.
maxSummedLikelihood = ones(1,numberOfTimeBins,numberOfLikelihoods)*-inf;

% ---- Precompute matrices for which only subset of coefficient are read
%      in each loop iteration
% -- matrices retrieving easily time or frequency indices
pixFreqFull = repmat((1:numberOfFrequencyBins)',[1 numberOfTimeBins]);
pixTimeFull = repmat((1:numberOfTimeBins),[numberOfFrequencyBins 1]);
% -- a choice of random pixels so that statistics are not computed only
%    for the loudest pixels
if not(strcmp(outputType,'timefrequencymap') || strcmp(outputType,'seedless'))  
  fixedMask = zeros(numberOfFrequencyBins, numberOfTimeBins);
  fixedMask(1:round(100/(100-blackPixelPrctile)):end) = 1;
  fixedMask = find(fixedMask);
end
    
for skyPositionNumber = 1 : numberOfSkyPositions,


    % ---- Status report.
    if (verboseFlag && (mod(skyPositionNumber, numberOfSkyPositions / 100) < 1))
        fprintf(1, 'processing sky position %d of %d (%d%% complete)...\n', ...
            skyPositionNumber, numberOfSkyPositions, ...
            round(100 * skyPositionNumber / numberOfSkyPositions));            
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %               compute detector responses
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
    % ---- Whitened responses for each detector, frequency x detector. 
    wFp = repmat(Fp(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ ...
          amplitudeSpectra;
    wFc = repmat(Fc(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ ...
          amplitudeSpectra;
    wFb = repmat(Fb(skyPositionNumber,:),[numberOfFrequencyBins 1]) ./ ...
          amplitudeSpectra;    
    % ---- Convert to dominant polarization (DP) frame.
    [wFpDPfull, wFcDPfull, psiDPfull] = convertToDominantPolarizationFrame(wFp,wFc);
    wFbDPfull = wFb; %-- no change to scalar mode     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           residual phase shifts for this sky position 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    residualTimeShiftPhases = exp(sqrt(-1) * 2 * pi * ...
        inbandFrequencies' * residualTimeShifts(skyPositionNumber,:));


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %          construct time-frequency maps for each channel   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % ---- Note that due to the time shifts the edges of the time-frequency
    %      map will contain small amounts of data from the transient 
    %      periods.

    % ---- Loop over detectors.  
    % for channelNumber = 1 : numberOfChannels
    % ---- Skip the first detector since it is used
    %      as the reference position, and therefore will always be at zero
    %      delay.  The advantage is that we only have to FFT its data once.
    for channelNumber = 2 : numberOfChannels


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                      FFT data                               %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Start and stop indices of the segment
        segmentStartIndex = segmentIndices(1, 1) ...
            + integerTimeShiftLengths(skyPositionNumber,channelNumber);
        segmentStopIndex = segmentIndices(max(numberOfSegments-1,1), 2) ...
            + integerTimeShiftLengths(skyPositionNumber,channelNumber);

        % ---- Separate-FFT-for-every-delay version
        if (offsetFraction==1)
            % ---- FFT with no overlapping - this code is faster than the more 
            %      general version below.
            % ---- Extract data for this detector.
            data = conditionedData(segmentStartIndex:segmentStopIndex,channelNumber);
            % ---- Reshape into array of size integrationLength x numberOfTimBins, 
            %      so that each segment to be FFTed occupies one column.
            dataArray = reshape(data,integrationLength, numberOfTimeBins);
            % ---- Apply window and FFT.
            timeFrequencyMapFull{channelNumber} = fft( windowData .* dataArray );
        else
            % ---- Reshape the data into rectangular array, where consecutive
            %      columns contain data according to the requested overlap. 
            for j=1:1/offsetFraction
                % ---- Find start and stop indices.
                offsetStartIndex = segmentStartIndex + integrationLength*(j-1)*offsetFraction;
                offsetStopIndex = segmentStopIndex + integrationLength*(j-1)*offsetFraction;
                % ---- Extract data for this detector.
                data = conditionedData(offsetStartIndex:offsetStopIndex,channelNumber);
                % ---- Reshape into array of size integrationLength x numberOfTimeBins, 
                %      so that each segment to be FFTed occupies one column.
                dataArray = reshape(data,integrationLength,[]);
                % ---- Time bins in full TF map that these segments correspond to.
                timeBins = [j:1/offsetFraction:numberOfTimeBins];
                % ---- Copy these FFTs into TF map for this channel.
                timeFrequencyMapFull{channelNumber}(:,timeBins) = ...
                    fft( windowData(:,1:size(dataArray,2)) .* dataArray );
            end
        end

        % ---- Retain only positive frequencies.
        timeFrequencyMap{channelNumber} = ...
            timeFrequencyMapFull{channelNumber}(frequencyIndex,:);

        % ---- Apply residual time shift.
        timeFrequencyMap{channelNumber} = timeFrequencyMap{channelNumber} ...
            .* repmat(residualTimeShiftPhases(:,channelNumber),[1,numberOfTimeBins]); 

    end % ---- loop over channels 
    

    % ---- Loop over internal circular time slides.
    for iCircSlide = 1:size(circTimeSlides,1)

        % ---- Construct clusters using total energy.
        switch outputType
            case {'clusters','injectionclusters','glitch'}
                % ---- Check for gates.
                gateMask = ones(numberOfFrequencyBins, numberOfTimeBins);
                if exist('allGates')==1
                    % ---- We need the circular time slide to figure out gate placement.
                    nTimePixelShiftedVect = circTimeSlides(iCircSlide,:)*sampleFrequency/offsetLength;
                    if any(round(nTimePixelShiftedVect)~=nTimePixelShiftedVect)
                        error(['Circular time shift of ' num2str(circTimeSlides(iCircSlide,:)) ...
                               ' is not a multiple of the time pixel step of ' num2str(offsetLength/sampleFrequency)]);
                    end
                    % ---- Divide map into sections based on gates.
                    [sectionStartIdx, sectionEndIdx, gateMask] = tfmapsections(allGates,sampleFrequency, ...
                        numberOfFrequencyBins,numberOfTimeBins,mapDim,nTimePixelShiftedVect);
                end
                blackPixelPrctileEffective = 100 - (100 - blackPixelPrctile) * mean(gateMask(:));
                % ---- Initialise variables. Do this only for the first time
                %      slide and first sky position. Note that we have not yet
                %      applied the circ slide so these calculations are done at
                %      zero lag.
                if iCircSlide == 1
                    totalEnergy = zeros(numberOfFrequencyBins, numberOfTimeBins);
                    for iChannel = 1:numberOfChannels
                        % ---- Determine black pixel threshold and black pixels in each detector. Ignore gated 
                        %      pixels when defining the threshold, so that loud glitches won't "use up" all the
                        %      black pixels. But include gated pixels when selecting black pixels - they are to
                        %      be analysed.
                        thisDetEnergy = real(timeFrequencyMap{iChannel}).^2 + imag(timeFrequencyMap{iChannel}).^2;
                        totalEnergy   = totalEnergy + thisDetEnergy;
                        if skyPositionNumber == 1
                            % ---- Black-pixel threshold for each detector. Mask out gated pixels (from all detectors - simplest) 
                            %      and correct requested percentile for portion of the map zeroed by gates.
                            blackThreshItf(iChannel) = simpleprctile(reshape(thisDetEnergy.*gateMask,[],1),blackPixelPrctileEffective);
                        end
                        % ---- Find black pixels in each detector, at zero lag. Include all pixels, including gated ones.
                        pixSingleUnshifted{iChannel} = find(thisDetEnergy > blackThreshItf(iChannel));
                    end
                    if skyPositionNumber == 1
                        % ---- Black-pixel threshold for total energy in the network. Mask out gated pixels (from all detectors - simplest) 
                        %      and correct requested percentile for portion of the map zeroed by gates.
                        blackThresh = simpleprctile(reshape(totalEnergy.*gateMask,[],1),blackPixelPrctileEffective);
                    end
                end
                for iChannel = 1:numberOfChannels
                    % ---- Apply circular time slide, use the fact that linear indices are column (frequency wise).
                    %      Play at +/-1 to get around indices starting at 1 and modulo at 0.
                    nTimePixelShifted = circTimeSlides(iCircSlide,iChannel)*sampleFrequency/offsetLength;
                    if abs(round(nTimePixelShifted)-nTimePixelShifted) > 0
                      error(['Circular time shift of ' num2str(circTimeSlides(iCircSlide,iChannel)) ...
                             ' is not a multiple of the time pixel step of ' num2str(offsetLength/sampleFrequency)]);
                    end
                    pixSingleShifted{iChannel} = shiftpixel(pixSingleUnshifted{iChannel}, ...
                                                            nTimePixelShifted*numberOfFrequencyBins, ...
                                                            numberOfTimeBins*numberOfFrequencyBins);
                end

                % ---- Construct pixel clusters.
                % ---- Combine black pixels from each detector.
                pixBlackMask = unique(vertcat(pixSingleShifted{:}));
                % ---- Undo circular time slide for each detector.
                for iChannel = 1:numberOfChannels
                    pixBlackUnshifted{iChannel} = shiftpixel(pixBlackMask, -circTimeSlides(iCircSlide,iChannel)*...
                                                             sampleFrequency/offsetLength*numberOfFrequencyBins,numberOfTimeBins*numberOfFrequencyBins);
                end

                % ---- Compute total energy for black pixels.
                totalEnergy = zeros(size(pixBlackMask));
                for iChannel = 1:numberOfChannels
                    totalEnergy = totalEnergy + ...
                        abs(timeFrequencyMap{iChannel}(pixBlackUnshifted{iChannel})).^2;
                end

                % ---- Recompute black pixel based on total energy.
                pixBlackMask = pixBlackMask(totalEnergy > blackThresh);
                totalEnergy = totalEnergy(totalEnergy > blackThresh);

                % ---- Find frequency bin of each retained pixel
                pixFreq = pixFreqFull(pixBlackMask);
                % ---- Find time bin of each retained pixel
                pixTime = pixTimeFull(pixBlackMask);


                % ---- Check for gates. If there are, split the TF map into sections as determined
                %      by the gates and cluster seach section separately. 
                if exist('allGates')==1
                    
                    % ---- Divide map into sections based on the tfmapsections() call above.
                    %      Perform clustering in loop over sections. 
                    labelledMap = [];
                    maxClusterLabel = 0;  %-- no clusters yet
                    for iSect = 1:length(sectionStartIdx)
                        % ---- Select only those black pixels that fall into this section.
                        sectIdx = find(pixTime>=sectionStartIdx(iSect) & pixTime<=sectionEndIdx(iSect));
                        % ---- Perform clustering. Ordinary 8-connected clustering
                        %      (no generalized clustering) using 'fast' algorithm.
                        if length(sectIdx)>0
                            try
                                labelledMap = [ labelledMap; maxClusterLabel + fastlabel([pixTime(sectIdx) pixFreq(sectIdx)], [numberOfTimeBins numberOfFrequencyBins], 8) ];
                                % ---- Update the current maximum cluster label.
                                maxClusterLabel = max(labelledMap);
                            catch
                                % ---- Extra verbosity for debugging.
                                disp(['Problem for map section ' num2str(iSect)])
                                disp(['size(labelledMap) = ' num2str(size(labelledMap))])
                                disp(['size(maxClusterLabel) = ' num2str(size(maxClusterLabel))])
                                tmp = fastlabel([pixTime(sectIdx) pixFreq(sectIdx)], [numberOfTimeBins numberOfFrequencyBins], 8);
                                disp(['size(fastlabel()) = ' num2str(size(tmp))])
                                error('Array size problem in call to fastlabel()')
                            end
                        end
                    end

                else

                    % ---- Perform clustering. Ordinary 8-connected clustering
                    %      (no generalized clustering) using 'fast' algorithm, applied to entire map as one.  
                    labelledMap = fastlabel([pixTime pixFreq], [numberOfTimeBins numberOfFrequencyBins], 8);

                end

                % ---- Remove pixels for clusters that have less than genClusteringThresh
                %      times the energy threshold for black pixels.
                %      If we have applied autoGating above then we should have
                %      genClusteringThresh(1) == 0 and this step is skipped.
                if genClusteringThresh(1) > 0
                    clusterArray = fastsparseclusterprop(labelledMap, totalEnergy);
                    keptLabelList = find(clusterArray > genClusteringThresh(1)*blackThresh);
                    % ---- Keep at least one cluster.
                    if isempty(keptLabelList)
                        keptLabelList = 1;
                    end
                    keptPixelMask = clustertopixel(labelledMap, keptLabelList)>0;
                    pixFreq = pixFreq(keptPixelMask);
                    pixTime = pixTime(keptPixelMask);
                    totalEnergy = totalEnergy(keptPixelMask);
                    pixBlackMask = pixBlackMask(keptPixelMask);

                    % ---- Cluster the remaining pixels again but with higher
                    %      connectivity (skip 1 pixel), compute also the
                    %      time-frequency properties of clusters.
                    labelledMap = fastlabel([pixTime pixFreq], [numberOfTimeBins numberOfFrequencyBins], genClusteringThresh(2));
                end

                % ---- Compute clusters with their time-frequency properties.
                clusterArray = fastsparseclusterprop(labelledMap, totalEnergy, pixTime, pixFreq);
                nClusters = size(clusterArray,1);

                % ---- Scale output units to seconds and Hertz
                T0 = mapDim(1);
                dT = mapDim(2);
                F0 = mapDim(3);
                dF = mapDim(4);
                clusterArray(:,1:3) = (clusterArray(:,1:3) - 1) * dT + T0;
                clusterArray(:,4:6) = (clusterArray(:,4:6) - 1) * dF + F0;

                % ---- Find mask of pixels for which to compute likelihoods.
                additionalPix = setdiff(fixedMask,pixBlackMask);
                pixMask = [pixBlackMask; additionalPix];
                labelledMap((end+1):(end+length(additionalPix)),1) = 0;
                % ---- Undo circular time slide for each detector.
                for iChannel = 1:numberOfChannels
                    pixMaskUnshifted{iChannel} = shiftpixel(pixMask, - circTimeSlides(iCircSlide,iChannel)*...
                                                            sampleFrequency/offsetLength*numberOfFrequencyBins,numberOfTimeBins*numberOfFrequencyBins);
                end

                % ---- Find frequency bin of each retained pixel.
                pixFreq = pixFreqFull(pixMask);
                % ---- Find time bin of each retained pixel.
                pixTime = pixTimeFull(pixMask);

                % ---- Desired output: all clusters for this sky position.
                loudestCluster{skyPositionNumber,iCircSlide} = [clusterArray zeros(size(clusterArray,1),numberOfLikelihoods-1)];

            otherwise

                % ---- Keep all pixels.
                pixMask = true(numberOfFrequencyBins, numberOfTimeBins);
                pixFreq = pixFreqFull(:);
                pixTime = pixTimeFull(:);

                % ---- Apply circular time slide.
                for channelNumber = 1 : numberOfChannels
                    pixMaskUnshifted{channelNumber} = pixMask;
                    if sum(abs(circTimeSlides(:)))
                        if iCircSlide == 1
                            timeFrequencyMap{channelNumber} = circshift(timeFrequencyMap{channelNumber}, ...
                                                                [0 circTimeSlides(iCircSlide,channelNumber)*...
                                                                sampleFrequency/offsetLength]);
                        else
                            timeFrequencyMap{channelNumber} = circshift(timeFrequencyMap{channelNumber}, ...
                                                                [0 (circTimeSlides(iCircSlide,channelNumber)-...
                                                                circTimeSlides(iCircSlide-1,channelNumber))*...
                                                                sampleFrequency/offsetLength]);
                        end
                    end 
                end % -- loop over channels 

        end % -- switch outputType

        % ---- Matrix M_AB components.  These are the dot products of wFp, with
        %      themselves and each other, for each frequency, computed in
        %      the DP frame.
        wFpDP = wFpDPfull(pixFreq,:);
        wFcDP = wFcDPfull(pixFreq,:);
        wFbDP = wFbDPfull(pixFreq,:);
        psiDP = psiDPfull(pixFreq,:);
        Mpp = sum(wFpDP.*wFpDP,2);
        Mcc = sum(wFcDP.*wFcDP,2);
        Mbb = sum(wFbDP.*wFbDP,2);
        epsilon = Mcc./Mpp;

        % ---- Retain relevant time frequency map bins
        for iChannel = 1:numberOfChannels
          pixTFmap{iChannel} = timeFrequencyMap{iChannel}(pixMaskUnshifted{iChannel});
          pixEnergyMap{iChannel} = real(pixTFmap{iChannel}).^2 + ...
              imag(pixTFmap{iChannel}).^2;
        end

        % ---- Compute antenna-response weighted time-frequency maps.
        channelNumber = 1;
        wFpTimeFrequencyMap = zeros(size(pixFreq)); 
        wFcTimeFrequencyMap = zeros(size(pixFreq)); 
        wFbTimeFrequencyMap = zeros(size(pixFreq));

        for channelNumber = 1 : numberOfChannels
            wFpTimeFrequencyMap = wFpTimeFrequencyMap + ...
                pixTFmap{channelNumber} .* wFpDP(:,channelNumber);
            wFcTimeFrequencyMap = wFcTimeFrequencyMap + ... 
                pixTFmap{channelNumber} .* wFcDP(:,channelNumber);
            wFbTimeFrequencyMap = wFbTimeFrequencyMap + ...
                pixTFmap{channelNumber} .* wFbDP(:,channelNumber);
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                compute likelihood maps                      %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % ---- Compute each of the requested likelihoodType maps
        for jLikelihoodType = 1:numberOfLikelihoods
            % ---- Initialize likelihood map to zero
            likelihood = zeros(size(pixFreq));

            % ---- Compute given likelihood type
            switch likelihoodType{jLikelihoodType}

                case 'loghbayesiancirc'
                    % ---- Tikhonov-regularized likelihood for cicularly polarised GWs, with
                    %      Bayesian Occam factor that depends on sky direction. Hardwired range
                    %      of GWB amplitudes.
                    % ---- This likelihood is computed differently from most others because it
                    %      involves marginalisation over model parameters (amplitude and left/right
                    %      polarisation). As a consequence, we need to retain the likelihoods for 
                    %      each set of model parameters. The marginalisation is done in the 
                    %      "process likelihood maps" section below, c. line 2406.
                    % ---- KLUDGE: The new code will only work when we're using the reduced TF maps, 
                    %      not the full TF maps.
                    % ---- Resize and change initialization of likelihood to -infinity since likelihoods 
                    %      will be combined by logsumexp, so -infinity gives null contribution.
                    likelihood = repmat(-inf(size(likelihood)),1,10);
                    % ---- Compute the statistic.
                    irat=sqrt(-1);
                    wFrightTimeFrequencyMap = (wFpTimeFrequencyMap + irat*wFcTimeFrequencyMap)/sqrt(2);
                    wFrightEnergyMap = real(wFrightTimeFrequencyMap).^2 + imag(wFrightTimeFrequencyMap).^2;
                    wFleftTimeFrequencyMap = (wFpTimeFrequencyMap - irat*wFcTimeFrequencyMap)/sqrt(2);
                    wFleftEnergyMap = real(wFleftTimeFrequencyMap).^2 + imag(wFleftTimeFrequencyMap).^2;
                    Mppcc = (Mpp + Mcc)/2 ; 
                    iii = 1;
                    for strainSigma = 10.^[-23:0.5:-21];
                        invSM = (Mppcc + strainSigma^-2).^(-1);
                        logSM = log(1 + strainSigma^2 * Mppcc);
                        likelihood(:,iii)   = 1/2*( invSM .* wFrightEnergyMap - logSM); 
                        likelihood(:,iii+1) = 1/2*( invSM .* wFleftEnergyMap - logSM); 
                        iii = iii+2;
                    end
                case 'standard'
                    if (alignedNetwork)
                        % ---- For an aligned network (i.e., H1,H2), we choose to 
                        %      interpret standard likelihood = plus energy.  
                        % ---- Hard constraint likelihood detection statistic
                        likelihood = Mpp.^(-1) .* ( ...
                            real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 ...
                        );
                    else
                        % ---- Standard likelihood detection statistic.
                        likelihood = Mpp.^(-1) .* ( ...
                            real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                            + Mcc.^(-1) .* ( ...
                            real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2);
                    end
                case 'totalenergy'
                    % ---- Total energy (incoherent) detection statistic.
                    channelNumber = 1;
                    likelihood = pixEnergyMap{channelNumber};
                    for channelNumber = 2 : numberOfChannels
                    likelihood = likelihood ...
                        + pixEnergyMap{channelNumber};
                    end
                case 'circenergy'
                        % ---- Compute larger of left/right
                        % circularly
                        %      polarized GWB.  (-) sign for last term SHOULD
                        %      correspond 
                        %      to hp = cos(), hc = sin(), or inspiral with
                        %      iota=0.
                        likelihood1 = ...
                            real(wFpTimeFrequencyMap).^2 + ...
                            imag(wFpTimeFrequencyMap).^2 ...
                            + real(wFcTimeFrequencyMap).^2 + ...
                            imag(wFcTimeFrequencyMap).^2 ...
                            - 2 * (imag(conj(wFpTimeFrequencyMap).* ...
                                        wFcTimeFrequencyMap));
                        likelihood1 = likelihood1 ./ (Mpp+Mcc);
                        likelihood2 = ...
                            real(wFpTimeFrequencyMap).^2 + ...
                            imag(wFpTimeFrequencyMap).^2 ...
                            + real(wFcTimeFrequencyMap).^2 + ...
                            imag(wFcTimeFrequencyMap).^2 ...
                            + 2 * (imag(conj(wFpTimeFrequencyMap).* ...
                                        wFcTimeFrequencyMap));
                        likelihood2 = likelihood2 ./ (Mpp+Mcc);
                        likelihood=max(likelihood1,likelihood2);
                case 'circinc'
                    % ---- For a general network (aligned or not):
                    for channelNumber = 1 : numberOfChannels
                      likelihood = likelihood + ...
                          (wFpDP(:,channelNumber).^2 + wFcDP(:,channelNumber).^2) ...
                          ./ (Mpp + Mcc) .*  ...
                           pixEnergyMap{channelNumber};
                    end
                case 'circnullenergy'
                        % ---- Compute larger of left/right
                        % circularly
                        %      polarized GWB.  (-) sign for last term SHOULD
                        %      correspond 
                        %      to hp = cos(), hc = sin(), or inspiral with
                        %      iota=0.
                        irat=sqrt(-1);
                        nMcc = Mcc./sqrt(Mpp.*Mcc);
                        nMpp = Mpp./sqrt(Mpp.*Mcc);
                        likelihood1 = abs(nMcc.*wFpTimeFrequencyMap + irat*nMpp.*wFcTimeFrequencyMap).^2./...
                            (Mpp+Mcc);
                        likelihood2 = abs(nMcc.*wFpTimeFrequencyMap - irat*nMpp.*wFcTimeFrequencyMap).^2./...
                            (Mpp+Mcc);
                        likelihood = min(likelihood1,likelihood2);
                case 'circnullinc'
                    % ---- For a general network (aligned or not):
                    for channelNumber = 1 : numberOfChannels
                      likelihood = likelihood + ( ...
                          ((Mcc.*wFpDP(:,channelNumber)).^2 + (Mpp.*wFcDP(:,channelNumber)).^2) ...
                          ./ ((Mpp + Mcc).*Mpp.*Mcc)) .* ...
                          pixEnergyMap{channelNumber} ;
                    end
                case 'powerlaw'
                   % -- If clusters already defined compute likelihood only
                   %    for those pixels                
                   if(numberOfChannels == 2 && not(H1H2network))
                     likelihood = ...
                         pixEnergyMap{1}.^(sqrt(11)./gaussianity{1}(pixFreq)).*...
                         pixEnergyMap{2}.^(sqrt(11)./ ...
                                                    gaussianity{2}(pixFreq));
                   elseif(numberOfChannels == 3 && not(H1H2network))
                     likelihood = ...
                         (pixEnergyMap{1}.^(sqrt(11)./gaussianity{1}(pixFreq))).*...
                         (pixEnergyMap{2}.^(sqrt(11)./gaussianity{2}(pixFreq))).*...
                         (pixEnergyMap{3}.^(sqrt(11)./gaussianity{3}(pixFreq)));

                     wNull = cross(wFpDP./repmat(sqrt(Mpp),[1 3]),wFcDP./...
                                   repmat(sqrt(Mcc),[1 3]),2);
                     N1 = abs(wNull(:,1));
                     N2 = abs(wNull(:,2));
                     N3 = abs(wNull(:,3));
                     % ---- Compute total energy
                     nullamplitude = zeros(size(pixFreq));
                     for channelNumber = 1 : numberOfChannels
                       nullamplitude = nullamplitude ...
                           + pixEnergyMap{channelNumber};
                     end
                     % ---- Subtract standard likelihood energy.
                     nullamplitude = nullamplitude - Mpp.^(-1) .* ( ...
                         real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                         - Mcc.^(-1).* ( ...
                             real(wFcTimeFrequencyMap).^2 + ...
                             imag(wFcTimeFrequencyMap).^2);
                     nullamplitude = sqrt(nullamplitude);
                     likelihood = likelihood.*...
                         (1./(1+abs(nullamplitude./N1).^(sqrt(44)./gaussianity{1}(pixFreq))) ...
                          + ...
                          1./(1+abs(nullamplitude./N2).^(sqrt(44)./gaussianity{2}(pixFreq))) ...
                          + ...
                          1./(1+abs(nullamplitude./N3).^(sqrt(44)./gaussianity{3}(pixFreq))) ...
                          );
                     likelihood = log(likelihood);
                   end
                case 'skypositiontheta'

                case 'skypositionphi'

                case 'energyitf1'
                    likelihood = pixEnergyMap{1};

                case 'energyitf2'
                    likelihood = pixEnergyMap{2};

                case 'energyitf3'
                    likelihood = pixEnergyMap{3};

                case 'energyitf4'
                    likelihood = pixEnergyMap{4};

                case 'loghbayesian'
                    % ---- Tikhonov-regularized standard likelihood, with Bayesian Occam factor
                    %      that depends on sky direction. Hardwired range of GWB amplitudes.
                    % ---- This likelihood is computed differently from most others because it
                    %      involves marginalisation over model parameters (amplitude) 
                    %      As a consequence, we need to retain the likelihoods for 
                    %      each set of model parameters. The marginalisation is done in the 
                    %      "process likelihood maps" section below, c. line 2406.
                    % ---- KLUDGE: The new code will only work when we're using the reduced TF maps, 
                    %      not the full TF maps.
                    % ---- Resize and change initialization of likelihood to -infinity since likelihoods 
                    %      will be combined by logsumexp, so -infinity gives null contribution.
                    likelihood = repmat(-inf(size(likelihood)),1,5);
                    % ---- Compute the statistic.
                    iii = 1;
                    for strainSigma = 10.^[-23:0.5:-21];
                        likelihood(:,iii) = 1/2 * ( ...
                              (Mpp + strainSigma^-2).^(-1) .* (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                            + (Mcc + strainSigma^-2).^(-1) .* (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                            - log((1 + strainSigma^2 * Mpp) .* (1 + strainSigma^2 * Mcc)) ...
                        ); 
                        iii = iii+1;
                    end
                    % % ---- Original code:
                    % likelihood = -inf(size(likelihood));
                    % for strainSigma = 10.^[-23:0.5:-21];
                    %     likelihoodtemp = 1/2 * ( ...
                    %         (Mpp + strainSigma^-2).^(-1) .* ...
                    %             (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                    %         + (Mcc + strainSigma^-2).^(-1) .* ...
                    %             (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                    %         - log((1 + strainSigma^2 * Mpp) .* (1 + strainSigma^2 * Mcc)) ...
                    %     ); 
                    %     likelihood = logsumexp(likelihood,likelihoodtemp);
                    % end

                case 'loghbayesianscalar'
                    % ---- Tikhonov-regularized scalar-mode likelihood, with Bayesian Occam factor
                    %      that depends on sky direction. Hardwired range of GWB amplitudes.
                    % ---- This likelihood is computed differently from most others because it
                    %      involves marginalisation over model parameters (amplitude) 
                    %      As a consequence, we need to retain the likelihoods for 
                    %      each set of model parameters. The marginalisation is done in the 
                    %      "process likelihood maps" section below, c. line 2406.
                    % ---- KLUDGE: The new code will only work when we're using the reduced TF maps, 
                    %      not the full TF maps.
                    % ---- Resize and change initialization of likelihood to -infinity since likelihoods 
                    %      will be combined by logsumexp, so -infinity gives null contribution.
                    likelihood = repmat(-inf(size(likelihood)),1,5);
                    % ---- Compute the statistic.
                    iii = 1;
                    for strainSigma = 10.^[-23:0.5:-21];
                        likelihood(:,iii) = 1/2 * ( ...
                              (Mbb + strainSigma^-2).^(-1) .* (real(wFbTimeFrequencyMap).^2 + imag(wFbTimeFrequencyMap).^2) ...
                            - log(1 + strainSigma^2 * Mbb) ...
                        );
                        iii = iii+1;
                    end
                    % % ---- Original code.
                    % likelihood = -inf(size(likelihood));
                    % for strainSigma = 10.^[-23:0.5:-21];
                    %     likelihoodtemp = 1/2 * ( ...
                    %          (Mbb + strainSigma^-2).^(-1) .* ...
                    %              (real(wFbTimeFrequencyMap).^2 + imag(wFbTimeFrequencyMap).^2) ...
                    %          - log((1 + strainSigma^2 * Mbb)));
                    %     likelihood = logsumexp(likelihood,likelihoodtemp);
                    % end

                case 'crossenergy'
                    % ---- Energy in the cross polarization in the DP frame.
                    likelihood = Mcc.^(-1) .* ( ...
                        real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2 ...
                        );
                case 'crossinc'
                    % ---- Incoherent component (autocorrelation terms) of the
                    %      energy in the cross polarization in the DP frame.
                    if (~alignedNetwork)
                        % ---- For an aligned network (i.e., H1,H2), we have
                        %      no sensitivity to the cross polarization.  Do
                        %      nothing (i.e., report zeros).
                        % ---- For a general non-aligned network:
                        for channelNumber = 1 : numberOfChannels
                            likelihood = likelihood + ( ...
                                    wFcDP(:,channelNumber).^2 ./ Mcc ...
                                ) .*  pixEnergyMap{channelNumber};
                        end
                    end
                case {'plusenergy','hardconstraint'}
                    % ---- Energy in the plus polarization in the DP frame.
                    %      Also known as the dard constraint likelihood.
                    likelihood = Mpp.^(-1) .* ( ...
                        real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 ...
                        );
                case 'plusinc'
                    % ---- Incoherent component (autocorrelation terms) of the
                    %      energy in the plus polarization in the DP frame.
                    % ---- For a general network (aligned or not):
                    for channelNumber = 1 : numberOfChannels
                        likelihood = likelihood + ( ...
                                wFpDP(:,channelNumber).^2 ./ Mpp ...
                            ) .* pixEnergyMap{channelNumber};
                    end

                case 'scalarenergy'

                    % ---- Energy in the scalar polarization.
                    likelihood = Mbb.^(-1) .* ( ...
                        real(wFbTimeFrequencyMap).^2 + imag(wFbTimeFrequencyMap).^2 ...
                        );

                case 'scalarinc'

                    % ---- Incoherent component (autocorrelation terms) of the
                    %      energy in the scalar polarization.
                    for channelNumber = 1 : numberOfChannels
                        likelihood = likelihood + ( ...
                                wFb(pixFreq,channelNumber).^2 ./ Mbb ...
                            ) .* ( ...
                                real(pixTFmap{channelNumber}).^2 ...
                                + imag(pixTFmap{channelNumber}).^2 ...
                            ) ;
                    end

                case 'ampenergy'
                 % -- coherent buildup of energy assuming |hc| ~ |hp|, phase
                 %    between hp and hc chosen to maximize the buildup
                 % -- This is the projection on the manifold spanned by 
                 %    |hp| = |hc|
                 optPhase = conj(wFcTimeFrequencyMap) .* wFpTimeFrequencyMap;
                 optPhase = optPhase ./ abs(optPhase);
                 projection = wFpTimeFrequencyMap + ...
                     optPhase .* wFcTimeFrequencyMap;
                 likelihood = (real(projection).^2 + imag(projection).^2)./ ...
                     (Mpp + Mcc);
                case 'ampinc'
                 % -- incoherent counterpart to ampenergy
                 optPhase = conj(wFcTimeFrequencyMap) .* wFpTimeFrequencyMap;
                 optPhase = optPhase ./ abs(optPhase);
                 for channelNumber = 1 : numberOfChannels
                   likelihood = likelihood + ...
                       (wFpDP(:,channelNumber).^2 + wFcDP(:,channelNumber).^2 + ...
                        2*real(optPhase).*wFpDP(:,channelNumber).* ...
                        wFcDP(:,channelNumber)) ./ (Mpp + Mcc) .*  ...
                       pixEnergyMap{channelNumber};
                 end
                case 'ampnullenergy'
                 % -- Coherent cancellation energy under the assumption 
                 %    |hp| ~ |hc|. This is the magnitude of the difference
                 %    vector between the data vector on its projection on
                 %    |hp| = |hc| manifold.
                 optPhase = conj(wFcTimeFrequencyMap) .* wFpTimeFrequencyMap;
                 optPhase = optPhase ./ abs(optPhase);
                 projection = wFpTimeFrequencyMap./Mpp - ...
                     optPhase .* wFcTimeFrequencyMap./Mcc;
                 likelihood = (real(projection).^2 + imag(projection).^2)./ ...
                     (1./Mpp + 1./Mcc);
                case 'ampnullinc'
                 % -- Incoherent counterpart to ampnullenergy
                 optPhase = conj(wFcTimeFrequencyMap) .* wFpTimeFrequencyMap;
                 optPhase = optPhase ./ abs(optPhase);
                 for channelNumber = 1 : numberOfChannels
                   likelihood = likelihood + ...
                       (wFpDP(:,channelNumber).^2./Mpp.^2 + ...
                        wFcDP(:,channelNumber).^2./Mcc.^2 - ...
                        2*real(optPhase).*wFpDP(:,channelNumber).* ...
                        wFcDP(:,channelNumber)./Mpp./Mcc)./ ...
                       (1./Mpp + 1./Mcc) .* ...
                       pixEnergyMap{channelNumber};
                 end
                case 'bandwidth'
                 % -- cluster bandwidth, in the sens of frequency standard
                 %    deviation over pixels in the cluster, currently will
                 %    yield zero if cluster spans only one frequency bin
                 %    most of the  computation is done in post-processing, compute
                 %    frequency and frequency squared here
                 likelihood1 = inbandFrequencies(pixFreq);
                 likelihood2 = inbandFrequencies(pixFreq).^2;
                case 'aglitchhardconstraint'
                    % ---- ???
                    glitchPrior=0.01;
                    display('Using aglitchhardconstraint');
                    penaltyMap=zeros(size(timeFrequencyMap{1}));
                    for channelNumber = 1 : numberOfChannels
                        penaltyMap = penaltyMap + ...
                            exp(0.5*repmat(Mpp.^(-1),[1 numberOfTimeBins]) ...
                            .*(abs(timeFrequencyMap{channelNumber} .* ...
                            repmat(wFpDP(:,channelNumber),[1,numberOfTimeBins])).^2));
                    end
                    likelihood = 0.5*repmat(Mpp.^(-1),[1 numberOfTimeBins]) .* ( ...
                        real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 ...
                        ) - log(1+glitchPrior*penaltyMap);
                case 'bayesian1e-21'
                    % ---- Tikhonov-regularized standard likelihood, with
                    %      Bayesian Occam factor that depends on sky direction.
                    % ---- Hardwired parameter for GWB amplitude.
                    strainSigma = 1e-21;
                    likelihood = 1/2 * ( ...
                        repmat((Mpp + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                        + repmat((Mcc + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                        - repmat(log((1 + strainSigma^2 * Mpp) .* (1 + strainSigma^2 * Mcc)), ...
                            [1 numberOfTimeBins]) ...
                    );                
                case 'bayesian3e-22'
                    % ---- Tikhonov-regularized standard likelihood, with
                    %      Bayesian Occam factor that depends on sky direction.
                    % ---- Hardwired parameter for GWB amplitude.
                    strainSigma = 3e-22;
                    likelihood = 1/2 * ( ...
                        repmat((Mpp + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                        + repmat((Mcc + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                        - repmat(log((1 + strainSigma^2 * Mpp) .* (1 + strainSigma^2 * Mcc)), ...
                            [1 numberOfTimeBins]) ...
                    );                
                case 'bayesian1e-22'
                    % ---- Tikhonov-regularized standard likelihood, with
                    %      Bayesian Occam factor that depends on sky direction.
                    % ---- Hardwired parameter for GWB amplitude.
                    strainSigma = 1e-22;
                    likelihood = 1/2 * ( ...
                        repmat((Mpp + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                        + repmat((Mcc + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                        - repmat(log((1 + strainSigma^2 * Mpp) .* (1 + strainSigma^2 * Mcc)), ...
                            [1 numberOfTimeBins]) ...
                    );                
                case 'bayesian3e-23'
                    % ---- Tikhonov-regularized standard likelihood, with
                    %      Bayesian Occam factor that depends on sky direction.
                    % ---- Hardwired parameter for GWB amplitude.
                    strainSigma = 3e-23;
                    likelihood = 1/2 * ( ...
                        repmat((Mpp + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                        + repmat((Mcc + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                        - repmat(log((1 + strainSigma^2 * Mpp) .* (1 + strainSigma^2 * Mcc)), ...
                            [1 numberOfTimeBins]) ...
                    );                
                case 'bayesian1e-23'
                    % ---- Tikhonov-regularized standard likelihood, with
                    %      Bayesian Occam factor that depends on sky direction.
                    % ---- Hardwired parameter for GWB amplitude.
                    strainSigma = 1e-23;
                    likelihood = 1/2 * ( ...
                        repmat((Mpp + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                        + repmat((Mcc + strainSigma^-2).^(-1),[1 numberOfTimeBins]) .* ...
                            (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                        - repmat(log((1 + strainSigma^2 * Mpp) .* (1 + strainSigma^2 * Mcc)), ...
                            [1 numberOfTimeBins]) ...
                    );                
                case 'bg'
                    % ---- Combined 'bayesian' and 'glitch' hypotheses.
                    strainSigma = 1e-22;
                    glitchPrior = .01;
                    glitchSigma = 10;
                    ic = zeros(numberOfFrequencyBins, numberOfChannels, numberOfChannels);
                    for i = 1:numberOfFrequencyBins
                        f = [wFp(i,:); wFc(i,:)]';
                        ict = eye(numberOfChannels)-f*inv(f'*f+eye(2)*strainSigma^-2)*f';
                        likelihood(i, :) = repmat(1/2*log(det(ict)), [1, numberOfTimeBins]);
                        ic(i, :, :) = ict;
                    end
                    ic = permute(ic, [2, 3, 1]);
                    for i = 1:3
                        for j = 1:3
                            t = squeeze(ic(i,j,:));
                            likelihood = likelihood - 1/2 .* ( ...
                                real(timeFrequencyMap{i}) .* real(timeFrequencyMap{j}) ...
                                + imag(timeFrequencyMap{i}) .* imag(timeFrequencyMap{j}) ...
                            ) .* repmat(t, [1, numberOfTimeBins]);
                        end
                        energy = real(timeFrequencyMap{i}).^2 + imag(timeFrequencyMap{i}).^2;
                        likelihood = likelihood - log( ...
                            (1-glitchPrior)/sqrt(2*pi)*exp(-1/2*energy) ...
                            + glitchPrior/glitchSigma*exp(-1/(2*glitchSigma^2)*energy) ...
                        );
                    end
                case 'circnullenergyreg'
                 % ---- Compute antenna-response weighted regularized time-frequency maps.
                 channelNumber = 1;
                 wFpTimeFrequencyMap = timeFrequencyMap{channelNumber} .* ...
                     sqrt(max(1-1./abs(timeFrequencyMap{channelNumber}).^2,0)).* ...
                     repmat(wFpDP(:,channelNumber),[1,numberOfTimeBins]); 
                 wFcTimeFrequencyMap = timeFrequencyMap{channelNumber} .* ...
                     sqrt(max(1-1./abs(timeFrequencyMap{channelNumber}).^2,0)).* ...
                     repmat(wFcDP(:,channelNumber),[1,numberOfTimeBins]); 
                 for channelNumber = 2 : numberOfChannels
                   wFpTimeFrequencyMap = wFpTimeFrequencyMap + ...
                       timeFrequencyMap{channelNumber} .* ...
                       sqrt(max(1-1./abs(timeFrequencyMap{channelNumber}).^2,0)).* ...
                       repmat(wFpDP(:,channelNumber),[1,numberOfTimeBins]); 
                   wFcTimeFrequencyMap = wFcTimeFrequencyMap + ... 
                       timeFrequencyMap{channelNumber} .* ...
                       sqrt(max(1-1./abs(timeFrequencyMap{channelNumber}).^2,0)).* ...
                       repmat(wFcDP(:,channelNumber),[1,numberOfTimeBins]);
                 end
                 irat=sqrt(-1);
                 nMcc = repmat(Mcc./sqrt(Mpp.*Mcc),[1  numberOfTimeBins]);
                 nMpp = repmat(Mpp./sqrt(Mpp.*Mcc),[1  numberOfTimeBins]);
                 likelihood1 = abs(nMcc.*wFpTimeFrequencyMap + irat*nMpp.*wFcTimeFrequencyMap).^2./...
                            repmat(Mpp+Mcc,[1  numberOfTimeBins]);
                 likelihood2 = abs(nMcc.*wFpTimeFrequencyMap - irat*nMpp.*wFcTimeFrequencyMap).^2./...
                            repmat(Mpp+Mcc,[1  numberOfTimeBins]);
                case 'circnullincreg'
                 % ---- For a general network (aligned or not):
                 for channelNumber = 1 : numberOfChannels
                   likelihood = likelihood + repmat( ...
                       ((Mcc.*wFpDP(:,channelNumber)).^2 + (Mpp.*wFcDP(:,channelNumber)).^2) ...
                       ./ ((Mpp + Mcc).*Mpp.*Mcc), [1,numberOfTimeBins] ) .* ...
                       ( abs(timeFrequencyMap{channelNumber}.* ...
                             sqrt(max(1-1./abs(timeFrequencyMap{channelNumber}).^2,0))).^2 ) ;
                 end


                case 'duration'
                 % -- cluster duration, in the sens of time standard
                 %    deviation over pixels in the cluster, currently will
                 %    yield zero if cluster spans only one time bin
                 %    most of the computation is done in post-processing,
                 %    compute time and time squared here
                 likelihood1 = ((pixTime-1)*mapDim(2) + mapDim(1));
                 likelihood2 = ((pixTime-1)*mapDim(2) + mapDim(1)).^2;
                case 'dev'
                    % ---- Elliptic likelihood coherent detection statistic, 
                    %      with regularization.
                    % ---- Regularizer: different for each frequency bin and sky 
                    %      position.
                    maxNormF2 = sum(wFp.^2+wFc.^2,2);
                    % ---- Loop over elliptic "templates" (internalAngle
                    %      values).
                    for internalAngleNumber = 1:numberOfInternalAngles
                        % ---- Projection operator.  Work in Earth-based
                        %      polarization frame, NOT dominant polarization
                        %      frame.  The Earth-based frame is the same for
                        %      all frequencies.
                        psi = internalAngles(internalAngleNumber,1);
                        eta = internalAngles(internalAngleNumber,2);
                        %
                        % ---- Construct elliptic projection operator.
                        coeffFp = cos(2*psi) + sqrt(-1)*sin(2*psi) .* eta;
                        coeffFc = sin(2*psi) - sqrt(-1)*cos(2*psi) .* eta;
                        projF = coeffFp * wFp + coeffFc * wFc;
                        % ---- Include regulator with hard-wired strength.
                        % normF = sum(real(projF).^2+imag(projF).^2,2).^0.5;
                        normF = (sum(real(projF).^2+imag(projF).^2,2) + 0.1*maxNormF2).^0.5;
                        projF = projF ./ repmat(normF,[1,numberOfChannels]); 
                        % ---- Compute |F.d|^2 / |F|^2.
                        templikelihood = zeros(numberOfFrequencyBins,numberOfTimeBins);
                        for channelNumber = 1 : numberOfChannels
                            templikelihood = templikelihood + ...
                                repmat(projF(:,channelNumber),[1,numberOfTimeBins]) ...
                                .* timeFrequencyMap{channelNumber};
                        end
                        templikelihood = real(templikelihood).^2 + imag(templikelihood).^2 ;
                        %
                        % ---- Keep all pixels in any time bin for which the 
                        %      likelihood summed over that time bin is larger
                        %      than the current largest summed likelihood
                        %      recorded for that time bin.
                        % ---- Time bins to keep.
                        index = find(sum(templikelihood,1)>maxSummedLikelihood(1,:,jLikelihoodType));
                        % ---- Record those largest summed likelihoods.
                        maxSummedLikelihood(1,index,jLikelihoodType) = sum(templikelihood(:,index),1);
                        % ---- Record the corresponding sky positions.
                        skyPositionIndex(1,index,jLikelihoodType) = skyPositionNumber;
                        % ---- Record the corresponding internal angles.
                        internalAngleIndex(1,index,jLikelihoodType) = internalAngleNumber;
                        % ---- Copy those time bins into the current TF map.
                        likelihood(:,index) = templikelihood(:,index);
                        % ---- Copy those time bins into the output TF map.
                        likelihoodTimeFrequencyMap(:,index,jLikelihoodType) = likelihood(:,index);                    
                    end               

                case 'elliptic'
                    % ---- Elliptic likelihood coherent detection statistic.
                    % ---- Loop over elliptic "templates" (internalAngle
                    %      values).
                    for internalAngleNumber = 1:numberOfInternalAngles
                        % ---- Projection operator.  Work in Earth-based
                        %      polarization frame, NOT dominant polarization
                        %      frame.  The Earth-based frame is the same for
                        %      all frequencies.
                        psi = internalAngles(internalAngleNumber,1);
                        eta = internalAngles(internalAngleNumber,2);
                        %
                        % ---- Construct elliptic projection operator.
                        coeffFp = cos(2*psi) + sqrt(-1)*sin(2*psi) .* eta;
                        coeffFc = sin(2*psi) - sqrt(-1)*cos(2*psi) .* eta;
                        projF = coeffFp * wFp + coeffFc * wFc;
                        normF = sum(real(projF).^2+imag(projF).^2,2).^0.5;
                        projF = projF ./ repmat(normF,[1,numberOfChannels]); 
                        % ---- Compute |F.d|^2 / |F|^2.
                        templikelihood = zeros(numberOfFrequencyBins,numberOfTimeBins);
                        for channelNumber = 1 : numberOfChannels
                            templikelihood = templikelihood + ...
                                repmat(projF(:,channelNumber),[1,numberOfTimeBins]) ...
                                .* timeFrequencyMap{channelNumber};
                        end
                        templikelihood = real(templikelihood).^2 + imag(templikelihood).^2 ;
                        %
                        % ---- Keep all pixels in any time bin for which the 
                        %      likelihood summed over that time bin is larger
                        %      than the current largest summed likelihood
                        %      recorded for that time bin.
                        % ---- Time bins to keep.
                        index = find(sum(templikelihood,1)>maxSummedLikelihood(1,:,jLikelihoodType));
                        % ---- Record those largest summed likelihoods.
                        maxSummedLikelihood(1,index,jLikelihoodType) = sum(templikelihood(:,index),1);
                        % ---- Record the corresponding sky positions.
                        skyPositionIndex(1,index,jLikelihoodType) = skyPositionNumber;
                        % ---- Record the corresponding internal angles.
                        internalAngleIndex(1,index,jLikelihoodType) = internalAngleNumber;
                        % ---- Copy those time bins into the current TF map.
                        likelihood(:,index) = templikelihood(:,index);
                        % ---- Copy those time bins into the output TF map.
                        likelihoodTimeFrequencyMap(:,index,jLikelihoodType) = likelihood(:,index);
                    end
                case 'geometric'
                    likelihood = reshape(geomean(...
                        [abs(pixTFmap{1}(:)).^2 ...
                         abs(pixTFmap{2}(:)).^2],2),size(pixTFmap{2}));

                case 'glitch'
                    % ---- Tikhonov-regularized likelihood ratio of simple 
                    %      glitch model vs. noise.
                    % ---- Hardwired parameters for glitch probability and
                    %      amplitude.
                    glitchPrior = 0.01;
                    glitchSigma = 10;
                    for i = 1:numberOfChannels
                        energy = real(timeFrequencyMap{i}).^2 + imag(timeFrequencyMap{i}).^2;
                        likelihood = likelihood + log( ...
                            (1-glitchPrior)/sqrt(2*pi)*exp(-1/2*energy) ...
                            + glitchPrior/glitchSigma*exp(-1/(2*glitchSigma^2)*energy) ...
                        );
                    end
                    % ---- Make power-like.
                    likelihood = -likelihood;
                case 'glitchhardconstraint'
                    % ---- Likelihood statistic for GWB vs. loud glitch
                    %      hypotheses.
                    glitchPrior=0.01;
                    penaltyMap=zeros(size(timeFrequencyMap{1}));
                    for channelNumber = 1 : numberOfChannels
                        penaltyMap=penaltyMap+exp(0.5*(abs(timeFrequencyMap{channelNumber}).^2));
                    end
                    likelihood = 0.5*repmat(Mpp.^(-1),[1 numberOfTimeBins]) .* ( ...
                        real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 ...
                        ) - log(1+glitchPrior*penaltyMap);
                case 'glitchstandard'
                    % ---- Likelihood statistic for GWB vs. loud glitch
                    %      hypotheses.
                    glitchPrior=0.5;
                    penaltyMap=zeros(size(timeFrequencyMap{1}));
                    for channelNumber = 1 : numberOfChannels
                        penaltyMap=penaltyMap+exp(0.5*(abs(timeFrequencyMap{channelNumber}).^2));
                    end
                    % ---- Now compute standard likelihood map and subtract
                    %      penalty map from it.  For details of the
                    %      calculation, see case 'standard'. 
                    if (alignedNetwork)
                        channelNumber = 1;
                        likelihood = real(timeFrequencyMap{channelNumber}).^2 ...
                            + imag(timeFrequencyMap{channelNumber}).^2 ;
                        for channelNumber = 2 : numberOfChannels
                        likelihood = likelihood ...
                            + real(timeFrequencyMap{channelNumber}).^2 ...
                            + imag(timeFrequencyMap{channelNumber}).^2 ;
                        end
                        likelihood = 0.5*likelihood - log(1+glitchPrior*penaltyMap);
                    else
                        likelihood = 0.5*repmat(Mpp.^(-1),[1 numberOfTimeBins]) .* ( ...
                            real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 )...
                            + 0.5*repmat(Mcc.^(-1),[1 numberOfTimeBins]) .* ( ...
                            real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                            - log(1+glitchPrior*penaltyMap);
                    end
                case 'H1H2nullenergy'
                    % ---- Null energy statistic for the H1-H2 null stream.
                    %      The following code will compute the null energy
                    %      associated with H1-H2 regardless of what other
                    %      detectors are in the network.  If H1 or H2 is not in
                    %      the network then the energy is returned as 0. 
                    %      Note: When H1-H2 is the only null stream available, 
                    %      H1H2nullenergy = nullenergy to machine precision.
                    if (H1H2network)
                        % ---- For the H1-H2 sub-netowrk, the associated 
                        %      "whitened" null energy is given by 
                        %        E_null = 1/(S1+S2) * |S1^0.5*d1 - S2^0.5*d2|^2
                        %               = 1/(A1^2+A2^2) * |A1*d1 - A2*d2|^2
                        %      d1, d2 are the whitened data streams and S1, S2 
                        %      (A1, A2) are the corresponding power (amplitude) 
                        %      spectra.
                        % ---- Compute null energy.
                        A1 = repmat(amplitudeSpectra(:,indexH1),[1 numberOfTimeBins]); 
                        A2 = repmat(amplitudeSpectra(:,indexH2),[1 numberOfTimeBins]); 
                        z = A1 .* timeFrequencyMap{indexH1} ...
                            - A2 .* timeFrequencyMap{indexH2};
                        likelihood = real(z).^2 + imag(z).^2;
                        likelihood = likelihood ./ (A1.^2+A2.^2);
                        clear A1 A2 z
                    end
                case 'H1H2nullinc'
                    % ---- Incoherent component of the null energy for H1-H2. 
                    %      The following code will compute the "nullinc"
                    %      associated with H1-H2 regardless of what other
                    %      detectors are in the network.  If H1 or H2 is not in
                    %      the network then the energy is returned as 0.
                    %      Note: When H1-H2 is the only null stream available, 
                    %      H1H2nullinc = nullinc to machine precision.
                    if (H1H2network)
                        % ---- For the H1-H2 sub-netowrk, the associated "whitened" 
                        %      nullinc statistic is given by 
                        %        I_null = 1/(S1+S2) * (S1*|d1|^2 + S2*|d2|^2)
                        %               = 1/(A1^2+A2^2) * (A1^2*|d1|^2 + A2^2*|d2|^2)
                        %      d1, d2 are the whitened data streams and S1, S2 
                        %      (A1, A2) are the corresponding power (amplitude) 
                        %      spectra.
                        % ---- Compute nullinc statistic.
                        S1 = repmat(amplitudeSpectra(:,indexH1).^2,[1 numberOfTimeBins]);
                        S2 = repmat(amplitudeSpectra(:,indexH2).^2,[1 numberOfTimeBins]);
                        likelihood = S1 .* ( real(timeFrequencyMap{indexH1}).^2 ...
                            + imag(timeFrequencyMap{indexH1}).^2 ) ...
                            + S2 .* ( real(timeFrequencyMap{indexH2}).^2 ...
                            + imag(timeFrequencyMap{indexH2}).^2 );
                        likelihood = likelihood ./ (S1+S2);
                        clear S1 S2
                    end
                case 'maxglitchhardconstraint'
                    % ---- ???
                    loudestChannel=ones(size(timeFrequencyMap{1}));
                    loudestTFpixel=zeros(size(timeFrequencyMap{1}));
                    for channelNumber = 1 : numberOfChannels
                        mask = loudestTFpixel < abs(timeFrequencyMap{channelNumber}).^2-log(100);
                        loudestTFpixel(mask) = abs(timeFrequencyMap{channelNumber}(mask)).^2-log(100);
                        loudestChannel(mask) = channelNumber;
                    end
                    loudestTFpixel=sqrt(loudestTFpixel);
                    likelihood =  repmat(Mpp.^(-1),[1 numberOfTimeBins]) .* ( ...
                        real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 ) ...
                        - repmat(Mpp.^(-1),[1 numberOfTimeBins]) .*(abs(loudestTFpixel .* ...
                        wFpDP(repmat((1:numberOfFrequencyBins)', ...
                        [1 numberOfTimeBins])+(loudestChannel-1)*numberOfFrequencyBins) ).^2);
                case 'maxglitchstandard' 
                    % ---- ???
                    loudestTFpixel=zeros(size(timeFrequencyMap{1}));
                    for channelNumber = 1 : numberOfChannels
                        mask = loudestTFpixel < abs(timeFrequencyMap{channelNumber}).^2;
                        loudestTFpixel(mask) = abs(timeFrequencyMap{channelNumber}(mask)).^2;
                    end
                    if (alignedNetwork)
                        % ---- For an aligned network (i.e., H1,H2), we have
                        %      standard = total. 
                        % ---- Compute total energy.
                        channelNumber = 1;  
                        likelihood = real(timeFrequencyMap{channelNumber}).^2 ... 
                            + imag(timeFrequencyMap{channelNumber}).^2 ;  
                        for channelNumber = 2 : numberOfChannels 
                        likelihood = likelihood ... 
                            + real(timeFrequencyMap{channelNumber}).^2 ... 
                            + imag(timeFrequencyMap{channelNumber}).^2 ;
                        end    
                    else 
                        likelihood = repmat(Mpp.^(-1),[1 numberOfTimeBins]) .* ( ...  
                            real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2 )... 
                            + repmat(Mcc.^(-1),[1 numberOfTimeBins]) .* ( ... 
                            real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ;
                    end             
                    likelihood = likelihood - loudestTFpixel;
                case 'nullenergy'
                    % ---- Null energy statistic.
                    if (alignedNetwork)
                        % ---- For an aligned network (i.e., H1,H2), we have
                        %      null energy = total - hard, so compute total  
                        %      and hard separately and take the difference.
                        % ---- Compute total energy.
                        channelNumber = 1;
                        likelihood = pixEnergyMap{channelNumber};
                        for channelNumber = 2 : numberOfChannels
                        likelihood = likelihood ...
                            + pixEnergyMap{channelNumber};
                        end
                        % ---- Subtract hard constraint likelihood.
                        likelihood = likelihood - Mpp.^(-1) .* ( ...
                            real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2);
                    elseif (numberOfChannels == 2)
                        % ---- Report zeros; the actual computation gives only
                        %      round-off errors anyway! 
                        likelihood = zeros(size(pixTFmap{1}));
                    else
                        % ---- For an general non-aligned network we have
                        %      null energy = total - standard, so compute total 
                        %      and standard separately and take the difference.
                        % ---- Compute total energy.
                        channelNumber = 1;
                        likelihood = pixEnergyMap{channelNumber};
                        for channelNumber = 2 : numberOfChannels
                        likelihood = likelihood ...
                            + pixEnergyMap{channelNumber};
                        end
                        % ---- Subtract standard likelihood energy.
                        likelihood = likelihood - Mpp.^(-1) .* ( ...
                            real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                            - Mcc.^(-1) .* ( ...
                            real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2);
                    end
                case {'nullinc','incoherentenergy'}
                    % ---- Incoherent energy statistic: diagonal
                    %      (autocorrelation) terms of null energy.
                    if (alignedNetwork)
                        % ---- For an aligned network (i.e., H1,H2), we have
                        %      null energy = total - hard.
                        for channelNumber = 1 : numberOfChannels
                            likelihood = likelihood ...
                                + ( ...
                                    1 - wFpDP(:,channelNumber).^2 ./ Mpp ...
                                ) .* pixEnergyMap{channelNumber};
                        end
                    elseif (numberOfChannels == 2)
                        % ---- Report zeros; the actual computation gives only
                        %      round-off errors anyway! 
                        likelihood = zeros(size(pixTFmap{1}));
                    else
                        % ---- For a general non-aligned network we have
                        %      null energy = total - standard.
                        for channelNumber = 1 : numberOfChannels
                            likelihood = likelihood ...
                                + ( ...
                                    1 - wFpDP(:,channelNumber).^2 ./ Mpp ...
                                    - wFcDP(:,channelNumber).^2 ./ Mcc ...
                                ) .* pixEnergyMap{channelNumber};
                        end
                    end

                case 'scalarnullenergy'

                    % ---- Null energy statistic.
                    % ---- For a general network we have
                    %      null energy = total - scalar, so compute total 
                    %      and scalar separately and take the difference.
                    % ---- Compute total energy.
                    channelNumber = 1;
                    likelihood = pixEnergyMap{channelNumber};
                    for channelNumber = 2 : numberOfChannels
                    likelihood = likelihood ...
                        + pixEnergyMap{channelNumber};
                    end
                    % ---- Subtract standard likelihood energy.
                    likelihood = likelihood - Mbb.^(-1) .* ( ...
                        real(wFbTimeFrequencyMap).^2 + imag(wFbTimeFrequencyMap).^2);

                case 'scalarnullinc'

                    % ---- For a general non-aligned network we have
                    %      null energy = total - scalar.
                    for channelNumber = 1 : numberOfChannels
                        likelihood = likelihood ...
                            + ( ...
                                1 - wFb(pixFreq,channelNumber).^2 ./ Mbb ...
                            ) .* ( ...
                                real(pixTFmap{channelNumber}).^2 ...
                                + imag(pixTFmap{channelNumber}).^2 ...
                            ) ;
                    end

                case 'powerlawfixed'
                   for iChannel = 1:numberOfChannels
                     gaussianityFixed{iChannel} = ...
                         sqrt(44)*ones(numberOfFrequencyBins,numberOfTimeBins);
                   end
                   if(numberOfChannels == 2 && not(H1H2network))
                     likelihood = ...
                         abs(timeFrequencyMap{1}).^(sqrt(44)./gaussianityFixed{1}).*...
                         abs(timeFrequencyMap{2}).^(sqrt(44)./ ...
                                                    gaussianityFixed{2});
                   elseif(numberOfChannels == 3 && not(H1H2network))
                     likelihood = ...
                         (abs(timeFrequencyMap{1}).^(sqrt(44)./gaussianityFixed{1})).*...
                         (abs(timeFrequencyMap{2}).^(sqrt(44)./gaussianityFixed{2})).*...
                         (abs(timeFrequencyMap{3}).^(sqrt(44)./ ...
                                                    gaussianityFixed{3}));

                     wNull = cross(wFpDP./repmat(sqrt(Mpp),[1 3]),wFcDP./...
                                   repmat(sqrt(Mcc),[1 3]),2);
                     N1 = abs(repmat(wNull(:,1),[1 numberOfTimeBins]));
                     N2 = abs(repmat(wNull(:,2),[1 numberOfTimeBins]));
                     N3 = abs(repmat(wNull(:,3),[1 numberOfTimeBins]));
                     % ---- Compute total energy
                     nullamplitude = zeros(numberOfFrequencyBins, ...
                                           numberOfTimeBins);
                     for channelNumber = 1 : numberOfChannels
                       nullamplitude = nullamplitude ...
                           + real(timeFrequencyMap{channelNumber}).^2 ...
                           + imag(timeFrequencyMap{channelNumber}).^2 ;
                     end
                     % ---- Subtract standard likelihood energy.
                     nullamplitude = nullamplitude - repmat(Mpp.^(-1),[1 ...
                                         numberOfTimeBins]) .* ( ...
                         real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                         ...
                         - repmat(Mcc.^(-1),[1 numberOfTimeBins]) .* ( ...
                             real(wFcTimeFrequencyMap).^2 + ...
                             imag(wFcTimeFrequencyMap).^2);
                     nullamplitude = sqrt(nullamplitude);
                     likelihood = likelihood.*...
                         (1./(1+abs(nullamplitude./N1).^(sqrt(44)./gaussianityFixed{1})) ...
                          + ...
                          1./(1+abs(nullamplitude./N2).^(sqrt(44)./gaussianityFixed{2})) ...
                          + ...
                          1./(1+abs(nullamplitude./N3).^(sqrt(44)./ ...
                                                      gaussianityFixed{3})));
                     likelihood = log(likelihood);
                   end

                case 'softconstraint'
                    % ---- Soft constraint detection statistic.
                    likelihood = repmat(Mpp.^(-1),[1 numberOfTimeBins]) .* ( ...
                          (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                        + (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                    );
                    % % ---- Noise-only distribution of soft constraint
                    % %      likelihood varies with sky position.  To compare
                    % %      different sky positions, convert "likelihood" to
                    % %      ln(probability density).
                    % likelihood = xsoftconstraintsignificance(2*likelihood,epsilon);
                    % % ---- Normalize so the for epsilon->0 we get hard
                    % %      constraint.
                    % likelihood = -(likelihood + log(2));
                case 'unbiasedsoft'
                    % ---- Soft constraint detection statistic, with mean for 
                    %      sky position removed.
                    likelihood = repmat((Mpp+Mcc).^(-1),[1 numberOfTimeBins]) .* ( ...
                          (real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2) ...
                        + (real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                    );
                case 'timeshift1'

                case 'timeshift2'

                case {'elinear', 'ilinear', 'psi', 'elinearnull', 'ilinearnull'}

                    if (exist('psi_earth_max','var')==0)

                        % ---- Compute likelihood for best-fit linear polarization.
                        %      Use unregularized version (delta->Inf).
                        %      Include scale factor (which cancels out off all
                        %      calculations) to avoid over/underflow errors. 
                        scale = 1e-85;
                        realdpdc = real(conj(wFpTimeFrequencyMap).*wFcTimeFrequencyMap);
                        alpha = 2 * realdpdc .* (Mpp - Mcc) * scale;
                        beta  = 2 * (Mpp.*(real(wFcTimeFrequencyMap).^2 + imag(wFcTimeFrequencyMap).^2) ...
                                   - Mcc.*(real(wFpTimeFrequencyMap).^2 + imag(wFpTimeFrequencyMap).^2)) * scale;
                        gamma = 2 * realdpdc .* (Mpp + Mcc) * scale;

                        % ---- The quadratic equation gives two possible solutions
                        %      based on the sign of the discriminant term.  Also,
                        %      we have a "x"->"x+pi" ambiguity from the atan()
                        %      function. There are thus 4 possible solutions, and I
                        %      don't know how to select the right one a priori.  We
                        %      therefore check each and select the one that gives
                        %      the smallest value of abs(alpha+beta*sin()+gamma*cos()).
                        % ---- Compute all possible solutions.
                        psi4 = zeros(length(alpha),4);
                        psi4(:,1) = 0.25*atan((-beta.*gamma+alpha.*(gamma.^2+beta.^2-alpha.^2).^0.5)./(beta.^2-alpha.^2));
                        psi4(:,2) = psi4(:,1) + pi/4;
                        psi4(:,3) = 0.25*atan((-beta.*gamma-alpha.*(gamma.^2+beta.^2-alpha.^2).^0.5)./(beta.^2-alpha.^2));
                        psi4(:,4) = psi4(:,3) + pi/4;
                        % ---- Wrap psi values to [0,pi/2).
                        psi4 = mod(psi4, pi/2);
                        % check4 = abs(repmat(alpha,1,4) + repmat(beta,1,4).*sin(4*psi4) + repmat(gamma,1,4).*cos(4*psi4))./std(gamma);

                        % ---- Compute the likelihood for each of the four values of psi.
                        likelihood4 = zeros(length(alpha),4);
                        for ii = 1:4
                            % ---- Likelihood in dominant polarization frame.
                            likelihood4(:,ii) = ( ((abs(wFpTimeFrequencyMap)).^2).*(cos(2*psi4(:,ii)).^2) + ...
                                ((abs(wFcTimeFrequencyMap)).^2).*(sin(2*psi4(:,ii)).^2) + ...
                                2*(realdpdc.*sin(2*psi4(:,ii))).*cos(2*psi4(:,ii)) ) ...
                                ./ ( Mpp.*(cos(2*psi4(:,ii))).^2 + Mcc.*(sin(2*psi4(:,ii))).^2 );
                        end

                        % ---- Select largest likelihood for psi for elinear
                        % ilinear and psi
                        [likelihood, index4] = max(likelihood4,[],2);
                        % ---- Tricky bit to get the indices right.  This would be easy with row vectors ...
                        index4converted = sub2ind(size(psi4),[1:length(index4)]',index4);
                        psimax = psi4(index4converted);
                        % ---- Select smallest likelihood for psi for elinearnull
                        % ilinearnull.
                        [likelihood, index4] = min(likelihood4,[],2);
                        % ---- Tricky bit to get the indices right.  This would be easy with row vectors ...
                        index4converted = sub2ind(size(psi4),[1:length(index4)]',index4);
                        psimin = psi4(index4converted);
                        % ---- Loop over all clusters, find average psi for each. 
                        %      Record psi both in DPF (for calculations) and Earth-based
                        %      frame (for output).
                        psi_earth_max = zeros(size(psimax));
                        psi_earth_min = zeros(size(psimax));
                        npix = zeros(size(psimax));

                        psi_avg_map_min = 1/4*atan2(sin(4*(psimin+psiDP)), cos(4*(psimin+psiDP)));
                        psi_avg_map_max = 1/4*atan2(sin(4*(psimax+psiDP)), cos(4*(psimax+psiDP)));

                        % Do single pixel case
                        bins = unique(labelledMap);
                        [a,b]=histc(labelledMap,bins);
                        y = a(b);
                        index = find(y==1);
                        npix(index) = y(index);
                        psi_earth_max(index) = psi_avg_map_max(index);
                        psimax(index) = psi_avg_map_max(index)-psiDP(index);
                        psi_earth_min(index) = psi_avg_map_min(index);
                        psimin(index) = psi_avg_map_min(index)-psiDP(index);

                        % Do double pixel case
                        index = find(y==2);
                        npix(index) = y(index);
                        twoclustersmap = labelledMap(index);
                        twoclusters = unique(twoclustersmap);
                        index1 = zeros(2,length(twoclusters));
                        for ii = 1:length(twoclusters)
                           indexes = find(labelledMap == twoclusters(ii));
                           index1(:,ii) = indexes;
                        end
                        psi_avg = 1/4*atan2(mean(sin(4*(psimax(index1)+psiDP(index1)))), mean(cos(4*(psimax(index1)+psiDP(index1)))));
                        psi_avg_repmat = repmat(psi_avg,2,1);
                        psi_avg_repmat = psi_avg_repmat(:);
                        index(1:2:length(index)) = index1(1,:)';
                        index(2:2:length(index)) = index1(2,:)';
                        psi_earth_max(index) = psi_avg_repmat;
                        psimax(index) = psi_avg_repmat-psiDP(index);
                        psi_avg = 1/4*atan2(mean(sin(4*(psimin(index1)+psiDP(index1)))), mean(cos(4*(psimin(index1)+psiDP(index1)))));
                        psi_avg_repmat = repmat(psi_avg,2,1);
                        psi_avg_repmat = psi_avg_repmat(:);
                        index(1:2:length(index)) = index1(1,:)';
                        index(2:2:length(index)) = index1(2,:)';
                        psi_earth_min(index) = psi_avg_repmat;
                        psimin(index) = psi_avg_repmat-psiDP(index);

                        % Do triple pixel case
                        index = find(y==3);
                        npix(index) = y(index);
                        threeclustersmap = labelledMap(index);
                        threeclusters = unique(threeclustersmap);
                        index1 = zeros(3,length(threeclusters));
                        for ii = 1:length(threeclusters)
                           indexes = find(labelledMap == threeclusters(ii));
                           index1(:,ii) = indexes;
                        end
                        psi_avg = 1/4*atan2(mean(sin(4*(psimax(index1)+psiDP(index1)))), mean(cos(4*(psimax(index1)+psiDP(index1)))));
                        psi_avg_repmat = repmat(psi_avg,3,1);
                        psi_avg_repmat = psi_avg_repmat(:);
                        index(1:3:length(index)) = index1(1,:)';
                        index(2:3:length(index)) = index1(2,:)';
                        index(3:3:length(index)) = index1(3,:)';
                        psi_earth_max(index) = psi_avg_repmat;
                        psimax(index) = psi_avg_repmat-psiDP(index);
                        psi_avg = 1/4*atan2(mean(sin(4*(psimin(index1)+psiDP(index1)))), mean(cos(4*(psimin(index1)+psiDP(index1)))));
                        psi_avg_repmat = repmat(psi_avg,3,1);
                        psi_avg_repmat = psi_avg_repmat(:);
                        index(1:3:length(index)) = index1(1,:)';
                        index(2:3:length(index)) = index1(2,:)';
                        index(3:3:length(index)) = index1(3,:)';
                        psi_earth_min(index) = psi_avg_repmat;
                        psimin(index) = psi_avg_repmat-psiDP(index);

                        % Do multi pixel case
                        multindex = bins(find((a~=1) & (a~=2) & (a~=3)));
                        multindex = multindex(multindex~=0);
                        for aa = 1:length(multindex)
                            kk = multindex(aa);
                            % ---- Find all pixels belonging to cluster kk.
                            index = find(labelledMap == kk);
                            % ---- Number of pixels in cluster kk.
                            npix(index) = length(index);
                            % ---- Compute average psi angle over all pixels in this 
                            %      cluster.  We need to do this in the Earth-based 
                            %      frame, not the DPF! 
                            % ---- Give all pixels in the cluster equal weighting.  
                            %      (This works about as well as likelihood-weighted 
                            %      averaging.  It also works for both elinear (maximum 
                            %      likelihood) and elinearnull (minimum likelihood).

                            psi_avg = 1/4*atan2(mean(sin(4*(psimax(index)+psiDP(index)))), mean(cos(4*(psimax(index)+psiDP(index)))));
                            % ---- Weight each pixel by likelihood when calculating the
                            %      average psi.  This does not improve psi estimation 
                            %      significantly.  Also we'd need a special case for 
                            %      the minimum likelihood case.
                            % meany = mean(likelihood(index).*sin(4*(psi(index)+psiDP(index)))); 
                            % meanx = mean(likelihood(index).*cos(4*(psi(index)+psiDP(index))));
                            % psi_avg = 1/4*atan2(meany, meanx);
                            % ---- Set psi for each pixel to this average value.  Record
                            %      in both Earth-fixed and DP frames.
                            psi_earth_max(index) = psi_avg;
                            psimax(index) = psi_avg-psiDP(index);
                            psi_avg = 1/4*atan2(mean(sin(4*(psimin(index)+psiDP(index)))), mean(cos(4*(psimin(index)+psiDP(index)))));
                            psi_earth_min(index) = psi_avg;
                            psimin(index) = psi_avg-psiDP(index);
                        end

                    % ---- Wrap psi values to [0,pi/2).  
                    psi_earth_max = mod(psi_earth_max, pi/2);
                    psi_earth_min = mod(psi_earth_min, pi/2);
                    end

                    % ---- Requested output.
                    switch likelihoodType{jLikelihoodType}
                            case 'psi'
                                % ---- Desired output is the psi angle.  Rescale by 
                                %      1/npix so that clustering (which sums over all 
                                %      clusters) returns the desired psi_earth value.
                                likelihood = psi_earth_max ./ npix;
                            case 'elinear' % Fine the way it is
                                % ---- Recompute coherent likelihood with averaged psi.
                                likelihood = ( ((abs(wFpTimeFrequencyMap)).^2).*(cos(2*psimax).^2) + ...
                                    ((abs(wFcTimeFrequencyMap)).^2).*(sin(2*psimax).^2) + ...
                                    2*(realdpdc.*sin(2*psimax)).*cos(2*psimax) ) ...
                                    ./ ( Mpp.*(cos(2*psimax)).^2 + Mcc.*(sin(2*psimax)).^2 );
                            case 'elinearnull' % needs min
                                % ---- Recompute coherent likelihood with averaged psi.
                                likelihood = ( ((abs(wFpTimeFrequencyMap)).^2).*(cos(2*psimin).^2) + ...
                                    ((abs(wFcTimeFrequencyMap)).^2).*(sin(2*psimin).^2) + ...
                                    2*(realdpdc.*sin(2*psimin)).*cos(2*psimin) ) ...
                                    ./ ( Mpp.*(cos(2*psimin)).^2 + Mcc.*(sin(2*psimin)).^2 );
                            case 'ilinear' % Fine the way it is
                                % ---- Recompute incoherent likelihood with averaged psi.
                                likelihood = zeros(size(likelihood));
                                for channelNumber = 1 : numberOfChannels
                                    likelihood = likelihood + ...
                                        ( wFpDP(:, channelNumber) .* cos(2*psimax) + ...
                                          wFcDP(:, channelNumber) .* sin(2*psimax) ).^2 .* ...
                                        pixEnergyMap{channelNumber};
                                end
                                likelihood = likelihood ./ (Mpp.*(cos(2*psimax)).^2 + Mcc.*(sin(2*psimax)).^2);
                            case 'ilinearnull' % Needs min
                                % ---- Recompute incoherent likelihood with averaged psi.
                                likelihood = zeros(size(likelihood));
                                for channelNumber = 1 : numberOfChannels
                                    likelihood = likelihood + ...
                                        ( wFpDP(:, channelNumber) .* cos(2*psimin) + ...
                                          wFcDP(:, channelNumber) .* sin(2*psimin) ).^2 .* ...
                                        pixEnergyMap{channelNumber};
                                end
                                likelihood = likelihood ./ (Mpp.*(cos(2*psimin)).^2 + Mcc.*(sin(2*psimin)).^2);
                    end

                otherwise
                    error(['No algorithm for computing likelihoodType ' ...
                        likelihoodType{jLikelihoodType} ]);

            end  % -- end switch likelihoodType


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                process likelihood maps                      %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            switch outputType

                case {1, 'followup'}

                    % ---- Process time-frequency map to find clusters.
                    if (jLikelihoodType == 1)
                        % ---- Find clusters of map pixels with large likelihood in the
                        %      first likelihood map (the first likelihood type is the 
                        %      "detection statistic").  
                        % ---- Compute likelihood threshold to give black-pixel probability
                        %      of 1% for the detection statistic likelihood.
                        % clustering needs positive weigths to compute correctly peak
                        % time and peak frequency, loghbayesian can become
                        % negative if the sky position considered is highly unlikely
                        blackThresh = max(prctile(likelihood(:),blackPixelPrctileEffective),0);
                        % ---- For cluster properties in physical units:
                        % ---- Call clustering algorithm.  Ordinary connected-4 clustering
                        %      (no generalized clustering) using 'fast' algorithm.  Output
                        %      cluster properties in physical units (sec, Hz).
                        [clusterArray, clusterStruct, labelledMap] = clusterTFmapNew( ...
                            likelihood,1,'fastconnected',blackThresh,8,0,0,0,0,mapDim);

                        % ---- Desired output: loudest cluster for each sky position.
                        % ---- Find clusters which overlap the time interval of +/-1
                        %      second around the center of the map.
                        overlapClusters = find( ...
                            (clusterArray(:,maxTcol) > blockLength/sampleFrequency/2 - 1) & ...
                            (clusterArray(:,minTcol) < blockLength/sampleFrequency/2 + 1) );
                        % ---- Select the loudest cluster (the one with the largest
                        %      first likelihood).  Store it, remembering to leave
                        %      space for the other likelhoods to be computed.
                        [M I] = max(clusterArray(overlapClusters,likelihoodColOffset+1));
                        loudestCluster(skyPositionNumber,:) = [ ...
                            clusterArray(overlapClusters(I),:) zeros(1,numberOfLikelihoods-1)];
                        % ---- Delete all other clusters from the labelled map and 
                        %      reset cluster label to 1.
                        %      Note: the 1* is required so that the labelledMap is 
                        %      double array rather than a logical array.
                        loudestLabel = overlapClusters(I);
                        labelledMap = 1*(labelledMap == loudestLabel); 
                    else
                        % ---- Compute "auxiliary" likelihood for pre-defined
                        %      clusters in labelledMap.
                        clusterArray = fastclusterprop(labelledMap, likelihood);
                        % ---- Record only likelihood value from clusterArray.
                        loudestCluster(skyPositionNumber,likelihoodColOffset+jLikelihoodType) = ...
                            clusterArray(end);
                    end

                case {'sparseclusters'}

                    % ---- Process time-frequency map to find clusters.
                    if (jLikelihoodType == 1)
                        % ---- Find clusters of map pixels with large likelihood in the
                        %      first likelihood map (the first likelihood type is the 
                        %      "detection statistic").  
                        % ---- Compute likelihood threshold to give black-pixel probability
                        %      of 1% for the detection statistic likelihood.
                        % clustering needs positive weigths to compute correctly peak
                        % time and peak frequency, loghbayesian can become
                        % negative if the sky position considered is highly unlikely
                        blackThresh = max(prctile(likelihood(:),blackPixelPrctileEffective),0);
                        % ---- For cluster properties in physical units:
                        % ---- Call clustering algorithm.  Ordinary connected-4 clustering
                        %      (no generalized clustering) using 'fast' algorithm.  Output
                        %      cluster properties in physical units (sec, Hz).
                        [clusterArray, clusterStruct, labelledMap] = clusterTFmapNew( ...
                            likelihood,1,'fastconnected',blackThresh,8,0,0,0,0,mapDim);

                        % ---- Desired output: loudest cluster in each 1-second
                        %      block over all sky positions. 
                        %      KLUDGE: This bit of code assumes we are analysing the entire
                        %      time interval except the start and end transient intervals.
                        %      A more robust code would be based purely on the
                        %      segmentIndices or mapDim variables.
                        secondBins = [1 : (blockLength-2*transientLength)/sampleFrequency];
                        % ---- Find loudest cluster in each 1-sec block for
                        %      this sky position.  Use peak time.
                        labelledMap_temp = zeros(size(labelledMap));
                        % for secondBinRightEdge = [1:blockLength/sampleFrequency];
                        for secondBinRightEdge = secondBins;
                            overlapClusters = find( ceil(clusterArray(:,meanTcol)) == secondBinRightEdge+transientLength/sampleFrequency );
                            % ---- Select the loudest cluster (the one with the largest
                            %      first likelihood).  Store it if it is louder than loudest previously found in this bin.
                            M = [];
                            if length(overlapClusters)
                                [M I] = max(clusterArray(overlapClusters,likelihoodColOffset+1));
                            end
                            if (length(M) & M > loudestCluster(secondBinRightEdge,likelihoodColOffset+1))
                                loudestCluster(secondBinRightEdge,:) = [ ...
                                    clusterArray(overlapClusters(I),:) zeros(1,numberOfLikelihoods-1)];
                                % ---- Add this cluster to the labelled map and 
                                %      reset cluster label to the bin number.
                                loudestLabel = overlapClusters(I);
                                labelledMap_temp = labelledMap_temp + ...
                                    (secondBinRightEdge)*(labelledMap == loudestLabel); 
                            end
                        end
                        labelledMap = labelledMap_temp;
                        clear labelledMap_temp
                    else
                        % ---- Compute "auxiliary" likelihood for pre-defined
                        %      clusters in labelledMap.
                        clusterArray = fastclusterprop(labelledMap, likelihood);
                        % ---- Record only likelihood value from clusterArray.
                        k = find(clusterArray(:,end) > 0);
                        loudestCluster(k,likelihoodColOffset+jLikelihoodType) = ...
                            clusterArray(k,end);
                    end

                case {'clusters','injectionclusters','glitch'}

                        % ---- Compute "auxiliary" likelihood for pre-defined
                        %      clusters in labelledMap.
                        switch likelihoodType{jLikelihoodType}
                         case 'powerlaw'
                          newLabelledMap = zeros(size(likelihood));
                          if skyPositionNumber == 1
                              powerlawThr = simpleprctile(likelihood(:), 50);
                          end
                          bwSubMask = likelihood > powerlawThr & labelledMap > 0;
                          if sum(bwSubMask(:)) > 0
                              if genClusteringThresh(1) > 0
                                  newLabelledMap(bwSubMask) = ...
                                      fastlabel([pixTime(bwSubMask) pixFreq(bwSubMask)],...
                                                [numberOfTimeBins numberOfFrequencyBins], ...
                                                genClusteringThresh(2));
                              else
                                  newLabelledMap(bwSubMask) = ...
                                      fastlabel([pixTime(bwSubMask) pixFreq(bwSubMask)],...
                                                [numberOfTimeBins numberOfFrequencyBins], ...
                                                8);
                              end
                          end
                          statSumMap = statisticSumLabelledMap(newLabelledMap, ...
                                                               likelihood);
                          clusterArray = fastclustermaxprop(labelledMap, ...
                                                            statSumMap);
                         case {'circenergy', 'circnullenergy', 'circnullenergyreg'}
                          clusterArray1  = ...
                              fastsparseclusterprop(labelledMap, likelihood1);
                          clusterArray2  = ...
                              fastsparseclusterprop(labelledMap, likelihood2);
                          if strcmp('circenergy',likelihoodType{jLikelihoodType})
                            clusterArray = max(clusterArray1,clusterArray2);
                          elseif  strcmp('circnullenergy',likelihoodType{jLikelihoodType}) ...
                                || strcmp('circnullenergyreg',likelihoodType{jLikelihoodType})
                            clusterArray = min(clusterArray1,clusterArray2);
                          else
                            error(['Only circenergy, circnullenergy and circnullenergyreg are ' ...
                                   'catered for']);
                          end
                         case 'bandwidth'
                          % bandwith is \sqrt{(\sum_i (f_i-f_mean)^2)/N}
                          clusterArray1 = fastsparseclusterprop(labelledMap, likelihood1);
                          clusterArray2 = fastsparseclusterprop(labelledMap, likelihood2);
                          clusterArray = sqrt((...
                              clusterArray2(:,end) ...
                              -2*clusterArray1(:,end).*loudestCluster{skyPositionNumber,iCircSlide}(:,meanFcol)...
                              +loudestCluster{skyPositionNumber,iCircSlide}(:,nPixelsCol) ...
                              .*loudestCluster{skyPositionNumber,iCircSlide}(:,meanFcol).^2)...
                                              ./loudestCluster{skyPositionNumber,iCircSlide}(:,nPixelsCol));                          
                         case 'duration'
                          % duration is \sqrt{(\sum_i (t_i-t_mean)^2)/N}
                          clusterArray1 = fastsparseclusterprop(labelledMap, likelihood1);
                          clusterArray2 = fastsparseclusterprop(labelledMap, likelihood2);
                          clusterArray = sqrt((...
                              clusterArray2(:,end) ...
                              -2*clusterArray1(:,end).*loudestCluster{skyPositionNumber,iCircSlide}(:,meanTcol)...
                              +loudestCluster{skyPositionNumber,iCircSlide}(:,nPixelsCol) ...
                              .*loudestCluster{skyPositionNumber,iCircSlide}(:,meanTcol).^2)...
                                              ./loudestCluster{skyPositionNumber,iCircSlide}(:,nPixelsCol));                          
                         case 'skypositiontheta'
                          clusterArray = repmat(skyPositions(skyPositionNumber,1),nClusters,1);
                         case 'skypositionphi'
                          clusterArray = repmat(skyPositions(skyPositionNumber,2),nClusters,1);
                         case 'timeshift1'
                          clusterArray = repmat(timeShifts(skyPositionNumber,1),nClusters,1);
                         case 'timeshift2'
                          clusterArray = repmat(timeShifts(skyPositionNumber,2),nClusters,1);
                         case {'loghbayesian','loghbayesiancirc','loghbayesianscalar'}
                          % ---- Marginalise over the various combinations of amplitude (all) and left/right polarisation (circ).
                          clusterArray = fastsparseclusterprop(labelledMap, likelihood(:,1));
                          for icol=2:size(likelihood,2)
                              clusterArrayAll = fastsparseclusterprop(labelledMap, likelihood(:,icol));
                              clusterArray(:,end) = logsumexp(clusterArray(:,end), clusterArrayAll(:,end));
                          end 
                         otherwise
                          clusterArray = fastsparseclusterprop(labelledMap, likelihood);
                        end
                        if strcmp('loghbayesian',likelihoodType{jLikelihoodType}) ... 
                            || strcmp('loghbayesiancirc',likelihoodType{jLikelihoodType}) ...
                            || strcmp('loghbayesianscalar',likelihoodType{jLikelihoodType})
                            % ---- Calculating Prob = P(x) * P(Omega), where 
                            %      likelihood = log(P(x)).
                            %      log(Prob) = log(P(x) * P(Omega)) 
                            %                = log(P(x))  + log(P(Omega))
                            %                = likelihood + log(P(Omega))
                            % ---- If pOmega = 1, logProb = likelihood.
                            clusterArray(:,end) = clusterArray(:,end) + log(pOmega(skyPositionNumber));
                        end
                        % ---- Record only likelihood value from clusterArray.
                        loudestCluster{skyPositionNumber,iCircSlide}(:,likelihoodColOffset+jLikelihoodType) = ...
                            clusterArray(:,end);

                case 'sphericalclusters'

                    % ---- Desired output: ???
                    for l=0:nSph-1
                        sphercialCoef(:,:,l+1,jLikelihoodType)= ...
                            sphercialCoef(:,:,l+1,jLikelihoodType)+ ...
                            conj(Ym0(1,2,l+1,skyPositionNumber))*likelihood(:,:);
                    end

                case 'expintclusters'

                    % ---- Desired output: ???
                    likelihoodTimeFrequencyMap(:,:,jLikelihoodType)= ...
                        likelihoodTimeFrequencyMap(:,:,jLikelihoodType) + ...
                        exp(0.5*likelihood(:,:)) * sin(skyPositions(skyPositionNumber,1));

                case 'squareintclusters'

                    % ---- Desired output: ???
                    likelihoodTimeFrequencyMap(:,:,jLikelihoodType)= ...
                        likelihoodTimeFrequencyMap(:,:,jLikelihoodType) + ...
                        (likelihood(:,:)).^2 * sin(skyPositions(skyPositionNumber,1));

                case {0, 'timefrequencymap','seedless'} 

                % loghbayesian likelihoods are arrays with rows for each amplitude prior
            % need to marginalize over priors before reshaping into a time-frequency array
                if strcmp('loghbayesian',likelihoodType{jLikelihoodType}) ... 
                        || strcmp('loghbayesiancirc',likelihoodType{jLikelihoodType}) ...
                        || strcmp('loghbayesianscalar',likelihoodType{jLikelihoodType})
                % ---- Marginalize over the various combinations of amplitude (all) and left/right polarisation (circ).
                likelihoodArray = likelihood(:,1);
                for icol=2:size(likelihood,2)
                    likelihoodArray = logsumexp(likelihoodArray,likelihood(:,icol));
                end	
            else

                likelihoodArray = likelihood;
            end

                    % ---- Reshape from vector back to time-frequency array
                    likelihood = reshape(likelihoodArray,size(pixMask));

                    if (strncmp(likelihoodType{jLikelihoodType},'bayesian',8))
                        % ---- If the likelihood type is any kind of Bayesian,
                        %      marginalize instead of maximizing.  This is a bit
                        %      tricky, since we are supposed to compute sum_sky[
                        %      exp[ sum_f [likelihood] ] ].  
                        %      KLUDGE: Do sum over frequency and record result in
                        %      first frequency bin.  Zero out rest of bins.
                        overflowPreventer = max(max(sum(likelihood,1)), ...
                            max(likelihoodTimeFrequencyMap(1,:,jLikelihoodType)));
                        likelihoodTimeFrequencyMap(1,:,jLikelihoodType) = log( ...
                            exp(likelihoodTimeFrequencyMap(1,:,jLikelihoodType) - overflowPreventer) ...
                            + exp(sum(likelihood,1) - overflowPreventer) ...
                            ) + overflowPreventer;

                        % we are marginalizing over direction, but we still want to
                        % keep track of the most plausible sky direction

                        index = find(sum(likelihood,1)>maxSummedLikelihood(1,:,jLikelihoodType));
                        maxSummedLikelihood(1,index,jLikelihoodType) = sum(likelihood(:,index),1);
                        skyPositionIndex(1,index,jLikelihoodType) = skyPositionNumber;

                    else % not bayesian
                        % ---- Desired output: time-frequency maps (default).
                        %
                        % ---- Keep all pixels in any time bin for which the likelihood
                        %      summed over that time bin is larger than the current largest
                        %      summed likelihood recorded for that time bin (or smaller for 
                        %      the null energy).
                        % ---- Time bins to keep.
                        if (strcmp(likelihoodType{jLikelihoodType},'nullenergy'))
                            % --- Treat null energy differently from others, since we want
                            %     to minimize it rather than maximize it.
                            if (skyPositionNumber==1)
                                % ---- Keep all null energies, since they are the best yet
                                %      measured.  Need special treatment for the first sky
                                %      position because maxSummedLikelihood is initialied
                                %      to zero.
                                index = 1:size(likelihood,2);
                            else
                                % ---- Keep LOW null energies instead of high.
                                index = find(sum(likelihood,1)<maxSummedLikelihood(1,:,jLikelihoodType));
                            end
                        else
                            index = find(sum(likelihood,1)>maxSummedLikelihood(1,:,jLikelihoodType));
                        end
                        % ---- Record those largest summed likelihoods.
                        maxSummedLikelihood(1,index,jLikelihoodType) = sum(likelihood(:,index),1);
                        % ---- Record the corresponding sky positions.
                        skyPositionIndex(1,index,jLikelihoodType) = skyPositionNumber;
                        % ---- Copy those time bins into the output TF map.
                        likelihoodTimeFrequencyMap(:,index,jLikelihoodType) = likelihood(:,index);
                    end

            end  %-- switch outputType

        end  % -- end loop over likelihood types

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %                    cluster decimation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if not(strcmp(outputType,'timefrequencymap') || strcmp(outputType,'seedless'))

          % ---- Fraction of clusters to keep.
          minClusterRate = decimateRate; % --- should ensure that we don't decimate
                                         % the injection trigger
                                         % the proportion below doesn't affect the decimation when min
                                         % and max rate are equal
          clusterProportion = 0.1;

          % convert rates into number of clusters
          switch outputType
           case {'clusters','glitch'}
            windowDuration = (blockLength-2*transientLength)/sampleFrequency;
           case 'injectionclusters'
            windowDuration = 16;
           otherwise
            error(['Unrecognized outputType: ' outputType])
          end
          lowerClusterNb = round(minClusterRate*windowDuration);

          % apply decimation 
          loudestCluster{skyPositionNumber,iCircSlide} = xdecimatecluster(...
            loudestCluster{skyPositionNumber,iCircSlide}, clusterProportion,...
            lowerClusterNb, lowerClusterNb, likelihoodType, detectionStat);

        end

        if strcmp(outputType,'seedless')

           index = find(strcmpi(likelihoodType,detectionStat));
           thislikelihoodMap = squeeze(likelihoodTimeFrequencyMap(:,:,index));
           analysisLength = integrationLength;
           analysisTime = analysisLength/sampleFrequency;
           [max_cluster,t,f] = seedless(thislikelihoodMap,analysisTime,sampleFrequency,...
               frequencyBand,'',seedlessParams);
           thislikelihoodMap1 = thislikelihoodMap;
           tvals = sum(max_cluster.reconMax);
           tindexes = find(tvals);
           fvals = sum(max_cluster.reconMax');
           [junk,maxindex] = max(fvals);

           tmin = t(tindexes(1));
           tmid = t(tindexes(round(length(tindexes)/2)));
           tmax = t(tindexes(end));
           fmin = f(min(find(fvals>0)));
           fmax = f(max(find(fvals>0)));
           fmid = f(maxindex(1));
           numpixels = max_cluster.nPix;

           likelihoodMapHold = likelihoodTimeFrequencyMap;
           likelihoodMap = {};
           vec = [];
           vec(1,1) = tmin; vec(1,2) = tmid; vec(1,3) = tmax;
           if isfield(max_cluster,'var1_max')
              vec(1,4) = fmid; vec(1,5) = max_cluster.var1_max; vec(1,6) = max_cluster.var2_max;
           else
              vec(1,4) = fmin; vec(1,5) = fmid; vec(1,6) = fmax;
           end
           vec(1,7) = numpixels;

           for i = 1:length(likelihoodType)
              thislikelihoodMap = squeeze(likelihoodMapHold(:,:,i));
              if isfield(max_cluster,'rmapH')
                 snr = sum(sum(thislikelihoodMap.*max_cluster.rmapH));
              else
                 snr = (1/sqrt(numpixels))*sum(sum(thislikelihoodMap.*max_cluster.reconMax));
              end
              vec(1,i+7) = snr;
           end
           loudestCluster{skyPositionNumber,iCircSlide} = vec;
        end

        % ---- Do super-clustering as you go to keep memory usage under
        %      control
        if mod(skyPositionNumber,100) == 0
            switch outputType
             case {'clusters','injectionclusters','glitch'}     
              loudestCluster{1,iCircSlide} = xsupercluster({loudestCluster{:,iCircSlide}},...
                                                           likelihoodType,detectionStat);
              for iCell = 2:skyPositionNumber
                  loudestCluster{iCell,iCircSlide} = [];
              end
            end
        end
        clear psi_earth_max psi_earth_min
        
    end % -- end loop over internal circular time slides

    
end  % -- end loop over sky positions

% ---- Replace time-frequency likelihood maps with alternate output if
%      alternate output is requested.
switch outputType
    case {1, 'followup', 'sparseclusters','seedless'}
        % ---- Output loudest cluster at each sky position.
        likelihoodTimeFrequencyMap{1} = loudestCluster;
    case {'clusters','injectionclusters','glitch'}     
        if verboseFlag
            disp('Super cluster across sky positions')
        end
        likelihoodTimeFrequencyMap = [{''}];
        for iCircSlide = 1:size(circTimeSlides,1)
            likelihoodTimeFrequencyMap(iCircSlide) = {xsupercluster({loudestCluster{:,iCircSlide}},likelihoodType,detectionStat)};
        end
    case 'sphericalclusters'
        likelihoodTimeFrequencyMap{1} = squeeze(sqrt(sum(sphercialCoef.^2,3)))/numberOfSkyPositions;
    case 'expintclusters'
        likelihoodTimeFrequencyMap{1} = log(likelihoodTimeFrequencyMap/numberOfSkyPositions/4/pi);
    case 'squareintclusters'
        likelihoodTimeFrequencyMap{1} = sqrt(likelihoodTimeFrequencyMap/numberOfSkyPositions/4/pi);
    otherwise
        % ---- Enforce cell array output.
        temp = likelihoodTimeFrequencyMap;
        clear likelihoodTimeFrequencyMap
        likelihoodTimeFrequencyMap{1} = temp;
        clear temp;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ---- Return to calling function
return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               helper functions                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ------------------------------------------------------------------------------

function c = logsumexp(a, b)
% LOGSUMEXP - compute log(exp(a) + exp(b)) for matrices a and b while guarding 
% against overfow if exp(a) and or exp(b) are very large. 
%
% Written by Antony Searle.
% Updated by Michal Was for 10% speed improvement

d = max(a, b);
d(d == -Inf) = 0;

% c = log(exp(a - d) + exp(b - d)) + d;
c = log(1 + exp(a + b - d - d)) + d;


% ------------------------------------------------------------------------------

function shiftedPixIdx = shiftpixel(pixIdx, shift, tfMapLength)
% SHIFTPIXEL - compute the new linear indices of TF map pixels after they are 
% time-shifted.

% ---- Default shift (ignoring map edges).
shiftedPixIdx = pixIdx  + shift;

% ---- Wrap pixels that go beyond the left or right edge of the TF map.
shiftedPixIdx = shiftedPixIdx + ...
    tfMapLength*((shiftedPixIdx<=0) - (shiftedPixIdx>tfMapLength));


% ------------------------------------------------------------------------------

function [sectionStartIdx, sectionEndIdx, gateMask] = tfmapsections(allGates, ...
    sampleFrequency,numberOfFrequencyBins,numberOfTimeBins,mapDim,nTimePixelShiftedVect)
% TFMAPSECTIONS - return time indices of TF map segmented according to an input gate list.
%
% use:
%  [sectionStartIdx, sectionEndIdx, gateMask] = tfmapsections(allGates, ...
%      sampleFrequency,numberOfFrequencyBins,numberOfTimeBins,mapDim,nTimePixelShiftedVect)
%
%  allGates           Cell array with one element per detector. Each element is 
%                     a two-column array (which may be empty). Columns are 
%                     index of the start of the first sample in the gate and 
%                     sample index of the last sample in the gate.
%  sampleFrequency    Scalar. Sample rate [Hz] of the timeseries.
%  numberOfFrequencyBins  Scalar. Number of frequency bins in the time-frequency map.
%  numberOfTimeBins   Scalar. Number of time bins in the time-frequency map.
%  mapDim             Vector. Elements are: 
%                       1. centre time of first time bin [s] with respect to the
%                          start of the timeseries 
%                       2. offset between consecutive time bins [s]
%                       3. centre frequency first freq bin [Hz]
%                       4. frequency spacing of consecutive freq bins [Hz].
%  nTimePixelShiftedVect Vector of circular time slides [bins] to be applied. One
%                     element per detector.
%
%  sectionStartIdx    Vector. List of start indices of each section.
%  sectionEndIdx      Vector. List of end indices of each section.
%  gateMask           Array (size: numberOfFrequencyBins x numberOfTimeBins).
%                     Each element is 0 (1) if the corresponding time bin is
%                     (is not) in a gate.
%
% author: patrick.sutton@ligo.org

% ---- Assign defaults.
if nargin<6
    % ---- Default: no time slides.
    nTimePixelShiftedVect = zeros(1,length(allGates));
end

% % ---- KLUDGE: verbosity for debugging.
% disp(['map dimensions: ' num2str(mapDim)])

% ---- Convert gates from indices of timeseries to time bins in the map and
%      apply circular time slides. We retain only gates that lie entirely or 
%      partially in the map, because the circular shifts applied to the map 
%      ignore any data in the transient portions so we don't want gates in the 
%      transient portions being shifted into the map.
for iDet = 1:length(allGates)

    % ---- Check whether this detector has any gates.
    if ~isempty(allGates{iDet})
       
        % % ---- KLUDGE: verbosity for debugging.
        % disp(['--------------------------------------------'])
        % disp(['Detector ' num2str(iDet) ': gates activated.'])
	% disp(['sample start/end: ' num2str(allGates{iDet})])
	% %allGates{iDet}

        % ---- First convert gates from samples to times (from beginning of the
        %      timeseries) and then from time to nearest bin centre index.  
        %      Gate format is one element per detector, which may be empty.
        %      Input columns: start index since beginning of timeseries and stop index.
        %      Output columns: gate bin start index, gate bin end index.
        %      The "-1" is because sampleindex=1 corresponds to t=0.
        %      The "+1"s are needed because matlab counts from 1 rather than 0.
        T0 = mapDim(1);
        dT = mapDim(2);
        shiftedGatesBin{iDet}(:,1) = round(((allGates{iDet}(:,1)-1)/sampleFrequency-T0)/dT)+1;
        shiftedGatesBin{iDet}(:,2) = round( (allGates{iDet}(:,2)   /sampleFrequency-T0)/dT)+1;

        % % ---- KLUDGE: verbosity for debugging.
	% disp(['bin start/end: ' num2str(shiftedGatesBin{iDet})])

        % ---- Now check for gates that fall partially before or after the map.
        %      Trim these to retain only the portion of the gate inside the map.
        %      (We do this check first because it cannot reduce the number of 
        %      gates to zero, so we don't have to check later for empty gate lists.)
        trimIdx = find(shiftedGatesBin{iDet}(:,1)<1);
        if ~isempty(trimIdx)
            shiftedGatesBin{iDet}(trimIdx,:) = 1;
        end
        trimIdx = find(shiftedGatesBin{iDet}(:,2)>numberOfTimeBins);
        if ~isempty(trimIdx)
            shiftedGatesBin{iDet}(trimIdx,2) = numberOfTimeBins;
        end
        % ---- Now delete any gates that fall entirely before or after the map.
        %      A gate long enough to encompass the entire map also does not 
        %      divide the map into sections and can be deleted.
        %      (This may reduce the number of gates to zero.)
        deleteIdx = find( shiftedGatesBin{iDet}(:,1)>numberOfTimeBins | ...
                          shiftedGatesBin{iDet}(:,2)<1 | ...
                         (shiftedGatesBin{iDet}(:,1)<=1 & shiftedGatesBin{iDet}(:,2)>=numberOfTimeBins)); 
        if ~isempty(deleteIdx)
            shiftedGatesBin{iDet}(deleteIdx,:) = [];
        end
        
        % % ---- KLUDGE: verbosity for debugging.
	% disp(['trimmed bin start/end: ' num2str(shiftedGatesBin{iDet})])

        % ---- Apply circular time shifts.
        shiftedGatesBin{iDet} = shiftpixel(shiftedGatesBin{iDet}*numberOfFrequencyBins, ...
                                           nTimePixelShiftedVect(iDet)*numberOfFrequencyBins, ...
                                           numberOfTimeBins*numberOfFrequencyBins);
        shiftedGatesBin{iDet} = shiftedGatesBin{iDet}/numberOfFrequencyBins; 
        % ---- Finally check for gates that have been shifted to overlap a map
        %      boundary. These will have start idx > end idx. Split these into 
        %      separate gates at the beginning and end of the map.
        splitIdx = find(shiftedGatesBin{iDet}(:,1)>shiftedGatesBin{iDet}(:,2));
        if ~isempty(splitIdx)
            % ---- Copy of the gates, truncated at the end of the map.
            newGates = shiftedGatesBin{iDet}(splitIdx,:);
            newGates(:,2) = numberOfTimeBins; 
            % ---- Truncate the original gates at the start of the map.
            shiftedGatesBin{iDet}(splitIdx,1) = 1;
            % ---- Add the new gates to the list.
            shiftedGatesBin{iDet} = [shiftedGatesBin{iDet} ; newGates];
        end

        % % ---- KLUDGE: verbosity for debugging.
	% disp(['after circ shifts bin start/end: ']) % num2str(shiftedGatesBin{iDet})])
	% shiftedGatesBin{iDet}

    else
        
        % ---- Need to initialise shiftedGatesBin.
        shiftedGatesBin{iDet} = [];
        
    end
    
end

% ---- Now concatenate all gates into a single list.
gateArray = integerintervalunion(shiftedGatesBin);
% ---- Count the number of gates.
numGates = size(gateArray,1);

% ---- By default we process the entire TF map as a single section, with no
%      masking of pixels.
sectionStartIdx(1) = 1;
sectionEndIdx(1)   = numberOfTimeBins;
gateMask           = ones(numberOfFrequencyBins, numberOfTimeBins);

% ---- Construct a list of map sections (intervals of the time index). 
if numGates > 0

    % ---- Extract lists of gate start and end bins.
    gateStartIdx = gateArray(:,1);
    gateEndIdx   = gateArray(:,2);

    % ---- Zero out these time intervals in gateMask.
    for iGate=1:numGates
        gateMask(:,gateStartIdx(iGate):gateEndIdx(iGate)) = 0;
    end

    % ---- Divide time bins 1:numberOfTimeBins into sections according to
    %      the gates, over-writing the default single section defined above.
    %      Note that gates from xdetection.m should be non-overlapping by construction. 
    if gateStartIdx(1) > 1
        % ---- The first section is the time interval before the first gate.
        sectionStartIdx(1) = 1;
        sectionEndIdx(1)   = gateStartIdx(1)-1;
    else
        % ---- The first bin is in a gate, so there is no section before a gate.
        sectionStartIdx(1) = [];
        sectionEndIdx(1)   = [];
    end
    for iGate = 1:numGates
        % ---- Add a section representing this gate.
        sectionStartIdx(end+1)   = gateStartIdx(iGate);
        sectionEndIdx(end+1)     = gateEndIdx(iGate);
        if iGate<numGates
            % ---- Add a section for the interval between this gate and the next.
            sectionStartIdx(end+1)   = gateEndIdx(iGate)+1;
            sectionEndIdx(end+1)     = gateStartIdx(iGate+1)-1;
        else
            % ---- For the last gate, only add a following section if the
            %      gate stops before the end of the list of time bins.
            if gateEndIdx(iGate) < numberOfTimeBins
                sectionStartIdx(end+1)   = gateEndIdx(iGate)+1;
                sectionEndIdx(end+1)     = numberOfTimeBins;
            end                                    
        end
    end

end  %-- if numGates > 0

