function [injectionGPS_s, injectionGPS_ns, injectionPhi, injectionTheta, ...
          injectionPsi, injectionType, injectionParam, injectionCatalog] = ...
          getmdcinjectionparams(startTime, blockTime, mdcLogFileName)
% GETMDCINJECTIONPARAMS - Get signal parameters from a GravEn MDC log file
%
% WARNING: This function does not work!  Try getsnmdcinjectionparams.m.
%
%  [injectionGPS_s, injectionGPS_ns, injectionPhi, injectionTheta, ...
%      injectionPsi, injectionType, injectionParam, injectionCatalog] ...
%       = getmdcinjectionparams(startTime, blockTime, mdcLogName)
%
%  startTime    Scalar.  Start time of the data being analysed.
%  blockTime    Scalar.  Duration (s) of the data being analysed.
%  mdcLogName   String.  Name of log file specifying simulated signals to 
%               be injected.
%
%  [injectionGPS_s, injectionGPS_ns, injectionPhi, injectionTheta, ...
%      injectionPsi, injectionType, injectionParam]
%               Injection parameters; see XMAKEGWBINJECTIONFILE.
%  injectionCatalog  
%               Cell arry of strings.  Name of waveform catalog file used 
%               by XMAKEWAVEFORM to generate the injections.  Empty if
%               catalogs not used.
% 
% Only injections in the interval [startTime,startTime+blockTime] are
% returned.
%
% $Id$

warning('This function does not work!  Try getsnmdcinjectionparams.m.');

% ---- Check number of arguments
if (nargin~=3) 
    error('Wrong number of input arguments.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Read injection params from mdc log file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Use rdburstmdclogfile function from matapps/src/simulation/BurstMDC
[numMdc,mdcField,mdcData] = rdburstmdclog(mdcLogFileName);

injectionHrss    = mdcData.SimHrss;
injectionOrient  = acos(mdcData.Internal_x);
injectionTheta   = acos(mdcData.External_x);
injectionPhi     = mdcData.External_phi;
injectionPsi     = mdcData.External_psi;
injectionID      = mdcData.GravEn_SimID;  %-- names of files containing waveforms
injectionSimName = mdcData.SimName;  %-- eg: Dimmelmeier_signal_s20a3o05_ls_STRAIN,
                                     %       M0p2L60R15f1000t100_hanntaper_plus
% ---- For sine-Gaussian waveforms (only) use distance as well.
injectionDist    = mdcData.SimHrss;

% ---- Pre-allocate storage for other outputs.
injectionType    = cell(numMdc,1);
injectionParam   = cell(numMdc,1);
injectionCatalog = cell(numMdc,1);

% ---- Determine how signal arrival time is stored in log file, and
%      translate to X-Pipeline convention as needed.
if isfield(mdcData,'SimStartGPS')
   % ---- Add 0.5s onto SimStartGPS to get peak time of injection.
   injectionGPS   = mdcData.SimStartGPS + 0.5;
elseif isfield(mdcData,'EarthCtrGPS')
   % ---- No need to add 0.5s into EarthCtrGPS.
   injectionGPS   = mdcData.EarthCtrGPS;
else
   error('Neither SimStartGPS or EarthCtrGPS in mdc log file')
end

% ---- Loop over all injections in MDC log file, and determine details for
%      each.
for idx = 1:numMdc

    switch injectionID{idx}
        
        case 'Waveforms/M0p2L60R15f1000t1000.dat_hanntaper-plus.txt;Waveforms/M0p2L60R15f1000t1000.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'M0p2L60R15f1000t1000.dat_hanntaper-plus'];

        case 'Waveforms/M0p2L60R15f1000t100_hanntaper_plus.txt;Waveforms/M0p2L60R15f1000t100_hanntaper_cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'M0p2L60R15f1000t100_hanntaper_plus'];

        case 'Waveforms/M0p2L60R15f400t100.dat_hanntaper-plus.txt;Waveforms/M0p2L60R15f400t100.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'M0p2L60R15f400t100.dat_hanntaper-plus'];

        case 'Waveforms/M1p5L60R15f1000t1000.dat_hanntaper-plus.txt;Waveforms/M1p5L60R15f1000t1000.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'M1p5L60R15f1000t1000.dat_hanntaper-plus'];

        case 'Waveforms/M1p5L60R15f1000t100_hanntaper_plus.txt;Waveforms/M1p5L60R15f1000t100_hanntaper_plus.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'M1p5L60R15f1000t100_hanntaper_plus'];

        case 'Waveforms/M1p5L60R15f400t100.dat_hanntaper-plus.txt;Waveforms/M1p5L60R15f400t100.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'M1p5L60R15f400t100.dat_hanntaper-plus'];
            
        case 'Waveforms/piroM10eta0.3fac0.2.dat_hanntaper-plus.txt;Waveforms/piroM10eta0.3fac0.2.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'piroM10eta0.3fac0.2.dat_hanntaper-plus'];

        case 'Waveforms/piroM10eta0.6fac0.2.dat_hanntaper-plus.txt;Waveforms/piroM10eta0.6fac0.2.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'piroM10eta0.6fac0.2.dat_hanntaper-plus'];

        case 'Waveforms/piroM5eta0.3fac0.2.dat_hanntaper-plus.txt;Waveforms/piroM5eta0.3fac0.2.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'piroM5eta0.3fac0.2.dat_hanntaper-plus'];

        case 'Waveforms/piroM5eta0.6fac0.2.dat_hanntaper-plus.txt;Waveforms/piroM5eta0.6fac0.2.dat_hanntaper-cross.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'piroM5eta0.6fac0.2.dat_hanntaper-plus'];
            
        case 'Waveforms/processed_s15.0.h-plus.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'processed_s15.0.h-plus'];

        case 'Waveforms/processed_s15-time-rhplus_matter-plus.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'processed_s15-time-rhplus_matter-plus'];

        case 'Waveforms/processed_signal_s15a2o05_ls-plus.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'processed_signal_s15a2o05_ls-plus'];

        case 'Waveforms/processed_signal_s15a2o09_ls-plus.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'processed_signal_s15a2o09_ls-plus'];

        case 'Waveforms/processed_signal_s15a3o15_ls-plus.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'processed_signal_s15a3o15_ls-plus'];
            
        case 'Waveforms/SG1304Q8d9.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'SG1304Q8d9-lin'];

        case 'Waveforms/SG235Q8d9.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'SG235Q8d9-lin'];

        case 'Waveforms/SG1304Q8d9.txt;Waveforms/CG1304Q8d9.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'SG1304Q8d9-ell'];

        case 'Waveforms/SG235Q8d9.txt;Waveforms/CG235Q8d9.txt'
            injectionCatalog{idx} = 'opticalsnsearch.mat';
            injectionType{idx}    = 'opticalsnsearch';
            injectionParam{idx}   = [num2str(injectionHrss{idx}) '~' 'SG235Q8d9-ell'];
            
    end
    
%     warning('THE FOLLOWING CODE IS COMPLETELY NON_FUNCTIONAL!');
%
%     % ---- The portion of injectionSimName up to the first '_' indicates
%     %      the waveform type.  The interpretation of the rest of the string
%     %      depends on this first value.  We translate from MDC to X format
%     %      using a look-up table based on this portion of injectionSimName.
%     chopIdx = strfind(injectionSimName{idx},'_');
%     % ---- Get waveform name so that to know which catalog name to use.
%     if ~isempty(chopIdx)
%         injectionSimNameStart = injectionSimName{idx}(1:chopIdx-1);
%     else 
%         % ---- If there is no underscore keep the whole SimName.
%         injectionSimNameStart = injectionSimName{idx};
%     end
% 
%     % ---- Translate SimName into waveform type and corresponding catalog
%     %      (if any).
%     switch injectionSimNameStart
%         case 'SG1304Q8d9'
%             warning('Cannot tell what inclination angle is ... !?');
%             injectionType{idx} = 'chirplet';
%             % ---- For non-optimal inclination last argument should be pi-iota.            
%             injectionParam{idx} = [num2str(injectionDist(idx)) '~1/1304~1304~0~-pi/2~pi'];            
%         case 'SG235Q8d9'
%             warning('Cannot tell what inclination angle is ... !?');
%             injectionType{idx} = 'chirplet';
%             % ---- For non-optimal inclination last argument should be pi-iota.            
%             injectionParam{idx} = [num2str(injectionDist(idx)) '~1/235~235~0~-pi/2~pi'];                    
%         case 'piroM5eta0.3fac0.2.dat'
%             injectionType{idx} = 'piro';
%             % injectionParam{idx} = [];
%             injectionParam{idx} = 'piroM5eta0.3fac0.2.dat';
%             injectionCatalog{idx} = 'Pirocat';
%         case 'piroM5eta0.6fac0.2.dat'
%             injectionType{idx} = 'piro';
%             % injectionParam{idx} = [];
%             injectionParam{idx} = 'piroM5eta0.6fac0.2.dat';
%             injectionCatalog{idx} = 'Pirocat';
%         case 'piroM10eta0.3fac0.2.dat'
%             injectionType{idx} = 'piro';
%             % injectionParam{idx} = [];
%             injectionParam{idx} = 'piroM10eta0.3fac0.2.dat';
%             injectionCatalog{idx} = 'Pirocat';
%         case 'piroM10eta0.6fac0.2.dat'
%             injectionType{idx} = 'piro';
%             % injectionParam{idx} = [];
%             injectionParam{idx} = 'piroM10eta0.6fac0.2.dat';
%             injectionCatalog{idx} = 'Pirocat';
%         case 'Dimmelmeier'  
%             injectionCatalog{idx} = 'DOMJ08';
%             injectionType{idx}    = 'DOMJ08';
%             injectionCatalog{idx} = 'DOMJ08';
%         case 'Murphy')  
%         injectionCatalog{idx} = 'Murphy2009';
%         case 'M0p2L60R15f1000t100')  
%         injectionCatalog{idx} = 'longbar';
%         case 'M1p5L60R15f1000t100')  
%         injectionCatalog{idx} = 'longbar';
%     elseif strcmp(injectionSimNameStart(1:2), 'SG')  
%         injectionCatalog{idx} = 'SGEL';
%         %injectionCatalog{idx} = 'SG';
%     else 
%         error(['Injection SimName ' injectionSimNameStart ' not recognized.  Cannot translate.'])
%     end	
%        
%     % ---- Get waveform type.
%     if strcmp(injectionSimNameStart, 'M0p2L60R15f1000t100')
%         warning('Not sure which is right set of parameters -- please check this code.');
%         % ---- This?
%         % injectionParam{idx} = '0.2~60e5~15e5~1000~0.1~0~0';
%         % ---- Or this?
%         distance = '1'; %-- dummy values for the moment.
%         theta    = '0';
%         injectionParam{idx} = [distance '~0.2~6e6~1.5e6~1000~0.025~0~' theta];
%         [dummy,hp,hc] = xmakewaveform(injectionCatalog{idx},injectionParam{idx},1,0.5,16384);
%         [dummy,hrss] = xoptimalsnr([hp,hc],0,16384);
%         distance = num2str(hrss / injectionHrss(idx));
%         theta = num2str(injectionOrient(idx));
%         injectionParam{idx} = [distance '~0.2~6e6~1.5e6~1000~0.025~0~' theta];
%         case 'M1p5L60R15f1000t100')
%         case 'M1p5L60R15f1000t100')
%         % inject optimal at default distance
%         injectionParam{idx} = '1.5~60e5~15e5~1000~0.1~0~0';
%         injectionParam{idx} = 'M1p5L60R15f1000t100';
% 
%         else    		
%             injectionParam{idx} = injectionSimName{idx}(chopIdx+1:length(injectionSimName{idx}));
%             % remove '_STRAIN' suffix (7 trailing characters)
%             injectionParam{idx} = injectionParam{idx}(1:(length(injectionParam{idx})-7));
%     end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Keep only injections between startTime and startTime + blockTime
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

keepInjection = []; %-- row number of injections to keep after checking GPS time
for idx = 1:numMdc  %-- loop over all inj in mdclogfile
    if ( (injectionGPS(idx) >= startTime) & (injectionGPS(idx) <= startTime+blockTime) )
        keepInjection = [keepInjection; idx];
    end   
end

injectionGPS        = injectionGPS(keepInjection);
injectionTheta      = injectionTheta(keepInjection); 
injectionPhi        = injectionPhi(keepInjection);
injectionPsi        = injectionPsi(keepInjection);
injectionType       = injectionType(keepInjection);
injectionParam      = injectionParam(keepInjection);
%
injectionCatalog    = injectionCatalog(keepInjection);

% ---- split GPS time into s and ns part
% split into sec and nanosec part
injectionGPS_s   = floor(injectionGPS);
injectionGPS_ns  = ((injectionGPS - floor(injectionGPS)) .* 1e9);

% ---- Done.
return
