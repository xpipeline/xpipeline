function [boundingBox, centralTime, centralFrequency, nPixels, likelihood] ...
    = xclusterarraytoproperties(clusterArray);
%
% xclusterarraytoproperties - Convert a single 2-D array of event clusters 
% into separate variables.
%
% usage:
%
% [boundingBox, centralTime, centralFrequency, nPixels, likelihood] ...
%     = xclusterarraytoproperties(clusterArray);
%
% clusterArray      Array of event clusters as produced by clusterTFmapNew.
%
% boundingBox       Array with columns
%                     column 1: minimum time of cluster
%                     column 2: minimum frequency of cluster
%                     column 3: duration of cluster
%                     column 4: bandwidth of cluster
% centralTime       Vector of weighted central time of clusters.
% centralFrequency  Vector of weighted central frequency of clusters.
% nPixels           Vector of number of time-frequency pixels in each cluster.
% likelihood        Array of likelihood values for each cluster.
%
% See clusterTFmapNew.
%
% $Id$

% ---- Hardcoded parameters reflecting ouput from clusterTFmapNew.
% ---- Number of columns in output from clusterTFmapNew.
minTcol = 1; meanTcol = 2; maxTcol = 3;
minFcol = 4; meanFcol = 5; maxFcol = 6;
areaCol = 7;
% ---- These first N columns are not likelihoods.
likelihoodColOffset = 7; 

% ---- Split clusterArray data into separate variables.
if (~isempty(clusterArray) )
    boundingBox = zeros(size(clusterArray,1),4);
    boundingBox(:,1) = clusterArray(:,minTcol);
    boundingBox(:,2) = clusterArray(:,minFcol);
    boundingBox(:,3) = clusterArray(:,maxTcol) - clusterArray(:,minTcol);
    boundingBox(:,4) = clusterArray(:,maxFcol) - clusterArray(:,minFcol);
    % ---- Central time.
    centralTime = clusterArray(:,meanTcol);
    % ---- Central frequency.
    centralFrequency = clusterArray(:,meanFcol);
    % ---- Number of time-frequency pixels.
    nPixels = clusterArray(:,areaCol);
    % ---- Likelihoods.
    likelihood = clusterArray(:,likelihoodColOffset+1:end);
else 
    boundingBox = [];
    centralTime = [];
    centralFrequency = [];
    nPixels = [];
    likelihood = [];
end % -- clusterArray non-empty

% ---- Done.
return


