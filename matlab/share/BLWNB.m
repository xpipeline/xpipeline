function x = blwnb(f,df,dt,fs)
% BLWNB - Generate a random signal of given duration with constant power
% in a given band (and zero power out of band).
%
%   x = blwnb(f,df,dt,fs)
%
%   f   Scalar. Minimum signal frequency [Hz].
%   df  Scalar. Full signal bandwidth [Hz].
%   dt  Scalar. Signal duration [s].
%   fs  Scalar. Signal sample rate [Hz].
%
% Power is restricted to the band (f,f+df).  
% Note that fs must be greater than 2*(f+df).
%
% original version: L. S. Finn, 2004.08.03
%
% $Id$

% ---- Check that fs > 2*(f+df), otherwise sampling rate is not high enough to 
%      cover requested frequency range of the signal.
if (fs <= abs(2*(f+df)))
  error(...
    'Sampling rate fs is too small. ',...
    'fs = %f must be greater than 2*(f+df) = %f',... 
    fs, abs(2*(f+df)));
end
if (f < 0 | df <= 0 | fs <= 0 | dt <= 0)
  error('All arguments must be greater than zero');
end

% ---- Generate white noise with duration dt at sample rate df. This will be
%      white over the band [-df/2,df/2].
nSamp = ceil(dt*df);
x = randn([1,nSamp]);

% ---- Resample to desired sample rate fs.
[p,q] = rat(fs/df);
% ---- The resample function doesn't work if p*q>2^31. Check this condition before proceeding. 
tol = 1e-6; %-- default tolerance of resample()
while p*q>2^31
    tol = tol*10;
    [p,q] = rat(fs/df,tol);
end
x = resample(x,p,q);

% ---- Note that the rat() function returns p,q values that give the desired 
%      ratio to a default accuracy of 1e-6. This is a big enough error that 
%      x may be a few samples too short or too long. If too long, then truncate 
%      to duration dt. If too short, zero-pad to duration dt.
nSamp = round(dt*fs);
if length(x) > nSamp
    x = x(1:nSamp);
elseif length(x) < nSamp
    x(nSamp) = 0;
end

% ---- Heterodyne up by f+df/2 (moves zero frequency to center of desired band).
fup = f+df/2;
x = x.*exp(-2*pi*i*fup/fs*(1:numel(x)));

% ---- Take real part and adjust amplitude.
x = real(x)/sqrt(2);

% ---- Done.
return

