function [gdata, gates, diagnostic] = autogate(data,transLength,filterType,filterLength,sigmaThreshold,windowType,windowLength,vetoLength,vetoThreshold)
% AUTOGATE - create gates based on whitened data and apply to the data 
%
% usage:
%
%   [gdata, gates] = autogate(data,transLength,filterType,filterLength, ...
%                sigmaThresh,windowType,windowLength,vetoLength,vetoThresh)
%
% data          Array of whitened data, one column per detector.
% transLength   Non-negative integer. Number samples at each end of data 
%               that should be ignored due to, e.g., filter transients.
% filterType    String. Type of window used for the moving average energy
%               calculation. Must be one of the window types recognised by
%               matlab, currently: 'bartlett', 'barthannwin', 'blackman',
%               'blackmanharris', 'bohmanwin', 'chebwin', 'gausswin',
%               'hamming', 'hann', 'kaiser', 'nuttallwin', 'parzenwin',
%               'rectwin', 'taylorwin', 'triang', 'tukeywin'.   
% filterLength  Positive integer. Length of the moving average filter in
%               samples.
% sigmaThresh   Positve scalar. Threshold on normalised energy to create
%               a gate.
% windowType    String. Type of window used for the gate; recognised values
%               are the same as for filterType.
% windowLength  Whole even number. Length of the gate window in samples.
% vetoLength    Optional whole number. Veto (ignore) a gate if there is a
%               gate in any other detector within +/-vetoLength samples. 
%               Use [] (default) to not veto. 
% vetoThresh    Optional positve scalar. Threshold on normalised energy in
%               any other detector at which to veto a coincident gate in a
%               given detector. Defaults to sigmaThresh.
%
% gdata         Array of gated data. Identical to data except any
%               auto-generated gates have been applied.
% gates         Cell array, with one element per column of gdata. Each
%               element is an array listing any auto-gates created for that
%               channel. Columns are: 
%                 index of first sample of zeroed data
%                 index of last sample of zeroed data
%
% AUTOGATE computes the running average energy E in the data (i.e., summing
% over data.^2) for each column of data using a filter of length and type
% determined by filterLength and filterType. Gates are defined at any
% samples such that (E-medE)/stdE > sigmaThresh , where medE and stdE are
% the median and standard deviation of E computed using all samples
% excluding the first and last transLength, and where stdE is computed from
% the percentiles of the E distribution to avoid skewing by large glitches.
% Intervals of gated samples are zeroed out. The gates are preceeded and
% followed by an inverse window (1-window) defined by windowType and
% windowLength. Half of the window is applied at each end of the gate to
% ensure smooth transitions. Users should avoid windows that don't start
% and end at zero (gausswin, hamming, kaiser, rectwin, taylorwin).
%
% If the optional input vetoLength is nonempty, then overlapping gates in
% different detectors are ignored. Specifically, "veto" gates are generated
% for all detectors using the threshold vetoThresh. Any gate that overlaps
% with a veto gate in any other detector is ignored ("vetoed"). Overlap is
% defined as having any common samples, with a +/-vetoLength coincidence
% window allowance. Gate vetoing is a safety measure to minimise the
% likelihood of erroneously gating very loud gravitational-wave signals. 
%
% Finally, if two gates for the same timeseries lie within windowLength
% samples of each other, they are combined into a single gate.
%
% If an error is encountered in trying to construct the filter or gating
% windows (e.g. an unrecognised filterType or windowType) then the function
% defaults to using filterType = 'rectwin' or windowType = 'tukeywin'.
%
% $Id$


% ---- Non-functional code. Included for developers to see easily the
%      various window shapes available.
if false
    % ---- Test code to view the available window types:
    filterType = {'bartlett', 'barthannwin', 'blackman', 'blackmanharris', 'bohmanwin', ...
        'chebwin', 'gausswin', 'hamming', 'hann', 'kaiser', 'nuttallwin', 'parzenwin', ...
        'rectwin', 'taylorwin', 'triang', 'tukeywin'}
    for ii=1:length(filterType)
        fh = str2func(filterType{ii});
        figure; plot(fh(100)); title(filterType{ii});
    end
end


% ---- Checks on inputs.
narginchk(7,9);
% ---- Assign defaults if needed.
if nargin<9
    vetoThreshold = sigmaThreshold;
end
% ---- Assign defaults if needed.
if nargin<8
    vetoLength = [];
end
% ---- data should be a numeric array with channels columnwise.
if ~isnumeric(data) || size(data,1) < size(data,2)
    error('Input data must be a numeric array with each column a timeseries.');
end
if length(transLength) > 1 || transLength < 0 || transLength~=round(transLength)
    error('Input transLength must be a non-negative integer scalar.');
end
if ~ischar(filterType)
    error('Input filterType must be a string.');
end
if length(filterLength) > 1 || filterLength < 1 || filterLength~=round(filterLength)
    error('Input filterLength must be a positive integer scalar.');
end
if length(sigmaThreshold) > 1 || sigmaThreshold < 0
    error('Input sigmaThreshold must be a positive scalar.');
end
if ~ischar(windowType)
    error('Input windowType must be a string.');
end
if length(windowLength) > 1 || windowLength < 2 || (windowLength/2)~=round(windowLength/2)
    error('Input filterLength must be a positive even scalar.');
end

% ---- Issue a warning if windowType does not start and end at zero.
badTypes = {'gausswin', 'hamming', 'kaiser', 'rectwin', 'taylorwin'};
if any(strcmp(windowType,badTypes))
    warning(['Requested windowType ' windowType ...
        ' does not start and end at zero. This is not recommended.']);
end


% ---- Construct filter: running weighted average of energy.
try
    fh = str2func(filterType);
    wind = fh(filterLength);
catch
    % ---- Default to rectangular window if the requested filterType is not
    %      recognised.
    warning(['Unable to make a window of type ' filterType '. Using rectwin.'])
    wind = rectwin(filterLength);
end    
b = ones(1,filterLength) / sum(wind); %-- force to have mean of 1.
a = 1;


% ---- Apply filter.
Nsamp  = size(data,1);
Ndet   = size(data,2);
energy = zeros(size(data));
for ii = 1:Ndet 
    % ---- Compute running average energy.
    energy(:,ii) = filter(b,a,data(:,ii).^2);
    % ---- Compute median standard deviation, ignoring filter transient
    %      sections.
    idx = [(transLength+1):(Nsamp-transLength)]';
    sigmaEnergy(ii)  = mean(diff(prctile(energy(idx,ii),[16 50 84])));
    % ---- Rescaled energy: remove mean and renormalise by standard deviation, per channel.
    normEnergy(:,ii) = (energy(:,ii)-median(energy(idx,ii)))/sigmaEnergy(ii);
end

if nargout>2
    for ii = 1:Ndet 
        [diagnostic.normEnergyMax(1,ii),diagnostic.normEnergyMaxIdx(1,ii)] = max(normEnergy(:,ii));
        diagnostic.normEnergyMaxOther(1,ii) = 0;
        minInd = max(diagnostic.normEnergyMaxIdx(1,ii) - filterLength,0);
        maxInd = min(diagnostic.normEnergyMaxIdx(1,ii) + filterLength,size(normEnergy,1));
        for jj = 1:Ndet
            if jj~=ii
                tmp = max(normEnergy(minInd:maxInd,jj));
                diagnostic.normEnergyMaxOther(1,ii) = max(diagnostic.normEnergyMaxOther(1,ii),tmp);
            end
        end
    end
end


% ---- Find excursions above sigmaThreshold.
%      Demonstration of logic of index manipulation:
%         idx = [9:13 21:22 27 31:35]'
%         idx =     9    10    11    12    13    21    22    27    31    32    33    34    35
%         diffIdx = diff(idx)
%         diffIdx =     1     1     1     1     8     1     5     4     1     1     1     1
%         breakIdx = find(diffIdx>1)
%         breakIdx =     5     7     8
%         A = [0; breakIdx]
%         A =     0     5     7     8
%         B = ones(size(A))
%         B =     1     1     1     1
%         C = A+B
%         C =     1     6     8     9
%         A(1) = []; A(end+1) = length(idx)
%         A =     5     7     8    13
%         [C A]
%         [C A] =
%              1     5
%              6     7
%              8     8
%              9    13
%         gates = idx([C A])
%         gates =
%              9    13
%             21    22
%             27    27
%             31    35
for ii = 1:Ndet 
    % ---- Find gates.
    idx = find(normEnergy(:,ii) > sigmaThreshold);
    if ~isempty(idx)
        diffIdx  = diff(idx);
        breakIdx = find(diffIdx>1);
        A = [0; breakIdx];
        B = ones(size(A));
        C = A+B;
        A(1) = [];
        A(end+1) = length(idx);
        idxStart = idx(C);
        idxStop  = idx(A);
        % ---- Correct stop index for the filterLength. 
        %      Don't correct for gates that start within filterLength of the end
        %      of the data series otherwise the gate start would be earlier than
        %      the gate end. In practice these won't matter as this region will
        %      be discarded from later analysis provided filterLength <
        %      translength (default 16 sec for aLIGO/aVirgo searches).  
        ind = (idxStart + filterLength - 1) <= Nsamp;
        idxStop(ind)  = idxStop(ind) - (filterLength - 1);
        gates{ii} = [idxStart idxStop];
    else
        gates{ii} = [];
    end
    % ---- Find "veto" gates.
    idx = find(normEnergy(:,ii) > vetoThreshold);
    if ~isempty(idx)
        diffIdx  = diff(idx);
        breakIdx = find(diffIdx>1);
        A = [0; breakIdx];
        B = ones(size(A));
        C = A+B;
        A(1) = [];
        A(end+1) = length(idx);
        idxStart = idx(C);
        idxStop  = idx(A);
        % ---- Correct stop index for the filterLength.
        ind = (idxStart + filterLength - 1) <= Nsamp;
        idxStop(ind)  = idxStop(ind) - (filterLength - 1);
        vetoGates{ii} = [idxStart idxStop];
    else
        vetoGates{ii} = [];
    end
end


% ---- Optionally veto gates that are coincident between detectors.
if ~isempty(vetoLength)
    vetoIdx = cell(1,Ndet);
    % ---- Find all pairs of coincident gates.
    for ii = 1:Ndet
        for jj = 1:Ndet 
            if ii~=jj && ~isempty(gates{ii}) && ~isempty(vetoGates{jj})
                X  = gates{ii}(:,1);
                dX = gates{ii}(:,2) - gates{ii}(:,1) + vetoLength;
                Y  = vetoGates{jj}(:,1);
                dY = vetoGates{jj}(:,2) - vetoGates{jj}(:,1) + vetoLength;
                C = Coincidence2(X,dX,Y,dY);
                if ~isempty(C)
                    vetoIdx{ii} = union(vetoIdx{ii},C(:,5));
                end
            end
        end 
    end
    % ---- Delete all coincident gates.
    for ii = 1:Ndet
        if ~isempty(vetoIdx{ii})
            gates{ii}(vetoIdx{ii},:) = [];
        end
    end
end


% ---- Find any gates that start within windowLength of a previous gate in 
%      the same detector and combine them, since they will overlap anyway.
for ii = 1:Ndet 
    if ~isempty(gates{ii})
        idxStart = gates{ii}(:,1);
        idxStop  = gates{ii}(:,2);
        for jj=length(idxStart):-1:2
            if idxStart(jj)-idxStop(jj-1) < windowLength
                idxStop(jj-1) = idxStop(jj);
                idxStart(jj) = [];
                idxStop(jj)  = [];
            end
        end
        gates{ii} = [idxStart idxStop];
    end
end


% ---- Construct window.
try
    fh = str2func(windowType);
    wind = fh(windowLength);
catch
    % ---- Default to rectangular window if the requested filterType is not
    %      recognised.
    warning(['Unable to make a window of type ' windowType '. Using tukeywin.'])
    wind = tukeywin(windowLength);
end    
wind = wind / max(abs(wind)); %-- force to have peak value of 1.


% ---- Apply gates. Note that all gates are non-overlapping since we have
%      combined any that overlap, above.
gdata = data;
for ii = 1:Ndet 
    if ~isempty(gates{ii})
        mask = ones(Nsamp,1);
        idxStartAll = gates{ii}(:,1);
        idxStopAll  = gates{ii}(:,2);
        for jj=1:length(idxStartAll)
            idxStart = idxStartAll(jj);
            idxStop  = idxStopAll(jj);
            % ---- Zero out central portion of the gate, where the energy
            %      is above threshold.
            mask(idxStart:idxStop) = 0;
            % ---- Smoothly transition gate from 1 to 0 at the start.
            rampIdx = (idxStart-windowLength/2):(idxStart-1);
            rampIdx(rampIdx<1) = []; %-- in case ramp extends beyond start of timeseries
            rampLength = length(rampIdx);
            mask(rampIdx) = 1 - wind((windowLength/2-rampLength+1):windowLength/2);
            % ---- Smoothly transition gate from 0 to 1 at the end.
            rampIdx = (idxStop+1):(idxStop+windowLength/2);
            rampIdx(rampIdx>Nsamp) = []; %-- in case ramp extends beyond end of timeseries
            rampLength = length(rampIdx);
            mask(rampIdx) = 1 - wind((windowLength/2+1):(windowLength/2+rampLength));
        end
        % ---- Apply combined gates.
        gdata(:,ii) = gdata(:,ii) .* mask;
    end
end


return
