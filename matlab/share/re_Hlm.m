function reH = re_Hlm(m,Idd,factor)
% RE_HLM: Real part of expansion param for spin-2 weighted spherical harmonics.
%
% use:
%
%   reH = re_Hlm(m,Idd,factor)
%
% m       Scalar. Spherical harmonic "m" value.  Must be -2, -1, 0, 2, or 2.
% Idd     3x3 cell array.  Each element holds the timeseries of the second
%         derivative of the corresponding element of the quadrupole moment. 
% factor  Optional scalar.  Overall multiplicative factor to be applied to
%         Idd. 
%
% Returns the real parts of the coefficients, when decomposing hp - ihc in
% spin-2 spherical harmonics. The calculation is performed only for l=2
% harmonics.  See LONGBAR.

% ---- Check input arguments.
error(nargchk(2, 3, nargin));
if nargin<2
    factor = 1;
end
if size(Idd) ~= [3,3]
    error('Input Idd must be a 3x3 cell array.');
end

% ---- Compute output.
if m < -2 || m > 2
	error('m must be one of -2, -1, 0, 1, 2.');
elseif m == 0
	reH = factor * sqrt(32*pi/15) * (Idd{3,3} - 0.5 * (Idd{1,1} + Idd{2,2}));
elseif m == 1
	reH =  - factor * sqrt(16*pi/5) * Idd{1,3};
elseif m == 2
	reH = factor * sqrt(4*pi/5) * (Idd{1,1} - Idd{2,2});
elseif m == -1
	reH =  factor * sqrt(16*pi/5) * Idd{1,3};
elseif m == -2
	reH = factor * sqrt(4*pi/5) * (Idd{1,1} - Idd{2,2});
else
	error('something went very wrong, check the function'); 
end

% -- Return to calling function   
return
