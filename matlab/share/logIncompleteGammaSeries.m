function Y = logIncompleteGammaSeries(X,A)
%
% logIncompleteGammaSeries - Series expansion approximation for the
% logarithm of the incomplete Gamma function, for X >> A.  In MatLab
% notation,
%
%   Y  = logIncompleteGammaSeries(X,A)
%
%   X   Array.  1/2 * quantity that is chi-squared distributed.
%   A   Array.  1/2 * degrees of freedom.
%   
%   Y   Significance of X values assuming chi-squared statistics.  The
%       significance is defined log(1-Pchi2cdf), where Pchi2cdf is the
%       cumulative distribution function (see chi2cdf).  
%
% This function uses a series expansion for X >> A to compute the
% significance.  This expansion is more accurate than MATLAB's built-in
% chi2cdf for large signficances. In MATLAB notation,
%
%   Y  = logIncompleteGammaSeries(X,A)
%     := log(gamma(A) * (1 - gammainc(X,A))) 
%
% In the mathematical notation used by Abramowitz and Stegun,
%
%   Y := log(Gamma(A,X)/Gamma(A))
%
% The series expansion used for the approximation is
%
%   Y ~ - X + (A-1)*log(X) - gammaln(A) + log( 1 ...
%           + (A-1)./(X) + ...
%           + (A-1)*(A-2)./X.^2 ...
%           + (A-1)*(A-2)*(A-3)./X.^3 + ...
%           + (A-1)*(A-2)*(A-3)*(A-4)./X.^4 ...
%           + (A-1)*(A-2)*(A-3)*(A-4)*(A-5)./X.^5 ...
%           + (A-1)*(A-2)*(A-3)*(A-4)*(A-5)*(A-6)./X.^6 ...
%           );
%
% This approximation is taken from Abramowitz and Stegun, equation 6.5.32.
%
% See also gamma, gammainc, gammaln.
%
% Patrick J. Sutton, 2006.08.28

% ---- Check for valid argument sizes.
%      Make sure input arguments are the same size.
if (isempty(A) | isempty(X))
    error('Empty input argument.')
end
if (max(size(A))>1 & max(size(X))>1)
    if (size(A)~=size(X))
        error('Inputs arguments A and X must have the same size if both are not scalars.');
    end
elseif (max(size(A))>1)
    X = X*ones(size(A));
elseif (max(size(X))>1)
    A = A*ones(size(X));
end

% ---- Compute series expansion.
Y = - X + (A-1).*log(X) - gammaln(A) + log( 1 ...
        + (A-1)./(X) + ...
        + (A-1).*(A-2)./X.^2 ...
        + (A-1).*(A-2).*(A-3)./X.^3 + ...
        + (A-1).*(A-2).*(A-3).*(A-4)./X.^4 ...
        + (A-1).*(A-2).*(A-3).*(A-4).*(A-5)./X.^5 ...
        + (A-1).*(A-2).*(A-3).*(A-4).*(A-5).*(A-6)./X.^6 ...
        );

% ---- Done
return

      