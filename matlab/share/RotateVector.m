function Z = RotateVector(X,Y,psi)
% ROTATEVECTOR - Rotate vectors about an arbitrary axis.
%
%   Z = RotateVector(X,Y,psi)
%
% Rotate vector X around the vector Y by angle psi [rad].  All vectors must be
% 3-vectors in Cartesian coordinates; Y must be non-null.  X may be a 3xN
% matrix, in which case each column is treated as a separate vector.  The
% columns of the matrix Z are the rotated vectors.
%
% If length(psi) = 1 then all vectors X are rotated by the same angle.  
% If length(psi) = size(X,2) then each X is rotated by the corresponding psi.
%
% Patrick J. Sutton (patrick.sutton@astro.cf.ac.uk)

% ---- Check that X,Y are 3-vectors and convert X, Y to column vectors
%      if necessary.
if (size(X)==[1,3]) 
    X = X';
end
if (size(Y)==[1,3]) 
    Y = Y';
end
% ---- Force psi to row vector.
psi = psi(:).';

if (isequal(size(X,1),3)*isequal(size(Y),[3,1])==0) 
    error('Input vectors must be 3-vectors.')
end

% ---- Check that Y is non-null.
if (sum(Y'*Y)==0) 
    error('Input vector Y (axis of rotation) must be non-null.')
end

% ---- This script takes the vector X (Cartesian coordinates) and rotates
%      it by an angle psi about the vector Y.  It returns the rotated
%      vector Z. Note: the axis must be specified by a unit vector,
%      therefore normalize Y.
Y = Y/norm(Y);

% ---- Rotate.  (Checked matrix is correct for general angle of rotation 
%      psi of general initial vector X about general unit vector Y).
if length(psi)==1
    S = [ 0 -Y(3) Y(2); Y(3) 0 -Y(1); -Y(2) Y(1) 0];
    R = [1 0 0; 0 1 0; 0 0 1] + sin(psi)*S + (1-cos(psi))*S^2;
    Z = R*X;
else
    Z = zeros(size(X));
    S = [ 0 -Y(3) Y(2); Y(3) 0 -Y(1); -Y(2) Y(1) 0];
    S2 = S^2;
    % ---- Do calculation of x, y, z components separately.
    for ii = 1:3
       R = S(ii,:)'*sin(psi) + S2(ii,:)'*(1-cos(psi));
       R(ii,:) = R(ii,:) + 1;
       Z(ii,:) = sum(R.*X,1);
    end
end

% ---- Done.
return

