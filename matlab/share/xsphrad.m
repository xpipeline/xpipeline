function xsphrad(conditionedData, amplitudeSpectra, blockStartTime, blockStopTime, channelNames, parameters, pointingMap, timeShift) 
% XSPHRAD - Perform spherical radiometer time-frequency burst analysis. 
%
% usage: 
%
%  xsphrad(conditionedData, amplitudeSpectra, blockStartTime, blockStopTime, ...
%          channelNames, parameters, pointingMap, timeShift) 
%
%  conditionedData      Nx3 array. Each column holds the conditioned data stream
%                       (time series) for one detector.
%  amplitudeSpectra     Array. Each column is the noise amplitude spectrum for 
%                       one detector.
%  blockStartTime 	Scalar. Start time of analysis [s].
%  blockStopTime	Scalar. End time of analysis [s].
%  channelNames 	Cell array. Elements are the names of the h(t) channel 
%                       for each detector.
%  parameter            Struct as returned by xparameters.m. Required fields:
%                           blockDuration
%                           clusterThresholds
%                           eOrder
%                           FFTLen
%                           frequencyRange
%                           outputDirectory
%                           regionDirection
%                           sampleFrequency
%                           sphradCode
%                           sphradDebug
%                           sphradDistance
%                           sphradFollowUps
%                           sphradRegions
%                           sphradSkyPrior
%                           sphradTestLocalisation
%                           skyPosition
%                           transientFactor
%                           whiteningDuration
%  pointingMap          Cell array of [gps_time, theta, phi].
%                       Used to determine skypos to use for trigger time.
%                       Need to 'convert' gps_time into 'time since block start'.
%                       See the description of "sky_pointing.txt" at 
%                       https://gravity.astro.cf.ac.uk/dokuwiki/cardiff/mark/installing
%                       Use {} if you wish not to use this argument.
%  timeShift		Optional. Vector of circular time shiftsa to be applied to the second 
%                       detector. The third detector will be shifted by the negative
%                       of these values, while the first detector will not be 
%                       circularly shifted.
%
% All outputs are to files.  These are as follows:
% 	cross-power for each detector pair (In SH power)
%	total cross-power (in SH power)
%	total likelihood (cross+response)
%	tfmap size (frequency, bandwidth, time, duration)
%	output info for cluster 
%	likelihood/SH power skymaps (for SH centre time)
%	.m holding ts grid and tf grid (detector pairs etc)

fprintf(1,'Doing cross-correlation with the spherical radiometer\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            Preparatory                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Get detectors. 
for detIdx = 1:length(channelNames)
    allDet{detIdx} = channelNames{detIdx}(1:2);
end

% ---- The sphrad code uses simplified names for many of the parameters in the 
%      parameters structure.  We manually copy the required variables into a 
%      new struct with the expected names.
analysisStruct.deltaT 		= 1/parameters.sampleFrequency;
analysisStruct.blockStartTime 	= blockStartTime;
analysisStruct.blockStopTime 	= blockStopTime;
analysisStruct.blockLen         = parameters.blockDuration;
analysisStruct.transientLen 	= round(parameters.sampleFrequency*parameters.whiteningDuration*parameters.transientFactor);
analysisStruct.freqRange	= parameters.frequencyRange;
analysisStruct.FFTlen 		= parameters.FFTLen; 
analysisStruct.clusterThresholds= parameters.clusterThresholds;
analysisStruct.debugLevel	= parameters.sphradDebug;
analysisStruct.nRegions		= parameters.sphradRegions;	
analysisStruct.codeVersion	= parameters.sphradCode;	
analysisStruct.pixelDistance	= parameters.sphradDistance;	
analysisStruct.regionDirection	= parameters.regionDirection;
analysisStruct.sampleFrequency  = parameters.sampleFrequency;
if ~(isempty(parameters.skyPosition))
    analysisStruct.skyPos	= parameters.skyPosition;
end
if isfield(parameters,'sphradBlurring')
    analysisStruct.sphradBlurring = parameters.sphradBlurring;
end
% ---- Used to pass information about sky pointing:
%        -1 - Sky prior exists
%         0 - None
%        >1 - Number of 'regions' in our time-skyPos map
analysisStruct.skyPrior 	= parameters.sphradSkyPrior;
analysisStruct.eOrder		= parameters.eOrder;
analysisStruct.followUps  	= parameters.sphradFollowUps;
% ---- Are we testing how the pointing statistics perform?
analysisStruct.testLocalisation = parameters.sphradTestLocalisation;

% ---- Generate output directory location.  This is based on default omega
%      pipeline behaviour - ghastly.
% outputDirectory	= ['./testchangedpath/' num2str(blockStartTime) '-' num2str(blockStartTime+analysisStruct.blockLen) ];
% disp(outputDirectory)
analysisStruct.outputDirectory 	= parameters.outputDirectory;

% ---- Generate segment information. 
%      From local variables (was parsing the path before)
analysisStruct.segmentStartTime = blockStartTime;
analysisStruct.segmentDuration  = analysisStruct.blockLen;

% ---- Check if we have a sky pointing map to use.
%      It has the following format:
%        793154960 0.14 0.44
%        793154967 1.14 0.88
%        793155031 2.14 1.32
%        793155095 3.14 1.76
%      So, we just need to subtract off the blockStartTime, and shift according
%      to the transient length.
if ((analysisStruct.skyPrior >= 1) || (analysisStruct.testLocalisation == 1))

    nReg = 0;
    timeMap=[];

    fprintf(1,'Sky pointing map:\n');
    for inj=1:length(pointingMap)
        % ---- Split into component parts, convert from strings to an array.
        splitMap = regexp(pointingMap{inj}, ' ','split')
        splitMap{1} = str2num(splitMap{1}); 
        splitMap{2} = str2num(splitMap{2}); 
        splitMap{3} = str2num(splitMap{3});
        splitMap = cell2mat(splitMap);
        fprintf(1, 'gps: %d ',splitMap(1)); 

        % ---- Normalise to this block (include transient length).
        splitMap(1) = splitMap(1) - blockStartTime - analysisStruct.whiteningDuration*parameters.transientFactor;

        fprintf(1, '(relative %d) -> (%g, %g)\n',splitMap(1),splitMap(2),splitMap(3)); 

        if (splitMap(1) < 0)
            fprintf (1,'Rejected, outside our window\n');
            timeMap = [0 splitMap(2:end) ];
            nReg = 1;
        elseif (splitMap(1) > analysisStruct.blockLen - analysisStruct.whiteningDuration*parameters.transientFactor)
            fprintf(1, 'Rejected, too high\n');
        else
            if (nReg == 0)
                timeMap = [0 splitMap(2:end) ];
            else
                timeMap = [timeMap splitMap];
            end
            nReg = nReg + 1;
        end
    end

    analysisStruct.skyPos2 = timeMap;
    if (analysisStruct.skyPrior >=1)
        analysisStruct.skyPrior = nReg;
    else
        analysisStruct.testLocalisation = nReg;
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Prepare the data                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for idx=1:length(channelNames)
    % ---- Remove the "transient length" samples corrupted by the conditioning
    %      from each end of each data timeseries.
    data{idx} = conditionedData((analysisStruct.transientLen:(end-analysisStruct.transientLen)),idx)';
    % ---- sphrad code expects power spectrum information to be passed in as 
    %      inverse of noise amplitude.
    coeffs{idx} = 1./amplitudeSpectra(:,idx)';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     That's enough messing around, now let's analyse the data!                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(1,'Starting likelihood calculation\n');

% ---- Carry out full analysis. Data products are dumped directly to files 
%      so there is no returned variable.
if exist('timeShift','var')
    sphrad(length(channelNames), data, analysisStruct, allDet , 0, 'full', coeffs, timeShift(:)');
else
    sphrad(length(channelNames), data, analysisStruct, allDet , 0, 'full', coeffs);
end

% Creating a movie fileName
% Loop over the array, SHinv each time slice,
% then F(j)=getframe to save image
% then use movie(F) to play it.
% Need to use consistent colormap so that it doesn't fluctuate.
% caxis([min max]) for each image

disp('Finished block'); 

return
end

% -----------------------------------------------------------------------------

function skyPos(skyMap, response)

% ---- Get SH data
max_l = round(sqrt(size(skyMap,1)));
sky= SHinv(skyMap,max_l);

[theta, phi] = find(max(sky(:)) == sky);

disp('Here')

imagesc(abs(sky));
colorbar;
hold on;
% ---- Image plots the matrix the wrong way around 
scatter(phi,theta,100,'k');
xlabel('phi');ylabel('theta');
hold off

%theta = [0:1:pi];
%phi = [0:1:2*pi];

thetaRes = size(sky,1);
phiRes = size(sky,2);

fprintf(1, 'Sky position of maxima: {%f, %f}\n',(phi/thetaRes)*2*pi, pi*(theta/phiRes));
fprintf(1, 'Sky position of maxima: {%f, %f}\n',(phi/phiRes)*2*pi, pi*(theta/thetaRes));
fprintf(1, 'Sky position of maxima: {%f, %f}\n',pi-(phi/thetaRes)*pi, 2*pi-(theta/phiRes)*pi*2);
fprintf(1, 'Sky position of maxima: {%f, %f}\n',pi-(phi/phiRes)*pi, 2*pi-(theta/thetaRes)*pi*2);

end

% -----------------------------------------------------------------------------

function [theta, phiRes]=g2p(x,y,res)

theta = pi-(y/res)*pi;
phi =  2*pi-((x/res)*pi*2);

fprintf(1,'Sky pos: {%f,%f}\n',theta, phi);
fprintf(1,'Or: {%f,%f}\n',pi-(x/res)*pi, 2*pi-(y/res)*2*pi);

end

% -----------------------------------------------------------------------------

function trigger = determine_parameters(SHgrid, transients, analysisStruct, blockStartTime)

% ---- Get power in spherical harmonics
for i=1:size(SHgrid,2)
  SHpower(i) = sum(norm(SHgrid(:,i)));
end

for i=1:2:size(SHgrid,2)
  seqSHpower(round(i/2)) = sum(norm(SHgrid(:,i)));
end

% ---- Convert section index into a time
sectionStart = transients/analysisStruct.sampleFrequency;
sectionLen = analysisStruct.duration-sectionStart*2;
sectionMax = find(max(SHpower) == SHpower);
sectionTimeStep = sectionLen/(length(SHpower)+1);

% ---- Add the trigger time to the transient and block durations
%      This should give the accurate trigger time.
trigger.time = blockStartTime+ sectionStart+sectionMax*sectionTimeStep;

% ---- Use full duration at half maximum for trigger length
%      In sections
duration = length(find(seqSHpower > (max(seqSHpower)/2)));
%      In seconds
trigger.duration = duration*(sectionLen/(length(seqSHpower)));

% ---- Frequency
max_l = sqrt(size(SHgrid,1));
bWidth = size(SHgrid,1)-max_l;

% ---- Get largest SH coefficient
freq=max(find(abs(SHgrid(max_l:round(end/2),sectionMax)) > abs(max(SHpower)/100)));
% ---- Normalise
freq=freq/(bWidth/2);
% ---- Change to frequency
trigger.frequency=freq*analysisStruct.sampleFrequency;

end

