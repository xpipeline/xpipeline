function [SNR, h_rss, h_peak, Fchar, bw, Tchar, dur, Tchar_w, dur_w, tfbox, h_weighted] = xoptimalsnr(h,t0,fs,S,F0,dF,Fmin,Fmax,tfFrac)
% XOPTIMALSNR - Compute SNR and other properties of a signal in noise.
%
% XOPTIMALSNR - Compute the SNR and time-frequency measures of a waveform
% in a specified noise background.  Simpler noise-independent measures of 
% the wave amplitude are also provided.
%
%   [SNR, h_rss, h_peak, Fchar, bw, Tchar, dur, Tchar_w, dur_w, tfbox, hw] = ...
%       xoptimalsnr(h,t0,fs,S,F0,dF,Fmin,Fmax,tfFrac)
%
%   h      Array.  Waveform timeseries data.  Each column holds the 
%          timeseries for one of the GW polarizations.  (There may be any
%          number of polarisations, for the non-GR case.  The order of 
%          polarizations does not matter.)  The timeseries duration should 
%          be a power of 2.
%   t0     Scalar.  Time at which the first waveform data point h(1,:) is
%          sampled.   
%   fs     Scalar.  Sampling rate (Hz) of waveform data.  This should be a
%          power of 2.
%   S      Vector (optional).  Noise background one-sided POWER (not
%          amplitude) spectrum.  If supplied, it must cover at least the
%          range [Fmin, Fmax], and must be sampled in ascending
%          order of frequency.  For noise-independent signal measures use
%          S=f0=df=[].  In this case the constant spectrum S(f)=2 will be
%          used (corresponding to a two-sided noise spectrum of unity).
%   F0     Scalar or vector (optional).  Scalar: Frequency at which S(1) is
%          sampled.  Vector: frequencies at which S is sampled.
%   dF     Scalar (optional).  Frequency spacing of the noise spectrum S.
%          If F0 is a vector then dF = [] must be used.
%   Fmin   Scalar (optional).  Minimum frequency (Hz) to include in 
%          frequency-domain calculations.  Defaults to 0 if [] or no noise
%          spectrum specified, or minimum frequency of noise spectrum.
%   Fmax   Scalar (optional).  Maximum frequency (Hz) to include in 
%          frequency-domain calculations. Defaults to Nyquist if [] or no noise
%          spectrum specified, or maximum frequency of noise spectrum.
%   tfFrac Optional scalar in interval (0,1). Fraction of noise-weighted signal
%          energy (SNR^2), measured separately in time and frequency domains, to
%          be used in computing tfbox output.
%
% Computations are done in the time domain (TD) and frequency
% domain (FD) using the energy distributions
%      p_TD = h(:,1).^2 + h(:,2).^2;
%      p_FD = 2(|FFT(h(:,1))|.^2 + |FFT(h(:,2))|.^2);  % for f>=0
% With these conventions, the output is
%   SNR    Signal-to-noise ratio of h in the given noise background, 
%          defined as 
%            SNR = (2 \int_Fmin^Fmax df p_FD./S).^0.5
%   h_rss  The root-sum-square amplitude (Hz^-0.5) of the waveform h:
%            h_rss = \int_Fmin^Fmax df p_FD
%   h_peak The maximum instantaneous RMS amplitude of the waveform h over 
%          polarisations:
%            h_peak = max(p_TD).^0.5
%   Fchar  Characteristic frequency (Hz) of h in the given noise  
%          background, defined as 
%                    \int_Fmin^Fmax df f p_FD./S
%            Fchar = ----------------------------------------
%                     \int_Fmin^Fmax df p_FD./S
%          where Fmin = max(f) and \tilde(h) is the FFT of h.  
%   bw     Effective bandwidth (Hz) of h in the given noise  
%          background, defined as 
%                 \int_Fmin^Fmax df (f-Fchar).^2 p_FD./S
%            bw = ---------------------------------------------------
%                  \int_Fmin^Fmax df p_FD./S
%          where Fmin = max(f) and \tilde(h) is the FFT of h.  
%   Tchar  Characteristic time at which the event occurs WITHOUT noise
%          weighting, defined as  
%                    \int dt t p_TD(t)
%            Tchar = -----------------
%                     \int dt p_TD(t)
%   dur    Duration of the signal WITHOUT noise weighting, defined as 
%                    \int dt (t-Tchar).^2 p_TD(t)
%            Tchar = ----------------------------
%                     \int dt p_TD(t)
%   Tchar_w Characteristic time at which the event occurs WITH noise
%          weighting. Defined as for Tchar except calculated using hw.
%   dur_w  Duration of the signal WITH noise weighting. Defined as for bw
%          except calculated using hw. 
%   tfbox  Vector. Bounding box containing 99% of noise-weighted signal
%          energy (SNR^2), measured separately in time and frequency domains.
%          Elements: 
%            tfbox(1) - start time [s]
%            tfbox(2) - start frequency [Hz]
%            tfbox(3) - duration [s]
%            tfbox(4) - bandwidth [s]
%   hw     Vector. Noise-weighted time-domain waveform, defined as 
%            hw = ifft(fft(h)/S^0.5))
%          If S is not specified then this will be the same as h up to 
%          normalisation.
%
% Notes:
%
% The power spectrum S is interpolated (if needed) to the frequencies of
% fft(h).
%
% Note that no windowing is used for computing FFTs; windowing and 
% averaging is not really sensible for a transient signal, since by
% definition the statistical properties of the signal are changing over
% time.  There may be problems for, e.g., band-passed noise bursts.
%
% The SNR and h_rss measures are restricted to the frequency interval 
% [Fmin,Fmax], while h_peak is evaluated in the time domain (i.e., using 
% data from the full frequency range. 
%
% See FFTCONVENTIONS for information on the conventions used for Fourier
% transforms. 
% 
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Checks.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Valid number of input arguments.
error(nargchk(3,9,nargin,'struct'))

% ---- Optional arguments.
if (nargin<9)
    tfFrac = 0.99;
end
% ---- Is there an input noise spectrum?
if ( (nargin<6) || isequal(S,[]) || isequal(F0,[]) )
    % ---- Set flag to make dummy noise spectrum.
    noise = 0;
else
    noise = 1;
    % ---- Validate noise spectrum data.
    if ~isvector(S)
        error('Spectrum S be a vector or empty array.');
    end
    if ~( (isscalar(dF) & dF>0)  | isequal(dF,[]) )
        error(['Spectrum frequency resolution dF must be a positive scalar ' ...
            '(if F0 is a scalar) or empty (if F0 is a vector).']);
    end
    if ~( (isscalar(F0) & F0>=0) | (isvector(F0) & length(S) == length(F0)))
        error(['Spectrum lowest frequency F0 must be a non-negative scalar,'...
               ' or a vector of equal length to S']);
    end
    if isscalar(F0) & isequal(dF,[])
        error('Frequency sampling step not provided and F0 is a scalar')
    end
    if ~isscalar(F0) & ~isequal(dF,[])
      error('Frequency sampling step provided, but F0 is a vector')
    end
    % ---- Vector of sampled noise frequencies.
    if isscalar(F0)
      F = F0+[0:length(S)-1]'*dF;
    else
      if any(diff(F0)<=0)
        error('F0 is not an ascending frequency series')
      end
      F = F0;
      warning('Using hand provided spectrum sampling. Beware, it will be interpolated.')
    end

    % ---- Force column vectors.
    S = S(:);
end
% ---- Frequency range of analysis.
if ( (nargin<8) || isequal(Fmax,[]) )
    if noise
        % ---- Default to highest frequency in supplied spectrum.
        Fmax = F(end);
    else
        % ---- Default to Nyquist.
        Fmax = floor(fs/2);
    end
end
if ( (nargin<7) || isequal(Fmin,[]) )
    if noise
        % ---- Default to lowest frequency in supplied spectrum.
        Fmin = F(1);
    else
        % ---- Default to DC.
        Fmin = 0;
    end
end

% ---- Error checks.
if (~isscalar(Fmax) | Fmax<=0)
    error('Frequency limit Fmax must be a positive scalar.');
end
if (~isscalar(Fmin) | Fmin<0)
    error('Frequency limit Fmin must be a non-negative scalar.');
end
if Fmin>=Fmax
    error('Frequency limits must satisfy Fmin<Fmax.');
end
% ---- Require positive sampling rate.
if fs<=0
    error('Sampling rate fs must be positive.');
end
if ~isscalar(t0)
    error('Timeseries start time t0 must be a scalar.');
end
% % ---- Remove this check to allow for non-GR polarisations.
% % ---- Input timeseries must be one- or two-column array.
% if (size(h,2)>2)
%     error('Input timeseries h must be a one- or two-column array.');
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Preparations.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Number of data points.  Force to be even.
N = size(h,1);
if ~iseven(N)
    warning(['Function not guaranteed for waveforms with odd number ' ...
        'of samples.  Dropping last sample']);
    h(end,:) = [];
    N = size(h,1);    
end

% ---- Duration of timeseries [sec].
T = N/fs;  

% ---- Verify that T is a power of 2.
if T ~= 2^round(log2(T))
    warning(['Function is not guaranteed for timeseries durations that ' ...
        'are not powers of 2.']);
end

% ---- Sample times.
t = t0+1/fs*[0:(N-1)]';

% ---- If no noise spectrum is supplied then make a dummy noise vector 
%      covering [0,Nyquist] Hz.  This will allow us to assume henceforth
%      that S, F, F0, and dF are defined.
if (~noise)
    dF = 1/T;
    F0 = 0;
    S = 2*ones(N/2+1,1);
    F = F0+[0:length(S)-1]'*dF;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Time-domain properties WITHOUT noise weighting.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Distribution of signal energy in time.
p_TD = sum(h.^2,2);

% ---- Peak amplitude.
h_peak = max(p_TD).^0.5;

% ---- Characteristic time.
Tchar = sum(t.*p_TD)./sum(p_TD);

% ---- Duration.  
dur = (sum((t-Tchar).^2.*p_TD)./sum(p_TD))^0.5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Time-domain properties WITH noise weighting.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~noise 

    Tchar_w = Tchar;
    dur_w   = dur;
    h_weighted = h;

else 

    % ---- Apply noise spectrum to create a noise-weighted time-domain waveform.
    hf_orig = 1/fs*fft(h);
    f_orig = [ 0:N/2 , -N/2+1:-1 ]'/T;
    S_orig = S;
    F_orig = F;
    one_over_S = zeros(size(f_orig));
    idx = find(f_orig>=F(1) & f_orig<=F(end));
    one_over_S(idx) = interp1(F,S.^(-1),f_orig(idx));   %-- one-sided
    one_over_S(end:-1:(N/2+1)) = one_over_S(2:(N/2+1)); %-- two-sided
    hf_weighted = hf_orig .* repmat(one_over_S.^0.5,1,size(h,2));
    h_weighted = real(ifft(hf_weighted)); %-- cast off any small imaginary pat from numerical error
    % ---- The normalisation is arbitrary; reset to a convenient number.
    %h_weighted = h_weighted/norm(h_weighted);

    % ---- Distribution of signal energy in time.
    p_TD = sum(h_weighted.^2,2);

    % ---- Characteristic time.
    Tchar_w = sum(t.*p_TD)./sum(p_TD);

    % ---- Duration.  
    dur_w = (sum((t-Tchar_w).^2.*p_TD)./sum(p_TD))^0.5;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Frequency-domain calculations.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Signal.

% ---- Standard FFT; works by column.
hf = 1/fs*fft(h);
% ---- Vector holding frequencies in usual screwy FFT order:
%        vector element:   [ 1  2  ...  N/2-1  N/2    N/2+1            N/2+2   ... N-1  N ]
%        frequency (df):   [ 0  1  ...  N/2-2  N/2-1  (N/2 or -N/2)   -N/2+1  ... -2   -1 ]
f = [ 0:N/2 , -N/2+1:-1 ]'/T;
% ---- Keep only frequency components in [Fmin,Fmax].  Note that most of
%      the frequency-domain formulas include a factor of 2 to account for
%      negative frequencies. 
index = find(f>=Fmin & f<=Fmax);
f = f(index);
hf = hf(index,:);
% ---- Distribution of signal energy in frequency.
p_FD = 2*sum(hf.*conj(hf),2);
% ---- f=0 and f=Nyq bins should count for 1/2.
if (f(1)==0)
    p_FD(1) = 0.5*p_FD(1);
end
if (f(end)==fs/2)
    p_FD(end) = 0.5*p_FD(end);
end

% ---- Noise.

% ---- Does vector of sampled noise frequencies cover requested range?
if ( (F(1)>f(1)) | (F(end)<f(end)) ) 
    error('Noise spectrum does not cover desired frequency range.')
end
% ---- Force interpolation of S from sampled frequencies F to data
%      frequencies f.
S = interp1(F,S,f);
% S = exp(interp1(log(F),log(S),log(f)));  %-- this fails if F(1)=0
F = f;
dF = 1/T;
F0 = F(1);

% ---- All set to do calculations.  Assured that f, F interchangable.

% ---- SNR^2 versus frequency.
SNR2f = 2*p_FD./S;

% ---- SNR on [Fmin,Fmax].
SNR = (dF*sum(SNR2f)).^0.5;

% ---- Characteristic frequency.
Fchar = sum(f.*SNR2f)./sum(SNR2f);

% ---- Characteristic bandwidth.  
bw = (sum((f-Fchar).^2.*SNR2f)./sum(SNR2f))^0.5;

% ---- RSS amplitude.
h_rss = (dF*sum(p_FD)).^0.5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Bounding box
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargout >= 10

    prc_start = (1-tfFrac)/2;
    prc_end   = (1+tfFrac)/2;

    % ---- Time containing 99% of total h_weighted(t)^2.
    % ---- Accumulation of h_weighted(t)^2 over time.
    cht2 = cumsum(p_TD)/sum(p_TD);
    % ---- Select out unique points (necessary for linear interp).
    [Z,I] = unique(cht2);
    cht2 = cht2(I);
    zt = t(I);
    tfbox(1) = interp1(cht2,zt,prc_start);
    tfbox(3) = interp1(cht2,zt,prc_end) - tfbox(1);

    % ---- Frequency interval containing central 99% of total |h(f)|^2/S(f).
    % ---- Accumulation of |h(f)|^2 over frequency.
    chf2 = cumsum(SNR2f)/sum(SNR2f);
    % ---- Select out unique points (necessary for linear interp).
    [Z,I] = unique(chf2);
    chf2 = chf2(I);
    zf = f(I);
    tfbox(2) = interp1(chf2,zf,prc_start);
    tfbox(4) = interp1(chf2,zf,prc_end) - tfbox(2);

end

% ---- Done
return

