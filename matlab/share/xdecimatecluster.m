function [decimatedClusters decimationMask] = ...
    xdecimatecluster(clusterArray,proportion,lowerLimit,upperLimit, ...
                     likelihoodType,detectionStat,verboseFlag)
% xdecimatecluster - decimate clusters to lower the amount of triggers to process
%
% [decimatedClusters decimationMask] = xdecimatecluster(clusterArray,proportion, ...
%                                      lowerLimit,upperLimit,likelihoodType,detectionStat)
%
% clusterArray     Matrix. List of clusters in flat array format which
%                  are to be decimated
% proportion       Scalar. Proportion of clusters that are kept by the
%                  decimation (this proportion is changed if the number
%                  of kept clusters do not respect the lower and upper
%                  limit of clusters, see below)
% lowerLimit       Scalar intenger. Minimal number of kept clusters. If
%                  the input number of clusters is smaller, all clusters
%                  are kept.
% upperLimit       Scalar intenger. Maximal number of kept clusters.
% likelihoodType   Cell Array. Each element is a string giving the name of the
%                  corresponding likelihood in clusterArray.
% detectionStat    String. Name of likelihood to use as the detection statistic (i.e. loudness)
% verboseFlag      Scalar. Optional, default value is 0. Controls the verbosity
%                  of the function, should be set to 0 if not in debug mode.
%
% decimatedClusters  Matrix. List of clusters in flat array format which
%                    are kept by the decimation
% decimationMask     Vector. Logical mask of which clusters are
%                    decimated. Has the same number of elements as the
%                    input number of clusters, value 1 for kept clusters
%                    and value 0 for decimated clusters.

narginchk(6,7)

if nargin == 6
  verboseFlag = 0;
end

if lowerLimit < 1
  error(['The lower limit in decimation should be at least 1. It is ' ...
         num2str(lowerLimit)])
end

if lowerLimit > upperLimit
  error(['The lower limit: ' num2str(lowerLimit) ...
         ' is larger than the upper limit: ' num2str(upperLimit)]);
end

% ---- If number of cluster is already below lower limit, keep everything
%      and skip useless steps to get better computational performance
if lowerLimit >= size(clusterArray,1)
    decimationMask = true(size(clusterArray,1),1);
    decimatedClusters = clusterArray;
    return
end

% ---- extract fields from flat array cluster format
[boundingBox, centralTime, centralFrequency, nPixels, likelihood] ...
    = xclusterarraytoproperties(clusterArray);

% ---- Compute significance of detection statistic for each event in each 
%      list.
% ---- Find the desired detection statistic in the list of likelihoods
iDetStat = find(strcmp(detectionStat,likelihoodType));
detectionSignificance = likelihoodsignificance(likelihood(:,iDetStat), ...
                                          likelihoodType{iDetStat},nPixels);

% ---- Compute and apply threshold on significance to keep
%      only proportion fraction of clusters. 
sortedSig = sort(detectionSignificance);
significanceThreshold = simpleprctile(sortedSig, ...
                                (1-proportion)*100);

% ---- enforce lower/upper limit
if length(sortedSig) > lowerLimit
  significanceThreshold = min(significanceThreshold, ...
                              sortedSig(end-lowerLimit+1));
else
  significanceThreshold = min(significanceThreshold, ...
                              sortedSig(1));
end
if length(sortedSig) > upperLimit
  significanceThreshold = max(significanceThreshold, ...
                              sortedSig(end-upperLimit+1));
end

% ---- apply decimation to clusters

decimationMask = detectionSignificance >= significanceThreshold;
decimatedClusters = clusterArray(decimationMask,:);


% ---- debugging info
if verboseFlag
  thrOld = prctile(detectionSignificance, ...
                   (1-proportion)*100);
  maskOld = detectionSignificance >  thrOld;
  disp(['New kept / Old kept / All: ' num2str(sum(decimationMask)) '/' ...
        num2str(sum(maskOld)) '/' num2str(length(decimationMask))])
end
