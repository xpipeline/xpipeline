function chunkEnds = chunkIndices( nSamp, duration, transient, offset )
%chunkIndices    Calculate the endpoints of the chunks to consider of
%                length duration within a sample of length nSamp, with
%                transient length of transient, with user-specified overlap.
%
%   chunkEnds = chunkIndices( nSamp, duration, transient, offset )
%
% Inputs:
%    nSamp        Scalar number of samples in the series
%    duration     Scalar number of samples per chunk to output
%    transient    Scalar number of samples to ignore at the beginning and
%                 end due to filter transients
%    offset       Optional.  Scalar number of samples between the start of 
%                 consecutive chunks.  Default = ceil(0.5*duration); i.e., 
%                 50% overlap of chunks.
%
% Outputs:
%    chunkEnds    Matrix of indices of chunks to consider.
%                 chunkEnds(i,:) = [ start_i, end_i ]
%
% Revision History
% 11 July 2005    Leo C. Stein    Initial Write
%
% $Id$

% Check for overlap of chunks.
if (nargin<4)
    offset = ceil(0.5*duration);
end

% Error checks.
if (nSamp~=round(nSamp) | nSamp<=0)
    error('Number of samples in the series must be a positive integer.');
end
if (duration~=round(duration) | duration<=0)
    error('Chunk duration must be a positive integer.');
end
if (transient~=round(transient) | transient<0)
    error('Transient length must be a non-negative integer.');
end
if (offset~=round(offset) | offset<=0)
    error('Offset must be a positive integer.');
end

% Equally spaced points, offset apart, starting after the transient
% response.
iStart = [transient+1:offset:nSamp-transient-duration+1];

% The -1 is because Matlab convention is inclusive
iStop = iStart + duration - 1;

% Pack up results.
chunkEnds = [ iStart' iStop' ];

% Done.
return
