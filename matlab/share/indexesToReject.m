function [reject1, reject2]=indexesToReject(significance1, significance2, interesectMap)
significance1=significance1(:)';
significance2=significance2(:)';
dim1coord = repmat((1:size(interesectMap,1))',1,size(interesectMap,2));
dim2coord = repmat(1:size(interesectMap,2),size(interesectMap,1),1);

sigMap1 = repmat((significance1)',1,size(interesectMap,2));
sigMap2 = repmat( significance2,size(interesectMap,1),1);

sigLesser1 = sigMap1 <= sigMap2;
sigLesser2 = sigMap1 > sigMap2;

reject1 = unique(dim1coord(interesectMap & sigLesser1));
reject2 = unique(dim2coord(interesectMap & sigLesser2 )); 