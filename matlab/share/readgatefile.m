function [allGatesAutoFormat, allGatesXCondFormat] = readgatefile(gatingFileName,startTime,stopTime,sampleFrequency,timeOffsets)
% READGATEFILE - read gate information from a file
%
% usage:
% 
%  [autoFormat, xcondFormat] = readgatefile(gatingFileName,startTime,stopTime,sampleFrequency,timeOffsets)
%
%  gatingFileName    String. Name of "master" gating file.
%  startTime         Scalar. Start of timeseries [sec]. 
%  stopTime          Scalar. End of timeseries [sec]. 
%  sampleFrequency   Scalar. Sample rate of timeseries [Hz].
%  timeOffsets       Optional vector. Time offsets [sec] to be applied for each 
%                    detector. Default: 0 for each detector.
%
%  autoFormat        Cell array; see below.
%  xcondFormat       Cell array; see below.
%
% READGATEFILE reads gate information from a set of files. The "master" gating
% file contains a list of files, one per detector, that list the gates for the
% corresponding detector. The format of each of these per-detector files is a
% numeric array with columns
%   1. gate center time [sec]
%   2. duration of central "zero" portion of window [seconds]
%   3. total duration of the two taper portions on either side of the 
%      window [seconds]
% An empty file is treated as no gates for that detector (allGates{i}=[]).
%
% For each detector i only gates with centre times in the interval 
% [startTime+timeOffsets(i), stopTime+timeOffsets(i)] are returned. The gates are
% returned in two formats:
%
%  autoFormat: format used by AUTOGATES.M:
%   1. index of first sample to be gated
%   2. index of last sample to be gated
%
%  xcondFormat: format used by XCONDITION.M:
%   1. gate center time [samples]
%   2. duration of central "zero" portion of window [seconds]
%   3. total duration of the two taper portions on either side of the 
%      window [seconds]
%
% $Id$

% ---- Checks on inputs.
narginchk(4,5);

% ---- Open "master" gating file.
fileID = fopen(gatingFileName);
gatingPaths = textscan(fileID,'%s','delimiter','\n');
gatingPaths = gatingPaths{1};
fclose(fileID);

% ---- Assign defaults if needed.
if nargin<5
    timeOffsets = zeros(1,length(gatingPaths));
end

% ---- Initialise storage for output. Empty elements mean no gates for that
%      detector.
allGatesXCondFormat = cell(length(gatingPaths),1);
allGatesAutoFormat  = cell(length(gatingPaths),1);

% ---- Loop over detectors. Read gating file for each and extract
%      gates within the current analysis time interval. 
for iCh = 1:length(gatingPaths)
    if ~isempty(gatingPaths{iCh})
        channelGates = load(gatingPaths{iCh})
        timeOffset = timeOffsets(iCh);
        if channelGates
            % ---- Extract all gates with center time in the analysis segment.
            %      The gate file format is one row per gate, with columns:
            %        1. center time [GPS seconds]
            %        2. duration of central "zero" portion of window [seconds]
            %        3. total duration of the two taper portions on either side 
            %           of the window [seconds]
            segChGates = channelGates(channelGates(:,1)>(startTime+timeOffset) ...
                                    & channelGates(:,1)<(stopTime+timeOffset),:);
            if ~isempty(segChGates)
                % ---- Convert relative gate centre time to time relative to 
                %      start of timeseries, and undo time offsets for gates.
                segChGates(:,1) = segChGates(:,1) - timeOffset - startTime;
                %
                % ---- Convert format of gate data to that used by xcondition.m.
                %      This is the same as the gate file except the first column
                %      is center time in [samples]. The "+1" is because t=0
                %      corresponds to index=1.
                segChGates(:,1) = segChGates(:,1) * sampleFrequency + 1;
                allGatesXCondFormat{iCh} = segChGates;
                %
                % ---- Convert format of gate data to that used by autogate.m 
                %      to allow for easy combining with autogates. Autogate
                %      format is two columns:
                %        1. index of first sample to be gated
                %        2. index of last sample to be gated
                gateLength = round(segChGates(:,2) * sampleFrequency);
                gateStart  = segChGates(:,1) - floor(gateLength/2); 
                gateStop   = gateStart + gateLength - 1;
                allGatesAutoFormat{iCh} = [gateStart gateStop];
            end
        end
    end
end

% ---- Done.
return

