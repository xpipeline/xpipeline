function yStruct = xsuperclusterstruct(xStruct,dT,dF,method) 
% XSUPERCLUSTERSTRUCT - wrapper to XSUPERCLUSTER for processing cluster structs
%
% usage:
%
%  Y = xsuperclusterstruct(X,dT,dF,method) 
%
%  X      X-Pipeline cluster struct.
%  dT     Optional scalar. Time coincidence window [sec] to be used in computing
%         cluster overlaps. Default 0.
%  dF     Optional scalar. Freq coincidence window [Hz] to be used in computing
%         cluster overlaps. Default 0.
%  method Optional string. If supplied, it is passed to XSUPERCLUSTER. See 
%         XSUPERCLUSTER for recognised values and default value 
%         (Note: method='standard' does not work; see below for details). 
%
%  Y      X-Pipeline cluster struct composed of clusters from X selected by the
%         XSUPERCLUSTER 'fast' algorithm, and clusters from X with mask==0 
%
% $Id$

% ---- Developer's note: method = 'standard' does not work because that method 
%      in XSUPERCLUSTER requires the input cell array to have more than one
%      element (trigger list), while this function takes a single input list.

% ---- Checks on inputs.
narginchk(1,4);
if nargin==4 && strcmpi(method,'standard')
    error('Function does not work for method=standard')
end

% ---- Assign defaults if needed.
if nargin<3
    dF = 0;
end
if nargin<2
    dT = 0;
end

% ---- xsupercluster takes input triggers in flat array format. That means that
%      properties not stored in the flat array (such as pass flags) will be lost
%      if using xsupercluster directly. We get around this by making a copy of 
%      the input struct triggers in flat array format, keeping only the data 
%      needed by xsupercluster (time-frequency properties and a ranking 
%      statistic), and adding a dummy "likelihood" that is just a counter from 
%      1,...,N of the input triggers. The output values of this "likelihood" 
%      are then the row numbers of the triggers in the input struct that are
%      selected by the superclustering. We then use xclustersubset() to select 
%      these surviving triggers directly from the input struct, thus preserving 
%      all the trigger information.

% ---- Convert selected triggers to array format.
xArray = xclusterstructtoarray(xStruct);

% ---- Construct reduced array with dummy likelihood.
dummyArray = [xArray(:,1:7) xStruct.significance [1:length(xStruct.significance)]'];
likelihoodTypes = {'significance','index'};

% ---- Add coincidence windows to end time (column 3) and end frequency (column 6)
dummyArray(:,3) = dummyArray(:,3) + dT;
dummyArray(:,6) = dummyArray(:,6) + dF;

% ---- Pass through xsupercluster.
if nargin==4
    [~, ~, ~, dummyLikelihood] = xsupercluster({dummyArray},likelihoodTypes,'significance',method);
else
    [~, ~, ~, dummyLikelihood] = xsupercluster({dummyArray},likelihoodTypes,'significance');
end

% ---- Down-select to surviving triggers.
yStruct = xclustersubset(xStruct,dummyLikelihood(:,2));

% ---- Done.
return

