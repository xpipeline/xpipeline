function xappendinjectionresults(fileName,clusterInj,verificationParameters,forceFlag)
% XAPPENDINJECTIONRESULTS - Append injection triggers to existing output file.
% 
% XAPPENDINJECTIONRESULTS appends injection triggers to an existing output 
% matlab file a produced by XDETECTION. This is useful for handling long 
% injections that straddle segment boundaries.
%
% usage:
%
%     xappendinjectionresults(fileName,clusterInj,verifyParam,forceFlag)
%
% fileName    String. Name of existing injection file to which results are to be
%             appended.
% clusterInj  Struct array of X-Pipeline triggers to be added to the file.
% verifyParam Optional struct. If supplied, values in the struct are checked 
%             against variables stored in the existing output file. If all 
%             checks are passed, the input clusterInj is merged into the 
%             clusterInj in the existing file. Otherwise either the existing 
%             file is over-written or an error occurs; see below.
% forceFlag   Optional boolean (default false). If false, then an error
%             occurs if there are inconsistencies between values stored 
%             in the struct and the file (except as noted below). If true,
%             the file is over-written in these circumstances instead of
%             exiting with error. This is useful in large-scale analyses
%             where file I/O problems can cause the original file to be
%             corrupt.
%          
% The following checks are performed:
% 1) The difference  dt = (startTime)_new - (startTime)_existing must satisfy
%    dt ~= 0 for the triggers to be appended. If dt==0 then it is assumed
%    we are re-running the same job so the original file is over-written with
%    the contents of verifyParam.
% 2) stopTime is subjected to the same check as startTime.
% 3) The variables 'clusterInj', 'skyPositions', 'svnversion_xdetection' are 
%    ignored in the comparison.
% 4) All other variables in verifyParam are compared to those in the existing 
%    file. An error occurs if there are any discrepancies between values stored 
%    in the struct and the file (except as noted above), or if any fields in the 
%    struct do not correspond to variables in the file. However, the dt==0 
%    comparison takes precedence; in that case the file is over-written instead
%    of exiting with error.
%
% The appended file is updated to include the new triggers from clusterInj. 
% Also, the stopTime is updated to match verifyParam.stopTime. All other
% data in the existing file are unchanged.
%
% $Id$ 

% ---- Check number of input arguments, assign defaults.
narginchk(3,4);
if (nargin<4) 
    forceFlag = false;
end

% ---- Default write mode when calling this function.
writeMode = 'append';

% ---- Flag indicating if we have found a mismatch between existing file and verification parameters.
errorCondition = false;

% ---- Load pre-existing file.
try
    fileStruct = load(fileName);
    fileLoaded = true;
catch 
    writeMode = 'replace';
    warning(['Unable to read existing file ' fileName '; over-writing.']);
    fileLoaded = false;
end

% ---- Conduct checks if desired (and file has been loaded).
if (nargin>=3) & fileLoaded

  % ---- Encase check in a try-catch block as we are prone to errors in production runs due to corrupt .mat files.
  try

    fname = fieldnames(verificationParameters);
    for ii=1:length(fname)
        old = getfield(fileStruct,fname{ii});
        new = getfield(verificationParameters,fname{ii});
        switch fname{ii}
            case 'clusterInj'
                % ---- Do nothing; these are the new triggers to be merged,
            case 'startTime'
                % ---- Start times should be offset by a time dt > 0, since 
                %      jobs for a given injection are run in order of increasing
                %      segment number.  Normally the offset will be
                %      (blockTime - 2*transientTime), but may be less than this
                %      for a segment at the end of a science segment, or longer
                %      if the injection is long compared to the block time (it 
                %      may run over several consecutive blocks.
                %      Note that we'll get an error -- as desired -- 
                %      if any of these quantities don't exist! 
                dt = verificationParameters.startTime - fileStruct.startTime;
                if (dt == 0) 
                    writeMode = 'replace';
                    warning(['Start time matches original injection file. Over-writing.']);
                end
            case 'stopTime'
                % ---- Same check as for startTime.
                dt = verificationParameters.stopTime - fileStruct.stopTime;
                if (dt == 0) 
                    writeMode = 'replace';
                    warning(['Stop time matches original injection file. Over-writing.']);
                end
            case 'skyPositions'
                % ----  Do nothing - this changes with Earth's rotation.
            case 'svnversion_xdetection'
                % ----  Do nothing - this sometimes has an extra \n at start.
            otherwise
                if ~isequal(old,new)
                    warning(['Mismatch in values of ' fname{ii} '.']);
                    disp([fname{ii} ' in existing file:']);
                    disp(old);
                    disp([fname{ii} ' of data to be appended:']);
                    disp(new);
                    errorCondition = true;
                end
        end
    end

  catch

    % ---- Set error flag.
    errorCondition = true;

  end

end

% ---- If we wish to append but have an error condition, then exit. (For 
%      replacement there's no need to exit since we're over-writing the 
%      original file.) 
if errorCondition && strcmp(writeMode,'append') 
    if forceFlag
        warning('Error in comparison of existing file with data to be appended. Forcing over-write of existing file.');
        writeMode = 'replace';
    else
        error('Error in comparison of existing file with data to be appended. Exiting.');
    end
end

switch writeMode

    case 'append'

        % ---- Merge trigger lists. Note that the injection file for a single 
        %      injection job only holds triggers in the last element of clusterInj.
        mergedSet = xclustermerge(fileStruct.clusterInj(end),clusterInj(end));
        % ---- Make sure order of field names matches that in fileStruct.clusterInj 
        %      or else the next step will fail. 
        mergedSet = orderfields(mergedSet,fileStruct.clusterInj);
        % ---- Over write original triggers with expanded set.
        fileStruct.clusterInj(end) = mergedSet;
        % ---- Update stopTime.
        fileStruct.stopTime = verificationParameters.stopTime;

        % ---- Write modified file.
        save(fileName,'-struct','fileStruct');

    case 'replace'

        % ---- Over-write existing file.
        save(fileName,'-struct','verificationParameters');

    otherwise

        error(['writeMode ' writeMode ' not recognised. How did we get here?!?']);

end

% ---- Done.
return

