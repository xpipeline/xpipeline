function [data, amplitudeSpectra, analysisLength, transientLength, ...
    normalizationFactor] = xcondition(data, sampleFrequencies, ...
    minimumFrequency, analysisTime, whiteningTime, sampleFrequency, ...
    verboseFlag, trainingMode, gateList, overwhiten)
% XCONDITION Resample, high pass filter, and whiten time series data.
%
% XCONDITION resamples, high pass filters and whitens time series data with
% minimal phase shifting.  The data from all channels are first resampled
% to a common sample frequency.  The resampled data is then detrended by
% zero-phase high pass filtering with a cutoff at 0.5 times the specified
% minimum analysis frequency.  The data is whitened by zero-phase linear
% prediction so that it is uncorrelated on time scales shorter than the
% requested time scale for whitening.  The whitened data is inverse Fourier
% transformed back to the time domain.  Finally, the whitened data is
% normalised.
%
% usage:
%
% [data, amplitudeSpectra, analysisLength, transientLength, ...
%     normalizationFactor] = xcondition(data, sampleFrequencies, ...
%     minimumFrequency, analysisTime, whiteningTime, sampleFrequency, ...
%     verboseFlag, trainingMode, gateList, overwhiten);
%
%   data                 Cell or numeric array of input time series data.
%                        If a cell array, then each element must contain a
%                        single row or column vector representing the input
%                        data for one channel.  If a numeric array, then
%                        each column represents one channel.  
%   sampleFrequencies    Vector of sample frequencies of input data [Hz].
%   minimumFrequency     Scalar.  Minimum frequency of subsequent analysis 
%                        [Hz].
%   analysisTime         Vector.  FFT integration times of subsequent
%                        analyses [seconds]. 
%   whiteningTime        Scalar.  Length of whitening filter [seconds].
%   sampleFrequency      Scalar.  Sample frequency of output data [Hz].
%   verboseFlag          Boolean flag for verbose status output.
%   trainingMode         Optional array or string specifying how the data
%                        is to be whitened.  If an array of size
%                        halfDataLength x numberOfChannels, it is used as a
%                        pre-computed linear predictor error filter (lpef)
%                        to whiten the data. If a string, the lpef is
%                        trained using the data being analysed.  
%                        Specifically, if 'search' then filters are trained
%                        on-the-fly using the full input data set.  If
%                        'veto' or 'frankenveto', then data conditioning
%                        filters are trained ignoring an interval of
%                        duration 3*max(analysisTime) in the center of the
%                        data segment.  (This veto mode avoids training
%                        filters on the transient being studied.)  If not
%                        supplied, then defaults to 'frankenveto'.
%                        WARNING: Current version of code will normalize 
%                        conditioned data using hard-wired constants
%                        computed for LIGO SRD noise on 64-1024 Hz.  THIS
%                        SHOULD BE FIXED.
%   gateList             Optional cell array. Applies gates at specified
%                        times with a Tukey window. Size must match number
%                        of detectors, with an empty array for detectors
%                        with no gates. Each array has one line per gate to
%                        be applied, with columns as follows:
%                          i. sample number of center of the gate (at sample
%                            rate sampleFrequency) [integer sample]
%                          ii. "zero time": duration of the central portion
%                            of the window that has value zero [seconds]
%                          iii. "taper time": total duration of the nonzero
%                            portions of the window [seconds]
%   overwhiten           Optional boolean. If true then over-whiten data by
%                        applying the LPEF a second time. IN THIS CASE THE 
%                        DATA NORMALISATION IS UNDEFINED. Default false.
%
%   conditionedData      Matrix of conditioned time-domain data.  
%   amplitudeSpectra     Cell array.  Each element is the matrix of
%                        amplitude spectral densities of the input data
%                        [1/sqrt(Hz)]; see below. 
%   analysisLength       Vector.  Transform length of subsequent analyses 
%                        [samples].
%   transientLength      Scalar. Approximate duration of filter transients
%                        [samples].
%   normalizationFactor  Vector.  Amplitude scaling factor that should be
%                        applied to conditionedData to normalize it for the
%                        corresponding analysisTime; see below.   
%
% The resulting conditioned data is returned as a matrix of time-domain
% data with each channel in a separate column.  XCONDITION also returns a
% matrix whose columns contain the one-sided amplitude spectral densities
% of the high pass filtered data at a frequency resolution corresponding to
% the requested analysis times.
%
% The conditioned data are normalized such that a Fourier transform
% analysis with the analysisTime(1) results in frequency-domain
% coefficients with a mean-squared amplitude of unity.  Equivalently, the
% conditioned data will have (ignoring loss of bandwidth due to the
% high-pass filter) a variance of
%
%   variance = 1 / (sampleFrequency * analysisTime(1))
%
% The amplitude spectra and power spectra estimated for the input data are
% one-sided, in Hz.  For example, input white noise of variance sigma2 has 
%
%   amplitudeSpectra = (2 * sigma2 / samplingFrequency)^0.5.
% 
% With these conventions, the conditioned data will have in-band one-sided
% power spectrum of  
%
%   power spectrum = 2 * variance / sampleFrequency
%                  = 2 / (sampleFrequency^2 * analysisTime(1))
%
% When analysisTime has multiple elements, the conditioned data is
% normalized using the first element of analysisTime.  To normalize for a
% different analysis time, analysisTime(j), multiply the conditioned data
% for detector k by the corresponding element normalizationFactor(j,k);
% i.e. 
%
%   conditionedData(:,k) -> conditionedData(:,k) * normalizationFactor(j,k)
%
% The whitening filter may be supplied externally, or trained on-the-fly
% using the input data.  If the latter, the central 5*max(analysisTime)
% seconds of the resampled data are optionally ignored when estimating the 
% amplitude spectra and constructing the linear predictor filters.
%
% Finally, XCONDITION returns the required Fourier transform length for
% subsequent analysis (simply sampleFrequency*analysisTime) and the
% estimated length of filter transients which should be excluded from the
% analysis.  Note that, due to use of zero-phase filtering, transients are
% present at both the beginning and the end of the data stream.
%
% Note: XCONDITION currently assumes a rectangular analysis window.
%
% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%
% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   hardcoded parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- If set to non zero the conditioning will be tested on the fly
autoTest = 1;

% ---- High pass filter order.
hpfOrder = 6;

% ---- Ratio of high pass filter frequency to minimum analysis frequency.
hpfCutoffFactor = 0.5;

% ---- Ratio of transient length to whitening filter length scale
%      (conservative). 
transientFactor = 4;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   process and validate command line arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check for sufficient command line arguments.
narginchk(6, 10);


% ---- Check optional arguments.
if (nargin < 10) 
    % ---- By default whiten rather than over-whiten.
    overwhiten = false;    
end
% ---- By default do not apply gating.
gate = false;
if (nargin >= 9)
    if ~isempty(gateList)
        gate = true;
    end 
end
if (nargin < 8) 
    % ---- By default do not train on central portion of data.
    trainingMode = 'frankenveto';    
end
if (nargin < 7) 
    % ---- Disable debug output by default.
    verboseFlag = false;
end

% ---- Verify that data is a cell array.
if ~iscell(data),
    % ---- Insert data into a single cell.
    data = mat2cell(data, size(data, 1), size(data, 2));
end

% ---- Force one dimensional (column vector) cell array.
data = data(:);

% ---- Determine number of channels.
numberOfChannels = length(data);

% ---- Force row vector of sample frequencies.
sampleFrequencies = sampleFrequencies(:)';

% ---- Verify that number of sample frequencies matches number of channels.
if length(sampleFrequencies) ~= numberOfChannels,
    error('number of sample frequencies and channels are inconsistent');
end

% ---- Force data into column vectors and determine data lengths.
dataLengths = zeros(1, numberOfChannels);
for channelNumber = 1 : numberOfChannels,
    data{channelNumber} = data{channelNumber}(:);
    dataLengths(channelNumber) = length(data{channelNumber});
end

% ---- Determine data durations.
dataTimes = dataLengths ./ sampleFrequencies;

% ---- Verify that data durations are equal for all channels.
if ~all(dataTimes == dataTimes(1)),
    error('data durations are not equivalent');
end

% ---- Determine length of resampled data.
dataTime = dataTimes(1);
dataLength = dataTime * sampleFrequency;
halfDataLength = dataLength / 2 + 1;

% ---- Verify that resampled data length is an integer power of 2.
if bitand(dataLength,dataLength-1),
    error('data length is not an integer power of two');
end

% ---- Force analysisTime to be a column vector.
analysisTime = analysisTime(:);

% ---- Determine analysis length (FFT length of subsequent analysis).
analysisLength = analysisTime * sampleFrequency;
halfAnalysisLength = analysisLength / 2 + 1;

% ---- Verify that analysis length is an integer power of 2.
if any(bitand(analysisLength,analysisLength-1)),
    error('analysis length is not an integer power of two');
end

% ---- Verify that whitening time is at least as long as FFT length for 
%      all subsequent analyses.
if (whiteningTime < max(analysisTime)),
    error('whitening time scale must exceed analysis time scale');
end

% ---- Determine whitening length.
whiteningLength = whiteningTime * sampleFrequency;

% ---- Determine Nyquist frequency of analysis.
nyquistFrequency = sampleFrequency / 2;

% ---- High pass filter cutoff frequency.
hpfCutoffFrequency = hpfCutoffFactor * minimumFrequency;

% ---- Verify that high pass filter cutoff frequency is physically sane.
if (hpfCutoffFrequency < 1 / whiteningTime),
    error('high pass filter cutoff frequency must exceed 1/whiteningTime');
end
if (hpfCutoffFrequency > nyquistFrequency),
    error('high pass filter cutoff frequency exceeds Nyquist frequency');
end

% ---- Estimate approximate transient length.
transientLength = transientFactor * whiteningLength;

% ---- Verify that we have some data left after dropping
%      transient-polluted edges. 
if any(analysisLength > (dataLength - 2 * transientLength)),
    error('analysis length exceeds usable data length');
end

% ---- Validate filter trainingMode variable: must be a halfDataLength x
%      numberOfChannels array or a known string.
if (isstr(trainingMode))
    switch trainingMode
        case {'veto','frankenveto','search'}
        otherwise
            error('trainingMode not a recognized string.')
    end
    makeFilter = 1;
else
    if ~(isnumeric(trainingMode) && size(trainingMode,1)==halfDataLength ...
            && size(trainingMode,2)==numberOfChannels)
        error('trainingMode filter array not of the correct size.')
    end
    makeFilter = 0;
end

% ---- Verify than overwhiten flag is a boolean / logical.
if ~islogical(overwhiten)
    error('Optional argument overwhiten must be a logical.');
end

% ---- Verify that gate information is correct.
if gate
    if ~iscell(gateList)
        error('Optional argument gateList must be a cell array.')
    end
    if length(gateList) ~= numberOfChannels
        error('Length of gateList must match number of channels.')
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   start timer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Start timer.
if verboseFlag
  globalStartTime = clock;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   resample data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag
    fprintf(1, 'resampling data ...');
    localStartTime = clock;
end

for channelNumber = 1 : numberOfChannels
    
    data{channelNumber} = xresample(data{channelNumber}, sampleFrequencies(channelNumber), sampleFrequency);

end
    
% ---- Convert cell array of resampled data to numeric array with each
%      column holding one channel.
data = [data{:}];

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   high pass filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Hard-coded high-pass filter.  It is applied forwards and backwards
%      to minimize phase distortion.  With these parameters, the magnitude
%      of the impulse response falls below 1e-3 in about 30 ms.  So, data
%      corruption at the edges of the data segment is minimal. 

if verboseFlag
    fprintf(1, 'applying high pass filter ...');
    localStartTime = clock;
end

for channelNumber = 1 : numberOfChannels

    % ---- High-pass filter.
    [data(:,channelNumber), hpfSOS] = xhpf(data(:,channelNumber), sampleFrequency, hpfCutoffFrequency, hpfOrder);

    % ---- Taper ends.
    data(:,channelNumber) = xtaper(data(:,channelNumber), sampleFrequency);
    
end

% ---- Estimate frequency response of high-pass filter, so we can account
%      for it in the PSD estimation.  We estimate the response by filtering
%      a unit impulse, then FFTing the filtered data.  Use each of the 
%      frequency resolutions of the output amplitudeSpectra. 
for ii = 1:length(analysisTime)
    impulseData = zeros(analysisTime(ii)*sampleFrequency,1);
    impulseData(end/2) = 1;
    impulseData = sosfiltfilt(hpfSOS, impulseData);
    hpfResponse_temp = abs(fft(impulseData));
    % ---- Keep non-negative frequencies only.
    hpfResponse{ii} = hpfResponse_temp(1:end/2+1);
end
clear hpfResponse_temp impulseData ii

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock,localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   optionally gate loud glitches
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if gate
    if verboseFlag
        fprintf(1, 'Checking for gates ...\n');
        localStartTime = clock;
    end
    % ---- Loop over detectors.
    for ii = 1:numberOfChannels
        currDetector = gateList{ii};
        if isempty(currDetector)
            % ---- No gates for this detector.
            continue
        else
            if verboseFlag
                fprintf(1, ['  ... applying gates for detector number ', num2str(ii), '.\n']);
            end
        end
        % ---- Loop over gates for this detector.
        for jj = 1:length(currDetector(:,1))
            centerSample = round(currDetector(jj,1)); %-- force to be an integer
            zeroTime     = currDetector(jj,2);
            taperTime    = currDetector(jj,3);
            % ---- Make window. Force the length of the window to be an
            %      even number.
            alpha = taperTime/(zeroTime+taperTime);
            windowSamples = 2*round(sampleFrequency*(zeroTime+taperTime)/2);
            window = 1 - tukeywin(windowSamples,alpha);
            % ---- Pad window with "ones" to make the same length as the
            %      data timeseries.
            samplesBefore = centerSample - windowSamples/2;
            samplesAfter  = length(data) - centerSample - windowSamples/2;
            % ---- Check for the case where the window extends off one end
            %      of the data timeseries. 
            if samplesBefore < 0 
                window(1:abs(samplesBefore)) = [];
                samplesBefore = 0;
            end
            if samplesAfter < 0 
                window((end-abs(samplesAfter)+1):end) = [];
                samplesAfter = 0;
            end
            % ---- Construct and apply the full gating window.
            gateVect = [ones(samplesBefore,1); window; ones(samplesAfter,1)];
            data(:,ii) = data(:,ii).*gateVect;
        end
    end
    if verboseFlag
        fprintf(1, ' done. (%.1f seconds)\n', etime(clock,localStartTime));
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   optionally zero out data around event for training filter 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Retain copy of full data.  (Do this regardless of whether or not we
%      zero out central region in working copy.)
fulldata = data;
if (makeFilter)
    switch trainingMode
        case {'veto','frankenveto'}
            % ---- For veto-mode operation, zero out 3*max(analysisLength) 
            %      samples centered on the event time.  Note: It might be 
            %      an improvement to zero the data smoothly.
            zeroStart = 1 + (dataLength - 3*max(analysisLength)) / 2;
            zeroStop = zeroStart + 3*max(analysisLength) - 1;
            data(zeroStart:zeroStop,:) = 0;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 compute accurate power spectrum                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use the median mean average algorithm (as detailed in the FINDCHIRP paper) 
% to calculate an initial PSD. This reduces the effect of large glitches and/or
% injections when training the linear predictor error filter.

%%
PSDraw = zeros(size(data,1),numberOfChannels);
for channelIdx=1:numberOfChannels
    % ---- Estimate PSD with resolution of 1/whiteningTime.
    [PSDintermediate Fraw] = medianmeanaveragespectrum(data(:,channelIdx),sampleFrequency,whiteningLength);
    % ---- Convert from physical units to discrete one-sided PSD.
    PSDintermediate = PSDintermediate * sampleFrequency * dataLength;
    % ---- Interpolate from coarse frequency spacing 1/whiteningLength to
    %      fine resolution 1/dataTime.
    PSDintermediate = interp1(Fraw,PSDintermediate,[0:1/dataTime:sampleFrequency/2]');
    % ---- Saturate the PSD at low/high frequency 
    %      otherwise the whitening filters try to whiten the low frequency
    %      (below high pass filter cut-off) and high frequency (above the
    %      anti-aliasing filter cut-off) part of the data.
    hpfCutOffBin = hpfCutoffFrequency/sampleFrequency*dataLength;
    PSDintermediate = PSDintermediate;
    PSDintermediate(1:hpfCutOffBin) = ...
        max(PSDintermediate(1:hpfCutOffBin),PSDintermediate(hpfCutOffBin));
    resamplingCutOffBin = ceil(0.98*dataLength/2);
    PSDintermediate(resamplingCutOffBin:end) = ...
        max(PSDintermediate(resamplingCutOffBin:end),PSDintermediate(resamplingCutOffBin));
    % ---- Pack into 2-sided PSD.
    PSDintermediate = ...
        0.5 * [PSDintermediate; flipud(PSDintermediate(2:end-1))];
    % ---- Store in array.
    PSDraw(:,channelIdx) = PSDintermediate;
    
    clear PSDintermediate;
end
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   fast fourier transform 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag
    fprintf(1, 'fourier transforming signals ...');
    localStartTime = clock;
end

% ---- Fourier transform.
fulldata = fft(fulldata);
data = fft(data);

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   estimate auto-correlation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag
    fprintf(1, 'estimating signal autocorrelation ...');
    localStartTime = clock;
end

% ---- Auto-correlation estimates.  Needed for both LPEF training and PSD
%      estimation.  Based on working copy of data with optionally zeroed
%      out part.
% ---- The auto-correlation will suffer from a bias due to the zeroing of 
%      N=whiteningLength points at the beginning and end of the data vector
%      (this was done to remove high pass filter transients).  We make an
%      approximate correction for this bias by normalizing the
%      auto-correlation by the number of non-zeroed data samples at zero
%      lag.  This is sufficient to ensure that the PSD is normalized
%      correctly -- the normalization is based on the auto-correlation at
%      zero lag.
% ---- Compute and normalize the auto-correlation.  The real() call here
%      removes any machine-precision-level imaginary part (the analytic
%      result is purely real). 

% Use accurate PSD to calculate autoCorrelation
autoCorrelation = real(ifft(PSDraw)) / (dataLength-2*whiteningLength);

% % ---- A more precise correction would presumably normalize the
% %      auto-correlation by the number of non-zeroed data samples at each
% %      lag.  The following code does this, but it is found to cause
% %      under-whitening of the data at low frequencies.  We therefore stick
% %      with the simple normalization of the zero-lag auto-correlation.
% % ---- This counts the number of samples in the auto-correlation that are
% %      forced to zero at each lag.
% numberZeroed = (4*whiteningLength) * ones(dataLength,1);
% numberZeroed(1:2*whiteningLength) = 2*whiteningLength ...
%     + [0:(2*whiteningLength-1)]';
% numberZeroed(end-2*whiteningLength+1:end) = 2*whiteningLength ...
%     + [(2*whiteningLength):-1:1]';
% % ---- The correct normalization of the auto-correlation counts only the
% %      non-zeroed samples.  The real() call here removes any
% %      machine-precision-level imaginary part (the analytic result is
% %      purely real). 
% autoCorrelation = real(ifft(data.*conj(data))) ./ (dataLength-numberZeroed);

% ---- Finished computation of autoCorrelation, so no longer need data 
%      nulled around event time.  Replace with fulldata.
data = fulldata;
clear fulldata;

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   construct linear predictor error filter 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Construct Linear Predictor Error Filter (LPEF).
if (makeFilter)
    
    if verboseFlag
        fprintf(1, 'constructing LPEF ...');
        localStartTime = clock;
    end

    % ---- Solve for LPEF coefficients.
    lpefCoefficients = levinson(autoCorrelation(1:whiteningLength,:), ...
        whiteningLength - 1)';

    % ---- Pad time-domain LPEF to match length of data.  This will make it
    %      easy to apply in the frequency domain.
    lpefCoefficients = [lpefCoefficients; ...
        zeros(dataLength - whiteningLength, numberOfChannels);];

    % ---- Obtain LPEF in the frequency domain. 
    lpefCoefficients = fft(lpefCoefficients);

    % ---- Take absolute value to zero the phase of the LPEF.
    lpefCoefficients = abs(lpefCoefficients);

    % ---- Test that lpef respones fits well the PSD
    if autoTest
      normLPEFfactor = 1./median(lpefCoefficients);
      normLPEF = lpefCoefficients*diag(normLPEFfactor);
      normPSDfactor = sqrt(median(PSDraw));
      normPSD = 1./sqrt(PSDraw)*diag(normPSDfactor);
      deviation = abs(normLPEF-normPSD);
      % ---- ensure that the two estimate are equal within 1% over 95% of
      %      the frequency band
      if max(prctile(deviation,95,1)) > 0.05
        warning(['LPEF PSD fitting has a deviation ' num2str(prctile(deviation,95,1)) ...
               ' > 5%. There is probably a problem with the whitening.'])
      end
      clear normLPEF normPSD deviation
    end

    % ---- Extract zero to Nyquist frequency LPEF coefficients.
    lpefCoefficients = lpefCoefficients(1:halfDataLength,:);

    if verboseFlag
        fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
    end

else

    % ---- Use input argument trainingMode at LPEF coefficients.
    lpefCoefficients = trainingMode;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   apply linear predictor error filter 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag
    fprintf(1, 'applying zero phase lpefs ...');
    localStartTime = clock;
end

% ---- Extract zero to Nyquist frequency data.
data = data(1 : halfDataLength, :);

% ---- Apply zero-phase LPEF.
data = lpefCoefficients .* data;

% ---- Optionally over-whiten by applying LPEF a second time.
if overwhiten
    warning('Overwhitening. The output data normalisation is not defined in this case. Caveat emptor!');
    data = lpefCoefficients .* data;
end

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   estimate power spectra of input data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag
    fprintf(1, 'computing amplitude spectra of input data ...');
    localStartTime = clock;
end

% ---- We want to estimate PSD at resolution 1/analysis.  To do this we 
%      use the fact that the PSD is proportional to lpefCoefficients.^(-2).
%      We average the PSD across multiple frequency bins.  We the normalize
%      our estimated PSD using using C[0] = mean(PSD). 

% ---- This code does the averaging of the PSD in two steps.  First, we
%      average consecutive bins of the PSD down to a resolution equal to
%      1/whiteningTime.  We then normalize the PSD using C[0] = mean(PSD). 
%      Second, we average 1/PSD instead of PSD down to
%      the target frequency resolution.  
%
%      Notes:
%
%      1) Averaging 1/PSD handles the impact of lines more accurately for
%      determining their effect on the received SNR.  
%      2) We don't do the entire averaging with 1/PSD because averaging the
%      PSD in a single bin is exponentially distributed, and averaging it's
%      inverse produces a strong bias.  However, if we first average the
%      PSD down to the whitening resolution, the averaged PSD is Gaussian
%      distributed.  Numerical tests show that averaging 1/PSD at this
%      point introduces no measurable bias for white noise for dataLength
%      and whiteningLength values tpyically used in analyses.

% ---- First pass: average PSD to resolution matching whiteningTime.
%
% ---- Number of consecutive bins to be averaged together to give PSD at 
%      target frequency resolution.
numberBinAverage = dataLength/whiteningLength;
% ---- Perform averaging.  (PSD is proportional to lpefCoefficients.^(-2).)
powerSpectra = binaverage(lpefCoefficients.^(-2),numberBinAverage);
halfWhiteningLength = size(powerSpectra,1);
%
% ---- Determine overall normalisation of PSD using the fact that the
%      autocorrelation is the Fourier transform of the PSD, and therefore
%      autoCorrelation(lag==0) = mean(PSD).  
temp_discrete_PSD_2_sided = [powerSpectra; powerSpectra(end-1:-1:2,:)];
% ---- Find beta, ratio between autoCorr(0) = mean(PSD).
beta = autoCorrelation(1,:) ./ mean(temp_discrete_PSD_2_sided,1);
% ---- Normalise 2 sided PSD using beta.
discrete_PSD_2_sided = temp_discrete_PSD_2_sided * diag(beta);
% ---- Construct amplitude spectra.
amplitudeSpectraWT = (2*discrete_PSD_2_sided(1:halfWhiteningLength,:) ...
    ./ sampleFrequency).^(0.5);
% ---- Clean out variable that use a lot of memory.
clear autoCorrelation lpefCoefficients 
clear temp_discrete_PSD_2_sided discrete_PSD_2_sided powerSpectra 

% ---- Second pass: average PSD to resolution matching each value of
%      analysisTime. 
%
for ii = 1:length(analysisTime)
    % ---- Number of consecutive bins to be averaged together to give PSD at 
    %      target frequency resolution.
    numberBinAverage = whiteningLength/analysisLength(ii);
    invPowerSpectra = binaverage(amplitudeSpectraWT.^(-2),numberBinAverage);
    amplitudeSpectra{ii} = invPowerSpectra.^(-0.5);

    % ---- Divide by impulse response of high pass filter to account for that
    %      filter.  Note that we do this after normalizing the PSD, since
    %      autoCorrelation was computed after high-pass filtering.
    amplitudeSpectra{ii} = amplitudeSpectra{ii} ./ repmat(hpfResponse{ii},[1,numberOfChannels]);
end

% ---- Clean up.
clear hpfResponse invPowerSpectra amplitudeSpectraWT;

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock,localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   return to the time domain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag
    fprintf(1, 'returning to the time domain ...');
    localStartTime = clock;
end

% ---- Inverse Fourier transform to the time domain.
data = real(ifft([data; conj(flipud(data(2 : end - 1, :)));]));

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   normalize conditioned data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if verboseFlag
    fprintf(1, 'normalizing conditioned data...');
    localStartTime = clock;
end

% ---- Indices of valid data (i.e., after cutting off filter transient
%      regions).
validDataStart = transientLength + 1;
validDataStop = dataLength - transientLength;
switch trainingMode
    case {'veto','frankenveto'}
        % ---- See above where we zero out data around the event for
        %      training the LPEF.
        validDataIndices = [validDataStart:zeroStart-1, ...
            zeroStop+1:validDataStop];
    otherwise
        validDataIndices = validDataStart:validDataStop;
end

for channelNumber = 1:numberOfChannels
    % ---- Estimate PSD of conditioned data at first target analysis
    %      resolution. 
    condPowerSpectrum = medianmeanaveragespectrum( ...
        data(validDataIndices,channelNumber),sampleFrequency,analysisLength(1));
    % ---- Select in-band frequencies.
    frequencies = 0 : sampleFrequency / analysisLength(1) : nyquistFrequency;
    inBandIndices = find((frequencies > 1.2 * hpfCutoffFrequency) & ...
                         (frequencies < 0.8 * nyquistFrequency));
    % ---- Normalization factor needed to make average of PSD = 1 over all
    %      in-band frequencies for this detector.
    normalizations(:,channelNumber) = mean(condPowerSpectrum(inBandIndices));

end
% ---- Reset normalization factor to match desired output (see help info).
normalizations = ((2/(sampleFrequency*analysisLength(1))) ./ normalizations).^0.5;

% ---- Apply normalization factor to each detector.
data = data * diag(normalizations);

% ---- Normalization factors for all all other analysisTime values.
normalizationFactor = (analysisTime(1)./analysisTime).^0.5;

if verboseFlag
    fprintf(1, ' done. (%.1f seconds)\n', etime(clock, localStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   total elapsed time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Report total elapsed time.
if verboseFlag
    fprintf(1, 'total elapsed time: %.1f\n', etime(clock, globalStartTime));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Return to calling function.
return




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   helper function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Y = binaverage(X,Navg)
%
% BINAVERAGE - average together consecutive row bins in an array.
%
% BINAVERAGE averages together consecutive row bins in an array X.  Each
% element in the output matrix Y is the mean of Navg rows of X, except for
% the first and last sample of Y, which contain only half the number of
% bins. 
% 
% usage:
%
%   Y = binaverage(X,Navg)
%
%   X     Column vector or array.
%   Navg  Number of consecutive rows of X to average together.
%
%   Y     Output array from averaging Navg consecutive rows of X.
%
% Requirements: size(X,1) must have the value 2^N+1, with N a positive
% integer.  Navg must be a power of 2.

% Note: This function may well work for some other values of size(X,1) or
% of Navg; however the above restrictions are always satisfied for arrays
% representing one-sided PSDs in X-Pipeline, so we don't care about more
% general cases.

% ---- Figure out various lengths needed.
Ncol = size(X,2);
NrowIn = size(X,1);
NrowOut = (NrowIn-1)/Navg + 1;

% ---- Checks on lengths.
% ---- Verify that NrowIn is an integer power of 2 + 1.
if bitand(NrowIn-1,NrowIn-2),
    error('input matrix X must have integer power of two + 1 rows');
end
if bitand(Navg,Navg-1),
    error('Navg must be an integer power of two');
end

% ---- Check for trivial case of Navg = 1. In this case do nothing.
if Navg==1
    Y = X;
    return
end

% ---- Prepare storage for output.
Y = zeros(NrowOut,Ncol);

% ---- We average together consecutive bins in groups of Navg, except for 
%      the first and last sample, which contain only half the number of
%      bins. 

% ---- First bin.
Y(1,:) = mean(X(1:Navg/2,:),1);

% ---- Middle bins.  This part is a bit dense.  First determine indices of
%      bins which are not in the first or last sample.  
indexRange = Navg/2+1 : NrowIn-Navg/2-1;
% ---- Then take mean by reshaping the array so that all bins to be
%      averaged together are in a single column. 
tempArray  = reshape(X(indexRange,:),Navg,[]); 
tempArray  = mean(tempArray,1);
% ---- Finally reshape back to the correct format.  Note that we 
%      automatically test that the dimensions are right by virtue of
%      assigning the reshaped data to specific elements of Y.
Y(2:end-1,:) = reshape(tempArray,[],Ncol);

% ---- Last bin.
Y(end,:) = mean(X(end-Navg/2:end,:),1);

% ---- Done.
return

