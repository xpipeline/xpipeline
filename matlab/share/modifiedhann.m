function W = modifiedhann(N)
%
% Create modified Hann window.  This window function is constructed by 
% making a Hann window of half the desired length, splitting the window in
% the middle, and inserting half the desried length of ones.
%
%   W = modifiedhann(N)
%
%   N   Integer.  Length of window.   Must be a power of 2.
%
%   W   Column vector of length N containing the window values.  
%
% $Id$

% ---- Create Hann window of 1/2 desired length.
W = hann(1/2*N);

% ---- Split window in the middle, insert N/2 values of 1.
W(3/4*N+1:N) = W(1/4*N+1:1/2*N);
W(1/4*N+1:3/4*N) = 1;

% ---- Done.
return
