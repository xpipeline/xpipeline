function deltaT = computeTimeShifts( channels, skyPositions )
% computeTimeShifts: Calculate time delays between a specified set of
% detectors and the center of the Earth for a given set of sky positions.
%
%     deltaT = computeTimeShifts( channels, skyPositions );
%
% channels      Either a cell array of channel names, or a matrix of the
%               form channels(iDet,:) = det.V' (see LoadDetectorData).
% skyPositions  Array of sky positions, with format as in sinusoidalMap.
%               Only the first two columns (theta and phi values) are used.
%
% deltaT        Matrix of time shifts (seconds) of size
%               (number of sky positions)x(number of channels).  The  
%               delay is defined as (arrival time at the detector) - 
%               (arrival time at center of Earth).
%
% See also LoadDetectorData, ComputeAntennaResponse
%
% $Id$

% Constants
% Speed of light (in m/s).
speedOfLight = 299792458;

% Derived variables
% Number of detectors
nDet  = length(channels);

% Load the position matrix (d by 3) of detector locations with origin at
% Earth center (in meters)
if (iscell(channels))
    for iDet = 1:nDet
        % The detector is matched by the first character of the channel name.
        det       = LoadDetectorData(channels{iDet}(1));
        r(iDet,:) = det.V';
    end;
else
    r = channels;
end;

% Construct the sky direction assumed from skyPosition
omega = [sin(skyPositions(:,1)).*cos(skyPositions(:,2)) ...  % x
         sin(skyPositions(:,1)).*sin(skyPositions(:,2)) ...  % y
         cos(skyPositions(:,1))];              % z
 
% Calculate the time delay for each detector (in seconds)
deltaT = -omega*r'/speedOfLight;

% Done
return
