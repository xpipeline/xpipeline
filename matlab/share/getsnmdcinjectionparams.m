function [injectionGPS_s, injectionGPS_ns, injectionPhi, injectionTheta, ...
          injectionPsi, injectionType, injectionParam] = ...
          getsnmdcinjectionparams(startTime, blockTime, mdcLogFileName)
% GETSNMDCINJECTIONPARAMS - Translate BurstMDC SN MDC log file to X format.
%
%  [GPS_s, GPS_ns, phi, theta, psi, type, param] ...
%       = getsnmdcinjectionparams(startTime, blockTime, mdcLogName)
%
%  startTime    Scalar.  Start time of the data being analysed.
%  blockTime    Scalar.  Duration (s) of the data being analysed.
%  mdcLogName   String.  Name of log file specifying simulated signals to 
%               be injected.
%
%  [GPS_s, GPS_ns, phi, theta, psi, type, param]
%               Injection parameters; see XMAKEGWBINJECTIONFILE.
% 
% GETSNMDCINJECTIONPARAMS translates BurstMDC injection log files to the
% X-Pipeline format.  It is designed to work ONLY FOR LOG FILES FOR THE
% SUPERNOVA SEARCH; the corresponding X-Pipeline catalog file is
% osnsearch.mat.
%
% Only injections in the interval [startTime,startTime+blockTime] are
% returned.
%
% $Id$

% ---- Check number of arguments
if (nargin~=3) 
    error('Wrong number of input arguments.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Read injection params from mdc log file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Use rdburstmdclogfile function from matapps/src/simulation/BurstMDC
[numMdc,mdcField,mdcData] = rdburstmdclog(mdcLogFileName);

injectionHrss    = mdcData.SimHrss;
injectionHpHp    = mdcData.SimHpHp;
injectionHcHc    = mdcData.SimHcHc;
injectionCosIota = mdcData.Internal_x;
injectionTheta   = acos(mdcData.External_x);
injectionPhi     = mdcData.External_phi;
injectionPsi     = mdcData.External_psi;
injectionID      = mdcData.GravEn_SimID;  %-- name of files containing waveforms
injectionSimName = mdcData.SimName;  %-- eg: M0p2L60R15f1000t100_hanntaper_plus

for idx = 1:numMdc
    x = injectionSimName{idx};
    if length(x) >= 5 
        if strcmpi(x(1:5),'L15-2') || ...
           strcmpi(x(1:5),'L15-3') || ...
           strcmpi(x(1:5),'N20-2') || ...
           strcmpi(x(1:5),'W15-2') || ...
           strcmpi(x(1:5),'W15-4') 
           % ---- Inclination factors already applied in waveform file; reset
           %      inclination to zero to avoid cos(iota)<1 sign correction
           %      being applied twice. 
           injectionCosIota(idx) = 1;
        end
    end
end

% ---- Pre-allocate storage for other outputs.
injectionType    = cell(numMdc,1);
injectionParam   = cell(numMdc,1);

% ---- Determine how signal arrival time is stored in log file, and
%      translate to X-Pipeline convention as needed.
if isfield(mdcData,'SimStartGPS')
   % ---- Add 0.5s onto SimStartGPS to get peak time of injection.
   injectionGPS   = mdcData.SimStartGPS + 0.5;
elseif isfield(mdcData,'EarthCtrGPS')
   % ---- No need to add 0.5s into EarthCtrGPS.
   injectionGPS   = mdcData.EarthCtrGPS;
else
   error('Neither SimStartGPS nor EarthCtrGPS in mdc log file')
end

% ---- Loop over all injections in MDC log file, and determine details for
%      each.
for idx = 1:numMdc

    injectionType{idx}  = 'osnsearch';
    injectionHrssHp = injectionHpHp(idx).^0.5;
    injectionHrssHc = injectionHcHc(idx).^0.5 * sign(injectionCosIota(idx));
    injectionParam{idx} = [num2str(injectionHrssHp) '~' num2str(injectionHrssHc) '~' injectionID{idx}];
    % injectionParam{idx} = [num2str(injectionHrss(idx)) '~' injectionID{idx}];

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Keep only injections between startTime and startTime + blockTime
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

keepInjection = []; %-- row number of injections to keep after checking GPS time
for idx = 1:numMdc  %-- loop over all inj in mdclogfile
    if ( (injectionGPS(idx) >= startTime) & (injectionGPS(idx) <= startTime+blockTime) )
        keepInjection = [keepInjection; idx];
    end   
end

injectionGPS        = injectionGPS(keepInjection);
injectionTheta      = injectionTheta(keepInjection); 
injectionPhi        = injectionPhi(keepInjection);
injectionPsi        = injectionPsi(keepInjection);
injectionType       = injectionType(keepInjection);
injectionParam      = injectionParam(keepInjection);

% ---- Split GPS time into s and ns part
injectionGPS_s   = floor(injectionGPS);
injectionGPS_ns  = ((injectionGPS - floor(injectionGPS)) .* 1e9);

% ---- Done.
return
