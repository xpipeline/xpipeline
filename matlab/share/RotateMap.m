function [coordinates] = RotateMap(V,phi,theta,psi, usedeg)
% ROTATEMAP - rotate cartesian vectors by Euler angles
%
% usage :
%
%  coordinates=RotateMap(V,phi,theta,psi)
%
%    V : coordinates map to rotate, size must be 3,N (x,y,z, first frame)
%    phi : rotation angle around the z axis -> new frame (k,l,z)
%    theta : rotation angle around the k axis -> new frame (k,n,p)
%    psi : rotation angle around the p axis -> new frame (x',y',p=z')
%    usedeg : flag to specify if input value are in degrees
%
%    coordinates : 3*N in the rotated frame
%
%  Note that this is an "active" rotation, that is the vectors V are being
%  rotated relative to the initial frame (x,y,z). It is equal to using
%  the RotateVector function 3 times: first rotating by angle psi around z
%  axis, then by angle theta around x axis, and finaly by angle phi around z
%  axis. Note that in this equivalent construction of the Euler angle
%  rotation, all three rotation are relative to the axis in the initial frame.

% Nicolas Leroy and Michal Was (leroy@lal.in2p3.fr)                                                          
                                                          
% -----  check size
if size(V,2)~=3
error('wrong input size, must be 3,N');
end

% ----- prepare variables depending on the units used for the angles
if usedeg==1
sp=sind(phi);
cp=cosd(phi);
st=sind(theta);
ct=cosd(theta);
spp=sind(psi);
cpp=cosd(psi);
else
sp=sin(phi);
cp=cos(phi);
st=sin(theta);
ct=cos(theta);
spp=sin(psi);
cpp=cos(psi);
end

% ---- perfom the rotation
                                                          
x1 = (cpp*cp-ct*sp*spp)*V(:,1) + (-cp*spp-ct*cpp*sp)*V(:,2) + sp*st*V(:,3);
x2 = (ct*cp*spp+cpp*sp)*V(:,1) + (-spp*sp+ct*cp*cpp)*V(:,2) - cp*st*V(:,3);
x3 = st*spp*V(:,1) + st*cpp*V(:,2) + ct*V(:,3);

coordinates=[x1 x2 x3];

% ---- Done
return