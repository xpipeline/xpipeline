function [b, n] = xbasis(a)
% XBASIS - Return the residual and null stream coefficients for an array of
% of whitened antenna responses and frequencies

%#mex

b = zeros(size(a, 2), size(a, 2), size(a, 3));
n = zeros(size(a, 3), 2);
for i = 1:size(a, 3)
    [u, s, v] = svd(a(:, :, i));
    b(:, :, i) = v';
	n(i,:) = diag(s);
end