
% ---- Timeseries duration [s].
T = 4;
% ---- Data sample rate [Hz].
fs = 4096;
% ---- Time samples.
t = [0:(T*fs-1)]'/fs;
% ---- Network.
ifos = {'H','L','V'};
% ---- HEALPix number.
N = 32;
% ---- Directory holding sample glitch files.
glitchDir = '/Users/psutton/Desktop/stuff_directories/project_students/Ciaran/glitches/';


% ---- Generate some sample GWB sky maps.
igwb = 0;
if true

    igwb = igwb+1
    % ---- Create an injection log file for a short Gaussian pulse.
    [status,result] = system('echo "1000000000 500000000 0.50 1.57 1.11 G 3e-22~0.001" > test_inj.txt');
    % ---- Read the injection log file and make a 1-second ling h(t) timeseries for each detector.
    [h, gps_s, gps_ns, phi_inj, theta_inj, psi_inj] = xinjectsignal(1000000000-T/2,T,ifos,[fs,fs,fs],'test_inj.txt');
    h = cell2mat(h');
    % ---- Add some noise.
    h = h + 1e-22*randn(size(h));
    figure; plot(t,h)
    title('GWB: Gaussian')
    saveas(gcf,['gwb_' num2str(igwb) '_timeseries.png'],'png');
    % ---- Generate the sky map.
    [map,Enull,Inull,theta,phi] = generateskymap(gps_s,h,fs,ifos,N);
    % ---- Plot and save preferred map.
    map.probDensity = Enull./Inull-1;
    hand = plotskymap(map,'mollweid');
    title('GWB: Gaussian')
    saveas(gcf,['gwb_' num2str(igwb) '_skymap.png'],'png');
    
    igwb = igwb+1
    % ---- Create an injection log file for a short sine-Gaussian pulse.
    [status,result] = system('echo "1000000000 500000000 0.0 0.1 1.11 SG 3e-22~9~200" > test_inj.txt');
    % [status,result] = system('echo "1000000000 500000000 2.71 0.321 1.11 G 1e-21~0.001" > test_inj.txt');
    % ---- Read the injection log file and make a 1-second ling h(t) timeseries for each detector.
    [h, gps_s, gps_ns, phi_inj, theta_inj, psi_inj] = xinjectsignal(1000000000-T/2,T,ifos,[fs,fs,fs],'test_inj.txt');
    h = cell2mat(h');
    % ---- Add some noise.
    h = h + 1e-22*randn(size(h));
    figure; plot(t,h)
    title('GWB: sine-Gaussian')
    saveas(gcf,['gwb_' num2str(igwb) '_timeseries.png'],'png');
    % ---- Generate the sky map.
    [map,Enull,Inull,theta,phi] = generateskymap(gps_s,h,fs,ifos,N);
    % ---- Plot and save preferred map.
    map.probDensity = Enull./Inull-1;
    hand = plotskymap(map,'mollweid');
    title('GWB: sine-Gaussian')
    saveas(gcf,['gwb_' num2str(igwb) '_skymap.png'],'png');
    
    igwb = igwb+1
    % ---- Create an injection log file for a short WNB pulse.
    [status,result] = system('echo "1000000000 500000000 2.71 0.321 1.11 WNB 1e-21~200~100~0.1~48271" > test_inj.txt');
    % ---- Read the injection log file and make a 1-second ling h(t) timeseries for each detector.
    [h, gps_s, gps_ns, phi_inj, theta_inj, psi_inj] = xinjectsignal(1000000000-T/2,T,ifos,[fs,fs,fs],'test_inj.txt');
    h = cell2mat(h');
    % ---- Add some noise.
    h = h + 1e-22*randn(size(h));
    figure; plot(t,h)
    title('GWB: WNB')
    saveas(gcf,['gwb_' num2str(igwb) '_timeseries.png'],'png');
    % ---- Generate the sky map.
    [map,Enull,Inull,theta,phi] = generateskymap(gps_s,h,fs,ifos,N);
    % ---- Plot and save preferred map.
    map.probDensity = Enull./Inull-1;
    hand = plotskymap(map,'mollweid');
    title('GWB: WNB')
    saveas(gcf,['gwb_' num2str(igwb) '_skymap.png'],'png');

end


% ---- Generate some sample glitch sky maps.
iglitch = 0;
if true

    iglitch = iglitch+1
    % ---- Read sample glitch files.
    H = load([glitchDir 'Blip_429_H.txt']);
    L = load([glitchDir 'Blip_430_H.txt']);
    V = load([glitchDir 'Blip_432_H.txt']);
    h = [H L V];
    figure; plot(t,h)
    title('Blip: 429, 430, 432');
    saveas(gcf,['glitch_' num2str(iglitch) '_timeseries.png'],'png');
    % ---- Generate the sky map.
    [map,Enull,Inull,theta,phi] = generateskymap(gps_s,h,fs,ifos,N);
    % ---- Plot and save preferred map.
    map.probDensity = Enull./Inull-1;
    hand = plotskymap(map,'mollweid');
    title('Blip: 429, 430, 432');
    saveas(gcf,['glitch_' num2str(iglitch) '_skymap.png'],'png');

    iglitch = iglitch+1
    % ---- Read sample glitch files.
    H = load([glitchDir 'Koi_Fish_54_H.txt']);
    L = load([glitchDir 'Koi_Fish_55_H.txt']);
    V = load([glitchDir 'Koi_Fish_56_H.txt']);
    h = [H L V];
    figure; plot(t,h)
    title('Koi Fish: 54, 55, 56');
    saveas(gcf,['glitch_' num2str(iglitch) '_timeseries.png'],'png');
    % ---- Generate the sky map.
    [map,Enull,Inull,theta,phi] = generateskymap(gps_s,h,fs,ifos,N);
    % ---- Plot and save preferred map.
    map.probDensity = Enull./Inull-1;
    hand = plotskymap(map,'mollweid');
    title('Koi Fish: 54, 55, 56');
    saveas(gcf,['glitch_' num2str(iglitch) '_skymap.png'],'png');

    iglitch = iglitch+1
    % ---- Read sample glitch files.
    H = load([glitchDir 'Low_Frequency_Burst_1382_L.txt']);
    L = load([glitchDir 'Low_Frequency_Burst_1385_L.txt']);
    V = load([glitchDir 'Low_Frequency_Burst_1406_L.txt']);
    h = [H L V];
    figure; plot(t,h)
    title('Low Frequency Burst: 1382,1385, 1406');
    saveas(gcf,['glitch_' num2str(iglitch) '_timeseries.png'],'png');
    % ---- Generate the sky map.
    [map,Enull,Inull,theta,phi] = generateskymap(gps_s,h,fs,ifos,N);
    % ---- Plot and save preferred map.
    map.probDensity = Enull./Inull-1;
    hand = plotskymap(map,'mollweid');
    title('Low Frequency Burst: 1382,1385, 1406');
    saveas(gcf,['glitch_' num2str(iglitch) '_skymap.png'],'png');

end



