#!/usr/bin/perl -w

#=== Specify LIGO run ===#
$LIGORUN = 'S6';
$DIRDATE = 'test';

#=== Specify directories ===#
$HOMEDIR    = $ENV{'HOME'};
$GRBDIR     = sprintf "%s/Grb/%s/IPN", $HOMEDIR, $LIGORUN;
$SCRIPTSDIR = sprintf "%s/Scripts", $GRBDIR;
$EMAILDIR   = sprintf "%s/IpnData/Emails/IPN_%s_%s", $GRBDIR, $LIGORUN, $DIRDATE;
$PARSEDIR   = sprintf "%s/IpnData/Parsed/IPN_%s_%s", $GRBDIR, $LIGORUN, $DIRDATE;
$KNOWNDIR   = sprintf "%s/IpnData/Known/IPN_%s_%s", $GRBDIR, $LIGORUN, $DIRDATE;

$LASTHEADER = 'X-Proofpoint-Spam-Details';
$KNOWNGRB   = 'can\'t[\\s\\n]+improve';
$XTEXT      = 'X-Sun-Data-Type:\s*(text|default)';
$XEPS       = 'X-Sun-Data-Type:\s*postscript-file';

#=== Open output log file ===#
$logFile = sprintf "%s/parseemail_%s.log", $SCRIPTSDIR, $LIGORUN;
open LOGFILE, ">>$logFile"
  or die "Error opening output log file $logFile: $!";

#=== Specify parsed GRBs file ===#
$parsedGrbFile = sprintf "%s/parsedgrbs_ipnemail_%s.txt", $SCRIPTSDIR, $LIGORUN;

#=== Read parsed GRBs ===#
@parsedGrbs      = ();
@parsedGrbDates  = ();
@parsedGrbEmails = ();
if (-e $parsedGrbFile) {
  open PARSEDGRB, "<$parsedGrbFile"
    or die "Error opening input parsed GRB file $parsedGrbFile: $!";
  chomp(@parsedGrbs = <PARSEDGRB>);
  close PARSEDGRB;

  $pCtr = 0;
  foreach $parsedGrb (@parsedGrbs) {
    ($parsedGrbDates[$pCtr],$parsedGrbEmails[$pCtr]) = split(' ',$parsedGrb);
    $pCtr++;
  }
}

#=== Open parsed GRB file for output ===#
open PARSEDGRB, ">>$parsedGrbFile"
  or die "Error opening output parsed GRB file $parsedGrbFile: $!";


$lsCommand = sprintf "ls $EMAILDIR/*";
@emailFiles = `$lsCommand`;
chomp(@emailFiles);

print "\n";

#=== Loop through emails ===#
$nGRB = 0;
$parsedCtr = 0;
$knownCtr  = 0;
$emailCtr  = 0;
$otherCtr  = 0;
foreach $emailFile (@emailFiles) {
  open EMAILIN, "<$emailFile"
    or die "Error opening input email $emailFile: $!";

  $emailCtr++;

  @emailLines = ();
  chomp(@emailLines = <EMAILIN>);
  close EMAILIN;

  #=== Initilize various flags ===#
  $emailDate        = '';
  $grbDate          = '';
  $emailStart       = 0;
  $dataFlag         = 0;
  $messageFlag      = 0;
  $parsedGrbFlag    = 0;
  $knownGrbFlag     = 0;
  $psFlag           = 0;
 
  #=== Loop through email lines ===#
  $lCtr = 0;
  foreach $emailLine (@emailLines) {

    if ($emailStart == 0) {

      #=== Find email date ===#
      if ($emailLine =~ /^Date: (.+)$/) {
        $emailDate = $1;
      }

      #=== Find subject ===#
      if ($emailLine =~ /^Subject:\s*GRB\s*(\d+[a-zA-Z]?)/) {
        $grbDate = $1;
      }

      #=== Find last header marker ===#
      if ($emailLine =~ /^$LASTHEADER/) {
        $emailStart = $lCtr + 3;
      }
    } else {

      if ($dataFlag == 0) {
        if ($messageFlag == 0) {
          if ($emailDate eq '') {
            print "Error: Email date not found for\n$emailFile\n\n";
            printf LOGFILE "Error: Email not found for\n%s\n\n", $emailFile;
          }
          if ($grbDate eq '') {
            print "Error: GRB date not found for\n$emailFile\n\n";
            printf LOGFILE "Error: GRB date not found for\n%s\n\n", $emailFile;
          }
          $messageFlag = 1;

          if ($emailDate ne '' && $grbDate ne '') {

            $dataFlag = 1;

            #=== Check if this GRB has been parsed ===#
            $pCtr = 0;
            $grbEmailCtr = 1;
            foreach $parsedGrbDate (@parsedGrbDates) {
              if ($parsedGrbDate eq $grbDate) {
                if ($parsedGrbEmails[$pCtr] eq $emailFile) {
                  $parsedGrbFlag = 1;
                  last;
                } else {
                  $grbEmailCtr++;
                }
              }
              $pCtr++;
            }

            #=== If GRB has not been parsed ===#
            if ($parsedGrbFlag == 0) {

              $parsedGrbDates[$#parsedGrbDates+1]   = $grbDate;
              $parsedGrbEmails[$#parsedGrbEmails+1] = $emailFile;
              $nGRB++;
         
              #=== Open output GRB file ===#
              $grbFile = '';
              if ($grbEmailCtr == 1) {
                $grbFile = sprintf "%s/grb%s.txt", $PARSEDIR, $grbDate;
              } else {
                $grbFile = sprintf "%s/grb%s_%d.txt", $PARSEDIR, $grbDate, $grbEmailCtr;
              }
              open GRBFILE, ">>$grbFile"
                or die "Error opening output GRB file $grbFile: $!";

              printf GRBFILE "Date: %s\n", $emailDate;
              printf GRBFILE "Subject: GRB %s\n", $grbDate;
            }
          }
        }
      } else {
        if ($parsedGrbFlag == 0 && $lCtr >= $emailStart) {
          #=== Check if a postcript plot is attached ===#
          if ($psFlag == 0 && $emailLine =~ /$XEPS/) {
            $psFlag = 1;
            if ($grbEmailCtr == 1) {
              $grbFilePs = sprintf "%s/grb%s.eps", $PARSEDIR, $grbDate;
            } else {
              $grbFilePs = sprintf "%s/grb%s_%d.eps", $PARSEDIR, $grbDate, $grbEmailCtr;
            }
            open GRBFILEPS, ">>$grbFilePs"
              or die "Error opening output GRB file $grbFilePs: $!";
          }

          if (!($emailLine =~ /^X-Sun/) && !($emailLine =~ /------/) &&
              !($emailLine =~ /Isabel/) && !($emailLine =~ /Kevin/)) {
            #print "$emailLine\n";
            if ($psFlag == 1) {
                printf GRBFILEPS "%s\n", $emailLine;
            } else {
              printf GRBFILE "%s\n", $emailLine;
              if ($emailLine =~ /$KNOWNGRB/) {
                $knownGrbFlag = 1;
              }
            }
          } else {
            if ($emailLine =~ /$XTEXT/ && $psFlag == 1) {
              $psFlag = 2;
            }
          }
        }
      }
    }

    $lCtr++;
  }
  #=== End loop through email lines ===#

  if ($dataFlag == 1 && $parsedGrbFlag == 0) {
    $parsedCtr++;
    print "$emailFile\n";
    print "$nGRB GRB $grbDate\n\n";
    printf PARSEDGRB "%s %s\n", $grbDate, $emailFile;
    close GRBFILE;
    if ($psFlag > 0) {
      close GRBFILEPS;
    }
    if ($knownGrbFlag == 1) {
      $knownCtr++;
      $mvCommand = sprintf "mv $grbFile $KNOWNDIR/";
      system $mvCommand;
      if ($psFlag > 0) {
        $mvCommand = sprintf "mv $grbFilePs $KNOWNDIR/";
        system $mvCommand;
      }
    }
  } else {
    if ($dataFlag == 0) {
      $otherCtr++;
    }
  }

#  if ($nGRB > 5) {
#    exit;
#  }
}
#=== End loop through emails ===#

printf LOGFILE "Done processing all emails in %s.\n", $EMAILDIR;
print  LOGFILE "\n";
printf LOGFILE "Total number of emails         =  %3d\n\n", $emailCtr;

printf LOGFILE "Total number of IPN-only GRBs  =  %3d\n", $parsedCtr-$knownCtr;
printf LOGFILE "Total number of known GRBs     =  %3d\n", $knownCtr;
printf LOGFILE "Total number of non-GRB emails =  %3d\n", $otherCtr;
print  LOGFILE "                                 -----\n";
printf LOGFILE "                                  %3d\n", $parsedCtr+$otherCtr;
print  LOGFILE "\n\n";

#=== End parseEmail ===#
