function [ILABEL0] = plot_IPNcontours(grbIPNpath,grbDate);
%function to plot the IPN contours
% Input parameters are :
% grbIPNpath : path to get the IPN parameters
% grbDate : name of the GRB
% return value:
% ILABEL0 : labels for the legend on the IPN contours

y=0;

%=== Read the IPN localization parameters ===%
[ALPHA0,DEC0,RADIUS0,RADIUSC0,RADIUSERR0,ILABEL0,ICOLOR0] = readIPN_wpath(grbDate,grbIPNpath);

%=== Identify different circles ===%
iipn    = strmatch('IPN',ILABEL0);
ieclip  = strmatch('ECLIP',ILABEL0);
iearth  = strmatch('EARTH',ILABEL0);
imars   = strmatch('MARS',ILABEL0);
isuzaku = strmatch('SUZAKU',ILABEL0);
ihessi  = strmatch('HESSI',ILABEL0);
ihete   = strmatch('HETE',ILABEL0);

%=== Convert to a single row ===%
iipn0   = iipn';
iearth = iearth';
imars = imars';
isuzaku = isuzaku';
ihessi = ihessi';
ihete = ihete';

%=== Get annuli parameters ===%
ALPHAIPN0     = ALPHA0(iipn0);
DECIPN0       = DEC0(iipn0);
RADIUSIPN0    = RADIUSC0(iipn0);
RADIUSERRIPN0 = RADIUSERR0(iipn0);
nIPN          = length(ALPHAIPN0);

[RADIUSERRIPN0,isort] = sort(RADIUSERRIPN0);
RADIUSIPN0            = RADIUSIPN0(isort);
ALPHAIPN0             = ALPHAIPN0(isort);
DECIPN0               = DECIPN0(isort);

%=== Get blocking constraints ===%
iblock       = [iearth imars isuzaku ihessi ihete];
ALPHABLOCK0  = ALPHA0(iblock);
DECBLOCK0    = DEC0(iblock);
RADIUSBLOCK0 = RADIUS0(iblock);

alphaGrid = [];
decGrid   = [];
eBoxArea  = 0;
areaGrid  = [];
rProbGrid = [];
minProb   = 100.0;
maxProb   = 0.0;
nGrid     = 0;


if (size(RADIUSERRIPN0,2)>0)
[minIPNerr,indexIPN] = min(RADIUSERRIPN0)
%find this value in the main vector (ALPHA0, ...)
indexIPNref=find(RADIUSERR0 == minIPNerr)
else
%at least there is the ecliptic otherwise ....
indexIPNref=1;
end


%=== Specify step size of dec ===%
DELDEC = 0.01;

%=== Prepare figure ===%
%figure('Position', [0 0 780 580]);
figure;
set(gcf,'Position', [0 0 1170 650]);
set(gcf,'color','w');
%hold on;

hmap = axes('box', 'on', 'Position', [0.10 0.11 0.85 0.82], 'LineWidth', 0.01, 'TickLength',[0 0],...
            'FontSize',16,'FontWeight','normal','fontname','times');
axis([0 360 -90 90]);
axes(hmap);

xticks = [0:30:360];
yticks = [-90:10:90];

set(gca,'xscale','lin','yscale','lin','xgrid','on','XDir','reverse','ygrid','on','xtick',xticks,'ytick',yticks);
hold on;

xlabel('R I G H T   A S C E N S I O N  (degrees)','fontsize',16);
ylabel('D E C L I N A T I O N  (degrees)','fontsize',16);

%=== Move the x-axis label down a bit ===%
%xlab = get(gca,'xlabel');
%set(xlab,'position',get(xlab,'position') - [0 3.0 0]);

%ylab = get(gca,'ylabel');
%set(ylab,'position',get(ylab,'position') - [0.5 0.0 0]);

titleString = sprintf('E V E N T  %s', grbDate);
title(titleString,'fontsize',16,'fontname','times');

hp=[];

ileg = [];
ic = 0;
for ic = 1:length(ALPHA0)

  %=== Initialize circle parameters ===%
  ALPHA  = ALPHA0(ic);
  DECR    = DEC0(ic);
  RADIUS = RADIUS0(ic);
  LABEL  = ILABEL0{ic};
  COLOR  = ICOLOR0{ic};
  ERRADIUS = RADIUSERR0(ic);

  if(ERRADIUS<0)
  ERRADIUS=0 
  end

  R1 = RADIUS - ERRADIUS;

  DECMIN = DECR-R1;
  DECMAX = DECR+R1;

  DEC = [DECMIN:DELDEC:DECMAX];
  [alphaLeft,alphaRight,decLeft,decRight,edgeFlag] = paveGrid(DEC,ALPHA,DECR,R1);
  alphaAll = [alphaLeft alphaRight];
  decAll   = [decLeft decRight];

  h1=plot(alphaAll,decAll,'-','MarkerEdgeColor',COLOR,'Color',COLOR,'linewidth',1,'markersize',1);  
grid on;
hp=[hp h1];
  R1=RADIUS + ERRADIUS;

  DECMIN = DECR-R1;
  DECMAX = DECR+R1;

  DEC = [DECMIN:DELDEC:DECMAX];
  [alphaLeft,alphaRight,decLeft,decRight,edgeFlag] = paveGrid(DEC,ALPHA,DECR,R1);
  alphaAll = [alphaLeft alphaRight];
  decAll   = [decLeft decRight];

  h=plot(alphaAll,decAll,'-','MarkerEdgeColor',COLOR,'Color',COLOR,'linewidth',1,'markersize',1);
set(get(get(h,'Annotation'),'LegendInformation'),...
    'IconDisplayStyle','off'); % Exclude line from legend

end


LEGLABEL = ILABEL0(ileg);



