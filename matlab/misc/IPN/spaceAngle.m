function angleDiff = spaceAngle(ra0,dec0,ra1,dec1)
% SPACEANGLE - Calculates space angle between two sky positions 
%
% usage :
%
% angleDiff = spaceAngle(ra0,dec0,ra1,dec1)
%
%   ra0 : right ascension of the first position (in degrees)
%   dec0 : declination angle of the first position (in degrees)
%   ra1 : right ascencion of the second position (in degrees)
%   dec0 : declination angle of the second position (in degrees)
%
%   angleDiff : difference of between the two positions in degrees

% I. Leonor and N. Leroy


cosAngleDiff = cosd(dec1).*cosd(dec0).*cosd(ra1-ra0) + sind(dec1).*sind(dec0);
angleDiff    = acosd(cosAngleDiff);

% ---- Done
return

