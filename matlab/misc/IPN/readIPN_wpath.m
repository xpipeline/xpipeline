function [ALPHA0,DEC0,RADIUS0,RADIUSC0,RADIUSERR0,ILABEL0,ICOLOR0,ETIME0,TLABEL0] = ...
            readIPN_wpath(grbDate,path)
% READIPN_WPATH - function to read the IPN parameters stored in ASCII file
% 
% usage :
%   
%   [ALPHA0,DEC0,RADIUS0,RADIUSC0,RADIUSERR0,ILABEL0,ICOLOR0,ETIME0,TLABEL0] = readIPN_wpath(grbDate,path)
%
%       grbDate : string with the grb date ie 'yymmdd*' like '070201'
%       path : directory where the IPN parameters files are stored
%
%       ALPHA0 : list of RA for IPN centers
%       DEC0 : list of declination of the IPN centers
%       RADIUS0 : list of IPN radius
%       RADIUSCO
%       RADIUSERR0 : list of width for IPN rings
%       ILABEL0 : names of the different line to appeared in the plots
%       ICOLOR0 : colors for the different lines in the contours to appeared in the plots
%       ETIME0 :
%       TLABEL0

% I. Leonor and N. Leroy

%=== Specify file type ===%
%fileType = 'params';
fileType = 'parsed';

%=== Initialize arrays ===%
ALPHA0     = [];
DEC0       = [];
RADIUS0    = [];
RADIUSC0   = [];
RADIUSERR0 = [];
ETIME0     = [];
ETIMEERR0  = [];
ILABEL0    = {};
ICOLOR0    = {};
TLABEL0    = {};

ANNCOLOR  = ['b','m','g','c'];

%=== Specify input directory ===%
dataDir = path;

%=== Open input file ===%
ipnFile = sprintf('%s/grb%s_%s.txt', dataDir, grbDate, fileType);
%=== Check if file exists ===%
if exist(ipnFile,'file') == 0
  fprintf(1,'\nInput file %s does not exist.\n\n', ipnFile);
  return;
end
ifp = fopen(ipnFile,'r');

%=== Read data from input file ===%
dataLines = {};
nLines = 0;
while 1
  tline = fgetl(ifp);
  if ~ischar(tline)
    break;
  end

  %=== Skip comment lines and empty lines ===%
  tline = deblank(tline);
  if strncmp(tline,'#',1) == 1 || strcmp(tline,'') == 1
    continue;
  end

  %=== Save to cell array ===%
  nLines = nLines + 1;
  dataLines{nLines} = tline;
end

dataLines = dataLines';
%for dataLine = dataLines'
%  fprintf(1,'%s\n', char(dataLine));
%end

%=== Find circles ===%

%=== Find IPN annuli ===%
imatch = strmatch('IPN',dataLines);
ictr = 0;
actr = 0;
for im = imatch'
  ictr = ictr + 1;
  actr = actr + 1;
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse circle parameters ===%
  [ILABEL0(ictr),ALPHA0(ictr),DEC0(ictr),RADIUS,RADIUSERR,ETIME0(actr),ETIMEERR0(actr)] = ...
    strread(dataLines{im},'%s %f %f %f+/- %f %f+/- %f');

  RADIUSERR0(ictr)  = RADIUSERR;
  RADIUSC0(ictr)    = RADIUS;
  RADIUS0(ictr)     = RADIUS;

  TLABEL0(actr)     = ILABEL0(ictr);
  ICOLOR0{ictr}   = ANNCOLOR(actr);
end


%=== Find ecliptic latitude band ===%
imatch = strmatch('ECLIP',dataLines);
%if there is two eclip keywords, we have a band and it will be consider like an IPN circle
%we have the same alpha and dec, only the radius is different
%we then compute the mean radius and the error as max-min
radiuseclipend=-1;
radiusec=[];
errradiuseclip=-1;
for im = imatch'
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse circle parameters ===%
  [ecliplabel,alphaeclip,dececlip,radiuseclip] = ...
    strread(dataLines{im},'%s %f %f %f');
  radiusec=[radiusec, radiuseclip];
end

if size(radiusec,2)>0

if size(radiusec,2) == 2
    %two eclip keywords define a band
radiuseclipend=mean(radiusec);
errradiuseclip=(max(radiusec)-min(radiusec))/2;
else
%otherwise only one circle and we need to choose to keep north or south to
%the line
radiuseclipend=radiusec(1);

annSignal=-1;
decMinEclip = dececlip - radiuseclipend;
decMaxEclip = dececlip + radiuseclipend;
if decMinEclip < -90
   decMinEclip = -180.0 - decMinEclip;
end
if decMaxEclip > 90
   decMaxEclip = 180.0 - decMaxEclip;
end
decMinEclip
decMaxEclip

%=== If the single ecliptic circle is in the north, then so is the signal ===%
%=== Need to check this part: ===%
if ((decMaxEclip + decMinEclip) > 0)
   annSignal = 1;
end

if annSignal==1
errradiuseclip=0;
else
errradiuseclip=-1;
end 

end
end

if radiuseclipend ~= -1
  ictr = ictr + 1;
  ILABEL0(ictr) = ecliplabel;
  ALPHA0(ictr) = alphaeclip;
  DEC0(ictr) = dececlip;
  RADIUS0(ictr) = radiuseclipend;
  RADIUSERR0(ictr) = errradiuseclip
  ICOLOR0{ictr} = 'r';
end

%=== Find Earth-blocking circles ===%
imatch = strmatch('EARTH',dataLines);
for im = imatch'
  ictr = ictr + 1;
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse circle parameters ===%
  [ILABEL0(ictr),ALPHA0(ictr),DEC0(ictr),RADIUS0(ictr)] = ...
    strread(dataLines{im},'%s %f %f %f');

  RADIUSERR0(ictr)=0;

  ICOLOR0{ictr} = 'k';
end

%=== Find Mars-blocking circles ===%
imatch = strmatch('MARS',dataLines);
for im = imatch'
  ictr = ictr + 1;
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse circle parameters ===%
  [ILABEL0(ictr),ALPHA0(ictr),DEC0(ictr),RADIUS0(ictr)] = ...
    strread(dataLines{im},'%s %f %f %f');

  RADIUSERR0(ictr)=0;

%  ICOLOR0{ictr} = '--k';
  ICOLOR0{ictr} = 'k';
end

%=== Find Suzaku Earth-blocking circles ===%
imatch = strmatch('SUZAKU',dataLines);
for im = imatch'
  ictr = ictr + 1;
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse circle parameters ===%
  [ILABEL0(ictr),ALPHA0(ictr),DEC0(ictr),RADIUS0(ictr)] = ...
    strread(dataLines{im},'%s %f %f %f');

  RADIUSERR0(ictr)=0;

%  ICOLOR0{ictr} = '--k';
  ICOLOR0{ictr} = 'k';
end

%=== Find HESSI-blocking circles ===%
imatch = strmatch('HESSI',dataLines);
for im = imatch'
  ictr = ictr + 1;
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse circle parameters ===%
  [ILABEL0(ictr),ALPHA0(ictr),DEC0(ictr),RADIUS0(ictr)] = ...
    strread(dataLines{im},'%s %f %f %f');

  RADIUSERR0(ictr)=0;

%  ICOLOR0{ictr} = '--k';
  ICOLOR0{ictr} = 'k';

end

%=== Find intersection corners ===%
CORNERS0 = [];
CLABEL0  = {};
cctr = 0;
imatch = strmatch('CORNER',dataLines);
imatch = [imatch;strmatch('ECORNER',dataLines)];
imatch = [imatch;strmatch('MCORNER',dataLines)];
imatch = [imatch;strmatch('SCORNER',dataLines)];
imatch = [imatch;strmatch('HCORNER',dataLines)];
for im = imatch'
  cctr = cctr + 1;
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse corner parameters ===%
  [CLABEL0(cctr),CORNERS0(cctr,1),CORNERS0(cctr,2)] = ...
    strread(dataLines{im},'%s %f %f');
end

%=== Find Earth-crossing times ===%
imatch = strmatch('TEARTH',dataLines);
for im = imatch'
  actr = actr + 1;
  fprintf(1,'%s\n', dataLines{im});
  %=== Parse time parameters ===%
  [TLABEL0(actr),ETIME0(actr)] = strread(dataLines{im},'%s %f');

end

% ----- Done
return

