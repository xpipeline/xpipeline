function plotGrid(grbDate,ALPHA0,DEC0,RADIUS0,RADIUSERR0,ILABEL0,ICOLOR0,alphaGrid,decGrid,...
                  savePlotFlag,plotDir,plotTag)


%=== Check number of input arguments ===#
error(nargchk(9,12,nargin));

nArgs = nargin;

%== Flag if this is a circle instead of annulus ===#
circleFlag = 0;

%=== Specify step size of dec ===%
DELDEC = 0.01;

%=== Prepare figure ===%
figure('vis','off');
set(gcf,'Position', [0 0 1170 650]);
set(gcf,'color','w');

hmap = axes('box', 'on', 'Position', [0.10 0.11 0.85 0.82], 'LineWidth', 0.01, 'TickLength',[0 0],...
            'FontSize',16,'FontWeight','normal','fontname','times');
axis([0 24 -90 90]);
axes(hmap);

xticks = [0:2:24];
yticks = [-90:10:90];

set(gca,'xscale','lin','yscale','lin','xgrid','on','ygrid','on','xtick',xticks,'ytick',yticks);
hold on;

xlabel('R I G H T   A S C E N S I O N  (hours)','fontsize',16);
ylabel('D E C L I N A T I O N  (degrees)','fontsize',16);

%=== Move the x-axis label down a bit ===%
xlab = get(gca,'xlabel');
set(xlab,'position',get(xlab,'position') - [0 3.0 0]);

ylab = get(gca,'ylabel');
set(ylab,'position',get(ylab,'position') - [0.5 0.0 0]);

titleString = sprintf('E V E N T  %s', grbDate);
title(titleString,'fontsize',16,'fontname','times');


ileg = [];
ic = 0;
for ic = 1:length(ALPHA0)

  %if ic > 6
  %  return;
  %end

  %=== Initialize circle parameters ===%
  ALPHA  = ALPHA0(ic);
  DECR    = DEC0(ic);
  RADIUS = RADIUS0(ic);
  LABEL  = ILABEL0{ic};
  COLOR  = ICOLOR0{ic};
  ERRADIUS = RADIUSERR0(ic);

  if(ERRADIUS<0)
  ERRADIUS=0 
  end

  R1 = RADIUS - ERRADIUS;

  DECMIN = DECR-R1;
  DECMAX = DECR+R1;

  DEC = [DECMIN:DELDEC:DECMAX];
  [alphaLeft,alphaRight,decLeft,decRight,edgeFlag] = paveGrid(DEC,ALPHA,DECR,R1);
  alphaAll = [alphaLeft alphaRight];
  decAll   = [decLeft decRight];

  plot(alphaAll/15.0,decAll,'.','MarkerEdgeColor',COLOR,'linewidth',1,'markersize',1);
  grid on;

  R1=RADIUS + ERRADIUS;

  DECMIN = DECR-R1;
  DECMAX = DECR+R1;

  DEC = [DECMIN:DELDEC:DECMAX];
  [alphaLeft,alphaRight,decLeft,decRight,edgeFlag] = paveGrid(DEC,ALPHA,DECR,R1);
  alphaAll = [alphaLeft alphaRight];
  decAll   = [decLeft decRight];

  plot(alphaAll/15.0,decAll,'.','MarkerEdgeColor',COLOR,'linewidth',1,'markersize',1);


 % fprintf(1,'%10.6f %10.6f %10.6f %-10s %3s %2d %d\n', ALPHA, DEC, RADIUS, char(LABEL), COLOR );

  if ic > 1 && strncmp(LABEL,'IPN',3) == 1 && strcmp(LABEL,ILABEL0{ic-1}) == 1 && ...
     RADIUS0(ic-1) > 0.0
    set(get(get(plc,'Annotation'),'LegendInformation'),'IconDisplayStyle','off'); 
  else
    ileg = [ileg ic];
  end
end


LEGLABEL = ILABEL0(ileg);
legend(LEGLABEL,'location','best');

%=== Plot search grid ===%
[nrows,ncols] = size(alphaGrid);
alphaGridp=[];
decGridp=[];
if (ncols>1e5)
%too much point to display, so make a random choice in the point to plot
%=== Initialize random number generator ===%
%pause(2*runid);
rTime = sum(1000*clock);
rand('state',rTime);
r = ceil(ncols.*rand(100000,1));
alphaGridp=alphaGrid(r);
decGridp=decGrid(r);
else
alphaGridp=alphaGrid;
decGridp=decGrid;
end

  plot(alphaGridp./15.0, decGridp,...
       'd','markersize',2,'markerfacecolor','r','markeredgecolor','r');

%=== Save plot ===%
if savePlotFlag == 1
  fprintf(1,'\n');
  pngFile = sprintf('%s/grb%s_%s.png', plotDir, grbDate, plotTag);
  fprintf(1,'%s\n\n', pngFile);
  print('-dpng','-r200',pngFile);
end

%=== End plotGrid.m ===%
