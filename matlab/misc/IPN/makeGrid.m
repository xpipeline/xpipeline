function resgrid = makeGrid(grbDate,parsedpath, pointSep, writeFlag,griddir,savePlotFlag,plotdir, PlotFlagProc)
% MAKEGRID - function that will compute the search grid file for the IPN search
%
%   usage :
%
%     resgrid = makeGrid(grbDate,parsedpath, writeFlag,griddir,savePlotFlag,plotdir, PlotFlagProc)
%
%        grbDate : string with grb date ie 'yymmdd*' 
%        parsedpath : path where are stored the IPN parameters
%        pointSep : separation bewteen two points in the grid in degrees
%        writeFlag : flag to allow generation of the grid ascii files
%        griddir : output directory where the grid file will be stored
%        savePlotFlag : flag to save the plots generated (ie whole sky with IPN constraints and grid positions)
%        plotdir : directory where the plots will be saved
%        PlotFlagProc : flag to produce the plots

% I. Leonor and N. Leroy

resgrid=0;

grbDate=char(grbDate);

%=== Specify absolute path for saving plots ===%
plotDir = sprintf(griddir);

%=== Specify absolute path for saving grid files ===%
gridDir  = sprintf(plotdir);


%=== Read the IPN localization parameters ===%
[ALPHA0,DEC0,RADIUS0,RADIUSC0,RADIUSERR0,ILABEL0,ICOLOR0] = readIPN_wpath(grbDate,parsedpath);

if size(ALPHA0,1) == 0 
   disp('no info in the file, skip this grb');
   resgrid=1;
   return;
end

%=== Identify different circles ===%
iipn    = strmatch('IPN',ILABEL0);
ieclip  = strmatch('ECLIP',ILABEL0);
iearth  = strmatch('EARTH',ILABEL0);
imars   = strmatch('MARS',ILABEL0);
isuzaku = strmatch('SUZAKU',ILABEL0);
ihessi  = strmatch('HESSI',ILABEL0);
ihete   = strmatch('HETE',ILABEL0);

%=== Convert to a single row ===%
iipn0   = iipn';
iearth = iearth';
imars = imars';
isuzaku = isuzaku';
ihessi = ihessi';
ihete = ihete';

%=== Get annuli parameters ===%
ALPHAIPN0     = ALPHA0(iipn0);
DECIPN0       = DEC0(iipn0);
RADIUSIPN0    = RADIUSC0(iipn0);
RADIUSERRIPN0 = RADIUSERR0(iipn0);
nIPN          = length(ALPHAIPN0);

[RADIUSERRIPN0,isort] = sort(RADIUSERRIPN0);
RADIUSIPN0            = RADIUSIPN0(isort);
ALPHAIPN0             = ALPHAIPN0(isort);
DECIPN0               = DECIPN0(isort);

%=== Get blocking constraints ===%
iblock       = [iearth imars isuzaku ihessi ihete];
ALPHABLOCK0  = ALPHA0(iblock);
DECBLOCK0    = DEC0(iblock);
RADIUSBLOCK0 = RADIUS0(iblock);


DELRAD  = pointSep;

%=== Remove underscore from GRB name, if any ===%
unscore = [];
unscore = strfind(grbDate,'_');
if isempty(unscore) ~= 1
    grbDateMain = sscanf(grbDate,'%c',unscore-1);
else
    grbDateMain = grbDate;
end

DELARC  = DELRAD;
DELDEC  = DELRAD;
DELEDGE = DELRAD/2.0;
DELEDGE = 1.0/2.0;

%=== Specify plot tags ===%
plotTagSearch = sprintf('gridsearch_%.2fdeg', DELRAD);
plotTagSim    = sprintf('gridsim_%.2fdeg', DELRAD);

%=== Specify minimum and maximum number of simulated sky positions ===%
MINSIM = 5000;
MAXSIM = 5000;

%=== Specify minimum number of simulated sky positions per square degree ===%
MINSIMBIN = 10;

%=== Set flag which indicates successful grid creation ===%
gridFlag = 0;

alphaGrid = [];
decGrid   = [];
eBoxArea  = 0;
areaGrid  = [];
rProbGrid = [];
minProb   = 100.0;
maxProb   = 0.0;
nGrid     = 0;

gps=988000000;

%transform to Earth fix coordinates
[phi, theta] = radectoearth(ALPHA0,DEC0,gps);

if (size(RADIUSERRIPN0,2)>0)
[minIPNerr,indexIPN] = min(RADIUSERRIPN0)
%find this value in the main vector (ALPHA0, ...)
indexIPNref=find(RADIUSERR0 == minIPNerr)
else
%at least there is the ecliptic otherwise ....
indexIPNref=1;
end

%create sky positions grids
%thetarad=1.8*pi/180;
thetarad=DELRAD*pi/180;
stepangle=DELRAD/2;
coorsinus= sinusoidalMap(thetarad,pi,0,1);
coorb=coorsinus(:,1:2);

%rotate to the direction of the choosen IPN (thinnest one)

theta_g1=coorb(:,1);
phi_g1=coorb(:,2);
%put phi between -pi and pi
phi_g1 = mod(phi_g1+pi,2*pi)-pi;
%transform to unit vector to perform rotation
V1=CartesianPointingVector(phi_g1,theta_g1);
%perform rotation to have the pole to the center of the thinest circle
V_g=RotateMap(V1,pi/2+phi(indexIPNref),theta(indexIPNref),-pi/2,0);
%return to equatorial coordinates
[phi_g, theta_g] = CartesiantoSpherical(V_g);
phi_g=phi_g';
theta_g=theta_g';

size_circle=length(ALPHA0);
mask_g=true(size(V_g,1),1);

%do the scalar product
for i=1:size_circle
    %first move to cartesian coordinates
    V = CartesianPointingVector(phi(i),theta(i));

    if ( RADIUSERR0(i)>0 )
        rmin = RADIUS0(i)-RADIUSERR0(i)-stepangle;
        rmax = RADIUS0(i)+RADIUSERR0(i)+stepangle;
    else
    %only for eclip band to allow to know if we need to keep south or north to eclip line
        rmin = 0;
        rmax = RADIUS0(i)+stepangle;
    end

    if(rmin<0)
      rmin=0;
    end

%to be checked
%    if(rmax>90)
%      rmax=90;
%    end

    crmin=cosd(rmin);
    crmax=cosd(rmax);


    scalprod=[];
    scalprod = V_g*V';

    if ( ILABEL0{i}(1:3)=='IPN' )
        mask_g=mask_g & (scalprod >= crmax & scalprod <= crmin);
    else
        if ( ILABEL0{i}(1:3)=='ECL' )    
             %need to make specific choice if only one ECLIP 
             if (RADIUSERR0(i)>0)
                mask_g=mask_g & (scalprod >= crmax & scalprod <= crmin);
             else
                 RADIUSERR0(i)
                 if (RADIUSERR0(i)==0)
                     disp('here');
                    mask_g=mask_g & (scalprod >= crmax & scalprod <= crmin);
                 else
                     disp('or here')
                    mask_g=mask_g & (scalprod <= crmax);
                 end
             end

        else
        %blocking constraints
        mask_g=mask_g & (scalprod <= cosd(RADIUS0(i)));
        end
    end

end

%point to enter the search grid
phi_gok=phi_g(mask_g);
theta_gok=theta_g(mask_g);
%rotation back to ra-dec for grid and good points
[alphaAll,decAll]=earthtoradec(phi_gok,theta_gok,gps);

    %=== Calculate error box area ===%
    nAll  = length(alphaAll);
    nGrid = nGrid + nAll;

    delEdge=DELRAD./sqrt(1-V_g(mask_g,3));

singleArea=delEdge.*DELRAD;


    eBoxArea = sum(singleArea);
    areaGrid = singleArea;

rProbGrid=ones(length(alphaAll),1);

    %=== Calculate probability ===%
for i=1:length(ALPHAIPN0)
rsigma=RADIUSERRIPN0(i)/3.;
angleDiff = spaceAngle(ALPHAIPN0(i),DECIPN0(i),alphaAll,decAll);
angleDiff=round(angleDiff.*10)/10;
newprob = gaussmf(angleDiff,[rsigma,RADIUSIPN0(i)]);
rProbGrid=rProbGrid.*newprob;
end

  %=== Scale probabilities so that largest probability is unity ===%
minProb=min(rProbGrid);
maxProb=max(rProbGrid);
rProbGrid=rProbGrid./maxProb;

    alphaGrid = [alphaGrid;alphaAll];
    decGrid   = [decGrid;decAll];

  fprintf(1,'Number of grid points = %d\n', nGrid);
  fprintf(1,'       Error box area = %.7f square degrees\n\n', eBoxArea);
 
  if PlotFlagProc == 1
    plotGrid(grbDate,ALPHA0,DEC0,RADIUS0,RADIUSERR0,ILABEL0,ICOLOR0,alphaGrid,decGrid,...
           savePlotFlag,plotDir,plotTagSearch);
  end

  %=== Generate random sky positions ===%
  nThrows = 1/minProb;
  if nThrows < MINSIMBIN
    nThrows = MINSIMBIN;
  end
  nThrowsApprox = MINSIMBIN*eBoxArea;
  if nThrowsApprox < MINSIM
    nThrows = ceil(MINSIM/eBoxArea);
  end
  
  alpha1=0;
  dec1=0;
  
  if (nIPN>0)
      RMIN = RADIUSIPN0(1) - RADIUSERRIPN0(1);
      RMAX = RADIUSIPN0(1) + RADIUSERRIPN0(1);
      alpha1=ALPHAIPN0(1);
      dec1=DECIPN0(1);
  else
     %only ecliptic
        alpha1=ALPHA0(1);
        dec1=DEC0(1);
     if (RADIUSERR0(1)>0)         
       %two ecliptic
       RMIN = RADIUS0(1) - RADIUSERR0(1);
       RMAX = RADIUS0(1) + RADIUSERR0(1);
     else
       %one ecliptic
        RMIN=0;
        RMAX=RADIUS0(1);
     end
  end

  %=== Check the case of a circle instead of an annulus ===%
  if RMIN < 0
    RMIN = 0.0;
  end

  [alphaThrow,decThrow] = throwSky(nThrows,alpha1,dec1,alphaGrid,decGrid,rProbGrid,...
                                     DELRAD,RMIN,RMAX);

  %=== Write output grid files ===%
  if writeFlag == 1
    gridFile    = sprintf('%s/grb%s_gridsearch_weight_%.2fdeg.txt', gridDir, grbDate, DELRAD);
    gridSimFile = sprintf('%s/grb%s_gridsim_weight_%.2fdeg.txt', gridDir, grbDate, DELRAD);
    writeGrid(gridFile,alphaGrid,decGrid,rProbGrid,areaGrid);
    writeGrid(gridSimFile,alphaThrow,decThrow);
  end


  [trows,tcols] = size(alphaThrow);
  %=== Calculate total number of simulated positions ===%
  nSim = tcols;

  fprintf(1,'Total number of throws per square degree:  %d\n', nThrows);
  fprintf(1,'     Total number of simulated positions:  %d\n\n', nSim);

  %=== Plot simulated sky positions ===%
 if PlotFlagProc == 1
  plotGrid(grbDate,ALPHA0,DEC0,RADIUS0,RADIUSERR0,ILABEL0,ICOLOR0,alphaThrow,decThrow,...
           savePlotFlag,plotDir,plotTagSim);
 end

% ---- Done
return

