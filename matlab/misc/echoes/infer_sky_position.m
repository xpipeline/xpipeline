

% ---- Compute delay that maximises cross-correlation.
Y = innerproduct(detectorData{1},detectorData{2},tf.fs,ASDnoise{1}(:,1).*ASDnoise{1}(:,2),FSpecCond(1),FSpecCond(2)-FSpecCond(1),tf.fmin,tf.fmax);
[valY,indY] = max(abs(Y));
delay = [(length(detectorData{1}) - indY + 1)/tf.fs 0];
% ---- Check:
Ym = innerproduct(detectorData{1},circshift(detectorData{2},-(length(detectorData{1}) - indY + 1)),tf.fs,ASDnoise{1}(:,1).*ASDnoise{1}(:,2),FSpecCond(1),FSpecCond(2)-FSpecCond(1),tf.fmin,tf.fmax);
[valYm,indYm] = max(abs(Ym));
if indYm~=1
error('Time shift messed up.');
end
% ---- Compute sky positions.
ifos = network.detector;
ifos{3} = 'V';
[theta_zp,phi_zp,theta_zm,phi_zm] = xtimedelaytoskyposition(delay,ifos,0);
% ---- Sanity check: delays.
D = computeTimeShifts(network.detector, [theta_zp, phi_zp; theta_zm, phi_zm])
if abs(D(1,2)-D(1,1)-delay(1))>1e-5
error('Delay messed up.');
end
% ---- Select sky position with largest antenna response.
[Fp, Fc] = antennaPatterns(network.detector, [theta_zp, phi_zp; theta_zm, phi_zm]);
Fss = sum(Fp.^2+Fc.^2,2);
if Fss(1)>Fss(2)
theta = theta_zp;
phi   = phi_zp;
else
theta = theta_zm;
phi   = phi_zm;
end
% ---- Convert to ra,dec.
[ra, dec] = earthtoradec(phi,theta,analysis.triggerTime)
ifos


