function [skyPositions, pixel, distance] = minimalgrid(dt, detectorSet)
% minimalgrid:  Return the (approximately) smallest set of sky positions 
% ("pixels") such that the maximum timing difference for any detector pair
% between any point on the sky and the nearest pixel is less than or equal 
% to a specified number.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WARNING:  This works well for 3 detectors.  For 5 detectors sinusoidalMap
% gives better performance for a fixed number of pixels.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  [skyPositions, pixel] = minimalgrid(dt, detectorSet);
%
%  dt           Scalar.  Approximate maximum time difference between
%               pixels. 
%  detectorSet  String or cell array of strings.  If string, it holds the 
%               first letter of the name of each detector in the network.  
%               If a cell array, the detector names are inferred from the
%               first letter of each cell element (i.e., channel name). 
%
%  skyPositions Array of sky positions.  The format is the same as for
%               sinusoidalMap.  The first column is polar angle theta, 
%               second is azimuthal angle phi, third is area represented by
%               the pixel, fourth is fraction of sky represented by the 
%               pixel (third column = 4*pi*fourth column).
%  pixel        Array of unit vector pointing directions (x,y,z) in
%               Earth-based Cartesian coordinates for the pixels in 
%               skyPositions.  
% distance      Maximum timing error along any baseline between the 
%               returned pixel and desired time delays that this function
%               aims for internally.  (Intended for debugging only.)
%
% The maximum timing error for any point on the sky (difference with
% closest pixel) is about 0.5*dt.  Typical timing errors are about 0.25*dt.
% 
% See also sinusoidalMap, minimaltimedelaygrid.
%
% -- Patrick J. Sutton
%
% $Id$


%----- Useful constants.
c = 299792458;

%----- Check to see if detectors are specified by string or by a cell array
%      of channel names.
if (iscell(detectorSet))
    for jChan=1:length(detectorSet)
        dS(1,jChan) = detectorSet{jChan}(1);
    end
    detectorSet = dS;
end

%----- Load detector info.
detectorVertex = [];
for jdetector=1:length(detectorSet);
    detector = LoadDetectorData(detectorSet(jdetector));
    detectorVertex = [detectorVertex, detector.V];
end

%----- Make array of baselines.
baseline = [];
for idetector=1:size(detectorVertex,2)-1
    for jdetector=idetector+1:size(detectorVertex,2)
        %----- baseline vectors in metres
        baseline = [baseline, ... 
            detectorVertex(:,idetector) - detectorVertex(:,jdetector)];
    end
end
%----- Convert baselines to light-seconds.
baseline = baseline/c;
%----- Compute unit vectors along baselines.
baseline_length = sum(baseline.^2,1).^(0.5);
% baseline_unit = baseline*diag( sum(baseline.^2,1).^(-0.5) );
%----- Number of baselines.
nBaseline = size(baseline,2);

%----- Spacing (rad) in initial grid.  Make it 1/10th of the smallest
%      angular scale that can be emasured by the network.
minScale = 0.0035;  % gives sinusoidal map with 1e6 points, using 32MB of memory 
scale = max(0.1*dt/max(baseline_length),minScale);
%----- Use sinusoidalMap function to get approximately equal-area grid.
skyPositions = sinusoidalMap(scale);

%----- Cartesian vector of pointing directions of initial grid.
omega = [sin(skyPositions(:,1)).*cos(skyPositions(:,2)), ...
    sin(skyPositions(:,1)).*sin(skyPositions(:,2)), ...
    cos(skyPositions(:,1))];
%----- Delays against each baseline.
delay = omega*baseline;
%----- Min and max (signed) delays against each baseline.
%      Put in array with identical rows so can use .*, etc.
delay_min = zeros(size(delay));
for iBaseline=1:nBaseline
    delay_min(:,iBaseline) = min(delay(:,iBaseline),[],1);
end
%----- Imagine set of hypercubes of side length dt filling Cartesian delay
%      space.  Set up grid such that the minimum time delay for each 
%      baseline over the entire sky occurs on a cell boundary. Then can use
%      mod(delay-delay_min,dt) to figure out which cell any sky coordinate
%      belongs in, and how far from the cell boundaries it is.
%      Use as "template" the pixel closest to the center of each hypercube.
%----- Time delay of each point from lower-delay-sides of cell. 
delay_mod = mod(delay-delay_min,dt);
%----- Center of grid cell is a 0.5*dt away from each edge.
%      Distance of each point from center.
distance = max(abs(delay_mod-0.5*dt),[],2);  
%----- Integer number of cells away from delay=0 boundary.
delay_disc = round((delay-delay_min-delay_mod)/dt);
%----- Sort points first by cell then in decreasing distance from cell
%      center.  (Use decreasing distance because 'unique' operation keeps
%      last of repeated rows.)
[Z, IZ] = sortrows([delay_disc,distance],[1:nBaseline,-(nBaseline+1)]);
%----- Keep only a single point in each cell - the last one from sortrows.
[Y, IY] = unique(Z(:,1:nBaseline),'rows');
%----- Index into original arrays of points to be kept.
Ikeep = IZ(IY);
%----- Keep desired points from original high-resolution sky map.
skyPositions = skyPositions(Ikeep,1:2);
skyPositions(:,3) = [IY(1); diff(IY)];  % number of high-res points grouped into this pixel
skyPositions(:,3) = 4*pi/sum(skyPositions(:,3))*skyPositions(:,3);  % area in this pixel
skyPositions(:,4) = 4*pi./skyPositions(:,3);  % fraction of sky in this pixel 
%----- Unit pointing vectors for kept points.
pixel = omega(Ikeep,:);  
%----- Distance of kept points from cell centers.
distance = distance(Ikeep);  

%----- Done
return
