function [eff, ampl] = efficiencyfromfigfile(figFile,Nscales)
% EFFICIENCYFROMFIGFILE - extract efficiency vs. amplitude from x-pipeline fig
%
% usage:
%
%   [eff, ampl] = efficiencyfromfigfile(figFile,Nscales)
%
%  figFile	String. Name of .fig file to be read.
%  Nscales  Natural number. Number of distinct amplitudes at which the
%           efficiency is sampled in the .fig file.
%
%  eff      Vector. Efficiency at each of the corresponding injection
%           amplitudes.
%  ampl     Vector. Amplitude at which each of the corresponding efficiencies is
%           computed. Typically this is the RSS amplitude of the injections.
%
% If the file contains multiple efficiency curves then the lowest average
% efficiency is returned (typically post DQ vetoes and coherent vetoes).
%
% $Id$

% ---- Open the figure file, extract all data, and close the figure file.
fig = open(figFile);
[X,Y] = datafromplot(gca);
close(fig);

% ---- Initialise storage for the outputs.
eff = ones(Nscales,1);
ampl = [];

% ---- Parse the data to find the correct (lowest) efficiency curve.
for jj=1:length(Y)
    if length(Y{jj})==Nscales & sum(Y{jj})<sum(eff)
        eff  = Y{jj};
        ampl = X{jj};
    end
end

% ---- If ampl = [] then no efficiecy curves were found - exit with error.
if isempty(ampl)
    error(['No efficiency curves of length ' num2str(Nscales) ' found in ' figFile '.'])
end

% ---- Sometimes efficiency can be slightly below zero (~-eps). Correct.
eff(eff<0) = 0;

% ---- Sort in ascending amplitude order.
[ampl,idx] = sort(ampl);
eff = eff(idx);

% ---- Done.
return
