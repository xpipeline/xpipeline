function [likelihood_signal, likelihood_noise, gps, peakTime, ... 
    offsetSeconds, rho2p, rho2c, phi, theta, psi, epsilon] = ...
    predictlikelihood(injectionFileName,injectionScale, ...
    analysisTime,offsetFraction,referenceDetector,Fmin,Fmax, ...
    windowType,sampleFrequency,detectors,whiteNoise)
%
% predictlikelihood - Reads a GWB injection file and predicts the signal
% likelihoods (standard, hard constraint, etc.) that should be measured at 
% the source sky location for a specified network.  The design detector 
% noise spectra are used, as determined by the function SRD.
%
% [likelihood_signal, likelihood_noise, gps, peakTime, ... 
%     offsetSeconds, rho2p, rho2c, phi, theta, psi, epsilon] = ...
%     predictlikelihood(injectionFileName,injectionScale, ...
%     analysisTime,offsetFraction,referenceDetector,Fmin,Fmax, ...
%     windowType,sampleFrequency,detectors,whiteNoise)
%
%    injectionFileName  String.  Name of injection parameter file.
%    injectionScale     Scalar.  Additional amplitude scaling factor
%                       applied to injections.
%    analysisTime       Scalar.  Integration length (seconds) of likelihood
%                       analysis.
%    offsetFraction     Scalar.  Fraction of analysisTime by which
%                       consecutive integration data segments are offset 
%                       (i.e., determines overlapping).
%    referenceDetector  String.  Detector used as reference location.
%                       E.g.: 'H'.
%    Fmin               Scalar.  Minimum frequency (Hz) included in
%                       likelihood calculation.
%    Fmax               Scalar.  Maximum frequency (Hz) included in
%                       likelihood calculation.
%    windowType         String.  Window type used in Fourier transforms.
%    sampleFrequency    Scalar.  Sampling rate of data.
%    detectors          Cell row vector.  Names of detectors in the
%                       network.  Recognized values are
%                       'G','G1','H','H1','H2','L','L1','V','V1'.  Use
%                       'Gfake' or 'Vfake' to model the GEO or Virgo
%                       detectors with the LIGO SRD noise spectrum. 
%    whiteNoise         Optional scalar.  If nonzero use spectra for 
%                       unit-variance white noise rather than design noise.
%
%    likelihood_signal  Array of size Nx5, where N is the number of 
%                       injections specified in injectionFileName.  Each 
%                       holds the signal contribution to the following 
%                       likelihoods: standard, soft constraint, hard
%                       constraint, elliptic, total energy (in that order).
%                       Nnote that the elliptic likelihood is computed
%                       assuming the GWB is indeed elliptically polarized.
%    likelihood_noise   Array of the same size as likelihood_signal.  Holds 
%                       the mean noise contribution to each likelihood,
%                       computed assuming chi-squared statistics. 
%    gps                Vector.  Injection time at center of Earth (sec).
%    peakTime           Vector.  Non-integer part of injection time at the
%                       reference detector (sec).
%    offsetSeconds      Vector.  Difference: peakTime - nearest bin center.
%    rho2p              SNR^2 of the plus waveform in the GWB source frame 
%                       relative to the noise power spectrum of each
%                       detector, for optimal orientation.
%    rho2c              SNR^2 of the plus waveform in the GWB source frame 
%                       relative to the noise power spectrum of each
%                       detector, for optimal orientation.
%    phi                Vector.  Azimuthal sky position angle (radians).
%    theta              Vector.  Polar sky position angle (radians).
%    psi                Vector.  Polarization angle (radians).
%    epsilon            Vector.  Ratio |\vec{F}_x|^2 / |\vec{F}_+|^2 in the
%                       dominant polarization frame at the injection sky
%                       position.
%   
% The total expected likelihood is the sum of the likelihood_signal and
% likelihood_noise contributions.  The signal contribution to the
% likelihood and to rho2p, rho2c includes the effects of data windowing,
% finite integration time, and the offset of the peak time of the injection
% from the center of the window.
%
% Sky position and polarization are in Earth-based coordinates.
%
% Caveats: 
% --------
%
% 1) The hard and soft constraint likelihoods, which use the dominant
% polarization (DP) frame, are calculated assuming that the DP frame is
% independent of frequency.  (This calculation is done in networkSNR2.)
% This is true only if the detector spectra have the same shape across the
% frequency band of the signal; i.e., for networks consisting of LIGO and
% 'Gfake', 'Vfake' detectors.
%
% 2) The per-detector SNRs rho2p, rho2c are computed assuming the plus and
% cross waveforms are othrogonal.  However, the likelihoods are valid 
% independent of this assumption.  (Summing the SNRs over the network
% cancels out the h_+ * h_c terms due to the orthogonality of F_+, F_x in
% the dominant polarization frame.
%
% 3) The noise contribution for elliptic is an underestimate. There is a
% trials factor for the multiple polarizations and amplitude ratios tested.
% I do not know of any good analytic estimate for this factor.
% 
% 4) This function assumes that all integer GPS boundaries are boundaries  
% of an analysis segment.
%
% This function calls networkSNR2.m
% 
% initial write Patrick J. Sutton 2006.07.11
%
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Preliminaries.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Assign default values to optional arguments.
if (nargin < 11)
    whiteNoise = 0;
end

% ---- Useful constants.
speedOfLight = 299792458;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Prepare window.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Number of data samples in time domain.
analysisLength = analysisTime*sampleFrequency;

% ---- Construct window and record mean-square value.
eval(['windowData = ' windowType '(' num2str(analysisLength) ');']);
% ---- Mean-square value of window function.
windowMS = mean(windowData.^2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Load data on reference detector.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Load data on reference detector.
referenceDetectorData = LoadDetectorData(referenceDetector);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Read injection file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

startTime = 0;
duration = 1e10;
% startTime = 700000000;
% duration = 500;
injectionParameters = readinjectionfile(startTime,duration, ...
    injectionFileName);
numberOfEvents = size(injectionParameters,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Noise spectra.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Frequencies sampled.
dF = 1/analysisTime;
F = [0:dF:Fmax]';
k = find(F>=Fmin);
F = F(k);
numberOfFrequencyBins = length(F);

% ---- Number of detectors.
numberOfDetectors = length(detectors);

% ---- Later in computing DP frame, treat all detectors as equal by default.
sigma = ones(1,numberOfDetectors); 
% ---- Design noise spectra.
powerSpectrum = zeros(numberOfFrequencyBins,numberOfDetectors);
for jDetector = 1:numberOfDetectors
    if (whiteNoise)
        powerSpectrum(:,jDetector) = 2/sampleFrequency * ones(length(F),1);
    else
        switch detectors{jDetector}
            case {'G','G1'}
                powerSpectrum(:,jDetector) = SRD('GEO',F);
            case {'Gfake','G1fake'}
                powerSpectrum(:,jDetector) = SRD('LIGO',F);
                detectors{jDetector} = 'G';
            case {'H', 'H1', 'L', 'L1'}
                powerSpectrum(:,jDetector) = SRD('LIGO',F);
            case 'H2'
                powerSpectrum(:,jDetector) = 4*SRD('LIGO',F);
                sigma(jDetector) = 2;
            case {'V', 'V1'}
                powerSpectrum(:,jDetector) = SRD('VIRGO',F);
            case {'Vfake','V1fake'}
                powerSpectrum(:,jDetector) = SRD('LIGO',F);
                detectors{jDetector} = 'V';
            otherwise
                error(['Detector ' detectors{jDetector} ' not recognized.']);
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Compute expected SNR in each detector for each injection.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Storage for results
peakTime = zeros(numberOfEvents,1);
offsetSeconds = zeros(numberOfEvents,1);
rho2p = zeros(numberOfEvents,numberOfDetectors);
rho2c = zeros(numberOfEvents,numberOfDetectors);
phi = zeros(numberOfEvents,1);
theta = zeros(numberOfEvents,1);
psi = zeros(numberOfEvents,1);
gps = zeros(numberOfEvents,1);

% ---- Loop over injections
for eventNumber=1:numberOfEvents

    if (mod(eventNumber,1000)==0)
        disp(['Processing eventNumber ' num2str(eventNumber)])
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Determine peak time of injection in time-frequency map.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ---- Get precise time of this injection.
    [gps_s injectionParameters{eventNumber}] = strtok(injectionParameters{eventNumber});
    [gps_ns injectionParameters{eventNumber}] = strtok(injectionParameters{eventNumber});
    [phi_string injectionParameters{eventNumber}] = strtok(injectionParameters{eventNumber});
    [theta_string injectionParameters{eventNumber}] = strtok(injectionParameters{eventNumber});
    [psi_string injectionParameters{eventNumber}] = strtok(injectionParameters{eventNumber});
    [GWB_name injectionParameters{eventNumber}] = strtok(injectionParameters{eventNumber});
    [GWB_params injectionParameters{eventNumber}] = strtok(injectionParameters{eventNumber});
    gps_s = str2num(gps_s);
    gps_ns = str2num(gps_ns);
    phi(eventNumber) = str2num(phi_string);
    theta(eventNumber) = str2num(theta_string);
    psi(eventNumber) = str2num(psi_string);
    gps(eventNumber) = gps_s+1e-9*gps_ns;

    % ---- Need to compute peak time of injection at reference detector.
    % ---- Pointing vector to source position.
    sourceOmega = CartesianPointingVector(phi(eventNumber),theta(eventNumber));
    delay = - sourceOmega * referenceDetectorData.V  / speedOfLight;

    % ---- Non-integer part of peak time. 
    peakTime(eventNumber) = gps_ns*1e-9 + delay;
    if (peakTime(eventNumber)<0)
        peakTime(eventNumber) = peakTime(eventNumber)+1;
    elseif (peakTime(eventNumber)>1)
        peakTime(eventNumber) = peakTime(eventNumber)-1;
    end

    % ---- Non-integer times of bin centers.  KLUDGE: Only works for
    %      analysisTime < 2 seconds!
    binsPerSecond = 1/(analysisTime*offsetFraction);
    binTime = [-binsPerSecond:binsPerSecond]*analysisTime*offsetFraction ...
        + analysisTime/2;
    k = find(binTime>=0 & binTime<=1);
    binTime = binTime(k);

    % ---- Find bin with center closest to peak time.
    [absOffsetSeconds bin] = min(abs(binTime-peakTime(eventNumber)));
    % ---- Delay from bin center to peak time.
    offsetSeconds(eventNumber) = peakTime(eventNumber) - binTime(bin);

    % ---- Make waveform.
    [t,hp,hc] = xmakewaveform(GWB_name,GWB_params,analysisTime, ...
        analysisTime/2+offsetSeconds(eventNumber),sampleFrequency);  

    % ---- Compute SNR for each detector.
    %      Note: Assume that hc, hp orthogonal for desired noise spectrum. 
    %      Note: OptimalSNR outputs AMPLITUDE SNR.  Must square this (later). 
    for jDetector = 1:numberOfDetectors
        if (max(abs(hp))>0)
            rho2p(eventNumber,jDetector) = OptimalSNR(windowData.*hp,0, ...
                sampleFrequency,powerSpectrum(:,jDetector),F(1),dF,F(1),F(end));
        end
        if (max(abs(hc))>0)
            rho2c(eventNumber,jDetector) = OptimalSNR(windowData.*hc,0, ...
                sampleFrequency,powerSpectrum(:,jDetector),F(1),dF,F(1),F(end));
        end
    end

end

% ---- Rescale SNRs according to injectionScale, convert from amplitude ->
% power SNR.
rho2p = injectionScale.^2 * rho2p.^2;
rho2c = injectionScale.^2 * rho2c.^2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Predict "on source" likelihoods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Compute sum-squared SNRs of injections.
[sumRho2 sumRho2pDP sumRho2cDP epsilon] = networkSNR2(phi,theta,psi, ...
    rho2p,rho2c,detectors,sigma);

% ---- Signal contribution to each likelihood type.
%      The 1/2 factor is because xdetection sums over positive frequencies
%      only.
likelihood_elliptic = 1/2 * sumRho2pDP + 1/2 * sumRho2cDP;
likelihood_hard     = 1/2 * sumRho2pDP;
likelihood_soft     = 1/2 * sumRho2pDP + 1/2 * epsilon .* sumRho2cDP ;
likelihood_standard = 1/2 * sumRho2pDP + 1/2 * sumRho2cDP;
likelihood_total    = 1/2 * sumRho2pDP + 1/2 * sumRho2cDP;

% ---- Pack up results.
likelihood_signal = [ likelihood_standard, likelihood_soft, likelihood_hard, ...
    likelihood_elliptic, likelihood_total];

% ---- Add mean noise contribution to each likelihood type. 
%      Note: The noise contribution for elliptic is an underestimate.  
%      There is a trials factor for the multiple polarizations and
%      amplitude ratios tested.  I do not know of any good analytic
%      estimate for this factor.
noise_elliptic = numberOfFrequencyBins*windowMS * ones(numberOfEvents,1);  
noise_hard = numberOfFrequencyBins*windowMS * ones(numberOfEvents,1);
noise_soft = (1+epsilon)*numberOfFrequencyBins*windowMS;
noise_standard = 2*numberOfFrequencyBins*windowMS * ones(numberOfEvents,1);
noise_total = numberOfDetectors*numberOfFrequencyBins*windowMS * ones(numberOfEvents,1);

% ---- Pack up results.
likelihood_noise = [ noise_standard, noise_soft, noise_hard, ...
    noise_elliptic, noise_total];

% ---- Done.
return

