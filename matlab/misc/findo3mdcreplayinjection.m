xgps = 1394838005.735;
% ----------------------------------
% see https://wiki.ligo.org/Computing/DASWG/O3EndToEndReplay

% ---- Determine time shift and modified GPS time to compare to injection log.
pass1Start = 1301856000;
pass1Shift =   39552000;
passDur    =    3456000;
passNumber = ceil((gps - pass1Start)/passDur);
timeShift = pass1Shift + (passNumber-1)*passDur;
gps0 = gps - timeShift;

% ---- Identify corresponding log entry.
idx = find(abs(logData.time-gps0)<1);

if length(idx)==1

    % ---- Determine injection location in Earth-fixed coordinates.
    ra_deg  = logData.right_ascension(idx) * 180/pi;
    dec_deg = logData.declination(idx) * 180/pi;
    [phi, theta] = radectoearth(ra_deg,dec_deg,gps0);
    
    % ---- Convert to (ra,dec) at original trigger time.
    [ra, dec] = earthtoradec(phi,theta,gps);
    
    disp('-------------------------------------------------------------------')
    disp(['           Trigger GPS: ' num2str(gps)])
    disp(['  Modified Trigger GPS: ' num2str(gps0)])
    disp(['               Log GPS: ' num2str(logData.time(idx))])
    disp(['Log GPS - Mod Trig GPS: ' num2str(logData.time(idx)-gps0)])
    disp(['               Log SNR: ' num2str(logData.snr_net(idx))])
    disp(['                Log RA: ' num2str(logData.right_ascension(idx))])
    disp(['               Log Dec: ' num2str(logData.declination(idx))])
    disp(['               Log phi: ' num2str(phi)])
    disp(['             Log theta: ' num2str(theta)])
    disp(['          Effective RA: ' num2str(ra) ' (' num2str(ra/15) 'h)'])
    disp(['         Effective Dec: ' num2str(dec)])
    disp('-------------------------------------------------------------------')

else

    error([num2str(length(idx)) 'log entry(ies) found'])

end