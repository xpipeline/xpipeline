function [rankCorrelationMap, skyPositions] = ...
    xcorrelate(channelNames, conditionedData, sampleFrequency, ...
            skyPositions, elementLength, ...
            transientLength, frequencyBands, verboseFlag)
% XCORRELATE Fieller rank correlation over the sky
% 
% [rankCorrelationMap, skyPositions] = ...
%    xcorrelate(channelNames, conditionedData, sampleFrequency, ...
%            skyPositions, elementLength, ...
%            transientLength, frequencyBands, verboseFlag)
%
%    channelNames       cell array of channel name strings
%    conditionedData    matrix time domain conditioned data
%    sampleFrequency    sampling frequency of conditioned data [Hz]
%    skyPositions       matrix of sky positions [radians] (in and out)
%    elementLength      transform length of fourier analyses [samples]
%    transientLength    duration of conditioning transients [samples]
%    frequencyBands     matrix of frequency bands to search over [Hz]
%    verboseFlag        boolean flag to control status output (default 0)
% 
%    rankCorrelationMap resulting sky map array (see below)
%
% See also XPIPELINE, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, XMAPSKY and
% XINTERPRET.
%
% See also ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        process command line arguments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
error(nargchk(7, 8, nargin));

% default arguments
if nargin == 7,
  verboseFlag = 0;
end

% force one dimensional cell array
channelNames = channelNames(:);

% number of detectors
numberOfChannels = length(channelNames);

% validate number of detectors
if numberOfChannels < 3,
  error('data is required from three or more non-aligned detectors');
end

% validate conditioned data
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% block length in samples
blockLength = size(conditionedData, 1);

% nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% validate frequency bands
if size(frequencyBands, 2) ~= 2,
  error('frequency bands must be a two column matrix');
end
if max(max(frequencyBands)) > nyquistFrequency,
  error('requested frequency bands exceed nyquist frequency');
end
if any(diff(frequencyBands, 1, 2) <= 0),
  error('frequency bands have negative bandwidth');
end

% number of frequency bands to consider
numberOfFrequencyBands = size(frequencyBands, 1);

% validate integer power of two element length
if bitand(elementLength,elementLength-1),
  error('element length is not an integer power of two');
end

% determine half element lengths
halfElementLength = elementLength / 2 + 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  partition block into overlapping elements                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% calculate the indices of the endpoints of the elements of length
% elementLength with 50% overlap
elementIndices = chunkIndices(blockLength, elementLength, transientLength);

% number of elements
numberOfElements = size(elementIndices, 1);

% one-sided frequency vector for elements
elementFrequencies = nyquistFrequency * (0 : halfElementLength - 1) / ...
                     (halfElementLength - 1);

% frequency band indices into one-sided frequency domain element
frequencyIndices = cell(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  frequencyIndices{frequencyBandNumber} = ...
      find((elementFrequencies >= frequencyBands(frequencyBandNumber, 1))& ...
           (elementFrequencies < frequencyBands(frequencyBandNumber, 2)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   basic geometry of detectors                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

detector = cell(numberOfChannels, 1);

for detectorNumber = 1 : numberOfChannels
    detector{detectorNumber} = LoadDetectorData(channelNames{detectorNumber}(1));
end

speedOfLight = 299792458; % m/s

baseline = zeros(3, numberOfChannels);
delays = cell(numberOfChannels,1);

for channelNumber = 1:numberOfChannels
    baseline(:, channelNumber) = detector{channelNumber}.V - detector{1}.V;
    maxDelay = norm(baseline(:,channelNumber)) / speedOfLight * sampleFrequency;
    delays{channelNumber} = round(-maxDelay):round(maxDelay);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 make the sky map (3 detectors only)                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta = [];
phi = [];

% if (numberOfChannels == 3)
%     perpendicular = cross(baseline(:,2), baseline(:,3));
%     perpendicular = perpendicular / norm(perpendicular);
%     plane = inv([baseline(:,2)'; baseline(:,3)'; perpendicular'] / (speedOfLight ^ 2));
%     plane = plane(:, 1:2);
% 
%     for delay12 = delays{2}
%         fprintf(1, 'delay12 = %d\n', delay12);
%         for delay13 = delays{3}
%             velocity = plane * [ delay12 ; delay13 ] / sampleFrequency;
%             if (norm(velocity) < speedOfLight)
%                 v1 = velocity - perpendicular * sqrt((speedOfLight^2) - (norm(velocity)^2));
%                 v2 = velocity + perpendicular * sqrt((speedOfLight^2) - (norm(velocity)^2));
%                 theta = [theta ; atan2(sqrt(v1(1)^2 + v1(2)^2), v1(3)) ; atan2(sqrt(v2(1)^2+v2(2)^2), v2(3)) ];
%                 phi = [ phi ; atan2(v1(2), v1(1)) ; atan2(v2(2), v2(1)) ];
%             end
%         end
%     end
% 
%     skyPositions = [ theta , phi ];
% end

% validate sky positions
if size(skyPositions, 2) < 2,
  error('sky positions must be at least a two column matrix');
end
if any(skyPositions(:, 1) < 0 | skyPositions(:, 1) > pi),
  error('theta outside of [0, pi]');
end
if any(skyPositions(:, 2) < -pi | skyPositions(:, 2) >= pi),
  error('phi outside of [-pi, pi)');
end

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         begin loop over time delays                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for skyPositionNumber = 1 : numberOfSkyPositions
    timeShifts = computeTimeShifts(channelNames, skyPositions(skyPositionNumber, :));
    timeShifts = timeShifts(:).' - timeShifts(1);
    timeShiftLengths = timeShifts * sampleFrequency;
    integerTimeShiftLengths = round(timeShiftLengths);
    indexDelay(:, skyPositionNumber) = integerTimeShiftLengths + 1;
end

for channelNumber = 1 : numberOfChannels
    indexDelay(channelNumber, :) = indexDelay(channelNumber, :) - delays{channelNumber}(1);
end

% initialized result structure
rankCorrelationMap = zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);

dataFrequencies = sampleFrequency * (0 : (length(conditionedData) - 1)) / (length(conditionedData) - 1);
dataFrequencies = repmat(dataFrequencies',1,numberOfChannels);

samples = 10000;
newDistribution = sum(sort(randn(elementLength, samples), 1), 2) / samples; % normalisation?

threshold = chi2inv(1 - 1 / (numberOfSkyPositions * numberOfElements * numberOfFrequencyBands), numberOfChannels)

for frequencyBandNumber = 1 : numberOfFrequencyBands

    fprintf(1, 'frequency band %d of %d\n', frequencyBandNumber, numberOfFrequencyBands);

    conditionedDataF = fft(conditionedData);
    conditionedDataF(find((dataFrequencies < frequencyBands(frequencyBandNumber, 1)) | (dataFrequencies >= frequencyBands(frequencyBandNumber, 2)))) = 0;
    bandData = ifft(conditionedDataF, 'symmetric');
    bandFraction = (frequencyBands(frequencyBandNumber, 2) - frequencyBands(frequencyBandNumber, 1)) / nyquistFrequency;
    
    for elementNumber = 1 : numberOfElements
        
        fprintf(1, '  element %d of %d\n', elementNumber, numberOfElements);
        
        thisElement = (elementIndices(elementNumber, 1)):(elementIndices(elementNumber, 2));
        n = length(thisElement);
                
        % precompute transformed data
        
        redistributed = cell(numberOfChannels, 1);               
        
        for channelNumber = 1 : numberOfChannels
            redistributed{channelNumber} = zeros(n, length(delays{channelNumber}));
            for delayNumber = 1 : length(delays{channelNumber})
                [ignore, s] = sort(bandData(thisElement+delays{channelNumber}(delayNumber),channelNumber));
                s(s) = newDistribution;
                redistributed{channelNumber}(:, delayNumber) = s;
            end
        end
        
        % precompute pairwise correlations
        
        correlations = cell(numberOfChannels, numberOfChannels);
        
        for channelNumberA = 1 : (numberOfChannels - 1)
            for channelNumberB = (channelNumberA + 1) : numberOfChannels
                correlations{channelNumberA, channelNumberB} = zeros(length(delays{channelNumberA}), length(delays{channelNumberB}));
                for delayNumberA = 1 : length(delays{channelNumberA})
                    x = redistributed{channelNumberA}(:, delayNumberA);
                    for delayNumberB = 1 : length(delays{channelNumberB})
                        y = redistributed{channelNumberB}(:, delayNumberB);
                        correlations{channelNumberA, channelNumberB}(delayNumberA, delayNumberB) = sum(x .* y) * bandFraction / sqrt(n * bandFraction);
                    end
                end
            end
        end
        
        % lookup correlations for sky positions
        
        for skyPositionNumber = 1 : numberOfSkyPositions
            correlation = 0;
            for channelNumberA = 1 : (numberOfChannels - 1)
                delayNumberA = indexDelay(channelNumberA, skyPositionNumber);
                for channelNumberB = (channelNumberA + 1) : numberOfChannels
                    correlation = correlation + correlations{channelNumberA, channelNumberB}(indexDelay(channelNumberA, skyPositionNumber), indexDelay(channelNumberB, skyPositionNumber)) ^ 2;
                end
            end
            rankCorrelationMap(frequencyBandNumber, elementNumber, skyPositionNumber) = correlation;
        end
    end
end

% cull the sky map so null stream is only calculated for those directions
% that exceed a threshold at some time in some frequency band

candidateDirections = find(reshape(max(max(rankCorrelationMap, [], 1), [], 2), skyPositionNumber, 1) > threshold);
skyPositions = skyPositions(candidateDirections, :);
rankCorrelationMap = rankCorrelationMap(:,:,candidateDirections);
