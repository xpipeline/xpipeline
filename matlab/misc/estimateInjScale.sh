#!/bin/bash

# $Id$

# prevent core dumps
ulimit -c 0

# tell us where we are
hostname
# XPIPELINE_MATLAB_ROOT=/ldcg/matlab_r2006a
# set environment to locate Matlab run time libraries
BIT=`uname -p`
if [[ $BIT == "x86_64" ]]; then
LD_LIBRARY_PATH=${XPIPELINE_MATLAB_ROOT}/bin/glnxa64:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=${XPIPELINE_MATLAB_ROOT}/extern/lib/glnxa64:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=${XPIPELINE_MATLAB_ROOT}/sys/os/glnxa64:${LD_LIBRARY_PATH}
echo "loaded 64-bit libraries"
else
LD_LIBRARY_PATH=${XPIPELINE_MATLAB_ROOT}/bin/glnx86:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=${XPIPELINE_MATLAB_ROOT}/extern/lib/glnx86:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=${XPIPELINE_MATLAB_ROOT}/sys/os/glnx86:${LD_LIBRARY_PATH}
echo "loaded 32-bit libraries"
fi
export LD_LIBRARY_PATH

# run the executable
#HOSTTYPE=`hostname | grep node`
#if [[ -n $HOSTTYPE ]]; then
#echo "tmp directory on node"
#uniquedir=/data/`hostname`/`whoami`/tmp/ID_`uuidgen`
#else
#echo "tmp directory in local path"
#uniquedir=./tmp/ID_`uuidgen`       
#fi
uniquedir=/usr1/`whoami`/tmp/ID_`uuidgen`       
mkdir -p $uniquedir
echo $uniquedir
cp ${XPIPELINE_ROOT}/post/estimateInjScale $uniquedir
cp ${XPIPELINE_ROOT}/post/estimateInjScale.ctf $uniquedir
$uniquedir/estimateInjScale $1 $2 
exitstatus=$?
rm -rf $uniquedir
exit $exitstatus
