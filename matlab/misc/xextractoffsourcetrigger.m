injScales = [0];
%myFolders = {'output/simulations_ebbh-e_', 'output/simulations_ebbh-e_', 'output/simulations_ebbh-e_', 'output/simulations_ebbh-e_', 'output/simulations_ebbh-e_', 'output/simulations_ebbh-e_', 'output/simulations_ebbh-e_', 'output/simulations_ebbh-e_'};

%myFolders = {'output/simulations_ebbh-e_'};
%myFolders = {'/home/maxime.fays/XsphradTests/r4436_jw1_rerun/l_20/auto_web'};
myFolders = {'../auto_web'};
%myFolders = {'/home/valeriu/public_html/JW1/JW1-H1L1V1_run3_LNSEP_PDSQ_BW_wPRF4'};
% ---- for testing
%injScales = [17];
%myFolders = {'test_u/simulations_ebbh-e_'};

fid1 = fopen('offsource_significance.txt','w');
fid2 = fopen('offsource_t1.txt','w');
fid3 = fopen('offsource_t2.txt','w');
fid4 = fopen('offsource_t3.txt','w');
fid5 = fopen('offsource_duration.txt','w');
fid6 = fopen('offsource_bw.txt','w');
fid7 = fopen('offsource_npixels.txt','w');
%fid7 = fopen('ebbh-e_flag1_previousScale.txt','w');
fid8 = fopen('offsource_nullE.txt','w');
fid9 = fopen('offsource_nullEinc.txt','w');
%fid10 = fopen('ebbh-e_scale_previousScale.txt','w');

for i=1:length(myFolders)
        for j=1:length(injScales)
                myFolder = [myFolders{i}];% num2str(injScales(j))];
                disp(myFolder);
                if ~isdir(myFolder)
                  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
                  disp(errorMessage);
                  return;
                end
                filePattern = fullfile(myFolder, 'JW1-H1L1V1_run3_SC5_PrfSignifOnly_closedbox.mat');
                matFiles = dir(filePattern);
                for k = 1:length(matFiles)
                  baseFileName = matFiles(k).name;
                  disp(baseFileName);
                  fullFileName = fullfile(myFolder, baseFileName);
                  s = load(fullFileName);
                  %ntriggers = length(s.injAssociatedTrigger(:,27));
                  for m = 1:1
                    for n = 1:1
		      if 1
                        %fprintf(fid7, '%f\n', s.injAssociatedTrigger(n,m).significance)
                        if 1
                          fprintf(fid1, '%f\n', s.offSourceSelectedAfterAllCuts.likelihood(:,4));
                          fprintf(fid2, '%f\n', s.offSourceSelectedAfterAllCuts.unslidTime(:,1));
                          fprintf(fid3, '%f\n', s.offSourceSelectedAfterAllCuts.unslidTime(:,2));
                          fprintf(fid4, '%f\n', s.offSourceSelectedAfterAllCuts.unslidTime(:,3));
                          fprintf(fid7, '%f\n', s.offSourceSelectedAfterAllCuts.nPixels);
                          fprintf(fid5, '%f\n', s.offSourceSelectedAfterAllCuts.boundingBox(:,3));
                          fprintf(fid6, '%f\n', s.offSourceSelectedAfterAllCuts.boundingBox(:,4));
                          fprintf(fid8, '%f\n', s.offSourceSelectedAfterAllCuts.likelihood(:,8));
                          fprintf(fid9, '%f\n', s.offSourceSelectedAfterAllCuts.likelihood(:,9));
                        end
                      end

                    end
                  end
                  fprintf(1, 'Done reading %s\n', fullFileName);
                end
        end
end
fclose(fid1);
fclose(fid2);
fclose(fid3);
fclose(fid4);
fclose(fid5);
fclose(fid6);
fclose(fid7);
%fclose(fid7);
fclose(fid8);
fclose(fid9);
%fclose(fid10);
