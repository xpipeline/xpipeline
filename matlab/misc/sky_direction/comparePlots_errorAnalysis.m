%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to plot distributions and other things for comparing
% two different source direction estimation methods.
% Uses the output of bayesianSourceDirectionErrorAnalysis.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- only use triple coincident data
% useTC = false;
useTC = true;

minimizationPath = '../run_1/';
bayesianPath = '../run_33_grid_1/';

% path to data files to compare
comparePaths = {'../run_random/'; minimizationPath; '../run_33/'; bayesianPath};

% ---- pick the correct waveform corresponding to the paths specified above
% waveformStr = 'GAUSS1';
% tcIndices = tripleCoincident('../triple_source/VHL:GAUSS1_EGC_source.dat', '../injections.txt');

% waveformStr = 'GAUSS4';
% tcIndices = tripleCoincident('../triple_source/VHL:GAUSS4_EGC_source.dat', '../injections.txt');

% waveformStr = 'SG235';
% tcIndices = tripleCoincident('../triple_source/VHL:Q15F235_EGC_source.dat', '../injections.txt');

% waveformStr = 'SG820';
% tcIndices = tripleCoincident('../triple_source/VHL:Q15F820_EGC_source.dat', '../injections.txt');

waveformStr = 'DFMA1B2G1';
tcIndices = tripleCoincident('../triple_source/VHL:A1B2G1_EGC_source.dat', '../injections.txt');

% waveformStr = 'DFMA2B4G1';
% tcIndices = tripleCoincident('../triple_source/VHL:A2B4G1_EGC_source.dat', '../injections.txt');


maxPointingErrLength = 1259;
inBoundTC = find(tcIndices <= maxPointingErrLength);
tcIndices = tcIndices(inBoundTC);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% distribution comparison
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nBins = 50;
bins = [(pi/nBins)/2:pi/nBins:pi];

figure;

f = open([minimizationPath '/data.mat']);
if useTC
	ptErrs = f.pointingErrs(tcIndices);
else
	ptErrs = f.pointingErrs;
end
hist0 = hist(ptErrs, bins);
stairs(bins, hist0, 'b');
hold on;

f = open([bayesianPath '/data.mat']);
if useTC
	ptErrs = f.pointingErrs(tcIndices);
else
	ptErrs = f.pointingErrs;
end
hist1 = hist(ptErrs, bins);
stairs(bins, hist1, 'r');

yMax = max(max(hist0), max(hist1));
ylim([0 yMax+20]);
xlim([0 pi]);

xlabel('pointing err (rad)');
ylabel('distribution');

if useTC
	title([waveformStr ', triple coincident']);
else
	title(waveformStr);
end

legend('min(Enull)', 'Bayesian, 3x3 grid');

if useTC
	saveStr = [bayesianPath 'pointing_dist_comp_TC'];
else
	saveStr = [bayesianPath 'pointing_dist_comp'];
end

saveas(gcf, [saveStr '.png']);
saveas(gcf, [saveStr '.fig']);
saveas(gcf, [saveStr '.eps'], 'epsc2');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cumulative distributions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% bin width in degrees
bins = [0:0.25:180];
prctileStrs = [];
figure;
colors = get(gca, 'ColorOrder');
colors = [colors; colors];

for i = 1:length(comparePaths)
	iPath = comparePaths{i};
	
	load([iPath '/data.mat']);
	
	% pointingError_deg = real(minPointingErrs)*180/pi;
	
	if useTC
		pointingError_deg = real(pointingErrs(tcIndices))*180/pi;
	else
		pointingError_deg = real(pointingErrs)*180/pi;
	end
	
	% pointingError_deg = real(pointingErrs)*180/pi;
	% pointingError_deg = real(thetaErrs)*180/pi;
	% pointingError_deg = real(phiErrs)*180/pi;
	% pointingError_deg = real(psiErrs)*180/pi;
	
	nPointingError_deg = histc(pointingError_deg, bins);
	% stairs([0:180],nPointingError_deg/max(nPointingError_deg),'linewidth',2);
	% hold on;
	stairs(bins, cumsum(nPointingError_deg)/sum(nPointingError_deg), ...
		'-','linewidth',2,'Color',colors(i,:));
	% disp(['Median pointing error (deg): ' num2str(prctile(pointingError_deg,50))]);
	prctileStr = [iPath ': 50%%: ' num2str(prctile(pointingError_deg,50)) ...
				': 68%%: ' num2str(prctile(pointingError_deg,68))];
	disp(prctileStr);
	prctileStrs = [prctileStrs '\n' prctileStr];
	
	hold on;
end

grid;
% legend('minimization','bayesian','location','southeast');
% legend(strrep(comparePaths, '_', '\_'),'location','southeast');
legend('random','min(Enull)','Bayesian','Bayesian, 3x3 grid','location','southeast');

if useTC
	title([waveformStr ', triple coincident']);
else
	title(waveformStr);
end

xlabel('Pointing error (deg)');
ylabel('Fraction of GWBs');
xlim([0 180]);

if useTC
	saveStr = [bayesianPath 'pointing_cumdist_comp_TC'];
else
	saveStr = [bayesianPath 'pointing_cumdist_comp'];
end

saveas(gcf, [saveStr '.png']);
saveas(gcf, [saveStr '.fig']);
saveas(gcf, [saveStr '.eps'], 'epsc2');

% save percentile data
if useTC
	fid = fopen([bayesianPath 'prctiles_TC.txt'], 'w');
else
	fid = fopen([bayesianPath 'prctiles.txt'], 'w');
end

fprintf(fid, prctileStrs);
fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pointing error 'scatter' plot (pointing error vs. skymap number)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
set(gcf, 'PaperPosition', [0 0 8 3]);
subplot(1,2,1);
f = load([minimizationPath '/data.mat']);

if useTC
	ptErrs = f.pointingErrs(tcIndices);
else
	ptErrs = f.pointingErrs;
end

nonNanLength = length(find(~isnan(ptErrs)));
plot([1:nonNanLength], ptErrs(1:nonNanLength), '.');
xlim([1 nonNanLength]);
xlabel('skymap number');
ylabel('pointing error (rad)');

if useTC
	title('min(Enull), triple coincident');
else
	title('min(Enull)');
end

%%%%
subplot(1,2,2);
f = load([bayesianPath '/data.mat']);

if useTC
	ptErrs = f.pointingErrs(tcIndices);
else
	ptErrs = f.pointingErrs;
end

nonNanLength = length(find(~isnan(ptErrs)));
plot([1:nonNanLength], ptErrs(1:nonNanLength), '.');
xlim([1 nonNanLength]);
xlabel('skymap number');
ylabel('pointing error (rad)');

if useTC
	title('Bayesian, 3x3 grid, triple coincident');
else
	title('Bayesian, 3x3 grid');
end


if useTC
	saveStr = [bayesianPath 'pointing_comp_TC'];
else
	saveStr = [bayesianPath 'pointing_comp'];
end

saveas(gcf, [saveStr '.png']);
saveas(gcf, [saveStr '.fig']);
saveas(gcf, [saveStr '.eps'], 'epsc2');


