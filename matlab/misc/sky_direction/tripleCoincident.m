function tcIndices = tripleCoincident(tcFile, injectionsFile);
% tripleCoincident: return the row indices in injectionsFile
% that have entries in tcFile.

tcData = dlmread(tcFile);
injectionsData = dlmread(injectionsFile);

tcTimes = floor(tcData(:,1));

tcIndices = [];
for i = 1:length(tcTimes)
	index = find(injectionsData(:,1) == tcTimes(i));
	if ~isempty(index)
		tcIndices = [tcIndices; index];
	end
end

return;