function gradmap = naiveGradient(CP, skymap, delta)
% naiveGradient - Calculate an approximate naive gradient to a map
%
% Usage:
%     gradmap = naiveGradient(coords,  map, delta)
%
%     coords    An m-by-n matrix of m vectors in n-space
%     map       An m-by-1 matrix of values at the m coordinate points
%     delta     A scalar radius within which points contribute to the gradient
%
%     gradmap   An m-by-n matrix, each column of which is the gradient in that
%               coordinate direction.
%

%  Initial write: Leo C. Stein 2006.04.02
%
%  $Id$


nEl = size(CP,1);
nDir = size(CP,2);

gradmap = zeros([length(skymap) nDir]);

for iEl = 1:nEl,

    dCP = CP - repmat(CP(iEl,:),nEl,1);

    ball = find(sum(dCP.^2,2)<=delta);

    gradmap(iEl,:) = (1/(delta*length(ball))) * sum( ...
        repmat((skymap(ball)-skymap(iEl)),1,nDir) .* dCP(ball,:), ...
        1);

end;
