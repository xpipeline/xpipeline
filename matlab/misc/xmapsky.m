function [nullEnergyMap, incoherentEnergyMap, totalEnergyMap, numberOfFreqBins, hp, hc] = ...
    xmapsky(channelNames, conditionedData, sampleFrequency, ...
            amplitudeSpectra, skyPositions, elementLength, elementOffset, ...
            transientLength, frequencyBands, analysisMode, verboseFlag)
% XMAPSKY Map gravitational-wave burst consistency as a function of sky position
% 
% XMAPSKY generates a sky map describing the consistency of the signals observed
% in a network of three of more non-aligned detectors with the assumption that
% they are due to a gravitational-wave burst from a particular direction on the
% sky.  The sky map is generated for the requested list of sky positions and the
% requested analysis time scale and frequency bands.  To do so, the conditioned
% data is first partitioned into overlapping elements of the specified length.
% For each element, the signals from all detectors are then examined in the
% frequency domain for consistency with a burst coming from the specified sky
% positions.
%
% usage:
% 
%   [nullEnergyMap, incoherentEnergyMap, totalEnergyMap, numberOfFreqBins] = ...
%       xmapsky(channelNames, conditionedData, sampleFrequency, ...
%               amplitudeSpectra, skyPositions, elementLength, ...
%               transientLengths, frequencyBands, verboseFlag);
%
%    channelNames         cell array of channel name strings
%    conditionedData      matrix time domain conditioned data
%    sampleFrequency      sampling frequency of conditioned data [Hz]
%    amplitudeSpectra     matrix amplitude spectral densities [1/sqrt(Hz)]
%    skyPositions         matrix of sky positions [radians]
%    elementLength        transform length of fourier analyses [samples]
%    elementOffset        Scalar.  Number of samples between start of 
%                         consecutive elements.
%    transientLength      duration of conditioning transients [samples]
%    frequencyBands       matrix of frequency bands to search over [Hz]
%    analysisMode         string specifying 'search', 'veto', or 'frankenveto' analysis
%    verboseFlag          boolean flag to control status output (default 0)
% 
%    nullEnergyMap        resulting null energy map array (see below)
%    incoherentEnergyMap  resulting incoherent energy map array (see below)
%    totalEnergyMap       resulting total energy map array (see below)
%    numberOfFreqBins     number of frequency bins in each band (for
%                         statistical testing)
%    hp                   estimated plus waveforms array
%    hc                   estimated cross waveforms array
%
% The resulting sky maps are three dimensional array with the following form.
%
%   nullEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   incoherentEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   totalEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%
% The conditioned data should be provided as a matrix of time domain data with
% each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of one-sided
% frequency domain data at a frequency resolution corresponding to the desired
% element length and with each channel in a separate column.
%
% The desired sky position should be provided as a two column matrix of the form
% [theta phi], where theta is the geocentric colatitude running from 0 at the
% North pole to pi at the South pole and phi is the geocentric longitude in
% Earth fixed coordinates with 0 on the prime meridian.
%
% The desired frequency bands should be provided as a two column matrix of
% the form [lowFrequency highFrequency].
%
% See also XPIPELINE, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, and
% XINTERPRET.
%
% See also ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        process command line arguments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
error(nargchk(10, 11, nargin));

% default arguments
if nargin == 10,
  verboseFlag = 0;
end

% check that analysisMode is a recognized type
if (~(strcmp(analysisMode,'veto') || strcmp(analysisMode,'search') || strcmp(analysisMode,'frankenveto')))
  error('analysisMode is not a recognized type');
end

% force one dimensional cell array
channelNames = channelNames(:);

% number of detectors
numberOfChannels = length(channelNames);

% validate number of detectors
if numberOfChannels < 3,
  error('data is required from three or more non-aligned detectors');
end

% number of null sums
numberOfNullSums = numberOfChannels - 2;

% validate conditioned data
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% block length in samples
blockLength = size(conditionedData, 1);

% validate sky positions
if size(skyPositions, 2) < 2,
  error('sky positions must be a at least a two column matrix');
end
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% number of frequency bands to consider
numberOfFrequencyBands = size(frequencyBands, 1);

% we don't bother to complicate the code to allow this rarely used option
if numberOfFrequencyBands > 1
  warning('ignoring all but first frequency band');
end

% validate frequency bands
if size(frequencyBands, 2) ~= 2,
  error('frequency bands must be a two column matrix');
end
if max(max(frequencyBands)) > nyquistFrequency,
  error('requested frequency bands exceed nyquist frequency');
end
if any(diff(frequencyBands, 1, 2) <= 0),
  error('frequency bands have negative bandwidth');
end


% validate integer power of two element length
if bitand(elementLength, elementLength - 1),
  error('element length is not an integer power of two');
end

% determine half element lengths
halfElementLength = elementLength / 2 + 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  partition block into overlapping elements                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% calculate the indices of the endpoints of the elements of length
% elementLength with 50% overlap
elementIndices = chunkIndices(blockLength, elementLength, transientLength, elementOffset);

% in 'veto' mode keep only the element overlapping the middle of the data 
% segment
if (strcmp(analysisMode,'veto'))
    elementIndices = elementIndices(floor((size(elementIndices,1)+1)/2),:);
end

% Attack of the FrankenVeto monster!
if (strcmp(analysisMode,'frankenveto'))
    % Make sky maps for a small set of chunks centered on the event time.
    % Later combine over chunks into FrankenMaps!
    %
    % Compute chunkIndices using transient time to eliminate all but an  
    % interval of duration 3*elementLength at the center of the block. 
    % Hardwire overlapping of chunks at 50%.
    % elementOffset = ceil(0.5*elementLength);
    elementIndices = chunkIndices(blockLength, elementLength, ...
        floor(0.5*(blockLength-3*elementLength)), elementOffset);
end

% number of elements
numberOfElements = size(elementIndices, 1);

% one-sided frequency vector for elements
elementFrequencies = nyquistFrequency * (0 : halfElementLength - 1) / ...
                     (halfElementLength - 1);

% frequency band indices into one-sided frequency domain element
frequencyIndices = cell(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  frequencyIndices{frequencyBandNumber} = ...
    [min(find(elementFrequencies >= frequencyBands(frequencyBandNumber, 1))) ...
     max(find(elementFrequencies <= frequencyBands(frequencyBandNumber, 2)))];
	%     [find(elementFrequencies >= frequencyBands(frequencyBandNumber, 1),1) ...
	%      find(elementFrequencies <= frequencyBands(frequencyBandNumber, 2),1,'last')];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         determine degrees of freedom                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Number of frequency bins in each frequency band.  Note that current
%      code (below) does not include highest bin in each band.
numberOfFreqBins = zeros(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  numberOfFreqBins(frequencyBandNumber) = ...
      diff(frequencyIndices{frequencyBandNumber});
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         preload detector information                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for iDet = 1:numberOfChannels
    % The detector is matched by the first character of the channel name.
    detData      = LoadDetectorData(channelNames{iDet}(1));
    % the position vectors
    rDet(iDet,:) = detData.V';
    % the cell array of detector response tensors
    dDet{iDet}   = detData.d;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       precompute all detector response                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
% compute detector responses for this sky position
[aFplus, aFcross] = antennaPatterns(dDet, skyPositions);
aTimeShifts = computeTimeShifts(rDet, skyPositions);
aTimeShiftLengths = aTimeShifts * sampleFrequency;
aIntegerTimeShiftLengths = round(aTimeShiftLengths);
aResidualTimeShiftLengths = aTimeShiftLengths - aIntegerTimeShiftLengths;

ampMat = permute(amplitudeSpectra,[3 2 1]);
ampMat = [ampMat;ampMat];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        begin loop over sky positions                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialized result structure
nullEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);
incoherentEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);
totalEnergyMap = ...
    zeros(numberOfFrequencyBands, numberOfElements, numberOfSkyPositions);

% Assign storage for extracted, time-shifted time series
elementTD = zeros(elementLength, numberOfChannels);

% keep a running total to find the smallest null energy
bestNullEnergy = Inf;

% begin loop over sky positions
for skyPositionNumber = 1 : numberOfSkyPositions,
  
  % report status
  if verboseFlag && (mod(skyPositionNumber, numberOfSkyPositions / 100) < 1)
    fprintf(1, 'processing sky position %d of %d (%d%% complete)...\n', ...
            skyPositionNumber, numberOfSkyPositions, ...
            round(100 * skyPositionNumber / numberOfSkyPositions));            
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                       determine detector response                            %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % antenna repsonses for this sky position
  Fplus = aFplus(skyPositionNumber,:);
  Fcross = aFcross(skyPositionNumber,:);

  % determine integer sample shift and residual phase shift
  integerTimeShiftLengths = aIntegerTimeShiftLengths(skyPositionNumber,:);
  residualTimeShiftPhases = exp(sqrt(-1) * 2 * pi * elementFrequencies.' * ...
                            aResidualTimeShiftLengths(skyPositionNumber,:) ...
                            / sampleFrequency);

  % determine null sum coefficients
  A = repmat([Fplus;Fcross],[1 1 halfElementLength]) ./ ampMat;

  [A, n] = xbasis(A);
  size(n);
  A = permute(A, [3 2 1]);
  
  % keep a running total to find the largest total energy
  bestTotalEnergy = -Inf;

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                         begin loop over elements                             %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % begin loop over elements
  for elementNumber = 1 : numberOfElements
  
    % start and stop indices of element
    elementStartIndex = elementIndices(elementNumber, 1);
    elementStopIndex = elementIndices(elementNumber, 2);
  
    for channelNumber = 1 : numberOfChannels
      elementTD(:, channelNumber) = ...
          conditionedData((elementStartIndex + ...
                           integerTimeShiftLengths(channelNumber)) : ...
                          (elementStopIndex + ...
                           integerTimeShiftLengths(channelNumber)), ...
                          channelNumber);
    end

    % apply fourier transform
    element = fft(elementTD);
    
    % take one-sided transform
    element = element(1 : halfElementLength, :);
    
    % apply residual time shift
    element = element .* residualTimeShiftPhases;

    % construct null and incoherent energies
    nullEnergy = zeros(halfElementLength, 1);
    incoherentEnergy = zeros(halfElementLength, 1);
    for nullSumNumber = 1 : numberOfNullSums,
        nullTerms = A(:, :, nullSumNumber + 2) .* element;
        nullSum = sum(nullTerms, 2);
        nullEnergy = nullEnergy + real(nullSum).^2 + imag(nullSum).^2;
        incoherentEnergy = incoherentEnergy + ...
            sum(real(nullTerms).^2 + imag(nullTerms).^2, 2);
    end

    % construct total energies
    totalEnergy = sum(real(element).^2 + imag(element).^2, 2);

    % construct cumulative sums over frequencies
    nullEnergy = [0; cumsum(nullEnergy);];
    incoherentEnergy = [0; cumsum(incoherentEnergy);];
    totalEnergy = [0; cumsum(totalEnergy);];
    %
    % insert frequency band energies into sky maps
    % KLUDGE: With shift by pre-pended zero in element 1 above, this 
    % construction gives the energy in the band [fmin,fmax); i.e., not
    % including the last frequency bin.  Fix this!  Then also fix the count
    % of numberOfFreqBins above.
    for frequencyBandNumber = 1 : numberOfFrequencyBands,
      nullEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
          diff(nullEnergy(frequencyIndices{frequencyBandNumber}));
      incoherentEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
          diff(incoherentEnergy(frequencyIndices{frequencyBandNumber}));
      totalEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
          diff(totalEnergy(frequencyIndices{frequencyBandNumber}));
    end

    % % ---- This code does not restrict to the desired frequency range, so
    %        it has been disabled until fixed.  Define dummy outputs.
    hp = [];
    hc = [];
    % % ---- sum over frequency bins
    % nullEnergy = sum(nullEnergy);
    % incoherentEnergy = sum(incoherentEnergy);
    % totalEnergy = sum(totalEnergy);
    % 
    % % ---- assign to maps
    % nullEnergyMap(1, elementNumber, skyPositionNumber) = nullEnergy;
    % incoherentEnergyMap(1, elementNumber, skyPositionNumber) = incoherentEnergy;
    % totalEnergyMap(1, elementNumber, skyPositionNumber) = totalEnergy;
	
    % if nullEnergy < bestNullEnergy && totalEnergy > bestTotalEnergy
    %     % construct plus and cross polarizations
    %     hp = zeros(1, halfElementLength);
    %     hc = zeros(1, halfElementLength);
    %     for frequencyIndex = 1 : halfElementLength,
    %         hp(:, frequencyIndex) = A(frequencyIndex, :, 1) * element(frequencyIndex, :).';
    %         hc(:, frequencyIndex) = A(frequencyIndex, :, 2) * element(frequencyIndex, :).';
    %     end
    % 
    %     % convert to time domain.
    %     hfullp = hp' ./ n(:,1);
    %     hfullp = [hfullp; conj(flipud(hfullp(2:end-1,:)))];
    %     hfullp_t = ifft(hfullp);
    %     hp = real(hfullp_t);
    % 
    %     hfullc = hc' ./ n(:,2);
    %     hfullc = [hfullc; conj(flipud(hfullc(2:end-1,:)))];
    %     hfullc_t = ifft(hfullc);
    %     hc = real(hfullc_t);
    % 
    % 
    %     bestNullEnergy = nullEnergy;
    %     bestTotalEnergy = totalEnergy;
    % end
	
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                          end loop over elements                              %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
  % end loop over elements
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         end loop over sky positions                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% end loop over sky positions
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         frankenmaps                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (strcmp(analysisMode,'frankenveto'))
    %----- Each type of map: combine over chunks into FrankenMaps!

    %----- Sum over frequencies, and make the singleton dimension last (gone)
    tEM = permute(sum(totalEnergyMap,1),[2 3 1]);

    
    %----- For each sky pixel, find segment with maximum total energy.
    [valtot, indextot] = max(tEM,[],1);

    %----- Make FrankenMaps based on total energy.

    for frequencyBandNumber = 1:numberOfFrequencyBands,
        sindex = sub2ind(...
            [numberOfFrequencyBands numberOfElements numberOfSkyPositions],...
            frequencyBandNumber*ones(1,numberOfSkyPositions),...
            indextot,1:numberOfSkyPositions);

        frankenNullEnergyMap(frequencyBandNumber,1,:) = nullEnergyMap(sindex);
        frankenIncoherentEnergyMap(frequencyBandNumber,1,:) = incoherentEnergyMap(sindex);
        frankenTotalEnergyMap(frequencyBandNumber,1,:) = totalEnergyMap(sindex);
    end
    
    nullEnergyMap = frankenNullEnergyMap;
    incoherentEnergyMap = frankenIncoherentEnergyMap;
    totalEnergyMap = frankenTotalEnergyMap;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% return to calling function
return;
