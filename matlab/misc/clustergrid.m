function [clusterNumber, skyCells] = clustergrid(skyPositions, dt, detectorSet)
% clustergrid:  Cluster sky positions sky positions by the maximum timing
% difference for any detector pair between them.
%
%  [clusterNumber skyCells] = clustergrid(skyPositions, dt, detectorSet)
%
%  skyPositions Array of sky positions.  The format is the same as for
%               sinusoidalMap.  Only the first two columns are used (polar 
%               angle theta and azimuthal angle phi).
%  dt           Scalar.  Approximate maximum time difference between
%               pixels. 
%  detectorSet  String or cell array of strings.  If string, it holds the 
%               first letter of the name of each detector in the network.  
%               If a cell array, the detector names are inferred from the
%               first letter of each cell element (i.e., channel name). 
%
%  clusterNumber  Vector of positive integers, one for each skyPosition.
%               Sky positions with the same clusterNumber are in the same 
%               cluster.
%  skyCells     Cell array of length max(clusterNumber).  Each element
%               contains the subarray of skyPositions that belongs to the 
%               corresponding cluster (same clusterNumber). 
%
% See also sinusoidalMap.
%
% $Id$


%----- Useful constants.
c = 299792458;

%----- Check to see if detectors are specified by string or by a cell array
%      of channel names.
if (iscell(detectorSet))
    for jChan=1:length(detectorSet)
        dS(1,jChan) = detectorSet{jChan}(1);
    end
    detectorSet = dS;
end

%----- Load detector info.
detectorVertex = [];
for jdetector=1:length(detectorSet);
    detector = LoadDetectorData(detectorSet(jdetector));
    detectorVertex = [detectorVertex, detector.V];
end

%----- Make array of baselines.
baseline = [];
for idetector=1:size(detectorVertex,2)-1
    for jdetector=idetector+1:size(detectorVertex,2)
        %----- baseline vectors in metres
        baseline = [baseline, ... 
            detectorVertex(:,idetector) - detectorVertex(:,jdetector)];
    end
end
%----- Convert baselines to light-seconds.
baseline = baseline/c;
%----- Compute unit vectors along baselines.
baseline_length = sum(baseline.^2,1).^(0.5);
% baseline_unit = baseline*diag( sum(baseline.^2,1).^(-0.5) );
%----- Number of baselines.
nBaseline = size(baseline,2);

%----- Cartesian vector of pointing directions of initial grid.
omega = [sin(skyPositions(:,1)).*cos(skyPositions(:,2)), ...
    sin(skyPositions(:,1)).*sin(skyPositions(:,2)), ...
    cos(skyPositions(:,1))];
%----- Delays against each baseline.
delay = omega*baseline;
%----- Min and max (signed) delays against each baseline.
%      Put in array with identical rows so can use .*, etc.
delay_min = zeros(size(delay));
for iBaseline=1:nBaseline
    delay_min(:,iBaseline) = min(delay(:,iBaseline),[],1);
end
%----- Imagine set of hypercubes of side length dt filling Cartesian delay
%      space.  Set up grid such that the minimum time delay for each 
%      baseline over the entire sky occurs on a cell boundary. Then can use
%      mod(delay-delay_min,dt) to figure out which cell any sky coordinate
%      belongs in, and how far from the cell boundaries it is.
%      Use as "template" the pixel closest to the center of each hypercube.
%----- Time delay of each point from lower-delay-sides of cell. 
delay_mod = mod(delay-delay_min,dt);
%----- Center of grid cell is a 0.5*dt away from each edge.
%      Distance of each point from center.
distance = max(abs(delay_mod-0.5*dt),[],2);  
%----- Integer number of cells away from delay=0 boundary.
delay_disc = round((delay-delay_min-delay_mod)/dt);
%----- Sort points by cell.
[Y,IY] = sortrows(delay_disc);
% skyPositions = skyPositions(IY,:);
%----- 
[Z, IZ, JZ] = unique(Y,'rows');
clusterNumber = zeros(length(IY),1);
clusterNumber(IY) = JZ;
%----- 
skyCells = cell(max(clusterNumber),2);
for jCluster = 1:max(clusterNumber)
    skyCells{jCluster,2} = find(clusterNumber==jCluster);
    skyCells{jCluster,1} = skyPositions(logical(clusterNumber==jCluster),:);
end

%----- Done
return
