function [map,Enull,Inull,Etot,theta,phi] = generateskymap(gps,h,fs,ifos,N,tikhon)
% GENERATESKYMAP - make null-energy-based sky maps from three input h streams
%
% usage:
%
% [map,Enull,Inull,Etot,theta,phi] = generateskymap(gps,h,fs,ifos,N,tikhon)
%
%   gps     Scalar. GPS time of the event.
%   h       Mx3 array. Each column is the timeseries strain data from one
%           detector.
%   fs      Scalar. Sample rate [Hz] of the timeseries data.
%   ifos    3-element cell array of strings. Each element identifies the
%           detector for the corresponding column of h. Must be recognised by
%           LOADDETECTORDATA.
%   N       Scalar. HEALPix number for output sky maps. The output maps will
%           contain 12*N^2 points, where N must be a positive power of 2 (i.e.,
%           N=2^n for n=1,2,...). 
%   tikhon  Optional scalar. Tikhonov regularisation parameters. The
%           unregularised maximum-likelihood reconstruction corresponds to the
%           limit tikhon >> 1. If not specified then the limit tikhon->Inf is
%           used.
%
%   map     Struct with fields 
%             ra  - right ascension [deg] of map pixels
%             dec - declination [deg] of map pixels
%             probDensity - map "density"; set equal to Enull values
%             order - HEALPix order number of each pixel. Always log2(N).
%             scheme - HEALPix ordering scheme. Always 'nest'.
%           This map makes plotting easy, e.g. plotskymap(map,'mollweid').
%   Enull   Vector of coherent null energy values for each map pixel.
%   Inull   Vector of incoherent null energy values for each map pixel.
%   Etot    Vector of total energy values for each map pixel (constant over sky).
%   theta   Vector polar angles [rad] for each map pixel.
%   phi     Vector azimuthal angles [rad] for each map pixel.
%
% $Id$


% ------------------------------------------------------------------------------
%    Sample code to generate h(t) for each detector in the network (not used). 
% ------------------------------------------------------------------------------

if false
    % ---- Network.
    ifos = {'H','L','V'};

    % ---- h sample rate [Hz].
    fs = 1024;

    % ---- Create an injection log file for a short Gaussian pulse.
    [status,result] = system('echo "1337069300 500000000 1.00 1.57 1.11 G 1e-21~0.001" > test_inj.txt');
    % [status,result] = system('echo "1000000000 500000000 0.0 0.1 1.11 G 1e-21~0.001" > test_inj.txt');
    % [status,result] = system('echo "1000000000 500000000 2.71 0.321 1.11 G 1e-21~0.001" > test_inj.txt');
    % ---- Read the injection log file and make a 1-second ling h(t) timeseries for each detector.
    [h, gps_s, gps_ns, phi_inj, theta_inj, psi_inj] = xinjectsignal(1337069300,1,ifos,[fs,fs,fs],'test_inj.txt');
    h = cell2mat(h');
    fid = fopen('injection_data.txt','w');
    fprintf(fid,'%g %g %g\n',h');
    fclose(fid);
end

% ------------------------------------------------------------------------------
%    Preparatory.
% ------------------------------------------------------------------------------

% ---- Check number of input aguments and assign defaults.
narginchk(5,6)
if nargin<6
    tikhon = false;
end

% ---- Number of timeseries samples.
Ns = size(h,1);

% ---- Assuming h~1e-21, rescale by white PSD value to get sensible E values.
h = h / 3e-22;


% ------------------------------------------------------------------------------
%   Make the sky grid.
% ------------------------------------------------------------------------------

% ---- Make the grid. Use nested ordering to match convention for LIGO skymaps.
skyGrid = transpose(cell2mat(pix2ang(N,'nest',true)));
theta   = skyGrid(:,1);
phi     = skyGrid(:,2);
Nsky    = length(phi);
% ---- Sanity check.
if Nsky~=12*N^2
    error(['Unexpected number of sky positions: Nsky=' num2str(Nsky) ', N=' num2str(N) '.'])
end

% ---- Compute the time delay (t_detector - t_earth-centre) for each of the
%      skyGrid positions.
deltaT = computeTimeShifts(ifos, skyGrid);
% ---- Convert time delay to samples, rounded.
deltaTsamp = round(deltaT*fs);
warning('Ignoring sub-sample delays.')

% ---- Compute antenna responses over the grid.
for jj=1:length(ifos)
    [Fp(:,jj), Fc(:,jj)] = ComputeAntennaResponse(phi,theta,0,ifos{jj});
end
if tikhon
    [fp fc psiDP] = convertToDominantPolarizationFrame(Fp,Fc);
end


% ------------------------------------------------------------------------------
%   Make the sky map.
% ------------------------------------------------------------------------------

Enull = zeros(Nsky,1);
Inull = zeros(Nsky,1);
for ii = 1:Nsky
    % ---- Apply time shift to the nearest sample.
    for jj=1:length(ifos)
        sh(:,jj) = circshift(h(:,jj),-deltaTsamp(ii,jj));
    end
    if tikhon
        % ---- Compute coherent and incoherent null energy maps for
        %      Tikohonv-regularised waveform reconstruction.
        % ---- Symmetric "projection" matrix. (It's not actually a projection
        %      matrix for finite values of tikhon because it is not idempotent.) 
        M = eye(3) ... 
            - tikhon^2/(1+tikhon^2*sum(fp(ii,:).^2))*transpose(fp(ii,:))*fp(ii,:) ...
            - tikhon^2/(1+tikhon^2*sum(fc(ii,:).^2))*transpose(fc(ii,:))*fc(ii,:);
        % ---- Coherent null energy matrix operator (E_null = d^T S_E d).
        S_E = M * M;  %-- M^T = M
        % ---- Compute coherent null energy.
        Enull(ii) = sum(sum((sh * S_E) .* sh,2));
        % ---- Incoherent null energy matrix operator (I_null = d^T S_I d).
        S_I = diag(diag(S_E));
        % ---- Compute incoherent null energy.
        Inull(ii) = sum(sum((sh * S_I) .* sh,2));
    else
        % ---- Compute coherent and incoherent null energy maps for
        %      unregularised maximum-likelihood waveform reconstruction. This
        %      simpler code gives the same results as the Tikohonov code with
        %      tikhon >> 1.
        % ---- Null direction unit vector.
        K = cross(Fp(ii,:),Fc(ii,:));
        K = K/norm(K);
        % ---- Compute both coherent null and incoherent null energies.
        null = sh .* repmat(K,Ns,1);
        Enull(ii) = sum(sum(null,2).^2);
        Inull(ii) = sum(sum(null.^2));
    end
end

Etot = sum(h(:).^2) * ones(size(Enull));


% ------------------------------------------------------------------------------
%    Prepare map struct output in desired format.
% ------------------------------------------------------------------------------

% ---- Make 'map' output.
% map.probDensity = Enull;
%
L_sky = (1-Enull./Inull) .* (Etot - Enull);
alpha = 0;
beta  = 1;
sigma = fs;
antenna_response_rss = (sum(Fp.^2,2) + sum(Fc.^2,2)).^0.5;
unnorm_prob_map = (antenna_response_rss).^alpha .* exp( -((max(L_sky) - L_sky)/sigma).^beta );
map.probDensity = unnorm_prob_map / sum(unnorm_prob_map);

%
[map.ra, map.dec] = earthtoradec(phi,theta,gps);
map.order = log2(N)*ones(size(phi));
map.scheme = 'nest';


% ------------------------------------------------------------------------------
%    Sample code to make plots (not used).
% ------------------------------------------------------------------------------

if false 
    
    % ---- Plot injection timeseries.
    t = [1:Ns]'/fs;
    figure; 
    plot(t,h,'linewidth',2); 
    grid on; hold on
    % axis([0.45 0.55 -2.5e-20 2.5e-20])
    set(gca,'fontsize',16)
    xlabel('time [s]')
    ylabel('strain')
    legend('H','L','V')

    % ---- Sky map plots.
    plotskymap(map,'mollweid');
    title('E_{null}')
    %
    map.probDensity = Inull;
    plotskymap(map,'mollweid');
    title('I_{null}')
    %
    map.probDensity = Enull-Inull;
    handle4 = plotskymap(map,'mollweid');
    title('E_{null}-I_{null}')
    %
    map.probDensity = Enull./Inull-1;
    handle5 = plotskymap(map,'mollweid');
    title('E_{null}/I_{null} - 1')
    
    % % ---- Plot injection timeseries.
    % t = [1:Ns]'/fs;
    % figure; 
    % subplot(2,3,[1,4])
    % plot(t,h,'linewidth',2); 
    % grid on; hold on
    % % axis([0.45 0.55 -2.5e-20 2.5e-20])
    % set(gca,'fontsize',16)
    % xlabel('time [s]')
    % ylabel('strain')
    % legend('H','L','V')
    % 
    % % ---- Sky map plots.
    % subplot(2,3,2)
    % handle2 = xproject(skyGrid, Enull, 'mollweid', ifos); 
    % colorbar
    % set(gca,'fontsize',16)
    % xlabel('phi [rad]')
    % ylabel('latitude [rad]')
    % title('E_{null}')
    %
    % subplot(2,3,3)
    % handle3 = xproject(skyGrid, Inull, 'mollweid', ifos); 
    % colorbar
    % set(gca,'fontsize',16)
    % xlabel('phi [rad]')
    % ylabel('latitude [rad]')
    % title('I_{null}')
    %
    % subplot(2,3,5)
    % handle4 = xproject(skyGrid, Enull-Inull, 'mollweid', ifos);
    % colorbar
    % set(gca,'fontsize',16)
    % xlabel('phi [rad]')
    % ylabel('latitude [rad]')
    % title('E_{null}-I_{null}')
    %
    % subplot(2,3,6)
    % handle5 = xproject(skyGrid, Enull./Inull-1, 'mollweid', ifos);
    % colorbar
    % set(gca,'fontsize',16)
    % xlabel('phi [rad]')
    % ylabel('latitude [rad]')
    % title('E_{null}/I_{null} - 1')

end


% ---- Done.
return

