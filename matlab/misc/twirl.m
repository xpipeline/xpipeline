function twirl(filename)
%TWIRL(FILENAME) Saves a movie of one complete rotation of the current
% figure

set(gca,'CameraViewAngleMode','manual');
for a = 0:719
    view(a, 30 * sin(2 * pi * a / 720));
    M(a+1) = getframe(gcf); % using figure handle saves whole figure rather 
end                       % than just interior of axes

movie2avi(M, filename, 'FPS', 30);