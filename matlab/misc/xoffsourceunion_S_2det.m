function outCluster = xoffsourceunion(dirName,fileListString,outFile)
% XOFFSOURCEUNION - finds triggers associated to long background events and unions them up into one
%
% usage:
%
%   xinjunion(dirName,fileList)
%
%   dirName    String. Directory containing all files to be read.
%   fileList   String. List of results files to be read. 
%   outFile    Optional string. File to output merged clusters to. If not
%              specified, original input file is over-written. 
%
% $Id$

% ---- Checks.
% ---- comment out to work in 2007a
% ---- careful this one doesnt work for matlabs older than 2013a
narginchk(2, 3);

% ---- Parse file list string into cell array of separate file names.
fileList = strread(fileListString,'%s');

% ---- Loop over all files to be merged.
for iFile = 1:length(fileList)
    fileName = [dirName '/' fileList{iFile}];
    s = load(fileName);

    % ---- find fake job numbers for each of the slides
    fjn = unique(s.cluster.fakeJobNumber);
    for m=1:length(fjn)

        % ---- Keep all elements with flag injectionProcessedMask == 1.
        index = find(s.cluster.fakeJobNumber==fjn(m));
        % ---- find nearby triggers in time and freq
        %bb = s.clusterInj(index).boundingBox;
        bb = s.cluster.boundingBox(index,:);
        Ntrig = size(bb,1);
        olp = zeros(Ntrig,Ntrig);
        indI = [];
        indJ = [];
        %win = [0 0 2 5]; %-- coincidence window
        win = [0 0 5 8]; %-- modified coincidence window
        for i=1:Ntrig-1
            for j=(i+1):Ntrig
                olp(i,j) = rectint(bb(i,:)+win,bb(j,:)+win);
                if olp(i,j) > 0
                    indI(end+1) = i;
                    indJ(end+1) = j;		
                end
            end
        end
        % ---- Combine pairs into two-column matrix.
        pairs = [indI' indJ'];
        %check pairs
        if isempty(pairs)
            disp('pairs are empty, we dont have overlapping boxes!');
        else
            disp('pairs are not empty!');
            %disp(pairs);
        end
    
        % ------------------------------------------------------------------------------

        % ---- Only proceed if we have triggers that need to be merged.
        if ~isempty(pairs)
        
            % ---- Split "pairs" matrix into two columns for convenience.
            pairsI = pairs(:,1);
            pairsJ = pairs(:,2);
            % ---- Complete list of triggers to be checked for membership of a generalised cluster.
            trigToCheck = 1:Ntrig;
            % ---- Storage for list of triggers in each generalised cluster.
            genClust = cell(Ntrig,1);
            % ---- Counter over generalised clusters.
            igenClust = 0;

            % ---- Loop over all Ntrig triggers, starting from the last one.
            while ~isempty(trigToCheck)    
                % ---- Start new generalsied cluster.
                igenClust = igenClust + 1;
                % ---- Select next trigger to check, from the end of the list.
                iTrig = trigToCheck(end);
                k = find(pairsI==iTrig | pairsJ==iTrig);
                % ---- Add all pairs contaiing this trigger to our current generalsied
                %      cluster, or just this one trigger if it is not piared with anyone else.
                if ~isempty(k)
                    tmp = pairs(k,:);
                    genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                    % ---- Delete all pairs containing this trigger from our working list.
                    pairs(k,:) = [];
                    pairsI(k,:) = [];
                    pairsJ(k,:) = [];
                else
                    genClust{igenClust} = iTrig;
                end
                % ---- We're finished with this trigger. Delete it from the list of
                %      to-be-checked and add it to the list of triggers we've checked. 
                trigToCheck = setdiff(trigToCheck,iTrig); 
                checkedTrigs = iTrig;
                % ---- By adding pairs to our generalsied cluser, we may have introduced new
                %      triggers. We now have to check each of the new triggers for any
                %      additional triggers they're clustered with that also have to be added ... 
                newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                while ~isempty(newtrigs)
                    for ii = 1:length(newtrigs)
                        k = find(pairsI==newtrigs(ii) | pairsJ==newtrigs(ii));
                        if ~isempty(k)
                            tmp = pairs(k,:);
                            genClust{igenClust} = union(genClust{igenClust},tmp(:)');
                            pairs(k,:) = [];
                            pairsI(k,:) = [];
                            pairsJ(k,:) = [];
                        end
                    end
                    checkedTrigs = union(checkedTrigs,newtrigs);
                    trigToCheck = setdiff(trigToCheck,checkedTrigs); % -- finished with this trigger
                    % --- Are there more new triggers?  If so, we go through the while loop
                    %     again.  If not, we go on to the next generalised cluster.
                    newtrigs = setdiff(genClust{igenClust},checkedTrigs);
                end        
            end

            % ---- Delete empty genClusts.
            genClust = genClust(1:igenClust);

            % ------------------------------------------------------------------------------

            % ---- Merge triggers to be clustered together.
            for igenClust=1:length(genClust)

                % ---- Triggers to be merged for this generalised cluster.
                trigIdx = genClust{igenClust};

                % ---- chop the initial trigger list and reshape the boundingBox
                % ---- apply threshold on number of pixels <20px, second trial ---- %
                newTriggers = xclustersubset(s.cluster,index(trigIdx));
                for p=1:length(newTriggers.nPixels)
                    if newTriggers.nPixels(p)<5
                        newTriggers.nPixels(p) = 1;
                        newTriggers.likelihood(p,:) = 0.0;
                        newTriggers.significance(p) = 0.0;
                    end
                end
                % ---- apply weighted average on number of pixels ---- %
                % ---- downgrade triggers in [350,450] Hz frequency band
                if length(trigIdx)>1
                    for g=1:length(trigIdx)
                        % ---- introduce a prefactor to rid off of significance < 2.75
                        % ---- and scale the off/inj by prf1234
                        if newTriggers.likelihood(g,2) > 3.00001 & (1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.boundingBox(g,4) + 0.0000001))) > 0.00000001 & newTriggers.boundingBox(g,4) > 4.0
                            % introduce basic coherence checks
                            % --------------------------------
                            % do alphaNullIoverE>1.82
                            %likelihood1 = newTriggers.likelihood(g,8:9);
                            %ratioArray1=zeros(2,2);
                            %ratioArray1(1,2) = 1.82;
                            %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                            % do circEoverI> 1.2
                            likelihood2 = newTriggers.likelihood(g,10:11);
                            ratioArray2=zeros(2,2);
                            ratioArray2(1,2) = 1.2;
                            coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                            % done coherent tests, get prefactors now
                            prf1 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.nPixels(g)));
                            prf2 = (newTriggers.boundingBox(g,3) + 0.0000001)/(log(newTriggers.likelihood(g,2)));
                            prf3 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.boundingBox(g,4) + 0.0000001));
                            %prf4 = 1.0 - sqrt(log(newTriggers.likelihood(g,2))/(newTriggers.likelihood(g,8)));
                            %prf9 = abs(newTriggers.likelihood(g,8) - newTriggers.likelihood(g,9))/newTriggers.nPixels(g);
                            prf = prf1*prf1*prf2*prf2*prf3*prf3*coh_prf2;%*prf4*prf9*prf9);
                        else
                            prf = 0.0;
                        end
                        disp(prf);
                        if newTriggers.peakFrequency(g)>340.0 & newTriggers.peakFrequency(g)<460.0 & newTriggers.peakFrequency(g)>680.0 & newTriggers.peakFrequency(g)<690.0
                            newTriggers.likelihood(g,:) = 0.01*newTriggers.likelihood(g,:)*newTriggers.nPixels(g)*prf;
                            newTriggers.significance(g) = 0.01*newTriggers.significance(g)*newTriggers.nPixels(g)*prf;
                        else
                            newTriggers.likelihood(g,:) = newTriggers.likelihood(g,:)*newTriggers.nPixels(g)*prf;
                            newTriggers.significance(g) = newTriggers.significance(g)*newTriggers.nPixels(g)*prf;
                        end
                    end
                    % ---- weigh by sum(nPixels)
                    newTriggers.likelihood(1,:) = (sum(newTriggers.likelihood))/(sum(newTriggers.nPixels));
                    newTriggers.significance(1) = (sum(newTriggers.significance))/(sum(newTriggers.nPixels));
                else
                    % ---- plop prf here again
                    if newTriggers.likelihood(1,2) > 3.00001 & (1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.boundingBox(1,4) + 0.0000001))) > 0.00000001 & newTriggers.boundingBox(1,4) > 4.0
                        % introduce basic coherence checks
                        % --------------------------------
                        % do alphaNullIoverE>1.82
                        %likelihood1 = newTriggers.likelihood(1,8:9);
                        %ratioArray1=zeros(2,2);
                        %ratioArray1(1,2) = 1.82;
                        %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                        % do circEoverI> 1.2
                        likelihood2 = newTriggers.likelihood(1,10:11);
                        ratioArray2=zeros(2,2);
                        ratioArray2(1,2) = 1.2;
                        coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                        % done coherent tests, get prefactors now
                        prf1 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.nPixels(1)));
                        prf2 = (newTriggers.boundingBox(1,3) + 0.0000001)/(log(newTriggers.likelihood(1,2)));
                        prf3 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.boundingBox(1,4) + 0.0000001));
                        %prf4 = 1.0 - sqrt(log(newTriggers.likelihood(1,2))/(newTriggers.likelihood(1,8)));
                        %prf9 = abs(newTriggers.likelihood(1,8) - newTriggers.likelihood(1,9))/newTriggers.nPixels(1);
                        prf = prf1*prf1*prf2*prf2*prf3*prf3*coh_prf2;%*prf4*prf9*prf9);
                    else
                        prf = 0.0;
                    end
                    disp(prf);
                    if newTriggers.peakFrequency(1)>340.0 & newTriggers.peakFrequency(1)<460.0 & newTriggers.peakFrequency(1)>680.0 & newTriggers.peakFrequency(1)<690.0
                        newTriggers.likelihood(1,:) = 0.01*newTriggers.likelihood(1,:)*prf;
                        newTriggers.significance(1) = 0.01*newTriggers.significance(1)*prf;
                    else
                        newTriggers.likelihood(1,:) = newTriggers.likelihood(1,:)*prf;
                        newTriggers.significance(1) = newTriggers.significance(1)*prf;
                    end
                end
                newTriggers.nPixels(1) = sum(newTriggers.nPixels);
                mint = min(newTriggers.boundingBox(:,1));
                maxt = max(newTriggers.boundingBox(:,1)+newTriggers.boundingBox(:,3));
                minf = min(newTriggers.boundingBox(:,2));
                maxf = max(newTriggers.boundingBox(:,2)+newTriggers.boundingBox(:,4));
                duration = maxt - mint;
                bw = maxf - minf;
                newTriggers.boundingBox(1,:) = [mint minf duration bw];
                newTriggers.peakTime(1,:) = mint + 0.5*duration;
                newTriggers.peakFrequency(1,:) = minf + 0.5*bw;

                % ---- Keep only first (merged) trigger.
                newTriggers = xclustersubset(newTriggers,1);
    
                if igenClust==1
                    clusterInj = newTriggers;
                else
                    clusterInj = xclustermerge(clusterInj,newTriggers);
                end
    
            end

        else 

            % ---- No merging needed for this fakeJobNumber. Use unedited triggers.
            % ---- but apply threshold on pixels ---- %
            clusterInj = xclustersubset(s.cluster,index);
            for r=1:length(clusterInj.nPixels)
                if clusterInj.nPixels(r)<5
                    clusterInj.nPixels(r) = 1;
                    clusterInj.likelihood(r,:) = 0.0;
                    clusterInj.significance(r) = 0.0;
                end
            end
            % ---- extra bit of messing with likelihoods ---- %
            % ---- downgrade in frequency band
            for a=1:length(clusterInj.peakFrequency)
                % ---- plop prf here again
                if clusterInj.likelihood(a,2) > 3.00001 & (1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.boundingBox(a,4) + 0.0000001))) > 0.00000001 & clusterInj.boundingBox(a,4) > 4.0
                    % introduce basic coherence checks
                    % --------------------------------
                    % do alphaNullIoverE>1.82
                    %likelihood1 = clusterInj.likelihood(a,8:9);
                    %ratioArray1=zeros(2,2);
                    %ratioArray1(1,2) = 1.82;
                    %coh_prf1 = xapplyalpharatiotest(likelihood1,ratioArray1);
                    % do circEoverI> 1.2
                    likelihood2 = clusterInj.likelihood(a,10:11);
                    ratioArray2=zeros(2,2);
                    ratioArray2(1,2) = 1.2;
                    coh_prf2 = xapplyratiotest(likelihood2,ratioArray2);
                    % done coherent tests, get prefactors now
                    prf1 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.nPixels(a)));
                    prf2 = (clusterInj.boundingBox(a,3) + 0.0000001)/(log(clusterInj.likelihood(a,2)));
                    prf3 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.boundingBox(a,4) + 0.0000001));
                    %prf4 = 1.0 - sqrt(log(clusterInj.likelihood(a,2))/(clusterInj.likelihood(a,8)));
                    %prf9 = abs(clusterInj.likelihood(a,8) - clusterInj.likelihood(a,9))/clusterInj.nPixels(a);
                    prf = prf1*prf1*prf2*prf2*prf3*prf3*coh_prf2;%*prf4*prf9*prf9);
                else
                    prf = 0.0;
                end
                disp(prf);
                if clusterInj.peakFrequency(a)>340.0 & clusterInj.peakFrequency(a)<460.0 & clusterInj.peakFrequency(a)>680.0 & clusterInj.peakFrequency(a)<690.0
                    clusterInj.likelihood(a,:) = 0.01*clusterInj.likelihood(a,:)*prf;
                    clusterInj.significance(a) = 0.01*clusterInj.significance(a)*prf;
                else
                    clusterInj.likelihood(a,:) = clusterInj.likelihood(a,:)*prf;
                    clusterInj.significance(a) = clusterInj.significance(a)*prf;
                end
            end
 
        end
        % ---- Save merged triggers for this fakeJobNumber into output array.
        if m==1
            outCluster = clusterInj;
        else
            outCluster = xclustermerge(outCluster,clusterInj);
        end
    end % ---- for loop on fakeJobNumber (fjn)

    % ---- Re-order field names to match original file.
    outCluster = orderfields(outCluster,s.cluster);
    % ---- Now save clusters back to original file.
    s.cluster = outCluster;

    if nargin<3
        outFile = fileName;
    end
    save(outFile, '-struct', 's');
    
end

% ---- Done.
return
