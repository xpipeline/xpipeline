% ---- Script extract efficiency curves from .fig files to a text file.

% ------------------------------------------------------------------------------
%    User-specified parameters.
% ------------------------------------------------------------------------------

% ---- Update each of these sections for your analysis.

% ---- List of web page "figfile" directories to loop over. Directory names
%      should end with "/'.
baseDir = {'/home/bonnierebecca.weaver/SN2020fqv/run2/auto_web/SN2020fqv_run2_closedbox_figfiles/' , ...
           '/home/bonnierebecca.weaver/SN2020dpw/run3/auto_web/SN2020dpw_run3_closedbox_figfiles/'};

% ---- List of injection sets to loop over and the default distance in kpc for
%      each. These must match the names (left-hand side of the 
%      <name>=<type>!<paramaters> line of the .ini file) and distances used in 
%      your .ini file. 
set = cell(0);  defaultDist = [];
set{end+1} = 's15a2o05';    defaultDist(end+1) = 10;
set{end+1} = 's15a2o09';    defaultDist(end+1) = 10;
set{end+1} = 's15a3o15';    defaultDist(end+1) = 10;
set{end+1} = 's15-yakunin'; defaultDist(end+1) = 10;
set{end+1} = 's15-ott';     defaultDist(end+1) = 10;
set{end+1} = 'pirom10d0eta0d3fac0d2';   defaultDist(end+1) = 1000;
set{end+1} = 'pirom10d0eta0d6fac0d2';   defaultDist(end+1) = 1000;
set{end+1} = 'pirom5d0eta0d3fac0d2';    defaultDist(end+1) = 1000;
set{end+1} = 'pirom5d0eta0d6fac0d2';    defaultDist(end+1) = 1000;
set{end+1} = 'barm0d2L60R10f400t100';   defaultDist(end+1) = 1000;
set{end+1} = 'barm0d2L60R10f400t1000';  defaultDist(end+1) = 1000;
set{end+1} = 'barm0d2L60R10f800t100';   defaultDist(end+1) = 1000;
set{end+1} = 'barm1d0L60R10f400t100';   defaultDist(end+1) = 1000;
set{end+1} = 'barm1d0L60R10f400t1000';  defaultDist(end+1) = 1000;
set{end+1} = 'barm1d0L60R10f800t25';    defaultDist(end+1) = 1000;
set{end+1} = 'muellerl15-2';    defaultDist(end+1) = 10;
set{end+1} = 'muellerl15-3';    defaultDist(end+1) = 10;
set{end+1} = 'muellern20-2';    defaultDist(end+1) = 10;
set{end+1} = 'muellerw15-2';    defaultDist(end+1) = 10;
set{end+1} = 'muellerw15-4';    defaultDist(end+1) = 10;

% ---- List of injection scales used. These must match your .ini file. 
scales = [0.0100,0.0147,0.0215,0.0316,0.0464,0.0681,0.1000,0.1468,0.2154, ...
          0.3162,0.4642,0.6813,1.0000,1.4678,2.1544,3.1623,4.6416,6.8129, ...
          10.0000,14.678,21.544,31.623,46.416,68.129,100.0]';

% ---- Base name for output text files. The files will be named as follows:
%        <outName>_efficiencies_1.txt  --> efficiencies for baseDir{1}. First
%            column is injection scale, remaining columns are efficiencies 
%            for each injection set.
%        <outName>_efficiencies_2.txt  --> efficiencies for baseDir{2}, etc. 
%        <outName>_amplitudes_1.txt  --> amplitudes for baseDir{1}. First 
%            column is injection scale, remaining columns are amplitudes 
%            (usually RSS) for each injection set for each injection scale.
%        <outName>_amplitudes_2.txt  --> amplitudes for baseDir{2}, etc. 
%        <outName>_distances_1.txt  --> distances for baseDir{1}. First
%            column is injection scale, remaining columns are distances (usually 
%            kpc) for each injection set.
%        <outName>_distances_2.txt  --> distances for baseDir{12}, etc. 
outName = 'bonnie';


% ------------------------------------------------------------------------------
%    You should not need to edit below this line.
% ------------------------------------------------------------------------------

% ---- Set path (CIT).
addpath /home/xpipeline/xpipeline/trunk/matlab/share/
addpath /home/xpipeline/xpipeline/trunk/matlab/searches/grb
addpath /home/xpipeline/xpipeline/trunk/matlab/utilities/  
addpath /home/xpipeline/xpipeline/trunk/matlab/misc/ 
addpath /home/xpipeline/opt/xpipeline/dev/share/fastclusterprop/
addpath /home/edaw/libframe/matlab

% ---- Loop over directories.
for iDir = 1:length(baseDir)

    disp(['Processing ' baseDir{iDir} ' ...'])

    % ---- Initialise storage for efficiencies, amplitudes, and distances for
    %      this directory. 
    eff{iDir} = zeros(length(scales),length(set));
    ampl{iDir} = zeros(length(scales),length(set));
    dist{iDir} = zeros(length(scales),length(set));
 
    % ---- Loop over waveform sets.
    for ii=1:length(set)

        disp(['... processing ' set{ii} ' ...'])

        % ---- Extract efficiency from figFile.
        figFile = [baseDir{iDir} 'eff_' lower(set{ii}) '.fig'];
        [tmp_eff, tmp_ampl] = efficiencyfromfigfile(figFile,length(scales));
        eff{iDir}(:,ii) = tmp_eff(:); 
        ampl{iDir}(:,ii) = tmp_ampl(:); 
        dist{iDir}(:,ii) =  defaultDist(ii) ./ scales(:); 

    end
    
    % ---- Spew output to screen to check for obvious problems.
    disp(' ... efficiencies by injection scale:')
    [scales eff{iDir}]
    
    % ---- Store results to plain-text files.
    fid = fopen([outName '_efficiencies_' num2str(iDir) '.txt'],'w');
    fprintf(fid,['%g' repmat(' %g',1,length(set)) '\n'],[scales eff{iDir}]');
    fclose(fid);
    %
    fid = fopen([outName '_amplitudes_' num2str(iDir) '.txt'],'w');
    fprintf(fid,['%g' repmat(' %g',1,length(set)) '\n'],[scales ampl{iDir}]');
    fclose(fid);
    %
    fid = fopen([outName '_distances_' num2str(iDir) '.txt'],'w');
    fprintf(fid,['%g' repmat(' %g',1,length(set)) '\n'],[scales dist{iDir}]');
    fclose(fid);

end

