function [medCohSNR medPenCohSNR nSky] = deriveCircSens(analysis,parametersCell,ulSNRlist,detSensCL99)

%iWave = find(strcmp(parametersCell{1},'2.5e-21~0.006667~150~0~0~0'));
iWave = find(strcmp(parametersCell{1},'2.500e-21~6.667e-03~1.500e+02~0.000e+00~0.000e+00~0.000e+00'))
%iWave = find(strcmp(parametersCell{1},'1.4~1.4~0~20'));

% ---- calibration corrections
scaleFactorH1 = 1.068;
scaleFactorH2 = 1.098;
scaleFactorL1 = 0.963;

% ---- Calibration correction are already taken into account when
%      constructing the detectors PSDs
sF(1) = 1; % scaleFactorL1;
sF(2) = 1; % scaleFactorH2;
sF(3) = 1;

clear expSNR fom fomPlus fomCross
nGRBs = length(analysis);
for iGRB = 1:nGRBs
  nDet(iGRB) = length(analysis{iGRB}.detectorList);
  nSky(iGRB) = length(analysis{iGRB}.skyPositionsTimes)/3;
  nWave = size(ulSNRlist{iGRB},1);

  % -- produce more sky position to average out correctly the time
  % dependence
  [firstTime iSky] = min(analysis{iGRB}.skyPositionsTimes);
  gridAtFirstTime = analysis{iGRB}.skyPositionsTimes == firstTime;
  [ra dec] = earthtoradec(analysis{iGRB}.skyPositions(gridAtFirstTime,2),...
                          analysis{iGRB}.skyPositions(gridAtFirstTime,1),...
                          firstTime);
  newSkyPos = zeros(132*size(ra,1),3);
  timeRange = firstTime + (-124:5:535);
  for iTime = 1:length(timeRange);
    [phi, theta] = radectoearth(ra,dec,timeRange(iTime));
    newSkyPos((iTime-1)*size(ra,1)+1:iTime*size(ra,1),:) =...
        [phi theta ...
         analysis{iGRB}.skyPositions(gridAtFirstTime,3)];
  end
  coordinates = newSkyPos(:,[1 2]);
  probability = newSkyPos(:,3)/sum(newSkyPos(:,3));

  % -- compute antenna response
  clear F
  for iDet = 1:nDet(iGRB)
    [Fp, Fc] = ComputeAntennaResponse(coordinates(:,1),coordinates(:,2),0,analysis{iGRB}.detectorList{iDet});
    F(:,iDet) = sqrt((Fp.^2 +  Fc.^2)/2);
  end
%  F = ((analysis{iGRB}.FpList(:).^2 + ...
%          analysis{iGRB}.FcList(:).^2)/2).^0.5';
%  F = repmat(F,[3 1]);
%  probability = [1/3 1/3 1/3]';

  clear expSNR
  fom = zeros(size(F,1),1);
  for iDet = 1:nDet(iGRB)
    % SNR expected for an injection scale at hrss50%
    expSNR(:,iDet) =  ulSNRlist{iGRB}(iWave,iDet)*detSensCL99(iGRB,iWave)*F(:,iDet);
    for jDet = 1:iDet-1
      fom = fom + 2*expSNR(:,iDet).^2.*expSNR(:,jDet).^2;
    end
  end
  fom = (fom./(sum(expSNR.^4,2)+sum(expSNR.^2,2).^2)*(nDet(iGRB)+1)/(nDet(iGRB)-1)).^0.25;
%fom = (fom./(sum(expSNR.^4,2))).^0.25;

  cohSNR = sortrows([probability sqrt(sum(expSNR.^2,2))]);
  penCohSNR = sortrows([probability sqrt(sum(expSNR.^2,2)).*fom]);
%penCohSNR = sortrows([probability sqrt(sum(expSNR.^2,2)).*min(fom,1)]);

  iMedCohSNR = find(cumsum(cohSNR(:,1))<0.5,1,'last');
  medCohSNR(iGRB) = mean(cohSNR(iMedCohSNR:iMedCohSNR+1,2));
  
  iMedPenCohSNR = find(cumsum(penCohSNR(:,1))<0.5,1,'last');
  medPenCohSNR(iGRB) = mean(penCohSNR(iMedPenCohSNR:iMedPenCohSNR+1,2));
end

