function [Enull,Einc] = predictSkyMapFD(sourcePositions,hrss,fMin,fMax, ...
    numberOfPixels,skyPositions,channel,S,F,varargin)
%
% predictSkyMapFD: Predict sky maps of the null and incoherent energies for
% an unpolarized GWB incident from a specified direction or set of directions.
% 
% This is a variant of predictSkyMap, generalized to work in the frequency
% domain using direction-dependent measures of the GWB properties (hrss and
% frequency range).  
%
% Usage:
% 
%  [Enull,Einc,Etot] = predictSkyMapFD(sourcePositions,hrss,fMin,fMax, ...
%     numberOfPixels,skyPositions,channel,powerSpectra,frequency)
%
%   sourcePositions      Array of trial sky positions of the source.  One
%                        sky position per row, with columns polar angle
%                        (rad, [0,\pi]) and azimuthal angle (rad, [0,2\pi])
%                        in Earth-based coordinates. 
%   hrss                 Vector of estimated hrss values of the GWB, one
%                        for each source position.  If a scalar, then the
%                        same value is used for all sourcePositions.
%	fMin                 Vector.  Minimum frequency (Hz) of the GWB estimated
%                        for each source position.  If a scalar, then the
%                        same value is used for all sourcePositions.
%	fMax                 Vector.  Maximum frequency (Hz) of the GWB estimated
%                        for each source position.  If a scalar, then the
%                        same value is used for all sourcePositions.
%   numberOfPixels       Vector.  Number of time-frequency pixels in the
%                        GWB estimated for each source position.  If a
%                        scalar, then the same value is used for all
%                        sourcePositions. (Needed to account for noise
%                        statistics.)  
%   skyPositions         Array of sky positions at which to predict
%                        likelihoods.  Same format as sourcePositions.
%   channel              Cell array of strings.  Names of detectors in the 
%                        network; e.g., {'H1','L1','V1'}.
%   powerSpectra         Array.  Each column holds the power spectrum of
%                        the corresponding detector.
%   frequency            Column vector.  Frequencies at which corresponding
%                        values of powerSpectra are measured.
%
%   Enull                Array of predicted null energy values for each 
%                        sky position x source position.
%   Einc                 Array of predicted incoherent energy values for each 
%                        sky position x source position.
%
% predictSkyMapUP models the GWB as an unpolarized white noise burst.
% Specifically, the GWB has equal power in each polarization, the two
% polarizations are ucorrelated, and the spectrum is white across a
% user-specified frequency band, [fMin,fMax].  Then
%
%   |h_+(f)| = |h_x(f)| = hrss/[2(fMax-fMin)^(1/2)]  for fMin <= f <= fMax
%                       = 0                          otherwise
%
%   < h_+(f)^* h_x(f) > = 0
%
% With the optional argument,value pair
%
%     predictSkyMapFD(...,'linearpolarization',psi)
%
% The GWB will be modelled as linearly polarized with trial polarization
% values listed in the vector psi.  The output maps will have a third
% dimension of length(psi) holding the maps for each value of psi.
%
%   |h_+(f)| = hrss/[2^(1/2)*(fMax-fMin)^(1/2)]  for fMin <= f <= fMax
%            = 0                                 otherwise
%   |h_x(f)| = 0
%
%   |h(f)| = |F_+ cos(2*psi) + F_x sin(2*psi)| * |h_+(f)|
%
% first version: Patrick J. Sutton 2006.03.27
% 
% $Id$


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Checks.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Check that other input arguments have valid sizes.
if (size(sourcePositions,2)<2)
    error('Array of test source positions must have at least two columns.')
end
if (size(skyPositions,2)<2)
    error('Array of output sky positions must have at least two columns.')
end

% ---- Check for positive cluster size.
if (numberOfPixels<=0)
    error('Degrees of freedom must be positive.')
end

% % ---- Assign default values to optional arguments.
baselines = [1:length(channel)];
sourcePolarizations = [];

% ---- Check for optional arguments.
if (nargin>9 && length(varargin))
    % ---- Make sure they are in pairs.
    if (length(varargin) == 2*floor(length(varargin)/2))
        % ---- Parse them.
        index = 1;
        while index<length(varargin)
            switch lower(varargin{index})
                case 'linearpolarization'
                    sourcePolarizations = varargin{index+1};
                case 'nullbaselines'
                    baselines = varargin{index+1};
                otherwise
                    error(['Property value ' varargin{index} ' not recognized.']);
            end
            index = index + 2;
        end
    else
        error('Optional arguments must appear in pairs: property_name, property_value.');
    end
end

if length(hrss)==1
	hrss = repmat(hrss, size(sourcePositions,1), 1);
end
if length(fMin)==1
	fMin = repmat(fMin, size(sourcePositions,1), 1);
end
if length(fMax)==1
	fMax = repmat(fMax, size(sourcePositions,1), 1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Network information.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Speed of light (m/s).
speedOfLight = 299792458;

% ---- Number of detectors.
numberOfDetectors = length(channel);

% ---- Load detector information.
for iChannel = 1:numberOfDetectors
    detector{iChannel} = LoadDetectorData(channel{iChannel});
end
% 	if isequal(channel{iChannel}, 'V1')
% 		S(:,iChannel) = SRD('Virgo-LV',F);
% 	else
% 		S(:,iChannel) = SRD('LIGO-LV',F);
% 	end
% warning on;


% ---- Compute baselines between detector pairs.
% ---- Detector baseline vectors.
base = [];
% ---- Detector baseline unit vectors.
bhat = [];
for iChannel = 1:numberOfDetectors-1
    for jChannel = iChannel+1:numberOfDetectors
        b = detector{iChannel}.V-detector{jChannel}.V;
        base = [base, b];
        if (norm(b)>1)
            bhat = [bhat, b/(b'*b)^0.5];
        else
            bhat = [bhat, zeros(size(b))];
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Quantities related to output sky positions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Row pointing vector to each sky position in output maps.
skyOmega = CartesianPointingVector(skyPositions(:,2),skyPositions(:,1));
numberOfSkyPositions = size(skyPositions,1);

% ---- Antenna responses for these sky positions.
skyFp = zeros(numberOfSkyPositions,numberOfDetectors);
skyFc = zeros(numberOfSkyPositions,numberOfDetectors);
for iChannel = 1:numberOfDetectors
    [skyFp(:,iChannel), skyFc(:,iChannel)] = ComputeAntennaResponse( ...
        skyPositions(:,2), skyPositions(:,1), ...
        zeros(numberOfSkyPositions,1), detector{iChannel}.d);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Quantities related to trial source positions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Row pointing vector to each trial source position.
sourceOmega = CartesianPointingVector(sourcePositions(:,2),sourcePositions(:,1));
numberOfSourcePositions = size(sourcePositions,1);
numberOfSourcePolarizations = length(sourcePolarizations);

% ---- Antenna responses for trial source positions.
sourceFp = zeros(numberOfSourcePositions,numberOfDetectors);
sourceFc = zeros(numberOfSourcePositions,numberOfDetectors);
for iChannel = 1:numberOfDetectors
    [sourceFp(:,iChannel), sourceFc(:,iChannel)] = ComputeAntennaResponse( ...
        sourcePositions(:,2), sourcePositions(:,1), ...
        zeros(numberOfSourcePositions,1), detector{iChannel}.d);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Compute likelihood maps.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ---- Storage for output.
Enull = zeros(numberOfSkyPositions,numberOfSourcePositions,max(numberOfSourcePolarizations,1));
Einc = zeros(numberOfSkyPositions,numberOfSourcePositions,max(numberOfSourcePolarizations,1));

    % ---- Loop over trial source positions.
    for jPosition = 1:numberOfSourcePositions
        
		% ---- Loop over frequencies where GWB is non-zero.
        inbandFreqIndex = find(F>=fMin(jPosition) & F<=fMax(jPosition));
        inbandFreqIndex = transpose(inbandFreqIndex(:)); %-- row vector
		for jFreq = inbandFreqIndex
                        
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %             Construct projection operator 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- Noise amplitudes of each detector relative to the first detector.
            sigma = (S(jFreq,:)/S(jFreq,1)).^(0.5);

			% ---- Convert antenna responses to DP frame.  (Note that these
			%      antenna responses have tthe correct relative weighting,
			%      by sigma, but not the correct absolute weighting, by S.)
			[skyFpDP skyFcDP psiDP] = ...
                convertToDominantPolarizationFrame(...
                skyFp * diag(sigma.^(-1)), skyFc * diag(sigma.^(-1)));

			% ---- Unit vectors in F+, Fx directions in DP frame.
			skyFpDPhat = skyFpDP ./ repmat(sum(skyFpDP.^2,2).^0.5,[1,numberOfDetectors]);
			skyFcDPhat = skyFcDP ./ repmat(sum(skyFcDP.^2,2).^0.5,[1,numberOfDetectors]);

			% ---- Projection operator used to compute null and incoherent energies for
			%      each sky position: 
			%          Enull(f) = \sum_{a,b} d_a^*(f) Q_ab(f) d_b(f)
			%      where in the dominant polarization frame
			%          Q = I - \hat{F+}\hat{F+}^T - \hat{Fx}\hat{Fx}^T 
			%      Compute Q matrix.  Store diagonal and below-diagonal ("cross") terms
			%      separately.
			% ---- Diagonal (incoherent) part.  Qdiagonal = [Q11, Q22, ...] 
			Qdiagonal = 1 - skyFpDPhat.^2 - skyFcDPhat.^2;
			% ---- Indices of off-diagonal terms. careful! must match order of bhat!
			crosstermIndices = find(tril(ones(length(detector)),-1));
			[Ir,Ic] =  find(tril(ones(numberOfDetectors),-1));
			% ---- Off-diagonal (coherent or cross-correlation) part.  
			%      Qcrossterms = [Q21, Q31, ..., QD1, Q32, ... , Q{D,D-1}] 
			Qcrossterms = zeros(numberOfSkyPositions,numberOfDetectors*(numberOfDetectors-1)/2);
			for index = 1:length(crosstermIndices)
			    Qcrossterms(:,index) = ...
			        - skyFpDPhat(:,Ir(index)).*skyFpDPhat(:,Ic(index)) ...
			        - skyFcDPhat(:,Ir(index)).*skyFcDPhat(:,Ic(index));
            end

            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %             Construct likelihood maps 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % ---- Incoherent energy (incoherent statistic).
            if (numberOfSourcePolarizations > 0)
                % ---- Linearly polarized signal model: loop over source
                %      polarizations.
                for jPolarization = 1:numberOfSourcePolarizations
                    % ---- Polarization angle of source relative to Earth frame.
                    psi_s = sourcePolarizations(jPolarization);                    
                    % ---- Antenna responses for this source position and polarization.
                    sourceFpEff = (cos(2*psi_s)*sourceFp(jPosition,:) ...
                        + sin(2*psi_s)*sourceFc(jPosition,:)) ;
                    % ---- Linearly polarized signal model.
                    Einc(:,jPosition,jPolarization) = 1/2 * Qdiagonal * ...
                        transpose( sourceFpEff.^2 .* S(jFreq,:).^(-1) ) ...
                        * hrss(jPosition).^2 ...
                        / (2*(fMax(jPosition)-fMin(jPosition)));
                end
            else
                % ---- Unpolarized signal model.
                Einc(:,jPosition) = 1/2 * Qdiagonal * transpose( ...
                        (sourceFp(jPosition,:).^2 + sourceFc(jPosition,:).^2) ...
                        .* S(jFreq,:).^(-1) ...
                    ) * hrss(jPosition).^2 ...
                    / (4*(fMax(jPosition)-fMin(jPosition)));
            end

	        % ---- Null energy, diagonal (autocorrelation) part.
	        Enull(:,jPosition,:) = Einc(:,jPosition,:);
            
	        % ---- Add in off-diagonal (cross-correlation) part, but only for
	        %      sky locations and baselines where time delay is correct. 
	        %      Remember the factor 2 from symmetry: ab and ba elements,
	        %      which cancels the factor 1/2 from using only f>0 bins.
	        for iBaseline = baselines 
				skyTimeDelays = (skyOmega*base(:,iBaseline) ...
                    - sourceOmega(jPosition,:)*base(:,iBaseline)) / speedOfLight;

                if (numberOfSourcePolarizations > 0)
                    % ---- Linearly polarized signal model: loop over source
                    %      polarizations.
                    for jPolarization = 1:numberOfSourcePolarizations
                        % ---- Polarization angle of source relative to Earth frame.
                        psi_s = sourcePolarizations(jPolarization);                    
                        % ---- Antenna responses for this source position and polarization.
                        sourceFpEff = (cos(2*psi_s)*sourceFp(jPosition,:) ...
                            + sin(2*psi_s)*sourceFc(jPosition,:)) ;
                        % ---- Linearly polarized signal model.
                        Enull(:,jPosition,jPolarization) = Enull(:,jPosition,jPolarization) ...
                            + cos(2*pi*F(jFreq)*skyTimeDelays) ... 
                            .* Qcrossterms(:,iBaseline) .* ( ...
                                sourceFpEff(Ir(iBaseline)) .* sourceFpEff(Ic(iBaseline)) ...
                            ) * (S(jFreq,Ir(iBaseline))*S(jFreq,Ic(iBaseline)))^(-1/2) ...
                            * hrss(jPosition).^2 / (2*(fMax(jPosition)-fMin(jPosition)));
                        
                    end
                else
                    % ---- Unpolarized signal model.
                    Enull(:,jPosition) = Enull(:,jPosition) ...
                        + cos(2*pi*F(jFreq)*skyTimeDelays) ... 
                        .* Qcrossterms(:,iBaseline) .* ( ...
                            sourceFp(Ir(iBaseline)) .* sourceFp(Ic(iBaseline)) ...
                            + sourceFc(Ir(iBaseline)) .* sourceFc(Ic(iBaseline)) ...
                        ) * (S(jFreq,Ir(iBaseline))*S(jFreq,Ic(iBaseline)))^(-1/2) ...
                        * hrss(jPosition).^2 / (4*(fMax(jPosition)-fMin(jPosition)));
                end
	        end

% 	        % ---- Make sure no null energies are < 0.  (Seen sometimes near edges of
% 	        %      ring intersections.)
% 	        k = find(Enull(:,jPosition,jPolarization)<0);
% 	        Enull(k,jPosition,jPolarization) = 0;

		end % -- loop over frequencies
		
    end  % -- loop over source positions

% end  % -- loop over source polarizations

% ---- Add mean noise contributions to each likelihood.
Enull = Enull + (numberOfDetectors - 2) * numberOfPixels;
Einc = Einc + (numberOfDetectors - 2) * numberOfPixels;

% ---- Done.
return;
