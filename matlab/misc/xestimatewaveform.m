function [hp, hc] = ...
    xestimatewaveform(channelNames, conditionedData, sampleFrequency, ...
            amplitudeSpectra, skyPositions, elementLength, ...
            transientLength, frequencyBands, analysisMode, verboseFlag, elementStart, elementStop)
%
% Need to include input args: elementStart elementStop (first and last
% elements for which signal is present).
%
% XESTIMATEWAVEFORM Extract best-fit gravitational waveforms for a given
% sky position
% 

% XMAPSKY generates a sky map describing the consistency of the signals observed
% in a network of three of more non-aligned detectors with the assumption that
% they are due to a gravitational-wave burst from a particular direction on the
% sky.  The sky map is generated for the requested list of sky positions and the
% requested analysis time scale and frequency bands.  To do so, the conditioned
% data is first partitioned into overlapping elements of the specified length.
% For each element, the signals from all detectors are then examined in the
% frequency domain for consistency with a burst coming from the specified sky
% positions.
%
% usage:
% 
%   [nullEnergyMap, incoherentEnergyMap, totalEnergyMap, degreesOfFreedom] = ...
%       xmapsky(channelNames, conditionedData, sampleFrequency, ...
%               amplitudeSpectra, skyPositions, elementLength, ...
%               transientLengths, frequencyBands, verboseFlag);
%
%    channelNames         cell array of channel name strings
%    conditionedData      matrix time domain conditioned data
%    sampleFrequency      sampling frequency of conditioned data [Hz]
%    amplitudeSpectra     matrix amplitude spectral densities [1/sqrt(Hz)]
%    skyPositions         matrix of sky positions [radians]
%    elementLength        transform length of fourier analyses [samples]
%    transientLength      duration of conditioning transients [samples]
%    frequencyBands       matrix of frequency bands to search over [Hz]
%    analysisMode         string specifying 'search', 'veto', or 'frankenveto' analysis
%    verboseFlag          boolean flag to control status output (default 0)
% 
%    nullEnergyMap        resulting null energy map array (see below)
%    incoherentEnergyMap  resulting incoherent energy map array (see below)
%    totalEnergyMap       resulting total energy map array (see below)
%    degreesOfFreedom     number of degrees of freedom for statistical testing
%
% The resulting sky maps are three dimensional array with the following form.
%
%   nullEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   incoherentEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%   totalEnergyMap(frequencyBandIndex, elementIndex, skyPositionIndex)
%
% The conditioned data should be provided as a matrix of time domain data with
% each channel in a separate column.
%
% The amplitude spectral densities should be provided as a matrix of one-sided
% frequency domain data at a frequency resolution corresponding to the desired
% element length and with each channel in a separate column.
%
% The desired sky position should be provided as a two column matrix of the form
% [theta phi], where theta is the geocentric colatitude running from 0 at the
% North pole to pi at the South pole and phi is the geocentric longitude in
% Earth fixed coordinates with 0 on the prime meridian.
%
% The desired frequency bands should be provided as a two column matrix of
% the form [lowFrequency highFrequency].
%
% See also XPIPELINE, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, and
% XINTERPRET.
%
% See also ComputeAntennaResponse, LoadDetectorData.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        process command line arguments                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check for sufficient command line arguments
% error(nargchk(9, 10, nargin));

% default arguments
% if nargin == 9,
%   verboseFlag = 0;
% end

% check that analysisMode is a recognized type
% if (~(strcmp(analysisMode,'veto') | strcmp(analysisMode,'search') | strcmp(analysisMode,'frankenveto')))
%   error('analysisMode is not a recognized type');
% end

% force one dimensional cell array
channelNames = channelNames(:);

% number of detectors
numberOfChannels = length(channelNames);

% validate number of detectors
if numberOfChannels < 3,
  error('data is required from three or more non-aligned detectors');
end

% number of null sums
numberOfNullSums = numberOfChannels - 2;

% validate conditioned data
if size(conditionedData, 2) ~= numberOfChannels,
  error('conditioned data is inconsistent with number of detectors');
end

% block length in samples
blockLength = size(conditionedData, 1);

% validate sky positions
if size(skyPositions, 2) < 2,
  error('sky positions must be a at least a two column matrix');
end
if any((skyPositions(:, 1) < 0) | (skyPositions(:, 1) > pi)),
  error('theta outside of [0, pi]');
end
if any((skyPositions(:, 2) < -pi) | (skyPositions(:, 2) >= pi)),
  error('phi outside of [-pi, pi)');
end

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% nyquist frequency
nyquistFrequency = sampleFrequency / 2;

% validate frequency bands
if size(frequencyBands, 2) ~= 2,
  error('frequency bands must be a two column matrix');
end
if max(max(frequencyBands)) > nyquistFrequency,
  error('requested frequency bands exceed nyquist frequency');
end
if any(diff(frequencyBands, 1, 2) <= 0),
  error('frequency bands have negative bandwidth');
end

% number of frequency bands to consider
numberOfFrequencyBands = size(frequencyBands, 1);

% validate integer power of two element length
if bitand(elementLength, elementLength - 1),
  error('element length is not an integer power of two');
end

% determine half element lengths
halfElementLength = elementLength / 2 + 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  partition block into overlapping elements                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% calculate the indices of the endpoints of the elements of length
% elementLength with 50% overlap
elementIndices = chunkIndices(blockLength, elementLength, transientLength);

% in 'veto' mode keep only the element overlapping the middle of the data 
% segment
if (strcmp(analysisMode,'veto'))
    elementIndices = elementIndices((size(elementIndices,1)+1)/2,:);
end

% Attack of the FrankenVeto monster!
if (strcmp(analysisMode,'frankenveto'))
    % Make sky maps for a small set of chunks centered on the event time.
    % Later combine over chunks into FrankenMaps!
    %
    % Trial: Make maps for +/-1/2 sec centered on event time.  
    % Hardwire overlapping of chunks.
    offset = ceil(0.5*elementLength);
    % Compute chunkIndices using transient time to eliminate all but an  
    % interval of duration 3*elementLength at the center of the block. 
    elementIndices = chunkIndices(blockLength, elementLength, ...
        floor(0.5*(blockLength-3*elementLength)), offset);
end

% keep only indices corresponding to [elementStart,elementStop]
elementIndices = [elementIndices(elementStart,1), elementIndices(elementStop,2)];

% number of elements
numberOfElements = size(elementIndices, 1);

% one-sided frequency vector for elements
elementFrequencies = nyquistFrequency * (0 : halfElementLength - 1) / ...
                     (halfElementLength - 1);

% frequency band indices into one-sided frequency domain element
frequencyIndices = cell(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  frequencyIndices{frequencyBandNumber} = ...
    [min(find(elementFrequencies >= frequencyBands(frequencyBandNumber, 1))) ...
     max(find(elementFrequencies <= frequencyBands(frequencyBandNumber, 2)))];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         determine degrees of freedom                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% number of degrees of freedom in each frequency band
degreesOfFreedom = zeros(numberOfFrequencyBands, 1);
for frequencyBandNumber = 1 : numberOfFrequencyBands,
  degreesOfFreedom(frequencyBandNumber) = ...
      diff(frequencyIndices{frequencyBandNumber});
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         preload detector information                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for iDet = 1:numberOfChannels
    % The detector is matched by the first character of the channel name.
    detData      = LoadDetectorData(channelNames{iDet}(1));
    % the position vectors
    rDet(iDet,:) = detData.V';
    % the cell array of detector response tensors
    dDet{iDet}   = detData.d;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        begin loop over sky positions                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% begin loop over sky positions
for skyPositionNumber = 1 : numberOfSkyPositions,

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                       determine detector response                            %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % compute detector responses for this sky position
  [Fplus, Fcross] = antennaPatterns(dDet, ...
                                    skyPositions(skyPositionNumber, :));
  timeShifts = computeTimeShifts(rDet, ...
                                 skyPositions(skyPositionNumber, :));

  % force row vectors
  Fplus = Fplus(:).';
  Fcross = Fcross(:).';
  timeShifts = timeShifts(:).';

  % determine integer sample shift and residual phase shift
  timeShiftLengths = timeShifts * sampleFrequency;
  integerTimeShiftLengths = round(timeShiftLengths);
  residualTimeShiftLengths = timeShiftLengths - integerTimeShiftLengths;
  residualTimeShiftPhases = exp(sqrt(-1) * 2 * pi * elementFrequencies.' * ...
                                residualTimeShiftLengths / sampleFrequency);

  % determine null sum coefficients
  V = zeros(2, numberOfChannels, halfElementLength);
  for frequencyIndex = 1 : halfElementLength,
      V(:, :, frequencyIndex) = pinv( ...
          [Fplus ./ amplitudeSpectra(frequencyIndex, :); ... 
           Fcross ./amplitudeSpectra(frequencyIndex, :)]' ...
      );
  end
  % V = permute(V, [3 2 1]);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                         begin loop over elements                             %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % begin loop over elements
  for elementNumber = 1 : numberOfElements
  
    % start and stop indices of element
    elementStartIndex = elementIndices(elementNumber, 1);
    elementStopIndex = elementIndices(elementNumber, 2);
  
    % extract time shifted time series for element
    element = zeros(elementLength, numberOfChannels);
    for channelNumber = 1 : numberOfChannels
      element(:, channelNumber) = ...
          conditionedData((elementStartIndex + ...
                           integerTimeShiftLengths(channelNumber)) : ...
                          (elementStopIndex + ...
                           integerTimeShiftLengths(channelNumber)), ...
                          channelNumber);
    end
    
    % apply fourier transform
    element = fft(element);
    
    % take one-sided transform
    element = element(1 : halfElementLength, :);
    
    % apply residual time shift
    element = element .* residualTimeShiftPhases;

    % construct plus and cross polarizations
    h  = zeros(2, halfElementLength);
    for frequencyIndex = 1 : halfElementLength,
        h(:, frequencyIndex) = V(:, :, frequencyIndex) * element(frequencyIndex, :).';
    end

    % convert to time domain.
    hfull = h';
    hfull = [hfull; conj(flipud(hfull(2:end-1,:)))];
    hfull_t = ifft(hfull);
    hp = real(hfull_t(:,1));
    hc = real(hfull_t(:,2));

%     % insert frequency band energies into sky maps
%     for frequencyBandNumber = 1 : numberOfFrequencyBands,
%       nullEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
%           diff(nullEnergy(frequencyIndices{frequencyBandNumber}));
%       incoherentEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
%           diff(incoherentEnergy(frequencyIndices{frequencyBandNumber}));
%       totalEnergyMap(frequencyBandNumber, elementNumber, skyPositionNumber) = ...
%           diff(totalEnergy(frequencyIndices{frequencyBandNumber}));
%     end
      
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %                          end loop over elements                              %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
  % end loop over elements
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         end loop over sky positions                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% end loop over sky positions
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          return to calling function                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% return to calling function
return;
