function blur = gaussblur(map,sigma,maxR)
% GAUSSBLUR - convolve a 2D array with a Gaussian
%
% usage:
%
%   blur = gaussblur(map,sigma,maxR)
%
% map     Input array.
% sigma   Scalar or two-element vector. One-sigma width of Gaussian.
% maxR    Scalar. Width of smoothing kernel.
%
% blur    Smoothed map.
%
% The output map is obtained by convolving the input map with a Gaussian
% with the specified sigma value. The convolution is limited to elements
% within +/- maxR of a given element in both dimensions. If sigma has two
% elements, the output map is the difference of the map obtained blurring
% with sigma(1) and than obtained by blurring with sigma(2).
%
% WARNING: This is NOT a particularly efficienct implementation (i.e., it
% is slow). It would be prudent to look to see if any more efficient
% implementations already exist.
%
% Patrick.Sutton@ligo.org
% $Id$

% ---- Prepare output.
blur = zeros(size(map));

% ---- Loop over input map.
for ii=1:size(map,1)
    for jj=1:size(map,2)
        idx = sub2ind(size(map),ii,jj);
        for ik=-maxR:maxR
            for jk=-maxR:maxR
                if ( (ii+ik)>=1 && (ii+ik)<=size(map,1) && (jj+jk)>=1 && (jj+jk)<=size(map,2) )
                    weight = (2*pi*sigma(1)^2)^(-1)*exp(-(ik^2+jk^2)/(2*sigma(1)^2));
                    if length(sigma==2)
                        weight = weight - (2*pi*sigma(2)^2)^(-1)*exp(-(ik^2+jk^2)/(2*sigma(2)^2));
                    end
                    idxk = sub2ind(size(map),ii+ik,jj+jk);
                    blur(idx) = blur(idx) + weight * map(idxk);
                end
            end
        end
    end
end

return
