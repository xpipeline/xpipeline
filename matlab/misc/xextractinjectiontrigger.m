injScales = [0];
%myFolders = {'output/simulations_adi-e_', 'output/simulations_adi-e_', 'output/simulations_adi-e_', 'output/simulations_adi-e_', 'output/simulations_adi-e_', 'output/simulations_adi-e_', 'output/simulations_adi-e_', 'output/simulations_adi-e_'};

%myFolders = {'output/simulations_adi-e_'};
%myFolders = {'/home/maxime.fays/XsphradTests/r4436_jw1_rerun/l_20/auto_web'};
%myFolders = {'/home/valeriu/public_html/JW1/JW1-H1L1V1_run3_LNSEP_PDSQ_BW_wPRF4'};
myFolders = {'../auto_web'};
% ---- for testing
%injScales = [17];
%myFolders = {'test_u/simulations_adi-e_'};

fid1 = fopen('adi-e_significance_0.txt','w');
fid2 = fopen('adi-e_flag1_0.txt','w');
fid3 = fopen('adi-e_flag2_0.txt','w');
fid4 = fopen('adi-e_flag3_0.txt','w');
fid6 = fopen('adi-e_flag4_0.txt','w');
fid5 = fopen('adi-e_scale_0.txt','w');
fid7 = fopen('adi-e_duration_0.txt','w');
fid8 = fopen('adi-e_bw_0.txt','w');
%fid6 = fopen('adi-e_significance_previousScale.txt','w');
%fid7 = fopen('adi-e_flag1_previousScale.txt','w');
%fid8 = fopen('adi-e_flag2_previousScale.txt','w');
fid9 = fopen('adi-e_npixels_0.txt','w');
fid10 = fopen('adi-e_nullE_0.txt','w');
fid11 = fopen('adi-e_nullEinc_0.txt','w');

for i=1:length(myFolders)
        for j=1:length(injScales)
                myFolder = [myFolders{i}];% num2str(injScales(j))];
                disp(myFolder);
                if ~isdir(myFolder)
                  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
                  disp(errorMessage);
                  return;
                end
                filePattern = fullfile(myFolder, 'JW1-H1L1V1_run3_SC5_PrfSignifOnly_closedboxassociatedTriggers_adi-e.mat');
                matFiles = dir(filePattern);
                for k = 1:length(matFiles)
                  baseFileName = matFiles(k).name;
                  disp(baseFileName);
                  fullFileName = fullfile(myFolder, baseFileName);
                  [injAssTrig, injProcMask, injScale] = loadinjassoctrigfile(fullFileName);
                  ntriggers = length(injProcMask(:,27));
                  for m = 12:12
                    for n = 1:ntriggers
		      if length(injAssTrig(n,m).pass) == 1
                        %fprintf(fid7, '%f\n', injAssTrig(n,m).significance)
                        %if s.injAssociatedTrigger(n,m).significance < 19.35
                        if injProcMask(n,m) == 1
                          fprintf(fid1, '%f\n', injAssTrig(n,m).likelihood(:,4));
                          fprintf(fid2, '%i\n', injAssTrig(n,m).passVetoSegs);
                          fprintf(fid3, '%i\n', injAssTrig(n,m).passInjCoinc);
                          fprintf(fid4, '%i\n', injAssTrig(n,m).passRatio);
                          fprintf(fid6, '%i\n', injAssTrig(n,m).passFixedRatio);
                          fprintf(fid5, '%f\n', injScale(m));
                          disp(injScale(m));
                          fprintf(fid7, '%f\n', injAssTrig(n,m).boundingBox(:,3));
                          fprintf(fid8, '%f\n', injAssTrig(n,m).boundingBox(:,4));
                          fprintf(fid9, '%i\n', injAssTrig(n,m).nPixels);
                          fprintf(fid10, '%f\n', injAssTrig(n,m).likelihood(:,8));
                          fprintf(fid11, '%f\n', injAssTrig(n,m).likelihood(:,9));
                        %end
                        end
                      end

                    end
                  end
                  fprintf(1, 'Done reading %s\n', fullFileName);
                end
        end
end
fclose(fid1);
fclose(fid2);
fclose(fid3);
fclose(fid4);
fclose(fid6);
fclose(fid5);
fclose(fid7);
fclose(fid8);
%fclose(fid6);
%fclose(fid7);
%fclose(fid8);
fclose(fid9);
fclose(fid10);
fclose(fid11);
