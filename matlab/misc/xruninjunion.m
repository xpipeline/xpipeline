


% ---- production
injScales = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26];
myFolders = {'output/simulations_adi-a_', 'output/simulations_adi-b_', 'output/simulations_adi-c_', 'output/simulations_adi-d_', 'output/simulations_adi-e_', 'output/simulations_ebbh-a_', 'output/simulations_ebbh-d_', 'output/simulations_ebbh-e_','output/simulations_mva_'};

% ---- testing
%injScales = [10];
%myFolders = {'output/simulations_adi-e_'};
%myFolders = {'output/simulations_ebbh-d_', 'output/simulations_ebbh-e_'};
%myFolders = {'output/simulations_adi-b_', 'output/simulations_adi-c_', 'output/simulations_adi-d_', 'output/simulations_adi-e_', 'output/simulations_ebbh-a_', 'output/simulations_ebbh-d_', 'output/simulations_ebbh-e_'};
%injScales = [17];
%myFolders = {'test_u/simulations_ebbh-d_'};
% ---- end testing

for i=1:length(myFolders)
	for j=1:length(injScales)
		myFolder = [myFolders{i} num2str(injScales(j))];
		disp(myFolder);
		if ~isdir(myFolder)
		  errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
		  disp(errorMessage);
		  return;
		end
		filePattern = fullfile(myFolder, '*.mat');
		matFiles = dir(filePattern);
		for k = 1:length(matFiles)
		  baseFileName = matFiles(k).name;
		  disp(baseFileName);
		  fullFileName = fullfile(myFolder, baseFileName);
		  fprintf(1, 'Now reading %s\n', fullFileName);
		  xinjunion_S(myFolder, baseFileName);
		  fprintf(1, 'Done reading %s\n', fullFileName);
		end
	end
end
