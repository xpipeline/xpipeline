function t = spearman(x, y)
% Spearman's rank correlation coefficent 

n = length(x);
r = corr(x, y, 'type', 'Spearman');
t = r * sqrt((n - 2) / (1 - r^2));