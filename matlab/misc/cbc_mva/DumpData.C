// DumpData.C
//
// This macro provides a simple example on how to dump out classifier values.
//
// Adapted from earlier codes by Thomas Adams (10/07/14) and James Clark (11/02/11). 

#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <stdio.h>

#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TPluginManager.h"
#include "TStopwatch.h"

//#include "TMVAGui.C"

#if not defined(__CINT__) || defined(__MAKECINT__)
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#endif

using namespace TMVA;

void DumpData( TString myMethodList = "", TString outputDir = "", string sLikelihoodsFile="likelihoods.dat")
{
    //============================================================
    // default MVA methods to be trained + tested
    //============================================================

    // this loads the library
    TMVA::Tools::Instance();

    std::map<std::string,int> Use;

    Use["Cuts"]            = 0;
    Use["CutsD"]           = 0;
    Use["CutsPCA"]         = 0;
    Use["CutsGA"]          = 0;
    Use["CutsSA"]          = 0;
    // ---
    Use["Likelihood"]      = 0;
    Use["LikelihoodD"]     = 0; // the "D" extension indicates decorrelated input variables (see option strings)
    Use["LikelihoodPCA"]   = 0; // the "PCA" extension indicates PCA-transformed input variables (see option strings)
    Use["LikelihoodKDE"]   = 0;
    Use["LikelihoodMIX"]   = 0;
    // ---
    Use["PDERS"]           = 0;
    Use["PDERSD"]          = 0;
    Use["PDERSPCA"]        = 0;
    Use["PDERSkNN"]        = 0; // depreciated until further notice
    Use["PDEFoam"]         = 0;
    // --
    Use["KNN"]             = 0;
    // ---
    Use["HMatrix"]         = 0;
    Use["Fisher"]          = 0;
    Use["FisherG"]         = 0;
    Use["BoostedFisher"]   = 0;
    Use["LD"]              = 0;
    // ---
    Use["FDA_GA"]          = 0;
    Use["FDA_SA"]          = 0;
    Use["FDA_MC"]          = 0;
    Use["FDA_MT"]          = 0;
    Use["FDA_GAMT"]        = 0;
    Use["FDA_MCMT"]        = 0;
    // ---
    Use["MLP"]             = 0; // this is the recommended ANN
    Use["MLPBFGS"]         = 0; // recommended ANN with optional training method
    Use["CFMlpANN"]        = 0; // *** missing
    Use["TMlpANN"]         = 0;
    // ---
    Use["SVM"]             = 0;
    // ---
    Use["BDT"]             = 1;
    Use["BDTD"]            = 0;
    Use["BDTG"]            = 0;
    Use["BDTB"]            = 0;
    // ---
    Use["RuleFit"]         = 0;
    // ---
    Use["Plugin"]          = 0;
    //============================================================

    std::cout << std::endl;
    std::cout << "==> Starting DumpData.C" << std::endl;

    if (myMethodList != "") {
        for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) it->second = 0;

        std::vector<TString> mlist = gTools().SplitString( myMethodList, ',' );
        for (UInt_t i=0; i<mlist.size(); i++) {
            std::string regMethod(mlist[i]);

            if (Use.find(regMethod) == Use.end()) {
                std::cout << "Method \"" << regMethod << "\" not known in TMVA under this name. Choose among the following:" << std::endl;
                for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) std::cout << it->first << " ";
                std::cout << std::endl;
                return;
            }
            Use[regMethod] = 1;
        }
    }

    //============================================================
    // create the Reader object
    //============================================================
    TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );

    Float_t var1;
    Float_t var2;
    Float_t var3;
    Float_t var4;
    Float_t var5;
    Float_t var6;
    Float_t var7;
    Float_t var8;
    Float_t var9;
    Float_t var10;
    Float_t var11;
    Float_t var12;
    Float_t var13;
    Float_t var14;
    Float_t var15;
    Float_t var16;
    Float_t var17;
    //UInt_t  CoincPage;
    //UInt_t  CoincID;
    Float_t CoincPage;
    Float_t CoincID;

    //============================================================
    // Variables
    //============================================================

    reader->AddVariable( "Mass1",   &var1);
    reader->AddVariable( "Mass2",   &var2);
    reader->AddVariable( "Spin1z",   &var3);
    reader->AddVariable( "Spin2z",   &var4);
    reader->AddVariable( "Templateid",   &var5);
    reader->AddVariable( "Chisq_dof",   &var6);
    reader->AddVariable( "Templateduration",   &var7);
    reader->AddVariable( "L1chisq",   &var8);
    reader->AddVariable( "L1coaphase",   &var9);
    reader->AddVariable( "L1endtime",   &var10);
    reader->AddVariable( "L1sigmasq",   &var11);
    reader->AddVariable( "L1snr",   &var12);
    reader->AddVariable( "H1chisq",   &var13);
    reader->AddVariable( "H1coaphase",   &var14);
    reader->AddVariable( "H1endtime",   &var15);
    reader->AddVariable( "H1sigmasq",   &var16);
    reader->AddVariable( "H1snr",   &var17);
    reader->AddVariable( "CoincPage",   &CoincPage);
    reader->AddVariable( "CoincID",   &CoincID);
    cout << "KLUDGE!" << endl;
//  reader->AddSpectator( "CoincPage",   &CoincPage);
//  reader->AddSpectator( "CoincID",   &CoincID);
    // ---- Read likelihoods from file.
    // ---- Assign storage for more variables than we're going to need.
//  Float_t Var[100], Spec[100];
    // ---- Open likelihoods file.
//  ifstream LikelihoodsFile;
//  LikelihoodsFile.open(sLikelihoodsFile.c_str());
    // ---- Verify that file is present.
//  if (!LikelihoodsFile) {
//      cout << "Unable to open file " << sLikelihoodsFile.c_str()  << "\n";
//      exit(8);
//  }
    // ---- Read likelihoods and add as variables. Following xtmvaCalssification.py, 
    //      we ignore sky position, add log() for any non-log variable, and add the 
    //      raw variable if it is named log* already.
//  int entryno = 0;
//          reader->AddVariable( temp, &Var[entryno]);
//          entryno++;
//      }

    //============================================================
    // book the MVA methods
    //============================================================
    TString dir    = outputDir + "/weights/";
    TString prefix = "xtmvaClassification";

    for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) {
        if (it->second) {
            TString methodName = it->first + " method";
            TString weightfile = dir + prefix + "_" + TString(it->first) + ".weights.xml";
            reader->BookMVA( methodName, weightfile ); 
        }
    }

    //============================================================
    // Prepare input tree (this must be replaced by your data source)
    // in this example, there is a toy tree with signal and one with background events
    // we'll later on use only the "signal" events for the test in this example.
    //============================================================
    TFile *input(0);

    //============================================================
    TString fname = outputDir + "/" + "TMVA_GRB_Output.root";

    input = TFile::Open( fname ); // check if file in local directory exists
    if (!input) {
        std::cout << "ERROR: could not open data file" << std::endl;
        exit(1);
    }
    std::cout << "--- Using input file: " << input->GetName() << std::endl;

    //============================================================
    // prepare the tree
    // - here the variable names have to corresponds to your tree
    // - you can use the same variables as above which is slightly faster,
    //   but of course you can use different ones and copy the values inside the event loop

    //============================================================
    // For the training data
    //============================================================
    std::cout << "--- Select Training sample" << std::endl;
    TTree* trainTree = (TTree*)input->Get("TrainTree");

    // ---- Open output file data streams.
    ofstream trainSig;
    trainSig.open("temp_signal_training.txt");
    trainSig.precision(8);
    ofstream trainBack;
    trainBack.open("Output_background_training.txt");
    trainBack.precision(8);

    // ---- Hard-coded list of trigger properties.
    trainTree->SetBranchAddress( "Mass1",   &var1);
    trainTree->SetBranchAddress( "Mass2",   &var2);
    trainTree->SetBranchAddress( "Spin1z",   &var3);
    trainTree->SetBranchAddress( "Spin2z",   &var4);
    trainTree->SetBranchAddress( "Templateid",   &var5);
    trainTree->SetBranchAddress( "Chisq_dof",   &var6);
    trainTree->SetBranchAddress( "Templateduration",   &var7);
    trainTree->SetBranchAddress( "L1chisq",   &var8);
    trainTree->SetBranchAddress( "L1coaphase",   &var9);
    trainTree->SetBranchAddress( "L1endtime",   &var10);
    trainTree->SetBranchAddress( "L1sigmasq",   &var11);
    trainTree->SetBranchAddress( "L1snr",   &var12);
    trainTree->SetBranchAddress( "H1chisq",   &var13);
    trainTree->SetBranchAddress( "H1coaphase",   &var14);
    trainTree->SetBranchAddress( "H1endtime",   &var15);
    trainTree->SetBranchAddress( "H1sigmasq",   &var16);
    trainTree->SetBranchAddress( "H1snr",   &var17);
    trainTree->SetBranchAddress( "CoincPage",   &CoincPage);
    trainTree->SetBranchAddress( "CoincID",   &CoincID);
    //*********************************************************************

    std::cout << "********************************************************" << std::endl;
    std::cout << " ATTEMPTING DUMP OF TREE CONTENTS WITH PRINT()          " << std::endl;
    std::cout << "********************************************************" << std::endl;
    trainTree->Print();
    std::cout << "********************************************************" << std::endl;

    std::cout << "--- Processing: " << trainTree->GetEntries() << " events" << std::endl;
    TStopwatch sw;
    sw.Start();
    for (Long64_t ievt=0; ievt<trainTree->GetEntries();ievt++) {
        if (ievt%1000 == 0){
            std::cout << "--- ... Processing event: " << ievt << std::endl;
        }
        trainTree->GetEntry(ievt);

/*
        if (Use["MLP"  ]) {
             if( injIdx[ievt] != 0) {
                trainSig << (((reader->EvaluateMVA( "MLP method" )) + 1.25) / 2.75) * 100.0 << "  " << injIdx[ievt] << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << trainSig::endl;
             }
             else {
                trainBack << (((reader->EvaluateMVA( "MLP method" )) + 1.25) / 2.75) * 100.0 << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << trainBack::endl;
             }
        }
*/

        if (Use["BDT"  ]) {
          if (ievt%1000 == 0){
            //cout << (reader->EvaluateMVA( "BDT method" )) << endl;
            //trainSig << CoincID[ievt] << " " << (reader->EvaluateMVA( "BDT method" )) << endl;
            trainSig << (reader->EvaluateMVA( "BDT method" )) << endl;
          }
          //trainSig << "Foo! " << endl;
          //trainSig << (((reader->EvaluateMVA( "BDT method" )) + 0.80) / 1.60  * 100.0 << trainSig::endl;
//          cout << " At line 282 ... " << endl;
/*
            if( CoincPage[ievt] != 0) {
               trainSig << (((reader->EvaluateMVA( "BDT method" )) + 0.8) / 1.6) * 100.0 << "  " << CoincPage[ievt] << "  " << CoincID[ievt] << trainSig::endl;
            }
            else {
               trainBack << (((reader->EvaluateMVA( "BDT method" )) + 0.8) / 1.6) * 100.0 << "  " << CoincPage[ievt] << "  " << CoincID[ievt] << trainBack::endl;
            }
*/
        }

/*
        if (Use["BDTG" ]) {
            if( injIdx[ievt] != 0) {
               trainSig << ((reader->EvaluateMVA( "BDTG method" )) + 1.0) * 50.0 << "  " << injIdx[ievt] << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << trainSig::endl;
            }
            else {
               trainBack << ((reader->EvaluateMVA( "BDTG method" )) + 1.0) * 50.0 << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << trainBack::endl;
            }
        }
*/
    }
    trainSig.close();
    trainBack.close();

    //============================================================
    // prepare the tree
    // - here the variable names have to corresponds to your tree
    // - you can use the same variables as above which is slightly faster,
    //   but of course you can use different ones and copy the values inside the event loop

    //============================================================
    // For the testing data
    //============================================================

    std::cout << "--- Select Testing sample" << std::endl;
    TTree* testTree = (TTree*)input->Get("TestTree");

    // ---- Open output file streams.
    ofstream testSig;
    ofstream testBack;
    ofstream onSource;
    testSig.open("temp_signal_testing.txt");
    testSig.precision(8);
    testBack.open("Output_background_testing.txt");
    testBack.precision(8);
    onSource.open("Output_on_source.txt");
    onSource.precision(8);

    // ---- Hard-coded list of trigger properties.
    testTree->SetBranchAddress( "Mass1",   &var1 );
    testTree->SetBranchAddress( "Mass2",   &var2 );
    testTree->SetBranchAddress( "Spin1z",   &var3 );
    testTree->SetBranchAddress( "Spin2z",   &var4 );
    testTree->SetBranchAddress( "Templateid",   &var5 );
    testTree->SetBranchAddress( "Chisq_dof",   &var6 );
    testTree->SetBranchAddress( "Templateduration",   &var7 );
    testTree->SetBranchAddress( "L1chisq",   &var8 );
    testTree->SetBranchAddress( "L1coaphase",   &var9 );
    testTree->SetBranchAddress( "L1endtime",   &var10 );
    testTree->SetBranchAddress( "L1sigmasq",   &var11 );
    testTree->SetBranchAddress( "L1snr",   &var12 );
    testTree->SetBranchAddress( "H1chisq",   &var13 );
    testTree->SetBranchAddress( "H1coaphase",   &var14 );
    testTree->SetBranchAddress( "H1endtime",   &var15 );
    testTree->SetBranchAddress( "H1sigmasq",   &var16 );
    testTree->SetBranchAddress( "H1snr",   &var17 );
    testTree->SetBranchAddress( "CoincPage",   &CoincPage );
    testTree->SetBranchAddress( "CoincID",   &CoincID );
    //*******************************************************************
    std::cout << "--- Processing: " << testTree->GetEntries() << " events" << std::endl;
    //TStopwatch sw;
    //sw.Start();
    for (Long64_t ievt=0; ievt<testTree->GetEntries();ievt++) {
        if (ievt%1000 == 0) {
            std::cout << "--- ... Processing event: " << ievt << std::endl;
        }

        testTree->GetEntry(ievt);
/*
        if (Use["MLP"  ]) {
            if( injIdx[ievt] != 0) {
               testSig << (((reader->EvaluateMVA( "MLP method" )) + 1.25) / 2.75) * 100.0 << "  " << injIdx[ievt] << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << "  " << matfile[ievt] << testSig::endl;
            }
            else {
                if( jobNum[ievt] != 0) {
                    testBack << (((reader->EvaluateMVA( "MLP method" )) + 1.25) / 2.75) * 100.0 << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << testBack::endl;
                }
                else {
                    onSource << (((reader->EvaluateMVA( "MLP method" )) + 1.25) / 2.75) * 100.0 << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << onSource::endl;
                }
            }
        }
*/

//      cout << "KLUDGE - Skipping!" << endl;
/*
        if (Use["BDT"  ]) {
//          cout << " At line 378 ... " << endl;
            if( CoincPage[ievt] != 0) {
                testSig << (((reader->EvaluateMVA( "BDT method" )) + 0.8) / 1.6) * 100.0 << "  " << CoincPage[ievt] << "  " << CoincID[ievt] << testSig::endl;
            }
            else {
//              if( CoincPage[ievt] != 0) {
                    testBack << (((reader->EvaluateMVA( "BDT method" )) + 0.8) / 1.6) * 100.0 << "  " << CoincPage[ievt] << "  " << CoincID[ievt] << testBack::endl;
//              }
//              else {
//                  onSource << (((reader->EvaluateMVA( "BDT method" )) + 0.8) / 1.6) * 100.0 << "  " << CoincPage[ievt] << "  " << CoincID[ievt] << testSig::endl;
//              }
            }
        }
*/

/*
        if (Use["BDTG" ]) {
            if( injIdx[ievt] != 0) {
                testSig << ((reader->EvaluateMVA( "BDTG method" )) + 1.0) * 50.0 << "  " << injIdx[ievt] << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << "  " << matfile[ievt] << testSig::endl;
            }
            else {
                if( jobNum[ievt] != 0) {
                    testBack << ((reader->EvaluateMVA( "BDTG method" )) + 1.0) * 50.0 << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << testBack::endl;
                }
                else {
                    onSource << ((reader->EvaluateMVA( "BDTG method" )) + 1.0) * 50.0 << "  " << triggerIdx[ievt] << "  " << uniqIdx[ievt] << onSource::endl;
                }
            }
        }
*/
    }
    // get elapsed time
    sw.Stop();
    std::cout << "--- End of event loop: "; sw.Print();

    testSig.close();
    testBack.close();
    onSource.close();

    delete reader;

    std::cout << "==> Finished DumpData.C" << std::endl;
}
  
