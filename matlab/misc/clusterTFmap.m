function [clusters, clusters_Struct] = clusterTFmap(map, clusterType, clusterParam1, clusterParam2, mapDim)
% clusterTFmap - Generate clusters of events from a time-frequency map.
%
% [clusters clustersStruct] = clusterTFmap(map,type,param1,param2,mapDim)
% 
%   map         Two-dimensional array of time-frequency map values.  
%               Dimension 1 = frequency, dimension 2 = time.
%   type        String.  Clustering method.  See below.
%   param1      Parameter to modify clustering.  See below.
%   param2      Parameter to modify clustering.  See below.
%   mapDim      Optional.  4-element vector [T0, dT, F0, dF] where: 
%                 T0 is the central time (sec) of the first column of map, 
%                 dT is the time (sec) between consecutive columns of map, 
%                 F0 is the central frequency (Hz) of the first row of map, 
%                 dF is the frequency interval (Hz) between rows of map. 
%
%   clusters    mx8 matrix (m is # of clusters)
%               column 1: minimum time of cluster
%               column 2: weighted center time of cluster
%               column 3: maximum time of cluster
%               column 4: minimum frequency of cluster
%               column 5: weighted center frequency of cluster
%               column 6: maximum frequency of cluster
%               column 7: summed map values over cluster
%               column 8: number of pixels in cluster
%               All times and frequencies are in units of bins, unless 
%               mapDim specified, in which case output is in sec and Hz.
%   clustersStruct  Struct array containing regionprops properties for
%               clusters.  See regionprops command.  Non-empty only for
%               'connected' clustering type.
%
% Usage details:
%
% clusterTFmap(map,'maxOverFrequency') outputs one event cluster per time
% bin in the input map.  Each cluster is constructed from the single pixel
% with the largest map value in that time bin.
%
% clusterTFmap(map,'sumOverFrequency') outputs one event cluster per time
% bin in the input map.  Each cluster is constructed from the sum of all
% pixels in that time bin.
%
% clusterTFmap(map,'connected',bpp,connectivityNumber), where bpp ("black
% pixel probability") is a scalar in (0,1) [default 0.01] and
% connectivityNumber is either 4 or 8, creates event clusters in a two-step
% procedure. First, the map pixels are converted to logicals by the
% operation (map > threshold), where threshold is chosen so that the
% fraction bpp of highest map values are above it ("black pixels").  Then,
% black pixels are clustered together using the bwlabel function and the
% specified connectivityNumber (4 = nearest neighbors only, 8 = nearest and
% next-nearest neighbors [default]).  See bwlabel for more details.
%
% clusterTFmap(map,'hierarchical',bpp,distanceThreshold), where bpp ("black
% pixel probability") is a scalar in (0,1) [default 0.01] and
% distanceThreshold is a scalar, creates event clusters in a two-step
% procdure. First, the map is thresholded as for 'connected' clustering.
% Then, black pixels are clustered together hierarchically using the
% functions pdist ('cityblock' metric), linkage, and cluster (with 'cutoff'
% value of distanceThreshold [default 3]).
%
% initial write: Stephen Poprocki
%
% $Id$

% ---- Check for valid number command line arguments
error(nargchk(2, 5, nargin));

switch(clusterType)

    case 'connected'
        
        % ---- Set connectivityNumber.
        if (nargin>=4)
            connectivityNumber = clusterParam2;
        else
            connectivityNumber = 8;
        end
        if (connectivityNumber~=4 & connectivityNumber~=8)
            error('connectivityNumber must be 4 or 8.  See bwlabel.')
        end

        % ---- Calculate bitmapThreshold based on percentile of measured
        %      map values.
        if (nargin>=3)
            percentile = (1 - clusterParam1)*100;
        else
            percentile = 99.0;
        end
        bitmapThresh = prctile(map(:),percentile);

        % ---- Threshold to create bitmap.
        bitmap = map > bitmapThresh;

        % ---- Identify clusters and generate labels. 
        [labelledbitmap,numberOfTriggers] = bwlabel(bitmap,connectivityNumber);

        % ---- Compute cluster properties.
        clusters_Struct = regionprops(labelledbitmap,'Area','BoundingBox', ...
            'PixelIdxList','PixelList');
        %
        clusters_BoundingBox = reshape([clusters_Struct.BoundingBox],4,[])';
        clusters_tmin = clusters_BoundingBox(:,1);
        clusters_tmax = clusters_BoundingBox(:,1)+clusters_BoundingBox(:,3);
        clusters_fmin = clusters_BoundingBox(:,2);
        clusters_fmax = clusters_BoundingBox(:,2)+clusters_BoundingBox(:,4);
        %
        clusters_Likelihood = zeros(numberOfTriggers,1);
        clusters_tpeak = zeros(numberOfTriggers,1);
        clusters_fpeak = zeros(numberOfTriggers,1);
        for clusterNumber = 1:numberOfTriggers
            pixels = clusters_Struct(clusterNumber).PixelIdxList;
            pixelsT = clusters_Struct(clusterNumber).PixelList(:,1);
            pixelsF = clusters_Struct(clusterNumber).PixelList(:,2);
            clusters_Likelihood(clusterNumber) = sum(map(pixels));
            clusters_tpeak(clusterNumber) = sum(map(pixels) .* pixelsT) ...
                / clusters_Likelihood(clusterNumber);
            clusters_fpeak(clusterNumber) = sum(map(pixels) .* pixelsF) ...
                / clusters_Likelihood(clusterNumber);
        end
        clusters = [clusters_tmin, clusters_tpeak clusters_tmax ...
            clusters_fmin, clusters_fpeak clusters_fmax ...
            clusters_Likelihood [clusters_Struct.Area]'];
        
    case 'hierarchical'
        
        % ---- Largest distance that can separate pixels in a cluster.
        if (nargin>=4)
            distanceThresh = clusterParam2;
        else
            distanceThresh = 3;
        end

        % ---- Calculate bitmapThreshold based on percentile of measured
        %      map values.
        if (nargin>=3)
            percentile = (1 - clusterParam1)*100;
        else
            percentile = 99.0;
        end
        bitmapThresh = prctile(map(:),percentile);

        % ---- Threshold to create bitmap.
        bitmap = map > bitmapThresh;
        
        % ---- Apply hierarchical clustering to bitmap.
        [y,x] = find(bitmap==1);
        X = [y,x];
        Y = pdist(X,'cityblock');
        Z = linkage(Y,'single');
        C = cluster(Z,'cutoff',distanceThresh,'criterion','distance');
        clusters = zeros(max(C), 7);

        for i = 1:max(C)
            clusterIndices = find(C==i);
            clusterFreqs = X(clusterIndices, 1);
            clusterTimes = X(clusterIndices, 2);

            tMin = min(clusterTimes);
            tMax = max(clusterTimes);
            fMin = min(clusterFreqs);
            fMax = max(clusterFreqs);

            area = length(clusterIndices);
            
            % calculate weighted center time, center frequency, and summed likelihood
            tc = 0;
            fc = 0;
            summedLikelihood = 0;

            for j = 1:size(clusterIndices,1)
                pixelFreq = clusterFreqs(j);
                pixelTime = clusterTimes(j);
                likelihoodValue = map(pixelFreq,pixelTime);
                tc = tc + pixelTime*likelihoodValue;
                fc = fc + pixelFreq*likelihoodValue;
                summedLikelihood = summedLikelihood + likelihoodValue;
            end
            tc = tc / summedLikelihood;
            fc = fc / summedLikelihood;

            clusters(i,:) = [tMin, tc, tMax, fMin, fc, fMax, summedLikelihood, area];
        end
        clusters_Struct = [];
        
    case 'sumOverFrequency'

        numberOfFrequencyBins = size(map, 1);
        frequencyBins = [1:numberOfFrequencyBins];
        numberOfTimeBins = size(map, 2);
        timeBins = [1:numberOfTimeBins];

        minTime = timeBins-0.5;
        peakTime = timeBins;
        maxTime = timeBins+0.5;
        
        summedLikelihood = sum(map,1);
        
        minFreq = 0.5 * ones(1,numberOfTimeBins);
        peakFreq = (frequencyBins * map) ./ summedLikelihood ;
        maxFreq = (numberOfFrequencyBins+0.5) * ones(1,numberOfTimeBins);

        area = numberOfFrequencyBins * ones(1,numberOfTimeBins);
        
        clusters = [minTime', peakTime', maxTime', minFreq', peakFreq', ...
            maxFreq', summedLikelihood', area'];
        clusters_Struct = [];
        
    case 'maxOverFrequency'

        numberOfTimeBins = size(map, 2);
        timeBins = [1:numberOfTimeBins];
        [summedLikelihood, maxFreq] = max(map,[],1);
        clusters = [timeBins'-0.5, timeBins', timeBins'+0.5, ...
            maxFreq'-0.5, maxFreq', maxFreq'+0.5, ...
            summedLikelihood', ones(numberOfTimeBins,1)];
        clusters_Struct = [];
        
end

% ---- Scale output units to seconds and Hertz if requested.
if (nargin==5 && length(mapDim)==4)
    T0 = mapDim(1);
    dT = mapDim(2);
    F0 = mapDim(3);
    dF = mapDim(4);
    clusters(:,1:3) = (clusters(:,1:3) - 1) * dT + T0;
    clusters(:,4:6) = (clusters(:,4:6) - 1) * dF + F0;
end

% ---- Done .
return

