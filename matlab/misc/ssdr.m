function significance = ssdr(x, y)
% Sum Square Difference of Ranks
%
% A non-parametric rank statistical test, returning the probability of
% uncorrelated series being more correlated than x and y.  Values
% near zero indicate strong correlation, values near one indicate strong 
% anticorrelation.
%
% From Numerical Recipes, section 14.6

n = length(x);

[z, r] = sort(x);
[z, s] = sort(y);
r(r) = 1:n;
s(s) = 1:n;

% statistic
D = sum((r-s).^2);

% mean
m = (n^3-n)/6;
v = ((n-1)*(n^2)*(n+1)^2)/36;
significance = normcdf(D, m, sqrt(v));
