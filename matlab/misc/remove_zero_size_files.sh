#!/bin/bash

# Deletes empty .err files and list deleted files in zero_size_err_files.txt.
# Usage:
#
#   remove_zero_size_files.sh <dir>
#
# where <dir> is the log directory to be cleaned up. The output file
# zero_size_err_files.txt is left in this directory. 

cd $1 
ls -s *.err | awk '{if ($1==0) print $2}' > zero_size_err_files.txt
while read filename 
do 
    rm $filename
done < zero_size_err_files.txt

