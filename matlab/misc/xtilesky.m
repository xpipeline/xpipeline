function skyPositions = xtilesky(maximumTimingError, channelNames);
% XTILESKY Optimal tiling of the sky for the specified timing error
%
% XTILESKY tiles the sky with the approximate mininum number of sky positions
% that gaurantee that, for any two detectors, no two points are separated by a
% specified worst case timing difference.
%
% usage:
%
%   skyPositions = xtilesky(maximumTimingError, channelNames);
%
%   maximumTimingError   maximum permissible timing error [s]
%   channelNames         cell array of channel name strings
%
%   skyPositions         matrix of sky positions [radians]
%
% The returned sky position are provided as a two column matrix of the form
% [theta phi], where theta is the geocentric colatitude running from 0 at the
% North pole to pi at the South pole and phi is the geocentric longitude in
% Earth fixed coordinates with 0 on the prime meridian.  The range of theta
% is [0, pi] and the range of phi is [-pi, pi);
%
% See also XPIPELINE, XREADDATA, XINJECTSIGNAL, XCONDITION, XMAPSKY, and
% XINTERPRET.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

error(nargchk(2, 2, nargin));

channelNames = channelNames(:);

numberOfChannels = length(channelNames);

detectorString = [];
for channelNumber = 1 : numberOfChannels,
  detectorString = [detectorString channelNames{channelNumber}(1)];
end
detectorString = sort(detectorString);

if strcmp(detectorString, 'HLV'),
  if maximumTimingError == 1.0e-3,
    load skygrids/minimalgrid_VHL_1ms.mat;
    skyPositions = map;
    return;
  elseif maximumTimingError == 0.8e-3,
    load skygrids/minimalgrid_VHL_0.8ms.mat;
    skyPositions = map;
    return;
  elseif maximumTimingError == 0.6e-3,
    load skygrids/minimalgrid_VHL_0.6ms.mat;
    skyPositions = map;
    return;
  elseif maximumTimingError == 0.4e-3,
    load skygrids/minimalgrid_VHL_0.4ms.mat;
    skyPositions = map;
    return;
  elseif maximumTimingError == 0.1e-3,
    load skygrids/minimalgrid_VHL_0.1ms.mat;
    skyPositions = map;
    return;
  end
end

if numberOfChannels < 5,
  skyPositions = minimalgrid(maximumTimingError, channelNames);
else
  speedOfLight = 2.997e10; %-- cm/s
  earthRadius = 6378e5; %-- cm
  maximumTimeDifference = 2 * earthRadius / speedOfLight;
  angularResolution = maximumTimingError / maximumTimeDifference;
  skyPositions = sinusoidalMap(angularResolution);
end
