#include "mex.h"

#ifndef min
#define min(a, b) (((a)<(b))?(a):(b))
#endif

#ifdef WIN32
#define dgesvd_ dgesvd
#endif

void xbasis(double* out_data, const double* in_data, int M, int N, int P)
{
    char JOBU = 'N';
    char JOBVT = 'A';
    double *A, *S, *VT, *WORK;
    int LWORK = -1;
    int INFO;
    int i, j, k;

    A = mxMalloc(M * N * sizeof(double));    
    S = mxMalloc(min(M, N) * sizeof(double));
    VT = mxMalloc(N * N * sizeof(double));        
    WORK = mxMalloc(sizeof(double));

    dgesvd_(&JOBU, &JOBVT, &M, &N, A, &M, S, 0, &M, VT, &N, WORK, &LWORK, &INFO);
    LWORK = WORK[0];
    WORK = mxRealloc(WORK, LWORK * sizeof(double));

    for (k = 0; k < P; ++k)
    {
    
        memcpy(A, in_data, M*N*sizeof(double));
        dgesvd_(&JOBU, &JOBVT, &M, &N, A, &M, S, 0, &M, VT, &N, WORK, &LWORK, &INFO);
    
        memcpy(out_data, VT, N * N * sizeof(double));
        
        in_data += M * N;
        out_data += N * N;        
    }
    
    mxFree(WORK);
    mxFree(VT);
    mxFree(S);
    mxFree(A);
}

void mexFunction(int nlhs, mxArray** plhs, int nrhs, const mxArray** prhs)
{
    const int* in_dimensions;
    int* out_dimensions;
    const double* in_data;
    double* out_data;
    if (nrhs != 1)
    {
        mexErrMsgTxt("One input required");
    }
    if (nrhs > 1)
    {
        mexErrMsgTxt("Too many outputs");
    }
    in_dimensions = mxGetDimensions(prhs[0]);
    if (!mxIsDouble(prhs[0]))
    {
        mexErrMsgTxt("Input must be double precision");
    }
    if (mxIsComplex(prhs[0]))
    {
        mxErrMsgTxt("Input must be real");
    }
    if (mxGetNumberOfDimensions(prhs[0]) != 3)
    {
        mxErrMsgTxt("Input must be 3 dimensional");
    }
    if (in_dimensions[0] >= in_dimensions[1])
    {
        mexErrMsgTxt("First dimension must be smaller than second dimension");
    }
    out_dimensions = mxMalloc(sizeof(double) * 3);
    out_dimensions[0] = in_dimensions[1];
    out_dimensions[1] = in_dimensions[1];
    out_dimensions[2] = in_dimensions[2];
    plhs[0] = mxCreateNumericArray(3, out_dimensions, mxDOUBLE_CLASS, mxREAL);
    in_data = mxGetPr(prhs[0]);
    out_data = mxGetPr(plhs[0]);
    
    xbasis(out_data, in_data, in_dimensions[0], in_dimensions[1], in_dimensions[2]);
    
    mxFree(out_dimensions);
}

