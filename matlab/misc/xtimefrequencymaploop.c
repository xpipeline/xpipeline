/* $Id$ */

#include "math.h"
#include "string.h"
#include "mex.h"

#define square(x) ((x)*(x))
#define max(x,y)  ((x)>(y) ? (x):(y))

/*
  Loop over sky positions converted to C for performance.

  May need to be mex'd on a 32 bit machine (ldas-pcdev2)
  // Does not work if mex'd with r14_sp3 (r13, r2006a ok) (?)

  mex xtimefrequencymaploop.c
  xtimefrequencymaploop(...............)

*/


/* ---- convenience functions for dynamic multidimentional arrays */

double **allocTwoD(int m, int n)
{
  /* dynamically allocate memory for a two dimentional array */
  int i;
  double **ret = malloc(m*sizeof(double *));

  for(i=0; i<m; i++)
  {
    ret[i] = malloc(n*sizeof(double));
  }

  return ret;
}

void freeTwoD(double **twoD, int m)
{
  int i;

  for(i=0; i<m; i++)
    free(twoD[i]);

  free(twoD);
}

double ***allocThreeD(int x, int y, int z)
{
  /* dynamically allocate memory for a three dimentional array.
     a = allocThreeD(1,2,3) is the same as a[1][2][3] */

  int i;
  double ***ret = malloc(x*sizeof(double **));

  for(i=0; i<x; i++)
  {
    ret[i] = (double **)allocTwoD(y, z);
  }

  return ret;
}
  
double **oneDtoTwoD(double *oneD, int m, int n)
{
  /* splits a one dimentional array into a two dimentional array */
  int i, j;
  double **ret = allocTwoD(m, n);

  /* set values */
  for(i=0; i<m; i++)
  {
    for(j=0; j<n; j++)
    {
      ret[i][j] = oneD[m*j+i];
    }
  }

  return ret;
}

double *twoDtoOneD(double **twoD, int m, int n)
{
  /* combines a two dimentional array into a one dimentional array by columns */
  int i, j;
  double *ret = malloc(m*n * sizeof(double));

  for(j=0; j<n; j++)
  {
    for(i=0; i<m; i++)
    {
      ret[i + j*m] = twoD[i][j];
    }
  }

  return ret;
}

void printOneD(double *oneD, int n)
{
  int i;

  for(i=0; i<n; i++)
    printf("\t%g", oneD[i]);
}

void printTwoD(double **twoD, int m, int n)
{
  int i;

  for(i=0; i<m; i++)
  {
    printOneD(twoD[i], n);
    printf("\n");
  }
}

void twoDmemcpy(double **dest, double **src, int m, int n)
{
  /* two dimentional version of memcpy */
  int i;

  for(i=0; i<m; i++)
    memcpy(dest[i], src[i], n);

}

/* ---- main function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* assign inputs */
    
    /* scalars */
    int numberOfChannels = mxGetScalar(prhs[0]);
    int numberOfDetectors = numberOfChannels;
    int numberOfFrequencyBins = mxGetScalar(prhs[1]);
    int numberOfInternalAngles = mxGetScalar(prhs[2]);
    int numberOfLikelihoods = mxGetScalar(prhs[3]);
    int numberOfSkyPositions = mxGetScalar(prhs[4]);
    int numberOfTimeBins = mxGetScalar(prhs[5]);
    double offsetFraction = mxGetScalar(prhs[6]);

    /* 1 x numberOfFrequencyBands vectors */
    double *inbandFrequencies = mxGetPr(prhs[7]);

    /* m x 2 matrices */
    int numberOfElements = mxGetM(prhs[8]);
    double **elementIndices = oneDtoTwoD(mxGetPr(prhs[8]), numberOfElements, 2);
    
    /* numberOfFrequencyBins x numberOfDetectors matrices */
    double **amplitudeSpectra = oneDtoTwoD(mxGetPr(prhs[9]), numberOfFrequencyBins, numberOfDetectors);

    /* numberOfSkyPositions x numberOfDetectors matrices */
    double **Fp = oneDtoTwoD(mxGetPr(prhs[10]), numberOfSkyPositions, numberOfDetectors);
    double **Fc = oneDtoTwoD(mxGetPr(prhs[11]), numberOfSkyPositions, numberOfDetectors);
    double **integerTimeShiftLengths = oneDtoTwoD(mxGetPr(prhs[12]), numberOfSkyPositions, numberOfDetectors);
    double **residualTimeShifts = oneDtoTwoD(mxGetPr(prhs[13]), numberOfSkyPositions, numberOfDetectors);

    /* FFT parameters */
    /* scalar */
    int elementLength = mxGetScalar(prhs[14]);

    /* elementLength x 1 */
    double *windowData = mxGetPr(prhs[15]);

    /* 65536(?) x numberOfDetectors */
    double **conditionedData = oneDtoTwoD(mxGetPr(prhs[16]), 65536, numberOfDetectors);

    /* numberOfDetectors x 1 cell array containing elementLength x numberOfTimeBins matrices. */
    double ***timeFrequencyMapFullRe = allocThreeD(numberOfChannels, elementLength, numberOfTimeBins);
    double ***timeFrequencyMapFullIm = allocThreeD(numberOfChannels, elementLength, numberOfTimeBins);

    /* first TF map already calculated. */
    timeFrequencyMapFullRe[0] = oneDtoTwoD(mxGetPr(mxGetCell(prhs[17], 0)), elementLength, numberOfTimeBins);
    timeFrequencyMapFullIm[0] = oneDtoTwoD(mxGetPi(mxGetCell(prhs[17], 0)), elementLength, numberOfTimeBins);
    
    /* initialize intermediate variables and allocate memory */
    int i, skyPositionNumber, kFreq, kDet, kTime, kElem, channelNumber;
    double **wFp = allocTwoD(numberOfFrequencyBins, numberOfDetectors);
    double **wFc = allocTwoD(numberOfFrequencyBins, numberOfDetectors);
    double **residualTimeShiftPhasesRe = allocTwoD(numberOfFrequencyBins, numberOfDetectors);
    double **residualTimeShiftPhasesIm = allocTwoD(numberOfFrequencyBins, numberOfDetectors);
    double **wFpTimeFrequencyMapRe = allocTwoD(numberOfFrequencyBins, numberOfTimeBins);
    double **wFpTimeFrequencyMapIm = allocTwoD(numberOfFrequencyBins, numberOfTimeBins);
    double **wFcTimeFrequencyMapRe = allocTwoD(numberOfFrequencyBins, numberOfTimeBins);
    double **wFcTimeFrequencyMapIm = allocTwoD(numberOfFrequencyBins, numberOfTimeBins);
    double **likelihood = allocTwoD(numberOfFrequencyBins, numberOfTimeBins);
    mxArray *fftInputMX = mxCreateDoubleMatrix(elementLength, numberOfTimeBins, mxREAL);
    double *fftInput = mxGetPr(fftInputMX);
    mxArray *fftOutputMX;
    
    double Mpp[numberOfFrequencyBins];
    double Mpc[numberOfFrequencyBins];
    double Mcc[numberOfFrequencyBins];

    double ***timeFrequencyMapRe = allocThreeD(numberOfChannels, numberOfFrequencyBins, numberOfTimeBins);
    double ***timeFrequencyMapIm = allocThreeD(numberOfChannels, numberOfFrequencyBins, numberOfTimeBins);
    
    
    /* assign outputs */
    /* Allocate memory and assign output pointer
       mxReal is our data-type */
    /* plhs[0] = mxCreateDoubleMatrix(colLen, rowLen, mxREAL); */

    /* Get a pointer to the data space in our newly allocated memory */
    /* outArray = mxGetPr(plhs[0]); */

    
    printf("numberOfChannels: %d\n", numberOfChannels);
    printf("numberOfFrequencyBins: %d\n", numberOfFrequencyBins);
    printf("numberOfInternalAngles: %d\n", numberOfInternalAngles);
    printf("numberOfLikelihoods: %d\n", numberOfLikelihoods);
    printf("numberOfSkyPositions: %d\n", numberOfSkyPositions);
    printf("numberOfTimeBins: %d\n", numberOfTimeBins);
    printf("offsetFraction: %f\n", offsetFraction);
    printf("----\n");
    printf("inbandFrequencies: %d x %d\n", mxGetM(prhs[7]), mxGetN(prhs[7]));
    printf("----\n");
    printf("elementIndices: %d x %d\n", mxGetM(prhs[8]), mxGetN(prhs[8]));
    printf("----\n");
    printf("amplitudeSpectra: %d x %d\n", mxGetM(prhs[9]), mxGetN(prhs[9]));
    printf("Fp: %d x %d\n", mxGetM(prhs[9]), mxGetN(prhs[10]));
    printf("Fc: %d x %d\n", mxGetM(prhs[10]), mxGetN(prhs[11]));
    printf("integerTimeShiftLengths: %d x %d\n", mxGetM(prhs[12]), mxGetN(prhs[12]));
    printf("residualTimeShifts: %d x %d\n", mxGetM(prhs[13]), mxGetN(prhs[13]));
    printf("----\n");
    printf("elementLength: %d\n", elementLength);
    printf("windowData: %d x %d\n", mxGetM(prhs[15]), mxGetN(prhs[15]));
    printf("conditionedData: %d x %d\n", mxGetM(prhs[16]), mxGetN(prhs[16]));
    printf("timeFrequencyMapFull: %d x %d (%d x %d)\n", mxGetM(prhs[17]), mxGetN(prhs[17]), mxGetM(mxGetCell(prhs[17],0)), mxGetN(mxGetCell(prhs[17],0)));
    
    /* loop over sky positions */
    for(skyPositionNumber=0; skyPositionNumber<numberOfSkyPositions; skyPositionNumber++)
    {
      if(fmod(skyPositionNumber,numberOfSkyPositions/100) < 1)
      {
	printf("processing sky position %d of %d (%d%% complete)...\n",
	  skyPositionNumber, numberOfSkyPositions,
	  (int)round(100 * (float)(skyPositionNumber+1)/numberOfSkyPositions));
      }
    
      /* compute detector responses */
      /* Whitened responses for each detector, frequency x detector. */
      for(kFreq=0; kFreq<numberOfFrequencyBins; kFreq++)
      {
	Mpp[kFreq] = 0;
	Mpc[kFreq] = 0;
	Mcc[kFreq] = 0;

	for(kDet=0; kDet<numberOfDetectors; kDet++)
	{
	  wFp[kFreq][kDet] = Fp[skyPositionNumber][kDet] / amplitudeSpectra[kFreq][kDet];
	  wFc[kFreq][kDet] = Fc[skyPositionNumber][kDet] / amplitudeSpectra[kFreq][kDet];
    
	  /* Matrix M_AB (:= \sum_det wFA wFB) components, for each frequency.
	   These are the inner products of the wFp, wFc vectors for each
	   frequency bin. */
	  Mpp[kFreq] += square(wFp[kFreq][kDet]);
	  Mpp[kFreq] += wFp[kFreq][kDet] * wFc[kFreq][kDet];
	  Mcc[kFreq] += square(wFc[kFreq][kDet]);
    
	  /* residual phase shifts for this sky position */
	  residualTimeShiftPhasesRe[kFreq][kDet] = cos(2 * M_PI *
		inbandFrequencies[kFreq] * residualTimeShifts[skyPositionNumber][kDet]);
          residualTimeShiftPhasesIm[kFreq][kDet] = sin(2 * M_PI *
		inbandFrequencies[kFreq] * residualTimeShifts[skyPositionNumber][kDet]);
	}
      }
      
      /* KLUDGE: insert dominant polarization code */

    
    
      /* construct time-frequency maps for each channel */
      
      /* Note that due to the time shifts the edges of the time-frequency map
      will contain small amounts of data from the transient periods.
      Loop over detectors.
      Skip first detector since it is used as the reference position -
      therefore always at zero delay and only have to FFT its data once. */
      

      for(channelNumber=1; channelNumber<numberOfChannels; channelNumber++)
      {
	/* FFT version */
	int elementStartIndex = elementIndices[0][0] + integerTimeShiftLengths[skyPositionNumber][channelNumber];
	/* int elementStopIndex = elementIndices[max(numberOfElements-2,0)][1] + integerTimeShiftLengths[skyPositionNumber][channelNumber]; */
	double *fftOutputRe;
	double *fftOutputIm;
	

	for(kTime=0; kTime<numberOfTimeBins; kTime++)
	{
	  for(kElem=0; kElem<elementLength; kElem++)	  
	  {
	    fftInput[kElem + kTime*elementLength] = windowData[kElem] * conditionedData[elementStartIndex-1 + kElem + kTime*elementLength][channelNumber];
	  }
	}

	
	/* call matlab fft function */
	mexCallMATLAB(1, &fftOutputMX, 1, &fftInputMX, "fft");

	/* copy output (SLOW) */
	/*
	fftOutputRe = oneDtoTwoD(mxGetPr(fftOutputMX), elementLength, numberOfTimeBins);
	fftOutputIm = oneDtoTwoD(mxGetPi(fftOutputMX), elementLength, numberOfTimeBins);
	
	twoDmemcpy(timeFrequencyMapFullRe[channelNumber], fftOutputRe, elementLength, numberOfTimeBins);
	twoDmemcpy(timeFrequencyMapFullIm[channelNumber], fftOutputIm, elementLength, numberOfTimeBins);
	*/

	/* copy output (slow) */

	fftOutputRe = mxGetPr(fftOutputMX);
	fftOutputIm = mxGetPi(fftOutputMX);

	for(kTime=0; kTime<numberOfTimeBins; kTime++)
	{
	  for(kElem=0; kElem<elementLength; kElem++)	  
	  {
	    timeFrequencyMapFullRe[channelNumber][kElem][kTime] = fftOutputRe[kElem + kTime*elementLength];
	    timeFrequencyMapFullIm[channelNumber][kElem][kTime] = fftOutputIm[kElem + kTime*elementLength];
	  }
	}
	

	/* free memory (slow) */
	/* freeTwoD(fftOutputRe, elementLength);
	freeTwoD(fftOutputIm, elementLength); */
	mxDestroyArray(fftOutputMX);
	
      }
      
      /* antenna-response weighted time-frequency maps, apply residual time shift */
      for(kFreq=0; kFreq<numberOfFrequencyBins; kFreq++)
      {
        for(kTime=0; kTime<numberOfTimeBins; kTime++)
	{
          wFpTimeFrequencyMapRe[kFreq][kTime] = 0;
          wFcTimeFrequencyMapRe[kFreq][kTime] = 0;
          wFpTimeFrequencyMapIm[kFreq][kTime] = 0;
          wFcTimeFrequencyMapIm[kFreq][kTime] = 0;
          for(kDet=0; kDet<numberOfDetectors; kDet++)
	  {
	    /* KLUDGE: need DP stuff */
	    wFpTimeFrequencyMapRe[kFreq][kTime] += timeFrequencyMapRe[kDet][kFreq][kTime] * wFp[kFreq][kDet] * residualTimeShiftPhasesRe[kFreq][kDet];
	    wFpTimeFrequencyMapIm[kFreq][kTime] += timeFrequencyMapIm[kDet][kFreq][kTime] * wFp[kFreq][kDet] * residualTimeShiftPhasesRe[kFreq][kDet];
            wFcTimeFrequencyMapRe[kFreq][kTime] += timeFrequencyMapRe[kDet][kFreq][kTime] * wFc[kFreq][kDet] * residualTimeShiftPhasesRe[kFreq][kDet];
            wFcTimeFrequencyMapIm[kFreq][kTime] += timeFrequencyMapIm[kDet][kFreq][kTime] * wFc[kFreq][kDet] * residualTimeShiftPhasesRe[kFreq][kDet];
          }

	  /* compute each of the requested likelihoodType maps */
	  /* KLUDGE: only do standard likelihood */
          likelihood[kFreq][kTime] = 
	    (square(wFpTimeFrequencyMapRe[kFreq][kTime])
	     + square(wFpTimeFrequencyMapIm[kFreq][kTime]))/Mpp[kFreq]
	  + (square(wFcTimeFrequencyMapRe[kFreq][kTime])
             + square(wFcTimeFrequencyMapIm[kFreq][kTime]))/Mcc[kFreq];

        }
      }
    
      /* break; */
    }
    
    /* printTwoD(likelihood, numberOfFrequencyBins, numberOfTimeBins); */
    /* printTwoD(fftInput, elementLength, numberOfTimeBins); */
    /* printOneD(fftInput[1], numberOfTimeBins); */
    printf("done.\n");
    
    return;
}
