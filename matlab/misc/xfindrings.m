function [cThetaMax, bhat, base] = xfindrings(skyPositions, skyMap, channel)
% XFINDRINGS - Find rings in skymaps.
%
%     [cThetaMax, bhat] = xfindrings(skyPositions, skyMap, channelNames)
%
% $Id$

%----- Prepare test data.
skyMap = squeeze(skyMap);

%----- Pointing vectors for the sky positions.
Omega = [sin(skyPositions(:,1)).*cos(skyPositions(:,2)),sin(skyPositions(:,1)).*sin(skyPositions(:,2)),cos(skyPositions(:,1))];

%----- Baselines.
for jChannel = 1:length(channel)
    detector{jChannel} = LoadDetectorData(channel{jChannel});
end
base = [];
bhat = [];
cTheta = [];
for iChannel = 1:length(channel)-1
    for jChannel = iChannel+1:length(channel)
        b = detector{iChannel}.V-detector{jChannel}.V;
        base = [base, b];
        bhat = [bhat, b/(b'*b)^0.5];
        cTheta = [cTheta, Omega*b/(b'*b)^0.5];
    end
end

%----- "Probability" or weighting of each sky position.
prob = skyPositions(:,4).*skyMap;
prob = prob / sum(prob);

%----- Loop over baselines.  Whiten wrt other baselines.
% Nf = 32;
Nf = 32*ones(1,size(base,2));
f0 = 200;
c = 299792458;
Nf = floor(1/2*size(skyPositions,1)*c/(4*f0)*(sum(base.^2,1)).^(-0.5))

probw = prob;

% %----- Threshold
% threshold = 0.9;
% test = prob;
% stest = sort(test);
% test0 = stest(floor(threshold*(length(stest))));
% wtest = zeros(size(test));
% k = find(test>test0);
% wtest(k) = 1;
% probw = wtest;

%----- First pass to figure out which ring is strongest.
for jbase = 1:size(base,2)
    [Y,I] = sort(cTheta(:,jbase));
    [index,J] = sort(I);
    temp = probw(I);
    padstart = mean(temp(1:Nf(jbase)))*ones(Nf(jbase),1);
    padend = mean(temp(end-Nf(jbase)+1:end))*ones(Nf(jbase),1);
    temp = [padstart; temp; padend];
    Z = flipud(filter(ones(Nf(jbase),1),1,flipud(filter(ones(Nf(jbase),1),1,temp))));
    Z(1:Nf(jbase)) = [];
    Z(end-Nf(jbase)+1:end) = [];
    % ring SNR := max value over median value
    a = sort(Z);
    ringval(jbase) = a(end)/a(floor(length(a)/2));
end
% figure; plot(fliplr(cTheta),skyMap,'.'); grid

cThetaMax = zeros(1,size(base,2));
probfit = zeros(length(probw),size(base,2));
wringval = zeros(1,size(base,2));
%----- whiten from strongest ring to weakest
[Vr,Ir] = sort(ringval,2,'descend');
for jbase = Ir
    [Y,I] = sort(cTheta(:,jbase));
    [index,J] = sort(I);
    temp = probw(I);
    padstart = mean(temp(1:Nf(jbase)))*ones(Nf(jbase),1);
    padend = mean(temp(end-Nf(jbase)+1:end))*ones(Nf(jbase),1);
    temp = [padstart; temp; padend];
    Z = flipud(filter(ones(Nf(jbase),1),1,flipud(filter(ones(Nf(jbase),1),1,temp))));
    Z(1:Nf(jbase)) = [];
    Z(end-Nf(jbase)+1:end) = [];
    Z = Z/max(Z);
    probfit(:,jbase) = Z(J);
    probw = probw./probfit(:,jbase);
    % ring SNR := max value over median value
    [a, ia] = sort(probfit(:,jbase));
    wringval(jbase) = a(end)/a(floor(length(a)/2));
    % ring location
    % [a, ia] = max(probfit(:,jbase));
    cThetaMax(jbase) = cTheta(ia(end),jbase);
end
% probw = probw/max(probw);
figure; plot(cTheta,probfit,'.'); grid

%----- Determine two sky locations from two loudest rings.
[Vr,Ir] = sort(wringval,2,'descend');

% theta errors:
% cThetaMax
% sourceOmega*bhat
% (cThetaMax-sourceOmega*bhat)*180/pi

% % simple histogramming
% threshold = 0.9;
% dx = 0.03;
% test = prob;
% stest = sort(test);
% test0 = stest(floor(threshold*(length(stest))));
% wtest = zeros(size(test));
% k = find(test>test0);
% wtest(k) = 1;
% % figure; xproject(skyPositions, wtest, 'sphere',channel); colorbar
% wcTheta = cTheta(k,:);
% x = [-1:dx:1]';
% n = hist(wcTheta,x);
% figure; plot(x,n); grid
% 
% % ring location
% [a, ia] = max(n);
% cThetaMaxHist = x(ia)';

