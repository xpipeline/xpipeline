function fraction = FractionAboveThreshold(data,threshold)
%
% FractionAboveThreshold - Estimate what fraction of sampled data values
% are greater than or equal to each of a specified set of threshold values,
% using interpolation.
%
%   fraction = FractionAboveThreshold(data,threshold)
% 
% data          Vector of measured data values.
% threshold     Vector of threshold values.
%
% fraction      Vector.  Fraction of data samples greater than or equal to
%               threshold.  If the interpolation fails when computing
%               fraction at some value, that element of fraction is set
%               equal to NaN.
%
% This function ignores data samples that are NaN or Inf.  It properly
% handles the counting of repeated data values.  It uses robustinterp1 in
% log10(fraction) to perform the fraction calculation. Together, these make
% this function fairly robust.
%
% To Do: 
% 1) Add error estimates on fraction, using binomial statistics.
%
% $Id$

% ---- Checks
error(nargchk(2,2,nargin));
if ~isvector(data)
    error('Input data must be a vector.')
end
if ~isvector(threshold)
    error('Input threshold must be a vector.')
end

% ---- Drop any NaN or Inf datas. 
badTrials = isnan(data) | isinf(data);
data(badTrials) = [];

% ---- Count number of "good" trials.
numberOfTrials = length(data);

% ---- Sort remaining datas and keep unique values.  We do this
%      such that repeated datas are properly counted in the fraction;
%      i.e., according to our "number greater than or equal to threshold"
%      rule. 
[data,I] = unique(sort(data),'first');
trialFraction = 1-(I-1)/numberOfTrials;

% ---- Robust log interpolation.
[fraction_int threshold_int] = robustinterp1(data,log10(trialFraction),threshold);
fraction_int = 10.^fraction_int;

% ---- Copy fraction into vector corresponding to threshold, padded with NaNs.
fraction = NaN * ones(size(threshold));
[junk,index] = intersect(threshold,threshold_int);
fraction(index) = fraction_int;

% ---- Inteprolation will give fraction = NaN at threshold values smaller than
%      smallest measured data, since there are no data points there 
%      for the interpolation to use.  Reset these fraction values to unity.
k = find(threshold(isnan(fraction))<=min(data));
fraction(k) = 1;

% ---- Done
return
