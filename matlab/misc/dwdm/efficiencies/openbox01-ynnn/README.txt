
------------------------------------------------------------------------

 Projected Wall Amplitude Limits:

        runName:               openbox01
        mode:                  testing
        applyTOAWeighting:     true
        applyDQVetoes:         false
        applyGravitySpyVetoes: false
        gravitySpyClass.Conf.: 0.99
        applyAmplRatio:        false
        amplRatioThreshold:    1
        seedValue:             836465
        FAR threshold:         3.1688e-08 Hz  (1 y^-1)

        A50% limits [x10^15]:
        d_min   d_max   O1      O2      O3a     Combined
        0       1       0.92    0.36    1.45    0.88
        1       10      0.78    0.35    1.30    0.82
        10      100     0.90    0.38    1.46    0.89
        100     1000    1.66    0.60    2.81    1.52
        1000    10000   11.27   4.37    16.93   9.44

        A90% limits [x10^15]:
        d_min   d_max   O1      O2      O3a     Combined
        0       1       3.76    1.30    6.11    3.76
        1       10      7.39    3.17    NaN     4.32
        10      100     3.63    1.47    5.07    3.55
        100     1000    7.97    2.57    13.09   7.18
        1000    10000   445.51  164.93  224.98  219.10

------------------------------------------------------------------------

