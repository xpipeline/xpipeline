
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01-yyyy_ARGaussian
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         true
	applyGravitySpyVetoes: true
	gravitySpyClass.Conf.: 0.99
	applyAmplRatio:        0.6
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)
 
	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.05	0.05	0.14	0.09
	1	10	0.05	0.05	0.10	0.08
	10	100	0.04	0.05	0.14	0.08
	100	1000	0.09	0.12	0.26	0.16
	1000	10000	0.60	0.55	1.61	0.86
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.26	0.39	0.90	0.55
	1	10	1.67	2.18	0.40	1.09
	10	100	0.20	0.41	0.87	0.49
	100	1000	0.90	1.28	1.98	1.72
	1000	10000	31.73	42.03	45.05	42.08
 
------------------------------------------------------------------------

