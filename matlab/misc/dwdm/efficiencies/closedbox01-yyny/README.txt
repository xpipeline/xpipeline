
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01-yyny
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         true
	applyGravitySpyVetoes: false
	gravitySpyClass.Conf.: 0.95
	applyAmplRatio:        true
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)
 
	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.08	0.07	0.46	0.20
	1	10	0.08	0.07	0.37	0.23
	10	100	0.08	0.07	0.48	0.19
	100	1000	0.18	0.16	0.83	0.39
	1000	10000	1.02	0.69	4.82	1.85
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.38	5.01	1.93	1.47
	1	10	NaN	NaN	1.09	NaN
	10	100	0.32	0.71	2.02	1.44
	100	1000	2.22	NaN	6.21	NaN
	1000	10000	NaN	NaN	1099.89	NaN
 
------------------------------------------------------------------------

