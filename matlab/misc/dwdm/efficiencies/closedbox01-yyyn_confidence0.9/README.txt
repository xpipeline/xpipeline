
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01-yyyn_confidence0.9
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         true
	applyGravitySpyVetoes: true
	gravitySpyClass.Conf.: 0.9
	applyAmplRatio:        false
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)
 
	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.05	0.03	0.10	0.06
	1	10	0.05	0.04	0.08	0.06
	10	100	0.04	0.03	0.10	0.06
	100	1000	0.08	0.05	0.18	0.09
	1000	10000	0.56	0.34	1.01	0.56
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.20	0.13	0.38	0.25
	1	10	0.50	0.34	0.20	0.36
	10	100	0.18	0.14	0.39	0.26
	100	1000	0.41	0.27	0.93	0.58
	1000	10000	14.32	11.81	18.56	15.40
 
------------------------------------------------------------------------

