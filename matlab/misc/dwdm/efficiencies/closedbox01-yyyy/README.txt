
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01-yyyy
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         true
	applyGravitySpyVetoes: true
	gravitySpyClass.Conf.: 0.99
	applyAmplRatio:        true
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)
 
	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.06	0.06	0.16	0.09
	1	10	0.06	0.05	0.13	0.09
	10	100	0.05	0.06	0.16	0.09
	100	1000	0.09	0.12	0.28	0.17
	1000	10000	0.70	0.57	1.65	0.92
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	0.26	NaN	1.05	0.72
	1	10	NaN	NaN	0.42	NaN
	10	100	0.21	0.69	0.83	0.57
	100	1000	1.08	NaN	NaN	NaN
	1000	10000	NaN	NaN	NaN	NaN
 
------------------------------------------------------------------------

