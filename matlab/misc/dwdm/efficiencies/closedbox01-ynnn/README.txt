
------------------------------------------------------------------------
 
 Projected Wall Amplitude Limits: 
 
	runName:               closedbox01
	mode:                  training
	applyTOAWeighting:     true
	applyDQVetoes:         false
	applyGravitySpyVetoes: false
	gravitySpyClass.Conf.: 0.99
	applyAmplRatio:        false
	amplRatioThreshold:    1
	seedValue:             836465
	FAR threshold:         3.1688e-08 Hz  (1 y^-1)


	A50% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	1.32	0.32	1.38	0.94
	1	10	1.62	0.30	1.18	0.93
	10	100	1.20	0.32	1.43	0.93
	100	1000	2.02	0.69	2.32	1.53
	1000	10000	15.33	2.75	13.86	7.59
 
	A90% limits [x10^15]: 
	d_min   d_max   O1	O2	O3a	Combined
	0	1	5.00	1.31	4.41	3.16
	1	10	NaN	2.12	3.51	5.66
	10	100	4.48	1.37	4.65	3.46
	100	1000	10.79	2.55	10.98	7.16
	1000	10000	390.11	109.70	207.97	190.93
 
------------------------------------------------------------------------

