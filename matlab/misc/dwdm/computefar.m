function [far, minFarIdx] = computefar(bckgrd_stat,bckgrd_far,trigger_stat)
% COMPUTEFAR - compute FAR of trigger by comparing to background distribution.
%
% usage:
%
%  [far, minFarIdx] = computefar(bckgrd_stat,bckgrd_far,trigger_stat)
%
%  bckgrd_stat      Vector of detection statistic values for background events.
%  bckgrd_far       Vector of false alarm ratesfor background events.
%  trigger_stat     Vector of detection statistic values for triggers to be
%                   assigned FARs. 
%
%  far              Vector of false alarm rates corresponding to trigger_stat.
%  minFarIdx        Vector. Indices of trigger_stat values that are higher than
%                   the highest bckgrd_stat value.
%
% COMPUTEFAR assigns the FAR by interpolation as follows:
%
%   far = exp(robustinterp1(log(bckgrd_stat),log(bckgrd_far),log(trigger_stat)))
%
% Input trigger_stat values below (above) the lowest (highest) bckgrd_stat
% values are assigned far = max(bckgrd_far) (far = min(bckgrd_far)).
%
% $Id$

% ---- Compute FARs.
far = exp(robustinterp1(log(bckgrd_stat(:)),log(bckgrd_far(:)),log(trigger_stat(:))));

% ---- Handle triggers with stat values lower than those of the background.
idx = find(trigger_stat < min(bckgrd_stat));
if ~isempty(idx)
    far(idx) = max(bckgrd_far);
end

% ---- Handle triggers with stat values larger than those of the background.
idx = find(trigger_stat > max(bckgrd_stat));
if ~isempty(idx)
    far(idx) = min(bckgrd_far);
end
minFarIdx = idx;

% ---- Done.
return
