% $Id$

% This script loads the on-source, off-source, and injection triggers, applies
% the requested vetoes and TOA weightings, and saves the data in one (large)
% convenient .mat file for easy post-processing.


% ------------------------------------------------------------------------------
%    Main loop over analysis times.
% ------------------------------------------------------------------------------

% ---- Loop over analysis times.
for iat=1:length(analysisTimes)

    % ---- Select analysisTime and wall number containing on- and off-source
    %      data.
    analysisTime = analysisTimes(iat)
    wallNumber = wallNumbers(iat)

    disp(['**********************************************************'])
    disp(['Processing analysisTime ' num2str(analysisTime) ' seconds.'])
    disp(['**********************************************************'])

    % ---- Loop over each observing runs.
    for ior = 1:length(obsRuns)

        obsRun = obsRuns{ior}
        disp(['----------------------------------------------------------'])
        disp(['  Processing observing run ' obsRun '.'])
        disp(['----------------------------------------------------------'])

        % ------------------------------------------------------------------------------
        %    Read on-source triggers.
        % ------------------------------------------------------------------------------

        % ---- Load on-source file (to determine observing time).
        disp(['Loading on-source triggers for ' obsRun '.'])
        fileName = [dataBaseDir '/' obsRun '/' num2str(wallNumber) '/output/on_source_0_0_merged.mat'];
        on = loadonsourcetriggers(fileName,minSignificance,verbose);
        Non = length(on.cluster.significance);

        % ---- Compute zero-lag observing time [seconds] by counting number of on-source blocks processed.
        fileName = [dataBaseDir '/' obsRun '/' num2str(wallNumber) '/input/event_on_source.txt'];
        [status,result] = system(['wc ' fileName]);
        NonJob(iat,ior) = str2num(strtok(result));
        Ton(iat,ior) = NonJob(iat,ior) * (on.blockTime - 2*on.transientTime - 2*nongwcoincidencewindow(iat));


        % ------------------------------------------------------------------------------
        %    Read off-source triggers.
        % ------------------------------------------------------------------------------

        % ---- Load all off-source triggers. The loadoffsourcetriggers()
        %      function defines the .passAll and .unslidTime fields which are
        %      needed later. 
        disp(['Loading off-source triggers for ' mode '.'])
        fileName = [dataBaseDir '/' obsRun '/' num2str(wallNumber) '/output/off_source_0_0_merged.mat'];
        [off, ujn_training, n_off_job] = loadoffsourcetriggers(fileName,verbose,seedValue,mode);

        % ---- Determine detector network from the triggers read. (Doing it this way
        %      helps avoid H<->L swapping errors.)
        ifos = off.detectorList;
        % ---- This clumsy struct version of ifos is needed by the function xapplyvetosegments.
        ifoStruct.detectorList = off.detectorList;


        % ------------------------------------------------------------------------------
        %    Compute TOA weighting.
        % ------------------------------------------------------------------------------

        % ---- Store as an additional property in the triggers.
        %      We will compute the sidereal time using peakTime (t_H). We could instead
        %      use, eg the average of t_H and t_L but it should make no difference, as 
        %      the TOA weighting is very small for |t_H-t_L| > 30s and the sidereal bins
        %      are 86400s/100 = 864s wide.
        %      Note: delay = Vector of time delays (t_H - t_L) [s].
        %      Also: cluster.likelihood(:,4+N) = start time of event in Nth detector, 
        %      so determine which detector is H, which L.
        idxH = find(strcmpi(ifos,'H1'));
        idxL = find(strcmpi(ifos,'L1'));
        %
        if Non > 0
            tmp_gps = on.cluster.peakTime;
            tmp_delay = on.cluster.likelihood(:,4+idxH)-on.cluster.likelihood(:,4+idxL);
            tmp_weight = gpstotoaweight(tmp_gps,tmp_delay,TOAweights);
            on.cluster.toaWeight = tmp_weight;
        else
            on.cluster.toaWeight = []; %-- ensures field exists            
        end
        %
        tmp_gps = off.cluster.peakTime;
        tmp_delay = off.cluster.likelihood(:,4+idxH)-off.cluster.likelihood(:,4+idxL);
        tmp_weight = gpstotoaweight(tmp_gps,tmp_delay,TOAweights);
        off.cluster.toaWeight = tmp_weight;


        % ------------------------------------------------------------------------------
        %   Record final sets of surviving on- and off-source triggers.
        % ------------------------------------------------------------------------------

        % ---- Copy into master arrays.
        onsource{iat,ior} = on;
        offsource{iat,ior} = off;
        NoffJob(iat,ior) = n_off_job;


        % ------------------------------------------------------------------------------
        %    Read injection triggers.
        % ------------------------------------------------------------------------------

        % ---- Loop over injection sets for this analysisTime
        injSet = injSets{iat};
        for iis = 1:length(injSet)

            % ---- If applying TOA weighting then we skip any injection sets for
            %      which all injection delays are unphysical.
            if ~applyTOAWeighting || (applyTOAWeighting && length(validinj{ior}{iat}{iis})>0)             

                % ---- Load injection triggers. The loadinjtriggers() function 
                %      defines the .passAll and .unslidTime fields which are
                %      needed later. 
                disp(['Loading injection trigger set ' num2str(injSet(iis)) ' (' num2str(iis) ' of ' num2str(length(injSet))  ') for ' mode '.'])
                simName = ['domainwallq' num2str(injSet(iis))];
                baseDir = [dataBaseDir obsRun '/' num2str(injSet(iis)) '/output/'];
                logFile = [dataBaseDir obsRun '/' num2str(injSet(iis)) '/injection_' simName '.txt'];
                [search, idx, injScales] = loadinjtriggers(baseDir,simName,logFile,false,seedValue,mode);
                % ---- Keep the 'trigs' field of the search struct, and save
                %      also the idx (injection number) as that is needed for TOA
                %      weighting.
                for ii=1:length(injScales)
                    injtrig{ii} = search{ii}.trig;
                    injtrig{ii}.idx = idx;
                end
                clear search


                % ------------------------------------------------------------------------------
                %    Compute TOA weighting.
                % ------------------------------------------------------------------------------

                % ---- Store as an additional property in the triggers.
                %      Note: delay = Vector of time delays (t_H - t_L) [s].
                %      See the code above for handling the 'off' trigger set.
                for ii=1:length(injScales)
                    tmp_gps = injtrig{ii}.peakTime;
                    tmp_delay = injtrig{ii}.likelihood(:,4+idxH)-injtrig{ii}.likelihood(:,4+idxL);
                    tmp_weight = gpstotoaweight(tmp_gps,tmp_delay,TOAweights);
                    injtrig{ii}.toaWeight = tmp_weight;
                end


                % ------------------------------------------------------------------------------
                %   Record final set of surviving injection triggers.
                % ------------------------------------------------------------------------------

                % ---- Copy into master array.
                injtrigAll{injSet(iis),ior} = injtrig;

                
            end %-- if ~applyTOAWeighting || ...
            
        end %-- injection set

        % ---- Save a temporary copy of the data ... this is to handle the common 
        %      problem of matlab being killed for an unknown reason when running 
        %      for multiple hours. 
        if saveTmpFiles
            tmpFileName = ['tmp-' num2str(iat) '-' num2str(ior) '_' triggerFileName];
            disp(['Saving temporary file ' tmpFileName ' holding triggers processed to this point ...'])
            save(tmpFileName,'-v7.3'); 
            disp('... finished save call.')
        end

    end %-- obs run
   
end %-- analysis time


% ------------------------------------------------------------------------------
%    Save workspace.
% ------------------------------------------------------------------------------

% ---- Save trigger data. If an output file of the given name already exists
%      then edit the file name until we generate a name for which no file exists
%      already.
while exist(triggerFileName)==2
    triggerFileName = ['tmp-' triggerFileName];
end
save(triggerFileName,'-v7.3'); 

