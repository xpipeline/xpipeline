function str=log2str(a)
% function by Michael Haderlein https://uk.mathworks.com/matlabcentral/profile/authors/2961490
if a
    str='true';
else
    str='false';
end
