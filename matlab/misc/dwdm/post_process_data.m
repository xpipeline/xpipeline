% $Id$

% Procedure:
%
%   fix target FAR for closed-box analysis
%   for each analysisTime
%     for each observing run
%       load on-source file and determine observing time
%       load off-source triggers 
%       (optional) load DQ triggers and apply to on- and off-source triggers
%       (optional) load gravity spy triggers and apply to on- and off-source triggers
%       (optional) apply ratio cut veto to on- and off-source triggers
%       (optional) apply TOA weighting to on- and off-source triggers
%       compute FAR distribution of off-source triggers 
%       compute FARs of on-source triggers 
%       for each injection set in this analysisTime
%         for each injection scale
%           load injection triggers
%           (optional) apply DQ vetoes to injection triggers
%           (optional) apply gravity spy vetoes to injection triggers
%           (optional) apply ratio cut veto to injection triggers
%           (optional) apply TOA weighting to injection triggers
%           compute FAR of each injection trigger
%         compute efficiency vs. injection scale
%         compute is50% and is90% limits (for checks)
%     average efficiency vs. injection scale for each run (weighted by observing
%       time) OR average efficiency vs. amplitude A for each thickness band (for
%       TOA weighting)
%     compute is50% and is90% limits for averaged efficiencies


% ------------------------------------------------------------------------------
%    Clear workspace.
% ------------------------------------------------------------------------------

% close all
clear
format compact


% ------------------------------------------------------------------------------
%    Set name of input trigger file.
% ------------------------------------------------------------------------------

% ---- If this file exists (from a previous run of these scripts) then triggers
%      will be read from it. Otherwise, the post_process_load_data script will 
%      be used to read triggers directly from the x-pipeline data files.
% inputFileName = '';
inputFileName = 'post_process_data_openbox-full-nyyn.mat';


% ------------------------------------------------------------------------------
%    Load data.
% ------------------------------------------------------------------------------

% ---- If combined trigger .mat file exists, then load it. Otherwise, generate
%      it.
if exist(inputFileName)==2
    disp(['Loading triggers from ' inputFileName]);
    load(inputFileName)
    % ---- Copy critical run settings to backup variable names in case they are 
    %      to be changed for this run.
    inputparams.fullRun               = fullRun;
    inputparams.runName               = runName;
    inputparams.triggerFileName       = triggerFileName;
    inputparams.outputFileName        = outputFileName;
    inputparams.mode                  = mode;
    inputparams.applyTOAWeighting     = applyTOAWeighting;
    inputparams.applyDQVetoes         = applyDQVetoes; 
    inputparams.applyGravitySpyVetoes = applyGravitySpyVetoes;
    inputparams.applyAmplRatio        = applyAmplRatio; 
    % ---- Re-run the setup script. This allows us to overwrite choices such as
    %      applyDQVetoes made when generating the trigger file, and/or to reset
    %      paths for running on a different machine to where the trigger file
    %      was generated. 
    post_process_setup
    % ---- Compare critical run settings used to generate triggers to requested values.
    % ---- We can't change the fullRun setting.
    if inputparams.fullRun ~= fullRun
        error('Input trigger set does not match requested value of fullRun.');
    end
    % ---- We can't change the mode (training/testing).
    if strcmpi(inputparams.mode,mode)==0
        error('Input trigger set does not match requested value of mode.');
    end
    % ---- We're not currently checking these.
    % inputparams.runName               = runName;
    % inputparams.triggerFileName       = triggerFileName;
    % inputparams.outputFileName        = outputFileName;
    % ---- The handling of injection sets will fail if we change applyTOAWeighting. 
    if inputparams.applyTOAWeighting ~= applyTOAWeighting
        error('Input trigger set does not match requested value of applyTOAWeighting.');
    end
    % ---- We can impose additional vetoes but not undo vetoes that have alredy been applied.
    if (inputparams.applyDQVetoes==true) & (applyDQVetoes==false)
        error('Input trigger set applied DQ vetoes but setup requests no DQ vetoes.');
    end
    if (inputparams.applyGravitySpyVetoes==true) & (applyGravitySpyVetoes==false)
        error('Input trigger set applied GravitySpy vetoes but setup requests no GravitySpy vetoes.');
    end
    if (inputparams.applyAmplRatio==true) & (applyAmplRatio==false)
        error('Input trigger set applied AmplRatio vetoes but setup requests no AmplRatio vetoes.');
    end
else
    % ---- Run the setup script.
    post_process_setup
    % ---- Run the script to generate the trigger file.
    disp(['Creating trigger file ' triggerFileName]);
    post_process_load_data
end


% ------------------------------------------------------------------------------
%    Post-process triggers: backgrounds and efficiencies.
% ------------------------------------------------------------------------------

post_process_efficiencies


% ------------------------------------------------------------------------------
%    Make plots, if requested.
% ------------------------------------------------------------------------------

if makePlots
    post_process_make_plots
end

