
% ------------------------------------------------------------------------------
%    Clear workspace.
% ------------------------------------------------------------------------------

close all
clear
format compact


% ------------------------------------------------------------------------------
%    Full run or test run?
% ------------------------------------------------------------------------------

fullRun = true;


% ------------------------------------------------------------------------------
%    Set path as needed.
% ------------------------------------------------------------------------------

% ---- Needed directories and files.
[status,result] = system('uname');
if status==0 
    switch lower(result(1:end-1))
        case 'darwin'
            baseDir = '/Users/psutton/Documents/xpipeline/trunk/matlab/misc/dwdm/';
            if fullRun
                warning('Unable to perform full run on laptop; switching to test mode.')
                fullRun = false;
            end
        case 'linux'
            addpath /home/xpipeline/xpipeline/trunk/matlab/share/
            addpath /home/xpipeline/xpipeline/trunk/matlab/searches/grb
            addpath /home/xpipeline/xpipeline/trunk/matlab/searches/sn
            addpath /home/xpipeline/xpipeline/trunk/matlab/utilities/  
            addpath /home/xpipeline/opt/xpipeline/dev/share/fastclusterprop/
            addpath /home/edaw/libframe/matlab
            baseDir = '/home/patrick.sutton/dwdm/runs/';
       otherwise
            error('System not recognised.');
    end
else
    error('System not recognised.');
end


% ------------------------------------------------------------------------------
%    Magic numbers, etc.
% ------------------------------------------------------------------------------

speedOfLight = 299792458;

% ---- Load detector network.
detectors = {'H','L'};
detector{1} = LoadDetectorData('H');
detector{2} = LoadDetectorData('L');

if fullRun
    
    % ---- Observing runs analysed.
    obsRuns = {'o1','o2','o3a'};

    % % ---- analysisTime values used.
    % analysisTimes = [1   1/2  1/4  1/8  1/16  1/32];
    % 
    % % ---- Link analysisTime values to wall number.
    % wallNumbers   = [29  100  101  105  1     102];

    % ---- Coincidence window used for corresponding wall number.
    nongwcoincidencewindow = [118 62 40 24 7 4]; 

    % ---- Wall numbers corresponding to each analysisTime.
    %      analysisTimes = 1/32
    injSets{6} = [102 4 8 10 13 15 20 22 24 27 31 32 35 37 42 43 44 48 54 56 57 ...
        60 61 66 69 81 82 85 95 97 203 206 209 210 211 214 216 218 228 230 232 ...
        234 239 245 253 257 259 260 264 265 267 269 273 274 275 276 278 282 283 ...
        288 289 290 292 293 297 801 806 809 810 811 814 815 816 817 818 822 825 ...
        829 833 834 839 842 845 848 851 852 857 858 860 862 863 864 867 868 869 ...
        870 871 873 876 877 879 885 892 895 897 899 1001 1003 1006 1008 1013 ...
        1014 1015 1016 1022 1026 1027 1028 1031 1034 1036 1039 1041 1043 1044 ...
        1048 1050 1055 1057 1059 1061 1062 1072 1074 1075 1082 1085 1087 1088 ...
        1089 1090 1095 1100];
    %      analysisTimes = 1/16  
    injSets{5} = [1 3 6 7 19 21 23 34 36 38 39 40 41 45 53 55 59 65 67 72 73 76 ...
        77 78 79 86 87 92 93 94 96 99 205 207 208 212 213 219 220 222 224 225 ...
        226 235 238 242 244 250 254 255 258 262 266 268 270 271 280 281 285 294 ...
        299 300 804 805 807 812 813 819 823 826 828 830 831 837 838 844 846 849 ...
        854 856 859 861 865 872 875 878 880 882 887 888 890 891 894 896 1005 ...
        1007 1011 1020 1021 1023 1035 1037 1038 1045 1046 1049 1051 1053 1063 ...
        1064 1065 1067 1068 1070 1071 1073 1080 1081 1083 1086 1091 1093 1097];
    %      analysisTimes = 1/8
    injSets{4} = [105 2 9 11 12 17 26 28 33 47 49 52 62 64 68 71 74 75 80 88 90 ...
        98 201 204 215 217 221 223 227 229 236 237 240 247 249 251 272 277 286 ...
        287 291 802 803 808 820 821 835 841 850 855 884 889 898 1004 1009 1010 ...
        1017 1018 1019 1024 1025 1029 1032 1033 1042 1052 1058 1060 1076 1077 ...
        1079 1092 1094 1098];
    %      analysisTimes = 1/4
    injSets{3} = [101 5 16 25 30 46 51 63 70 89 233 248 261 279 298 824 827 832 ...
        836 843 874 886 893 1030 1040 1056 1069 1096 1099];
    %      analysisTimes = 1/2
    injSets{2} = [100 14 18 58 84 202 231 246 252 263 296 847 853 866 1012 1054 ...
        1066 1078];
    %      analysisTimes = 1
    injSets{1} = [29 83 91 243 284 1084];

else
    
    % ---- Process only one file, for testing and debugging.
    warning('TEST RUN: processing one injection file only.')
    obsRuns = {'o1'}; 
    injSets{1} = 101;
    nongwcoincidencewindow(1) = 40;
    
end

% ---- Loop over all injection files.
for irun=1:length(obsRuns)

    for iset=1:length(injSets)

        for iwall=1:length(injSets{iset})

            % ---- Select run.
            wallNumber = num2str(injSets{iset}(iwall));
            valid = true;

            % ---- Read injection file and extract needed parameters.
            fileName = [baseDir obsRuns{irun} '/' wallNumber '/injection_domainwallq' wallNumber '.txt'];
            params = parseinjectionparameters(readinjectionfile(fileName),false);
            Ninj = size(params.gps_s,1);
            % ---- params fields:
            %          gps_s: [2000�2 double]
            %         gps_ns: [2000�2 double]
            %            phi: [2000�2 double]
            %          theta: [2000�2 double]
            %            psi: [2000�2 double]
            %           type: {2000�2 cell}
            %     parameters: {2000�2 cell}
            
            % ---- Sanity checks.
            if ~isequal(params.theta(:,1),params.theta(:,2)) || ~ isequal(params.phi(:,1),params.phi(:,2))
                warning(['Wall ' wallNumber ': inconsistent sky positions for H and L.']);
                valid = false;
            end
            peakTime = params.gps_s + 1e-9 * params.gps_ns;
            theta = params.theta(:,1);
            phi   = params.phi(:,1);
            
            % ---- Extract wall parameters (ordering: {A,d,v,theta,phi,ifo}).
            %      All injections in a given file have the same A,d,v, so just
            %      need to check the first injection.
            tmp = tildedelimstr2numorcell(params.parameters{1});
            A = tmp{1};
            d = tmp{2};
            speedOfWall = tmp{3};

            % ---- Sanity checks.
            if any(norm(detector{1}.V-detector{2}.V)/speedOfWall > nongwcoincidencewindow(iset))
                warning(['Wall ' wallNumber ': b/speed > window. Set unphysical.']);
                valid = false;
            end

            % ---- Unit vector pointing towards source (one row per injection).
            omega = [sin(theta).*cos(phi) , sin(theta).*sin(phi) , cos(theta)];

            % ---- Reset "COE" arrival times separately for each detector to compensate
            %      for speedOfWall. Time delay for incoming signal is defined wrt
            %      center of Earth (t_det - t_coe). 
            for iDet=1:length(detectors)
                delayLight(:,iDet) = - (omega * detector{iDet}.V) ./ speedOfLight;
                delayWall(:,iDet)  = - (omega * detector{iDet}.V) ./ speedOfWall;
                % ---- Peak time in injection file is constructed as follows:
                % peakTime(:,iDet)   = peakTime(:,iDet) + delayWall(:,iDet) - delayLight(:,iDet);
            end
            % ---- Compute error in delay (file - expected); hopefully this will be zero.
            %      Define delay as t_H - t_L.
            delay_exp = (delayWall(:,1) - delayLight(:,1)) - (delayWall(:,2) - delayLight(:,2));
            delay_act = peakTime(:,1)-peakTime(:,2);
            % ---- Hong randomised the peak times in injection files to unphsyical
            %      values. Need to find which injections were close enough to
            %      correct delays to be useful.  
            % % ---- Find any injections where the peak time delays are
            % %      within specified tolerance of the correct value.
            % tolerance = 0.5;  %-- [s]. The TOA bin width is 1 sec.
            % delay_err = delay_act-delay_exp;
            % idx = find(abs(delay_err)<tolerance);
            % ---- The TOA bin width is 1 sec, with bin edges at integer values. So
            %      the following selects all injections where the injected peak time
            %      delay falls into the same bin as the physical delay.
            idx = find(floor(delay_act)==floor(delay_exp));
            disp(['Wall ' wallNumber ' : ' num2str(length(idx)) ' injections have acceptable time delays.'])
            % disp(['Wall ' wallNumber ' delay correlation: ' num2str(corr(delay_exp,delay_act))])

            % ---- Record details of valid injections, if any.
            if length(idx)>0 && valid
                validinj{irun}{iset}{iwall}.idx        = idx;
                validinj{irun}{iset}{iwall}.gps_s      = params.gps_s(idx,:);
                validinj{irun}{iset}{iwall}.gps_ns     = params.gps_ns(idx,:);
                validinj{irun}{iset}{iwall}.phi        = params.phi(idx,:);
                validinj{irun}{iset}{iwall}.theta      = params.theta(idx,:);
                validinj{irun}{iset}{iwall}.psi        = params.psi(idx,:);
                validinj{irun}{iset}{iwall}.type       = params.type(idx,:);
                validinj{irun}{iset}{iwall}.parameters = params.parameters(idx,:);
            else
                validinj{irun}{iset}{iwall} = [];
            end

        end  %-- wall numbers

    end  %-- injSets

end  %-- obsRuns

% ---- Save results.
save('validinj.mat','validinj');
