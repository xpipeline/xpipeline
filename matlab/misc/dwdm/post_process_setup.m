% $Id$

% This script sets user-specified values & "magic numbers" for post-processing
% as well as needed paths.


% ------------------------------------------------------------------------------
%    User-specified parameters.
% ------------------------------------------------------------------------------

% ---- Full run or test run?
fullRun = true

% ---- Tag for output file names (arbitrary string).
runTag = 'full';

% ---- Closed-box or open-box run?
% mode = 'training' %-- closed box
mode = 'testing'  %-- open box

% ---- Target FAR for efficiencies; verbosity, save plots. It is over-written 
%      by the min on-source FAR for 'testing' (open-box) analyses.
thresholdFAR = 1/(365.25*86400);  %-- 1/year 

% ---- Minimum significance (abs(cc)) for which off-source triggers were saved.
minSignificance = 20;

% ---- Extra verbosity?
verbose = true;

% ---- Make/save plots?
makePlots = true;
savePlots = true;

% ---- Apply TOA weighting?
applyTOAWeighting = false;

% ---- Apply DQ vetoes?
applyDQVetoes = true;

% ---- Apply Gravity-Spy vetoes?
applyGravitySpyVetoes = true;
gravitySpyClassificationConfidence = 0.90;
gravitySpyMinDuration = 1.0001;  %-- [sec] 

% ---- Apply H/L amplitude ratio test?
applyAmplRatio = true;
amplRatioThreshold = 1; %-- |log10(H/L)| <= amplRatioThreshold

% ---- Save temporary copies of data files for every iat & ior loop (18).
saveTmpFiles = false;

% ---- Fix a non-negative integer seed value for repeatable random number generation.
seedValue = 836465; 


% ------------------------------------------------------------------------------
%    Generate file names automatically based on run parameters.
% ------------------------------------------------------------------------------

% ---- Tag for output file names.
switch mode
    case 'training'
        runName = ['closedbox-' runTag '-'];
    case 'testing'
        runName = ['openbox-' runTag '-'];
    otherwise 
        error(['mode ' mode ' not recognised.']);
end
if applyTOAWeighting
	runName = [runName 'y'];
else
	runName = [runName 'n'];
end
if applyDQVetoes
	runName = [runName 'y'];
else
	runName = [runName 'n'];
end
if applyGravitySpyVetoes
	runName = [runName 'y'];
else
	runName = [runName 'n'];
end
if applyAmplRatio
	runName = [runName 'y'];
else
	runName = [runName 'n'];
end
% ---- DO NOT EDIT. This file holds the triggers. It is created by the script 
%      post_process_load_data.m. It is redundant if outputFileName is also
%      generated.
triggerFileName = ['post_process_data_' runName '_triggers.mat']; 
% ---- DO NOT EDIT. This file holds all the data in triggerFileName plus the 
%      efficiencies and other post-processing results. It is created by the
%      script post_process_efficiencies.
outputFileName  = ['post_process_data_' runName '.mat']; 
disp(['runName:         ' runName]);
disp(['triggerFileName: ' triggerFileName]);
disp(['outputFileName:  ' outputFileName]);


% ------------------------------------------------------------------------------
%    Set path as needed.
% ------------------------------------------------------------------------------

% ---- Needed directories and files.
[status,result] = system('uname');
if status==0 
    % switch lower(result(1:end-1))
    switch lower(strtrim(result))
        case 'darwin'
%             if fullRun
%                 warning('Unable to perform full run on laptop; switching to test mode.')
%                 fullRun = false;
%             end
            dataBaseDir  = '/Users/psutton/Documents/xpipeline/trunk/matlab/misc/dwdm/domainwall_pp/';
            vetoBaseDir  = '/Users/psutton/Documents/xpipeline/branches/scripts/input/';
            gspyBaseDir  = '/Users/psutton/Documents/xpipeline/trunk/matlab/misc/dwdm/domainwall_pp/inputdata/';
            weightFile   = '/Users/psutton/Documents/xpipeline/trunk/matlab/misc/dwdm/TOAweighting_Nmc1e7_Nbin100.mat';
            if fullRun
                validInjFile = '/Users/psutton/Documents/xpipeline/trunk/matlab/misc/dwdm/validinj_full.mat';
            else
                validInjFile = '/Users/psutton/Documents/xpipeline/trunk/matlab/misc/dwdm/validinj_test.mat';            
            end
        case 'linux'
            dataBaseDir = '/home/patrick.sutton/dwdm/runs/';
            vetoBaseDir = '/home/xpipeline/xpipeline/branches/scripts/input/';
            gspyBaseDir = '/home/patrick.sutton/dwdm/post-processing/inputdata/';
            weightFile  = '/home/patrick.sutton/xpipeline/trunk/matlab/misc/dwdm/TOAweighting_Nmc1e7_Nbin100.mat';
            if fullRun
                validInjFile = '/home/patrick.sutton/xpipeline/trunk/matlab/misc/dwdm/validinj_full.mat';
            else
                validInjFile = '/home/patrick.sutton/xpipeline/trunk/matlab/misc/dwdm/validinj_test.mat';            
            end
            addpath /home/xpipeline/xpipeline/trunk/matlab/share/
            addpath /home/xpipeline/xpipeline/trunk/matlab/searches/grb
            addpath /home/xpipeline/xpipeline/trunk/matlab/searches/sn
            addpath /home/xpipeline/xpipeline/trunk/matlab/utilities/  
            addpath /home/xpipeline/opt/xpipeline/dev/share/fastclusterprop/
            addpath /home/edaw/libframe/matlab
       otherwise
            error(['System ' lower(result(1:end-1)) ' not recognised.']);
    end
    TOAweights  = load(weightFile);
    load(validInjFile);
else
    error('System not recognised.');
end


% ------------------------------------------------------------------------------
%    Magic numbers, etc.
% ------------------------------------------------------------------------------

% ---- Number of injection scales tested (same for all injection sets).
NinjScales = 43;

% ---- Number of injections at each amplitude (same for all injection sets).
NinjTotal = 2000;

% ---- Observing runs analysed.
obsRuns = {'o1','o2','o3a'};

% ---- analysisTime values used.
analysisTimes = [1   1/2  1/4  1/8  1/16  1/32];

% ---- Link analysisTime values to wall number.
wallNumbers   = [29  100  101  105  1     102];

% ---- Coincidence window used for corresponding wall number above. Note 
%      that we take the on-source zero-lag triggers from these wall numbers 
%      so we shoul duse the matching coincidence windows for efficiencies.
nongwcoincidencewindow = [118 62 40 24 7 4]; 

if fullRun
    
    % ---- All wall numbers corresponding to each analysisTime.
    %      analysisTimes = 1/32
    injSets{6} = [102 4 8 10 13 15 20 22 24 27 31 32 35 37 42 43 44 48 54 56 57 ...
        60 61 66 69 81 82 85 95 97 203 206 209 210 211 214 216 218 228 230 232 ...
        234 239 245 253 257 259 260 264 265 267 269 273 274 275 276 278 282 283 ...
        288 289 290 292 293 297 801 806 809 810 811 814 815 816 817 818 822 825 ...
        829 833 834 839 842 845 848 851 852 857 858 860 862 863 864 867 868 869 ...
        870 871 873 876 877 879 885 892 895 897 899 1001 1003 1006 1008 1013 ...
        1014 1015 1016 1022 1026 1027 1028 1031 1034 1036 1039 1041 1043 1044 ...
        1048 1050 1055 1057 1059 1061 1062 1072 1074 1075 1082 1085 1087 1088 ...
        1089 1090 1095 1100];
    %      analysisTimes = 1/16  
    injSets{5} = [1 3 6 7 19 21 23 34 36 38 39 40 41 45 53 55 59 65 67 72 73 76 ...
        77 78 79 86 87 92 93 94 96 99 205 207 208 212 213 219 220 222 224 225 ...
        226 235 238 242 244 250 254 255 258 262 266 268 270 271 280 281 285 294 ...
        299 300 804 805 807 812 813 819 823 826 828 830 831 837 838 844 846 849 ...
        854 856 859 861 865 872 875 878 880 882 887 888 890 891 894 896 1005 ...
        1007 1011 1020 1021 1023 1035 1037 1038 1045 1046 1049 1051 1053 1063 ...
        1064 1065 1067 1068 1070 1071 1073 1080 1081 1083 1086 1091 1093 1097];
    %      analysisTimes = 1/8
    injSets{4} = [105 2 9 11 12 17 26 28 33 47 49 52 62 64 68 71 74 75 80 88 90 ...
        98 201 204 215 217 221 223 227 229 236 237 240 247 249 251 272 277 286 ...
        287 291 802 803 808 820 821 835 841 850 855 884 889 898 1004 1009 1010 ...
        1017 1018 1019 1024 1025 1029 1032 1033 1042 1052 1058 1060 1076 1077 ...
        1079 1092 1094 1098];
    %      analysisTimes = 1/4
    injSets{3} = [101 5 16 25 30 46 51 63 70 89 233 248 261 279 298 824 827 832 ...
        836 843 874 886 893 1030 1040 1056 1069 1096 1099];
    %      analysisTimes = 1/2
    injSets{2} = [100 14 18 58 84 202 231 246 252 263 296 847 853 866 1012 1054 ...
        1066 1078];
    %      analysisTimes = 1
    injSets{1} = [29 83 91 243 284 1084];

else

    if true

        warning('TEST RUN: processing two injection files per run and analysis time.')
        % ---- First two wall numbers corresponding to each analysisTime.
        %      analysisTimes = 1/32
        injSets{6} = [102 4];
        %      analysisTimes = 1/16  
        injSets{5} = [1 3]; 
        %      analysisTimes = 1/8
        injSets{4} = [105 2];
        %      analysisTimes = 1/4
        injSets{3} = [101 5];
        %      analysisTimes = 1/2
        injSets{2} = [100 14];
        %      analysisTimes = 1
        injSets{1} = [29 83];

    else

        % ---- Process only one file, for testing and debugging.
        warning('TEST RUN: processing one injection file only.')
        obsRuns = {'o1'}; 
        injSets{1} = 101;
        analysisTimes = 1/4;
        wallNumbers   = 101;
        nongwcoincidencewindow(1) = 40;

    end

end
    
% ---- Universal for all runs.
sampleFrequency = 4096; 
analysisLengths = analysisTimes * sampleFrequency;

% ---- GPS start and end times of the runs (as fed into x-pipeline for analysis).
%      Read from, eg /home/hong.qi/dmdw/o1/863/domainwall_o1.ini and 
%      /home/hong.qi/dmdw/o1/863/grb.param
%        (igwn) [patrick.sutton@ldas-grid dwdm]$ lal_tconvert 1126051217
%        Sat Sep 12 00:00:00 UTC 2015
%        (igwn) [patrick.sutton@ldas-grid dwdm]$ lal_tconvert 1136649617
%        Tue Jan 12 16:00:00 UTC 2016
%        (igwn) [patrick.sutton@ldas-grid dwdm]$ lal_tconvert 1164556817
%        Wed Nov 30 16:00:00 UTC 2016
%        (igwn) [patrick.sutton@ldas-grid dwdm]$ lal_tconvert 1187733618
%        Fri Aug 25 22:00:00 UTC 2017
%        (igwn) [patrick.sutton@ldas-grid dwdm]$ lal_tconvert 1238166018
%        Mon Apr 01 15:00:00 UTC 2019
%        (igwn) [patrick.sutton@ldas-grid dwdm]$ lal_tconvert 1253977218
%        Tue Oct 01 15:00:00 UTC 2019
%
%             start [s]  end [s]    duration [s]
obsRunGPS = [ 1126051217 1136649617 10598400; ...  % O1
              1164556817 1187733618 23176801; ...  % O2
              1238166018 1253977218 15811200]; ... % O3a

% ---- Fix the state of the random number generator.
rng(seedValue);

