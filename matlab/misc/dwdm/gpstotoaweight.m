function wf = gpstotoaweight(gps,delay,wstruct)
% GPSTOTOAWEIGHT - Compute TOA weighting given event data and weight struct.
%
% usage:
%
%  wf = gpstotoaweight(gps,delay,wstruct)
%
%  gps      Vector of GPS times [s].
%  delay    Vector of time delays (t_H - t_L) [s].
%  wstruct  Struct. Must have the fields 
%                 dt: vector of delay time bin edges used to compute weighting.
%                  w: matrix of weights. Must have size Nt x Ns, where 
%                     Nt = (length(dt)-1) is the number of delay bins and Ns is
%                     the number of sidereal bins used to compute weighting.
%                     The sidereal bins are assumed to cover the sidereal day
%                     range [0h,24h].
%
%  wf       Vector. Multiplicative weight factor for each corresponding
%           GPS & delay input.
%
% See STANDARDHALOMODELTOAWEIGHTING.
%
% $Id$

% ---- Checks.
narginchk(3,3);

% ---- Magic numbers.
% ---- Sidereal day, in seconds. See https://en.wikipedia.org/wiki/Sidereal_time#Sidereal_day
sidday  = 86164.0905;

% ---- Extract numbers of bins.
Nt = size(wstruct.w,1);
Ns = size(wstruct.w,2);
if Nt ~= length(wstruct.dt)-1
    error('TOA struct weight array has the wrong number of delay bins.')
end

% ---- Convert GPS time to GMST, then to fraction of sidereal day, and then
%      sidereal bin number. Note that sidereal time is defined to go from 0h to
%      24h (0 to 86400 sec) not 0h to 23h56m, so we must divide gmst by 86400 to
%      figure out the fraction of a sidereal day.
gmst = GPSTOGMST(gps);
gmst_bin = ceil( (gmst/86400) * Ns );

% ---- Convert delay to delay bin number. For delays outside the range covered,
%      use the first/last bin weight.
t_bin = ceil( (delay-wstruct.dt(1))/(wstruct.dt(2)-wstruct.dt(1)) );
t_bin(t_bin<1) = 1;
t_bin(t_bin>Nt) = Nt;

% ---- Pull out weights.
wf = wstruct.w(sub2ind(size(wstruct.w),t_bin,gmst_bin));

% ---- Done.
return
