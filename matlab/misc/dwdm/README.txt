Scripts:
--------
compare_backgrounds.m  - makes plots of the background triggers for o1/o2/o3.
		 	 intended for sanity checks of the background.
post_process_data.m    - master post-processing script. computes ULs and detection candidates.

Helper functions:
-----------------
computefar             - compute FAR of trigger by comparing to background distribution.
extractinjcctrig       - extract triggers best matching injection times
gpstotoaweight         - compute TOA weighting given event data and weight struct.
loadinjtriggers        - load x-pipeline triggers from injection job files
loadoffsourcetriggers  - load x-pipeline triggers from an off-source job file
standardhalomodeltoaweighting - simulate H-L TOA differences for SHM 
unslidtriggertime      - compute trigger peak times corrected for time slides
weightedhistcounts     - histogram bin counts with weighting

