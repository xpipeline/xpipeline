% $Id$

% This script makes plots of the analysis results.


% ------------------------------------------------------------------------------
%    Plots.
% ------------------------------------------------------------------------------

% ---- Plot formatting.
fontSize = 16;

if applyTOAWeighting

    % --------------------------------------------------------------------------
    %    Efficiencies - TOA Weighting case
    % --------------------------------------------------------------------------

    % ---- Plot efficiencies: average over runs.
    figure;
    legendText = [];
    for ibin=1:Nbins
        semilogx(injScales*ABinValue{1}(ibin),efficiencyDAvg(ibin,:),'linewidth',2)
        grid on; hold on
        legendText{end+1} = [num2str(d_bins(ibin)) 'm<=d<' num2str(d_bins(ibin+1)) 'm'];
    end
    axis([get(gca,'xlim') 0 1])
    set(gca,'fontsize',16)
    xlabel('wall amplitude A')
    ylabel('efficiency')
    legend(legendText,'Location','NorthWest')
    title('combined efficiency')
    if savePlots
        saveas(gcf,['combined_efficiency.png'],'png')
    end

    % ---- Plot efficiencies: O1/O2/O3a separately.
    for ior = 1:length(obsRuns)
        figure;
        legendText = [];
        for ibin=1:Nbins
            semilogx(injScales*ABinValue{ior}(ibin),efficiencyD{ior}(ibin,:),'linewidth',2)
            grid on; hold on
            legendText{end+1} = [num2str(d_bins(ibin)) 'm<=d<' num2str(d_bins(ibin+1)) 'm'];
        end
        axis([get(gca,'xlim') 0 1])
        set(gca,'fontsize',16)
        xlabel('wall amplitude A')
        ylabel('efficiency')
        legend(legendText,'Location','NorthWest')
        title(['Observing Run ' obsRuns{ior}])
        if savePlots
            saveas(gcf,[obsRuns{ior} '_efficiency.png'],'png')
        end
    end

end %-- if applyTOAWeighting    

if true

    % --------------------------------------------------------------------------
    %    Backgrounds.
    % --------------------------------------------------------------------------

    % ---- Plot the FAR vs. |cc| scores.
    for ior = 1:length(obsRuns)
        figure;
        for iat=1:length(analysisTimes)    
            loglog(bckgrd{iat,ior}.sig,bckgrd{iat,ior}.far,'linewidth',2)
            hold on
        end
        grid on
        legend('1','1/2','1/4','1/8','1/16','1/32')
        set(gca,'fontsize',16)
        xlabel('|cc| * TOA weight')
        ylabel('FAR [Hz]')
        title([obsRuns{ior} ' - FAR'])
        if savePlots
            saveas(gcf,[obsRuns{ior} '_far_distn.png'],'png')
        end
    end

    % ---- Plot the cumulative distribution of |cc| scores.
    for ior = 1:length(obsRuns)
        figure;
        for iat=1:length(analysisTimes)    
            val = sort(offsource{iat,ior}.cluster.significance .* offsource{iat,ior}.cluster.toaWeight);
            N = length(val);
            loglog(val,[N:-1:1]'/N,'linewidth',2)
            hold on
        end
        grid on
        legend('1','1/2','1/4','1/8','1/16','1/32')
        set(gca,'fontsize',16)
        xlabel('|cc| * TOA weight')
        ylabel('cumulative fraction of triggers')
        title([obsRuns{ior} ' - Background'])
        if savePlots
            saveas(gcf,[obsRuns{ior} '_bckgrd_cum_cc_distn.png'],'png')
        end
    end

    % ---- Sanity check: Plot histogram of likelihood(:,end), which is the
    %      intra-analysisLength time shift at which max |cc| occurs.
    for ior = 1:length(obsRuns)
        figure; 
        for iat=1:length(analysisTimes)
            histogram(offsource{iat,ior}.cluster.likelihood(:,end) / analysisLengths(iat))
            hold on
        end
        legend('1','1/2','1/4','1/8','1/16','1/32')
        set(gca,'fontsize',16)
        xlabel('cc_{idx}')
        ylabel('number of triggers')
        title([obsRuns{ior} ' - Background'])
        grid on
        if savePlots
            saveas(gcf,[obsRuns{ior} '_bckgrd_cc_idx_distn.png'],'png')
        end
    end

    % ---- Sanity check: Plot histogram of t_L-t_H.
    for ior = 1:length(obsRuns)
        figure; 
        for iat=1:length(analysisTimes)
            subplot(2,3,iat)
            histogram(-diff(offsource{iat,ior}.cluster.likelihood(:,5:6),[],2))
            hold on
            legend(num2str(analysisTimes(iat)),'Location','SouthEast')
            set(gca,'fontsize',14)
            xlabel('t_H - t_L [s]')
            ylabel('number of triggers')
            title([obsRuns{ior} ' - Background'])
            grid on
        end
        pos = get(gcf,'position');
        set(gcf,'position',[pos(1:2) 2000 1000])
        if savePlots
            saveas(gcf,[obsRuns{ior} '_bckgrd_delta_t_distn.png'],'png')
        end
    end

    % --------------------------------------------------------------------------
    %    Other: bckgrd vs injection properties for selected injection set.
    % --------------------------------------------------------------------------

    % ---- Pick some injection scales to plot triggers ...
    %      We choose to plot injections for injection scales 13 and 19 for wall
    %      102 and background triggers from iat==6, o2 run (which should be our
    %      most sensitive dataset).
    % plotScales = [isn50,isn90];
    plotScales = [13,19];
    wallNumber = 102;
    injData(:,:,1) = injtrigAll{wallNumber}{plotScales(1)}.likelihood;
    injData(:,:,2) = injtrigAll{wallNumber}{plotScales(2)}.likelihood;
    run = 'o2';
    offData = offsource{6,2}.cluster.likelihood;
    
    % ---- p(|cc|/H/L) - logscale
    figure;
    h_off = histogram(offData(:,1)./ offData(:,3)./ offData(:,4),'Normalization','probability');
    clear legendText; legendText{1} = ['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'];
    grid on; hold on
    for ii = 1:length(plotScales)
        histogram(injData(:,1,ii)./injData(:,3,ii)./injData(:,4,ii),h_off.BinEdges ,'Normalization','probability');
        legendText{end+1} = ['injections (scale: ' num2str(ii) ')'];
    end
    set(gca,'fontsize',fontSize)
    xlabel('|cc|/|H|/|L|')
    ylabel('probability density')
    % legend(['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'],['injections (scale: ' injScale ')'])
    legend(legendText)
    if savePlots
        saveas(gcf,[obsRun '_' num2str(analysisTime) '_' run '_1.png'],'png')
    end

    % ---- p(|cc|) - logscale
    figure;
    histogram(log10(offData(:,1)),'Normalization','probability');
    clear legendText; legendText{1} = ['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'];
    grid on; hold on
    for ii = 1:length(plotScales)
        histogram(log10(injData(:,1,ii)),'Normalization','probability');
        legendText{end+1} = ['injections (scale: ' num2str(ii) ')'];
    end
    set(gca,'fontsize',fontSize)
    xlabel('log_{10}(|cc|)')
    ylabel('probability density')
    legend(legendText)
    if savePlots
        saveas(gcf,[obsRun '_' num2str(analysisTime) '_' run '_2.png'],'png')
    end

    % ---- p(H/L) - logscale
    figure;
    histogram(log10(offData(:,3)./offData(:,4)),'Normalization','probability')
    clear legendText; legendText{1} = ['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'];
    grid on; hold on
    for ii = 1:length(plotScales)
        histogram(log10(injData(:,3,ii)./injData(:,4,ii)),'Normalization','probability')
        legendText{end+1} = ['injections (scale: ' num2str(ii) ')'];
    end
    set(gca,'fontsize',fontSize)
    xlabel('log_{10}(|H|/|L|)')
    ylabel('probability density')
    legend(legendText)
    if savePlots
        saveas(gcf,[obsRun '_' num2str(analysisTime) '_' run '_3.png'],'png')
    end

    % figure;
    % plot(sort(abs(log10(offData(:,3)./offData(:,4)))),(1:Noff)/Noff)
    % grid on; hold on
    % plot(sort(abs(log10(injData(:,3,ii)./injData(:,4,ii)))),(1:Ninj)/Ninj)
    % set(gca,'fontsize',fontSize)
    % xlabel('abs(log_{10}(|H|/|L|))')
    % ylabel('cumulative distribution')
    % legend(['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'],['injections (scale: ' injScale ')'])

    % figure;
    % plot(abs(log10(offData(:,3)./offData(:,4))),offData(:,1)./offData(:,3)./offData(:,4),'.')
    % grid on; hold on
    % plot(abs(log10(injData(:,3,ii)./injData(:,4,ii))),injData(:,1,ii)./injData(:,3,ii)./injData(:,4,ii),'.')
    % set(gca,'fontsize',fontSize)
    % xlabel('abs(log_{10}(|H|/|L|))')
    % ylabel('|cc|/|H|/|L|')
    % legend(['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'],['injections (scale: ' injScale ')'])

    % ---- p(|cc|) vs. p(|cc|/H/L)
    figure;
    plot(offData(:,1)./offData(:,3)./offData(:,4),log10(offData(:,1)),'*');
    clear legendText; legendText{1} = ['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'];
    grid on; hold on
    for ii = 1:length(plotScales)
        plot(injData(:,1,ii)./injData(:,3,ii)./injData(:,4,ii),log10(injData(:,1,ii)),'.');
        legendText{end+1} = ['injections (scale: ' num2str(ii) ')'];
    end
    set(gca,'fontsize',fontSize)
    xlabel('|cc|/|H|/|L|')
    ylabel('log_{10}(|cc|)')
    legend(legendText,'Location','SouthEast')
    if savePlots
        saveas(gcf,[obsRun '_' num2str(analysisTime) '_' run '_4.png'],'png')
    end

    % ---- p(|cc|) vs. p(H/L)
    figure;
    plot(abs(log10(offData(:,3)./offData(:,4))),log10(offData(:,1)),'*');
    clear legendText; legendText{1} = ['off-source (duration: ' num2str(cell2mat(off.analysisTimesCell)) 's)'];
    grid on; hold on
    for ii = 1:length(plotScales)
        plot(abs(log10(injData(:,3,ii)./injData(:,4,ii))),log10(injData(:,1,ii)),'.');
        legendText{end+1} = ['injections (scale: ' num2str(ii) ')'];
    end
    set(gca,'fontsize',fontSize)
    xlabel('|log_{10}(|H|/|L|)|')
    ylabel('log_{10}(|cc|)')
    legend(legendText,'Location','SouthEast')
    if savePlots
        saveas(gcf,[obsRun '_' num2str(analysisTime) '_' run '_5.png'],'png')
    end
    
    %     % ---- Comparison of different treatments of the amplitude ratio.
    %     chk1 = load('efficiencies/closedbox01-yyyn/post_process_data_closedbox01-yyyn.mat','efficiencyDAvg')
    %     chk2 = load('efficiencies/closedbox01-yyyy_G0.6/post_process_data_closedbox01-yyyy_ARGaussian.mat','efficiencyDAvg')
    %     chk3 = load('efficiencies/closedbox01-yyyy/post_process_data_closedbox01-yyyy.mat','efficiencyDAvg')
    % 
    %     lineColor = {'b','r','c','m','g'}
    % 
    %     % ---- Plot efficiencies: average over runs.
    %     figure;
    %     legendText = [];
    %     for ibin=1:Nbins
    %         semilogx(injScales*ABinValue{1}(ibin),chk1.efficiencyDAvg(ibin,:),[lineColor{ibin} '-'],'linewidth',2)
    %         grid on; hold on
    %     end
    %     for ibin=1:Nbins
    %         semilogx(injScales*ABinValue{1}(ibin),chk2.efficiencyDAvg(ibin,:),[lineColor{ibin} '-.'],'linewidth',2)
    %     end
    %     for ibin=1:Nbins
    %         semilogx(injScales*ABinValue{1}(ibin),chk3.efficiencyDAvg(ibin,:),[lineColor{ibin} '--'],'linewidth',2)
    %     end
    %     set(gca,'fontsize',16)
    %     xlabel('wall amplitude A')
    %     ylabel('efficiency')
    %     title('Effect of Gaussian Amplitude Ratio Weighting (std.dev.=0.6) or Hard Cut')
    %     legendText =  {...
    %         'Not weighted: 0m<=d<1m',    'Not weighted: 1m<=d<10m',    'Not weighted: 10m<=d<100m',    'Not weighted: 100m<=d<1000m',    'Not weighted: 1000m<=d<10000m', ...
    %         'Weighted: 0m<=d<1m',    'Weighted: 1m<=d<10m',    'Weighted: 10m<=d<100m',    'Weighted: 100m<=d<1000m',    'Weighted: 1000m<=d<10000m', ...
    %         'Hard cut: 0m<=d<1m',    'Hard cut: 1m<=d<10m',    'Hard cut: 10m<=d<100m',    'Hard cut: 100m<=d<1000m',    'Hard cut: 1000m<=d<10000m'};
    %     legend(legendText,'Location','SouthEast')
    %     axis([1e-17 1e-13 0 1])
    %     if savePlots
    %         saveas(gcf,['combined_efficiency_not-weighted_vs_weighted_vs_hard-cut.png'],'png')
    %     end
    
end

