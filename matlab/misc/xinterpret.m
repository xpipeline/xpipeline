function xinterpret(skyPositions, nullPowerMap, incoherentPowerMap, degreesOfFreedom, channelNames)
% XINTERPRET Interpret sky maps for statistically significant results
%
% XINTERPRET displays a scatter plot of residual against null power for
% each element, band, and sky position.  All points associated with a
% particular band and element are plotted in a common color.  If a
% candidate point is identified, a map of null power over the sky for that
% element and band is plotted.
%
% usage:
%
%   xinterpret(skyPositions, skyMap, residualMap, degreesOfFreedom, channelNames);
%
%   skyPositions      matrix of sky positions [radians]
%   skyMap            sky map returned by XMAPSKY
%   residualMap       sky map returned by XMAPSKY
%   degreesOfFreedom  number of degrees of freedom for statistical testing
%   channelNames      cell array of channel name strings
%
% The desired sky position should be provided as a two column matrix of the form
% [theta phi], where theta is the geocentric colatitude running from 0 at the
% North pole to pi at the South pole and phi is the geocentric longitude in
% Earth fixed coordinates with 0 on the prime meridian.
%
% The sky map is a three dimensional array with the following form.
%
%   skyMap(frequencyBandIndex, elementIndex, skyPositionIndex).
%
% The residual map is a three dimensional array with the following form.
%
%   residualMap(frequencyBandIndex, elementIndex, skyPositionIndex).
%
% The optional cell array of channel name strings is used to label detector
% zenith directions and pairwise detector baseline directions on any spherical
% sky map figures.  If no channel names are specified, only the cartesian
% coordinate axes are labelled.
%
% See also XPIPELINE, XREADDATA, XINJECTSIGNAL, XCONDITION, XTILESKY, and
% XMAPSKY.

% Authors:
%   
%   Shourov Chatterji   shourov@ligo.caltech.edu
%   Albert Lazzarini    lazz@ligo.caltech.edu
%   Antony Searle       antony.searle@anu.edu.au
%   Leo Stein           lstein@ligo.caltech.edu
%   Patrick Sutton      psutton@ligo.caltech.edu
%   Massimo Tinto       massimo.tinto@jpl.nasa.gov

% $Id$

% check for sufficient command line arguments
error(nargchk(4, 5, nargin));

% default arguments
if nargin == 4,
  channelNames = {};
end

% number of elements
numberOfElements = size(nullPowerMap, 2);

% number of frequency bands
numberOfFrequencyBands = size(nullPowerMap, 1);

% number of sky positions
numberOfSkyPositions = size(skyPositions, 1);

% number of detectors
numberOfDetectors = length(channelNames);

% number of null streams
numberOfNullStreams = numberOfDetectors - 2;

% begin loop over frequency bands
for frequencyBandNumber = 1: numberOfFrequencyBands

  figure;
  clf;
  set(gca, 'FontSize', 16);
  plot(reshape(nullPowerMap(frequencyBandNumber, :, :), numberOfElements, ...
               numberOfSkyPositions)', ...
       reshape(incoherentPowerMap(frequencyBandNumber, :, :), numberOfElements, ...
               numberOfSkyPositions)', ...
       '.');
  xlabel('Null power');
  ylabel('Incoherent power');
  title(['Frequency band ' num2str(frequencyBandNumber)]);
  grid on;
  
%   % begin loop over elements
%   for elementNumber = 1 : numberOfElements
%       
%     % test for statistical significance
%     significant = (residualMap(frequencyBandNumber, elementNumber, :) > ...
%                    residualThreshold) & ...
%                   (skyMap(frequencyBandNumber, elementNumber, :) < ...
%                    nullThreshold);
%     significant = significant(:)';
% 
%     if any(significant),
% 
%       % display sky map
%       figure;
%       clf;
%       set(gca, 'FontSize', 16);
%       xproject(skyPositions, skyMap(frequencyBandNumber, elementNumber, :), ...
%                'sphere', channelNames);
%       title(['Frequency band ' num2str(frequencyBandNumber) ...
%              ' element ' num2str(elementNumber)]);           
%       colorbar;
%       hold on;
% 
%       % mark significant sky positions
%       for skyPositionNumber = find(significant),
%         theta = skyPositions(skyPositionNumber, 1);
%         phi = skyPositions(skyPositionNumber, 2);            
%         x = sin(theta) .* cos(phi);
%         y = sin(theta) .* sin(phi);
%         z = cos(theta);
%         plot3(x * [0 1.4], y * [0 1.4], z * [0 1.4], 'r-');     
%       end
%       hold off;
%       rotate3d on;
%     
%     end              
%   
%   % end loop over elements
%   end

% end loop over frequency bands
end

% save data to file
if ~isempty(strfind(version, '(R14)')),
  save skymap.mat skyPositions nullPowerMap incoherentPowerMap ...
       degreesOfFreedom channelNames -V6;
else
  save skymap.mat skyPositions nullPowerMap incoherentPowerMap ...
       degreesOfFreedom channelNames;
end
