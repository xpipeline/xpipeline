% $Id$

% load /archive/home/spoprock/detection/results_10_nowindow/results_0.mat
load /archive/home/spoprock/detection/results_20/results_0.mat

bitmapThresh = 10;
distanceThresh = 3;

likelihoodMapBlock = likelihoodMap([1:40],[880:920],1);
figure; imagesc(likelihoodMapBlock); colorbar;
bitmap = likelihoodMapBlock > bitmapThresh;
[x,y] = find(bitmap==1);
X = [x,y];
% Y = pdist(X,'euclidean');
Y = pdist(X,'cityblock');
Z = linkage(Y,'single');
% dendrogram(Z);
C = cluster(Z,'cutoff',distanceThresh,'criterion','distance');
clusteredIndex = zeros(size(bitmap));
clustered = zeros(size(bitmap));
clusterSums = zeros(max(C),1);

for i = 1:size(C,1)
    clusteredIndex(X(i,1),X(i,2)) = C(i);
    clusterSums(C(i)) = clusterSums(C(i)) + likelihoodMapBlock(X(i,1),X(i,2));
end

for i = 1:size(C,1)
    clustered(X(i,1),X(i,2)) = clusterSums(C(i));
end

figure; imagesc(clusteredIndex); colorbar;
figure; imagesc(clustered); colorbar;