dnl ============================
dnl == define target for make ==
dnl ============================
AC_DEFUN([SELECT_XTARGET],
[
dnl this line specifies the name of the target
dnl supplied by --with-xtarget=<target>. The default will be xdetection
AC_ARG_WITH(xtarget,
dnl this is the text help you get if you type ./configure --help
[  --with-xtarget=TARGET selects target, which can be xdetection, allsky, grb, glitch, sn, grbpprewrite or mva. The default is xdetection],
dnl this sets XTARGET to the one the user specified
XTARGET=${withval},
dnl default if no --with-xtarget in configuration line
XTARGET=xdetection)
dnl traps if user entered --with-xtarget or --without-xtarget
AC_MSG_CHECKING([for build target])
case ${XTARGET} in
xdetection)
    AC_MSG_RESULT([xdetection])
    NEEDPREREQ=yes
    ;;
allsky)
    AC_MSG_RESULT([allsky])
    NEEDPREREQ=yes
    ;;
grb)
    AC_MSG_RESULT([grb]);
    NEEDPREREQ=yes
    ;;
glitch)
    AC_MSG_RESULT([glitch])
    NEEDPREREQ=yes
    ;;
sn)
    AC_MSG_RESULT([sn])
    NEEDPREREQ=no
    ;;
grbpprewrite)
    AC_MSG_RESULT([grbbprewrite])
    NEEDPREREQ=no
    ;;
mva)
    AC_MSG_RESULT([mva])
    NEEDPREREQ=yes
    ;;
everything)
    AC_MSG_RESULT([everything])
    NEEDPREREQ=yes
    ;;
no)
    AC_MSG_RESULT([none])
    AC_MSG_ERROR([you cannot select no target using --without-xtarget here])
    ;;
yes)
    AC_MSG_RESULT([xdetection])
    XTARGET=xdetection
    AC_MSG_WARN([the target you selected using --with-xdetection was invalid. Using the default target, which is xdetection.])
    ;;
*)
     AC_MSG_RESULT([xdetection])
    XTARGET=xdetection
    AC_MSG_WARN([the target you selected using --with-xdetection was invalid. Using the default target, which is xdetection])
esac
dnl end of definition of function SELECT_XTARGET
])

AC_DEFUN([CHECK_MATLAB],
[
dnl == matlab itself ==
AC_ARG_WITH(matlab,
[  --with-matlab=DIR  the directory where MATLAB is installed ],
dnl == set MATLAB_ROOT to the specified directory if it was given
MATLAB_ROOT=${withval},
dnl == default yes
MATLAB_ROOT=yes)
dnl == matlab runtime environment ==
AC_ARG_WITH(mcr,
[  --with-mcr=DIR  the directory where the MATLAB runtime environment is installed],
MATLAB_MCR=${withval},
MATLAB_MCR=no)

dnl quit with error if somebody tried configure --without-matlab
if test ${MATLAB_ROOT} = "no"
then
    AC_MSG_ERROR([xpipeline requires MATLAB])
fi


dnl quit with error if somebody tried configure --without-matlab
if test ${MATLAB_ROOT} = "no"
then
    AC_MSG_ERROR([xpipeline requires MATLAB])
fi

dnl if --with-matlab was specified without giving a directory, find a working matlab

if test ${MATLAB_ROOT} = "yes"
then
    AC_MSG_CHECKING([for MATLAB installation])
    MATLAB_EXE=`which matlab`
dnl run matlab -n and extract TOOLBOX line from ensuing text
dnl this method handles the case where eg /usr/bin/matlab is a link
    MATLAB_BASE=`${MATLAB_EXE} -n | grep TOOLBOX | \
dnl remove whitespace
                 sed -e 's%\ %%g' \
dnl remove leading text before MATLAB location
                     -e 's%->sTOOLBOX=%%' \
dnl remove text following matlab location
                     -e 's%/toolbox%%'`
    AC_MSG_RESULT([${MATLAB_BASE}])
    AC_MSG_CHECKING([if ${MATLAB_BASE}/bin/matlab -n works])
dnl crude test that MATLAB is not some dummy useless placeholder or broken link
    MATLAB_TEST_RESULT=`${MATLAB_BASE}/bin/matlab -n | grep -c MATLAB`
    if test ${MATLAB_TEST_RESULT} != "0"
    then
        AC_MSG_RESULT([yes])
	MATLAB_ROOT=${MATLAB_BASE}
    else
    	AC_MSG_RESULT([no])
    	AC_MSG_ERROR([${MATLAB_BASE}/bin/matlab -n does not seem to run, check your distribution.])
    fi
else
dnl if --with-matlab=<DIR> was specified, check that this matlab works
    AC_MSG_CHECKING([if ${MATLAB_ROOT}/bin/matlab -n works])
    MATLAB_TEST_RESULT=`${MATLAB_ROOT}/bin/matlab -n | grep -c MATLAB`
    if test ${MATLAB_TEST_RESULT} != "0"
    then
        AC_MSG_RESULT([yes])	
    else
    	AC_MSG_RESULT([no])
    	AC_MSG_ERROR([${MATLAB_BASE}/bin/matlab -n does not seem to run, check your distribution.])
    fi
fi

dnl see if the user specified a different matlab runtime environment
AC_MSG_CHECKING([if you specified a separate MATLAB runtime environment])
if [ test ${MATLAB_MCR} = "no" ]
then
    AC_MSG_RESULT([no])
    MATLAB_MCR=${MATLAB_ROOT}
    AC_MSG_RESULT([Matlab runtime environment set to ${MATLAB_MCR}])
else
    AC_MSG_RESULT([yes])
    if [ test ${MATLAB_MCR} = "yes" ]
    then
dnl In the case where --with-mcr was specified without giving the location
	AC_MSG_ERROR([You must specify the location of the MATLAB Runtime Environment head directory (containing the appdata,bin,etc,sys,runtime,...etc directories)])
    else
dnl In the case where a separate MCR directory was specified
    AC_MSG_WARN([Ensure that your target system has the MATLAB Runtime Environment installed in directory ${MATLAB_MCR}, and that this installation is compatible with your MATLAB compiler (mcc) version.])
    fi
fi

dnl check that the computer hardware is a 64 bit intel x86 clone
AC_MSG_CHECKING([whether your computer is a 64 bit x86 system])
if test `uname -m` = "x86_64"
then
    AC_MSG_RESULT([yes])
else
    AC_MSG_RESULT([no])
    AC_MSG_ERROR([currently only 64 bit x86 systems are supported])
fi

dnl set MATLAB_ARCH
AC_MSG_CHECKING([MATLAB architecture])
MATLABSYSTEM=`uname`
case ${MATLABSYSTEM} in
Darwin)
    MATLAB_ARCH=maci64
    LDENV=DYLD_LIBRARY_PATH
    ;;
Linux)
    MATLAB_ARCH=glnxa64
    LDENV=LD_LIBRARY_PATH
    ;;
*)
    AC_MSG_RESULT([none])
    AC_MSG_ERROR([matlab architecture ${MATLABSYSTEM} not supported])
esac
AC_MSG_RESULT([${MATLAB_ARCH}])
AC_MSG_CHECKING([for mex file extension])
MEX_EXT=`${MATLAB_ROOT}/bin/mexext`
AC_MSG_RESULT([${MEX_EXT}])
AC_MSG_CHECKING([for mex command])
case ${MATLABSYSTEM} in
Darwin)
    MEXCOMMAND=${MATLAB_ROOT}/bin/mex
    ;;
Linux)
    MEXCOMMAND=${MATLAB_ROOT}/bin/mex
    ;;
*)
    AC_MSG_ERROR([unable to set mex command])
esac
AC_MSG_RESULT([${MEXCOMMAND}])

dnl determine whether CTGs are supported
AC_MSG_CHECKING([whether MATLAB supports CTFs])
if test -x ${MATLAB_BASE}/toolbox/compiler/deploy/${MATLAB_ARCH}/extractCTF
then
    AC_MSG_RESULT([yes])
    EXTRACTCTF=${MATLAB_BASE}/toolbox/compiler/deploy/${MATLAB_ARCH}/extractCTF
    EXTRACTCTFSCRIPT=extract_xpipeline_ctf.sh
    CTF_SUPPORTED="yes"
else
    AC_MSG_RESULT([no])
    EXTRACTCTF=""
    EXTRACTCTFSCRIPT=""
    CTF_SUPPORTED="no"
fi

dnl end of definition of function CHECK_MATLAB
])

dnl ============================================
dnl == Add seedless sources and flags, or not ==
dnl ============================================
AC_DEFUN([SELECT_SEEDLESS],
[
dnl this line specifies the name of the argument to configure which will
dnl be supplied with the syntax ./configure --with-seedless
AC_ARG_WITH(seedless,
dnl this is the text help you get if you type ./configure --help
[  --with-seedless enables comilation of seedless packages],
dnl define the string REPLACE_STRING to be substituted at the substitute stage
SEEDLESS_USE=${withval},
SEEDLESS_USE=no)
dnl if user invoked ./configure --with-oldmakefile then copy the old
dnl makefile style to Makefile.in, otherwise copy the new one
AC_MSG_CHECKING([whether to compile seedless packages])
if test ${SEEDLESS_USE} = yes
then
    AC_MSG_RESULT([yes])
    SEEDLESS="seedless/seedless.m seedless/initParallel.m seedless/ifo2dets.m seedless/gpu_tools/gArray.m seedless/gpu_tools/gColon.m seedless/gpu_tools/gGather.m seedless/gpu_tools/gLinspace.m seedless/gpu_tools/gOnes.m seedless/gpu_tools/gRand.m seedless/gpu_tools/gRandRamp.m seedless/gpu_tools/gZeros.m seedless/stochtrack/burstegardCut.m seedless/stochtrack/calF_stochtrack.m seedless/stochtrack/stochtrack.m seedless/stochtrack/stochtrackDefaults.m seedless/stochtrack/stochtrack_points.m seedless/stochtrack/stochtrack_tracks.m share/getdetector.m seedless/dealign_tfmap.m seedless/shift_vec.m seedless/align_tfmap.m"
    SEEDLESSFLAGS="-p distcomp"
else
    AC_MSG_RESULT([no])
    SEEDLESS=""
    SEEDLESSFLAGS=""
fi
   
dnl end of definition of function SELECT_SEEDLESS
])

dnl ==========================================
dnl == Find a working frgetvect mexecutable ==
dnl ==========================================
AC_DEFUN([EXTERNAL_FRGETVECT],
[
dnl this line handles the choice of an external path to frgetvect
dnl as specified using --with-frgetvectpath=<path>
AC_ARG_WITH(frgetvectpath,
[  --with-frgetvectpath enables specification of the full path to frgetvect],
FRGETVECT_USE=${withval},
FRGETVECT_USE=no)
dnl trap the case where --with-frgetvectpath is invoked without giving a path
AC_MSG_CHECKING([for frgetvect])
if test ${FRGETVECT_USE} = "yes"
then
    AC_MSG_RESULT([user supplied path to frgetvect missing])
    AC_MSG_WARN([you included --with-frgetvectpath, but without specifying the path])
    FRGETVECT_USE="no"		    
fi
dnl the case where --with-frgetvect isn't invoked at all, or was invoked without path above
if test ${FRGETVECT_USE} = "no"
then
    PKG_CHECK_MODULES([LIBFRAME], [libframe], , [echo "Not Found!" ; exit 1])
    FRGETVECT_USE=`pkg-config --variable=prefix libframe`/matlab
    AC_MSG_RESULT([using frgetvect at ${FRGETVECT_USE}])
dnl the case where --with-frgetvect=<user path> was given.
else
    if test -f ${FRGETVECT_USE}/frgetvect.${MEX_EXT}
    then
        AC_MSG_RESULT([${FRGETVECT_USE}])
    else
	AC_MSG_RESULT([not at the user supplied location])
	AC_MSG_WARN([no executable frgetvect.${MEX_EXT} at the location you specified])
        PKG_CHECK_MODULES([LIBFRAME], [libframe], , [echo "Not Found!" ; exit 1])
        FRGETVECT_USE=`pkg-config --variable=prefix libframe`/matlab
        AC_MSG_RESULT([using frgetvect at ${FRGETVECT_USE}])	
    fi
fi
dnl end of definition of function EXTERNAL_FRGETVECT
])
dnl ===========================================================================
dnl == Implement platform dependent mcc details and environment setup script ==
dnl ===========================================================================
AC_DEFUN([PLATFORM_DEPENDENT_SETUPS],
[
dnl figure out what platform we are on, and adjust setup accordingly
AC_MSG_CHECKING([target system])
TARGET_SYSTEM=`uname`
AC_MSG_RESULT([${TARGET_SYSTEM}])
if test ${TARGET_SYSTEM} = "Darwin"
then
    EXECFFD=".app"
    APPINSTALL="cp -Rf"
    SUBPATH="/Contents/MacOS/EXNAME_bin"
    echo "export DYLD_LIBRARY_PATH=${prefix}/lib:/usr/lib64:\$DYLD_LIBRARY_PATH" > envsetup.sh
    echo "export LIBPATH=\$DYLD_LIBRARY_PATH" >> envsetup.sh
else
    if test ${TARGET_SYSTEM} = "Linux"
    then
        EXECFFD=""
	APPINSTALL="install"
	echo "export LD_LIBRARY_PATH=/home/xpipeline/opt/libframe/lib/:${prefix}/lib:/usr/lib64:\$LD_LIBRARY_PATH" > envsetup.sh
	echo "export LIBPATH=\$LD_LIBRARY_PATH" >> envsetup.sh
    else
    AC_MSG_ERROR([Unsupported target, neither Linux or Darwin])
    fi
fi
echo "export XPIPE_INSTALL_BIN=${prefix}/bin" >> envsetup.sh
echo "export PATH=${prefix}/bin:\$PATH" >> envsetup.sh
AC_MSG_RESULT([source ./envsetup.sh to set up runtime environment])
dnl end of definition of function MCC_BY_PLATFORM
])
