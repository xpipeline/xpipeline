function [numMdc,mdcField,mdcData] = rdburstmdclog(logFile)
% RDBURSTMDCLOG - read Burst MDC log file
%   [numMdc,mdcField,mdcData] = rdburstmdclog(logFile)
%       logFile - name of Burst MDC logfile
%   numMdc - # of rows of data in log file
%   mdcField - array of field names from data
%   mdcData - structure with data vector for each field
%
%  - look at genburstmdclog.m for detail on field names
%   Expected fields are
%   GravEn_SimID, GravEn_Ampl, StartSamp1, StartSamp2,
%   Internal_x, Internal_phi, External_x, External_phi, External_psi,
%   FrameGPS, SimStartGPS, SimName, SimHrss,
%   <ifo>ctrGps,<ifo>fPlus,<ifo>fCross
%     where <ifo> = H1,H2,L1,G1,T1,V2
%
%   -- basically a wrapper for file2structarray
%
% $Id: rdburstmdclog.m,v 1.4 2007-08-01 15:23:02 kathorne Exp $

% READ data from file into columns with headings
% GET field names for output
numMdc = 0;
mdcField = [];
mdcData = [];
[numMdc,mdcData,numCol,colHead] = file2structarray(logFile);
if(numCol < 1)
    return
end
mdcField = fieldnames(mdcData);
return
