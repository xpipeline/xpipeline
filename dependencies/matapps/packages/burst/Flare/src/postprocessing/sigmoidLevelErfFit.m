function [hrss90, varargout] = ...
    sigmoidLevelErfFit(hrssVect, detectVect, fitoptions, plottitle, plotdir, plotname)
% 'sigmoidFit' Take in a vector of detected flags (ones or zeros) and corresponding
% hrss injected values and produce a sigmoid curve.
%
% Capable of using two distinct sigmoid functions - a complementary error
% function fit, and a logistics fit.
%
% [hrss50, hrss90, y] = sigmoidFit(hrssVect, detectVect, plotx, plottitle,
% plotdir, plotname) Make a sigmoid sensitivity fit.
%
% INTERFACE
%  hrssVect         -> input vector of hrss values to plot
%  detectVect       -> input vector of binary detect/did not detect
%  plottitle        -> title string for plot
%  plotdir          -> dir in which to save plot (optional, omit->not save)
%  plotname         -> name for the plot file (optional, omit->not save)
% fitoptions        -> struct, with optional fields:
%                      useScaleFactor - set to true for inspiral searches
%                      forceIsDiscrete - if true, fit will force discrete fitting
%                      isegw - if true, uses scale factor 1e7 instead of 1e21
%                      numDiscrete - if set, group the data into this many discrete bins.  use for WNB egw fits.
%---------------------------------------------------------------------
%  hrss50           <- sensitivity/upper limit statistic at 50%
%  hrss90           <- sensitivity/upper limit statistic at 90%
%  y                <- output values of the fit (corresponding to plotx)

% Peter Kalmus
% peter.kalmus@ligo.org
% $LastChangedDate: 2008-06-05 20:12:12 -0400 (Thu, 05 Jun 2008) $
% $Rev: 5098 $

if ~exist('plottitle', 'var')
    plottitle = 'no title';
end


[hrssVect, sortInd] = sort(hrssVect, 'ascend');
detectVect = detectVect(sortInd);

sigmoidErrPct = -1;
if iscol(hrssVect)
    hrssVect = hrssVect';
end
if iscol(detectVect)
    detectVect = detectVect';
end

useScaleFactor = false;
try
    useScaleFactor = fitoptions.useScaleFactor;
catch
end

forceIsDiscrete = false;
try
    forceIsDiscrete = fitoptions.forceIsDiscrete;
catch
end

isegw = false;
try
    isegw = fitoptions.isegw;
catch
end

isboot = false;
try
    isboot = fitoptions.isboot;
catch
end

try
    thisNumScaleFactors = fitoptions.numDiscrete;
catch
end

useErfFit = true;
try
    useErfFit = fitoptions.useErfFit;
catch
end

useHighModel = false;
try
    useHighModel = fitoptions.useHighModel;
catch
end


%% fit with a sigmoid and extract hrss50%.
% compare to the value from cal curve; compare to value from Sergei
% want to be able to get an x-value from a y-value.  just use interp

hrssMax = max(hrssVect);
hrssMin = min(hrssVect);

% get a complete plot for gwdaw.  todo:  refactor
plotx = logspace(log10(hrssMin), log10(hrssMax), 1000);

% only plot between max and min hrss
maxInd = max(findnearest(hrssMax, plotx));
minInd = min(findnearest(hrssMin, plotx));


if useErfFit
    scaleFac = 1;
    if isegw
        beta01Vect = [-10, -9, -8, -7, -6, -5];
    else
        beta01Vect = [-22, -21.5, -21.1, -21, -20.5, -20];
    end
    % scale
    beta0(2) = 1; %  1 / (hrssMax * scaleFac) * 20
    beta0(3) = -0.1;
    beta0(4) = 0.2;
    beta05Vect = [0, 0.05, 0.1];
    structCtr = 0;
    for ctr1 = 1 : length(beta01Vect)
        for ctr5 = 1 : length(beta05Vect)
            structCtr = structCtr + 1;
            betaRunStruct{structCtr}.beta01 = beta01Vect(ctr1);
            betaRunStruct{structCtr}.beta05 = beta05Vect(ctr5);
        end
    end
else
    if isegw
        scaleFac = 1e6;
        % shift
        beta01Vect = [1e-6 1e-5 1e-4 1e-3 1e-2 1e-1 1 10] * hrssMax/100 * scaleFac;
    else
        scaleFac = 1e21;
        % shift
        beta01Vect = [1e-4 1e-3 1e-2 1e-1 1 10] * hrssMax/10 * scaleFac;
    end

    if useScaleFactor
        scaleFac = 1;
    end


    % scale
    beta0(2) = 1;

    % level
    beta0(3) = 0.05;

    % unused
    beta0(4) = 0;
    beta05Vect = 0;

    structCtr = 0;
    for ctr1 = 1 : length(beta01Vect)
        for ctr5 = 1 : length(beta05Vect)
            structCtr = structCtr + 1;
            betaRunStruct{structCtr}.beta01 = beta01Vect(ctr1);
            betaRunStruct{structCtr}.beta05 = beta05Vect(ctr5);
        end
    end
end


% average, if it's a discrete plot
if exist('thisNumScaleFactors', 'var')
    if mod(length(hrssVect), thisNumScaleFactors) ~= 0
        error(sprintf('%s: numScaleFactors=%1.2f  lengthHrssVect=%1.2f',...
            thisNumScaleFactors, length(hrssVect)));
    end
    numPerScaleFactor = length(hrssVect)/thisNumScaleFactors;
    for i = 1 : thisNumScaleFactors
        uniqueHrss(i) = mean(hrssVect(1+(i-1)*numPerScaleFactor : i*numPerScaleFactor));
        uniqueDetect(i) = mean(detectVect(1+(i-1)*numPerScaleFactor : i*numPerScaleFactor));
    end
else
    uniqueHrss = unique(hrssVect);
end
isDiscrete = false;
defaultoptions = statset('nlinfit');
options = statset(defaultoptions, 'maxiter', 1000, 'DerivStep', eps^(1/2), 'TolFun', 1e-9);

% if there is a problem with this criteria, just check that each bin has same numElem
if (length(uniqueHrss) < 0.3 * length(hrssVect) || forceIsDiscrete) && ~exist('thisNumScaleFactors', 'var')
    isDiscrete = true;

    uniqueDetect = [];
    uniqueLengths = [];
    numDetected = [];
    numTried = [];
    for discreteCtr = 1 : length(uniqueHrss)
        discreteInd = find(hrssVect == uniqueHrss(discreteCtr));
        fractionVal = mean(detectVect(discreteInd));
        numTried(discreteCtr) = length(discreteInd);
        numDetected(discreteCtr) = sum(detectVect(discreteInd));
        uniqueLengths(discreteCtr) = length(detectVect(discreteInd));
        uniqueDetect(discreteCtr) = fractionVal;
    end

    gfVect = [];
    for betaCtr = 1 : length(betaRunStruct)
        beta0(1) = betaRunStruct{betaCtr}.beta01;
        beta0(5) = betaRunStruct{betaCtr}.beta05;
        warning off;
        if useErfFit
            [betaFitDiscreteTmp,rdiscreteTmp,JdiscreteTmp] = nlinfit(uniqueHrss * scaleFac, uniqueDetect, @erfcModel, beta0, options);
            yd = erfcModel(betaFitDiscreteTmp, uniqueHrss * scaleFac);
        else
            if useHighModel
                [betaFitDiscreteTmp,rdiscreteTmp,JdiscreteTmp] = nlinfit(uniqueHrss * scaleFac, uniqueDetect, @sigmoidPlusHighLevelModel, beta0, options);
                yd = sigmoidPlusHighLevelModel(betaFitDiscreteTmp, uniqueHrss * scaleFac);
            else 
                [betaFitDiscreteTmp,rdiscreteTmp,JdiscreteTmp] = nlinfit(uniqueHrss * scaleFac, uniqueDetect, @sigmoidPlusLevelModel, beta0, options);
                yd = sigmoidPlusLevelModel(betaFitDiscreteTmp, uniqueHrss * scaleFac);
            end
        end
        gf = gfit(uniqueDetect, yd);
        gfVect = [gfVect gf];
        fitOutputStruct{betaCtr}.beta = betaFitDiscreteTmp;
        fitOutputStruct{betaCtr}.r = rdiscreteTmp;
        fitOutputStruct{betaCtr}.J = JdiscreteTmp;
        warning on;
    end

    [minGf, fitInd] = min(gfVect);
    betaFitDiscrete = fitOutputStruct{fitInd}.beta;
    rdiscrete = fitOutputStruct{fitInd}.r;
    Jdiscrete = fitOutputStruct{fitInd}.J;

    if ~isboot
        display('betaFit:***');
        betaFitDiscrete(1)
        betaFitDiscrete(2)
        betaFitDiscrete(3)
        betaFitDiscrete(4)
        betaFitDiscrete(5)
    end

    betaFitDiscrete(1) = betaFitDiscrete(1) / scaleFac;
    betaFitDiscrete(2) = betaFitDiscrete(2) * scaleFac;
    if ~isboot
        display(sprintf('beta: %1.4e   alpha: %1.4e', betaFitDiscrete(1), betaFitDiscrete(2)));
    end

    if useErfFit
        yd = erfcModel(betaFitDiscrete, plotx);
    else
        if useHighModel
            yd = sigmoidPlusHighLevelModel(betaFitDiscrete, plotx);
        else 
            yd = sigmoidPlusLevelModel(betaFitDiscrete, plotx);
        end
    end

    level = yd(1);
    yscaled = yd - level;
    yscaled = yscaled / (1 - level);

    errs = sqrt( (uniqueDetect .* (1-uniqueDetect)) ./ uniqueLengths);
else
    %% THIS BLOCK WORKS FOR NON-DISCRETE (keep the "unique" names though)

    gfVect = [];
    for betaCtr = 1 : length(betaRunStruct)
        beta0(1) = betaRunStruct{betaCtr}.beta01;
        beta0(5) = betaRunStruct{betaCtr}.beta05;
        warning off;
        if useErfFit
            [betaFitDiscreteTmp,rdiscreteTmp,JdiscreteTmp] = nlinfit(uniqueHrss * scaleFac, uniqueDetect, @erfcModel, beta0, options);
            yd = erfcModel(betaFitDiscreteTmp, uniqueHrss * scaleFac);
        else
            [betaFitDiscreteTmp,rdiscreteTmp,JdiscreteTmp] = nlinfit(uniqueHrss * scaleFac, uniqueDetect, @sigmoidPlusLevelModel, beta0, options);
            yd = sigmoidPlusLevelModel(betaFitDiscreteTmp, uniqueHrss * scaleFac);
        end
        gf = gfit(uniqueDetect, yd);
        gfVect = [gfVect gf];
        fitOutputStruct{betaCtr}.beta = betaFitDiscreteTmp;
        fitOutputStruct{betaCtr}.r = rdiscreteTmp;
        fitOutputStruct{betaCtr}.J = JdiscreteTmp;
        warning on;
    end

    [minGf, fitInd] = min(gfVect);
    betaFitDiscrete = fitOutputStruct{fitInd}.beta;
    rdiscrete = fitOutputStruct{fitInd}.r;
    Jdiscrete = fitOutputStruct{fitInd}.J;

    if ~isboot
        display('betaFit:***');
        betaFitDiscrete(1)
        betaFitDiscrete(2)
        betaFitDiscrete(3)
        betaFitDiscrete(4)
        betaFitDiscrete(5)
    end

    betaFitDiscrete(1) = betaFitDiscrete(1) / scaleFac;
    betaFitDiscrete(2) = betaFitDiscrete(2) * scaleFac;
    if ~isboot
        display(sprintf('beta: %1.4e   alpha: %1.4e', betaFitDiscrete(1), betaFitDiscrete(2)));
    end

    % solves a bug where plotx range extended further than uniqueHrss
    % range, and the fit broke occasionally, for WNBs
    plotx = logspace(log10(uniqueHrss(1)), log10(uniqueHrss(end)), 1000);
    if useErfFit
        yd = erfcModel(betaFitDiscrete, plotx);
    else
        yd =  sigmoidPlusLevelModel(betaFitDiscrete, plotx);
    end

    level = yd(1);
    yscaled = yd - level;
    yscaled = yscaled / (1 - level);

    % running average
    % window for the running average.
    %     windowSize = 20;
    %     runAv = filter(ones(1,windowSize)/windowSize,1,detectVect);
    %     runAvLength = length(runAv);

    errs = sqrt( (uniqueDetect .* (1-uniqueDetect)) / thisNumScaleFactors);
end

% unique gives the last index of repeated series.
% you can get a long string of 1s tailing to the end, so chop it
onesInd = find(yd == 1);
% only keep the first one
onesInd = onesInd(2:end);
ydchop = yd;
ydchop(onesInd) = [];
[yUnique, m, n] = unique(ydchop);
xUnique = plotx(m);
try
    hrss50 = interp1(yUnique, xUnique, 0.5);
    hrss90 = interp1(yUnique, xUnique, 0.9);
catch
    xUnique
    yUnique
    display(sprintf('%s: error: %s', mfilename, lasterr));
end

display(sprintf('value at detection90: %1.3e', hrss90));

if isnan(hrss90)
    xUnique
    yUnique
end

% [yUniqueScaled, m, n] = unique(yscaled);
% xUniqueScaled = plotx(m);
% hrss50Scaled = interp1(yUniqueScaled, xUniqueScaled, 0.5);
% hrss90Scaled = interp1(yUniqueScaled, xUniqueScaled, 0.9);

if isDiscrete & ~isboot
    % estimate error in fit, 90
    warning off;
    if useErfFit
        [ypred,delta] = nlpredci(@erfcModel, hrss90, betaFitDiscrete, rdiscrete, Jdiscrete, 0.1)
    else
        [ypred,delta] = nlpredci(@sigmoidPlusLevelModel, hrss90, betaFitDiscrete, rdiscrete, Jdiscrete, 0.1)
    end
    warning on;

    delta = delta / scaleFac;
    ypredHigh = ypred+delta;
    ypredLow = ypred-delta;

    hrss90High = interp1(yUnique, xUnique, ypredHigh);
    hrss90Low = interp1(yUnique, xUnique, ypredLow);

    xdeltaMean = mean([abs(hrss90High-hrss90), abs(hrss90Low-hrss90)]);
    xdeltaUP = hrss90High-hrss90;

    sigmoidErrPct = pe(hrss90High, hrss90);
    sigmoidErrPctLL = pe(hrss90Low, hrss90);


    % estimate error in fit, 50
    warning off;
    if useErfFit
        [ypred50,delta50] = nlpredci(@erfcModel, hrss50, betaFitDiscrete, rdiscrete, Jdiscrete, 0.1)
    else
        [ypred50,delta50] = nlpredci(@sigmoidPlusLevelModel, hrss50, betaFitDiscrete, rdiscrete, Jdiscrete, 0.1)
    end


    warning on;

    delta50 = delta50 / scaleFac;
    ypredHigh50 = ypred50+delta50;
    ypredLow50 = ypred50-delta50;

    hrss50High = interp1(yUnique, xUnique, ypredHigh50);
    hrss50Low = interp1(yUnique, xUnique, ypredLow50);

    xdeltaMean50 = mean([abs(hrss50High-hrss50), abs(hrss50Low-hrss50)]);
    xdeltaUP50 = hrss50High-hrss50;

    sigmoidErrPct50 = pe(hrss50High, hrss50);
    sigmoidErrPctLL50 = pe(hrss50Low, hrss50);

end


% only plot if it's not a bootstrap run, to save time
if ~isboot && nargin > 4
    %% put on a plot, pct detected vs. hrss
    figure(1); close;
    figure(1); clf;

    warning off;
    if isDiscrete
        errorbar(uniqueHrss, uniqueDetect, errs, '.', 'Color', [1 0.5 0], 'MarkerSize', 30, 'LineWidth', 2);
        errorbarlogx();

        % print graph to standard out
        for xCtr = 1 : length(uniqueHrss)
            display(sprintf('%1.2f  %i/%i  x: %1.1e', uniqueDetect(xCtr), numDetected(xCtr), numTried(xCtr), uniqueHrss(xCtr) ));
        end
        display(sprintf('hrss90: %1.2e\n', hrss90));

        hold on;

        semilogx(plotx(minInd : maxInd), yd(minInd : maxInd), 'r', 'LineWidth', 3);
        %     semilogx(plotx(minInd : maxInd), y(minInd : maxInd), 'c');
    else
        errorbar(uniqueHrss, uniqueDetect, errs, '.', 'Color', [1 0.5 0], 'MarkerSize', 30, 'LineWidth', 2);
        errorbarlogx();

        %     semilogx(hrssVect, detectVect, 'v', 'Markersize', 10, 'LineWidth', 0.3);
        hold on;
        % semilogx(hrssVect, runAv, 'g.');
        semilogx(plotx(minInd : maxInd), yd(minInd : maxInd), 'r', 'LineWidth', 3);
    end
    warning on;

    hold off;
    grid on;
    setfonts(16);
    ylabel('fraction detected');

    if isegw
        legendUnits = 'Msun';
        legendBox = [0.5, 0.22, 0.4, 0.14];
        xlabel('Isotropic Energy [Msun]');
    else
        if useScaleFactor
            legendUnits = 'Mpc';
            legendBox = [0.15, 0.22, 0.4, 0.14];
            xlabel('Mpc');
        else
            legendUnits = 'Hz^{-1/2}';
            legendBox = [0.5, 0.22, 0.4, 0.14];
            xlabel('hrss [Hz^{-1/2}]');
        end
    end

    title(sprintf('Efficiency:  %s', plottitle));

    if isDiscrete
        if useScaleFactor
            annotation('textbox', legendBox, 'String',...
                sprintf('0.9:  %1.2e %s\n0.5:  %1.2e %s', hrss90, legendUnits, hrss50, legendUnits),...
                'FontWeight', 'bold', 'FontSize', 15, 'Backgroundcolor', [1,1,1]);
        else
            %         annotation('textbox', legendBox, 'String',...
            %             sprintf('0.9:  %1.2e + %1.1e %s\n0.5: %1.2e + %1.1e %s', hrss90, xdeltaUP, legendUnits, hrss50, xdeltaUP50, legendUnits),...
            %             'FontWeight', 'bold', 'FontSize', 15, 'Backgroundcolor', [1,1,1]);

            annotation('textbox', legendBox, 'String',...
                sprintf('0.9:  %1.2e %s\n0.5: %1.2e %s', hrss90, legendUnits, hrss50, legendUnits),...
                'FontWeight', 'bold', 'FontSize', 15, 'Backgroundcolor', [1,1,1]);
        end
    else
        annotation('textbox', legendBox, 'String',...
            sprintf('0.9:  %1.2e %s\n0.5: %1.2e %s', hrss90, legendUnits, hrss50, legendUnits),...
            'FontWeight', 'bold', 'FontSize', 15, 'Backgroundcolor', [1,1,1]);
    end

    axis tight;
    saveplot(plotdir, plotname);
    %     saveplot(plotdir, plotname, 'png');
end


%% process outputs
switch nargout,
    case 1,
        varargout = {};
    case 2,
        varargout = {hrss50};
    case 3,
        varargout = {hrss50, sigmoidErrPct};
    case 4
        varargout = {hrss50, sigmoidErrPct, minGf}
    case 5,
        varargout = {hrss50, minGf, plotx(minInd : maxInd), yd(minInd : maxInd) };
end

end

%%%
% members
%%%

%% LEVEL
function y = erfcModel(beta, x)
level = beta(5);
y = [];
for i = 1 : length(x)
    a = log10(x(i))-beta(1);

    if beta(2)==0
        display('beta(2)==0');
        y = -1;
        return;
    end

    % left part of curve
    if a < 0
        s = beta(2)*exp(a*beta(3));
        if s == 0
            %s==0 a<0 beta(2)=1.281e-01 a=-1.452e+00 beta(3)=5.151e+02
            % so the problem is beta(3) is too large.
            %             display(sprintf('s==0 a<0 beta(2)=%1.3e a=%1.3e beta(3)=%1.3e', beta(2), a, beta(3) ));
            y=-1;
            return;
        end
        y = [y level+erfc(abs(a./s))/(1/(0.5-level))];
        % right part of curve
    else
        s = beta(2)*exp(a*beta(4));
        if s == 0
            %             display('s==0 a>0');
            y=-1;
            return;
        end
        y = [y 1-erfc(abs(a./s))/2];
    end
end
end



function y = erfcModelNoLevel(beta, x)
y = [];
for i = 1 : length(x)
    a = log10(x(i))-beta(1);

    if beta(2)==0
        display('beta(2)==0');
        y = -1;
        return;
    end

    if a < 0
        s = beta(2)*exp(a*beta(3));
        if s == 0
            display('s==0 a<0');
            y=-1;
            return;
        end
        y = [y erfc(abs(a./s))/2];
    else
        s = beta(2)*exp(a*beta(4));
        if s == 0
            display('s==0 a>0');
            y=-1;
            return;
        end
        y = [y 1-erfc(abs(a./s))/2];
    end
end
end


function y = sigmoidPlusLevelModel(beta, x)
%P(t) = \frac{1}{1 + e^{-t}}
shift = beta(1);
scale = beta(2);
level = beta(3);
sizeVect = size(x);
y = level + ones(sizeVect(1), sizeVect(2)) ./ ( 1 + exp(-scale * (x-shift)) + (1/(1-level) -1) );
end

function y = sigmoidPlusHighLevelModel(beta, x)
% ---- This function fits sigmoids to efficiency curves which do not
%      tend to 1 at high amplitude scales, e.g., due to veto deadtime.
%P(t) = \frac{1-level}{1+e^{-scale *(t-shift)}}
shift = beta(1);
scale = beta(2);
level = beta(3);
sizeVect = size(x);
y = (ones(sizeVect(1), sizeVect(2)) - level) ./ (1 + exp(-scale * (x - shift)));
end
