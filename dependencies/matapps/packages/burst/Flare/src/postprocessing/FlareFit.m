function [fit90, fit50, ci, varargout] = ...
    FlareFit(thisWaveform, hrssVect, detectVect, fitoptions, plottitle, plotdir, plotname)
% 'FlareFit' Choose from among specialized fitting functions.
%
% Take in a vector of detected flags (ones or zeros) and corresponding
% hrss injected values and produce a sigmoid curve.
%
%
% INTERFACE
%  thisWaveform     -> string representing the simulation set to fit
%                      e.g. SGC150Q8d9, WNB100ms100to10000Hz
%  hrssVect         -> input vector of N hrss (or energy) values to plot
%  detectVect       -> input vector of N binary detect/did not detect
%  fitoptions       -> structure of options used by the fit routine, with following optional fields
%                      nboot - integer - perform booststrap with N ensembles
%                        (bootstrap not performed if N==0)
%                      useScaleFactor - bool - set to true for inspiral searches to have x-axis in Mpc
%                      forceIsDiscrete - if true, fit will force discrete fitting
%                      isegw - if true, scales fit params by 1e7 instead of 1e21
%                      numDiscrete - if set, group the data into this many discrete bins.  use for WNB egw fits.                      
%  plottitle        -> title string for plot
%  plotdir          -> dir in which to save plot (optional, omit->not save)
%  plotname         -> name for the plot file (optional, omit->not save)
%---------------------------------------------------------------------
%  fit50            <- sensitivity/upper limit statistic at 50%
%  fit90            <- sensitivity/upper limit statistic at 90%
%  ci               <- 90% confidence interval, if boostrapping is turned on
%  minGF            <- [optional] best goodness-of-fit measure for the fit
%  x                <- [optional] x values for a plot
%  y                <- [optional] y values for a plot

% Peter Kalmus
% peter.kalmus@ligo.org
% $LastChangedDate: 2008-03-22 11:23:44 -0400 (Sat, 22 Mar 2008) $
% $Rev: 4887 $ 

nboot = 0;
try
    nboot = fitoptions.nboot;
catch
end

% for these waveforms, use logistics function instead of ERF function
if ~isempty(findstr(thisWaveform, 'RDC')) || ~isempty(findstr(thisWaveform, 'SGC')) || ~isempty(findstr(thisWaveform, '100to1000Hz'))
    fitoptions.useErfFit = false;
end

if nargin == 5
    [fit90, fit50, minGF, x, y] = sigmoidLevelErfFit(hrssVect, detectVect, fitoptions, plottitle);
else
    [fit90, fit50, minGF, x, y] = sigmoidLevelErfFit(hrssVect, detectVect, fitoptions, plottitle,...
        plotdir, plotname);
end

ci = [fit90, fit90];
if nboot > 0
    fitoptions.isboot=true; % this is for optimizing, e.g. not saving plots
    if isrow(hrssVect)
        hrssVect = hrssVect';
    end
    if isrow(detectVect)
        detectVect = detectVect';
    end    
    ci = bootci(nboot,{@sigmoidLevelErfFit, hrssVect, detectVect, fitoptions},'alpha',0.1,'type', 'per')
end

%% process outputs
switch nargout,
    case 4
        varargout = {minGF};
    case 5
        varargout = {minGF, sigmoidErrPct};
    case 6
        varargout = {minGF, x, y};
end
    
