function saveplot(dir, file, varargin)
% saveplot Save a figure.
%
% saveplot(dir, file) Save the plot as psc2 with .ps extension.
%
% saveplot(dir, file, format) Save the plot as specified format, where format can be
% (for example) 'png', 'jpg', etc.  
%
% dir    - string representing path to file
% file   - string w/ file name (excluding extension)
% format - image file format to use
%
% see saveas

% Peter Kalmus
% peter.kalmus@ligo.org
% $LastChangedDate: 2008-03-05 11:53:14 -0500 (Wed, 05 Mar 2008) $
% $Rev: 4750 $ 

if ~strcmp(dir(end), filesep)
    dir = [dir filesep];
end

fileNameStr = [dir file];

display(sprintf('saveplot:  %s', fileNameStr));

if length(varargin) == 0
    saveas(gcf, fileNameStr, 'psc2');
    % other options, for example...
    %saveas(gcf, fileNameStr, 'jpg');
    %saveas(gcf, fileNameStr, 'png');    
else
    for i = 1 : length(varargin)
       saveas(gcf, fileNameStr, varargin{i}); 
    end
end
