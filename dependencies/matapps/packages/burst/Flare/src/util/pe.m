function percentError =  pe(val1, val2)
% pe percent error defined as (val2-val1)/val2.
%
% val1 and val2 can be vectors
%

% Peter Kalmus
% peter.kalmus@ligo.org
% $LastChangedDate: 2008-03-05 11:53:14 -0500 (Wed, 05 Mar 2008) $
% $Rev: 4750 $ 

if iscol(val1)
    val1 = val1';
end
if iscol(val2)
    val2 = val2';
end

percentError = (val1 - val2) ./ val2;
