function [commentStyle,commentChar] = getcommentstyle(fileName)

%GETCOMMENTSTYLE- get style of comment characters in file
%
%SYNOPSIS
%   [COMMENTSTYLE,COMMENTCHAR] = getcommentstyle(FILENAME)
%
%INPUT
%   FILENAME - datafile
%
%OUTPUTS
%   COMMENTSTYLE - apparent comment character style in data file
%       'shell' - '#' was used
%       'matlab' - '% was used
%       'c'   -  '/*' was used
%       'c++' - '//' was used
%   COMMENTCHAR - apparent comment character (#,%,/*,//)
%
%AUTHOR
%   Keith Thorne <keith.thorne@ligo.org>

%  $Id: getcommentstyle.m 6645 2010-12-09 16:09:14Z diego.menendez@LIGO.ORG $

% DEFINE comment styles and characters
% READ first line of file
% LOOP over comment styles
%   FIND position of comment character in first line
%   IF not found
%       SET postion to default (large, positive)
%   ENDIF
% ENDLOOP
% IF no valid comment characters  found
%   SET style to default
% ELSE
%   FIND which comment type occurs first
%   SET style to match first comment character
% ENDIF
% CLOSE file
DEF_STYLE = 'matlab';
DEF_CHAR = '%';
numStyle = 4;
styleList = cell(numStyle);
styleList{1} = 'shell';
styleList{2} = 'matlab';
styleList{3} = 'c';
styleList{4} = 'cc';
comChar = cell(numStyle);
comChar{1} = '#';
comChar{2} = '%';
comChar{3} = '/*';
comChar{4} = '//';
DEF_POS = 9999999;
posVal = ones(numStyle) * DEF_POS;
if(exist(fileName,'file') == false)
    commentStyle = DEF_STYLE;
    return
end
testData = textread(fileName,'%s',1);
testData = char(testData);
for k=1:numStyle
   comTest = char(comChar{k});
   posTest = strfind(testData,comTest);
   if(isempty(posTest))
       posVal(k) = DEF_POS;
   else
       posVal(k) = posTest(1);
   end
end
[minPos,minIdx] = min(posVal);
if(minPos >= DEF_POS)
    commentStyle = DEF_STYLE;
    commentChar = DEF_CHAR;
    return
else
    commentStyle = char(styleList{minIdx(1)});
    commentChar = char(comChar{minIdx(1)});
end
return
