#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <kippmath.h>
#include <fftw3.h>
#include <correlator.h>
#include <instrument.h>
#include <sh_series.h>
#include <inject.h>
#include <instruments.h>

#include <lal/Date.h>
#include <lal/DetResponse.h>
#include <lal/LALDatatypes.h>
#include <lal/LALSimulation.h>
#include <lal/LALError.h>
#include <lal/XLALError.h>

/* Matlab specific includes */
#include "mex.h"
#include "mat.h"

/* Debugging specific includes */
/* #include "memwatch.h" */

/* SH kit specific includes */
#include "SHtransforms.h"

/* SphRad specific includes */
#include "sphrad.h"
#include "sphrad_misc.h"
#include "sphrad_clust.h"
#include "sphrad_response.h"
#include "sphrad_skymap.h"
#include "sphrad_stat.h"
#include "sphrad_ext.h"
#include "sphrad_coherent.h"

#include <gsl/gsl_const.h>

/* Structure initialisation: define pointers to our data persistant products */
static struct correlator_network_baselines *baselines = NULL;
static struct correlator_network_plan_fd *fdplans = NULL;
struct instrument **instruments = NULL;

/* Declare subroutines */
struct sh_series_array *calculate_correlation(struct correlator_network_plan_fd *fdplans, struct detector_response_grid *antResponses, struct analysisData *inData, int *timeShifts);
struct analysisData *get_inputs(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[], int n_instr);
void print_analysisData(struct analysisData *new);
void analysisData_free(struct analysisData *aData);
static void myExitFcn();


/* MATLAB gateway routine */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
        mexEvalString("drawnow;"); /* Make sure that all text has been written out */

        long startAnalysis, stopAnalysis;
        startAnalysis = clock();

        /* Want info? */
        if (nrhs == 0)
        {
                mexPrintf("sphrad can be called with the following parameters:\n");
                mexPrintf("\tanalysisOut = sphrad(nIstr, data, analysisStruct, DetList, antennaRepose, analysisType)\n");
                mexPrintf("\t              analysisStruct has the following fields:\n");
                mexPrintf("\t                        .FFTlen (in s)\n");
                mexPrintf("\t                        .deltaT (in s)\n");
                mexPrintf("\t                        .blockStartTime (in s)\n");
                mexPrintf("\t                        .blockStopTime (in s)\n");
                mexPrintf("\t                        .blockLen (in s)\n");
                mexPrintf("\t                        .transientLen (in s)\n");
                mexPrintf("\t                        .outputDirectory\n");

                return;
        }

        int idx;           /* Use for local loops */
        int i, j, k, n, m; /* Use for iterarative loops */
        int new_test = 0;
        mwSize n_instr; /* Number of data streams */

        /* Get number of data stream */
        n_instr = mxGetScalar(prhs[0]);
        mexPrintf("%i instruments passed in\n", n_instr);

        /* Check if exit function registered */
        if (mexAtExit(myExitFcn))
                mexPrintf("Error, myExitFcn not registered properly");

        /* setup in variables */
        double multiplier;      /* input scalar */
        double maxPower = 0.0;  /* maximum power in all segments */
        double tempPower = 0.0; /* temporary variable to hold running power */
        int segmentIdx = 0;     /* segment with largest power */

        /* out - these really need a more descriptive name! */
        double *outMatrix = NULL;  /* output matrix */
        double *outMatrixi = NULL; /* output matrix */

        double *outValue; /* To stop segfaults */

        /* Timing variables */
        long startF, stopF;

        /* Structure to hold any returned array from correlation */
        /* This could be a time-frequency map or a time-harmonic map */
        struct sh_series_array *correlationResult = NULL;

        /* Structure to hold the antenna responses of our network */
        /* SH -> struct detector_response *allResponses = NULL; */
        /* Grid */
        struct detector_response_grid *allResponses = NULL;

        /* Initilise data structure to hold data structure */
        struct analysisData *aData;

        mexPrintf("Loading parameters\n");
        aData = get_inputs(nlhs, plhs, nrhs, prhs, n_instr);

        mexPrintf("Number of data streams: %i\n", aData->nDataStreams);
        for (i = 0; i < aData->nDataStreams; i++)
        {
                mexPrintf("\tStream %i\t", i);
                mexPrintf("\t\tName: %s\t", aData->detName[i]);
                mexPrintf("\t\tLen: %i\n", aData->lenDataStreams);
                mexPrintf("\n");
        }

        mexPrintf("\tAnalysis type: %s\n", aData->analysisType);
        mexPrintf("\tSample frequency: %d\n", aData->sampleFrequency);
        mexPrintf("\tFreq range: [%i, %i]\n", aData->freqRange[0], aData->freqRange[1]);

        mexPrintf("\nTime series length: \t%d\nFourier length : \t%f seconds\nData spacing: \t%f\n Data length: \t%i samples\nTime series length: \t%i\n\n", aData->lenDataStreams, aData->fourierLen, aData->deltaT, aData->lenDataStreams, aData->FFTLen);

        /* Setup detector information */
        double gmst = 0;
        const LALDetector *detectors[aData->nInstr];

        /* Check order - if different, regenerate all internal data products */
        if (fdplans || baselines || instruments)
                mexPrintf("NOTE: Using previously generated data products\n");
        else
                mexPrintf("NOTE: Generating data products\n");

        if (!instruments)
        {
                mexPrintf("\tGenerating instruments\n");
                instruments = (struct instrument **)malloc(aData->nInstr * sizeof(struct instrument *));
                /* Set instruments up */
                /* for (idx=0;idx<aData->nInstr;idx++)
                           instruments[idx] = instrument_from_name(aData->detName[idx]); */

                /* THIS IS FOR TWO and THREE DETECTORS */
                /* hack to avoid calling instrument_from_name from instruments.c, that in turn calls XLALDetectorPrefixToLALDetector */
                /* (see below for detectors[i] as well) */
                /* hack to bypass calling XLALDetectorPrefixToLALDetector that DOES NOT work */
                /* points to the correct indices inside lalCachedDetectors: idx 4-> H1, idx 5-> L1, idx 1-> V1 */
                /* see https://www.lsc-group.phys.uwm.edu/daswg/projects/lal/nightly/docs/html/_l_a_l_detectors_8h_source.html#l00167 for referencing indesces */
                /* documentation of XLALDetectorPrefixToLALDetector is here */
                /* https://www.lsc-group.phys.uwm.edu/daswg/projects/lal/nightly/docs/html/_l_a_l_simulation_8c_source.html#l00084 */
                /* HLV */
                if (aData->nInstr == 3)
                {
                        mexPrintf("\nThree instruments passed in, assuming HLV\n");
                        detectors[0] = &lalCachedDetectors[4];
                        instruments[0] = instrument_new(
                            detectors[0]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                            detectors[0]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                            detectors[0]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                        detectors[1] = &lalCachedDetectors[5];
                        instruments[1] = instrument_new(
                            detectors[1]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                            detectors[1]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                            detectors[1]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                        detectors[2] = &lalCachedDetectors[1];
                        instruments[2] = instrument_new(
                            detectors[2]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                            detectors[2]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                            detectors[2]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                }
                else
                {
                        /* HL */
                        if (strcmp(aData->detName[0], "H1") == 0 && strcmp(aData->detName[1], "L1") == 0)
                        {
                                mexPrintf("\nUsing H1 and L1 analysis\n");
                                detectors[0] = &lalCachedDetectors[4];
                                instruments[0] = instrument_new(
                                    detectors[0]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[0]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[0]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                                detectors[1] = &lalCachedDetectors[5];
                                instruments[1] = instrument_new(
                                    detectors[1]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[1]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[1]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                        }
                        /* HV */
                        else if (strcmp(aData->detName[0], "H1") == 0 && strcmp(aData->detName[1], "V1") == 0)
                        {
                                mexPrintf("\nUsing H1 and V1 analysis\n");
                                detectors[0] = &lalCachedDetectors[4];
                                instruments[0] = instrument_new(
                                    detectors[0]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[0]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[0]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                                detectors[1] = &lalCachedDetectors[1];
                                instruments[1] = instrument_new(
                                    detectors[1]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[1]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[1]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                        }
                        /* LV */
                        else if (strcmp(aData->detName[0], "L1") == 0 && strcmp(aData->detName[1], "V1") == 0)
                        {
                                mexPrintf("\nUsing L1 and V1 analysis\n");
                                detectors[0] = &lalCachedDetectors[5];
                                instruments[0] = instrument_new(
                                    detectors[0]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[0]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[0]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                                detectors[1] = &lalCachedDetectors[1];
                                instruments[1] = instrument_new(
                                    detectors[1]->location[0] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[1]->location[1] / GSL_CONST_MKS_SPEED_OF_LIGHT,
                                    detectors[1]->location[2] / GSL_CONST_MKS_SPEED_OF_LIGHT);
                        }
                }
        }
        else
                mexPrintf("\tUsing stored instruments\n");

        /* Get detector information */
        /* for (idx=0;idx<aData->nInstr;idx++)
                        detectors[idx] = XLALDetectorPrefixToLALDetector(aData->detName[idx]); */

        /* hack to bypass calling XLALDetectorPrefixToLALDetector that DOES NOT work */
        /* points to the correct indices inside lalCachedDetectors: idx 4-> H1, idx 5-> L1, idx 1-> V1 */
        /* see https://www.lsc-group.phys.uwm.edu/daswg/projects/lal/nightly/docs/html/_l_a_l_detectors_8h_source.html#l00167 for referencing indesces */
        /* documentation of XLALDetectorPrefixToLALDetector is here */
        /* https://www.lsc-group.phys.uwm.edu/daswg/projects/lal/nightly/docs/html/_l_a_l_simulation_8c_source.html#l00084 */
        /* HLV */
        if (aData->nInstr == 3)
        /* HLV */
        {
                detectors[0] = &lalCachedDetectors[4];
                detectors[1] = &lalCachedDetectors[5];
                detectors[2] = &lalCachedDetectors[1];
        }
        else
        {
                /* HL */
                if (strcmp(aData->detName[0], "H1") == 0 && strcmp(aData->detName[1], "L1") == 0)
                {
                        detectors[0] = &lalCachedDetectors[4];
                        detectors[1] = &lalCachedDetectors[5];
                }
                /* HV */
                else if (strcmp(aData->detName[0], "H1") == 0 && strcmp(aData->detName[1], "V1") == 0)
                {
                        detectors[0] = &lalCachedDetectors[4];
                        detectors[1] = &lalCachedDetectors[1];
                }
                /* LV */
                else if (strcmp(aData->detName[0], "L1") == 0 && strcmp(aData->detName[1], "V1") == 0)
                {
                        detectors[0] = &lalCachedDetectors[5];
                        detectors[1] = &lalCachedDetectors[1];
                }
        }
        /*
                * Report the Lat/Long of the detectors.
                *  Latitude: North pole(90) < Equator(0) < South pole(-90)
                *  Longitude: -180 < Prime Meridian (0) < 180
                * However, reported in radians
                */

        for (idx = 0; idx < aData->nInstr; idx++)
        {
                mexPrintf("\t%s detector location:\n", aData->detName[idx]);
                mexPrintf("\t  Lat:  %f\n", detectors[idx]->frDetector.vertexLatitudeRadians);
                mexPrintf("\t  Long: %f\n", detectors[idx]->frDetector.vertexLongitudeRadians);
        }

        /* Generate detector baselines for all pairs - including auto */
        if (!baselines)
        {
                /* Construct baselines */
                mexPrintf("\tConstructing baselines (fingers crossed)\n");
                baselines = correlator_network_allbaselines_new((const struct instrument *const *)instruments, aData->nInstr);
        }
        else
                mexPrintf("\tUsing stored baselines\n");

        /* Generate multi-resolution time-frequency map */
        if (strcmp(aData->analysisType, "multi_tfmap") == 0)
        {
                /* Let's test out new tf algorithm */
                int nScales = 4;
                int lenScales[nScales];
                lenScales[0] = aData->sampleFrequency;
                lenScales[1] = lenScales[0] / 2;
                lenScales[2] = lenScales[1] / 2;
                lenScales[3] = lenScales[2] / 2;

                multiTF_analysis(aData, lenScales, nScales, baselines);
                multiTF_constant_analysis(aData, lenScales, nScales, baselines);

                return;
        }

        /* Make this alterable, so we can look at specific frequencies? */
        int fBins = /*aData->FFTLen*/ (aData->lenDataStreams * aData->deltaT) / (aData->fourierLen / 2);
        fBins = aData->FFTLen;

        /* Normal fd plan */
        if (!fdplans)
        {
                /* Set clock going to time radiometer product generation */
                startF = clock();

                if (aData->eOrder[0] > 0)
                        /* If defined, generate plans using user defined spherical harmonic orders */
                        fdplans = /*full_*/ user_correlator_network_plan_fd_new(baselines, fBins, aData->deltaT, aData->eOrder);
                else if (aData->eOrder[0] < 0)
                        /* If not defined, generate plans using optimum values defined by time step and frequency */
                        fdplans = /*full_best_*/ correlator_network_plan_fd_new(baselines, fBins, aData->deltaT);
                else
                        /* Non azimuthally symmetric plan */
                        fdplans = /*mod_*/ correlator_network_plan_fd_new(baselines, fBins, aData->deltaT);

                stopF = clock();
                mexPrintf("*********************** Creating delay matrix with order [%d, %d, %d, %d] took: %f seconds ******************************\n", aData->eOrder[0], aData->eOrder[1], aData->eOrder[2], aData->eOrder[3], ((double)(stopF - startF)) / CLOCKS_PER_SEC);
        }
        else
        {
                mexPrintf("\tUsing stored plans\n");
        }

        if (!fdplans || !fdplans->plans)
        {
                mexPrintf("corr_net failed\n");
                return;
        }

        /* Create skymap with appropriate l value
                 * l value of sky due to baseline length and sample frequency or via user defined
                */
        if (aData->eOrder[0] > 0)
                aData->sky_l_max = aData->eOrder[3];
        else
                aData->sky_l_max = correlator_network_l_max(baselines, aData->deltaT);

        mexPrintf("l_max for correlator network: %i\n\n", aData->sky_l_max);

        /* Generate our antenna responses */
        mexPrintf("Generating antenna responses for network\n");

        /* Gridded version */
        allResponses = detector_response_grid_new(detectors, aData->nInstr, aData->sky_l_max);

        /* Dump out antenna responses */
        if (aData->debugLevel > 1)
        {
                product_output("response.mat", "Hp", allResponses->polarisations[0].plus, allResponses->gridSize, allResponses->gridSize, 1);
                product_output("response.mat", "Hx", allResponses->polarisations[0].cross, allResponses->gridSize, allResponses->gridSize, 1);

                product_output("response.mat", "Lp", allResponses->polarisations[1].plus, allResponses->gridSize, allResponses->gridSize, 1);
                product_output("response.mat", "Lx", allResponses->polarisations[1].cross, allResponses->gridSize, allResponses->gridSize, 1);

                product_output("response.mat", "Vp", allResponses->polarisations[2].plus, allResponses->gridSize, allResponses->gridSize, 1);
                product_output("response.mat", "Vx", allResponses->polarisations[2].cross, allResponses->gridSize, allResponses->gridSize, 1);
        }

        /* Integrate */
        mexPrintf("*** Starting integration ***\n");

        mexPrintf("Number of sections: %d\n", aData->nSections);
        int dataLength, timeBlock;
        int shLength;
        int columnLen;
        int arrayIdx;

        /* Define ouput lengths */
        shLength = sh_series_length(aData->sky_l_max, 0);
        dataLength = aData->nSections; /*(aData->lenDataStreams/(aData->FFTLen/2))-1;
                                                     */

        mexPrintf("%s: %d\n", "dataLength segments", dataLength);
        mexPrintf("%s: %d\n", "sky shLength", shLength);

        timeBlock = 0;
        columnLen = shLength /*sh_series_length(sky->l_max, sky->polar)*/;

        if ((aData->FFTLen * (aData->lenDataStreams / aData->FFTLen) - 1) > aData->lenDataStreams)
        {
                mexPrintf("integration number too high\n");
                return;
        }

        /* test to see what is happening with timeSHifts */
        /* Do internal time lags */
        int *timeShifts;
        if (aData->timeLags)
        {
                timeShifts = (int *)malloc(aData->nTimeLags * sizeof(int));
                for (i = 0; i < aData->nTimeLags; i++)
                        timeShifts[i] = (int)aData->timeLags[i];
        }
        else
        {
                timeShifts = (int *)malloc(sizeof(int));
                timeShifts[0] = 0;
        }

        /* Print out everythrng to do with the analysis */
        print_analysisData(aData);

        /* How long to calculate  */
        startF = clock();
        correlationResult = calculate_correlation(fdplans, allResponses, aData, timeShifts);
        stopF = clock();

        mexPrintf("*************** FUNCTION CALL TOOK: %f seconds ********************\n", ((double)(stopF - startF)) / CLOCKS_PER_SEC);

        free(timeShifts);

        if ((correlationResult == NULL) && (strcmp(aData->analysisType, "full") != 0))
                return;

        /* If not 'full' type analysis, we need to output something back to Matlab */
        if (strcmp(aData->analysisType, "full") != 0)
        {
                /* If running a full analysis, data is dumped out to mat files, so no need to return data */
                shLength = sh_series_length(correlationResult->l_max, correlationResult->polar);

                mexPrintf("\tMap size: (%d, %d)\n", shLength, correlationResult->n);

                plhs[0] = mxCreateDoubleMatrix(shLength, correlationResult->n, mxCOMPLEX);
                outMatrix = mxGetPr(plhs[0]);
                outMatrixi = mxGetPi(plhs[0]);

                /* Assign SH coefficients to output matrix */
                for (i = 0; i < correlationResult->n; i++)
                {
                        for (k = 0; k < shLength; k++)
                        {
                                arrayIdx = i * shLength + k;
                                if (arrayIdx > correlationResult->n * shLength)
                                {
                                        fprintf(stderr, "Error, out of bounds in memory array\n");
                                        return;
                                }
                                outMatrix[arrayIdx] = creal(correlationResult->series[i].coeff[k]);
                                outMatrixi[arrayIdx] = cimag(correlationResult->series[i].coeff[k]);
                        }
                }
        } /* End of ignore if FULL */
        else
        {
                /* Have carried out a full analysis, so nothing to actually return
                        * so check for return variable, and assign accordingly
                        */
                mexPrintf("\t %d return(s) requested\n", nlhs);
                for (idx = 0; idx < nlhs; idx++)
                {
                        mexPrintf("\tReturning 2.0 for parameter %d\n", idx);
                        plhs[idx] = mxCreateDoubleMatrix(1, 1, mxREAL);
                        outValue = mxGetPr(plhs[idx]);
                        outValue[0] = 2.0;
                }
        }

        /* Free data */
        mexPrintf("Freeing main() data\n");
        /*
                for(idx = 0; idx < aData->nInstr ; idx++)
                instrument_free(instruments[idx]);
                */

        if (strcmp(aData->analysisType, "full") != 0)
                sh_series_array_free(correlationResult);

        detector_response_grid_free(allResponses);
        analysisData_free(aData);

        stopAnalysis = clock();
        mexPrintf("***********************************************\n");
        mexPrintf("*    segment analysis took %f seconds          *\n", ((double)(stopAnalysis - startAnalysis)) / CLOCKS_PER_SEC);
        mexPrintf("***********************************************\n");
        mexPrintf("*       segment analysis finished             *\n");
        mexPrintf("***********************************************\n");
}

/* Function to perform the correlations.
* Sets up all of the data structures, then, depending on inData->analysisType, calculates the cross power:
* analysisType can be any of the following:
*
*	normal		- does cross-power, returns tsmap for each detector
*	restricted	- as above, but a frequency range is passed in to restrict the power calculation
*	fast_tfmap	- fast and dirty time-frequency map generation, returns tfmap based on 1d-spherical harmonic power
*	tfmap		- full time-frequency map, uses 2d-power
*	tfsmap		- generates a (very large) time-harmonic-frequency array
*	tfshmap		- generates the time-harmonic-frequency array
*	full		- does cross, auto and response calculations for specified time shifts
*/
struct sh_series_array *calculate_correlation(struct correlator_network_plan_fd *fdplans, struct detector_response_grid *antResponses, struct analysisData *inData, int *timeShifts)
{
        mexEvalString("drawnow;"); /* Make sure all text has been written out */

        long startAnalysis, stopAnalysis, cumulativeAnalysis = 0;

        startAnalysis = clock();

        /* Function to perform the correlations.
        * Sets up all of the data structures, then, depending on inData->analysisType, calculates the cross power:
        * analysisType can be any of the following:
        *
        *	normal		- does cross-power, returns tsmap for each detector
        *	restricted	- as above, but a frequency range is passed in to restrict the power calculation
        *	fast_tfmap	- fast and dirty time-frequency map generation, returns tfmap based on 1d-spherical harmonic power
        *	tfmap		- full time-frequency map, uses 2d-power
        *	tfsmap		- generates a (very large) time-harmonic-frequency array
        *	tfshmap		- generates the time-harmonic-frequency array
        *	full		- does cross, auto and response calculations for specified time shifts
        */

        struct sh_series_array *tf_map = NULL; /* time-frequency map */
        struct sh_series *tfSlice = NULL;      /* tf power */

        struct sh_series_array *ts_map = NULL; /* time-SH coefficient map */
        struct sh_series *sky = NULL;          /* 2d sky power */

        struct sh_series_array *each_ts_map[fdplans->baselines->n_baselines]; /* each detector pair */
        struct sh_series_array *eachSky = NULL;                               /* 2d sky power */

        struct sh_series *eachDetSky[fdplans->baselines->n_baselines]; /* Harmonic expansion for each detector pair */

        double *endSeries[inData->nInstr - 1]; /* Array to hold wrapped around data*/

        /* Cluster pixels on tfmap */
        double *tfMap = NULL;
        double *tfMap_copy = NULL;
        int *clusterMap = NULL;
        double *clusterStruct = NULL;
        double *sortedArray = NULL;
        double *sortedArray_copy = NULL;
        int numClusters = 0;
        int followUp = 2;
        double threshold[2];

        int timeBlock = 0;
        double tempPower = 0;
        double maxPower = 0;
        int segmentIdx = 0;
        int arrayIdx = 0;
        int shiftIdx = 0;
        int coeffIdx = 0;
        int clusterIdx = 0;
        int detIdx = 0;
        int nShifts = 0;
        int shLength;
        int *timeLagIdx;
        int dectIdx[inData->nInstr];
        int baseDectIdx[inData->nInstr];
        int restrictedIdx[2] = {0, inData->lenDataStreams};

        /* Output string variables */
        FILE *outFile;
        FILE *skyposOut;
        char *shiftPath;
        char *matFileName;
        char *skyFileName;
        char *txtFileName;
        char *restrictedName;

        /* Timeshift variables */
        char *baseName;
        complex double *skyGrid;

        /* FSH Transformation variables */
        int bw, size;

        bw = inData->sky_l_max + 1;
        size = bw * 2;

        /* Allocate memory */
        skyGrid = (complex double *)malloc(sizeof(complex double) * (size * size /*inData->sky_l_max * inData->sky_l_max*4*/));

        /* General looping variables */
        int i, j, k, idx, blockIdx, instrIdx;

        mexPrintf("Number of sections: %d\n", inData->nSections);

        /* Check string input and matching to make sure it is working as intended */
        mexPrintf("Carrying out %s analysis\n", inData->analysisType);

        /* Testing*/
        struct sh_series_array *tfs_map[inData->nSections]; /* time-frequency-SH map */

        /* Determine analysis type and initialise the appropriate variables */
        if ((strcmp(inData->analysisType, "normal") == 0) || (strcmp(inData->analysisType, "restricted") == 0))
        {
                mexPrintf("\tMatched with %s\n", inData->analysisType);

                /* Create structure to hold array of SH coefficients */
                ts_map = sh_series_array_new(inData->nSections, inData->sky_l_max, 0);

                /* Create skymap with appropriate l value
                 * l value of sky due to baseline length and sample frequency
                 */
                sky = sh_series_new_zero(inData->sky_l_max, 0);
                eachSky = sh_series_array_new(fdplans->baselines->n_baselines, inData->sky_l_max, 0);
        }
        else if (strcmp(inData->analysisType, "fast_tfmap") == 0)
        {
                mexPrintf("\tMatched with fast_tfmap\n");
                mexPrintf("\t - computing tfmap from 1d power\n");

                /* Create array to hold SH coefficient power by frequency*/
                ts_map = sh_series_array_new(inData->nSections, fdplans->plans[0]->delay_product->n - 1, 1);
                /* Create 'sky' */
                sky = sh_series_new_zero(ts_map->l_max, 1);
        }
        else if (strcmp(inData->analysisType, "tfmap") == 0)
        {
                mexPrintf("\tMatched with tfmap\n");

                /* Create array to hold the spherical harmonics decomposed by frequency
                   tf_map = sh_series_array_new(fdplans->plans[0]->delay_product->n, fdplans->plans[0]->power_2d->l_max, fdplans->plans[0]->power_2d->polar);
                 */

                /* Create array to hold SH coefficient power by frequency*/
                ts_map = sh_series_array_new(inData->nSections, fdplans->plans[0]->delay_product->n - 1, 1);

                /* Create skymap with appropriate l value
                 * l value of sky due to baseline length and sample frequency
                 */
                sky = sh_series_new_zero(ts_map->l_max, 1);
        }
        else if (strcmp(inData->analysisType, "fsmap") == 0)
        {
                /* Generate harmonic-frequency map for a single time bin */
                mexPrintf("\tMatched with fsmap\n");
                sky = sh_series_new_zero(fdplans->plans[0]->power_2d->l_max, 1);

                /* Create array to hold SH coefficient expansion for each frequency bin */
                ts_map = sh_series_array_new(fdplans->plans[0]->delay_product->n, fdplans->plans[0]->power_2d->l_max, fdplans->plans[0]->power_2d->polar);
                /*
                for (i=0;i<inData->nSections;i++)
                tfs_map[i] = sh_series_array_new(fdplans->plans[0]->delay_product->n, fdplans->plans[0]->power_2d->l_max, fdplans->plans[0]->power_2d->polar);
                if (tfs_map == NULL)
                        mexErrMsgIdAndTxt("SphRadiometer:allocate:noMem",
                                        "Failed to build the monster array");
                */
        }
        else if (strcmp(inData->analysisType, "tfsmap") == 0)
        {
                mexPrintf("\tMatched with tfsmap\n");
                sky = sh_series_new_zero(fdplans->plans[0]->power_2d->l_max, 1);

                /* Create array to hold SH coefficient power by frequency*/
                ts_map = sh_series_array_new(fdplans->plans[0]->delay_product->n, fdplans->plans[0]->power_2d->l_max, 1);

                for (i = 0; i < inData->nSections; i++)
                        tfs_map[i] = sh_series_array_new(fdplans->plans[0]->delay_product->n, fdplans->plans[0]->power_2d->l_max, fdplans->plans[0]->power_2d->polar);
                if (tfs_map == NULL)
                        mexErrMsgIdAndTxt("SphRadiometer:allocate:noMem",
                                          "Failed to build the monster array");
        }
        else if (strcmp(inData->analysisType, "tfshmap") == 0)
        {
                mexPrintf("\tMatched with time-freq-shmap\n");
                mexPrintf("\t  freq bin: [%d]\n", fdplans->plans[0]->delay_product->n);
                mexPrintf("\t  coeffs:   [%d]\n", fdplans->plans[0]->power_2d->l_max);

                mexPrintf("\t  network_l:[%d]\n", inData->sky_l_max);
                mexPrintf("\t  network:  [%d]\n", sh_series_length(inData->sky_l_max, 0));

                /* Create array to hold SH coefficient power by frequency*/
                /*  ts_map = sh_series_array_new(fdplans->plans[0]->delay_product->n, fdplans->plans[0]->power_2d->l_max, 1);
                sky = sh_series_new_zero(fdplans->plans[0]->power_2d->l_max, 1 );
                */
                ts_map = sh_series_array_new(fdplans->plans[0]->delay_product->n, inData->sky_l_max, 1);
                sky = sh_series_new_zero(inData->sky_l_max, 1);

                if ((ts_map == NULL) || (sky == NULL))
                        mexErrMsgIdAndTxt("SphRadiometer:allocate:noMem",
                                          "Failed to build the monster array");
        }
        else if (strcmp(inData->analysisType, "full") == 0)
        {
                mexPrintf("\tMatched with full\n");

                /* Set up variables to hold all detectors (PSDed), and reponse
                 * Then have to loop through analysis a few times:
                 *  - do allDet, response(+,x) and auto-corr
                 *  - form tfmap, cluster, get time-frequency dataLength
                 *  - do restricted analysis, form skymap, get skypos
                 */

                /* Create structure to hold array/slice of SH coefficients */
                ts_map = sh_series_array_new(inData->nSections, inData->sky_l_max, 0);
                sky = sh_series_new_zero(inData->sky_l_max, 0);

                /* Create arra/slice to hold SH coefficient power by frequency*/
                tf_map = sh_series_array_new(inData->nSections, fdplans->plans[0]->delay_product->n - 1, 1);
                tfSlice = sh_series_new_zero(tf_map->l_max, 1);

                /* Create structure to hold array of SH coefficients*/
                for (instrIdx = 0; instrIdx < fdplans->baselines->n_baselines; instrIdx++)
                {
                        each_ts_map[instrIdx] = sh_series_array_new(inData->nSections, inData->sky_l_max, 0);
                        eachDetSky[instrIdx] = sh_series_new_zero(inData->sky_l_max, 0);
                }

                /* Structure to hold cross-correlation for each detector pair
                   eachSky = sh_series_array_new(fdplans->baselines->n_baselines, inData->sky_l_max, 0);
                 */
                /*sky = sh_series_new_zero(inData->sky_l_max, 0 );*/
        }
        else
        {
                mexPrintf("Analysis type not matched, so proceeding with normal analysis\n");
                inData->analysisType = "normal";
                inData->antennaResponse = 0;
        }

        /* Dump out input data */
        mexPrintf("Analysis Input: \n\tnInstr: %i\n\tFFTLen: %i\n\tblockLen: %i\n\tdeltaT: %f\n\tl max: %i\n", inData->nInstr, inData->FFTLen, inData->lenDataStreams, inData->deltaT, inData->sky_l_max);

        /* Allocate memory from the time/frequency data */
        mexPrintf("Allocating arrays for analysis\n");

        for (instrIdx = 0; instrIdx < (inData->nInstr - 1); instrIdx++)
                endSeries[instrIdx] = malloc(inData->FFTLen * sizeof(*endSeries[instrIdx]));

        /* Create skymap with appropriate l value
        * l value of sky due to baseline length and sample frequency
        */
        shLength = sh_series_length(sky->l_max, sky->polar);

        if (sky == NULL)
        {
                mexPrintf("Could not allocate memory for sky\n");
                return 0x0;
        }

        /* Calculate the number of time-shifts requested */
        /* nShifts, inData->nInstr, timeShifts*/

        mexPrintf("\nChecking inData structure:\n");
        mexPrintf("\tAnalysis type: %s\n", inData->analysisType);
        mexPrintf("\tdeltaT: %g\n", inData->deltaT);
        mexPrintf("\tFFTLen: %d\n", inData->FFTLen);

        mexPrintf("\tSample frequency: %d\n", inData->sampleFrequency);
        mexPrintf("\tFreq range: [%i, %i]\n", inData->freqRange[0], inData->freqRange[1]);

        mexPrintf("Starting analysis loop\n");

        int lenShifts = (int)inData->nTimeLags /*inData->lenDataStreams/(timeShifts[1]*inData->sampleFrequency)*/;
        mexPrintf("LenShifts: %d\n", lenShifts);

        int bufferIdx = (inData->lenDataStreams / (inData->FFTLen / 2) - 1) * (inData->FFTLen / 2);
        mexPrintf("End point: %d\n", bufferIdx);

        /* Due to the way I process timeshifts, I need to create a bridge buffer that holds
        * the data from the end of the data stream and the start of the time stream.
        * This is used when the end of the window 'falls' outside of our data buffer
        * Currently hardwired for a 50% overlap.
        * Populate 'wrap around' array from beginning and end of time series */
        for (instrIdx = 0; instrIdx < (inData->nInstr - 1); instrIdx++)
        {
                memcpy(&endSeries[instrIdx][0], &inData->series[instrIdx + 1][(inData->lenDataStreams / (inData->FFTLen / 2) - 1) * (inData->FFTLen / 2)], (inData->FFTLen / 2) * sizeof(**endSeries));
                memcpy(&endSeries[instrIdx][(int)(inData->FFTLen / 2)], &inData->series[instrIdx + 1][0], (inData->FFTLen / 2) * sizeof(**endSeries));
        }

        /* Check data held in buffer */
        mexPrintf("\nOne, buff: %g\n", inData->series[1][0 /*bufferIdx+511*/]);
        mexPrintf("One,  end: %g\n", endSeries[0][(int)(inData->FFTLen / 2)]);
        if (inData->nInstr == 3)
        {
                mexPrintf("Two, buff: %g\n", inData->series[2][0 /*bufferIdx+511*/]);
                mexPrintf("Two,  end: %g\n\n", endSeries[1][(int)(inData->FFTLen / 2)]);
        }

        for (shiftIdx = 0; shiftIdx < lenShifts; shiftIdx++) /* Start of timeShift loop */
        {
                mexPrintf("**************************** ");
                mexPrintf("Analysing lag number %d of %d", shiftIdx + 1, inData->nTimeLags);
                mexPrintf(" ****************************\n");

                /* Zero all variables that will be re-used in subsequent loops */
                timeBlock = 0;

                restrictedIdx[0] = 0;
                restrictedIdx[1] = inData->lenDataStreams;

                if (shiftIdx == 0)
                {
                        /* Zero lag is 'special' as it is a one off event in the analysis
                         * so set up what we need here */
                        mexPrintf("\tPerforming zero-lag analysis\n");
                        int ii;
                        timeLagIdx = malloc((inData->nInstr - 1) * sizeof(int *));
                        for (ii = 0; ii < (inData->nInstr - 1); ii++)
                                timeLagIdx[ii] = 0;
                }
                else
                {
                        timeLagIdx = malloc((inData->nInstr - 1) * sizeof(int *));
                        /* Get index into timelag array for the current shift */
                        timeLagIdx[0] = shiftIdx;
                        timeLagIdx[1] = shiftIdx;
                }
                if (timeLagIdx == NULL)
                {
                        fprintf(stderr, "lag calculation failed, exiting\n");
                        return NULL;
                }

                mexPrintf("\tTime shifting detector 1 by 0 s\n");
                mexPrintf("\tTime shifting detector 2 by %d s (%d idx)\n", timeShifts[timeLagIdx[0]], timeLagIdx[0]);

                /* Make filename for .mat output*/
                shiftPath = (char *)malloc(60 * sizeof(char));
                sprintf(shiftPath, "SPHRAD_SHIFT_%d_DUMP", timeShifts[timeLagIdx[0]]);

                matFileName = make_filename(inData->outputDirectory, inData->detName, shiftPath, (int)round(inData->blockStartTime), (int)round(inData->blockLen), ".mat");
                mexPrintf("\nFilename for matlab products: %s\n", matFileName);

                /*Make filename for .txt event output */
                sprintf(shiftPath, "SPHRAD_EVENTS_SHIFT_%d", timeShifts[timeLagIdx[0]]);

                txtFileName = make_filename(inData->outputDirectory, inData->detName, shiftPath, (int)round(inData->segmentStartTime), (int)round(inData->segmentDuration), ".txt");
                mexPrintf("\nFilename for EVENT output: %s\n", txtFileName);

                /* We are assuming that the file has been cleared from the last run, if appropriate */
                outFile = fopen(txtFileName, "a");
                if (outFile == NULL)
                {
                        mexPrintf("Unable to open %s for writing\n", txtFileName);
                        return NULL;
                }

                /* Make filename for sky position output */
                sprintf(shiftPath, "SPHRAD_skyPos_SHIFT_%d", timeShifts[timeLagIdx[0]]);
                skyFileName = make_filename(inData->outputDirectory, inData->detName, shiftPath, (int)round(inData->segmentStartTime), (int)round(inData->segmentDuration), ".txt");
                mexPrintf("\nFilename for skyPos output: %s\n", skyFileName);

                free(shiftPath);

                /* We are assuming that the file has been cleared from the last run, if appropriate */
                skyposOut = fopen(skyFileName, "a");
                if (skyposOut == NULL)
                        mexPrintf("Unable to open %s for writing\n", skyFileName);

                /* allocate memory for restricted map name */
                restrictedName = (char *)malloc(40 * sizeof(char));

                /* Clean shift indices */
                dectIdx[0] = 0;
                dectIdx[1] = 0;
                dectIdx[2] = 0;

                /* Generate base datastream values */
                baseDectIdx[0] = 0;
                baseDectIdx[1] = timeShifts[timeLagIdx[0]] * inData->sampleFrequency;
                mexPrintf("BaseDectIdx[1] %i \n", baseDectIdx[1]);
                /* data - 2*transient - slide */
                if (shiftIdx == 0)
                {
                        baseDectIdx[2] = 0;
                }
                else
                {
                        baseDectIdx[2] = (inData->blockLen - 2 * (inData->transientLen) / (inData->FFTLen) - timeShifts[timeLagIdx[1]]) * inData->sampleFrequency;
                }
                mexPrintf("BaseDectIdx[2] %i \n", baseDectIdx[2]);

                /* If not a full analysis, carry out requested search */
                if (strcmp(inData->analysisType, "full") != 0)
                {
                        /* Test multi resolution time-frequency maps */
                        if (strcmp(inData->analysisType, "fast_tfmap") == 0)
                        {
                                sh_series_array_free(ts_map);

                                /* Create array to hold SH coefficient power by frequency*/
                                ts_map = sh_series_array_new(inData->aLen[inData->tfScale], fdplans->plans[0]->delay_product->n - 1, 1);
                        }

                        /* Just call whatever was asked for */
                        segmentIdx = cross_loop(NULL, ts_map, sky, inData, inData->analysisType, fdplans, endSeries, NULL, baseDectIdx, restrictedIdx, inData->tfScale);
                        mexPrintf("Array size: (%d, %d)\n\n", ts_map->n, sh_series_length(ts_map->l_max, ts_map->polar));
                }
                else
                {
                        /*
                         *   Carry out full analysis
                         */

                        /* Create a harmonic time series */
                        segmentIdx = cross_loop(each_ts_map, ts_map, sky, inData, "normal", fdplans, endSeries, NULL, baseDectIdx, restrictedIdx, 0);

                        /* SLOW METHOD OF EXTRACTING (in)coherent spherical harmonic values from the normal map.*/
                        mexPrintf("Analysing spherical harmonic time series for glitch information\n");
                        int segIdx = 0;
                        int segDur = 0;
                        int maxIdx = 0;
                        char *shlineFileName;
                        FILE *shOutFile;
                        double maxValue = -1e30;
                        int cIdx;

                        double incSHEnergy[segmentIdx];
                        double SHEnergy[segmentIdx];

                        /* Individual files */
                        /*    shlineFileName = make_filename(inData->outputDirectory, inData->detName, "shPower", (int)round(inData->blockStartTime), (int) round(inData->blockLen), ".txt");*/
                        /* Single file for all */
                        shlineFileName = make_filename(inData->outputDirectory, inData->detName, "shPower", (int)round(inData->segmentStartTime), (int)round(inData->segmentDuration), ".txt");

                        mexPrintf("\nFilename for shPower products: %s\n", shlineFileName);

                        shOutFile = fopen(shlineFileName, "a");
                        if (shOutFile == NULL)
                        {
                                mexPrintf("Unable to open %s for writing\n", shlineFileName);
                                return NULL;
                        }

                        /* For each segment */
                        for (segIdx = 0; segIdx < segmentIdx; segIdx++)
                        {
                                /* Record incoherent energy */
                                incSHEnergy[segIdx] = sqrt(pow(creal(ts_map->series[segIdx].coeff[0]), 2) + pow(cimag(ts_map->series[segIdx].coeff[0]), 2));

                                /* and calcualte coherent energy in the spherical harmonics */
                                SHEnergy[segIdx] = 0;
                                for (cIdx = 1; cIdx < sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar); cIdx++)
                                        SHEnergy[segIdx] += sqrt(pow(creal(ts_map->series[segIdx].coeff[cIdx]), 2) + pow(cimag(ts_map->series[segIdx].coeff[cIdx]), 2));

                                if (SHEnergy[segIdx] > maxValue)
                                {
                                        maxValue = SHEnergy[segIdx];
                                        maxIdx = segIdx;
                                }

                                fprintf(shOutFile, "%f\t%g\t%g\t%g\n", inData->blockStartTime + (inData->transientLen / inData->sampleFrequency) + (segIdx) * (inData->fourierLen / 2.), SHEnergy[segIdx], incSHEnergy[segIdx], SHEnergy[segIdx] / incSHEnergy[segIdx]);
                        }
                        mexPrintf("Maximum power occured in :\n\tsegment %d -> total power %f\n\tGlitch rejection parameter of %f\n\n", maxIdx, SHEnergy[maxIdx] + incSHEnergy[maxIdx], SHEnergy[maxIdx] / incSHEnergy[maxIdx]);

                        /* Output the shline */
                        fclose(shOutFile);
                        free(shlineFileName);

                        /* Check if followup required - else just output power line
                           And free all memory
                         */
                        if (inData->followUps == 0)
                        {
                                /* Output text variables */
                                free(matFileName);
                                free(skyFileName);
                                free(txtFileName);
                                free(timeLagIdx);
                                free(restrictedName);

                                sh_series_array_free(ts_map);
                                for (i = 0; i < fdplans->baselines->n_baselines; i++)
                                        sh_series_array_free(each_ts_map[i]);

                                break;
                        }

                        /* Use tsmap to get potential triggers */
                        if (inData->debugLevel > 1)
                        {
                                mexPrintf("Finding max SH power\n");
                                int pIdx = 0;
                                int maxIdx = 0;
                                double maxValue = 0;
                                int cIdx;
                                double *pArray;

                                pArray = (double *)malloc(ts_map->n * sizeof(double));
                                for (pIdx = 0; pIdx < (ts_map->n); pIdx++)
                                {
                                        pArray[pIdx] = 0;
                                        for (cIdx = 0; cIdx < sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar); cIdx++)
                                                pArray[pIdx] += sqrt(pow(creal(ts_map->series[pIdx].coeff[cIdx]), 2) + pow(cimag(ts_map->series[pIdx].coeff[cIdx]), 2));

                                        if (pArray[pIdx] > maxValue)
                                        {
                                                maxValue = pArray[pIdx];
                                                maxIdx = pIdx;
                                        }
                                }

                                if (inData->debugLevel > 1)
                                {
                                        restrictedIdx[0] = (int)round(maxIdx) * (inData->FFTLen / 2);
                                        restrictedIdx[1] = (int)round(maxIdx) * (inData->FFTLen / 2) + (inData->FFTLen);

                                        segmentIdx = cross_loop(NULL, ts_map, sky, inData, "normal", fdplans, endSeries, NULL, baseDectIdx, restrictedIdx, 0);

                                        product_output(matFileName, "HL_tsmap", fdplans->plans[0]->power_2d, sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar), 1, 2);
                                        product_output(matFileName, "HV_tsmap", fdplans->plans[1]->power_2d, sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar), 1, 2);
                                        product_output(matFileName, "LV_tsmap", fdplans->plans[2]->power_2d, sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar), 1, 2);
                                        product_output(matFileName, "H_tsmap", fdplans->plans[3]->power_2d, sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar), 1, 2);
                                        product_output(matFileName, "L_tsmap", fdplans->plans[4]->power_2d, sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar), 1, 2);
                                        product_output(matFileName, "V_tsmap", fdplans->plans[5]->power_2d, sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar), 1, 2);

                                        /* Reset the restricted index back to the length of the data stream */
                                        restrictedIdx[0] = 0;
                                        restrictedIdx[1] = inData->lenDataStreams;
                                }

                                mexPrintf("Dumping max power line\n");
                                product_output(matFileName, "sh_power", pArray, ts_map->n, 1, 1);

                                free(pArray);
                        }

                        /* Create a time-frequency map */
                        /* Remember that the actual size is tf_map->l_max+1 by tf_map->n, due to the fact we are using a sh_series structure to hold it */
                        segmentIdx = cross_loop(NULL, tf_map, tfSlice, inData, "fast_tfmap", fdplans, endSeries, NULL, baseDectIdx, restrictedIdx, 0);

                        /* Cluster pixels on tfmap */
                        mexPrintf("\tArray size: (%d, %d)\n\n", tf_map->n, sh_series_length(tf_map->l_max, tf_map->polar));

                        /* Value of lowest frequency that we can use */
                        int lowestFreq = 25;
                        int highestFreq = 1000;
                        int lowestIndex = round(lowestFreq * inData->fourierLen);
                        int highestIndex = round(highestFreq * inData->fourierLen);

                        /* Only cluster on the positive frequencies minus the DC component */
                        int timeBins = tf_map->n;
                        int freqBins = sh_series_length(tf_map->l_max, tf_map->polar) / 2;

                        int nElements = (tf_map->n * sh_series_length(tf_map->l_max, tf_map->polar));
                        sortedArray = (double *)calloc(timeBins * freqBins, sizeof(double));
                        tfMap = (double *)calloc(timeBins * freqBins, sizeof(double));

                        mexPrintf("\tTime-frequency mapping -> fBins - %d, tBins - %d\n", freqBins, timeBins);

                        /* Only extract the positive frequencies to cluster on */
                        int rowIdx, colIdx;
                        for (rowIdx = 0; rowIdx < timeBins; rowIdx++)
                                for (colIdx = 0; colIdx < (freqBins); colIdx++)
                                {

                                        idx = rowIdx * freqBins + colIdx;

                                        /* Remove bad data from the map */
                                        if (/*(rowIdx==0) || (rowIdx==(timeBins-1)) ||*/ (colIdx < lowestIndex) || (colIdx > highestIndex) || check_isnan(creal(tf_map->series[rowIdx].coeff[colIdx])) || check_isinf(creal(tf_map->series[rowIdx].coeff[colIdx])))
                                        {
                                                /* mexPrintf( "Value (%g) at (%d,%d) is out of bounds - setting to zero\n",creal(tf_map->series[rowIdx].coeff[colIdx]), rowIdx,colIdx);*/
                                                sortedArray[idx] = 0.0;
                                                tfMap[idx] = 0.0;
                                        }
                                        else
                                        {
                                                sortedArray[idx] = creal(tf_map->series[rowIdx].coeff[colIdx]);
                                                tfMap[idx] = creal(tf_map->series[rowIdx].coeff[colIdx]);
                                        }
                                }

                        /* Apply Gaussian blurring to smooth the map.
                         * Take the difference of maps with two different
                         * smoothing radii to enhance line features.
                         * Note that we use either sortedArray or tfMap 
                         * to set the pixel thresholds, depending on    
                         * whether or not regions are used. So we have  
                         * to manipulate copies of BOTH maps.    
                         */
                        if (inData->blurring[0] > 0)
                        {

                                mexPrintf("\tStarting blurring operation   \n");

                                /* First prepare storage. */
                                sortedArray_copy = (double *)calloc(timeBins * freqBins, sizeof(double));
                                sortedArray_copy = sortedArray;
                                tfMap_copy = (double *)calloc(timeBins * freqBins, sizeof(double));
                                tfMap_copy = tfMap;

                                /* Initialise variables for looping over blurring kernel. */
                                int kernel_idx, kernel_rowIdx, kernel_colIdx, kernel_maxIdx;
                                double sigSigma, bckSigma;
                                double weight;

                                /* Assign properties of the smoothing kernel. */
                                sigSigma = inData->blurring[0];
                                bckSigma = inData->blurring[1];
                                if (bckSigma > 0.0)
                                {
                                        kernel_maxIdx = 3.0 * bckSigma;
                                }
                                else
                                {
                                        kernel_maxIdx = 3.0 * sigSigma;
                                }

                                /* Verbosity for debugging */
                                mexPrintf("\tSet signal smoothing radius: sigSigma = %g pixels.\n", sigSigma);
                                mexPrintf("\tSet bckgrd smoothing radius: bckSigma = %g pixels.\n", bckSigma);
                                mexPrintf("\tSet smoothing kernel radius: kernel_maxIdx = %g pixels.\n", kernel_maxIdx);

                                /* Loop over map. */
                                for (rowIdx = 0; rowIdx < timeBins; rowIdx++)
                                {
                                        for (colIdx = 0; colIdx < freqBins; colIdx++)
                                        {

                                                /* index into maps */
                                                idx = rowIdx * freqBins + colIdx;

                                                /* Initialise map values to zero. */
                                                sortedArray_copy[idx] = 0;
                                                tfMap_copy[idx] = 0;

                                                /* loop over smoothing kernel */
                                                for (kernel_rowIdx = -kernel_maxIdx; kernel_rowIdx < (kernel_maxIdx + 1); kernel_rowIdx++)
                                                {
                                                        for (kernel_colIdx = -kernel_maxIdx; kernel_colIdx < (kernel_maxIdx + 1); kernel_colIdx++)
                                                        {
                                                                /* make sure we have not walked off the edge of the map */
                                                                if ((rowIdx + kernel_rowIdx < 0) || (rowIdx + kernel_rowIdx > (timeBins - 1)))
                                                                {
                                                                        continue;
                                                                }
                                                                else if ((colIdx + kernel_colIdx < 0) || (colIdx + kernel_colIdx > (freqBins - 1)))
                                                                {
                                                                        continue;
                                                                }
                                                                else
                                                                {
                                                                        /* location of the target pixel to be summed into current pixel */
                                                                        kernel_idx = (rowIdx + kernel_rowIdx) * freqBins + (colIdx + kernel_colIdx);
                                                                        /* weighting to be applied to target pixel */
                                                                        weight = pow(2.0 * 3.14 * sigSigma * sigSigma, -1) * exp(-(1.0 * kernel_rowIdx * kernel_rowIdx + 1.0 * kernel_colIdx * kernel_colIdx) / (2.0 * sigSigma * sigSigma));
                                                                        /* optionally subtract averaged background */
                                                                        if (inData->blurring[1] > 0)
                                                                        {
                                                                                weight = weight - pow(2.0 * 3.14 * bckSigma * bckSigma, -1) * exp(-(1.0 * kernel_rowIdx * kernel_rowIdx + 1.0 * kernel_colIdx * kernel_colIdx) / (2.0 * bckSigma * bckSigma));
                                                                        }
                                                                        /* add into the map */
                                                                        sortedArray_copy[idx] = sortedArray_copy[idx] + weight * sortedArray[kernel_idx];
                                                                        tfMap_copy[idx] = tfMap_copy[idx] + weight * tfMap[kernel_idx];
                                                                        if (((idx == 0) || (idx == 1) || (idx == 2000)) && (kernel_rowIdx == 0) && (kernel_colIdx == 0))
                                                                        {
                                                                                mexPrintf("\tAt centre of kernel. weight = %g\n", weight);
                                                                                mexPrintf("\t                     rowIdx = %d\n", rowIdx);
                                                                                mexPrintf("\t                     colIdx = %d\n", colIdx);
                                                                                mexPrintf("\t                        idx = %d\n", idx);
                                                                                mexPrintf("\t              kernel_rowIdx = %d\n", kernel_rowIdx);
                                                                                mexPrintf("\t              kernel_colIdx = %d\n", kernel_colIdx);
                                                                                mexPrintf("\t                 kernel_idx = %d\n", kernel_idx);
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }

                                /* Ovewrite original arrays with modified ones. */
                                sortedArray = sortedArray_copy;
                                tfMap = tfMap_copy;

                                mexPrintf("\tFinished blurring operation   \n");
                        } /* end of optional blurring */

                        /* Generate the percentage values
                         *	- if we have one threshold, or they are the same, perform nearest neighbour clustering
                         *	- if we have two unique values then we are performing coronal clustering
                         */
                        if (inData->nRegions == 0)
                        {
                                threshold[0] = prcValue(sortedArray, inData->clusterThresholds[0], timeBins * freqBins, 1);
                                threshold[1] = prcValue(sortedArray, inData->clusterThresholds[1], timeBins * freqBins, 0);
                                mexPrintf("\tPcrtile value for %g%% (%g%%) -> %g (%g)\n", inData->clusterThresholds[0], inData->clusterThresholds[1], threshold[0], threshold[1]);

                                /* Cluster on the time-frequency map *
                                   numClusters = pixel_chain(&clusterMap, &clusterStruct, tfMap, timeBins, freqBins,threshold, 0, inData->debugLevel, 0);*/
                                numClusters = pixel_chain(&clusterMap, &clusterStruct, tfMap, freqBins, timeBins, threshold, inData->pixelDistance, inData->debugLevel, inData->codeVersion);
                        }
                        else
                        {
                                numClusters = regional_pixel_chain(&clusterMap, &clusterStruct, tfMap, freqBins, timeBins, inData->clusterThresholds, inData->nRegions, inData->regionDirection, inData->pixelDistance, inData->debugLevel, inData->codeVersion);
                        }

                        /* Output all of the above products:
                         *	1) tfmap
                         *	2) clusterMap
                         *	3) clusterStruct - has 10 elements
                         */

                        if (inData->debugLevel >= 0)
                        {
                                /* If not quiet mode */
                                mexPrintf("Writing data products to matlab file\n");
                                product_output(matFileName, "full_tsmap", ts_map, sh_series_length(ts_map->series[0].l_max, ts_map->series[0].polar), ts_map->n, 2);
                                product_output(matFileName, "fast_tfmap", tf_map, sh_series_length(tf_map->l_max, tf_map->polar), tf_map->n, 2);
                                product_output(matFileName, "clusterMap", clusterMap, freqBins, timeBins, 0);
                                product_output(matFileName, "clusterStruct", clusterStruct, NUM_CLUSTER_ELEMENTS, numClusters, 1);
                        }
                        /* Remove unwanted memory */
                        free(sortedArray);
                        free(tfMap);

                        /* MEM: Test the removal of ts_map to save money. Create a smaller one for each cluster */
                        sh_series_array_free(ts_map);
                        for (i = 0; i < fdplans->baselines->n_baselines; i++)
                                sh_series_array_free(each_ts_map[i]);

                        /* If less clusters than defined followup number, assign accordingly */
                        if (numClusters < inData->followUps)
                                followUp = numClusters;
                        else
                                followUp = inData->followUps;

                        /* Structure to hold event information */
                        struct eventStruct *events[followUp];

                        mexPrintf("******************************* LOOPING OVER CLUSTERS *******************************\n");

                        /* Loop over the loudest clusters */
                        for (clusterIdx = 0; clusterIdx < followUp; clusterIdx++)
                        {
                                /* Allocate memory for the event structure */
                                int timeSpan = 0;

                                events[clusterIdx] = (struct eventStruct *)malloc(sizeof(struct eventStruct));
                                mexPrintf("*** Cluster %d of %d - numPixels: %d, Pixel boundary: (%d-%d, %d-%d), Cluster energy: %g ***\n", (clusterIdx + 1), followUp, (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 1]), (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 2]), (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 3]), (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 4]), (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 5]), clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 6]);

                                /* Create the name for the output structure */
                                sprintf(restrictedName, "restricted_tsmap%d", (clusterIdx + 1));

                                /* Freq range - probably need to add in a margin of error here */

                                inData->freqRange[0] = (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 2]);
                                inData->freqRange[1] = (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 3]);

                                /*      inData->freqRange[0] = 100;
                                inData->freqRange[0] = 101;
                                */

                                /* Check frequency range is within our boundaries */
                                if (inData->freqRange[0] < lowestIndex)
                                {
                                        inData->freqRange[0] = lowestIndex + 1;
                                        mexPrintf("\t RAISING FREQUENCY \n");
                                }

                                if (inData->freqRange[1] < lowestIndex)
                                {
                                        inData->freqRange[1] = lowestIndex + 2;
                                        mexPrintf("\t RAISING FREQUENCY \n");
                                }

                                if (inData->freqRange[0] == inData->freqRange[1])
                                        inData->freqRange[1] += 1;

                                /* Time range - in samples, not time bins */
                                restrictedIdx[0] = (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 4]) * (inData->FFTLen / 2);
                                restrictedIdx[1] = (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 5]) * (inData->FFTLen / 2) + (inData->FFTLen);
                                timeSpan = (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 5]) - (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 4]) + 1;

                                /* TEST DIFFERENT APPROACH */

                                /* Clear data from previous analysis
                                   memset(ts_map->coeff, 0+0i, inData->nSections * sh_series_length(inData->sky_l_max, 0) * sizeof(complex double));
                                 */

                                /* MEM: clearing and recreating the tsmap*/
                                ts_map = sh_series_array_new(timeSpan, inData->sky_l_max, 0);
                                memset(ts_map->coeff, 0 + 0i, timeSpan * sh_series_length(inData->sky_l_max, 0) * sizeof(complex double));

                                for (instrIdx = 0; instrIdx < fdplans->baselines->n_baselines; instrIdx++)
                                        each_ts_map[instrIdx] = sh_series_array_new(timeSpan, inData->sky_l_max, 0);

                                /* Need to check for sky location information */
                                if ((inData->skyPrior > 0) || (inData->testLocalisation > 0))
                                {
                                        /* Remember that start time is in time bins  */
                                        double injCentreTime = ((restrictedIdx[0] + (restrictedIdx[1] - restrictedIdx[0]) / 2.)) / inData->sampleFrequency;

                                        sky_pointing(inData->skyPos, inData->skyPos2, ((inData->skyPrior > 0) ? inData->skyPrior : inData->testLocalisation), injCentreTime);
                                }

                                /* For each top cluster, compute unsummed restricted tsmap  */
                                /* Sum across time bins then form our statistics of choice */
                                segmentIdx = cross_loop(each_ts_map, ts_map, sky, inData, "restricted", fdplans, endSeries, NULL, baseDectIdx, restrictedIdx, 0);

                                /* Outputting restricted tsmap summed across the detectors i.e. HL+LV+HV */
                                if (inData->debugLevel > 1)
                                        product_output(matFileName, restrictedName, ts_map, sh_series_length(ts_map->l_max, ts_map->polar), ts_map->n, 2);

                                /* Sum coeffs from each time bin together
                                 *   - can also use the time width of the cluster and calculate cross_loop for the single tfpixel
                                 */

                                int numTimeBins = 0;
                                int numCoeffs = 0;

                                numTimeBins = (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 5] - clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 4]);

                                /*      if (numTimeBins == 0)
                                numTimeBins = 1;
                                */
                                numCoeffs = sh_series_length(eachDetSky[0]->l_max, eachDetSky[0]->polar);

                                mexPrintf("\t numTimeBins:  %d,   numCoeffs:  %d \n", numTimeBins, numCoeffs);

                                mexPrintf("\t Taking data from %d -> %d\n", (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 4]), (int)round(clusterStruct[clusterIdx * NUM_CLUSTER_ELEMENTS + 5]));
                                /* mexPrintf("\t   -> (%g, %g)\n", creal(each_ts_map[0]->series[(int)round(clusterStruct[clusterIdx*NUM_CLUSTER_ELEMENTS+4])].coeff[0]), cimag(each_ts_map[0]->series[(int)round(clusterStruct[clusterIdx*NUM_CLUSTER_ELEMENTS+4])].coeff[0]));
                                 */

                                double incSHEnergyClust = 0;
                                double SHEnergyClust = 0;

                                /* Sum the time stripes together */
                                for (instrIdx = 0; instrIdx < antResponses->nBaselines; instrIdx++)
                                {
                                        for (coeffIdx = 0; coeffIdx < numCoeffs; coeffIdx++)
                                        {
                                                eachDetSky[instrIdx]->coeff[coeffIdx] = 0;
                                                /*
                                                for (arrayIdx=(int)round(clusterStruct[clusterIdx*NUM_CLUSTER_ELEMENTS+4]); arrayIdx <= (int)round(clusterStruct[clusterIdx*NUM_CLUSTER_ELEMENTS+5]);arrayIdx++)
                                                eachDetSky[instrIdx]->coeff[coeffIdx] += each_ts_map[instrIdx]->series[arrayIdx].coeff[coeffIdx];
                                                */
                                                for (arrayIdx = 0; arrayIdx <= numTimeBins; arrayIdx++)
                                                        eachDetSky[instrIdx]->coeff[coeffIdx] += each_ts_map[instrIdx]->series[arrayIdx].coeff[coeffIdx];

                                                /* TEST ------- Get new ratio power -------- TEST */
                                                /*	if (instrIdx < (antResponses->nBaselines-inData->nInstr))
                                                */
                                                if (coeffIdx > 0)
                                                        SHEnergyClust += sqrt(pow(creal(eachDetSky[instrIdx]->coeff[coeffIdx]), 2) + pow(cimag(eachDetSky[instrIdx]->coeff[coeffIdx]), 2));
                                        }
                                        /*	if (instrIdx < (antResponses->nBaselines-inData->nInstr))*/
                                        incSHEnergyClust += sqrt(pow(creal(eachDetSky[instrIdx]->coeff[0]), 2) + pow(cimag(eachDetSky[instrIdx]->coeff[0]), 2));
                                }

                                mexPrintf("\tRatio value:  (%f, %f) -> %f -> %f\n", SHEnergyClust, incSHEnergyClust, SHEnergyClust / incSHEnergyClust, incSHEnergyClust / SHEnergyClust);

                                if (inData->debugLevel >= 1)
                                {
                                        /* Create the name for the output structure */
                                        sprintf(restrictedName, "HLresSum%d", (clusterIdx + 1));
                                        product_output(matFileName, restrictedName, eachDetSky[0], sh_series_length(eachDetSky[0]->l_max, eachDetSky[0]->polar), 1, 2);

                                        /* Create the name for the output structure */
                                        sprintf(restrictedName, "HVresSum%d", (clusterIdx + 1));
                                        product_output(matFileName, restrictedName, eachDetSky[1], sh_series_length(eachDetSky[0]->l_max, eachDetSky[0]->polar), 1, 2);

                                        /* Create the name for the output structure */
                                        sprintf(restrictedName, "LVresSum%d", (clusterIdx + 1));
                                        product_output(matFileName, restrictedName, eachDetSky[2], sh_series_length(eachDetSky[0]->l_max, eachDetSky[0]->polar), 1, 2);

                                        /* Auto-correlation terms */
                                        sprintf(restrictedName, "HresSum%d", (clusterIdx + 1));
                                        product_output(matFileName, restrictedName, eachDetSky[3], sh_series_length(eachDetSky[0]->l_max, eachDetSky[0]->polar), 1, 2);
                                        sprintf(restrictedName, "LresSum%d", (clusterIdx + 1));
                                        product_output(matFileName, restrictedName, eachDetSky[4], sh_series_length(eachDetSky[0]->l_max, eachDetSky[0]->polar), 1, 2);
                                        sprintf(restrictedName, "VresSum%d", (clusterIdx + 1));
                                        product_output(matFileName, restrictedName, eachDetSky[5], sh_series_length(eachDetSky[0]->l_max, eachDetSky[0]->polar), 1, 2);
                                }

                                /* Now we generate our statistics on the frequency restricted sky */
                                double *energies = NULL;
                                double triggerTime = (inData->transientLen / inData->sampleFrequency) + (clusterStruct[4 + NUM_CLUSTER_ELEMENTS * clusterIdx] + (clusterStruct[5 + NUM_CLUSTER_ELEMENTS * clusterIdx] - clusterStruct[4 + NUM_CLUSTER_ELEMENTS * clusterIdx]) / 2.) * (inData->fourierLen / 2.);

                                mexPrintf("\tTrigger time: %g\n", triggerTime);

                                triggerTime = inData->blockStartTime + triggerTime;
                                energies = grid_calc_stats(eachDetSky, antResponses, inData, clusterIdx, matFileName, skyposOut, triggerTime);

                                /* Close skyposOut file. */
                                /* if (skyposOut != NULL)
                                   fclose(skyposOut); */

                                if ((energies[0] == 0) && (energies[3] == 0) && (energies[5] == 0))
                                        mexPrintf("\tERROR: Skipping cluster %d\n", clusterIdx + 1);

                                /* The following parameters are taken from clusterStruct.
                                 * Currently, this does not have any info regarding the physical space in which it works,
                                 * thus we need to convert it here.
                                 * We have a 50% overlap on blocks, a sample frequency of fs, and a fourier
                                 *      transform length of FFTlen thus:
                                 *        Time(s): blockStart + transients/fs + timeIdx*(FFTLen/2)
                                 *   Duration (s): durationLen*(FFTLen/2)
                                 *
                                 * Currently, we are using 1Hz resolution so:
                                 * Frequency (Hz): freqIdx
                                 * Bandwidth (Hz): freqLen
                                 */

                                /* Unphysical results - i.e. cluster indicies */
                                events[clusterIdx]->duration = clusterStruct[5 + NUM_CLUSTER_ELEMENTS * clusterIdx] - clusterStruct[4 + NUM_CLUSTER_ELEMENTS * clusterIdx];
                                events[clusterIdx]->bandwidth = clusterStruct[3 + NUM_CLUSTER_ELEMENTS * clusterIdx] - clusterStruct[2 + NUM_CLUSTER_ELEMENTS * clusterIdx];
                                events[clusterIdx]->time = clusterStruct[4 + NUM_CLUSTER_ELEMENTS * clusterIdx] + events[clusterIdx]->duration / 2.;
                                events[clusterIdx]->frequency = clusterStruct[2 + NUM_CLUSTER_ELEMENTS * clusterIdx] + events[clusterIdx]->bandwidth / 2.;

                                segIdx = events[clusterIdx]->time;
                                segDur = events[clusterIdx]->duration / 2;

                                /* Physical results */
                                events[clusterIdx]->time = inData->blockStartTime + (inData->transientLen / inData->sampleFrequency) + (events[clusterIdx]->time) * (inData->fourierLen / 2.);
                                events[clusterIdx]->duration = events[clusterIdx]->duration * (inData->fourierLen / 2.);
                                events[clusterIdx]->frequency = events[clusterIdx]->frequency * inData->freqRatio;
                                events[clusterIdx]->bandwidth = events[clusterIdx]->bandwidth * inData->freqRatio;

                                /* Information about the tfmap cluster */
                                events[clusterIdx]->clusterEnergy = clusterStruct[6 + NUM_CLUSTER_ELEMENTS * clusterIdx];
                                events[clusterIdx]->numPixels = (int)clusterStruct[1 + NUM_CLUSTER_ELEMENTS * clusterIdx];

                                /* Retrive power from spherical harmonic coefficients */
                                int shStart = 0;
                                int shEnd = 0;
                                int ii = 0;

                                /* Check start segment value */
                                mexPrintf("/****************** Glitch rejection *************************/\n");

                                if (0)
                                {
                                        shStart = segIdx - segDur;
                                        mexPrintf("shStart: %d -> ", shStart);
                                        if (shStart < 0)
                                                shStart = 0;
                                        mexPrintf("shPower (I, E) (%g, %g)\n", incSHEnergy[shStart], SHEnergy[shStart]);

                                        /* Check end segment value */
                                        shEnd = segIdx + segDur;
                                        mexPrintf("shEnd: %d -> ", shEnd);
                                        if (shEnd > tf_map->n)
                                                shEnd = tf_map->n;
                                        mexPrintf("shPower (I, E) (%g, %g)\n", incSHEnergy[shEnd], SHEnergy[shEnd]);

                                        /* Zero out trigger sums */
                                        events[clusterIdx]->shE = 0;
                                        events[clusterIdx]->shI = 0;

                                        /* Sum coherent and incoherent parts for each time bin that the trigger is in */
                                        for (ii = shStart; ii <= shEnd; ii++)
                                        {
                                                events[clusterIdx]->shE += SHEnergy[ii];
                                                events[clusterIdx]->shI += incSHEnergy[ii];
                                        }

                                        mexPrintf("Total over cluser (E, I) - > (%g,%g) -> ratio: %g\n", events[clusterIdx]->shE, events[clusterIdx]->shI, events[clusterIdx]->shE / events[clusterIdx]->shI);
                                }

                                /* Test new method of calculating SH glitch - using the frequency restricted version */
                                events[clusterIdx]->shE = SHEnergyClust;
                                events[clusterIdx]->shI = incSHEnergyClust;
                                mexPrintf("Total over cluser (E, I) - > (%g,%g) -> ratio %g\n", events[clusterIdx]->shE, events[clusterIdx]->shI, events[clusterIdx]->shE, events[clusterIdx]->shI);

                                /* Sky position from frequency restricted skymap */
                                events[clusterIdx]->theta = M_PI * (energies[0] / (2. * (inData->sky_l_max + 1)));
                                events[clusterIdx]->phi = 2 * M_PI * (energies[1] / (2. * (inData->sky_l_max + 1)));

                                /* Energies */
                                events[clusterIdx]->crossE = energies[4];
                                events[clusterIdx]->crossI = energies[6];
                                events[clusterIdx]->plusE = energies[3];
                                events[clusterIdx]->plusI = energies[5];
                                events[clusterIdx]->nullE = energies[8];
                                events[clusterIdx]->nullI = energies[9];
                                events[clusterIdx]->standardE = energies[2];
                                events[clusterIdx]->standardI = energies[7];

                                free(energies);

                                /* MEM: Test the removal and recreation of the ts_map */
                                sh_series_array_free(ts_map);
                                for (i = 0; i < fdplans->baselines->n_baselines; i++)
                                        sh_series_array_free(each_ts_map[i]);

                                if (clusterIdx < followUp)
                                        mexPrintf("**********************************************************\n");
                        } /* End of loop for each of the loudest clusters */

                        mexPrintf("************************** FINISHED LOOPING OVER CLUSTERS ***************************\n");

                        /* Write out event information in the format of an Omega EVENT file */
                        output_omega_event(followUp, outFile, inData, events);
                        fclose(outFile);

                        if (inData->debugLevel > 1)
                                mexPrintf("Freeing memory\n");
                        /* Free data associated with previous cycle */

                        for (clusterIdx = 0; clusterIdx < followUp; clusterIdx++)
                                free(events[clusterIdx]);

                        if (clusterMap != NULL)
                                free(clusterMap);

                        if (clusterStruct != NULL)
                                free(clusterStruct);

                } /* End full analysis */

                /*  if ((strcmp(inData->analysisType, "tfmap") != 0) || (strcmp(inData->analysisType, "tfsmap") != 0) || (strcmp(inData->analysisType, "fast_tfmap") != 0))
                {
                mexPrintf( "Max power was %g, in segment %i (%f seconds)\n",  identify_candidate(&ts_map->series[segmentIdx], inData->deltaT), segmentIdx,((double)segmentIdx/(double)inData->nSections)*inData->blockLen);
                }
                */

                /* Output text variables */
                free(matFileName);
                free(skyFileName);
                free(txtFileName);
                free(timeLagIdx);
                free(restrictedName);

                if (lenShifts > 1)
                {

                        mexPrintf("Freeing previous structures (sky, tfMap,tfSlice) not to increase memory per slide\n");
                        if (sky)
                                sh_series_free(sky);
                        if (tf_map)
                                sh_series_array_free(tf_map);
                        if (tfSlice)
                                sh_series_free(tfSlice);

                        mexPrintf("Recreating data structures for next time shifts\n");
                        /* Create structure to hold array/slice of SH coefficients */
                        ts_map = sh_series_array_new(inData->nSections, inData->sky_l_max, 0);
                        sky = sh_series_new_zero(inData->sky_l_max, 0);

                        /* Create arra/slice to hold SH coefficient power by frequency*/
                        tf_map = sh_series_array_new(inData->nSections, fdplans->plans[0]->delay_product->n - 1, 1);
                        tfSlice = sh_series_new_zero(tf_map->l_max, 1);

                        /* Create structure to hold array of SH coefficients*/
                        for (instrIdx = 0; instrIdx < fdplans->baselines->n_baselines; instrIdx++)
                                each_ts_map[instrIdx] = sh_series_array_new(inData->nSections, inData->sky_l_max, 0);
                }

                stopAnalysis = clock();

                cumulativeAnalysis += (stopAnalysis - startAnalysis);
                mexPrintf(" ################################################################# Shift %d - duration %fs (cumulative - %fs) ########################################################\n", shiftIdx + 1, ((double)(stopAnalysis - startAnalysis) / CLOCKS_PER_SEC), ((double)cumulativeAnalysis) / CLOCKS_PER_SEC);
                startAnalysis = stopAnalysis;

        } /* End of timeShift loop */

        if (lenShifts > 1)
        {
                mexPrintf("Freeing memory associated with timeshifts\n");
                /* Need to reinitalise the memory structures */
                if (ts_map)
                        sh_series_array_free(ts_map);
        }

        /* Free all of the structures */
        if (inData->debugLevel > 1)
                mexPrintf("Freeing data structures and allocated memory\n");

        /* Release files
        fclose(outFile);
        */
        if (skyposOut != NULL)
                fclose(skyposOut);

        if (strcmp(inData->analysisType, "tfsmap") == 0)
        {
                for (i = 0; i < inData->nSections; i++)
                        sh_series_array_free(tfs_map[i]);
        }

        for (instrIdx = 0; instrIdx < (inData->nInstr - 1); instrIdx++)
                free(endSeries[instrIdx]);

        /*
        if (strcmp(inData->analysisType, "full") == 0)
        for(instrIdx = 0; instrIdx < fdplans->baselines->n_baselines ; instrIdx++)
        sh_series_array_free(each_ts_map[instrIdx]);
        */

        sh_series_array_free(eachSky);
        free(skyGrid);
        sh_series_free(sky);

        if (strcmp(inData->analysisType, "full") == 0)
        {
                sh_series_array_free(tf_map);
                /*  sh_series_array_free(ts_map);*/
                sh_series_free(tfSlice);

                for (i = 0; i < fdplans->baselines->n_baselines; i++)
                {
                        /*    sh_series_array_free(each_ts_map[i]); */
                        sh_series_free(eachDetSky[i]);
                }
                return NULL;
        }
        else
                return ts_map;
}

/* Load analysis parameters into structure for convenience
*
* For cell arrays, the input data should be as follows:
* 0) n_instr
* 1) {data1 [,data2, data3]}
* 2) analysisStructure - [integration_length, deltaT, blockStart, blockLen, transientLent, segmentStart, segmentDuration]
* 3) {dect1 [, dect2, dect3]}
* 4) antennaResponse
* 5) analysisType 
*/
struct analysisData *get_inputs(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[], int n_instr)
{
        /* Initialise variables, structure and arrays */

        int nCols[n_instr]; /* Size of each data stream */
        int nRows[n_instr]; /* Check on orientation of data stream */
        int strLen = 0;     /* Counts number of column in array */
        int idx, loop, i;   /* Looping variables */

        /* Create and populate return strucuture */
        struct analysisData *new = malloc(sizeof(*new));

        const mxArray *strData = NULL; /* MEX specific variables for reading strings in */

        /* Initialise all arrays to NULL */
        for (idx = 0; i < MAX_DETECTORS; i++)
        {
                new->detName[idx] = NULL;
                new->series[idx] = NULL;
                new->nPSpectrum[idx] = NULL;
        }

        new->timeLags = NULL;
        new->nTimeLags = 1;

        /*
        * If either 1) or 3) are not cell arrays, need to use n_instr
        * to calc new positions, and calc new length.
        */

        /* Check number of input and output parameters */
        if (nrhs < 6)
        {
                mexErrMsgIdAndTxt("SphRadiometer:initiate:nrhs",
                                  "wrong number of inputs - need data and instrument name.\n"
                                  "sphrad(n_instr, {data1 [,data2, data3]}, analysisStruct, {dect1 [, dect2, dect3]}, antennaResponse, analysisType [,powerSpectrum, freqRange[2])");
        }

        if (nlhs > 3)
        {
                mexErrMsgIdAndTxt("SphRadiometer:initiate:nlhs",
                                  "Less than 3 outputs required.");
        }

        /* Check for cell arrays */
        if (!(mxIsCell(prhs[1])))
                mexErrMsgIdAndTxt("SphRadiometer:initiate:notCellArray",
                                  "Detector streams must be in a cell array.");

        if (!(mxIsCell(prhs[3])))
                mexErrMsgIdAndTxt("SphRadiometer:initiate:notCellArray",
                                  "Detector names must be in a cell array.");

        if (!(mxIsStruct(prhs[2])))
                mexErrMsgIdAndTxt("SphRadiometer:initiate:notStructure",
                                  "Analysis data must be specified in a structure.");

        /* Get how many detectors (i.e. data streams) are required */
        new->nDataStreams = mxGetNumberOfElements(prhs[1]);
        mexPrintf("Number of data streams: %i\n", new->nDataStreams);

        /* Get pointers to the data streams, and their lengths */
        for (idx = 0; idx < new->nDataStreams; idx++)
        {
                new->series[idx] = mxGetData(mxGetCell(prhs[1], idx));
                nCols[idx] = mxGetN(mxGetCell(prhs[1], idx));
                nRows[idx] = mxGetM(mxGetCell(prhs[1], idx));

                /* check that number of rows in data input argument is 1*/
                if (nRows[idx] != 1)
                        mexErrMsgIdAndTxt("SphRadiometer:initiate:notRowVector",
                                          "Input must be a row vector.");

                mexPrintf("\t%i)\tLength: %i samples\n", idx, nCols[idx]);
        }

        /* Check that the data streams are the same length */
        /* TO FIX : consistency check done only on first 2 detector ! */
        if ((nCols[0] != nCols[1]) || (nCols[0] != nCols[new->nDataStreams - 1]))
                mexErrMsgIdAndTxt("SphRadiometer:initiate:notSameLength",
                                  "Data steams must be the same length.");

        /* Retrieve the rest of the command line arguments */
        new->nInstr = mxGetNumberOfElements(prhs[3]);
        mexPrintf("Number of instruments: %d\n", new->nInstr);

        /* Start retrieve data from analysis structure */
        mxArray *structPtr;

        /* Length of data stream - in samples */
        new->lenDataStreams = nCols[1];

        /* FFT length - in seconds */
        structPtr = mxGetField(prhs[2], 0, "FFTlen");
        if (structPtr != NULL)
                new->fourierLen = mxGetScalar(structPtr);

        /* Sample spacing - in seconds*/
        structPtr = mxGetField(prhs[2], 0, "deltaT");
        if (structPtr != NULL)
                new->deltaT = mxGetScalar(structPtr);

        /* GPS time of start of block */
        structPtr = mxGetField(prhs[2], 0, "blockStartTime");
        if (structPtr != NULL)
                new->blockStartTime = mxGetScalar(structPtr);

        /* GPS time of end of analysis block */
        structPtr = mxGetField(prhs[2], 0, "blockStopTime");
        if (structPtr != NULL)
                new->blockStopTime = mxGetScalar(structPtr);

        /* GPS start time of segment */
        structPtr = mxGetField(prhs[2], 0, "segmentStartTime");
        if (structPtr != NULL)
                new->segmentStartTime = mxGetScalar(structPtr);

        /* Duration of segment */
        structPtr = mxGetField(prhs[2], 0, "segmentDuration");
        if (structPtr != NULL)
                new->segmentDuration = mxGetScalar(structPtr);

        /* Length of transients - in seconds*/
        structPtr = mxGetField(prhs[2], 0, "transientLen");
        if (structPtr != NULL)
                new->transientLen = mxGetScalar(structPtr);

        /* Length of one block (s)*/
        structPtr = mxGetField(prhs[2], 0, "blockLen");
        if (structPtr != NULL)
                new->blockLen = mxGetScalar(structPtr);

        /* Verbosity of output */
        structPtr = mxGetField(prhs[2], 0, "debugLevel");
        if (structPtr != NULL)
                new->debugLevel = mxGetScalar(structPtr);

        structPtr = mxGetField(prhs[2], 0, "freqRange");
        if (structPtr != NULL)
        {
                new->freqRange[0] = (int)round(mxGetPr(structPtr)[0]); /* First frequency value */
                new->freqRange[1] = (int)round(mxGetPr(structPtr)[1]); /* Second frequency value */

                if (new->freqRange[0] < 0)
                        new->freqRange[0] = 0;
                if ((new->freqRange[1] < 0) || (new->freqRange[1] > (new->fourierLen / new->deltaT)))
                        new->freqRange[1] = (int)round(new->fourierLen / new->deltaT) - 1;
        }
        else
        {
                new->freqRange[0] = 0;
                new->freqRange[1] = (int)round(new->fourierLen / new->deltaT) - 1;
        }

        /* Gaussian blurring of time-frequency clustering map. */
        structPtr = mxGetField(prhs[2], 0, "sphradBlurring");
        if (structPtr != NULL)
        {
                new->blurring[0] = mxGetPr(structPtr)[0]; /* signal blurring radius */
                new->blurring[1] = mxGetPr(structPtr)[1]; /* background blurring radius */
        }
        else
        {
                new->blurring[0] = 0.0;
                new->blurring[1] = 0.0;
        }

        structPtr = mxGetField(prhs[2], 0, "clusterThresholds");
        if (structPtr != NULL)
        {
                new->clusterThresholds[0] = mxGetPr(structPtr)[0]; /* First cluster value*/
                new->clusterThresholds[1] = mxGetPr(structPtr)[1]; /* Second cluster value */
        }

        structPtr = mxGetField(prhs[2], 0, "nRegions");
        if (structPtr != NULL)
                new->nRegions = mxGetScalar(structPtr); /* Number of regions to use for thresholding */
        else
                new->nRegions = 0;

        structPtr = mxGetField(prhs[2], 0, "regionDirection");
        if (structPtr != NULL)
                new->regionDirection = mxGetScalar(structPtr); /* Direction of regional thresholds */
        else
                new->regionDirection = 0;

        structPtr = mxGetField(prhs[2], 0, "codeVersion");
        if (structPtr != NULL)
                new->codeVersion = mxGetScalar(structPtr); /* Which clustering algorithm to use */
        else
                new->codeVersion = 0;

        structPtr = mxGetField(prhs[2], 0, "pixelDistance");
        if (structPtr != NULL)
                new->pixelDistance = mxGetScalar(structPtr); /* Maximum pixel distance from cluster */
        else
                new->pixelDistance = 1;

        structPtr = mxGetField(prhs[2], 0, "skyPos");
        if (structPtr != NULL)
        {
                new->skyPos[0] = mxGetPr(structPtr)[0]; /* Sky location to analyse for targeted search */
                new->skyPos[1] = mxGetPr(structPtr)[1];
        }
        else
        {
                new->skyPos[0] = -1e20;
                new->skyPos[1] = -1e20;
        }

        structPtr = mxGetField(prhs[2], 0, "skyPrior");
        if (structPtr != NULL)
                new->skyPrior = mxGetScalar(structPtr); /* Flag to specify the use of a prior on sky location for targeted searches */
        else
                new->skyPrior = 0;

        structPtr = mxGetField(prhs[2], 0, "skyPos2");
        if (structPtr != NULL)
                /* Sky location to analyse for targeted search */
                new->skyPos2 = mxGetPr(structPtr);
        else
                new->skyPos2 = NULL;

        structPtr = mxGetField(prhs[2], 0, "testLocalisation");
        if (structPtr != NULL)
                new->testLocalisation = mxGetScalar(structPtr); /* Flag to specify whether we are testing our sky localistation ability*/
        else
                new->testLocalisation = 0;

        /* Get string for output directory, if blank, set current */
        structPtr = mxGetField(prhs[2], 0, "outputDirectory");
        if (structPtr != NULL)
        {
                strLen = mxGetN(structPtr) + 1;
                new->outputDirectory = mxCalloc(strLen, sizeof(char));
                mxGetString(structPtr, new->outputDirectory, strLen);
        }

        structPtr = mxGetField(prhs[2], 0, "eOrder");
        if (structPtr != NULL)
        {
                new->eOrder[0] = mxGetPr(structPtr)[0]; /* Order of spherical harmonic expansion */
                new->eOrder[1] = mxGetPr(structPtr)[1];
                new->eOrder[2] = mxGetPr(structPtr)[2];
                new->eOrder[3] = mxGetPr(structPtr)[3];
        }
        else
        {
                new->eOrder[0] = -8;
                new->eOrder[1] = -8;
                new->eOrder[2] = -16;
                new->eOrder[3] = -18;
        }

        structPtr = mxGetField(prhs[2], 0, "followUps");
        if (structPtr != NULL)
                new->followUps = mxGetScalar(structPtr); /* number of clusters to follow up */

        /* Derived parameters */
        new->FFTLen = new->fourierLen / new->deltaT;     /* FFT length - in samples */
        new->sampleFrequency = round(1.0 / new->deltaT); /* Sample frequency - in Hz */

        new->freqRatio = new->sampleFrequency / new->FFTLen; /* ratio if FFT len != sample ferquency */

        /* Calculate the number of sections in the analysis for 50% overlap */
        new->nSections = new->lenDataStreams / new->FFTLen;
        new->nSections += ((double)new->lenDataStreams - ((double)new->FFTLen / 2.)) / new->FFTLen;

        /* antennaResponse is an option argument to control many, many things.
        * Too many in fact. Change to character input.
        * Anyhow, for now, it mainly controls which correlator to call:
        *   0 - normal
        *   1 - non-azimuthally symmetric
        *   2 - generate t-f mapping instead
        * Modulate the sky by the antenna reponse?
        * This is now the last but one input
        */
        new->antennaResponse = 0;

        /* Variable for the multi-resolution time-frequency map */
        new->tfScale = mxGetScalar(prhs[4]);

        /* Calculate the number of sections in the analysis for 50% overlap */
        new->aLen = malloc(/*new->tfScale*/ 4 * sizeof(new->aLen));
        for (idx = 0; idx < 4 /*new->tfScale*/; idx++)
        {
                new->aLen[idx] = floor(((double)new->lenDataStreams / (new->FFTLen / pow(2, idx))) + ((double)new->lenDataStreams - ((double)(new->FFTLen / pow(2, idx)) / 2.)) / (new->FFTLen / pow(2, idx)));
        }

        /* Retrieve instrument names */
        for (idx = 0; idx < new->nInstr; idx++)
        {
                new->detName[idx] = mxArrayToString(mxGetCell(prhs[3], idx));
                if (new->detName[idx] == NULL)
                        mexErrMsgIdAndTxt("SphRadiometer:initiate:notString",
                                          "Detector names must be string formatted");

                mexPrintf("DetName %d: %s\n", idx, new->detName[idx]);
        }

        /* Get string specifying analysis type */
        strData = prhs[5];
        strLen = mxGetN(strData) + 1;

        new->analysisType = mxCalloc(strLen, sizeof(char));
        mxGetString(strData, new->analysisType, strLen);

        /* Extract power spectrum */
        if (nrhs >= 7)
        {
                mexPrintf("Extracting power spectrum information\n");
                if (!(mxIsCell(prhs[6])))
                        mexErrMsgIdAndTxt("SphRadiometer:initiate:notCellArray",
                                          "Power spectrum must be specified as a cell array.");

                /* Make sure we have the same number of power spectrums as data streams */
                if (new->nDataStreams != mxGetNumberOfElements(prhs[6]))
                        mexErrMsgIdAndTxt("SphRadiometer:initiate:notSameSpectrumNumber",
                                          "Must have a power spectrum from each data channel.");

                for (idx = 0; idx < new->nDataStreams; idx++)
                {
                        new->nPSpectrum[idx] = mxGetData(mxGetCell(prhs[6], idx));
                        nCols[idx] = mxGetN(mxGetCell(prhs[6], idx));
                        nRows[idx] = mxGetM(mxGetCell(prhs[6], idx));

                        /* check that number of rows in data input argument is 1*/
                        if (nRows[idx] != 1)
                                mexErrMsgIdAndTxt("SphRadiometer:initiate:notRowVector",
                                                  "PowerSpectrum must be a row vector.");

                        mexPrintf("\t%i)\tLength: %i samples\n", idx, nCols[idx]);
                }

                /* Check that the data streams are the same length */
                if ((nCols[0] != nCols[1]) || (nCols[0] != nCols[new->nDataStreams - 1]))
                        mexErrMsgIdAndTxt("SphRadiometer:initiate:notSameLength",
                                          "Power spectrum vectors must be the same length.");
        }
        else
        {
                mexErrMsgIdAndTxt("SphRadiometer:initiate:notDefined",
                                  "Power spectrum not defined.");
        } /* End of Power Spectrum conditional */

        /* Get time lags */
        if ((nrhs >= 8) && (mxGetPr(prhs[7])))
        {
                mexPrintf("Extracting time lags\n");

                /* Get pointers to the data streams, and their lengths */
                new->timeLags = mxGetPr(prhs[7]);
                nCols[0] = mxGetN(prhs[7]);
                nRows[0] = mxGetM(prhs[7]);

                /* check that number of rows in data input argument is 1*/
                if (nRows[0] != 1)
                        mexErrMsgIdAndTxt("SphRadiometer:initiate:notRowVector",
                                          "Time lag must be a row vector.");

                mexPrintf("\tNumber of input lags: %i \n", nCols[0]);
                /* Verify that number of input lags is even (for two detectors). */
                /* if (nCols[0]%2)
                 *      mexErrMsgIdAndTxt("SphRadiometer:initiate:oddTimeLags","Must have even number of input lags.");
                 */
                new->nTimeLags = nCols[0];
                /* new->nTimeLags = (nCols[0]-2)*2+(nCols[0]-2); */
                mexPrintf("\tNumber of time shifts: %i \n", new->nTimeLags);

                if (new->nTimeLags <= 0)
                        mexErrMsgIdAndTxt("SphRadiometer:initiate:tooFewValues",
                                          "Not enough time lag values to generate a time shift. \nUsually happens when 2 are specifed, need a minimum of 3\n");
        }

        /* Zero sky max value */
        new->sky_l_max = 0;

        /* Dump out input parameters */
        mexPrintf("Input analysis type %s\n", new->analysisType);
        mexPrintf("Data block start time: %g and duration: %g (and end time: %g)\n", new->blockStartTime, new->blockLen, new->blockStopTime);
        mexPrintf("Time series length: \t%d samples\nFourier length: \t%f seconds (%d samples))\nData spacing: \t%f\n\n", new->lenDataStreams, new->fourierLen, new->FFTLen, new->deltaT);

        return new;
}

/* Plot out the data inside the analysisData structure */
void print_analysisData(struct analysisData *new)
{
        int i;

        mexPrintf("SphRad is configured as follows:\n");
        mexPrintf("General settings:\n");
        mexPrintf("\tnInstr:            %d\n", new->nInstr);
        mexPrintf("\tnDataStreams:      %d\n", new->nDataStreams);
        mexPrintf("\tlenDataStreams:    %d sample(s)\n", new->lenDataStreams);
        mexPrintf("\tblockStartTime:    %g seconds\n", new->blockStartTime);
        mexPrintf("\tblockLen:          %g second(s)\n", new->blockLen);
        mexPrintf("\tblockStopTime:     %g seconds\n", new->blockStopTime);
        mexPrintf("\tsegmentStartTime:  %g seconds\n", new->segmentStartTime);
        mexPrintf("\tsegmentDuration:   %g second(s)\n", new->segmentDuration);
        mexPrintf("\ttransientLen:      %g second(s)\n", new->transientLen);
        mexPrintf("\tfourierLen:        %g second(s)\n", new->fourierLen);
        mexPrintf("\tFFTLen:            %d sample(s)\n", new->FFTLen);
        mexPrintf("\tfreqRatio:	 %f \n", new->freqRatio);
        mexPrintf("Clustering settings\n");
        mexPrintf("\tclusterThresholds: (%g, %g)\n", new->clusterThresholds[0], new->clusterThresholds[1]);
        mexPrintf("\tnRegions:		 %d \n", new->nRegions);
        mexPrintf("\tregionDirection:	 %d \n", new->regionDirection);
        mexPrintf("\tcodeVersion:	 %d \n", new->codeVersion);
        mexPrintf("\tpixelDistance:	 %d \n", new->pixelDistance);

        mexPrintf("Sky localisation and pointing settings\n");
        mexPrintf("\tskyPos:		(%g, %g)\n", new->skyPos[0], new->skyPos[1]);
        if (new->skyPrior == -1)
                mexPrintf("\tskyPrior:		 True\n");
        else
        {
                mexPrintf("\tskyPrior:		 False\n");
                mexPrintf("\tskyRegions:	(%d regions)\n", new->skyPrior);
        }

        if (new->skyPos2 != NULL)
                for (i = 0; i < new->skyPrior; i++)
                        mexPrintf("\tsky region %d:    (%g -> (%g, %g))\n", i, new->skyPos2[0 + i * 3], new->skyPos2[1 + i * 3], new->skyPos2[2 + i * 3]);

        if (new->testLocalisation == 0)
                mexPrintf("\ttestLocalisation:		False\n	");
        else
                mexPrintf("\ttestLocalisation:		True\n");
        for (i = 0; i < new->testLocalisation; i++)
                mexPrintf("\t   %d:    (%g -> (%g, %g))\n", i, new->skyPos2[0 + i * 3], new->skyPos2[1 + i * 3], new->skyPos2[2 + i * 3]);

        mexPrintf("Resolution settings\n");
        mexPrintf("\teOrder:		(%d, %d, %d, %d)\n", new->eOrder[0], new->eOrder[1], new->eOrder[2], new->eOrder[3]);

        mexPrintf("Derived settings\n");
        mexPrintf("\tnSections:	 %d \n", new->nSections);
        mexPrintf("\tfs:                %d Hz\n", new->sampleFrequency);
        mexPrintf("\tdeltaT:            %g second(s)\n", new->deltaT);
        mexPrintf("\tsky l_max:         %d\n", new->sky_l_max);
        mexPrintf("\tdebugLevel:        %d\n", new->debugLevel);

        mexPrintf("\tnTimeLags:         %d\n", new->nTimeLags);

        mexPrintf("\toutputDir:	      %s\n", new->outputDirectory);
        mexPrintf("\tanalysis Type:     %s\n", new->analysisType);
        mexPrintf("\tantenna flag:      %d\n", new->antennaResponse);

        /* Assign pointers for data and detector names */
        mexPrintf("\tDetector names:\n\t(");
        for (i = 0; i < new->nInstr; i++)
                mexPrintf(" %s ", new->detName[i]);
        mexPrintf(")\n");
}

/* Free memory in analysisData */
void analysisData_free(struct analysisData *aData)
{
        int idx;

        for (idx = 0; idx < aData->nInstr; idx++)
                mxFree(aData->detName[idx]);

        free(aData->aLen);

        mxFree(aData->analysisType);
        mxFree(aData->outputDirectory);
        free(aData);
}

/* Deallocates memory used during loop */
static void myExitFcn()
{
        mexPrintf("\nWarning, MEX-file is being unloaded, clearing persistant data products.\n");
        int idx;
        if (fdplans)
        {
                user_correlator_network_plan_fd_free(fdplans);
                mexPrintf("Done fdplans\n");
        }

        if (instruments)
        {
                for (idx = 0; idx < baselines->n_instruments; idx++)
                        instrument_free(instruments[idx]);
                free(instruments);
                mexPrintf("Done instruments\n");
        }

        if (baselines)
        {
                correlator_network_allbaselines_free(baselines);
                mexPrintf("Done baselines\n");
        }
}