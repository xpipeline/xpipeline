/*
 * Copyright (C) 2010  Mark Edwards
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef __SPHRAD_COHERENT_H__
#define __SPHRAD_COHERENT_H__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>
#include <correlator.h>

#include "sphrad.h"


/* Debugging specific includes */
#ifdef MEMWATCH
#include "memwatch.h"
#endif

double *singlepolarization(int, double *, complex double *);
double *pre_singlepolarization(int , double *,  double *);
complex double *pixelPower(int *, int , int , int , int ,struct analysisData *);


#endif  /* __SPHRAD_COHERENT_H__ */

