/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __SPHRAD_STAT_H__
#define __SPHRAD_STAT_H__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include "mex.h"
#include "sphrad.h"
#include "sphrad_misc.h"
#include "sphrad_skymap.h"

#include <correlator.h>
#include <instrument.h>
#include <sh_series.h>
#include <instruments.h>

double genTik(double, double, double, double, double);
// double *calc_stats(struct sh_series **, struct detector_response *, int, char *, int, double);
double *grid_calc_stats(struct sh_series **, struct detector_response_grid *, struct analysisData *,int , char *, FILE *, double);

#endif  /* __SPHRAD_MISC_H__ */
