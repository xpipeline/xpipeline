/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "sphrad_skymap.h"

/* Debugging specific includes*/
#include "memwatch.h"

/* Function to generate the sky position with which to restrict the analysis.
 *  - takes the sky position time map for this segment, and the centre time of this trigger
 *  	sky_position - pointer to standard sky position array (theta, phi)
 *	sky_timeline - pointer to previously computed time/sky position map (gps_time, phi, theta)
 *	nPos	     - number of sky position 'regions' in our map
 *	trigger_time - centre time of trigger 
 */

int sky_pointing(double *sky_position, double *sky_timeline, int nPos, double trigger_time)
{
  int regionIdx=0;
/*
  for (regionIdx=0; regionIdx < nPos; regionIdx++)
	mexPrintf("Region %d/%d: %f ->(%f,%f)\n",regionIdx+1, nPos,sky_timeline[0+regionIdx*3],sky_timeline[1+regionIdx*3],sky_timeline[2+regionIdx*3]);
*/

  /* sky_timeline has the format (time, theta, phi) for each sky position */     
  mexPrintf("Time: %g -> ",trigger_time);
  for (regionIdx=0; regionIdx < (nPos-1); regionIdx++)
  {
	mexPrintf("\n\t %g-%g - ",sky_timeline[regionIdx*3], sky_timeline[3+regionIdx*3]);
    if ((trigger_time > sky_timeline[regionIdx*3])&&(trigger_time < sky_timeline[3+regionIdx*3]))
     {
	mexPrintf("is in: %g -> %g, so using (%g, %g) as sky position\n", sky_timeline[regionIdx*3], sky_timeline[3+regionIdx*3],sky_timeline[1+regionIdx*3], sky_timeline[2+regionIdx*3]);
        sky_position[1] = sky_timeline[1+regionIdx*3];
        sky_position[0] = sky_timeline[2+regionIdx*3];
	return 0;
     }else {
	mexPrintf("not within (%d): \n", regionIdx);
     }
   }

mexPrintf("is in: %g -> end, so using (%g, %g) as sky position\n", sky_timeline[(nPos-1)*3], sky_timeline[1+(nPos-1)*3], sky_timeline[2+(nPos-1)*3]);
sky_position[1] = sky_timeline[1+(nPos-1)*3];
sky_position[0] = sky_timeline[2+(nPos-1)*3];
return 0; 
}


/* Calculate the most likely sky position from a skymap 
 * As the following procedure may return more than one sky 
 * position, need to choose the most likely direction
 * There is a few ways to do this:
 *  - find most energetic pixel in each cluster, compare and return position of highest
 *  - compare cluster energies, return position of centre pixel of highest, 
 *  - calculate average pixel energy for cluster, compare and return centre pixel of highest
 * 
 */

// double *sky_position(double *skyMap, int xSize, int ySize, int blockStartTime, double triggerTime)
// {
//   double *sortedArray 	= NULL;
//   double threshold[2];
 
//   int gridIdx		= 0;
//   int nElements		= 0;
//   int numClusters 	= 0;
//   int *clusterMap 	= NULL;
//   double *clusterStruct = NULL;
  
//   double *skyPosition;
  
//   skyPosition = (double *) malloc(sizeof(double)*3);
//   sortedArray = (double *) malloc(xSize*ySize*sizeof(double));    
  
//   if ((skyPosition ==NULL) || (sortedArray == NULL))
//   {
//     fprintf(stderr, "MEMORY ERROR: Unable to allocate memory in skymap\n");
//     return NULL;
//   }

   
//   /**********************************************************************
//    *  Everything so far has been in an Earth-fixed co-ordinate system
//    *  to get an accurate/the sky location, we need to project the skymap 
//    *  onto a sky-fixed co-ordinate sytem using the analysis time
//    **********************************************************************/
//    if (blockStartTime != -1)
//    {
//       mexPrintf( "\n\tRotating sky\n");
//       double *rotGrid;
//       rotGrid = gps_rotate(skyMap, xSize,  blockStartTime, triggerTime);
            
//         /* Get max direction by just finding maximum! */
// 	int xIdx, yIdx, maxX=0, maxY=0;
// 	double maxValue=0;
// 	for (yIdx=0; yIdx<ySize; yIdx++)
// 	  for (xIdx=0; xIdx<xSize; xIdx++)
// 	  {
// 	    skyMap[yIdx*ySize+xIdx] = rotGrid[yIdx*ySize+xIdx];
// 	    if (skyMap[yIdx*ySize+xIdx] > maxValue)
// 	    {
// 	      maxValue = skyMap[yIdx*ySize+xIdx];
// 	      maxX = xIdx;
// 	      maxY = yIdx;
// 	    }
// 	  }

// 	mexPrintf( "\t    MAX -> Rotated Sky position for Esl:  (%d, %d) ->E %g\n",maxX, maxY, creal(maxValue));  
//    }
   
//     /* Number of sky pixels */
//     nElements = xSize*ySize;

//     /* First and foremost, we need to cluster on the skymap 
//     * Need to copy in array so that we can sort it without affecting 
//     *  the original
//     */
//     memcpy(sortedArray, skyMap,nElements*sizeof(double));

//     /* Generate the threshold values for the required percentages:   
//     *  - a high one to only pick out the most probable sky pixels, 
//     *	then a lower one to pick up some of the surrounding pixels
//     */
//     threshold[0] = prcValue(sortedArray, .999, nElements,1);
//     threshold[1] = prcValue(sortedArray, .96, nElements,0);
    
//     if (isnan(threshold[0]) || isnan(threshold[1]) || (threshold[0] == 0))
//     {
// 	mexPrintf( "NUMERICAL ERROR: Thresholding has failed in skymap\n");
// 	mexPrintf( "NUMERICAL ERROR: Unable to calculate sky location\n");
// 	skyPosition[0]=0; skyPosition[1]=0;skyPosition[2]=0;
// 	return NULL;
//     }
    
//     /* Cluster on the skymap */
//     numClusters = pixel_chain(&clusterMap, &clusterStruct, skyMap, xSize, ySize, threshold, 0, 0, 0);
   
//    /* Check that we found a direction */
//   if (numClusters == 0)
//    {
//       mexPrintf( "NUMERICAL ERROR: Clustering has failed in skymap\n");
//       mexPrintf( "NUMERICAL ERROR: Unable to calculate sky location\n");
//       skyPosition[0]=0; skyPosition[1]=0;skyPosition[2]=0;
//       return NULL;
//    }
   
//   /* Now, we have a few items that we can use to determine the direction of most interest
//    *  - cluster size, max pixel pixel and max cluster power
//    * We will test with max single pixel - isn't this just the same as getting the max for the skymap
//    * though? Should do something a little cleverer here!
//    */
  
//   /* Sort clusterStruct by max single pixel power, and pop off top cluster */
//   qsort(clusterStruct, numClusters, NUM_CLUSTER_ELEMENTS*sizeof(double), cmpMultiDoubleMP);

//   mexPrintf( "\t  Cluster information for max sky position: \n");
//   mexPrintf( "\t    Position: (%g, %g) - > %g\n", clusterStruct[7], clusterStruct[8], clusterStruct[9]);
   
//   /* Assign sky position */
//   skyPosition[0] = clusterStruct[7];
//   skyPosition[1] = clusterStruct[8];
//   /* And power */
//   skyPosition[2] = clusterStruct[9];
  
//   /* free allocated memory */
//   free(sortedArray);
//   free(clusterMap);
//   free(clusterStruct);
  
// return skyPosition;
// }

/* Walk over the skyMap and find the location of the maximum */
double *max_skyGrid(double *skyMap, int xSize, int ySize)
{
double maxValue=0;
int maxX=0;
int maxY=0;
int xIdx, yIdx;
double *skyPosition;
  
skyPosition = (double *) malloc(sizeof(double)*5);

/* Get location of the maximum value */

for (yIdx=0; yIdx<ySize; yIdx++)
  for (xIdx=0; xIdx<xSize; xIdx++)
  {
    if (skyMap[yIdx*xSize+xIdx] > maxValue)
    {
      maxValue = skyMap[yIdx*xSize+xIdx];
      maxX = xIdx;
      maxY = yIdx;
    }
  }
  
skyPosition[0] = maxY;
skyPosition[1] = maxX;
skyPosition[2] = maxValue;
skyPosition[3] = 0;
skyPosition[4] = 0;

return skyPosition;
}

/* Find maximum of skyMap, and calculate how much of the map has a value greater than the true location of any signal */
double *skyGrid_searched(double *skyMap, int xSize, int ySize, struct analysisData *inData)
{
double maxValue=0;
double currentValue=0;
int maxX=0;
int maxY=0;
int xIdx, yIdx;
int searchArea = 0;

/* calculate grid point relating to injection */
int thetaIdx = (int)round(((inData->skyPos[0])/M_PI)*(2.*(inData->sky_l_max+1)));
int phiIdx   = (int)round(((inData->skyPos[1])/(2.*M_PI))*(2.*(inData->sky_l_max+1)));
double trueValue = skyMap[(int) round(thetaIdx*xSize+phiIdx)];

double *skyPosition;
skyPosition = (double *) malloc(sizeof(double)*5);

/* Get location of the maximum value and count any pixels greater than true position */
for (yIdx=0; yIdx<ySize; yIdx++)
  for (xIdx=0; xIdx<xSize; xIdx++)
  {
    currentValue = skyMap[yIdx*xSize+xIdx];

    if (currentValue >= trueValue)
	 searchArea++;

    if (currentValue > maxValue)
    {
      maxValue = skyMap[yIdx*xSize+xIdx];
      maxX = xIdx;
      maxY = yIdx;
    }
  }
  
skyPosition[0] = maxY;
skyPosition[1] = maxX;
skyPosition[2] = maxValue;
skyPosition[3] = searchArea;
skyPosition[4] = xSize*ySize;

mexPrintf("\ttrue value (%g) at (%g, %g), found value (%g) at (%g, %g), search area (%d/%d)\n", skyMap[(int) round(skyPosition[0]*xSize+skyPosition[1])], inData->skyPos[0], inData->skyPos[1], skyPosition[2], M_PI*(skyPosition[0]/(2.*(inData->sky_l_max+1))), 2.*M_PI*(skyPosition[1]/(2.*(inData->sky_l_max+1))), (int) round(skyPosition[3]), (int) round(skyPosition[4]));

return skyPosition;
}


/* Calculate the most likely sky position from a skymap 
 * As the following procedure may return more than one sky 
 * position, need to choose the most likely direction
 * There is a few ways to do this:
 *  - find most energetic pixel in each cluster, compare and return position of highest
 *  - compare cluster energies, return position of centre pixel of highest, 
 *  - calculate average pixel energy for cluster, compare and return centre pixel of highest
 * 
 */

double *SH_sky_position(double *skyMap, int xSize, int ySize, struct analysisData *inData)
{
  double *sortedArray 	= NULL;
  double threshold[2];
 
  int nElements		= 0;
  int numClusters 	= 0;
  int *clusterMap 	= NULL;
  double *clusterStruct = NULL;
  
  double *skyPosition;
  
  skyPosition = (double *) malloc(sizeof(double)*3);
  sortedArray = (double *) malloc(xSize*ySize*sizeof(double));    
  
  if ((skyPosition ==NULL) || (sortedArray == NULL))
  {
    fprintf(stderr, "MEMORY ERROR: Unable to allocate memory in skymap\n");
    return NULL;
  }

   /* Number of sky pixels */
   nElements = xSize*ySize;

  /* First and foremost, we need to cluster on the skymap 
   * Need to copy in array so that we can sort it without affecting 
   *  the original
   */
   memcpy(sortedArray, skyMap,nElements*sizeof(double));

  /* Generate the threshold values for the required percentages:   
   *  - a high one to only pick out the most probable sky pixels, 
   *	then a lower one to pick up some of the surrounding pixels
   */
   threshold[0] = prcValue(sortedArray, .999, nElements,1);
   threshold[1] = prcValue(sortedArray, .96, nElements,0);
   
   if (isnan(threshold[0]) || isnan(threshold[1]) || (threshold[0] == 0))
   {
      mexPrintf( "NUMERICAL ERROR: Thresholding has failed in skymap\n");
      mexPrintf( "NUMERICAL ERROR: Unable to calculate sky location\n");
      skyPosition[0]=0; skyPosition[1]=0;skyPosition[2]=0;
      return skyPosition;
   }


   mexPrintf( "\t  In SKY: \n");

  /* Cluster on the skymap */
   numClusters = pixel_chain(&clusterMap, &clusterStruct, skyMap, xSize, ySize, threshold, 0, 0, 0);

  /* Now, we have a few items that we can use to determine the direction of most interest
   *  - cluster size, max pixel pixel and max cluster power
   * We will test with max single pixel - isn't this just the same as getting the max for the skymap
   * though? Should do something a little cleverer here!
   */
  
  /* Sort clusterStruct by max single pixel power, and pop off top cluster */
   qsort(clusterStruct, numClusters, NUM_CLUSTER_ELEMENTS*sizeof(double), cmpMultiDoubleMP);

   mexPrintf( "\t  Cluster information for max sky position: \n");
   mexPrintf( "\t    Position: (%g, %g) - > %g\n", clusterStruct[7], clusterStruct[8], clusterStruct[9]);
   
  /* Assign sky position */
  skyPosition[0] = clusterStruct[7];
  skyPosition[1] = clusterStruct[8];
  /* And power */
  skyPosition[2] = clusterStruct[9];
  
  /* free allocated memory */
  free(sortedArray);
  free(clusterMap);
  free(clusterStruct);
  
return skyPosition;
}

