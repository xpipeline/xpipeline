
#ifndef __SPHRAD_SKYMAP_H__
#define __SPHRAD_SKYMAP_H__

/* General includes */
#include <complex.h>
#include <math.h>

/* SH series functions and typedefs*/
#include <sh_series.h>

/* Definition for structure to hold analysis data */
#include "sphrad.h"
#include "sphrad_clust.h"
#include "sphrad_misc.h"

  
int sky_pointing(double *, double *, int, double);
// double *sky_position(double *, int , int , int , double );
double *max_skyGrid(double *, int, int );
double *skyGrid_searched(double *, int, int, struct analysisData *);

#endif  /* __SPHRAD_SKYMAP_H__ */

