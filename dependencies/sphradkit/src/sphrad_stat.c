/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include "sphrad_stat.h"

/* Debugging specific includes*/
#include "memwatch.h"

static int loop = 0;

/* Function to generate regulariser 
 * 	pStat - power in plus polarisation
 *	xStat - power in cross polarisation
 *	sumP - |Fp|^2
 *      sumX - |Fx|^2
 * 	amplitude - value to regularise to
 */
double genTik(double pStat, double xStat, double magP, double magX, double amp)
{

double ampamp = amp*amp;
double invamp = pow(ampamp, -1);

double pCom = pStat * magP;
double xCom = xStat * magX;

double top = pCom/(magP+invamp) + xCom/(magX+invamp);

double bottom = sqrt((magP*(ampamp))+1) * sqrt((magX*(ampamp))+1);

double tik=top-log(bottom);

return tik;
}


/* Frequency regulated Tikhonov statistic
 *  - the regulariser is made using a fixed SNR rather than a fixed strain.
 * Thus, delta is replaced by:
 *    delta^2 -> gamma^2 * S(f)
 *
 * where S(f) is averaged over all the detectors.  
 * Thusly:
 *    S = 1 / (1/S_1 + 1/S_2 + 1/S_3)
 *
 * Then, the prefactor becomes:
 *    (ep^-1+gamma^-2 * S^-1)^-1
 *
 * The physical interpretation here is that all high-frequency signals tend to have higher amplitudes, 
 * scaled exactly so that all frequencies are equally likely to get a detectable signal.   
 * This does not penalise one frequency over any another (however, you do still penalize sky positions with low F_+, F_x).

An advantage of this approach is that you can probably get away with a single value for gamma, gamma = a few.  You'd need to test different values to see what works well.

I have not tried this regularizer, but I'm curious how it would work.  I think cWB does something vaguely similar to this; i.e., regularizing relative to the noise floor rather than to absolute strain.




/* Now, to form the standard likelihood, we need to apply the antenna response function
 * to the cross-power and weight using the noise power spectrum :
 *   - calculate Esl((x,+),psi)=abs(sum(e(x,+).d(psi))^2
 *   - For three detectors:
 *	F1(x,+).d(1).F1(x,+).d(1) + F1(x,+).d(1).F2(x,+).d(2) + F1(x,+).d(1).F3(x,+).d(3)
 *    + F2(x,+).d(2).F1(x,+).d(1) + F2(x,+).d(2).F2(x,+).d(2) + F2(x,+).d(2).F3(x,+).d(3)
 *    + F3(x,+).d(3).F1(x,+).d(1) + F3(x,+).d(3).F2(x,+).d(2) + F3(x,+).d(3).F3(x,+).d(3)
 * Where Fn(x,+) = fn(x,+)/|f(x,+)|
 *
 * So:  sky holds the cross-power between each set of detectors over the , 
 *	response holds the antenna response for each set of detectors,
 * 	analysis holds the noise power spectrum for each of the detectors
 */



double *calc_stats_again(struct sh_series **skymap, struct detector_response *response, int clusterIdx, char *matFileName)
{
/* Given the above skymap and antenna responses, generate the statistics required  
 * Skymap holds the spherical harmonic expansion for each detector pair over the time range of
 * interest,while 
 * Reponse holds the spherical harmonic expansion for each polarisation and detector pairing
 */

int pairIdx, gridIdx;
int gridSize;

complex double *skyMap;
double *skyMapOut;
char *skyMapName;

double *energies;
complex double *Esl, *Ep, *Ex;
complex double *temp;
complex double *antenna[response[0].nBaselines];

/* width/height of skymap */
gridSize = (skymap[0]->l_max+1)*2;

/* Pixel skymap */
energies = (double *) malloc (sizeof(double)*5);
Esl = (complex double *) malloc(gridSize*2*sizeof(complex double));
Ex  = (complex double *) malloc(gridSize*2*sizeof(complex double));
Ep  = (complex double *) malloc(gridSize*2*sizeof(complex double));

temp = (complex double *) malloc(gridSize*2*sizeof(complex double));

/* Allocate memory for the results */
skyMap     = (complex double *) malloc(gridSize*2*sizeof(complex double));
skyMapOut  = (double *) malloc(gridSize*2*sizeof(double));
skyMapName = (char *) malloc(30*sizeof(char));

/* Memory allocated? */
if (!energies || !skyMap || !skyMapOut || !skyMapName || !Esl || !Ex || !Ep || !temp)
{
  fprintf(stderr, "\tMEM ERROR: Cannot allocate memory for basic variables (Ep, Esl etc)\n");
  return NULL;
}

/* Each antenna response */
for (pairIdx=0;pairIdx<response[0].nBaselines;pairIdx++)
{
  antenna[pairIdx] = (complex double *) malloc (gridSize*2*sizeof(complex double));
  if (!(antenna[pairIdx]))
  {
    fprintf(stderr, "\tMEM ERROR: Cannot allocate memory for stat generation (antenna pattern)\n");
    for (gridIdx=0;gridIdx<pairIdx;gridIdx++)
      free(antenna[gridIdx]);
    return NULL;  
  }
  
  FSHT_inverse(antenna[pairIdx], response[pairIdx].plus, response[pairIdx].plus->l_max+1);
  
  
}

/* Transform to the spatial domain to form the statistics */
for (pairIdx = 0; pairIdx < response[0].nBaselines ;pairIdx++)
{
  /* Generate skymap from coefficients */
  FSHT_inverse(temp, skymap[pairIdx], skymap[pairIdx]->l_max+1);
  
/*  temp1 = convolve_sh_series(skymap[pairIdx], response[pairIdx].plus);
  temp2 = convolve_sh_series(skymap[pairIdx], response[pairIdx].cross);
  
  if ((temp1 == NULL) || (temp2 == NULL))
    return NULL;
  
  sh_series_add(Ep,  1.0, temp1);
  sh_series_add(Ex,  1.0, temp2);
  
  sh_series_add(Esl, 1.0, Ep);
  sh_series_add(Esl, 1.0, Ex);*/
}
}



// double *calc_stats(struct sh_series **skymap, struct detector_response *response, int clusterIdx, char *matFileName, int blockStartTime, double triggerTime)
// {
// /* Given the above skymap and antenna responses, generate the statistics required  
//  * Skymap holds the spherical harmonic expansion for each detector pair over the time range of
//  * interest,while 
//  * OLD - Response holds the spherical harmonic expansion for each polarisation and detector pairing
//  * NEW - response is already a mapping - holds response for each single detector, unmodified.
//  */
// int pairIdx, gridIdx, i;
// int gridSize;
// struct sh_series *Ep, *Ex, *Esl, *Enull;
// struct sh_series *Ip, *Ix, *Isl, *Inull;

// struct sh_series *total, *temp1, *temp2, *test;

// complex double *skyMap;
// double *skyMapOut;
// char *skyMapName;
// double *temp;
// double *energies;
// /* Energy expansions */
// gridSize = (skymap[0]->l_max+1)*2;

// Ep  = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);
// Ex  = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);
// Esl = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);
// Enull = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);

// Ip  = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);
// Ix  = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);
// Isl = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);
// Inull = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);

// total = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);

// temp1 = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);
// temp2 = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);

// test = sh_series_new_zero(skymap[0]->l_max, skymap[0]->polar);

// /* Pixel skymap */
// energies = (double *) malloc (sizeof(double)*10);
// skyMap = (complex double *)malloc((Esl->l_max+1)*4*(Esl->l_max+1)*sizeof(complex double));
// skyMapOut = (double *)malloc((Esl->l_max+1)*4*(Esl->l_max+1)*sizeof(double));
// skyMapName = (char *)malloc(30*sizeof(char));

// /* Memory allocated? */
// if (!(energies || skyMap || skyMapOut || skyMapName || total || Ep || Ex || Esl || Enull || Ip || Ix || Isl || Inull))
// {
//   fprintf(stderr, "MEM ERROR: Cannot allocate memory for stat generation\n");
//   return NULL;
// }

// /* Form the statistics */
// /* Cross terms */
// for (pairIdx = 0; pairIdx < response[0].nBaselines-response[0].nInstr ;pairIdx++)
// {
//   temp1 = convolve_sh_series(skymap[pairIdx], response[pairIdx].plus);
//   temp2 = convolve_sh_series(skymap[pairIdx], response[pairIdx].cross);
  
//   if ((temp1 == NULL) || (temp2 == NULL))
//     return NULL;
  
//   /* Each cross-term appears twice i.e. HL and LH */
//   sh_series_add(Ep,  2.0, temp1);
//   sh_series_add(Ex,  2.0, temp2);  
// }


// /* Auto terms - only appear once as they lie on the diagonal */
// for (pairIdx = response[0].nBaselines-response[0].nInstr; pairIdx < response[0].nBaselines ;pairIdx++)
// {
//   temp1 = convolve_sh_series(skymap[pairIdx], response[pairIdx].plus);
//   temp2 = convolve_sh_series(skymap[pairIdx], response[pairIdx].cross);
  
//   if ((temp1 == NULL) || (temp2 == NULL))
//     return NULL;

//   /* Add auto into the coherent energy sum */
//   sh_series_add(Ep,  1.0, temp1);
//   sh_series_add(Ex,  1.0, temp2);

//   /* Incoherent */
//   sh_series_add(Ip, 1.0, temp1);
//   sh_series_add(Ix, 1.0, temp2);

//   /* Sum diagonal to get total energy */
//   sh_series_add(total, 1.0, skymap[pairIdx]);

// }

// /* Form (in)coherent standard likelihood 
//  * by adding plus and cross energies
//  */
// sh_series_add(Esl, 1.0, Ep);
// sh_series_add(Esl, 1.0, Ex);

// sh_series_add(Isl, 1.0, Ip);
// sh_series_add(Isl, 1.0, Ix);
  
// /* Null energies - (total energies -standard likelihood) */
// sh_series_add(Enull, 1.0, total);
// sh_series_add(Inull, 1.0, total);

// sh_series_sub(Enull, 1.0, Esl);
// sh_series_sub(Inull, 1.0, Isl);

// if (0)
// {
//   sprintf(skyMapName, "Ep_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Ep,sh_series_length(Ep->l_max,Ep->polar) ,1,2);

//   sprintf(skyMapName, "Ip_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Ip,sh_series_length(Ip->l_max,Ip->polar) ,1,2);

//   sprintf(skyMapName, "Ex_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Ex,sh_series_length(Ex->l_max,Ex->polar) ,1,2);

//   sprintf(skyMapName, "Ix_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Ix,sh_series_length(Ix->l_max,Ix->polar) ,1,2);

//   sprintf(skyMapName, "Esl_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Esl,sh_series_length(Esl->l_max,Esl->polar) ,1,2);

//   sprintf(skyMapName, "Isl_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Isl,sh_series_length(Isl->l_max,Isl->polar) ,1,2);

//   sprintf(skyMapName, "Enull_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Enull,sh_series_length(Enull->l_max,Enull->polar) ,1,2);

//   sprintf(skyMapName, "Inull_coeffs%d", (clusterIdx+1));
//   product_output(matFileName, skyMapName, Inull,sh_series_length(Inull->l_max,Inull->polar) ,1,2);
// }

// /* Generate skymap from coefficients */
// FSHT_inverse(skyMap, Esl->coeff, Esl->l_max+1);

// for (gridIdx=0;gridIdx<(4*(Esl->l_max+1)*(Esl->l_max+1));gridIdx++)
//   skyMapOut[gridIdx] = creal(skyMap[gridIdx]);

// /* Output (+,x) energies */
// sprintf(skyMapName, "Ep_coeffs%d", (clusterIdx+1));
// product_output(matFileName, skyMapName, Ep,sh_series_length(Ep->l_max,Ep->polar) ,1,2);

// sprintf(skyMapName, "Ex_coeffs%d", (clusterIdx+1));
// product_output(matFileName, skyMapName, Ex,sh_series_length(Ex->l_max,Ex->polar) ,1,2);

// /* Output standard likelihood over sky */
// sprintf(skyMapName, "Esl_skymap%d", (clusterIdx+1));
// product_output(matFileName, skyMapName, skyMapOut,2*(Esl->l_max+1) ,2*(Esl->l_max+1),1);



// /* Determine most likely sky position from skymap */
// temp = sky_position(skyMapOut, 2*(Esl->l_max+1) ,2*(Esl->l_max+1), blockStartTime, triggerTime);
// if (temp == NULL)
// {
//   for (i=0;i<10;i++)
// 	energies[i]= 0;

//   free(skyMap);
//   free(skyMapOut);
//   free(skyMapName);

//   sh_series_free(Ep);
//   sh_series_free(Ex);
//   sh_series_free(Esl);

//   sh_series_free(Ip);
//   sh_series_free(Ix);
//   sh_series_free(Isl);

//   sh_series_free(temp1);
//   sh_series_free(temp2);
 
//   free (temp);

//   return energies;
// }

// mexPrintf( "\t    Sky position for Esl:  (%g, %g) ->E %g\n",temp[0], temp[1], creal(temp[2]));

// /* Output */
// energies[0] = temp[0];	/* theta */
// energies[1] = temp[1];	/* phi */
// energies[2] = temp[2];	/* Esl */

// FSHT_inverse(skyMap, Ep->coeff, Ep->l_max+1);
// energies[3] = skyMap[(int) round(temp[0]*2*(Ep->l_max+1)+temp[1])];	/* Ep */

// FSHT_inverse(skyMap, Ex->coeff, Ex->l_max+1);
// energies[4] = skyMap[(int) round(temp[0]*2*(Ex->l_max+1)+temp[1])];	/* Ex */

// /* Incoherent */
// FSHT_inverse(skyMap, Ip->coeff, Ip->l_max+1);
// energies[5] = skyMap[(int) round(temp[0]*2*(Ip->l_max+1)+temp[1])];	/* Ip */

// FSHT_inverse(skyMap, Ix->coeff, Ix->l_max+1);
// energies[6] = skyMap[(int) round(temp[0]*2*(Ix->l_max+1)+temp[1])];	/* Ix */

// FSHT_inverse(skyMap, Isl->coeff, Isl->l_max+1);
// energies[7] = skyMap[(int) round(temp[0]*2*(Isl->l_max+1)+temp[1])];	/* Isl */

// /* Null (total - standard)*/
// FSHT_inverse(skyMap, Enull->coeff, Enull->l_max+1);
// energies[8] = skyMap[(int) round(temp[0]*2*(Enull->l_max+1)+temp[1])];	/* Enull */

// FSHT_inverse(skyMap, Inull->coeff, Inull->l_max+1);
// energies[9] = skyMap[(int) round(temp[0]*2*(Inull->l_max+1)+temp[1])];	/* Inull */

// for (i=0;i<9;i++)
//   if (check_isnan(energies[i]) || check_isinf(energies[i]))
//     energies[i]=0.0;

// /* Free memory */
// free(skyMap);
// free(skyMapOut);
// free(skyMapName);

// sh_series_free(Ep);
// sh_series_free(Ex);
// sh_series_free(Esl);

// sh_series_free(Ip);
// sh_series_free(Ix);
// sh_series_free(Isl);


// sh_series_free(temp1);
// sh_series_free(temp2);

// free (temp);

// return energies;
// }


double *grid_calc_stats(struct sh_series **skymap, struct detector_response_grid *response, struct analysisData *inData,int clusterIdx, char *matFileName, FILE *skyposOut, double triggerTime)
{
/* Given the above skymap and antenna responses, generate the statistics required  
 * Skymap holds the spherical harmonic expansion for each detector pair over the time range of
 * interest,while 
 * OLD - Response holds the spherical harmonic expansion for each polarisation and detector pairing
 * NEW - response is already a mapping - holds response for each single detector, unmodified.
 */

if (!(skymap))
{
  mexPrintf( "Correlation failed, exiting stat generation\n");
  return NULL;
}


/* Arrays to hold the weighted, normalised antenna response */
double *ep[response->nInstr], *ex[response->nInstr], *enull[response->nInstr];
double *eta;
double *magFp, *magFx, *magFn;
complex double *skyMap[response->nBaselines];
complex double tempP, tempX, tempN, tempTik;

/* Other variables */
int pairIdx, gridIdx, i, j, k, idx, skyIdx;

double *Ep, *Ex, *Esl, *Enull, *EnullAF ,*Esoft;
double *Ip, *Ix, *Isl, *Inull, *InullAF;

/* Modified SL */
double *Ecoherent;
double *EmodSL;
double *ratioSky;
double *logRatioSky;

double *total;
double *temp;
char *skyMapName;
double *energies;

/* Tikhonov regularised standard likelihood 
*/
int sNo = 0;
int strainValues = 7;
double strain[] = {1e-23, 5e-23, 1e-22, 5e-22, 1e-21, 5e-21, 1e-20};
mexPrintf("\t%d regularising amplitudes:\n\t  {", strainValues);
for (i=0;i<strainValues;i++)
  mexPrintf("%e, ", strain[i]);
mexPrintf("}\n");

double *TRmap;
TRmap = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
/**/

/* Coherent energies */
Ep    = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
Ex    = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
Esl   = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
Enull = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
EnullAF = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
Esoft = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));

/* Incoherent energies */
Ip  = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
Ix  = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
Isl = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
Inull = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
InullAF = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));

/* Total energy */
total = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));

/* Modified SL */
Ecoherent = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
EmodSL = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));

/* ratioSkymap */
ratioSky = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));
logRatioSky = (double *) malloc(response->gridSize*response->gridSize*sizeof(double));

/* Pixel skymap */
energies = (double *) malloc (sizeof(double)*10);
skyMapName = (char *)malloc(30*sizeof(char));

/* Memory allocated? */
if (!(energies || skyMapName || total || Esoft || Ep || Ex || Esl || Enull || EnullAF || Ip || Ix || Isl || Inull || InullAF || Ecoherent || EmodSL || ratioSky || logRatioSky))
{
  fprintf(stderr, "MEM ERROR: Cannot allocate memory for stat generation\n");
  return NULL;
}

eta = (double *) malloc (response->gridSize*response->gridSize*sizeof(double));

/* Allocate memory for orthonormal responses */
for (idx = 0; idx < response->nInstr; idx++)
{
  ep[idx] = (double *) malloc (response->gridSize*response->gridSize*sizeof(double));
  ex[idx] = (double *) malloc (response->gridSize*response->gridSize*sizeof(double));
  enull[idx] = (double *) malloc (response->gridSize*response->gridSize*sizeof(double));

  if (!ep[idx] || !ex[idx] || !enull[idx])
  {
    fprintf(stderr, "MEM ERROR: Cannot allocate memory for stat generation\n");
    return NULL;
  }    
}

/* Allocate memory for antenna magnitudes */
magFp = (double *) malloc (response->gridSize*response->gridSize*sizeof(double));
magFx = (double *) malloc (response->gridSize*response->gridSize*sizeof(double));
magFn = (double *) malloc (response->gridSize*response->gridSize*sizeof(double));

if (!magFp || !magFx || !magFn)
{
  fprintf(stderr, "MEM ERROR: Cannot allocate memory for stat generation\n");
  return NULL;
}    

/* Allocate memory for skymaps */
for (idx = 0; idx < response->nBaselines; idx++)
{
  skyMap[idx] = (complex double *) malloc (response->gridSize*response->gridSize*sizeof(complex double));
 
  if (!(skyMap[idx]))
  {
    fprintf(stderr, "MEM ERROR: Cannot allocate memory for stat generation\n");
    return NULL;
  }   
  
  /* Inverse transform to spatial domain */
  FSHT_inverse(skyMap[idx], skymap[idx]->coeff, skymap[idx]->l_max+1);
}

/* Generate our whitened, normalised antenna factors */
/* Same order as the correlation output 
form_response_grid(ep, ex, eta, response, inData->freqRange, inData->nPSpectrum);
*/

/* If the requested FFT len is different to the sample frequeny, then use the their ratio to extract the correct PSD frequencies */
int fRange[2];
fRange[0] = floor(inData->freqRange[0]*inData->freqRatio);
fRange[1] = ceil(inData->freqRange[1]*inData->freqRatio);

mexPrintf("\tFrequency change-up: Index(%d->%d)->Freq(%d->%d)\n", inData->freqRange[0], inData->freqRange[1], fRange[0], fRange[1]);

generate_response_grid(enull, magFn, ep, magFp, ex, magFx, eta, response, NULL, response->gridSize, inData->nInstr, inData->freqRange/*fRange*/,  inData->nPSpectrum);

/* Output single detector responses */
if ((inData->debugLevel >= 1) && (loop ==0))
{
  for (idx =0; idx < response->nBaselines; idx++)
	{
		sprintf(skyMapName, "raw_sky%d", (idx+1));
  		product_output(matFileName, skyMapName, skyMap[idx],response->gridSize ,response->gridSize,3);
	}
 

  sprintf(skyMapName, "Hep_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, ep[0],response->gridSize ,response->gridSize,3);

  sprintf(skyMapName, "Lep_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, ep[1],response->gridSize ,response->gridSize,3);

  sprintf(skyMapName, "Vep_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, ep[2],response->gridSize,response->gridSize,3);

  sprintf(skyMapName, "Hex_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, ex[0],response->gridSize,response->gridSize,3);

  sprintf(skyMapName, "Lex_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, ex[1],response->gridSize,response->gridSize,3);

  sprintf(skyMapName, "Vex_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, ex[2],response->gridSize,response->gridSize,3);

  sprintf(skyMapName, "Henull_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, enull[0],response->gridSize,response->gridSize,3);

  sprintf(skyMapName, "Lenull_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, enull[1],response->gridSize,response->gridSize,3);

  sprintf(skyMapName, "Venull_response%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, enull[2],response->gridSize,response->gridSize,3);

loop++;
}

/* Form the statistics */
/* Cross terms - 9 terms, 6 unique, 3 auto correlation, and 1 set of off-diagonal */
/* Loop over sky */
for (skyIdx = 0; skyIdx < response->gridSize*response->gridSize; skyIdx++)
{
  tempP=0;tempX=0, tempN=0; tempTik=0;
  total[skyIdx] = 0;

  /* ep, ex and eta are the single variety */
  k = 0;
  for(i = 1; i < response->nInstr; i++)
    for(j = 0; j < i; j++, k++)
    {
      /* Each cross-term appears twice i.e. HL and LH, so multiple by 2 */
      tempP += 2.*creal(skyMap[k][skyIdx])*ep[i][skyIdx]*ep[j][skyIdx];
      tempX += 2.*creal(skyMap[k][skyIdx])*ex[i][skyIdx]*ex[j][skyIdx];

      tempN += 2.*creal(skyMap[k][skyIdx])*enull[i][skyIdx]*enull[j][skyIdx];
    }
	  
  /* Standard likelihood should be a real number, so take that part of it */
  Ep[skyIdx] = creal(tempP);
  Ex[skyIdx] = creal(tempX);
  EnullAF[skyIdx] = creal(tempN);

  /* Tikhonov regularised */ 
  TRmap[skyIdx] = creal(tempTik);

  /* Generated coherent part of standard likelihood */
  Ecoherent[skyIdx] = creal(tempP+tempX);

  /* Auto correlation components */
  tempP=0;tempX=0; tempN=0; tempTik = 0;
  for (pairIdx = response->nBaselines-response->nInstr; pairIdx < response->nBaselines ;pairIdx++)
  {
    tempP += creal(skyMap[pairIdx][skyIdx]*ep[pairIdx-(response->nBaselines-response->nInstr)][skyIdx]*ep[pairIdx-(response->nBaselines-response->nInstr)][skyIdx]);
    tempX += creal(skyMap[pairIdx][skyIdx]*ex[pairIdx-(response->nBaselines-response->nInstr)][skyIdx]*ex[pairIdx-(response->nBaselines-response->nInstr)][skyIdx]);

    tempN += creal(skyMap[pairIdx][skyIdx])*enull[pairIdx-(response->nBaselines-response->nInstr)][skyIdx]*enull[pairIdx-(response->nBaselines-response->nInstr)][skyIdx];

    /* Sum diagonal to get total energy */
    total[skyIdx] += sqrt(pow(skyMap[pairIdx][skyIdx], 2));
/*      total[skyIdx] += creal(pow(skyMap[pairIdx][skyIdx], 2))+cimag(pow(skyMap[pairIdx][skyIdx], 2));*/

  }  
 
   
  /* Coherent */
  Ep[skyIdx] += creal(tempP);
  Ex[skyIdx] += creal(tempX);
  EnullAF[skyIdx] += creal(tempN);

  /* Incoherent */
  Ip[skyIdx] = creal(tempP);
  Ix[skyIdx] = creal(tempX);
  InullAF[skyIdx] = creal(tempN);

  /* Tikhonov regularised */
  tempTik =0;
  for (sNo=0; sNo<strainValues;sNo++)
  {
        double loopTemp = genTik(Ep[skyIdx], Ex[skyIdx], magFp[skyIdx], magFx[skyIdx], strain[sNo]);

	tempTik += (loopTemp);/*
	tempTik = logsumexp(tempTik, loopTemp);
*/
   }

  
  /* Tikhonov regularised */
  TRmap[skyIdx] += creal(tempTik);

  /* ratioSkymap calulcation - sum the ratio of each energy */
  ratioSky[skyIdx] = Ep[skyIdx]/Ip[skyIdx] + Ex[skyIdx]/Ix[skyIdx] + InullAF[skyIdx]/EnullAF[skyIdx];
  /* Test with the two sided pass on */
  logRatioSky[skyIdx] = abs(log(Ep[skyIdx]/Ip[skyIdx])) + abs(log(Ex[skyIdx]/Ix[skyIdx])) + InullAF[skyIdx]/EnullAF[skyIdx];

  /* Form soft constraint likelihood */
  Esoft[skyIdx] = Ep[skyIdx] + eta[skyIdx]*Ex[skyIdx];
  
  /* Form (in)coherent standard likelihood by adding plus and cross energies */
  Esl[skyIdx] = Ep[skyIdx]+Ex[skyIdx];  
  Isl[skyIdx] = Ip[skyIdx]+Ix[skyIdx];
    
  /* Null energies - (total energies -standard likelihood) */
  Enull[skyIdx] = total[skyIdx] - Esl[skyIdx];
  Inull[skyIdx] = total[skyIdx] - Isl[skyIdx];

  /* Generate actual modified standard likelihood */
  /* Lsky = (Esl*Ec)/(E*(E-Esl+|Ec|)) */
  EmodSL[skyIdx] = (Esl[skyIdx]*Ecoherent[skyIdx])/(total[skyIdx]*(total[skyIdx]-Esl[skyIdx]+abs(Ecoherent[skyIdx])));

}/* End over sky */


/*******************************************************************************************
 *    TESTING ONLY --- TESTING ONLY ----------- TESTING ONLY
 *   	DUMPING OUT ALL INTERNAL STATS
 *    TESTING ONLY --- TESTING ONLY ----------- TESTING ONLY
 *******************************************************************************************/
if (inData->debugLevel >= 1)
{
	char *bLine[6];
	int oLoop;

	bLine[0] = malloc(4); sprintf(bLine[0], "HL");
	bLine[1] = malloc(4); sprintf(bLine[1], "HV");
	bLine[2] = malloc(4); sprintf(bLine[2], "LV");
	bLine[3] = malloc(4); sprintf(bLine[3], "H");
	bLine[4] = malloc(4); sprintf(bLine[4], "L");
	bLine[5] = malloc(4); sprintf(bLine[5], "V");

	for (oLoop=0;oLoop<3;oLoop++)
	{
	  sprintf(skyMapName, "ep_%s_skymap%d", bLine[oLoop+3], (clusterIdx+1));
	  product_output("stat_juggling.mat", skyMapName, ep[oLoop],response->gridSize, response->gridSize,1);
	 
	  sprintf(skyMapName, "ex_%s_skymap%d",bLine[oLoop+3], (clusterIdx+1));
	  product_output("stat_juggling.mat", skyMapName, ex[oLoop],response->gridSize, response->gridSize,1);
	}

	for (oLoop=0;oLoop<6;oLoop++)
	{
	  sprintf(skyMapName, "raw_%s_skymap%d", bLine[oLoop], (clusterIdx+1));
	  product_output("stat_juggling.mat", skyMapName, skymap[oLoop], sh_series_length(skymap[oLoop]->l_max, skymap[oLoop]->polar), 1,2);

	  sprintf(skyMapName, "%s_skymap%d", bLine[oLoop], (clusterIdx+1));
	  product_output("stat_juggling.mat", skyMapName, skyMap[oLoop],response->gridSize, response->gridSize,3);
	}

	  sprintf(skyMapName, "magFp_skymap%d", (clusterIdx+1));
	  product_output("stat_juggling.mat", skyMapName, magFp,response->gridSize, response->gridSize,1);
	
	  sprintf(skyMapName, "magFx_skymap%d", (clusterIdx+1));
	  product_output("stat_juggling.mat", skyMapName, magFx,response->gridSize, response->gridSize,1);

	  sprintf(skyMapName, "magFn_skymap%d", (clusterIdx+1));
	  product_output("stat_juggling.mat", skyMapName, magFn,response->gridSize, response->gridSize,1);

	for (oLoop=0;oLoop<6;oLoop++)
		free(bLine[oLoop]);
}



/* Free memory associated with orthonormal responses */
for (idx = 0 ; idx < response->nInstr; idx++)
  {
    free(ep[idx]);
    free(ex[idx]);
    free(enull[idx]);
  }

/* Dump all data products */
if (inData->debugLevel >= 1)
{
  mexPrintf( "Writing out statistics\n");
  sprintf(skyMapName, "Ep_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ep, response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Ip_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ip,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Ex_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ex,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Ix_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ix,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Isl_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Isl,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Enull_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Enull,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Inull_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Inull,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Esoft_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Esoft,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "total_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, total,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "eta_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, eta,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "EnullAF_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, EnullAF,response->gridSize, response->gridSize,1);
}

/* Output standard likelihood over sky */
if (inData->debugLevel >= 0)
{
   sprintf(skyMapName, "Esl_skymap%d", (clusterIdx+1));
   product_output(matFileName, skyMapName,  Esl,response->gridSize, response->gridSize,1);
      
  sprintf(skyMapName, "Ep_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ep, response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Ip_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ip,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Ex_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ex,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Ix_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ix,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Enull_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, EnullAF,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Inull_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, InullAF,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "Ecoh_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, Ecoherent,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "modEsl_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, EmodSL,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "ratio_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, ratioSky,response->gridSize, response->gridSize,1);

  sprintf(skyMapName, "logratio_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, logRatioSky,response->gridSize, response->gridSize,1);
/**/
  sprintf(skyMapName, "tikhonov_skymap%d", (clusterIdx+1));
  product_output(matFileName, skyMapName, TRmap,response->gridSize, response->gridSize,1);
}


/* Determine most likely sky position from skymap */
/* Old method of sky position - cluster on skymap 
 temp = sky_position(Esl, response->gridSize ,response->gridSize, -1, 0.0);
 
temp = max_skyGrid(EmodSL, response->gridSize, response->gridSize);
*/
mexPrintf("\n-------------------------------------------------------------------------------\n");   
mexPrintf("\tStandard likelihood:\n");

if (inData->testLocalisation > 0)
 	temp = skyGrid_searched(Esl, response->gridSize, response->gridSize, inData);
else
	temp = max_skyGrid(Esl, response->gridSize, response->gridSize);
 
mexPrintf("\t    Sky position for Esl:  (%g, %g) ->E %g\n",temp[0], temp[1], temp[2]);
mexPrintf("\t                      -> (%g, %g) in radians\n", M_PI*(temp[0]/(2.*(inData->sky_l_max+1))), 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))));

mexPrintf("\t    Energies for said sky position: \n");
mexPrintf("\t           Ep -> %g\n", Ep[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ip -> %g\n", Ip[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ex -> %g\n", Ex[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ix -> %g\n", Ix[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("-------------------------------------------------------------------------------\n\n");   


if(skyposOut != NULL)
	fprintf(skyposOut, "%#017.6f \tsl \t%#1.3f \t%#1.3f \t%#.3f \t%#.3f \t%f \t",triggerTime, 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))), M_PI*(temp[0]/(2.*(inData->sky_l_max+1))),EnullAF[(int) round(temp[0]*response->gridSize+temp[1])],InullAF[(int) round(temp[0]*response->gridSize+temp[1])], temp[3]/temp[4]);	
free(temp);


/* Use ratio of energies to get sky position */
mexPrintf("\n-------------------------------------------------------------------------------\n");   
mexPrintf("\tSum of ratios:\n");

if (inData->testLocalisation > 0)
 	temp = skyGrid_searched(ratioSky, response->gridSize, response->gridSize, inData);
else
	temp = max_skyGrid(ratioSky, response->gridSize, response->gridSize);

mexPrintf("\t    Sky position for ratioSky:  (%g, %g) ->E %g\n",temp[0], temp[1], temp[2]);
mexPrintf("\t                      -> (%g, %g) in radians\n", M_PI*(temp[0]/(2.*(inData->sky_l_max+1))), 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))));

mexPrintf("\t    Energies for said sky position: \n");
mexPrintf("\t           Ep -> %g\n", Ep[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ip -> %g\n", Ip[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ex -> %g\n", Ex[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ix -> %g\n", Ix[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("-------------------------------------------------------------------------------\n\n");   

if(skyposOut != NULL)
	fprintf(skyposOut, "ratio \t%#1.3f \t%#1.3f \t%#.3f \t%#.3f \t%f \t", 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))), M_PI*(temp[0]/(2.*(inData->sky_l_max+1))) ,EnullAF[(int) round(temp[0]*response->gridSize+temp[1])],InullAF[(int) round(temp[0]*response->gridSize+temp[1])], temp[3]/temp[4]);	
free(temp);

/* Use log ratio of energies to get sky position */
mexPrintf("\n-------------------------------------------------------------------------------\n");   
mexPrintf("\tSum of log ratios:\n");

if (inData->testLocalisation > 0)
 	temp = skyGrid_searched(logRatioSky, response->gridSize, response->gridSize, inData);
else
	temp = max_skyGrid(logRatioSky, response->gridSize, response->gridSize);

mexPrintf("\t    Sky position for logRatioSky:  (%g, %g) ->E %g\n",temp[0], temp[1], temp[2]);
mexPrintf("\t                      -> (%g, %g) in radians\n", M_PI*(temp[0]/(2.*(inData->sky_l_max+1))), 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))));

mexPrintf("\t    Energies for said sky position: \n");
mexPrintf("\t           Ep -> %g\n", Ep[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ip -> %g\n", Ip[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ex -> %g\n", Ex[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ix -> %g\n", Ix[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("-------------------------------------------------------------------------------\n\n");   

if(skyposOut != NULL)
	fprintf(skyposOut, "logratio \t%#1.3f \t%#1.3f \t%#.3f \t%#.3f \t%f \t", 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))), M_PI*(temp[0]/(2.*(inData->sky_l_max+1))) ,EnullAF[(int) round(temp[0]*response->gridSize+temp[1])],InullAF[(int) round(temp[0]*response->gridSize+temp[1])], temp[3]/temp[4]);	
free(temp);


/* Use Tikhonov regularised standard likelihood to get sky position 
*/mexPrintf("\n-------------------------------------------------------------------------------\n");   
mexPrintf("\t  Tikhonov regulised:\n");

if (inData->testLocalisation > 0)
	temp = skyGrid_searched(TRmap, response->gridSize, response->gridSize, inData);
else
	temp = max_skyGrid(TRmap, response->gridSize, response->gridSize);
 
mexPrintf("\t    Sky position for TRmap:  (%g, %g) ->E %g\n",temp[0], temp[1], temp[2]);
mexPrintf("\t                      -> (%g, %g) in radians\n", M_PI*(temp[0]/(2.*(inData->sky_l_max+1))), 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))));

mexPrintf("\t    Energies for said sky position: \n");
mexPrintf("\t           Ep -> %g\n", Ep[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ip -> %g\n", Ip[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ex -> %g\n", Ex[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("\t           Ix -> %g\n", Ix[(int) round(temp[0]*response->gridSize+temp[1])]);
mexPrintf("-------------------------------------------------------------------------------\n\n");   

if(skyposOut != NULL)
	fprintf(skyposOut, "Tikhonov \t%#1.3f \t%#1.3f \t%#.3f \t%#.3f \t%f \t", 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))), M_PI*(temp[0]/(2.*(inData->sky_l_max+1))) ,EnullAF[(int) round(temp[0]*response->gridSize+temp[1])],InullAF[(int) round(temp[0]*response->gridSize+temp[1])], temp[3]/temp[4]);	
free(temp);
/**/

/* New method - just get position of maximum */
/*   this should really use Esl 
temp = max_skyGrid(Esl, response->gridSize ,response->gridSize);
*/
mexPrintf("\tmodEsl:\n");
if (inData->testLocalisation > 0)
        temp = skyGrid_searched(EmodSL, response->gridSize, response->gridSize, inData);
else
        temp = max_skyGrid(EmodSL, response->gridSize, response->gridSize);

if(skyposOut != NULL)
	fprintf(skyposOut, "cWb \t%#1.3f \t%#1.3f \t%#.3f \t%#.3f \t%f\n", 2.*M_PI*(temp[1]/(2.*(inData->sky_l_max+1))), M_PI*(temp[0]/(2.*(inData->sky_l_max+1))),EnullAF[(int) round(temp[0]*response->gridSize+temp[1])],InullAF[(int) round(temp[0]*response->gridSize+temp[1])], temp[3]/temp[4]);	

/* By my calcs, the above grid is :

(skypos[0]/xSize)*pi   
(skyPos[1]/ySize)*2*pi
*/


/* If sky position code failed, return 0's for everything */
if (temp == NULL)
{
  for (i=0;i<10;i++)
	energies[i]= 0;

  for (idx = 0; idx < response->nBaselines; idx++)
      free(skyMap[idx]);
  
  free(skyMapName);
  free(temp);
  
  free(Ep);
  free(Ex);
  free(Esl);
  free(Enull);
  free(EnullAF);

  free(Ip);
  free(Ix);
  free(Isl);
  free(Inull);
  free(InullAF);
  free(eta);

  free(total);

  free(Ecoherent);
  free(EmodSL);
  free(ratioSky);
  free(logRatioSky);

/**/ 
  free(magFp);
  free(magFx);
  free(magFn);
  free(TRmap);
/**/

  return energies;
}

/* Output */
mexPrintf("Pos (%g, %g)",inData->skyPos[0], inData->skyPos[1]);
if ((inData->skyPos[0] < 0) || (inData->testLocalisation > 0))
{  
  energies[0] = temp[0];	/* theta */
  energies[1] = temp[1];	/* phi */
  energies[2] = temp[2];	/* Esl */

  /* As we are using modified Esl to get sky position, get Esl from that position */
  energies[2] = Esl[(int) round(energies[0]*response->gridSize+energies[1])];
}
else
{
 /* Due to the way that the array is oriented in memory, we must subtract the requested theta value
    from PI, and the phi value from 2*PI */
 energies[0] = (int)round(((inData->skyPos[0])/M_PI)*(2.*(inData->sky_l_max+1)));			/* theta */
 energies[1] = (int)round(((inData->skyPos[1])/(2.*M_PI))*(2.*(inData->sky_l_max+1)));	/* phi - need to subtract from 2*PI due to memory ordering */
 energies[2] = Esl[(int) round(energies[0]*response->gridSize+energies[1])];			/* Esl */
}

mexPrintf("\t    Sky position for modEsl:  (%g, %g) ->E %g\n",energies[0], energies[1], energies[2]);
mexPrintf("\t                      -> (%g, %g) in radians\n", M_PI*(energies[0]/(2.*(inData->sky_l_max+1))), 2.*M_PI*(energies[1]/(2.*(inData->sky_l_max+1))));


mexPrintf("\t    Energies for said sky position: \n");
mexPrintf("\t           Ep(0,1) -> %g\n", Ep[(int) round(energies[0]*response->gridSize+energies[1])]);
mexPrintf("\t           Ip(0,1) -> %g\n", Ip[(int) round(energies[0]*response->gridSize+energies[1])]);
mexPrintf("\t           Ex(0,1) -> %g\n", Ex[(int) round(energies[0]*response->gridSize+energies[1])]);
mexPrintf("\t           Ix(0,1) -> %g\n", Ix[(int) round(energies[0]*response->gridSize+energies[1])]);

/* Extract energies corresponding to found sky position */
/* Coherent */
energies[3] = Ep[(int) round(energies[0]*response->gridSize+energies[1])];	/* Ep */
energies[4] = Ex[(int) round(energies[0]*response->gridSize+energies[1])];	/* Ex */

/* Incoherent */
energies[5] = Ip[(int) round(energies[0]*response->gridSize+energies[1])];	/* Ip */
energies[6] = Ix[(int) round(energies[0]*response->gridSize+energies[1])];	/* Ix */
energies[7] = Isl[(int) round(energies[0]*response->gridSize+energies[1])];	/* Isl */

/* Null (total - standard)*/
energies[8] = EnullAF[(int) round(energies[0]*response->gridSize+energies[1])];	/* Enull */
energies[9] = InullAF[(int) round(energies[0]*response->gridSize+energies[1])];	/* Inull */

/* Rotate sky due to GPS time, and get sky position 
  Ignore for now, need to use for real analysis, or output both
free(temp);
temp = sky_position(Esl, response->gridSize ,response->gridSize, inData->blockStartTime, triggerTime);
mexPrintf( "\tRotated sky position: (%g, %g) \n",energies[0],energies[1]);

if (temp)
{
  energies[0] = temp[0];	/* rotated theta 
  energies[1] = temp[1];	/* rotated phi 
  mexPrintf( " -> rotated (%g, %g) \n",energies[0],energies[1]);
}
*/

/* Free memory */
for (idx = 0; idx < response->nBaselines; idx++)
  free(skyMap[idx]);

free(skyMapName);

free(Ep);
free(Ex);
free(Esl);
free(Enull);
free(EnullAF);

free(Ip);
free(Ix);
free(Isl);
free(Inull);
free(InullAF);

free (total);
free (temp);
free (eta);
free (Esoft);

free(Ecoherent);
free(EmodSL);
free(ratioSky);
free(logRatioSky);

/**/
free(magFp);
free(magFx);
free(magFn);
free(TRmap);
/**/

return energies;
}
