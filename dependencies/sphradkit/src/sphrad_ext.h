/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __SPHRAD_EXT_H__
#define __SPHRAD_EXT_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include <correlator.h>
#include <instrument.h>
#include <sh_series.h>
#include <instruments.h>

#include "sphrad_response.h"

struct sh_series_array *unsummed_correlator_network_integrate_power_fd(struct sh_series_array *, complex double **, struct correlator_network_plan_fd *);

struct sh_series *fsum_correlator_network_integrate_power_fd(struct sh_series *, complex double **, struct correlator_network_plan_fd *);
struct sh_series *fsum_correlator_baseline_integrate_power_fd(const complex double *, const complex double *, struct correlator_plan_fd *, struct sh_series *);
struct sh_series *fsum_sh_series_array_dotc(struct sh_series *, struct sh_series *,const struct sh_series_array *, const complex double *, const struct sh_series_rotation_plan *, struct sh_series *);

/* Frequency range correlator */
struct sh_series *correlator_network_integrate_power_fd_range(struct sh_series *, complex double **, struct correlator_network_plan_fd *, int *);
struct sh_series *correlator_baseline_integrate_power_fd_range(const complex double *, const complex double *, struct correlator_plan_fd *, int *);


/* Decompose correlation output so that we can sum over frequency */
struct sh_series *decompose_sh_series_array_dotc(struct sh_series *, struct sh_series *,const struct sh_series_array *, const complex double *, const struct sh_series_rotation_plan *, struct sh_series_array *);
struct sh_series_array *decompose_correlator_baseline_integrate_power_fd(const complex double *, const complex double *, struct correlator_plan_fd *, struct sh_series_array *tf_map);
struct sh_series_array *decompose_correlator_network_integrate_power_fd(struct sh_series_array *, complex double **, struct correlator_network_plan_fd *plan);

/* Generate tf map for standard likelihood */
struct sh_series *generate_full_tfmap(struct sh_series_array *, complex double **, struct correlator_network_plan_fd *, struct detector_response *);

/* Need to have baselines for pairs of detectors and each single detector site */
struct sh_series *correlator_allnetwork_integrate_power_fd(struct sh_series *, complex double **, struct correlator_network_plan_fd *);
struct correlator_network_baselines *correlator_network_allbaselines_new(const struct instrument * const *, int );
void correlator_network_allbaselines_free(struct correlator_network_baselines *);

/* Generate delay_product et al in glorious 2d */
struct correlator_plan_fd *full_correlator_plan_fd_new(const struct correlator_baseline *, int , double );
struct correlator_plan_td *full_correlator_plan_td_new(const struct correlator_baseline *, double );
struct correlator_network_plan_fd *full_correlator_network_plan_fd_new(struct correlator_network_baselines *, int , double );

/* Generate delay_product with the specified expansion order */
struct correlator_network_plan_fd *user_correlator_network_plan_fd_new(struct correlator_network_baselines *, int , double delta_t, int *);

#endif  /* __SPHRAD_EXT_H__ */
