/*
 * Copyright (C) 2010  Mark Edwards
 *   (based on code written by Kipp Cannon, Copyright (C) 2006)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __SPHRAD_MISC_H__
#define __SPHRAD_MISC_H__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include "mex.h"

#include "sphrad.h"
#include "sphrad_response.h"

#include <correlator.h>
#include <instrument.h>
#include <sh_series.h>
#include <instruments.h>

/*int *generate_timelags(int *, int, int , int , int);*/

int check_isnan(double );
int check_isinf(double );

int cross_product(double *, double *, double *);

int cmpSingleDouble  (const void* , const void*);
int cmpMultiDouble   (const void* , const void*);
int cmpMultiDoubleMP (const void* , const void*);

/* Transform to the spatial domain and combine */ 
struct sh_series *convolve_sh_series(const struct sh_series *, const struct sh_series *);
struct sh_series *convolve_sh_grid(const struct sh_series *, const double *);

complex double sh_sum (struct sh_series *);

/* Data conversion functions */
mxArray *shseries_to_mx(mxArray *, int, int, const void *);
mxArray *c_to_mx(mxArray *, int, int, const void *, int);
mxArray *cc_to_mx(mxArray *, int, int, const void *);
complex double *sh_to_grid(const struct sh_series *);

/* Rotate a gridded skymap to the specified GPS time */
// double *gps_rotate(double *, int , int , double );

double identify_candidate(const struct sh_series *, double );

int *get_lagIndex( int, int , int , int , int );

/* Data output routines */ 
char *make_filename(char *, char ** , char *, int , int , char *);
int write_to_mat(char *, char *, mxArray *);
int write_events(char *, struct analysisData *, double *, int );
int output_omega_event(int, FILE *, struct analysisData *, struct eventStruct **);
void out_delay(mxArray *, struct correlator_network_plan_fd *, int );
void diagnostics_dump_sh_series(const struct sh_series *, char *);
void product_output(char *, char *,void *, int, int ,int);

mxArray *set_out(int, int , int , complex double *);

/* Main workhorse */
int cross_loop(struct sh_series_array **, struct sh_series_array *, struct sh_series * , struct analysisData * , char *,struct correlator_network_plan_fd *, double **,struct detector_response *, int *, int *, int);

#endif  /* __SPHRAD_MISC_H__ */
