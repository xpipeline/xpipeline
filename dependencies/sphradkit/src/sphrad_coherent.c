/*
 * Copyright (C) 2010  Mark Edwards
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <fftw3.h>

#include "sphrad_coherent.h"
#include "sphrad.h"


/* Debugging specific includes */
#include "memwatch.h"

/*
 *
 *   Back to the data
 *
 *
 * The calculation for the plus and cross polarizations are the same, so this
 * function can be passed ec or ep and calculate the appropriate coherent and
 * incoherent energies.
 * Coherent component - cross-correlation terms of the energy in the polarization
 * of the DP frame.
 *  E = abs(sum(e .* d))^2;
 *   = abs(e(1).*d(1)+ e(3).*d(3)+ e(3).*d(3))))^2
 *   = (e(1)*d(1))*conj(e(1)*d(1))+...
 *
 * Incoherent energy component - autocorrelation terms of the energy in the 
 * polarization in the DP frame.
 *
 * I = sum(abs(e .* d).^2);
 *   = sum(e.*d.*conj(e).*conj(d))
 *   = e(1)*d(1)*e(1)*conj(d(1))+ e(2)*d(2)*e(2)*conj(d(2))+ e(3)*d(3)*e(3)*conj(d(3))
 *
 */
double * singlepolarization(int nInstr, double eIN[nInstr],  complex double dIN[nInstr])
{
int dIdx;
int i, j, k;
complex double Etemp=0;
complex double Itemp=0;

double *energies;
energies = (double *) malloc(2*sizeof(double));


fprintf(stdout, "Response: %g, %g, %g\n",eIN[0],eIN[1],eIN[2]);
fprintf(stdout, "Data: (%g,%g), (%g,%g), (%g,%g)\n",creal(dIN[0]), cimag(dIN[0]),creal(dIN[1]), cimag(dIN[1]),creal(dIN[2]),cimag(dIN[2]));


  /* Calculate the auto terms */
  for (dIdx=0; dIdx < nInstr; dIdx++)
    Itemp += eIN[dIdx]*dIN[dIdx]*eIN[dIdx]*conj(dIN[dIdx]);
  
  /* Add the auto terms into the coherent sum */ 
  Etemp=Itemp;

  
  /* Each cross-term appears twice i.e. HL and LH, so multiple by 2 */
  for(i = 1; i < nInstr; i++)
    for(j = 0; j < i; j++)
      Etemp += 2*eIN[i]*dIN[i]*eIN[j]*conj(dIN[j]);

  energies[0] = creal(Etemp);
  energies[1] = creal(Itemp);
  
  fprintf(stdout, "E-> %g\n", energies[0]);
  fprintf(stdout, "I-> %g\n", energies[1]);
  
return energies;
}

/* Version of the singlepolarization for use with the skymap returned by the cross-correlator */
double * pre_singlepolarization(int nInstr, double eIN[nInstr],  double dIN[nInstr * (nInstr - 1) / 2])
{
  /* In this function, dIN is equivalent to d*conj(d), so we just multiple by the correct 
     antenna response values */
int dIdx;
int i, j, k;
complex double Etemp=0;
complex double Itemp=0;

double *energies;
energies = (double *) malloc(2*sizeof(double));


fprintf(stdout, "Ant:  %g, %g, %g\n",eIN[0],eIN[1],eIN[2]);
fprintf(stdout, "Data: %g, %g, %g\n",dIN[0],dIN[1],dIN[2]);


  /* Calculate the auto terms */
  for (dIdx=0; dIdx < nInstr; dIdx++)
    Itemp += eIN[dIdx]*eIN[dIdx]*dIN[dIdx+nInstr];
  
  /* Add the auto terms into the coherent sum */ 
  Etemp=Itemp;

  /* Each cross-term appears twice i.e. HL and LH, so multiple by 2 */
  k=0;
  for(i = 1; i < nInstr; i++)
    for(j = 0; j < i; j++, k++)
      Etemp += 2*eIN[i]*eIN[j]*dIN[k];

  energies[0] = creal(Etemp);
  energies[1] = creal(Itemp);
  
  fprintf(stdout, "E-> %g\n", energies[0]);
  fprintf(stdout, "I-> %g\n", energies[1]);
  
return energies;
}



/*********************************************************************************
 *        Helper function - construct frequency strip around trigger             *
 ********************************************************************************/
 /* 
    From tfmap get frequency, bandwidth, time, duration
      - given segment number, we can get sample numbers
    
  */

complex double *pixelPower(int segmentNo[2], int eDuration, int eBandwidth, int eFrequency, int freqSpan,struct analysisData *inData)
{
int instrIdx;
  
double triggerTime;
int triggerStart, triggerEnd, triggerDuration, FFTLen, fRange;
int new_bandwith, freqIdx;
double binFreq, freqDiff;
complex double *d;
  
/* Use tile data to construct the frequency strip around the trigger time
Trigger time in samples */
triggerStart = (int)ceil(segmentNo[0] * (freqSpan/2));
triggerEnd = (int)floor(segmentNo[1] * (freqSpan/2) + (freqSpan));

/* Trigger duration in samples 
triggerDuration = eDuration * inData->sampleFrequency;
triggerDuration = pow(ceil(log2(triggerDuration)),2);
*/
/* Segment data 
triggerStart = ceil(triggerTime - triggerDuration/2);
triggerEnd = floor(triggerTime + triggerDuration/2);*/
fRange = triggerEnd-triggerStart;

/* Extract data around the trigger time, length of the duration
 Then transform into the frequency domain */

/* Fourier data structures */
complex double **fSegment;
fftw_plan fftplans[inData->nInstr];

fSegment = (complex double **)malloc(inData->nInstr*sizeof(complex double*));
d = (complex double *)malloc(inData->nInstr*sizeof(complex double));

double *ptrTS=NULL;


mexPrintf("t-series 1: %g, %g, %g\n", inData->series[0][triggerStart],inData->series[0][triggerStart+1],inData->series[0][triggerStart+2]);
mexPrintf("t-series 2: %g, %g, %g\n", inData->series[1][triggerStart],inData->series[1][triggerStart+1],inData->series[1][triggerStart+2]);
mexPrintf("t-series 3: %g, %g, %g\n", inData->series[2][triggerStart],inData->series[2][triggerStart+1],inData->series[2][triggerStart+2]);

double *tSeries;
tSeries = (double *) malloc(fRange*sizeof(double));

for (instrIdx = 0; instrIdx < inData->nInstr; instrIdx++)
{
  fSegment[instrIdx] = malloc (fRange*sizeof(**fSegment));

  /* Create the FFT transform plans with empty data, populate before executing */ 
  fftplans[instrIdx] = correlator_tseries_to_fseries_plan(tSeries, fSegment[instrIdx], fRange);

  if ((fSegment[instrIdx] == NULL) || (fftplans[instrIdx] == NULL))
  {
    mexPrintf( "MEM ERROR: Unable to allocate memory for FFT structures\n");
    return NULL;
  }
 
  memcpy(tSeries, &inData->series[instrIdx][triggerStart], fRange*sizeof(double));
  correlator_tseries_to_fseries(tSeries, fSegment[instrIdx], fRange, fftplans[instrIdx]);      
}

mexPrintf("F-series 1: (%g,%g), (%g,%g), (%g,%g)\n", creal(fSegment[0][0]),cimag(fSegment[0][0]),creal(fSegment[0][1]),cimag(fSegment[0][1]),creal(fSegment[0][2]),cimag(fSegment[0][2]));
mexPrintf("F-series 2: (%g,%g), (%g,%g), (%g,%g)\n", creal(fSegment[1][0]),cimag(fSegment[1][0]),creal(fSegment[1][1]),cimag(fSegment[1][1]),creal(fSegment[1][2]),cimag(fSegment[1][2]));
mexPrintf("F-series 3: (%g,%g), (%g,%g), (%g,%g)\n", creal(fSegment[2][0]),cimag(fSegment[2][0]),creal(fSegment[2][1]),cimag(fSegment[2][1]),creal(fSegment[2][2]),cimag(fSegment[2][2]));




/* Need to determine which bin our trigger lies in */
freqIdx =(int) round((eFrequency/(inData->sampleFrequency/2.0))*(fRange/2.0));
new_bandwith =(eBandwidth/(inData->sampleFrequency/2.0))*(fRange/2.0);

for(instrIdx = 0; instrIdx < inData->nInstr ; instrIdx++) 
{
    d[instrIdx] = fSegment[instrIdx][freqIdx];
  
    free(fSegment[instrIdx]);
    fftw_destroy_plan(fftplans[instrIdx]);
}
free(fSegment);
free(tSeries);

/* Do frequency shift to center requested frequency in bin? */

mexPrintf("triggerSamples: %d-%d\nfreqIdx: %d\n fRange: %d\n", triggerStart, triggerEnd, freqIdx, fRange);

return d;
}


double *minFind(double *array, int size)
{
  int i;
  double minValue=0;
  double minIdx=0;
  double *returnValue;
  
  returnValue = (double *)malloc(2*sizeof(double));
  
  minValue = array[0];
  minIdx = 0.0;
  
  for (i=1; i<size;i++)
    if (array[i]<minValue)
    {
      minValue = array[i];
      minIdx = i;
    }

  returnValue[0] = minIdx;
  returnValue[1] = minValue;
return returnValue;
}



/********************************************************************************
* timeShift function
********************************************************************************/

double *ts_timeShift(double *data, double delay, int dataLength)
{
/* data is passed in as a timeseries
 * It is always returned as a shifted timeseries
 * Positive shifts to the left, and negative to the right */

int idx;
/********************************************************************************
* check passed paramters
********************************************************************************/

/* If no delay, just return timeseries  */
if (delay == 0)
    return data;

/* If time series data, convert to frequency domain 
data = fft(data);*/

/*********************************************
* calculate frequency bins and phase shift
**********************************************/

double *binFreq, *phase, *rot;
binFreq = (double *) malloc (dataLength*sizeof(double));
phase = (double *) malloc (dataLength*sizeof(double));
rot = (double *) malloc (dataLength*sizeof(double));

for (idx=0; idx<(dataLength-1); idx++)
{
  binFreq[idx] = ((idx+(int)floor(dataLength/2.)) % dataLength)-floor(dataLength/2);
  phase[idx] = (2.*M_PI*delay/dataLength)*binFreq[idx];
  rot[idx] = exp(I*phase[idx]);
}

/* Check for even length - we may lose information */
if ((dataLength % 2)==0)
  rot[dataLength/2+1]=cos(phase[dataLength/2+1]);


for (idx=0; idx<(dataLength-1); idx++)
  data[idx] = data[idx] * rot[idx];
/*
data=ifft(data);
data=real(data);
*/
return data;
}


/********************************************************************************
* frequency timeShift function
********************************************************************************/

void *freq_timeShift(complex double *data, double delay, int dataLength)
{
/* data is passed in as a frequency representation
 * Positive shifts to the left, and negative to the right */

int idx;

/*********************************************
* calculate frequency bins and phase shift
**********************************************/

double *binFreq, *phase, *rot;
binFreq = (double *) malloc (dataLength*sizeof(double));

phase = (double *) malloc (dataLength*sizeof(double));
rot = (double *) malloc (dataLength*sizeof(double));

for (idx=0; idx<(dataLength-1); idx++)
{
  binFreq[idx] = ((idx+(int)floor(dataLength/2.)) % dataLength)-floor(dataLength/2);
  phase[idx] = (2.*M_PI*delay/dataLength)*binFreq[idx];
  rot[idx] = exp(I*phase[idx]);
}

/* Check for even length - we may lose information */
if ((dataLength % 2)==0)
  rot[dataLength/2+1]=cos(phase[dataLength/2+1]);


for (idx=0; idx<(dataLength-1); idx++)
  data[idx] = data[idx] * rot[idx];
return data;
}
