/*
 * Copyright (C) 2010  Mark Edwards
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __SPHRAD_RESPONSE_H__
#define __SPHRAD_RESPONSE_H__

/* General includes */
#include <complex.h>
#include <math.h>

/* SH series functions and typedefs*/
#include <sh_series.h>

/* Detector structures */
#include <lal/DetResponse.h>
#include <lal/LALDatatypes.h>

/* Plus and cross for each detector pair  */
struct detector_response {
	struct sh_series *plus;
	struct sh_series *cross;
	int nBaselines;
	int nInstr;
};

struct detector_polarisations
{
	double *plus;
	double *cross;
};

/* Plus and cross for each detector pair  */
struct detector_response_grid 
{
	unsigned int max_l;
	int gridSize;
	int nInstr;
	int nBaselines;
	struct detector_polarisations *polarisations;
};

struct response_Nelement_data {
	const LALDetector **det;
	const int *crossDet;
	const int numDet;
	const int isPlus;
};

/* Functions to manipulate structures holding the antenna reponses */
void detector_response_free(struct detector_response *);
struct detector_response *detector_response_new(const LALDetector **, int, unsigned int );

void detector_response_grid_free(struct detector_response_grid *);
struct detector_response_grid *detector_response_grid_new(const LALDetector **, int, unsigned int );

int generate_response_grid(double **, double *, double **,double *, double **, double *, double *, struct detector_response_grid * ,const LALDetector **, int , int , int *, double **);

/* Functions to generate the antenna reponses */
static void Fzero(double *, double *, int , const LALDetector **, double , double );
static void FplusNDP(double * , double *, int, const LALDetector **, double , double );
static double response_Nelement(double , double , void *);
struct sh_series *responseN(const LALDetector **, int, unsigned int , int *, int );
int form_response(double **, double **,const LALDetector **, struct detector_response *,int,  double **);

int return_response(double *, double *, struct detector_response_grid *, int , int , double **);


#endif  /* __SPHRAD_RESPONSE_H__ */

