
FrameLib Installation Instructions

These notes are adapted from the helpful site http://www.gw-indigo.org/tiki-index.php?page=The+Data+Set

Installation Steps:

Download and unpack the libframe source code.
```
wget http://lappweb.in2p3.fr/virgo/FrameL/libframe-8.15.tar.gz
gunzip libframe-8.15.tar.gz
tar -xvf libframe-8.15.tar
```
- You can also get it from xpipeline/trunk/dependencies/LibFrame
- You can dump the source code anywhere; we will delete the source code once installed.
- Note that libframe-8.15 seems to work with gcc 4.8.5 but libframe-8.22 does not due to an unrecognised option -flat_namespace that gcc can't handle.

Create an installation location. I like to install packages under ~/opt/ (e.g. /home/patrick.sutton/opt/libframe).
```
mkdir -p SUITABLE_FOLDER_WHERE_YOU_WANT_TO_INSTALL
```

Compile and install the base version.
```
pushd libframe-8.15
./configure --prefix=SUITABLE_FOLDER_WHERE_YOU_WANT_TO_INSTALL
make
make install
popd
```
The install folder will now contain the subdirectories bin, include, lib, share. 

Mex compile frgetvect:
```
pushd libframe-8.15/matlab
matlab -nodisplay
>> mex frgetvect.c SUITABLE_FOLDER_WHERE_YOU_WANT_TO_INSTALL/lib/libFrame.so -I../src
>> quit
mkdir SUITABLE_FOLDER_WHERE_YOU_WANT_TO_INSTALL/matlab
cp frgetvect.m* SUITABLE_FOLDER_WHERE_YOU_WANT_TO_INSTALL/matlab
popd
```
This mex procedure produces the file frgetvect.mexa64 in your libframe-8.15/matlab folder.  The last steps are to copy the .mex and .m files into a new folder in your install directory; this is optional but puts all the installation products in one place. 

Clean up! You can now delete the source code:
```
rm -rf libframe-8.15
```



Using frgetvect in Interactive MatLab:

The following steps are required and sufficient for frame reading (e.g. there is no need to source xpipeline's envsetup.sh).
1. Add the directory containing libFrame.so.1 to LD_LIBRARY_PATH, e.g.:
```
export LD_LIBRARY_PATH=SUITABLE_FOLDER_WHERE_YOU_WANT_TO_INSTALL/lib/:$LD_LIBRARY_PATH
```
1. Add the directory containing frgetvect.m, frgetvect.mexa64 to the matlab path, e.g.: 
```
addpath SUITABLE_FOLDER_WHERE_YOU_WANT_TO_INSTALL/matlab/
```

Here is an example script to run in matlab to read a frame at CIT. I have installed in /home/patrick.sutton/opt/libframe/ :
```
addpath /home/patrick.sutton/opt/libframe/matlab
file = '/hdfs/frames/ER8/hoft_C01/H1/H-H1_HOFT_C01-11263/H-H1_HOFT_C01-1126301696-4096.gwf'
channel = 'H1:DCS-CALIB_STRAIN_C01'
gpsStart = 1126301701;
gpsDur = 1;
[data,tsamp,fsamp,gps0,strStart,unitX,unitY] = frgetvect(file, channel,  gpsStart,gpsDur);
max(abs(data))
```



Using frgetvect in Compiled MatLab Jobs (X-Pipeline):


For X-Pipeline compiled jobs to run there is only one requirement: add the directory containing libFrame.so.1 to LD_LIBRARY_PATH in envsetup.sh, e.g.
```
export LD_LIBRARY_PATH=/home/patrick.sutton/opt/libframe/lib/:/home/xpipeline/opt/xpipeline/r6028/lib:/usr/lib64:$LD_LIBRARY_PATH
```



